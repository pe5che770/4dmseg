#include "cudaTool.h"
#include <cuda.h>
#include "cudaHelper.h"
#include <iostream>
#include <math.h>

//#include "kinectIntrinsics.h"
//#include "kinectImplementation.h"

#define KERNEL_WIDTH_2 5
//(Kernel_width_2*2+1)^2
#define NUM_THREADS 49

//yes, these should be parameters.
#define SIGMA_S 3
#define SIGMA_D 0.05f

//#define FLYING_CONE 0.8f
#define FLYING_CONE 0.95f

texture<float,2> ct_tex_in;
texture<float,2> tex_filtered_depth;
__constant__ float exp_x2[64];

//description of the sensor used.
__constant__ float unproject[16];
__constant__ int ct_width_height[2];

#define CAST_I2X(I,Z) (-(unproject[0]*(I)				 + unproject[8]*(Z) + unproject[12])* (Z))
#define CAST_J2Y(J,Z) ((				unproject[5]*(J) + unproject[9]*(Z) + unproject[13])* (Z))
//*/

//simple test to see if buffers are adressed correctly
__global__ void inOutTest(float *target){
	//this method assumes the block dims are width x height, as well as the texture.
	int x = blockIdx.x;
	int y = blockIdx.y;
	int offset = blockIdx.x + blockIdx.y*ct_width_height[0];//WIDTH;

	target[offset] = tex2D(ct_tex_in, x, y);
}

/*__global__ void position(float){
	int i = blockIdx.x;
	int j = blockIdx.y;
	int offset = blockIdx.x + blockIdx.y*ct_width_height[0];//WIDTH;

	float z = tex2D(tex_filtered_depth, i,j);
	float x = CAST_I2X(i,z);
	float y = CAST_J2Y(j,z);
}*/

//compute the normalsm based on the depth values in the filtered_depth texture
__global__ void normalKernel(float *targetNormals){//(float * nx, float * ny, float * nz){
	//this method again assumes the block dim is widthxheight, and the same for the texture.
	int i = blockIdx.x;
	int j = blockIdx.y;
	int offset = blockIdx.x + blockIdx.y*ct_width_height[0];//WIDTH;
	float z, zip1, zjp1, rnrm;
	float3 target_nml;
	float3 vec1, vec2, vec3;

	z=tex2D(tex_filtered_depth, i,j);
	zip1 = tex2D(tex_filtered_depth, i+1,j);
	zjp1 = tex2D(tex_filtered_depth, i,j+1);
	
		
	//position computation
	//is correct.
	//x = CAST_I2X(i,z);
	//y = CAST_J2Y(j,z);
	vec1.x = CAST_I2X(i,z); vec1.y= CAST_J2Y(j,z); vec1.z = z;
	vec2.x = CAST_I2X(i+1,zip1); vec2.y= CAST_J2Y(j,zip1); vec2.z = zip1;

	vec3.x = CAST_I2X(i,zjp1); vec3.y= CAST_J2Y(j+1,zjp1); vec3.z = zjp1;
	//ztest = tex2D(tex_filtered_depth, i-i%5,j-j%5);
	//vec3.x = CAST_I2X(i-i%5,ztest); vec3.y= CAST_J2Y(j-j%5,ztest); vec3.z = ztest;

	target_nml.x = (vec2.y - vec1.y)*(vec3.z - vec1.z) - (vec2.z - vec1.z)*(vec3.y - vec1.y);//4* CAST_I2X(i,z) -CAST_I2X(i-1,zim1)-CAST_I2X(i+1,zip1)-CAST_I2X(i,zjp1)-CAST_I2X(i,zjm1);
	target_nml.y = (vec2.z - vec1.z)*(vec3.x - vec1.x) - (vec2.x - vec1.x)*(vec3.z - vec1.z);//4* CAST_J2Y(j,z) - CAST_J2Y(j-1,zjm1) - CAST_J2Y(j+1,zjp1)-CAST_J2Y(j,zip1)-CAST_J2Y(j,zim1);
	target_nml.z = (vec2.x - vec1.x)*(vec3.y - vec1.y) - (vec2.y - vec1.y)*(vec3.x - vec1.x);//4*z - zip1 -zjp1 - zjm1 - zim1;
	rnrm = rsqrt(target_nml.x*target_nml.x + target_nml.y*target_nml.y + target_nml.z*target_nml.z);
	rnrm= (zip1 == 0 ||zjp1==0? 0:rnrm);

	targetNormals[offset] = target_nml.x*rnrm;
	targetNormals[offset + ct_width_height[0] * ct_width_height[1]] = target_nml.y*rnrm;//WIDTH*HEIGHT
	targetNormals[offset + 2 * ct_width_height[0] * ct_width_height[1]] = target_nml.z*rnrm;//WIDTH*HEIGHT

	//testing... "normal" = vector to next point.
//	targetNormals[offset] = (vec3.x -vec1.x)*(zip1 == 0 ||zjp1==0 ||z == 0 ? 0:1);
//	targetNormals[offset+width_height[0]*width_height[1]] = (vec3.y-vec1.y)*(zip1 == 0 ||zjp1==0||z == 0 ? 0:1);
//	targetNormals[offset+2*width_height[0]*width_height[1]] = (vec3.z-vec1.z)*(zip1 == 0 ||zjp1==0||z == 0 ? 0:1);
}

inline __device__ float3 cross_andStuff(float3 vec1, float3 vec2, float3 vec3, float3 c, float scale){
	//computes the cross product of v2-v1 and v3-v1, normalizes it and and adds it to c.
	float3 temp_nml;
	temp_nml.x = ((vec2.y - vec1.y)*(vec3.z - vec1.z) - (vec2.z - vec1.z)*(vec3.y - vec1.y));
	temp_nml.y = ((vec2.z - vec1.z)*(vec3.x - vec1.x) - (vec2.x - vec1.x)*(vec3.z - vec1.z));
	temp_nml.z = ((vec2.x - vec1.x)*(vec3.y - vec1.y) - (vec2.y - vec1.y)*(vec3.x - vec1.x));
	float rnrm = rsqrt(temp_nml.x*temp_nml.x + temp_nml.y*temp_nml.y + temp_nml.z*temp_nml.z) *scale;
	temp_nml.x = temp_nml.x*rnrm + c.x;
	temp_nml.y = temp_nml.y*rnrm + c.y;
	temp_nml.z = temp_nml.z*rnrm + c.z;
	return temp_nml;
}

__global__ void normalKernel2(float *targetNormals){
	//this method again assumes the block dim is widthxheight, and the same for the texture.
	int i = blockIdx.x;
	int j = blockIdx.y;
	int offset = blockIdx.x + blockIdx.y*ct_width_height[0];
	float z, zip1, zjp1, zim1, zjm1, rnrm;
	float3 target_nml = {0,0,0};
	float3 vec1, vec2, vec3;

	z=tex2D(tex_filtered_depth, i,j);
	zip1 = tex2D(tex_filtered_depth, i+1,j);
	zjp1 = tex2D(tex_filtered_depth, i,j+1);
	zim1 = tex2D(tex_filtered_depth, i-1,j);
	zjm1 = tex2D(tex_filtered_depth, i,j-1);

	//position computation
	//is correct.
	//x = CAST_I2X(i,z);
	//y = CAST_J2Y(j,z);
	vec1.x = CAST_I2X(i,z); vec1.y= CAST_J2Y(j,z); vec1.z = z;
	vec2.x = CAST_I2X(i+1,zip1); vec2.y= CAST_J2Y(j,zip1); vec2.z = zip1;
	vec3.x = CAST_I2X(i,zjp1); vec3.y= CAST_J2Y(j+1,zjp1); vec3.z = zjp1;
	target_nml = cross_andStuff(vec2,vec3,vec1,target_nml, (zip1==0 || zjp1 == 0? 0:1));
	
	vec2.x = CAST_I2X(i,zjm1); vec2.y= CAST_J2Y(j-1,zjm1); vec2.z = zjm1;
	vec3.x = CAST_I2X(i+1,zip1); vec3.y= CAST_J2Y(j,zip1); vec3.z = zip1;
	target_nml = cross_andStuff(vec2,vec3,vec1,target_nml, (zjm1==0 || zip1 == 0? 0:1));

	vec2.x = CAST_I2X(i-1,zim1); vec2.y= CAST_J2Y(j,zim1); vec2.z = zim1;
	vec3.x = CAST_I2X(i,zjm1); vec3.y= CAST_J2Y(j-1,zjm1); vec3.z = zjm1;
	target_nml = cross_andStuff(vec2,vec3,vec1,target_nml, (zim1==0 || zjm1 == 0? 0:1));

	vec2.x = CAST_I2X(i,zjp1); vec2.y= CAST_J2Y(j+1,zjp1); vec2.z = zjp1;
	vec3.x = CAST_I2X(i-1,zim1); vec3.y= CAST_J2Y(j,zim1); vec3.z = zim1;
	target_nml = cross_andStuff(vec2,vec3,vec1,target_nml, (zim1==0 || zjp1 == 0? 0:1));

	rnrm = rsqrt(target_nml.x*target_nml.x + target_nml.y*target_nml.y + target_nml.z*target_nml.z);

	targetNormals[offset] = target_nml.x*rnrm;
								//WIDTH*HEIGHT
	targetNormals[offset + ct_width_height[0] * ct_width_height[1]] = target_nml.y*rnrm;
	targetNormals[offset + 2 * ct_width_height[0] * ct_width_height[1]] = target_nml.z*rnrm;
	

}

//vs flying pixels: pixels with too abrupt changes relative to the neighbor are kicked out 
// at each pixel it is checked if the neighbor pixel lies in a cone aligned to the view direction
__global__ void normalKernel3(float *targetNormals, bool checkFlying){//(float * nx, float * ny, float * nz){
	int i = blockIdx.x;
	int j = blockIdx.y;
	int offset = blockIdx.x + blockIdx.y*ct_width_height[0]; //WIDTH
	float z, zip1, zjp1, zim1, zjm1, rnrm;
	float p2_p_dot_p_sqr, norm_p2_p_sqr, norm_p_sqr;
	bool isFlying = false;

	float3 target_nml = {0,0,0};
	float3 vec1, vec2, vec3;

	z=tex2D(tex_filtered_depth, i,j);
	zip1 = tex2D(tex_filtered_depth, i+1,j);
	zjp1 = tex2D(tex_filtered_depth, i,j+1);
	zim1 = tex2D(tex_filtered_depth, i-1,j);
	zjm1 = tex2D(tex_filtered_depth, i,j-1);

	//position computation
	//is correct.
	//x = CAST_I2X(i,z);
	//y = CAST_J2Y(j,z);
	vec1.x = CAST_I2X(i,z); vec1.y= CAST_J2Y(j,z); vec1.z = z;
	vec2.x = CAST_I2X(i+1,zip1); vec2.y= CAST_J2Y(j,zip1); vec2.z = zip1;
	vec3.x = CAST_I2X(i,zjp1); vec3.y= CAST_J2Y(j+1,zjp1); vec3.z = zjp1;
	target_nml = cross_andStuff(vec2,vec3,vec1,target_nml, (zip1==0 || zjp1 == 0? 0:1));

	//flying?
	p2_p_dot_p_sqr = (vec2.x -vec1.x)*vec1.x + (vec2.y -vec1.y)*vec1.y + (vec2.z -vec1.z)*vec1.z;
	p2_p_dot_p_sqr *=p2_p_dot_p_sqr;
	norm_p2_p_sqr = (vec2.x -vec1.x)*(vec2.x -vec1.x) + (vec2.y -vec1.y)*(vec2.y -vec1.y) + (vec2.z -vec1.z)*(vec2.z -vec1.z);	
	norm_p_sqr = vec1.x*vec1.x + vec1.y*vec1.y + vec1.z*vec1.z;
	isFlying = (p2_p_dot_p_sqr > norm_p2_p_sqr*norm_p_sqr*FLYING_CONE ||isFlying);

	vec2.x = CAST_I2X(i,zjm1); vec2.y= CAST_J2Y(j-1,zjm1); vec2.z = zjm1;
	vec3.x = CAST_I2X(i+1,zip1); vec3.y= CAST_J2Y(j,zip1); vec3.z = zip1;
	target_nml = cross_andStuff(vec2,vec3,vec1,target_nml, (zjm1==0 || zip1 == 0? 0:1));

	//flying?
	p2_p_dot_p_sqr = (vec2.x -vec1.x)*vec1.x + (vec2.y -vec1.y)*vec1.y + (vec2.z -vec1.z)*vec1.z;
	p2_p_dot_p_sqr *=p2_p_dot_p_sqr;
	norm_p2_p_sqr = (vec2.x -vec1.x)*(vec2.x -vec1.x) + (vec2.y -vec1.y)*(vec2.y -vec1.y) + (vec2.z -vec1.z)*(vec2.z -vec1.z);	
	norm_p_sqr = vec1.x*vec1.x + vec1.y*vec1.y + vec1.z*vec1.z;
	isFlying = (p2_p_dot_p_sqr > norm_p2_p_sqr*norm_p_sqr*FLYING_CONE ||isFlying);

	vec2.x = CAST_I2X(i-1,zim1); vec2.y= CAST_J2Y(j,zim1); vec2.z = zim1;
	vec3.x = CAST_I2X(i,zjm1); vec3.y= CAST_J2Y(j-1,zjm1); vec3.z = zjm1;
	target_nml = cross_andStuff(vec2,vec3,vec1,target_nml, (zim1==0 || zjm1 == 0? 0:1));

	//flying?
	p2_p_dot_p_sqr = (vec2.x -vec1.x)*vec1.x + (vec2.y -vec1.y)*vec1.y + (vec2.z -vec1.z)*vec1.z;
	p2_p_dot_p_sqr *=p2_p_dot_p_sqr;
	norm_p2_p_sqr = (vec2.x -vec1.x)*(vec2.x -vec1.x) + (vec2.y -vec1.y)*(vec2.y -vec1.y) + (vec2.z -vec1.z)*(vec2.z -vec1.z);	
	norm_p_sqr = vec1.x*vec1.x + vec1.y*vec1.y + vec1.z*vec1.z;
	isFlying = (p2_p_dot_p_sqr > norm_p2_p_sqr*norm_p_sqr*FLYING_CONE ||isFlying);

	vec2.x = CAST_I2X(i,zjp1); vec2.y= CAST_J2Y(j+1,zjp1); vec2.z = zjp1;
	vec3.x = CAST_I2X(i-1,zim1); vec3.y= CAST_J2Y(j,zim1); vec3.z = zim1;
	target_nml = cross_andStuff(vec2,vec3,vec1,target_nml, (zim1==0 || zjp1 == 0? 0:1));

	//flying?
	p2_p_dot_p_sqr = (vec2.x -vec1.x)*vec1.x + (vec2.y -vec1.y)*vec1.y + (vec2.z -vec1.z)*vec1.z;
	p2_p_dot_p_sqr *=p2_p_dot_p_sqr;
	norm_p2_p_sqr = (vec2.x -vec1.x)*(vec2.x -vec1.x) + (vec2.y -vec1.y)*(vec2.y -vec1.y) + (vec2.z -vec1.z)*(vec2.z -vec1.z);	
	norm_p_sqr = vec1.x*vec1.x + vec1.y*vec1.y + vec1.z*vec1.z;
	isFlying = (p2_p_dot_p_sqr > norm_p2_p_sqr*norm_p_sqr*FLYING_CONE ||isFlying);

	rnrm = rsqrt(target_nml.x*target_nml.x + target_nml.y*target_nml.y + target_nml.z*target_nml.z);

	//flying
	rnrm = (isFlying &&checkFlying ? 0 : rnrm);

	targetNormals[offset] = target_nml.x*rnrm;
	targetNormals[offset + ct_width_height[0] * ct_width_height[1]] = target_nml.y*rnrm;
	targetNormals[offset + 2 * ct_width_height[0] * ct_width_height[1]] = target_nml.z*rnrm;
	

}

__device__ float bilatWeight(int delta_i, int delta_j, float delta_z){
	return exp_x2[abs(10*delta_i/SIGMA_S)]*
			exp_x2[abs(10*delta_j/SIGMA_S)]*
			exp_x2[__float2int_rz(10*abs(delta_z)/SIGMA_D)];
}

__device__ float bilatWeight(int delta_i, int delta_j, float delta_z, float z){
	return exp_x2[abs(10*delta_i/SIGMA_S)]*
			exp_x2[abs(10*delta_j/SIGMA_S)]*
			exp_x2[__float2int_rz(10*abs(delta_z/z)/SIGMA_D)];
}


__device__ float bilatWeight(int delta_i, int delta_j, float delta_z, float z, float sig_s, float sig_d){
	return exp_x2[__float2int_rz(abs(10*delta_i/sig_s))]*
			exp_x2[__float2int_rz(abs(10*delta_j/sig_s))]*
			//exp_x2[__float2int_rz(abs(10*delta_z)/sig_d)];
			exp_x2[__float2int_rz(abs(10*delta_z/(z+0.5))/sig_d)];
}

//bilateral kernel with redux on the tex_in texture, stored in target.
//The Redux really is not worth it for such small kernels.
__global__ void bilatKernelRedux(float * target){
	__shared__ float cache[NUM_THREADS];
	__shared__ float weights[NUM_THREADS];

	int x = blockIdx.x;
	int y = blockIdx.y;
	int x_p_i = blockIdx.x + (threadIdx.x - KERNEL_WIDTH_2);
	int y_p_j = blockIdx.y + (threadIdx.y - KERNEL_WIDTH_2);
	int offset = blockIdx.x + blockIdx.y*ct_width_height[0];//WIDTH.
	int cache_offset = (KERNEL_WIDTH_2*2+1)*threadIdx.x + threadIdx.y;
	//will be weighted with bilat weight.
	cache[cache_offset] = tex2D(ct_tex_in, x_p_i, y_p_j);
	weights[cache_offset] = 
		bilatWeight((threadIdx.x - KERNEL_WIDTH_2),
			(threadIdx.y - KERNEL_WIDTH_2),
			tex2D(ct_tex_in, x_p_i, y_p_j) - tex2D(ct_tex_in, x, y));

	//redux.
	__syncthreads();
	for(int i = 1; i < NUM_THREADS; i*=2){
		int index = 2*i*cache_offset;
		if(index +i <NUM_THREADS){
			cache[index] += cache[index +i]; 
			weights[index] += weights[index +i]; 
		}
		__syncthreads();
	}

	if(cache_offset ==0){
		target[offset] = cache[0]/ weights[0];
	}
}

//trivial bilateral kernel
__global__ void bilatKernel(float *target){
	int x = blockIdx.x;
	int y = blockIdx.y;

	int offset = blockIdx.x + blockIdx.y * ct_width_height[0];//WIDTH;
	float total=0;
	float weight=0;
	float w;
	float val_xy = tex2D(ct_tex_in, x, y), val_ij;

	//bilateral filtering...
	for(int i = -KERNEL_WIDTH_2; i <= KERNEL_WIDTH_2; i++){
		for(int j = -KERNEL_WIDTH_2; j <= KERNEL_WIDTH_2; j++){
			val_ij = tex2D(ct_tex_in, x + i, y + j);
			w= bilatWeight(i,j, val_xy - val_ij);
			w = (val_ij == 0? 0:w);
			total += val_ij*w;
			weight +=w;

		}
	}
	total/=weight;
	target[offset] = total;
}

//bilateral kernel with one paralelized for loop
//good tradeoff.
__global__ void bilatKernel2(float *target, float sig_s, float sig_d){
	__shared__ float cache[2*KERNEL_WIDTH_2 + 1];
	__shared__ float weight[2*KERNEL_WIDTH_2 + 1];

	int x = blockIdx.x;
	int y = blockIdx.y;
	int tid = threadIdx.x;
	int j = tid - KERNEL_WIDTH_2;
	int offset = blockIdx.x + blockIdx.y*ct_width_height[0];//WIDTH;
	float total=0;
	float total_weight=0, w;
	float val_ij, val_xy, min_ij_xy;

	
	val_xy = tex2D(ct_tex_in, x, y);
	for(int i = -KERNEL_WIDTH_2; i <= KERNEL_WIDTH_2; i++){
		val_ij = tex2D(ct_tex_in, x + i, y + j);
		/*w = exp_x2[abs(10*i/SIGMA_S)]*
			exp_x2[abs(10*j/SIGMA_S)]*
			exp_x2[__float2int_rz(10*abs(val_ij - val_xy)/SIGMA_D)];*/
		min_ij_xy = (val_xy < val_ij ? val_xy : val_ij );
		//w= bilatWeight(i,j,val_ij-val_xy, val_xy);
		w= bilatWeight(i,j,val_ij-val_xy, min_ij_xy, sig_s, sig_d);
		w = ((val_ij<0.1 || min_ij_xy <0.1) ? 0: w);
		total += val_ij*w;
		total_weight += w;
	}
	cache[tid] = total;
	weight[tid] = total_weight;
	__syncthreads();

	total = 0;
	total_weight = 0;
	if(tid == 0){
		for(int i = 0; i < KERNEL_WIDTH_2*2+1; i++){
			total += cache[i];
			total_weight += weight[i];
		}
		target[offset] = total/total_weight;
		target[offset] = (val_xy == 0 || total_weight < 2 ? val_xy : target[offset]);
	}
}

cudaTool::cudaTool(sensorDescriptionData sd)
{
	sensor_desc = sd;
	//example = new float[WIDTH*HEIGHT];
	example = new float[sd._width * sd._height];
	
	//choose cuda device...
	printCudaDeviceStats();
	selectCudaDevice();

	//alloc cuda memory...
	HANDLE_ERROR(cudaMalloc((void **) & dev_inSrc, sd._width*sd._height*sizeof(float)));
	HANDLE_ERROR(cudaMalloc((void **) & dev_outSrc, sd._width*sd._height*sizeof(float)));
	HANDLE_ERROR(cudaMalloc((void **) & dev_normals, 3*sd._width*sd._height*sizeof(float)));

	//bind to texture...
	//pitch needs to be a multiple of 256 ....
	cudaChannelFormatDesc desc = cudaCreateChannelDesc<float>();
	HANDLE_ERROR(cudaBindTexture2D(NULL, ct_tex_in, dev_inSrc, desc, sd._width, sd._height, sd._width*sizeof(float)));
	HANDLE_ERROR(cudaBindTexture2D(NULL, tex_filtered_depth, dev_outSrc,desc, sd._width, sd._height, sd._width*sizeof(float)));
	//precompute gaussian kernel.
	float expf[64];
	for(int i = 0; i < 64; i++){
		expf[i] = exp(-(i*0.1)*(i*0.1));
	}
	//and set it.
	HANDLE_ERROR(cudaMemcpyToSymbol(exp_x2,expf, sizeof(float)*64));

	//finally set the sensor dependent unproject matrix
	HANDLE_ERROR(cudaMemcpyToSymbol(unproject,sd._unproject, sizeof(float)*16));
	//and the width
	int cpu_wh[2] = {sd._width,sd._height};
	HANDLE_ERROR(cudaMemcpyToSymbol(ct_width_height, cpu_wh, sizeof(int) * 2));
}


cudaTool::~cudaTool(void)
{
	std::cout << "would exit cuda";
	delete[] example;

	//unbind cuda texture
	cudaUnbindTexture(ct_tex_in);
	//free cuda memory
	cudaFree(dev_inSrc);
	cudaFree(dev_outSrc);
	cudaFree(dev_normals);

}

void cudaTool::printCudaDeviceStats(){
	cudaDeviceProp prop;
	int count;

	//list devices...
	HANDLE_ERROR(cudaGetDeviceCount(&count));
	//std::cout << "There are " << count << " cuda devices \n";
	for(int i = 0; i < count; i++){
		cudaGetDeviceProperties(&prop,i);
	/*	std::cout << prop.name << " \n";
		std::cout << prop.multiProcessorCount << " cuda multi processors\n";
		std::cout << "max Grid dim: " <<prop.maxGridSize[0] << ","
			<<prop.maxGridSize[1] << ","
			<<prop.maxGridSize[2] << "\n";
		std::cout << "Threads: " << prop.maxThreadsDim[0] << ", " 
			<< prop.maxThreadsDim[1] << ", " 
			<< prop.maxThreadsDim[2] << "\n";
			*/
	}
}

void cudaTool::selectCudaDevice(){
	cudaDeviceProp prop;
	memset(&prop,0,sizeof(cudaDeviceProp));
	prop.major = 1;
	prop.minor = 3;
	HANDLE_ERROR(cudaChooseDevice(&dev, &prop));
	//std::cout << "choosing device number" << dev;
	HANDLE_ERROR(cudaSetDevice(dev));
}

//set dev_inSrc
void cudaTool::dev_setData(float * data)
{
											 //HEIGHT * WIDTH*
	HANDLE_ERROR(cudaMemcpy(dev_inSrc, data, sensor_desc._height * sensor_desc._width * sizeof(float),cudaMemcpyHostToDevice));
}

//copy from dev_outSrc
void cudaTool::dev_getData(float * data)
{
												//HEIGHT * WIDTH
	HANDLE_ERROR(cudaMemcpy(data, dev_outSrc, sensor_desc._height * sensor_desc._width*sizeof(float),cudaMemcpyDeviceToHost));
	
}


void cudaTool::dev_getNormals( float* data )
{
	HANDLE_ERROR(cudaMemcpy(data, dev_normals, 3*sensor_desc._height * sensor_desc._width*sizeof(float),cudaMemcpyDeviceToHost));
}


void cudaTool::bilateralFilter(float sig_s, float sig_d)
{
	dim3 blocks(sensor_desc._width, sensor_desc._height);//(WIDTH,HEIGHT);
	dim3 threads(KERNEL_WIDTH_2*2+1,KERNEL_WIDTH_2*2+1);
	//bilatKernel<<<blocks,1>>>(dev_outSrc);
	//bilatKernel2<<<blocks,KERNEL_WIDTH_2*2+1>>>(dev_outSrc);
	bilatKernel2<<<blocks,KERNEL_WIDTH_2*2+1>>>(dev_outSrc, sig_s, sig_d);
	//inOutTest<<<blocks,1>>>(dev_outSrc);
	//bilatKernelRedux<<<blocks,threads>>>(dev_outSrc);
}

void cudaTool::skipFiltering(){
	dim3 blocks(sensor_desc._width, sensor_desc._height);
	inOutTest<<<blocks,1>>>(dev_outSrc);
}

void cudaTool::computeNormals(bool checkFlying)
{
	dim3 blocks(sensor_desc._width, sensor_desc._height);
	dim3 threads(KERNEL_WIDTH_2*2+1,KERNEL_WIDTH_2*2+1);
	//bilatKernel<<<blocks,1>>>(dev_outSrc);
	//normalKernel<<<blocks,1>>>(dev_normals);
	//normalKernel2<<<blocks,1>>>(dev_normals);
	normalKernel3<<<blocks,1>>>(dev_normals, checkFlying);
	//inOutTest<<<blocks,1>>>(dev_outSrc);
	//bilatKernelRedux<<<blocks,threads>>>(dev_outSrc);
}

