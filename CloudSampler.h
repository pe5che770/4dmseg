#pragma once

#include "explainMeDefinitions.h"
#include "trajectory.h"

#undef max
#undef min
#ifndef Q_MOC_RUN
//for more keypoint detectors: newer pcl version.
#include <pcl/keypoints/harris_3d.h>
//supposedly much better:
#include <pcl/keypoints/iss_3d.h>
#endif

class CloudSampler
{
public:
	CloudSampler(void){}
	virtual ~CloudSampler(void){}

	virtual void sample(
			//input
			int n_per_frame,
			std::vector<explainMe::Cloud::Ptr> & allClouds,
			std::vector<explainMe::MatrixXf_rm> & denseSeg,
			//output
			std::vector<trajectory_R> & trajectories, 
			std::vector<pcl::PointXYZINormal> &seed_points, 
			std::vector<Eigen::Vector3f> &seed_normals,
			std::vector<int> & seed_cloud)  = 0;

	//note: the problem is that samples must in principle not
	//come from a cloud; if a feature is used, the points positions
	//might not correspond to any observed point.
	//Also sample positions could also be denoised differently,
	//In these cases -1 is returned as seedpointindex, as implemented here by default.
	virtual void sample(
		//input
		int n_per_frame,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		//output
		std::vector<trajectory_R> & trajectories,
		std::vector<pcl::PointXYZINormal> &seed_points,
		std::vector<Eigen::Vector3f> &seed_normals,
		std::vector<int> & seed_frame,
		std::vector<int> & seed_point_index)
	{
		sample(
			//input
			n_per_frame,
			allClouds,
			denseSeg,
			//output
			trajectories,
			seed_points,
			seed_normals,
			seed_frame);
		seed_point_index.resize(seed_frame.size(), -1);
		std::cout << "ERROR: sample with seed_point_index not implemented!"; exit(1);
	}
};


class UseOldSamples:public CloudSampler
{
public: 
	UseOldSamples(){}
	virtual ~ UseOldSamples(){}

	virtual void sample(
			//input
			int n_per_frame,
			std::vector<explainMe::Cloud::Ptr> & allClouds,
			std::vector<explainMe::MatrixXf_rm> & denseSeg,
			//output
			std::vector<trajectory_R> & trajectories, 
			std::vector<pcl::PointXYZINormal> &seed_points, 
			std::vector<Eigen::Vector3f> &seed_normals,
			std::vector<int> & seed_cloud)
	{
		//trajectories.clear();
		seed_points.resize(trajectories.size());
		seed_normals.resize(trajectories.size());
		seed_cloud.resize(trajectories.size());
		for(int i = 0; i < trajectories.size(); i++){
			trajectories[i] = trajectory_R(trajectories[i].seedEl(), trajectories[i].seedCloud(), trajectories[i].originalPointIdx());
			seed_points[i] = trajectories[i].seedEl();
			seed_normals[i] = trajectories[i].seedEl().getNormalVector3fMap();
			seed_cloud[i] = trajectories[i].seedCloud();
		}
	}

	virtual void sample(
		//input
		int n_per_frame,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		//output
		std::vector<trajectory_R> & trajectories,
		std::vector<pcl::PointXYZINormal> &seed_points,
		std::vector<Eigen::Vector3f> &seed_normals,
		std::vector<int> & seed_frame,
		std::vector<int> & seed_point_index)
	{
		seed_points.resize(trajectories.size());
		seed_normals.resize(trajectories.size());
		seed_frame.resize(trajectories.size());
		seed_point_index.resize(trajectories.size());
		for (int i = 0; i < trajectories.size(); i++){
			trajectories[i] = trajectory_R(trajectories[i].seedEl(), trajectories[i].seedCloud(), trajectories[i].originalPointIdx());
			seed_points[i] = trajectories[i].seedEl();
			seed_normals[i] = trajectories[i].seedEl().getNormalVector3fMap();
			seed_frame[i] = trajectories[i].seedCloud();
			seed_point_index[i] = trajectories[i].originalPointIdx();
		}
	}
};

class RandomSampler:public CloudSampler
{
public: 
	RandomSampler(){}
	virtual ~ RandomSampler(){}

	virtual void sample(
			//input
			int n_per_frame,
			std::vector<explainMe::Cloud::Ptr> & allClouds,
			std::vector<explainMe::MatrixXf_rm> & denseSeg,
			//output
			std::vector<trajectory_R> & trajectories, 
			std::vector<pcl::PointXYZINormal> &seed_points, 
			std::vector<Eigen::Vector3f> &seed_normals,
			std::vector<int> & seed_cloud)
	{
		trajectories.clear();
		for (int cloudIdx = 0; cloudIdx < allClouds.size(); cloudIdx++){
			for(int i = 0; i < n_per_frame; i++){//trajectories.size(); i++){
				//int frame_idx = i % allClouds.size();
				int rand_int = (rand()*RAND_MAX + rand()) % allClouds[cloudIdx]->size();
			
				auto & p_cloud = allClouds[cloudIdx]->at(rand_int);
				pcl::PointXYZINormal p;
				p.getVector4fMap() = p_cloud.getVector4fMap();
				p.getNormalVector4fMap() = p_cloud.getNormalVector4fMap();

				seed_points.push_back(p);
				seed_cloud.push_back(cloudIdx);
				seed_normals.push_back(p.getNormalVector3fMap());

				trajectories.push_back(trajectory_R(p, cloudIdx, rand_int));
			}
		}
	}

	void sample(
		//input
		int n_per_frame,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		//output
		std::vector<trajectory_R> & trajectories,
		std::vector<pcl::PointXYZINormal> &seed_points,
		std::vector<Eigen::Vector3f> &seed_normals,
		std::vector<int> & seed_frame,
		explainMe::MatrixXf_rm & sampleWeights)
	{
		sampleWeights.resize(n_per_frame*allClouds.size(), denseSeg[0].cols());
		trajectories.clear();
		int samplePointIndex = 0;
		for (int frame_idx = 0; frame_idx < allClouds.size(); frame_idx++){
			for (int i = 0; i < n_per_frame; i++){//trajectories.size(); i++){
				//int frame_idx = i % allClouds.size();
				int rand_int = (rand()*RAND_MAX + rand()) % allClouds[frame_idx]->size();

				auto & p_cloud = allClouds[frame_idx]->at(rand_int);
				pcl::PointXYZINormal p;
				p.getVector4fMap() = p_cloud.getVector4fMap();
				p.getNormalVector4fMap() = p_cloud.getNormalVector4fMap();

				seed_points.push_back(p);
				seed_frame.push_back(frame_idx );
				seed_normals.push_back(p.getNormalVector3fMap());

				trajectories.push_back(trajectory_R(p, frame_idx, rand_int));
				sampleWeights.row(samplePointIndex) = denseSeg[frame_idx].row(rand_int);

				samplePointIndex++;
			}
		}
	}

	void sample(
		//input
		int n_per_frame,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		//output
		std::vector<trajectory_R> & trajectories,
		std::vector<pcl::PointXYZINormal> &seed_points,
		std::vector<Eigen::Vector3f> &seed_normals,
		std::vector<int> & seed_frame,
		std::vector<int> & seed_point_index)
	{
		trajectories.clear();
		for (int frame_idx = 0; frame_idx < allClouds.size(); frame_idx++){
			for (int i = 0; i < n_per_frame; i++){//trajectories.size(); i++){
				//int frame_idx = i % allClouds.size();
				int rand_int = (rand()*RAND_MAX + rand()) % allClouds[frame_idx]->size();

				auto & p_cloud = allClouds[frame_idx]->at(rand_int);
				pcl::PointXYZINormal p;
				p.getVector4fMap() = p_cloud.getVector4fMap();
				p.getNormalVector4fMap() = p_cloud.getNormalVector4fMap();

				seed_points.push_back(p);
				seed_frame.push_back(frame_idx);
				seed_normals.push_back(p.getNormalVector3fMap());
				seed_point_index.push_back(rand_int);

				trajectories.push_back(trajectory_R(p, frame_idx, rand_int));
			}
		}
	}
};



//should also be quite good
//#include <pcl/keypoints/sift_keypoint.h>
/*class HarrisSampler: public CloudSampler
{
public:
	HarrisSampler(){}
	virtual ~HarrisSampler(){}

	virtual void sample(
			//input
			int n_per_frame,
			std::vector<explainMe::Cloud::Ptr> & allClouds,
			std::vector<explainMe::MatrixXf_rm> & denseSeg,
			int central_cloud,
			//output
			std::vector<trajectory> & trajectories, 
			std::vector<pcl::PointXYZINormal> &seed_points, 
			std::vector<Eigen::Vector3f> &seed_normals,
			std::vector<int> & seed_frame)
	{

		trajectories.clear();
		seed_points.clear();
		seed_normals.clear();
		seed_frame.clear();

		//maybe can do a multiresolution harris?

		pcl::HarrisKeypoint3D<pcl::PointXYZINormal,pcl::PointXYZINormal,pcl::PointXYZINormal> detector; 
		
		//Overall: quite sensitive to noise, will put a lot more features in noisy areas.

		//detector.setRadius (0.25); 
		//detector.setRadius (0.05); 
		//the larger the radius, the leass points sotps changing (on kinectv2= at 0.2) best around 0.03. 
		detector.setRadius (0.03);
		
		//detector.setKSearch(200); //ksearx does just not work. buggy
		detector.setNonMaxSupression (true); 
		//detector.setNonMaxSupression (true); 
		detector.setThreshold(0.0001);// the smaller, the more keypoints. for LOWE 0.1 or some magnitudes smaller
		detector.setRefine(false); //refine true leads to strange far away points definitely buggy.

		//various alternatives..
		//HARRIS can be run as harris, noble, lowe, curvature or tomasi.
		//curvature not working.
		//LOWE works, NOBLE too, HARRIS as well
		detector.setMethod(detector.LOWE); 

		//stupid but well, solve this later. Harris needs the intensity field.
		pcl::PointCloud<pcl::PointXYZINormal>::Ptr cld(new pcl::PointCloud<pcl::PointXYZINormal>());
		//pcl::PointCloud<pcl::Normal>::Ptr nml(new pcl::PointCloud<pcl::Normal>());
		
		
		pcl::PointCloud<pcl::PointXYZINormal>::Ptr keypoints(new pcl::PointCloud<pcl::PointXYZINormal>()); 
		pcl::PointXYZINormal p;
		for(int i = 0; i < allClouds.size(); i++){
			cld->resize(allClouds[i]->size());
			//nml->resize(allClouds[i]->size());
			for(int j = 0; j < allClouds[i]->size(); j++){
				p.getVector3fMap() << allClouds[i]->at(j).getVector3fMap();
				p.getNormalVector3fMap() << allClouds[i]->at(j).getNormalVector3fMap();
				cld->at(j) = (p);
				//nml->at(j).getNormalVector3fMap() << allClouds[i]->at(j).getNormalVector3fMap();
			}

			//float thr =  std::pow(10,i *6.0/allClouds.size()-3);
			//std::cout << "threshold: "  << thr ;
			//detector.setThreshold(thr);
			float r =  i *0.2/allClouds.size();
			//int k = 1000 *i *1.0/allClouds.size() + 1;
			//std::cout << "r: "  << r ;
			//std::cout << "k: "  << k ;
			//detector.setRadius (r);
						
			detector.setInputCloud(cld); 
			detector.setNormals(cld);
			std::cout << "computing key points...";
			detector.compute(*keypoints); 
			std::cout << "keypoints detected: " << keypoints->size() << std::endl; 

			
			for(int j = 0; j <keypoints->size(); j++){
				trajectories.push_back(trajectory(keypoints->at(j),i-central_cloud));
				seed_points.push_back(keypoints->at(j));
				seed_frame.push_back(i-central_cloud);
				seed_normals.push_back(keypoints->at(j).getNormalVector3fMap());
			}
			//hack for vis.
			//allClouds[i]->resize(keypoints->size());
			//for(int j = 0; j < allClouds[i]->size(); j++){
			//	allClouds[i]->at(j).getVector3fMap() = keypoints->at(j).getVector3fMap();
			//}
		}

		
		
	}
};*/
/*
class ISS3dSampler: public CloudSampler
{
public:
	ISS3dSampler(){}
	virtual ~ISS3dSampler(){}

	virtual void sample(
			//input
			int n_per_frame,
			std::vector<explainMe::Cloud::Ptr> & allClouds,
			std::vector<explainMe::MatrixXf_rm> & denseSeg,
			int central_cloud,
			//output
			std::vector<trajectory> & trajectories, 
			std::vector<pcl::PointXYZINormal> &seed_points, 
			std::vector<Eigen::Vector3f> &seed_normals,
			std::vector<int> & seed_frame)
	{
		pcl::ISSKeypoint3D<explainMe::Point, explainMe::Point, explainMe::Point> detector;

		std::cout << "computing key points...";
		pcl::PointCloud<explainMe::Point>::Ptr keypoints(new pcl::PointCloud<explainMe::Point>()); 
		

		
		float iss_salient_radius_ = 0.1;//0.1; //controls quality 
		float iss_non_max_radius_ = 0.33 *iss_salient_radius_; //0.06 //redundancy and number of features
		double iss_gamma_21_ (0.95); //.975;
		double iss_gamma_32_ (0.95); //not a huge influence
		double iss_min_neighbors_ (25); //useful against noise
		int iss_threads_ (24);
		//
		// Compute keypoints
		//
				
		
		detector.setSalientRadius (iss_salient_radius_);
		detector.setNonMaxRadius (iss_non_max_radius_);
		//detector.setThreshold21 (iss_gamma_21_);
		//detector.setThreshold32 (iss_gamma_32_);
		detector.setMinNeighbors (iss_min_neighbors_);
		detector.setNumberOfThreads (iss_threads_);
		
		trajectories.clear();
		trajectories.reserve(1000*allClouds.size());
		for(int i = 0; i < allClouds.size(); i++){
			pcl::search::KdTree<explainMe::Point>::Ptr tree (new pcl::search::KdTree<explainMe::Point> ());
			tree->setInputCloud(allClouds[i]);
			detector.setSearchMethod (tree);
			detector.setInputCloud (allClouds[i]);
			detector.compute (*keypoints);
			//hacked in vis.
			//allClouds[i]->resize(keypoints->size());
			//*allClouds[i] = *keypoints;
			//std::cout << "keypoints detected: " << keypoints->size() << std::endl; 
			
			pcl::PointXYZINormal p;
			for(int j = 0; j < keypoints->size(); j++){
				p.getVector3fMap() = keypoints->at(j).getVector3fMap();
				p.getNormalVector3fMap() = keypoints->at(j).getNormalVector3fMap();
				seed_points.push_back(p);
				seed_frame.push_back(i- central_cloud);
				seed_normals.push_back(keypoints->at(j).getNormalVector3fMap());

				trajectories.push_back( trajectory(p, i - central_cloud));
			}//
		}



	}
};*/

class RandomPerLabelSampler: public CloudSampler
{
public:
	RandomPerLabelSampler() {}
	virtual  ~RandomPerLabelSampler(){}
	virtual void sample(//input
			int n_per_frame,
			std::vector<explainMe::Cloud::Ptr> & allClouds,
			std::vector<explainMe::MatrixXf_rm> & denseSeg,
			//output
			std::vector<trajectory_R> & trajectories, 
			std::vector<pcl::PointXYZINormal> &seed_points, 
			std::vector<Eigen::Vector3f> &seed_normals,
			std::vector<int> & seed_frame)
	{

		trajectories.clear();
		seed_points.clear();
		seed_normals.clear();
		seed_frame.clear();
		
		std::cout << "Resampling, proportional to the weights";
		pcl::PointXYZINormal p;
		for(int cl = 0; cl < allClouds.size(); cl++)
		{
			explainMe::MatrixXf_rm & w = denseSeg[cl]; //dense weights of the current cloud.
			explainMe::RowVectorXf w_total = w.colwise().sum();
			int hyps = w.cols();
			int nPerHyp = n_per_frame/hyps;
			for(int hyp = 0; hyp < hyps; hyp++){
				for(int i = 0; i < allClouds[cl]->size(); i++){
					float total = w_total(hyp);
					float rand_w = (rand()*RAND_MAX + rand()) * 1.0/ (static_cast<float>(RAND_MAX)*RAND_MAX);
					if(rand_w < w(i,hyp) * nPerHyp / total){
						if(allClouds[cl]->at(i).z > 0 && allClouds[cl]->at(i).z*0 == 0 && 
							allClouds[cl]->at(i).getNormalVector3fMap().sum() * 0 == 0)
						{
							p.getVector3fMap() = allClouds[cl]->at(i).getVector3fMap();
							p.getNormalVector3fMap() = allClouds[cl]->at(i).getNormalVector3fMap();
							trajectories.push_back(trajectory_R(p, cl,i));
							seed_points.push_back(p);
							seed_normals.push_back(p.getNormalVector3fMap());
							seed_frame.push_back(cl);
						}
					}
				}
			}
		}
		std::cout << "resampling done.\n";
	}
};

class RandomLabelSampler : public CloudSampler
{
private:
	int label;
public:
	RandomLabelSampler(int label_to_sample) { label = label_to_sample; }
	virtual  ~RandomLabelSampler(){}
	virtual void sample(//input
		int n_per_frame,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		//int central_cloud,
		//output
		std::vector<trajectory_R> & trajectories,
		std::vector<pcl::PointXYZINormal> &seed_points,
		std::vector<Eigen::Vector3f> &seed_normals,
		std::vector<int> & seed_frame)
	{

		trajectories.clear();
		seed_points.clear();
		seed_normals.clear();
		seed_frame.clear();

		std::cout << "Resampling, proportional to the weights";
		pcl::PointXYZINormal p;
		for (int cl = 0; cl < allClouds.size(); cl++)
		{
			explainMe::MatrixXf_rm & w = denseSeg[cl]; //dense weights of the current cloud.
			explainMe::RowVectorXf w_total = w.colwise().sum();
			for (int i = 0; i < allClouds[cl]->size(); i++){
				float total = w_total(label);
				float rand_w = (rand()*RAND_MAX + rand()) * 1.0f / (static_cast<float>(RAND_MAX)*static_cast<float>(RAND_MAX));
				if (rand_w < w(i, label) * n_per_frame / total){
					if (allClouds[cl]->at(i).z > 0 && allClouds[cl]->at(i).z * 0 == 0 &&
						allClouds[cl]->at(i).getNormalVector3fMap().sum() * 0 == 0)
					{
						p.getVector3fMap() = allClouds[cl]->at(i).getVector3fMap();
						p.getNormalVector3fMap() = allClouds[cl]->at(i).getNormalVector3fMap();
						trajectories.push_back(trajectory_R(p, cl,i));
						seed_points.push_back(p);
						seed_normals.push_back(p.getNormalVector3fMap());
						seed_frame.push_back(cl);
					}
				}
			}
			
		}
		std::cout << "resampling done.\n";
	}

	virtual void sample(//input
		int n_per_frame,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		//output
		std::vector<pcl::PointXYZINormal> &seed_points,
		std::vector<Eigen::Vector3f> &seed_normals,
		std::vector<int> & seed_frame)
	{
		seed_points.clear();
		seed_normals.clear();
		seed_frame.clear();

		pcl::PointXYZINormal p;
		for (int cl = 0; cl < allClouds.size(); cl++)
		{
			explainMe::MatrixXf_rm & w = denseSeg[cl]; //dense weights of the current cloud.
			explainMe::RowVectorXf w_total = w.colwise().sum();
			for (int i = 0; i < allClouds[cl]->size(); i++){
				int l; w.row(i).maxCoeff(&l);
				if (l != label){
					continue;
				}
				float total = w_total(label);
				float rand_w = (rand()*RAND_MAX + rand()) * 1.0 / (static_cast<float>(RAND_MAX)*RAND_MAX);
				if (rand_w < w(i, label) * n_per_frame / total ){
					if (allClouds[cl]->at(i).z > 0 && allClouds[cl]->at(i).z * 0 == 0 &&
						allClouds[cl]->at(i).getNormalVector3fMap().sum() * 0 == 0)
					{
						p.getVector3fMap() = allClouds[cl]->at(i).getVector3fMap();
						p.getNormalVector3fMap() = allClouds[cl]->at(i).getNormalVector3fMap();
						seed_points.push_back(p);
						seed_normals.push_back(p.getNormalVector3fMap());
						seed_frame.push_back(cl);
					}
				}
			}

		}
	}
};