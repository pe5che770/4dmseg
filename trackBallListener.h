#pragma once
//#include "gl3w.h"
#include <QtOpenGL/QGLWidget>
#include <QMouseEvent>
#include "glDisplayable.h"
#include <QObject>
#include <QMatrix4x4>

class sceneManager;

class trackBallListener: public QObject
{
	Q_OBJECT

public:
	trackBallListener(QGLWidget * display, sceneManager & manager);
	~trackBallListener(void);

	void onMouseMove(QMouseEvent* event);
	void onMousePress(QMouseEvent* event );
	void onMouseWheel(QWheelEvent * event);

Q_SIGNALS:
	void rotation(QMatrix4x4 & mat);
	void camMovement(QMatrix4x4 & mat);

private:
	sceneManager & scene;
	QGLWidget * displyer;
	float lastx, lasty, lastz;
	float z_translation;
};
