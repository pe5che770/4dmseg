#pragma once
#include "explainMeDefinitions.h"
#include "explainMe_algorithm_withFeatures.h"
#include "ITMLib.h"

class RangeSensorDescription;
class ITMLibObjects;

class tsdfTracking{
private:
	ITMLibObjects * itmlib;
public:
	tsdfTracking(float maxDepth, RangeSensorDescription * d, float voxelSize = 0.005f);
	~tsdfTracking();

	void doFusion(std::vector<explainMe::Cloud::Ptr> & clouds, std::vector<Eigen::Matrix4f> & alignments, std::string outFileSTL);
	void doFusion(std::vector<explainMe::Cloud::Ptr> & clouds, std::vector<Eigen::Matrix4f> & alignments, explainMe::Cloud::Ptr fusedCld, std::string outFileStl = "",
		std::vector<explainMe::Cloud::Ptr>  clouds_full = std::vector<explainMe::Cloud::Ptr>(), bool cleanupMode = false, std::string outFileStl_afterCleanup = "");
	void doFusion(std::vector<explainMe::Cloud::Ptr> & clouds, std::vector<Eigen::Matrix4f> & alignments, Eigen::Matrix4f & global2Reference, explainMe::Cloud::Ptr fusedPoints, std::vector<int> & triangles, std::string outStlFile = "");
	//uses infinitam for registration too.
	void doFusion_and_regist(std::vector<explainMe::Cloud::Ptr> & clouds, float max_dist, RangeSensorDescription * d);
};