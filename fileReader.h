#pragma once

#include <string>
#include <vector>
#include "explainMeDefinitions.h"


class fileReader
{
public:
	fileReader(std::string folder, std::string basedepth = "depth_" , std::string baseimg = "img_");
	~fileReader(void);

	int numberOfFrames();
	void readFrame(int i, std::vector<explainMe::UINT16> & depth, std::vector<explainMe::RGBQUAD> & target_color);
	void readFrame(int i, std::vector<explainMe::UINT16> & depth, std::vector<explainMe::UINT16> & target_ir);
	int frameWidth();
	int frameHeight();
	static void listFiles(std::string folder, std::string query, std::vector<std::string> &target);
	std::vector<std::string> fileNames(int frame);

private:
	std::vector<std::string> files_depth;
	std::vector<std::string> files_color;
	std::string _folder;
	int _width, _height;
};

