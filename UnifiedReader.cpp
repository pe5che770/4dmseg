#include "UnifiedReader.h"
#include "fstream"
#include "cudaTool.h"
#include "fileReader.h"
#include <opencv2/opencv.hpp>

UnifiedReader::UnifiedReader(const std::string & folder):
currentCloud(new explainMe::Cloud())
{

	datafolder = folder;
	std::string calibFile = folder + "/calib.txt";
	std::cout << "File: " << calibFile << "\n";
	d = new unifiedSensorDescription(calibFile);
	std::cout << ":::::: Sensor Calibration :::::\n";
	std::cout << "fx\t" << d->f_x() << " \tfy\t " << d->f_y() <<"\n";
	std::cout << "px\t" << d->p_x() << " \tpy\t " << d->p_y() <<"\n";
	std::cout << "w\t" << d->width() << " \th\t " << d->height() << "\n";
	std::cout << "Depth units per meter: " << d->depthUnitsPerMeter() << "\n";
	d->coutNoiseModel();

	currentCloud->resize(d->width() * d->height());
	currentCloud->width = d->width();
	currentCloud->height = d->height();
	cuda = new cudaTool(d->descriptionData());

	
	fileReader::listFiles(folder, "depth_.*\\.png", d_files);
	fileReader::listFiles(folder, "ir_.*\\.png", ir_col_files);
	if (ir_col_files.size() == 0){
		has_ir = false;
		fileReader::listFiles(folder, "color_.*\\.png", ir_col_files);
	}

	if (ir_col_files.size() != d_files.size())
	{
		std::cout << "UnifiedReader :: Error: number of depth and ir/color files inconsistent, in folder:\n"
			<< folder << "\n"
			<< (has_ir ? "ir_.*.png:\t" : " \tcolor_.*.png:\t") << ir_col_files.size() << " \t depth_.*.png:\t" << d_files.size() << "\n";
		exit(1);
	}
}


UnifiedReader::~UnifiedReader()
{
	delete d;
	delete cuda;
}

bool UnifiedReader::loadFrame(int frame, bool cleanUp, float maxDist, bool bilateralFilter, float sig_z){
	if (frame >= numberOfFrames()){
		return false;
	}

	

	std::cout << "loading frame " << frame << "\n";
	std::cout << "names\n " << datafolder + "/" + d_files[frame] 
		<< " \t" << datafolder + "/" + ir_col_files[frame] << "\n";
	cv::Mat depth = cv::imread(datafolder + "/" + d_files[frame], CV_LOAD_IMAGE_ANYDEPTH);
	cv::Mat ir_rgb = cv::imread(datafolder + "/" + ir_col_files[frame], CV_LOAD_IMAGE_ANYDEPTH);

	//compute normals...
	int width = depth.cols;
	int numPx = depth.cols*depth.rows;
	std::cout << "numPx " << numPx << "\n";
	//ensure there is enough space allocated
	depthFloatBuffer.resize(numPx);
	normalFloatBuffer.resize(numPx * 3);

	for (int i = 0; i < depth.cols; i++) for (int j = 0; j< depth.rows; j++){
		depthFloatBuffer[j*depth.cols + i] = depth.at<uint16_t>(j, i) / d->depthUnitsPerMeter();
	}
	
	cuda->dev_setData(&depthFloatBuffer[0]);

	if (bilateralFilter)
	{
		std::cout << "Bilateral filter applied with sig_z " << sig_z << "\n";
		cuda->bilateralFilter(3, sig_z);
		cuda->dev_getData(&depthFloatBuffer[0]);
	}
	else{
		cuda->bilateralFilter();
	}

	cuda->computeNormals(false);
	cuda->dev_getNormals(&normalFloatBuffer[0]);

	float qnan = std::numeric_limits<float>::quiet_NaN();
	for (int i = 0; i < depth.cols; i++){

		for (int j = 0; j< depth.rows; j++){

			auto & p = currentCloud->at(i, j);
			p.normal_x = normalFloatBuffer[j*depth.cols + i];
			p.normal_y = normalFloatBuffer[j*depth.cols + i + numPx];
			p.normal_z = normalFloatBuffer[j*depth.cols + i + numPx * 2];

			//float sign = p.normal_z > 0 ? -1 : 1; 
			//point to eye!
			//float sign = (p.getVector3fMap().dot(p.getNormalVector3fMap()) < 0 ? 1 : -1);
			//p.getNormalVector3fMap() *= sign;

			if (depth.at<uint16_t>(j, i) == 0 && !(p.getNormalVector3fMap().squaredNorm() > 0.5) || depth.at<uint16_t>(j, i) / d->depthUnitsPerMeter() > maxDist){
				p.getVector3fMap() << qnan, qnan, qnan;
				p.getNormalVector3fMap() << qnan, qnan, qnan;
			}
			else{
				if (bilateralFilter)
				{
					p.z = depthFloatBuffer[j*depth.cols + i];
				}
				else{
					p.z = depth.at<uint16_t>(j, i) / d->depthUnitsPerMeter();
				}
				d->unproject(i, j, p.z, p.x, p.y);
			}

			if (!has_ir)
			{
				p.r = ir_rgb.at<cv::Vec3b>(j, i)[2];
				p.g = ir_rgb.at<cv::Vec3b>(j, i)[1];
				p.b = ir_rgb.at<cv::Vec3b>(j, i)[0];
			}
			else{
				uint8_t intensity = std::max(std::min(std::log(static_cast<float>(ir_rgb.at<uint16_t>(j, i))) / std::log(2) * 16 - 128, 255.0), 0.0);
				p.r = p.g = p.b = intensity;
			}
			p.curvature = d->sigmaHat(i, j, p.z, (has_ir ? ir_rgb.at<uint16_t>(j, i) : 0), p.getNormalVector3fMap().dot(-p.getVector3fMap().normalized()));//noise predicted somewhat using the intensity..
		}
	}
	return true;
}

std::vector<std::string> UnifiedReader::fileNames(int frame)
{
	std::vector<std::string> files;
	if (frame < numberOfFrames()){
		files.push_back(ir_col_files[frame]);
		files.push_back(d_files[frame]);
	}
	return  files;
}
