#pragma once
#include <QtWidgets>
#include <QThread>
#include <QString>
#include <QApplication>
#include <QtConcurrent>
#include "statsKeeper.h"
#include<iostream>

class QCustomPlot;

class statsWindow:public QMainWindow
{
	Q_OBJECT;
private:
	statsKeeper stats;
	QLabel * grandTotalTiming;
	QLabel * initTiming ;
	QLabel * interpolationTiming;
	QLabel * motionTiming ;
	QLabel * segTiming ;
	QLabel * otherTiming;
	QLabel * documentationTiming ;

	QLabel * initPercentage;
	QLabel * interpolationPercentage ;
	QLabel * motionPercentage;
	QLabel * segPercentage ;
	QLabel * documentationPercentage;
	QLabel * otherPercentage;

	QLabel * initDetails;
	QLabel * interpolationDetails;
	QLabel * motionDetails;
	QLabel * segDetails;
	QLabel * documentationDetails;
	QLabel * otherDetails;
	QLabel * generalInfo;

	QProgressBar * totalProgress;
	QCustomPlot * totalEnergy ;
	QCustomPlot * numHyps;
	QCustomPlot * registrationEnergy;
	//for corectly plotting the reg qualiEnergyty.
	int numRegEnergyPlots;
	float registrationEnergyMax;
public:
	statsWindow();
	~statsWindow();
	void initFields();
	void layoutGui();
	void setGeneralInfo(const std::string & info);

	static statsWindow & runNewStatsWindow(){
		statsWindow * myStats = new statsWindow();
		myStats->setAttribute(Qt::WA_DeleteOnClose);
		myStats->setAttribute(Qt::WA_QuitOnClose);

		QTimer * timer = new QTimer(myStats) ;
		timer->setInterval(1000);
		connect(timer, SIGNAL(timeout()), myStats, SLOT(updateFromStats()));
		timer->start();
		return *myStats;

	}
	
	template <typename RunnableQObjectToStat>
	static QFuture<void> setupLiveStatsAndDispatch(RunnableQObjectToStat & prg, statsWindow ** return_winHandle = NULL){
		statsWindow * myStats = new statsWindow();
		myStats->setAttribute(Qt::WA_DeleteOnClose);
		myStats->setAttribute(Qt::WA_QuitOnClose);

		QTimer * timer = new QTimer(myStats);
		timer->setInterval(1000);
		connect(timer, SIGNAL(timeout()), myStats, SLOT(updateFromStats()));
		connect(myStats, SIGNAL(closed()), timer, SLOT(stop()));
		timer->start();
		prg.setStats(myStats->stats);
		if (return_winHandle != NULL){
			//QTimer::singleShot(0, &prg, SLOT(run_with_stats()));
			*return_winHandle = myStats;
		}
		
		 auto future = QtConcurrent::run(&prg, &RunnableQObjectToStat::run_with_stats);
		return future;
	}


protected:
	void closeEvent(QCloseEvent *event){
		std::cout << "Stats window closed";
		Q_EMIT(SIGNAL(closed()));
	}


	/*bool event(QEvent * e){
		std::cout << "Event!" << e->type();
		return QMainWindow::event(e);
	}*/

public Q_SLOTS:
	void updateFromStats();
	void captureTimingsAndGraphsAsImage(const std::string & folder, const std::string & file);

Q_SIGNALS:
	void closed();
};

