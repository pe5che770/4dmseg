#include "algorithmParameters.h"
#include <fstream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/lexical_cast.hpp>
#include <string>
#include <boost/tokenizer.hpp>
#include <map>
#include "pcdReader.h"
#include "UnifiedReader.h"
#ifndef _USE_NO_CUDA_
#include "rawDataReaderKinect2.h"
#endif
#include "BonnReader.h"
#include "ppmJpgReader.h"


std::string parameterLoader::to_long_string(experimentParameters & exp_params, algorithmParameters & params){
	std::stringstream ss;
	ss << "Data: \t\t" << exp_params.dataFolder
		<< "\t(used frame " << params.first_frame << " +k delta" << params.delta_frame << " to: " << params.last_frame<< "\t\t ignored data after maxDist: " << params.maxDistance << "\n"
		<< "Software: \t\t" << exp_params.gitTag << "\t" << "Loaded state: " << exp_params.loadState << " use gpu dataterm " << exp_params.useGpuDataTerm << "\n"
		<< "Iterations: Init: " << exp_params.numIt_init << "\t" << "Main: " << exp_params.numIt_EM;
	ss << "\t\tOptional steps: \t\tcleanup flying: " << params.cleanUpClouds << " do Merging: " << params.mergeMotions << "\n";
	ss << "Parameters Sparse: \t\tsamples/frame " << params.samplesPerFrame << "\n";
	ss << "Parameters Init: \t"
		<< "\tOF quality: " << params.minOFQuality << "\t topAcceleration: " << params.topTrajAcceleration << " "
		<< "\tp" << params.p << ", " << params.p2 << " temp_smoothness: " << params.alpha_smootheness_w <<" stepsize: " << params.stepSize << "\n";
	ss << "Parameters main:\n"
		<< "\tRegistration: " << 
		(params.icp_type == MotionHypotheses::ICP_FANCY ? "fancy" :
		(params.icp_type == MotionHypotheses::ICP_FANCY_6D ? "fancy_6d" :
		(params.icp_type == MotionHypotheses::ICP_FANCY_SLOW ? "fancy_slow" :
		(params.icp_type == MotionHypotheses::ICP_INCREMENTAL ? "incremental" : "unknown"))))
		<<" \tIts: " << params.maxItICP << " a_p2pl: " << params.alpha_point2plane << " a_p2po: " << params.alpha_point2point << " culling_dist: " << params.icp_culling_dist << "\n";
	ss << "\tGC: dataterm weight: " << params.lambda_dataTerm << "smoothness weight : spa / tmp : " << params.lambda_smoothness_spatial << " / " << params.lambda_smoothness_temporal << "\t temporal_sigma_sqr: " << params.sig_windowSize_sqr << "\t spatial_sigma: " << params.sigma_dist <<  "\n"
		<< "\t\t base_penalty " << params.occlusion_basepenalty << " clamp_sqr: " << params.clamp_distSqr <</* " sigma_sqr (occlusion) " << params.stdDev_z_sqr <<*/ " with occlusion: " << params.model_occlusion << " "
		<< " use normals: " << params.gc_use_normals << "\tK_nn " << params.k_nn << "\t (crf it: " << params.maxItCRF << ")\n";
	ss << "\thyp pool: initial size " << params.numHypo << " top k shifts: " << params.k_generated_hyps << " how much over average: " << params.lambda_generate_hyp << "\n";
	ss << "\t\t labelshifts: " << params.generateHypotheses.byLabelShifts << " Death/Birth: " << params.generateHypotheses.byLabelShifts << " Spatial (outliers/ connected comp): " << params.generateHypotheses.byOutliers << ", " << params.generateHypotheses.byConnectedComponents << "\n";
		
	return ss.str();
}
void parameterLoader::loadFromFile(const std::string & filePath,
	experimentParameters & exp_params,
	algorithmParameters & alg_params)
{
	std::ifstream file;
	std::string line;
	file.open(filePath);


	//allow floats, bools and string parameters
	//float parameter names start with 'f'
	//string parameter names start with 's'
	//bool parameters start with 'b'
	//comment lines with #
	boost::char_separator<char> sep(" ");
	std::map<std::string, std::vector<float>> val;
	std::map<std::string, std::string> string_val;
	std::map<std::string, bool> bool_val;
	std::string name;

	if (file.is_open()){
		while (std::getline(file, line))
		{
			boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
			auto it = tokens.begin();
			if (it != tokens.end()){
				name = *it;
				it++;

				if (name.at(0) == 'f'){
					std::vector<float> vals;
					for (; it != tokens.end(); it++){
						vals.push_back(boost::lexical_cast<float>(*it));
					}
					val[name] = vals;
				}
				else if (name.at(0) == 's')
				{
					std::stringstream ss;
					bool add_space = false;
					for (; it != tokens.end(); it++){
						if (add_space){
							ss << " ";
						}
						ss << *it;

						add_space = true;
					}
					string_val[name] = ss.str();
				}
				else if (name.at(0) == 'b')
				{
					std::istringstream ss(*it);
					bool b;
					ss >> std::boolalpha >> b;
					bool_val[name] = b;
				}
				else if (name.at(0) == '#'){
					//comment.
				}
				else{
					std::cout << "could not parse line :\n" << line << "\n";
				}
			}
		}
		file.close();

		//mandatory parameters
		exp_params.dataFolder = string_val["s_dataset_folder"]; string_val.erase("s_dataset_folder");
		exp_params.gitTag = string_val["s_git_tag"]; string_val.erase("s_git_tag");
		exp_params.dataType = val["f_dataset_type"][0]; val.erase("f_dataset_type");

		//alg_params.central_frame = val["f_central_frame"][0]; val.erase("f_central_frame");
		alg_params.delta_frame = val["f_delta_frame"][0]; val.erase("f_delta_frame");
		alg_params.first_frame = val["f_first_frame"][0]; val.erase("f_first_frame");
		alg_params.last_frame = val["f_last_frame"][0]; val.erase("f_last_frame");


		if (val.find("f_num_it_init") != val.end()){
			exp_params.numIt_init = val["f_num_it_init"][0]; val.erase("f_num_it_init");
		}
		if (val.find("f_num_it_main") != val.end()){
			exp_params.numIt_EM = val["f_num_it_main"][0]; val.erase("f_num_it_main");
		}

		
		if (bool_val.find("b_skip_init") != bool_val.end())
		{
			exp_params.skipInit = bool_val["b_skip_init"];
			bool_val.erase("b_skip_init");
		}
		if (bool_val.find("b_load_state_from_file") != bool_val.end())
		{
			exp_params.loadState = bool_val["b_load_state_from_file"];
			bool_val.erase("b_load_state_from_file");
		}
		if (string_val.find("s_state_file") != string_val.end())
		{
			exp_params.stateFile = string_val["s_state_file"];
			string_val.erase("s_state_file");
		}
		if (string_val.find("s_motion_state_file") != string_val.end())
		{
			exp_params.motionStateFile = string_val["s_motion_state_file"];
			string_val.erase("s_motion_state_file");
		}
		if (string_val.find("s_ground_truth_motion_file") != string_val.end()){
			exp_params.ground_truth_motion_file = string_val["s_ground_truth_motion_file"];
			string_val.erase("s_ground_truth_motion_file");
		}
		if (val.find("f_doc_level") != val.end()){
			exp_params.docLevel = static_cast<experimentParameters::documentationLevel>((int)val["f_doc_level"][0]);
			val.erase("f_doc_level");
		}

		/*if (bool_val.find("b_store_clouds") != bool_val.end())
		{
		this->storeResultClouds = bool_val["b_store_clouds"];
		bool_val.erase("b_store_clouds");
		}*/

		if (bool_val.find("b_clean_up_clouds") != bool_val.end()){
			alg_params.cleanUpClouds = bool_val["b_clean_up_clouds"];
			bool_val.erase("b_clean_up_clouds");
		}
		if (bool_val.find("b_merge_similar_motions") != bool_val.end()){
			alg_params.mergeMotions = bool_val["b_merge_similar_motions"];
			bool_val.erase("b_merge_similar_motions");
		}
		if (bool_val.find("b_use_gpu_dterm") != bool_val.end()){
			exp_params.useGpuDataTerm = bool_val["b_use_gpu_dterm"];
			bool_val.erase("b_use_gpu_dterm");
		}

		if (val.find("f_num_hypo") != val.end()){
			alg_params.numHypo = val["f_num_hypo"][0];
			val.erase("f_num_hypo");
		}
		//additional parameters
		if (val.find("f_max_data_distance") != val.end()){
			alg_params.maxDistance = val["f_max_data_distance"][0];
			val.erase("f_max_data_distance");
		}
		if (val.find("f_min_of_quality") != val.end()){
			alg_params.minOFQuality = val["f_min_of_quality"][0];
			val.erase("f_min_of_quality");
		}
		if (val.find("f_top_traj_acceleration") != val.end()){
			alg_params.topTrajAcceleration = val["f_top_traj_acceleration"][0];
			val.erase("f_top_traj_acceleration");
		}
		if (val.find("f_delta_frame_optflow_seed") != val.end()){
			alg_params.delta_seedFrame = val["f_delta_frame_optflow_seed"][0];
			val.erase("f_delta_frame_optflow_seed");
		}
		if (val.find("f_p") != val.end()){
			alg_params.p = val["f_p"][0];
			val.erase("f_p");
		}
		if (val.find("f_p2") != val.end()){
			alg_params.p2 = val["f_p2"][0];
			val.erase("f_p2");
		}
		if (val.find("f_gradient_descent_step_size") != val.end()){
			alg_params.stepSize = val["f_gradient_descent_step_size"][0];
			val.erase("f_gradient_descent_step_size");
		}
		if (val.find("f_alpha_smoothness_w") != val.end()){
			alg_params.alpha_smootheness_w = val["f_alpha_smoothness_w"][0];
			val.erase("f_alpha_smoothness_w");
		}
		if (val.find("f_occlusion_basepenalty") != val.end()){
			alg_params.occlusion_basepenalty = val["f_occlusion_basepenalty"][0];
			val.erase("f_occlusion_basepenalty");
		}
		if (val.find("f_robust_metric_clamp_dist_sqr") != val.end()){
			alg_params.clamp_distSqr = val["f_robust_metric_clamp_dist_sqr"][0];
			val.erase("f_robust_metric_clamp_dist_sqr");
		}
		/*if (val.find("f_std_dev_z_sqr") != val.end()){
			alg_params.stdDev_z_sqr = val["f_std_dev_z_sqr"][0];
			val.erase("f_std_dev_z_sqr");
		}*/
		if (val.find("f_sig_windowsize_sqr") != val.end()){
			alg_params.sig_windowSize_sqr = val["f_sig_windowsize_sqr"][0];
			val.erase("f_sig_windowsize_sqr");
		}
		if (val.find("f_delete_label_numframes_threshold") != val.end()){
			alg_params.delete_labels_numframes_threshold = val["f_delete_label_numframes_threshold"][0];
			val.erase("f_delete_label_numframes_threshold");
		}
		if (bool_val.find("b_model_occlusion") != bool_val.end()){
			alg_params.model_occlusion = bool_val["b_model_occlusion"];
			bool_val.erase("b_model_occlusion");
		}
		if (bool_val.find("b_weight_error_via_windowsize") != bool_val.end()){
			alg_params.model_occlusion = bool_val["b_weight_error_via_windowsize"];
			bool_val.erase("b_weight_error_via_windowsize");
		}


		if (val.find("f_alpha_point2point") != val.end()){
			alg_params.alpha_point2point = val["f_alpha_point2point"][0];
			val.erase("f_alpha_point2point");
		}
		if (val.find("f_alpha_point2plane") != val.end()){
			alg_params.alpha_point2plane = val["f_alpha_point2plane"][0];
			val.erase("f_alpha_point2plane");
		}
		if (val.find("f_icp_culling_distance") != val.end()){
			alg_params.icp_culling_dist = val["f_icp_culling_distance"][0];
			val.erase("f_icp_culling_distance");
		}
		if (val.find("f_max_it_icp") != val.end()){
			alg_params.maxItICP = val["f_max_it_icp"][0];
			val.erase("f_max_it_icp");
		}
		if (val.find("f_per_hyp_cost_factor") != val.end()){
			alg_params.per_hyp_cost_factor = val["f_per_hyp_cost_factor"][0];
			val.erase("f_per_hyp_cost_factor");
		}

		

		if (string_val.find("s_icp_type") != string_val.end()){
			std::string what = string_val["s_icp_type"];
			if (what.compare("incremental") == 0){
				alg_params.icp_type = MotionHypotheses::ICP_INCREMENTAL;
			}
			else if (what.compare("fancy") == 0){
				alg_params.icp_type = MotionHypotheses::ICP_FANCY;
			}
			else if (what.compare("fancy_slow") == 0){
				alg_params.icp_type = MotionHypotheses::ICP_FANCY_SLOW;
			}
			else if (what.compare("fancy_6D") == 0){
				alg_params.icp_type = MotionHypotheses::ICP_FANCY_6D;
			}
			else{
				alg_params.icp_type = MotionHypotheses::ICP_FANCY;
				std::cout << "***Unknown icp type " << what << "\n";
			}
			string_val.erase("s_icp_type");
		}

		if (string_val.find("s_method_mode") != string_val.end()){
			std::string what = string_val["s_method_mode"];
			if (what.compare("CGF_17_CRF") == 0){
				exp_params.segmentationMode = experimentParameters::SEGMENTATION_CRF;
			}
			else if (what.compare("ARXIV_16_GC") == 0)
			{
				exp_params.segmentationMode = experimentParameters::SEGMENTATION_GC;
			}

			string_val.erase("s_method_mode");
		}


		if (val.find("f_samples_per_frame") != val.end()){
			alg_params.samplesPerFrame = val["f_samples_per_frame"][0];
			val.erase("f_samples_per_frame");
		}
		if (bool_val.find("b_samplePerHypo") != bool_val.end()){
			alg_params.samplePerLabel = val["b_samplePerHypo"][0];
			bool_val.erase("b_samplePerHypo");
		}
		if (val.find("f_knn") != val.end()){
			alg_params.k_nn = val["f_knn"][0];
			val.erase("f_knn");
		}
		if (val.find("f_sigma_dist") != val.end()){
			alg_params.sigma_dist = val["f_sigma_dist"][0];
			val.erase("f_sigma_dist");
		}
		if (val.find("f_lambda_dataterm") != val.end()){
			alg_params.lambda_dataTerm = val["f_lambda_dataterm"][0];
			val.erase("f_lambda_dataterm");
		}
		if (val.find("f_lambda_smooth_spatial") != val.end()){
			alg_params.lambda_smoothness_spatial = val["f_lambda_smooth_spatial"][0];
			val.erase("f_lambda_smooth_spatial");
		}
		if (val.find("f_lambda_smooth_temporal") != val.end()){
			alg_params.lambda_smoothness_temporal = val["f_lambda_smooth_temporal"][0];
			val.erase("f_lambda_smooth_temporal");
		}
		if (val.find("f_max_crf_it") != val.end()){
			alg_params.maxItCRF = val["f_max_crf_it"][0];
			val.erase("f_max_crf_it");
		}
		if (val.find("f_lambda_crf_s") != val.end()){
			alg_params.lambda_crf_s = val["f_lambda_crf_s"][0];
			val.erase("f_lambda_crf_s");
		}
		if (val.find("f_max_annealing_factor_crf") != val.end()){
			alg_params.max_annealing_factor_crf = val["f_max_annealing_factor_crf"][0];
			val.erase("f_max_annealing_factor_crf");
		}
		if (val.find("f_crf_sigma_sp_factor") != val.end()){
			alg_params.crf_sigma_sp_factor = val["f_crf_sigma_sp_factor"][0];
			val.erase("f_crf_sigma_sp_factor");
		}
		if (val.find("f_crf_sigma_t_factor") != val.end()){
			alg_params.crf_sigma_t_factor = val["f_crf_sigma_t_factor"][0];
			val.erase("f_crf_sigma_t_factor");
		}
		if (bool_val.find("b_min_crf_by_merge") != bool_val.end()){
			alg_params.crf_min_by_merge = bool_val["b_min_crf_by_merge"];
			bool_val.erase("b_min_crf_by_merge");
		}

		/*if (val.find("f_recursive_crf_sigma") != val.end()){
			alg_params.recursive_kernel_sigma_crf = val["f_recursive_crf_sigma"][0];
			val.erase("f_recursive_crf_sigma");
		}*/

		if (bool_val.find("b_gc_use_normals") != bool_val.end()){
			alg_params.gc_use_normals = bool_val["b_gc_use_normals"];
			bool_val.erase("b_gc_use_normals");
		}

		if (val.find("f_generate_max_hyp") != val.end()){
			alg_params.k_generated_hyps = val["f_generate_max_hyp"][0];
			val.erase("f_generate_max_hyp");
		}
		if (val.find("f_lambda_generate_hyp") != val.end()){
			alg_params.lambda_generate_hyp = val["f_lambda_generate_hyp"][0];
			val.erase("f_lambda_generate_hyp");
		}
		if (bool_val.find("b_generate_by_shifts") != bool_val.end()){
			alg_params.generateHypotheses.byLabelShifts = bool_val["b_generate_by_shifts"];
			bool_val.erase("b_generate_by_shifts");
		}
		if (bool_val.find("b_generate_by_carryovers") != bool_val.end()){
			alg_params.generateHypotheses.byCarryOvers = bool_val["b_generate_by_carryovers"];
			bool_val.erase("b_generate_by_carryovers");
		}
		if (bool_val.find("b_generate_by_outlierts_crf") != bool_val.end()){
			alg_params.generateHypotheses.byOutliers = bool_val["b_generate_by_outlierts_crf"];
			bool_val.erase("b_generate_by_outlierts_crf");
		}



		if (bool_val.size() != 0){
			std::cout << "Could not parse bool arguments:\n";
			for (auto it = bool_val.begin(); it != bool_val.end(); it++){
				std::cout << "\t" << it->first << "\n";
			}
			exit(1);
		}
		if (string_val.size() != 0){
			std::cout << "Could not parse string arguments:\n";
			for (auto it = string_val.begin(); it != string_val.end(); it++){
				std::cout << "\t" << it->first << "\n";
			}
			exit(1);
		}

		if (val.size() != 0){
			std::cout << "Could not parse float arguments:\n";
			for (auto it = val.begin(); it != val.end(); it++){
				std::cout << "\t" << it->first << "\n";
			}
			exit(1);
		}
	}
	else{
		std::cout << "Error reading file " << filePath << " \n";

		return;
	}

	alg_params.removeThreshold = std::max<float>(alg_params.samplesPerFrame * alg_params.delete_labels_numframes_threshold * 0.005, 50.f);

}

algorithmParameters::~algorithmParameters()
{

}


CloudReader::Ptr experimentParameters::createReader(){
	CloudReader::Ptr reader;
	if (dataType == pcd_data){
		reader = CloudReader::Ptr(new pcdReader(this->dataFolder));
	}
	else if (dataType == kinect2_ir_data){
#ifndef USE_NO_CUDA
		reader = CloudReader::Ptr(new rawDataReaderKinect2(this->dataFolder));
#else
		std::cout << "Error! Kinect data can only be read when cuda is enabled";
#endif // !USE_NO_CUDA
	}
	else if (dataType == bonn_data){
		reader = CloudReader::Ptr(new BonnReader(this->dataFolder));
	}
	else if (dataType == unified_reader){
		reader = CloudReader::Ptr(new UnifiedReader(this->dataFolder));
	}
	else if (dataType == ppm_jpg_data){
		reader = CloudReader::Ptr(new ppmJpgReader(this->dataFolder));
	}
	else if (dataType == sintel_data){
		reader = CloudReader::Ptr(new SintelReader(this->dataFolder));
	}
	else if (dataType == kinect2_ir_png_data){
		reader = CloudReader::Ptr(new Kin2PNGReader(this->dataFolder));
	}
	else{
		std::cout << "Unknown Data format type: " << dataType << "\n";
	}
	return reader;
}

