#pragma once
#include <QObject>
#include "gl3w.h"
#include <vector>
#include "explainMeDefinitions.h"

class QGLWidget;
class QMatrix4x4;
class QMainWindow;
class RangeSensorDescription;
class sceneManager;

class glOffscreenRenderer:public QObject
{
	Q_OBJECT
private:
	RangeSensorDescription * sensor_desc;
	QGLWidget * offscreenContext;
	QMainWindow * win;

	GLuint m_framebufferName, m_depthBufferName;
	//GLuint m_textureName;
	GLuint m_textureNames[2];

	std::string m_vshader, m_fshader, m_gshader;


	bool m_renderToScreen;
	
	sceneManager * scene;
	//glSurflets * glSurfels;
	QMatrix4x4 * proj, *modelview;

	//rendering target
	std::string render_target;

public:
	//Constructor for an Offscreen renderer, which will render scenes
	// with the resolution and frustum of the described sensor.
	//The shader arguments define the shader to be used, if no other
	//shader is specified when the rendertarget is updated the first time.
	glOffscreenRenderer(
		RangeSensorDescription * sensor_desc,
		bool renderToScreen,
		std::string vshader, 
		std::string fshader, 
		std::string gshader);
	~glOffscreenRenderer(void);
	

	void updateTriangles(std::string targetName, 
		explainMe::Cloud::Ptr & points,
		std::vector<int> indices,
		std::string vshader = "", 
		std::string fshader = "", 
		std::string gshader = "");
	void updateCloud(std::string targetName,
		explainMe::Cloud::Ptr & points);
	void setCamera(Eigen::Matrix4f cameraTransform);
	void doOffscreenRendering2IntBuffer(std::string targetName, int * alocatedBuffer = NULL);// *target = NULL);
	void doOffscreenRendering2IntBuffer(std::string targetName, std::vector<GLint> * target1, std::vector<GLint> * target2);

private:
	void prepare();
};

