#pragma once
#include "Initializer.h"

/**
*
*Not very good.
*The problem is that, as one weight is defined in terms of the others via something like 1- sum others,
* one has to constrain somehow that 1-sum others is a valid weight. And that's back to square one.
*/
class InitByEnergyMinimization_noProj :
	public ComputeWeights
{
private:
	explainMe::MatrixXf_rm d_dw_E;

	int numIt; float p1; float p2; float stepSize; int numHyp;

	int numClouds;

public:
	InitByEnergyMinimization_noProj(int numIt, 
		float p1, float p2, 
		float stepSize, 
		int numHyp, int numClouds);
	~InitByEnergyMinimization_noProj(void);

	virtual void findWeights(
		std::vector<trajectory_R> & traj_in, 
		explainMe::MatrixXf_rm & w_out);

	void getddwE(explainMe::MatrixXf_rm & target){
		target = d_dw_E;
	}
};

