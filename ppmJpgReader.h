#pragma once
#include "CloudReader.h"

class cudaTool;

class ppmJpgReader : public CloudReader
{
	std::string dataFolder;
	std::vector<std::string> depth_ppm, color_jpg;
	explainMe::Cloud::Ptr cloud;
	std::vector<float> depthFloatBuffer, normalFloatBuffer;
	RangeSensorDescription * d;
	cudaTool * cuda;
public:
	ppmJpgReader(std::string & folder);
	~ppmJpgReader();


	virtual RangeSensorDescription * getDescription(){
		return d;
	}
	virtual size_t numberOfFrames(){
		return depth_ppm.size();
	}
	virtual bool loadFrame(int frame, bool cleanUp, float maxDist, bool bilateralFilter, float sig_z);
	
	virtual std::vector<std::string> fileNames(int frame){
		std::vector<std::string> names;
		if (frame < depth_ppm.size()){
			names.push_back(depth_ppm[frame]);
		}
		if (frame < color_jpg.size()){
			names.push_back(color_jpg[frame]);
		}
		return names;
	}

	virtual explainMe::Cloud::Ptr & getPointCloud(){
		return cloud;
	}
};

