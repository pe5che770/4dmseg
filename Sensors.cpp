#include "Sensors.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/lexical_cast.hpp>
#include <string>
#include <boost/tokenizer.hpp>
#include <map>
#include <regex>

void RangeSensorDescription::setup(int width, int height, QMatrix4x4 &frustum){
		_width = width;
		_height = height;

		_viewPort = QMatrix4x4(
			(width)/2.0, 0,0,(width-1)/2.0,
			0,(height)/2.0,0,(height-1)/2.0,
			//(width)/2.0, 0,0,(width)/2.0,
			//0,(height)/2.0,0,(height)/2.0,
			0,0,1,0,
			0,0,0,1
			);/*QMatrix4x4((width-1)/2.0, 0,0,(width-1)/2.0,
			0,(height-1)/2.0,0,(height-1)/2.0,
			0,0,1,0,
			0,0,0,1
			);*/

		QMatrix4x4 viewPort_inv(2.0/width, 0,0, -((width -1)*1.0)/width,
			0, 2.0/height, 0, -((height -1)*1.0)/height,
			0,0,1,0,
			0,0,0,1);

		QMatrix4x4 zToNormalizedViewVolume = 
			QMatrix4x4(1, 0,0,0,
				0,1,0,0,
				0,0,frustum(2,2), frustum(2,3),
				0,0,0,1);

		_frustum =  frustum;

		//unproject maps (i,j,z) to the original point (x,y,z)
		//_unproject = frustum.inverted()*_viewPort.inverted() * zToNormalizedViewVolume;
		_unproject = frustum.inverted()*viewPort_inv * zToNormalizedViewVolume;
		//_project = /*zToNormalizedViewVolume.inverted() */ _viewPort * _frustum;

		int i = _width/2; int j = _height/2;
		float x1,y1,x2,y2,z=1;
		unproject(i,j,z,x1,y1);
		unproject(i+1,j+1,z,x2,y2);
		_sample_distance = 1.f*sqrtf((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));


		if(fx == 0 && fy == 0){
			extract(px, py, fx, fy);
		}
		K.fill(0);
		K(0,0) = fx; K(1,1) = fy; K(0,2) = px; K(1,2) = py; K(2,2) = 1;
	}

void RangeSensorDescription::extract(float & px, float & py, float &fx, float & fy){
	project(0.f, 0.f, 1.f, px, py);
	float px_plus_fx, py_plus_fy;
	project(1.f, 1.f, 1.f, px_plus_fx, py_plus_fy);
	fx = px_plus_fx - px;
	fy = py_plus_fy - py;
	//std::cout << "extracted principal and focals from frustum ("
	//	<< px << " " << py << ")\t (" << fx << " " << fy << ")\n";
}

//loader for .kin2 files
void RangeSensorDescription::loadFromFileKin2(std::string filePath){
	std::ifstream file;
	std::string line;
	file.open(filePath);


	boost::char_separator<char> sep(" ");
	std::map<std::string, std::vector<float>> val;
	std::string name;
	
	QMatrix4x4 frust;
	int w,h;

	if(file.is_open()){
		while(std::getline(file, line))
		{
			boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
			auto it = tokens.begin();
			name = *it;
			it++;

			std::vector<float> vals;
			for(; it != tokens.end(); it++){
				vals.push_back(boost::lexical_cast<float>(*it));
			}

			val[name] = vals;
		
		}
		file.close();

		w=val["depth"][0], h=val["depth"][1];
		float near = 0.1f; float far = 100.f;
		float left = -val["principal"][0]/val["focal"][0];
		float right =  (w-val["principal"][0])/val["focal"][0];
		float bottom =  -val["principal"][1]/val["focal"][1];
		float top =  (h-val["principal"][1])/val["focal"][1];

		left*=near; right*=near;bottom*=near;top*=near;

		frust.frustum(left,right,bottom,top,near,far);

		k2 = val["distortion"][0];k4 = val["distortion"][1];k6 = val["distortion"][2];
		px = val["principal"][0]; py = val["principal"][1]; fx=  -val["focal"][0];
		fy = val["focal"][1];

	}
	else{
		std::cout << "Error reading file " << filePath << " \n";
	}


	
	setup(w,h,frust);

}


void RangeSensorDescription::loadFromFile(std::string filePath){
	std::ifstream file;
	std::string line;
	file.open(filePath);

	int w=0, h=0;
	QMatrix4x4 frust;

	if(file.is_open()){
		std::getline(file, line);
		{
			std::istringstream in(line);
			in >> w >> h;
		}
		for(int i = 0; i < 4; i++){
			std::getline(file, line);
			std::istringstream in(line);
			in >> frust(i,0) >> frust(i,1) >> frust(i,2) >> frust(i,3);
		}
	}
	else{
		std::cout << "Error reading file " << filePath << " \n";
	}

	setup(w,h,frust);
}

void RangeSensorDescription::loadFromUnifiedFile(std::string filePath){
	std::ifstream file;
	std::string line;
	file.open(filePath);

	int w = 0, h = 0;
	QMatrix4x4 frust;

	if (file.is_open()){
		std::getline(file, line);
		{
			std::istringstream in(line);
			in >> w >> h;
		}
		std::getline(file, line);
		{
			std::istringstream in(line);
			in >> fx >> fy;
		}
		std::getline(file, line);
		{
			std::istringstream in(line);
			in >> px >> py;
		}
		std::getline(file, line);
		std::string  emptyline = std::regex_replace(line, std::regex("^ +| +$|( ) +"), "$1");
		if (emptyline.size() > 0){
			std::cout << "Illegal calib.txt format\n";
			std::cout << line << "\nshould be empty\n";
		}
		std::getline(file, line);
		if (std::regex_match(line, std::regex("^\\s*synthetic\\s*$")))
		{
			a_0 = 0.001;
			is_synthetic = true;
			std::cout << "Synthetic Sensor!\n";
		}
		else
		{
			{
				std::istringstream in(line);
				in >> a_0 >> a_1 >> a_2;
			}
			std::getline(file, line);
			{
				std::istringstream in(line);
				in >> b_1 >> b_2;
			}
			std::getline(file, line);
			{
				std::istringstream in(line);
				in >> c;
			}
		}
		std::getline(file, line);
		{
			std::istringstream in(line);
			in >> numUnitsPerMeter;
		}
		
	}
	else{
		std::cout << "Error reading file " << filePath << " \n";
	}

	float near = 0.1f; float far = 100.f;
	float left = -px / fx;
	float right = (w - px) / fx;
	float bottom = -py / fy;
	float top = (h - py) / fy;

	left *= near; right *= near; bottom *= near; top *= near;

	frust.frustum(left, right, bottom, top, near, far);
	setup(w, h, frust);
}

void RangeSensorDescription::saveToFile(std::string filePath){
	std::ofstream file;
	file.open(filePath);
	file << _width << " " << _height << "\n";
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 4; j++){
			file << boost::lexical_cast<std::string>(_frustum(i,j))/*std::to_string(_frustum(i,j))*/ << " ";
		}
		file << "\n";
	}
	file.close();
}