#pragma once

//#define NOMINMAX
//#include "gl3w.h"
#include <QtWidgets>
#include <QMainWindow>
#include <QtOpenGL/QGLFormat>
#include <QPushButton>
#include <QLabel>
//#include "DataCoordinator.h"
#include "glDisplay.h"

class mainWindow: public QMainWindow
{
	Q_OBJECT

public:
	mainWindow(QGLFormat & format);
	~mainWindow(void);
	void layoutGui();
	void setUpButtons();
	void addAction();
	sceneManager * getSceneManager();
	glDisplay* getGLWidget();
	glDisplay* getGLWidget_bot1();
	glDisplay* getGLWidget_bot2();
	glDisplay* getGLWidget_bot2_2();
	glDisplay* getGLWidget_bot3();
	QPushButton * getButton1();
	QPushButton * getButton2();
	QPushButton * getButton3();
	QPushButton * getButton4();

	void keyPressEvent(QKeyEvent* e);
	void printInfo(std::string info);

public Q_SLOTS:
	void printClicked();

Q_SIGNALS:
	void numericalKeyPressed(int value);
	void charKeyPressed(char value);

private:
	glDisplay * myGlDisp;
	glDisplay * glDisplay_bot1;
	glDisplay * glDisplay_bot2;
	glDisplay * glDisplay_bot2_2;
	glDisplay * glDisplay_bot3;
	QPushButton *b1, *b2,*b3,*b4;
	QLabel * l_out;
};

