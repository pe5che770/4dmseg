#include "InitByEnergyMinimization_smoothness.h"
#include "BundleAdjustment.h"
#include "mySimpleKdTree.h"

InitByEnergyMinimization_smoothness::InitByEnergyMinimization_smoothness(int numIt, 
		float p1, float p2, 
		float stepSize, 
		int numHyp, int numFrames,
		float alpha_smoothness)
{
		this->numIt = numIt;
	this->p1 = p1;
	this->p2 = p2;
	this->stepSize = stepSize;
	this->numHyp = numHyp;
	this->numFrames = numFrames;
	this->alpha_smoothness = alpha_smoothness;
}


InitByEnergyMinimization_smoothness::~InitByEnergyMinimization_smoothness(void)
{
}



void InitByEnergyMinimization_smoothness::findWeights(std::vector<trajectory_R> & trajectories,
	explainMe::MatrixXf_rm & w)
{
	//kill this one:
	int reference_frame =0;
	
	//find the neighborhood of trajectories
	findNeighborTrajectories(trajectories,3,N_start, N_end, numFrames);
	
	w.resize(trajectories.size(), numHyp);
	d_dw_E = w;
	d_dw_E.fill(1.f/numHyp);

	explainMe::MatrixXf_rm total_diff = d_dw_E;
	

	//init the ws
	//init rand, but concentrated around ones/numHyp
	w.fill(1);
	srand((unsigned int) 99638);
	for(int i = 0; i < w.rows(); i++){
		w.row(i) += explainMe::RowVectorXf::Random(numHyp);
		w.row(i) = w.row(i).cwiseAbs();
		w.row(i) = w.row(i)/w.row(i).sum();
	}



	//the whole shabang.
	Eigen::Matrix4f id = Eigen::Matrix4f::Identity();
	//alignmentSet s(id, reference_frame);
	//s.resize(first_frame, last_frame,id);
	std::vector<Eigen::Matrix4f> s(numFrames,id);


	//T[i_w][i_f] is the alignment 
	//			from ref frame to i_f.
	//			corresponding to the weight set i_w
	std::vector<std::vector<Eigen::Matrix4f>> T;
	T.resize(numHyp,s);


	//for general reprojection:
	
	explainMe::RowVectorXf w_center = explainMe::RowVectorXf::Ones(numHyp)/numHyp, new_constraint;
	Eigen::MatrixXf constraints(numHyp, numHyp);// = Matrixnf::Zeros();
	Eigen::MatrixXf e = Eigen::MatrixXf::Identity(numHyp,numHyp);
	int min_coeff_idx; float total_moved,max_movement;


	for(int it_step = 0; it_step < numIt; it_step ++){
		if (abortComp){
			return;
		}
		std::cout << it_step << "/" << numIt << "...";
		//compute alignments
		for(int hyp = 0; hyp < numHyp; hyp++){

			//used to reset alignments to identity. But might not need to.
			T[hyp] = s;

			//this does a fixed number of icp steps.
			//the reason that I don't use the closed formula is that alignments from 
			//the reference frame to each frame is computed. Not completely clear
			//how to solve for these with the closed formula. Dunno if appending the frame to frame 
			//solutions would be right.
			BundleAdjustment::pointToPlane_Bundle_step<explainMe::MatrixXf_rm>
				(trajectories,numFrames, reference_frame,w,hyp,T[hyp]);
		}
		
		
		//compute "gradient"

		//version for arbitrary paths
		compute_d_dw_E_w_pow_p_tempSmoothness(
			trajectories,
			N_start, N_end,
			w,	T,		
			numFrames, reference_frame,
			d_dw_E, p2,
			alpha_smoothness);

		//with temporal smoothnessterm
		/*compute_d_dw_E_w_pow_p_fast_incomplete_anyFrame(
			trajectories, N_start, N_end,	w,	T,		
			first_frame, last_frame, reference_frame,
			d_dw_E, params.p2,
			params.alpha_smootheness_w);*/


		explainMe::RowVectorXf w_tangent = explainMe::RowVectorXf::Ones(numHyp), tmp, w_proj, max_movement_possible, border_nrml;
		w_tangent.normalize();
		//d_dw_E_nonprojected = d_dw_E;

		
		for(int i = 0; i < d_dw_E.rows(); i++){
			
			//d_dw_E[i] = d_dw_E[i] - (w_tangent * d_dw_E[i].dot(w_tangent)); //reproject on w's tangential space.
			d_dw_E.row(i) = d_dw_E.row(i) - (w_tangent * d_dw_E.row(i).dot(w_tangent)); //reproject on w's tangential space.
		}

		//float avg_movement = d_dw_E.rowwise().norm().mean();
		float avg_movement = total_diff.rowwise().norm().mean();
		total_diff = w;

		for(int i = 0; i < w.rows(); i++){
			//gradient descent,
			//w.row(i) =  w.row(i) - d_dw_E.row(i)*stepSize;

			//guaranty step size.
			w.row(i) =  w.row(i) - d_dw_E.row(i)*stepSize/avg_movement;
			//"infinite step"
			//w[i] = Eigen::Vector3f::Ones()/3 - d_dw_E[i]/d_dw_E[i].norm() * 2.44948974278/3;//sqrt(6)/3


			// projection procedure: projection onto nd tetrahedron. see matlab implementation.
			//todo put in own method.
			//auto w_orig = w[i];
			//	w_proj = w[i]; 
			if(w.row(i).minCoeff() <0 ){ //if reprojection is needed.
				w_proj = w_center;
				tmp = w.row(i) - w_proj;
				constraints.fill(0);
				constraints.col(0) = w_tangent;
				total_moved = 0;

				for(int it_proj = 0; it_proj < numHyp-1 && total_moved < (1-1e-5); it_proj++){
					//this positive component w�se factor times tmp(i) can be added to w_proj(i) without it becoming negative
					max_movement_possible = - w_proj.cwiseQuotient(tmp); 
					max_movement_possible = (tmp.array() >=-1e-5).select(1e10,max_movement_possible.array()); //for pos or very small dims "infinite" movement ok.				

					//find coefficient which will first be zero when moving in direction tmp.
					max_movement = max_movement_possible.minCoeff(&min_coeff_idx);
					max_movement = std::min(max_movement, 1-total_moved);
					//move in this diection

					w_proj += tmp * max_movement;
					//now tmp needs to be reprojected, as one coordinate is zero.
					new_constraint = e.col(min_coeff_idx) - constraints * constraints.transpose() * e.col(min_coeff_idx);
					constraints.col(it_proj +1) = new_constraint.normalized();
					//tmp = tmp - constraints * constraints.transpose() *tmp;
					tmp = tmp - tmp *constraints * constraints.transpose();

					total_moved = total_moved + max_movement;


				}

				w.row(i) = w_proj;
				
				if(w_proj(0)*0 != 0){
					std::cout << "AAAAH, projection nan:\n " << w.row(i) << " \nprojected to\n" << w_proj;
					int a;
					std::cin >> a;
				}
			}

			//for numerics
			w.row(i) = (w.row(i).array() < 0 ).select(0,w.row(i));
			w.row(i) = w.row(i) / w.row(i).sum(); 
			
		}
		total_diff -= w;
	}
}


void InitByEnergyMinimization_smoothness::findNeighborTrajectories(
	std::vector<trajectory_R> & trajs, 
	int k,
	std::vector<std::vector<int>> & N_start, 
	std::vector<std::vector<int>> & N_end, 
	int numClouds)
{
	//std::cout << "Finding nns...";
	N_start.clear();
	N_start.resize(trajs.size());
	N_end = N_start;

	pcl::PointCloud<pcl::PointXYZINormal>::Ptr ps(new pcl::PointCloud<pcl::PointXYZINormal>());
	ps->reserve(trajs.size());
	std::vector<int> inds;
	inds.reserve(trajs.size());

	//fwd extension of trajs.
	for (int cloud = 0; cloud <numClouds; cloud++){
		//std::cout << frm << ":";
		ps->clear();
		inds.clear();
		for(int i = 0; i < trajs.size(); i++){
			if (trajs[i].seedCloud() < cloud && trajs[i].cloudInRange(cloud)){
				ps->push_back(trajs[i][cloud]);
				inds.push_back(i);
			}
		}
	
		if(ps->size() > 0){
			mySimpleKdTree tree;
			tree.setData(ps);
			simpleNNFinder knn(tree);

			std::vector<distSqr_idx> dist_sqr_idx;
			for(int i = 0; i < trajs.size(); i++){
				if(trajs[i].firstCloud() == cloud){
					auto & p = trajs[i][cloud];
					pxyz pt(p.x,p.y,p.z);
					knn.findkNN(pt,k,dist_sqr_idx);

					for(int j = 0; j < dist_sqr_idx.size(); j++){
						N_start[i].push_back(inds[dist_sqr_idx[j].index]);
					}
				}
			}
		}
	}


	//bwd extension of trajs.
	for (int cloud = 0; cloud <numClouds; cloud++){
		//std::cout << frm << ",";
		ps->clear();
		inds.clear();
		for(int i = 0; i < trajs.size(); i++){
			if (trajs[i].seedCloud() > cloud && trajs[i].cloudInRange(cloud)){
				ps->push_back(trajs[i][cloud]);
				inds.push_back(i);
			}
		}

		if(ps->size() > 0){
			mySimpleKdTree tree;
			tree.setData(ps);
			simpleNNFinder knn(tree);

			std::vector<distSqr_idx> dist_sqr_idx;
			for(int i = 0; i < trajs.size(); i++){
				if (trajs[i].lastCloud() == cloud){
					auto & p = trajs[i][cloud];
					pxyz pt(p.x,p.y,p.z);
					knn.findkNN(pt,k,dist_sqr_idx);
					for(int j = 0; j <  dist_sqr_idx.size(); j++){
						N_end[i].push_back(inds[dist_sqr_idx[j].index]);
					}
				}
			}
		}
	}
	//std::cout << "tadah";
}

void InitByEnergyMinimization_smoothness::compute_d_dw_E_w_pow_p_tempSmoothness(std::vector<trajectory_R> & paths,
													 std::vector<std::vector<int>> & N_start,std::vector<std::vector<int>> & N_end,
													 explainMe::MatrixXf_rm &w,
													 //T[i_f][i_w]
													 std::vector<std::vector<Eigen::Matrix4f>> &T,
													 int numFrames, int reference_frame,	
													 //std::vector<Eigen::VectorXf> &d_dw_E,
													 explainMe::MatrixXf_rm & d_dw_E,
													 float p,
													 float alpha)
{
	//In principal I'd need the gradient of the pointwise error function. For now I will simply use
	//x-c(x) normed and as error the point 2 point error.

	//d_dw_E is the current error plus a weighted sum of error gradients
	auto d_dw_E_smooth = d_dw_E; //TODO this alloc is very inefficient!

	std::vector<std::vector<Eigen::Matrix4f>> ainv = T;
	for(int i = 0; i < ainv.size(); i++){
		for(int j = 0; j < ainv[i].size(); j++){
			ainv[i][j] = T[i][j].inverse();
		}
	}

	//derivative for each (weight-set, path) pair (i.e. each weight)
	for(int i_w =0; i_w < w.cols(); i_w++){
		for(int i = 0; i < paths.size(); i++)
		{
			//compute one single derivative:
			d_dw_E(i,i_w) = 0;
			d_dw_E_smooth(i,i_w) =0;
			//the derivative is dependent of all hops(path length) and paths;
			//the first sum treats forward hops...
			for(int frm = paths[i].firstCloud(); frm <= paths[i].lastCloud(); frm++){
				if(paths[i].cloudInRange(frm))
				{
					//Eigen::Vector4f  p = paths[i][0].getVector4fMap();
					//current error to frame frm.
					d_dw_E(i,i_w) = d_dw_E(i,i_w) + p *pow(w(i,i_w),p-1) * 
						//point to point
			//			(T[i_w][frm] * ainv[i_w][paths[i].seedFrame()] * paths[i].seedEl().getVector4fMap() - paths[i][frm].getVector4fMap()).norm();
						//point to plane.
						std::abs((T[i_w][frm] * ainv[i_w][paths[i].seedCloud()] * paths[i].seedEl().getVector4fMap() - paths[i][frm].getVector4fMap()).topLeftCorner<3,1>().dot(paths[i][frm].getNormalVector3fMap()));
					//and the main sum is left out.
				}
			}

		}
	}//end for.

	//smoothness term
	float sim = 0; int frm, n;
	std::vector<float> num_constr(paths.size(), 0.f);
	for(int i = 0; i < paths.size(); i++)
	{
		for(int nbr = 0; nbr < N_start[i].size(); nbr++){
			n = N_start[i][nbr];
			frm = paths[i].firstCloud();
			sim = 1/(1+(paths[i][frm].getVector3fMap() - paths[n][frm].getVector3fMap()).norm())
				*pow(paths[i][frm].getNormalVector3fMap().dot(paths[n][frm].getNormalVector3fMap()),2.f);
			d_dw_E_smooth.row(i) += (w.row(i) - w.row(n))*alpha * sim;
			num_constr[i] += sim;
			d_dw_E_smooth.row(n) += (w.row(n) - w.row(i))*alpha * sim;
			num_constr[n] += sim;
		}
		for(int nbr = 0; nbr < N_end[i].size(); nbr++){
			n = N_end[i][nbr];
			frm = paths[i].lastCloud();
			sim = 1/(1+(paths[i][frm].getVector3fMap() - paths[n][frm].getVector3fMap()).norm())
				*pow(paths[i][frm].getNormalVector3fMap().dot(paths[n][frm].getNormalVector3fMap()),2.f);
			d_dw_E_smooth.row(i) += (w.row(i) - w.row(n))*alpha * sim;
			num_constr[i] += sim;
			d_dw_E_smooth.row(n) += (w.row(n) - w.row(i))*alpha * sim;
			num_constr[n] += sim;
			//d_dw_E[i] += (w[i] - w[N_end[i][nbr]])*alpha;
			//d_dw_E[N_end[i][nbr]] += (w[N_end[i][nbr]] - w[i])*alpha;
		}
	}
	for(int i = 0; i < paths.size(); i++)
	{
		d_dw_E.row(i) += d_dw_E_smooth.row(i)/(1 +num_constr[i]);
	}
}
