#include "BonnReader.h"
#include <fstream>
#include <sstream>
#include <opencv2/opencv.hpp>
#include <stdint.h>
#include <limits>
#include "cudaTool.h"

BonnReader::BonnReader(const std::string & folder):
currentCloud(new explainMe::Cloud())
{
	std::cout << "Loading Bonn type of files..\n";
	std::string associationFile = folder + "/associations.txt";
	listFiles(associationFile);
	std::cout << "Loaded " << numberOfFrames() << "filename pairs.\n";

	currentCloud->resize(640* 480);
	currentCloud->width = 640;
	currentCloud->height = 480;
	datafolder = folder;
	//uses an asus vtion pro live camera.
	d = new BonnAsusXtionProDescription();

	cuda = new cudaTool(d->descriptionData());

}

BonnReader::~BonnReader(){
	delete d;
	delete cuda;
}

void BonnReader::listFiles(const std::string & associationFile)
{
	std::string timestamp, filea, fileb;
	std::ifstream file_in(associationFile);
	char buff[2048];
	char delim = ' ';
	bool ok = true;
	std::vector<std::string> tokens;
	while (file_in.getline(buff, 2048)){
		std::istringstream ss(buff);
		ok = true;
		if (std::getline(ss, timestamp, delim)){
			tokens.clear();
			for (std::string each; std::getline(ss, each, delim); tokens.push_back(each));

			ok = tokens.size() == 3;
	
			if (!ok){
				std::cout << "Error reading color frame in pair : " << buff << "\n";
			}
			else{
				filea = tokens[0];
				fileb = tokens[2];
				d_rgb_files.push_back(std::pair<std::string, std::string>(filea, fileb));
			}
		}
	}
}

std::vector<std::string> BonnReader::fileNames(int frame){
	std::vector<std::string> files;
	files.push_back(datafolder + "/" +d_rgb_files[frame].first);
	files.push_back(datafolder + "/" + d_rgb_files[frame].second);
	std::cout << frame;
	return files;
}

bool BonnReader::loadFrame(int frame, bool cleanUp, float maxDist, bool bilateralFilter, float sig_z)
{
	if (frame >= numberOfFrames()){
		return false;
	}

	std::cout << "loading frame " << frame << "\n";
	std::cout << "names " << d_rgb_files[frame].first << " \t" << d_rgb_files[frame].second << "\n";
	cv::Mat depth = cv::imread(datafolder + "/" + d_rgb_files[frame].first, CV_LOAD_IMAGE_ANYDEPTH);
	cv::Mat rgb = cv::imread(datafolder + "/" + d_rgb_files[frame].second);

	//compute normals...
	int width = depth.cols;
	int numPx = depth.cols*depth.rows;
	std::cout << "numPx " << numPx << "\n";
	//ensure there is enough space allocated
	depthFloatBuffer.resize(numPx);
	normalFloatBuffer.resize(numPx * 3);

	for (int i = 0; i < depth.cols; i++) for (int j = 0; j< depth.rows; j++){
		depthFloatBuffer[j*depth.cols + i] = depth.at<uint16_t>(j, i) / 5000.f;
	}
	cuda->dev_setData(&depthFloatBuffer[0]);
	
	if (bilateralFilter)
	{
		std::cout << "Bilateral filter applied with sig_z " << sig_z << "\n";
		cuda->bilateralFilter(3, sig_z);
		cuda->dev_getData(&depthFloatBuffer[0]);
	}
	else{
		cuda->bilateralFilter();
	}
	
	cuda->computeNormals(false);
	cuda->dev_getNormals(&normalFloatBuffer[0]);

	float qnan = std::numeric_limits<float>::quiet_NaN();
	//std::cout << "Depth: " << depth.cols << " " << depth.rows << "\n";
	//std::cout << "Col: " << rgb.cols << " " << rgb.rows << "\n";
	//std::cout << "Detph has opencv type " << depth.type()<< "\n";
	//std::cout << CV_8UC3 << "\n";
	//std::cout << "Color has opencv type " << rgb.type() << "\n";
	for (int i = 0; i < depth.cols; i++){
		
		for (int j = 0; j< depth.rows; j++){
			
			auto & p = currentCloud->at(i, j);
			p.normal_x = -normalFloatBuffer[j*depth.cols + i];
			p.normal_y = -normalFloatBuffer[j*depth.cols + i + numPx];
			p.normal_z = -normalFloatBuffer[j*depth.cols + i + numPx * 2];
			
			if (depth.at<uint16_t>(j, i) == 0 && !(p.getNormalVector3fMap().squaredNorm() > 0.5) || depth.at<uint16_t>(j, i) / 5000.f > maxDist){
				p.getVector3fMap() << qnan, qnan, qnan;
				p.getNormalVector3fMap() << qnan, qnan, qnan;
			}
			else{
				if (bilateralFilter)
				{
					p.z = depthFloatBuffer[j*depth.cols + i];
				}
				else{
					p.z = depth.at<uint16_t>(j, i) / 5000.f;
				}
				d->unproject(i, j, p.z, p.x, p.y);
			}

			p.r = rgb.at<cv::Vec3b>(j, i)[2];
			p.g = rgb.at<cv::Vec3b>(j, i)[1];
			p.b = rgb.at<cv::Vec3b>(j, i)[0];

			p.curvature = d->sigmaHat(i, j, p.z,0, p.getNormalVector3fMap().dot(- p.getVector3fMap().normalized()));//noise predicted somewhat using the intensity..
		}
	}
	return true;
}

SintelReader::SintelReader(const std::string & folder) :
currentCloud(new explainMe::Cloud())
{
	std::cout << "Loading Bonn type of files..\n";
	std::string calibFile;
	listFiles(folder);
	std::cout << "Loaded " << numberOfFrames() << "filename pairs.\n";

	datafolder = folder;
	//uses an asus vtion pro live camera.
	d = new SintelCamera();
	cuda = new cudaTool(d->descriptionData());

	currentCloud->resize(d->width()* d->height());
	currentCloud->width = d->width();
	currentCloud->height = d->height();

}

SintelReader::~SintelReader(){
	delete cuda;
	delete d;
}

#include "fileReader.h"
#include <algorithm>
void SintelReader::listFiles(const std::string & folder)
{
	//str_compare("blabla_bla_100.png","blabla_bla_9.png");
	std::stringstream ss;
	//ss << basedepth << ".*\\.png";
	fileReader::listFiles(folder, std::string("framedepth_.*\\.png"), d_files);

	std::stringstream ss2;
	fileReader::listFiles(folder, std::string("frame_.*\\.png"), rgb_files);


	std::sort(d_files.begin(), d_files.end());
	std::sort(rgb_files.begin(), rgb_files.end());
}

bool SintelReader::loadFrame(int frame, bool cleanUp, float maxDist, bool bilateralFilter, float sig_z)
{
	if (frame >= numberOfFrames()){
		return false;
	}

	std::cout << "loading frame " << frame << "\n";
	std::cout << "names " << d_files[frame] << " \t" << rgb_files[frame] << "\n";
	cv::Mat depth = cv::imread(datafolder + "/" + d_files[frame], CV_LOAD_IMAGE_ANYDEPTH);
	cv::Mat rgb = cv::imread(datafolder + "/" + rgb_files[frame]);

	//compute normals...
	int width = depth.cols;
	int numPx = depth.cols*depth.rows;
	std::cout << "numPx " << numPx << "\n";
	//ensure there is enough space allocated
	depthFloatBuffer.resize(numPx);
	normalFloatBuffer.resize(numPx * 3);

	for (int i = 0; i < depth.cols; i++) for (int j = 0; j< depth.rows; j++){
		depthFloatBuffer[j*depth.cols + i] = depth.at<uint16_t>(j, i) / 1000.f;
	}
	cuda->dev_setData(&depthFloatBuffer[0]);
	
	if (bilateralFilter){
		cuda->bilateralFilter(3, sig_z);
		cuda->dev_getData(&depthFloatBuffer[0]);
	}
	else{
		cuda->bilateralFilter();
	}
	cuda->computeNormals(false);
	cuda->dev_getNormals(&normalFloatBuffer[0]);

	float qnan = std::numeric_limits<float>::quiet_NaN();
	//std::cout << "Depth: " << depth.cols << " " << depth.rows << "\n";
	//std::cout << "Col: " << rgb.cols << " " << rgb.rows << "\n";
	//std::cout << "Detph has opencv type " << depth.type()<< "\n";
	//std::cout << CV_8UC3 << "\n";
	//std::cout << "Color has opencv type " << rgb.type() << "\n";
	for (int i = 0; i < depth.cols; i++){

		for (int j = 0; j< depth.rows; j++){

			auto & p = currentCloud->at(i, j);
			p.normal_x = normalFloatBuffer[j*depth.cols + i];
			p.normal_y = normalFloatBuffer[j*depth.cols + i + numPx];
			p.normal_z = normalFloatBuffer[j*depth.cols + i + numPx * 2];

			if (depth.at<uint16_t>(j, i) == 0 || !(p.getNormalVector3fMap().squaredNorm() > 0.5) || depth.at<uint16_t>(j, i) / 1000.f > maxDist){
				p.getVector3fMap() << qnan, qnan, qnan;
				p.getNormalVector3fMap() << qnan, qnan, qnan;
			}
			else{
				if (bilateralFilter)
				{
					p.z = depthFloatBuffer[j*depth.cols + i];
				}
				else{
					p.z = depth.at<uint16_t>(j, i) / 1000.f;
				}
				d->unproject(i, j, p.z, p.x, p.y);
			}

			p.r = rgb.at<cv::Vec3b>(j, i)[2];
			p.g = rgb.at<cv::Vec3b>(j, i)[1];
			p.b = rgb.at<cv::Vec3b>(j, i)[0];

			p.curvature = d->sigmaHat(i, j, p.z, 0, p.getNormalVector3fMap().dot(-p.getVector3fMap().normalized()));
		}
	}
	return true;
}

std::vector<std::string> SintelReader::fileNames(int frame){
	std::vector<std::string> files;
	files.push_back(datafolder + "/" + rgb_files[frame]);
	files.push_back(datafolder + "/" + d_files[frame]);
	std::cout << frame;
	return files;
}


Kin2PNGReader::Kin2PNGReader(const std::string & folder) :
currentCloud(new explainMe::Cloud())
{
	std::cout << "Loading Bonn type of files..\n";
	std::string calibFile;
	listFiles(folder);
	std::cout << "Loaded " << numberOfFrames() << "filename pairs.\n";

	datafolder = folder;
	//uses an asus vtion pro live camera.
	d = new kinectV2Description();//new RangeSensorDescription(sensor_file/**desc*/, RangeSensorDescription::KIN2);
	d->loadFromFileKin2(folder +"test_.kin2");

	cuda = new cudaTool(d->descriptionData());

	currentCloud->resize(d->width()* d->height());
	currentCloud->width = d->width();
	currentCloud->height = d->height();

}

Kin2PNGReader::~Kin2PNGReader(){
	delete cuda;
	delete d;
}


void Kin2PNGReader::listFiles(const std::string & folder)
{
	//str_compare("blabla_bla_100.png","blabla_bla_9.png");
	std::stringstream ss;
	//ss << basedepth << ".*\\.png";
	fileReader::listFiles(folder, std::string("depth_.*\\.png"), d_files);

	std::stringstream ss2;
	fileReader::listFiles(folder, std::string("ir_.*\\.png"), ir_files);


	std::sort(d_files.begin(), d_files.end());
	std::sort(ir_files.begin(), ir_files.end());
}

bool Kin2PNGReader::loadFrame(int frame, bool cleanUp, float maxDist, bool bilateralFilter, float sig_z)
{
	if (frame >= numberOfFrames()){
		return false;
	}

	std::cout << "loading frame " << frame << "\n";
	std::cout << "names " << d_files[frame] << " \t" << ir_files[frame] << "\n";
	cv::Mat depth = cv::imread(datafolder + "/" + d_files[frame], CV_LOAD_IMAGE_ANYDEPTH);
	cv::Mat ir = cv::imread(datafolder + "/" + ir_files[frame], CV_LOAD_IMAGE_ANYDEPTH);

	//compute normals...
	int width = depth.cols;
	int numPx = depth.cols*depth.rows;
	std::cout << "numPx " << numPx << "\n";
	//ensure there is enough space allocated
	depthFloatBuffer.resize(numPx);
	normalFloatBuffer.resize(numPx * 3);

	for (int i = 0; i < depth.cols; i++) for (int j = 0; j< depth.rows; j++){
		depthFloatBuffer[j*depth.cols + i] = depth.at<uint16_t>(j, i) / 1000.f;
	}
	cuda->dev_setData(&depthFloatBuffer[0]);

	if (bilateralFilter){
		cuda->bilateralFilter(3, sig_z);
		cuda->dev_getData(&depthFloatBuffer[0]);
	}
	else{
		cuda->bilateralFilter();
	}
	cuda->computeNormals(false);
	cuda->dev_getNormals(&normalFloatBuffer[0]);

	float qnan = std::numeric_limits<float>::quiet_NaN();
	//std::cout << "Depth: " << depth.cols << " " << depth.rows << "\n";
	//std::cout << "Col: " << rgb.cols << " " << rgb.rows << "\n";
	//std::cout << "Detph has opencv type " << depth.type()<< "\n";
	//std::cout << CV_8UC3 << "\n";
	//std::cout << "Color has opencv type " << rgb.type() << "\n";
	for (int i = 0; i < depth.cols; i++){

		for (int j = 0; j< depth.rows; j++){

			auto & p = currentCloud->at(i, j);
			p.normal_x = normalFloatBuffer[j*depth.cols + i];
			p.normal_y = normalFloatBuffer[j*depth.cols + i + numPx];
			p.normal_z = normalFloatBuffer[j*depth.cols + i + numPx * 2];

			if (depth.at<uint16_t>(j, i) == 0 || !(p.getNormalVector3fMap().squaredNorm() > 0.5) || depth.at<uint16_t>(j, i) / 1000.f > maxDist){
				p.getVector3fMap() << qnan, qnan, qnan;
				p.getNormalVector3fMap() << qnan, qnan, qnan;
			}
			else{
				if (bilateralFilter)
				{
					p.z = depthFloatBuffer[j*depth.cols + i];
				}
				else{
					p.z = depth.at<uint16_t>(j, i) / 1000.f;
				}
				d->unproject(i, j, p.z, p.x, p.y);
			}
			uint8_t intensity = std::max(std::min(std::log(static_cast<float>(ir.at<uint16_t>(j,i))) / std::log(2) * 16 - 128, 255.0), 0.0);
			p.r = intensity;
			p.g = intensity;
			p.b = intensity;

			p.curvature = d->sigmaHat(i, j, p.z, ir.at<uint16_t>(j, i), p.getNormalVector3fMap().dot(-p.getVector3fMap().normalized()));
		}
	}
	return true;
}

std::vector<std::string> Kin2PNGReader::fileNames(int frame){
	std::vector<std::string> files;
	files.push_back(datafolder + "/" + ir_files[frame]);
	files.push_back(datafolder + "/" + d_files[frame]);
	std::cout << frame;
	return files;
}