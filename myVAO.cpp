
#include "myVAO.h"
#include "glDebugging.h"

#ifdef _MSC_VER
#define GL_PREFIX
#else
#define GL_PREFIX fun->
#endif


myVAO::myVAO(void)
#ifndef _MSC_VER
	: fun(new QOpenGLFunctions_3_3_Core)
#endif
{
#ifdef _MSC_VER
	glGenVertexArrays = 0;
	glBindVertexArray = 0;
	glDeleteVertexArrays = 0;
#else
	fun->initializeOpenGLFunctions();
#endif
	created = false;
}


myVAO::~myVAO(void)
{

	if(created){
		GL_PREFIX glDeleteVertexArrays(1, &vao);
		//glDebuggingStuff::didIDoWrong();
	}
}

void myVAO::create()
{
	if(!created){
#ifdef _MSC_VER
		glGenVertexArrays = (GL_GEN_VARRAYS) wglGetProcAddress("glGenVertexArrays");
		glBindVertexArray = (GL_BIND_VARRAY) wglGetProcAddress("glBindVertexArray");
		glDeleteVertexArrays = (GL_DELETE_VARRAY) wglGetProcAddress("glDeleteVertexArrays");
#endif
		GL_PREFIX glGenVertexArrays(1, &vao);
		glDebugging::didIDoWrong();
		created = true;
	}

}

void myVAO::bind()
{
	if(!created){
		create();
	}
	GL_PREFIX glBindVertexArray(vao);
	glDebugging::didIDoWrong();
}

void myVAO::release()
{
	if(created){
		GL_PREFIX glBindVertexArray(0);
	}
	glDebugging::didIDoWrong();
}

bool myVAO::isCreated()
{
	return created;
}
