#pragma once
#include "Initializer.h"


/**
*
*working "well enough"
* Alternative would be a more classic trajectory clustering approach.
*/
class InitByEnergyMinimization:
	public ComputeWeights
{
public:
	enum e_type {POINT_TO_POINT, POINT_TO_PLANE};
private:
	int numIt; float p1; float p2; float stepSize; int numHyp;
	explainMe::MatrixXf_rm d_dw_E;

	int numClouds;

	e_type energyType;

public:
	
	InitByEnergyMinimization(
		int numIt, 
		float p1, float p2, 
		float stepSize, 
		int numHyp, int numClouds,
		e_type type = POINT_TO_POINT);

	virtual ~InitByEnergyMinimization();

	virtual void findWeights(
		std::vector<trajectory_R> & traj_in, 
		explainMe::MatrixXf_rm & w_out);

	void getddwE(explainMe::MatrixXf_rm & target){
		target = d_dw_E;
	}

private:
	void compute_d_dw_E_w_pow_p_fast_incomplete_anyFrame(std::vector<trajectory_R> & paths,
											explainMe::MatrixXf_rm &w,
											//T[i_f][i_w]
											std::vector<std::vector<Eigen::Matrix4f>> &T,
											int numClouds, int reference_frame,	
											explainMe::MatrixXf_rm & d_dw_E,
											float p);

};
