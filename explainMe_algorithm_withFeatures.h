#pragma once
#include "explainMeDefinitions.h"
//#include <Windows.h>
#include "trajectory.h"
#include "CloudReader.h"

#include "Initializer.h"
#include <boost/shared_ptr.hpp>
#include "mySimpleKdTree.h"
#include "MotionHypotheses.h"
#include "CloudSampler.h"
#include "algorithmParameters.h"
#include "tuples.h"
#include <functional>
class rawDataReaderKinect2;
class pcdReader;


class explainMe_algorithm_withFeatures
{

public:
	typedef boost::shared_ptr<explainMe_algorithm_withFeatures> Ptr;
	typedef algorithmParameters Parameters;
	
	//params: 
	Parameters params;

	static bool abortComputation;

	
	typedef explainMe::MatrixXf_rm MatrixXf_rm;
	typedef Eigen::Matrix<float, 1, Eigen::Dynamic> RowVectorXf;
	//data provider
	CloudReader::Ptr reader;

	//various temp buffers
	explainMe::Cloud::Ptr c1,c2;
	Eigen::ArrayXXi segmentation_c1;
	Eigen::ArrayXXi segmentation_c2;

	// which cloud to use as world reference frame
	static const int REFERENCE_CLOUD = 0;
	
	std::vector<explainMe::Cloud::Ptr> allClouds;
	std::vector<explainMe::Cloud::Ptr> allClouds_asGrids;
	std::vector<mySimpleKdTree> allClouds_kdTree;
	std::vector<explainMe::MatrixXf_rm> denseSegmentation_allClouds;


	//actual state
	MotionHypotheses motions;
	//current trajectories
	std::vector<trajectory_R> trajectories; std::vector<std::vector<int>> N_start, N_end;
	MatrixXf_rm w; //row major: row i is the weightset of trajectory i

public:
	//std::vector<trajectory> trajectories_projected;

	explainMe_algorithm_withFeatures(CloudReader::Ptr reader_, Parameters & params);
	~explainMe_algorithm_withFeatures(void);

	static void abort(){
		abortComputation = true;
	}

	//DATA
	void loadAllClouds();
	int cloudIndex2ScannedFrameIndex(int j){
		return j*params.delta_frame + params.first_frame;
		//return (j - central_cloud)* params.delta_frame + params.central_frame;
	}
	int numClouds(){ return (int) allClouds.size();}

	void checkTrajectorySeeds();

	//Init Data from file
	void initState(std::string & segFile, std::string & motionFile);

	//datarepresentation: sparse to dense: 
	void step3_updateDenseSeg_from_sparse(bool use_seeds_only = false);

	//algorithm
	//Initial segmentation guess
	void step_1_and_2(Initializer::Ptr init_method, bool winnerTakesAll = false, stats * statistics = NULL);

	
	//algorithm:
	void step4_computeMotionHypothesesFromDenseSegmentation(
		bool updateDenseSegFromSparseSegFirst,
		bool genHypByLabelShift,
		bool genHypByCarryOver);

	
	//graphcut
	//void step5_attribute_graphcut();
	
	//datarepresentation:init sparse set
	//sample sparse set of points
	void step5_1_select_seed_points(
	std::vector<pcl::PointXYZINormal> &seed_points, 
	std::vector<Eigen::Vector3f> &seed_normals,
	std::vector<int> & seed_frame,
		CloudSampler * sampler
	);
	void step5_1_select_seed_points(
		std::vector<pcl::PointXYZINormal> &seed_points,
		std::vector<Eigen::Vector3f> &seed_normals,
		std::vector<int> & seed_frame,
		std::vector<int> & seed_point_index,
		CloudSampler * sampler
		);

	//graphcut unaries and initial seg
	//sets w to hypo_probs.
	void step5_2_initialLabeling(
		std::vector<pcl::PointXYZINormal> &seed_points, 
		std::vector<Eigen::Vector3f> &seed_normals,
		std::vector<int> & seed_cloud,
		MatrixXf_rm & hypothesis_probability,
		std::vector<int> & labeling);

	//GC unaries...
	void step5_2_compute_hypo_probs_gc_R(
		std::vector<pcl::PointXYZINormal> & seed_points,
		std::vector<int> & seed_cloud_idx,
		MatrixXf_rm & hypothesis_probability_result,
		bool winner_takes_all = true);
	
	void cast_distances_to_gc_unaries(MatrixXf_rm & hypothesis_probability, bool winner_takes_all);

	void step5_3_do_alpha_beta_expansion(
		std::vector<pcl::PointXYZINormal> &seed_points,
		std::vector<Eigen::Vector3f> &seed_normals,
		std::vector<int> & seed_cloud,
		std::vector<int> & seed_point_idx,
		MatrixXf_rm & hypothesis_probability);
	void step5_3_do_alpha_beta_expansion_one_global(
		std::vector<pcl::PointXYZINormal> &seed_points,
		std::vector<int> & seed_cloud,
		std::vector<int> & seed_point_idx,
		MatrixXf_rm & hypothesis_probability);

	//dataRepresemtation: set
	void set_sparse_w_to_MAP(MatrixXf_rm & hypothesis_probability);
	void set_sparse_w_to_MAP_update_Trajs(
		explainMe::MatrixXf_rm & hypothesis_probability,
		std::vector<pcl::PointXYZINormal> &seed_points,
		std::vector<int> & seed_cloud,
		std::vector<int> & seed_point_idx);


		
	//will refactor this...
	void computeMotionEnergy_with_reprojection(
		std::vector<pcl::PointXYZINormal> & points,
		//the index of the clouds each point was sampled from.
		std::vector<int> & point_cloudIdx,
		std::vector<Eigen::Matrix4f> & motion,
		//kdtrees containing int references to the points in the unstructured cloud
		//that are allowed to be treated as correpondences. 
		std::vector<mySimpleKdTree> & Data_label,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		//clouds, organized (as grid), to determne occlusions
		RangeSensorDescription & d,
		std::vector<explainMe::Cloud::Ptr> & allClouds_asGrids,
		bool random_sample,
		explainMe_algorithm_withFeatures::Parameters & params,
		Eigen::VectorXf & point_motionError,
		std::vector<float> & total_observation_weight);

		//will fuse fuseable labels and updatate the matrix segmentation accordingly. thr is the maximal allowed fraction
		//of per point unary cost increase. Both matrices are supposed to be per sample, i.e. global but sparse.
		static void step_5_3_5_fuseClosebyHypotheses(MatrixXf_rm & segmentation, MatrixXf_rm & dataterm, float thr = 0.02f);
		void step_5_3_7_addDenseHyps(std::vector<std::vector<std::vector<float>>> & hyps);
		//threshold = -1 : use a std value of #trajectories*0.005
		void step5_4_remove_spurious_hypos(float threshold = -1);

	
	//float currentError();
	float computeTotalEnergy();
	

	void interpolateWeights_onCloud(int cloudIdx,  MatrixXf_rm & interpolated_weights,
		MatrixXf_rm & w_per_traj_to_interpolate,bool useSeedsOnly = false);


	void getSegmentedOrganizedCloudsAndAlignments(int hyp, 
		std::vector<explainMe::Cloud::Ptr> & clouds, 
		std::vector<Eigen::Matrix4f> & alignments);

	void getSegmentedOrganizedCloudAndAlignment(int hyp, 
		int cld_idx,
		explainMe::Cloud::Ptr & cloud, 
		Eigen::Matrix4f & alignments);


private:
	void interpolateWeights_helper(explainMe::Cloud::Ptr cloud, 
		MatrixXf_rm & interpolated_weights,
		MatrixXf_rm & w_to_interpolate,
		int cloud_idx, bool useSeedsOnly = false);

};

