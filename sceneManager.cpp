#include "sceneManager.h"
#include "glPointCloud.h"
#include "glCorrespondences.h"
#include "glTriMesh.h"

/*#include "glExampleTriangles.h"
#include "glInstance.h"
#include "glSurflets.h"
#include "glSurfelVariance.h"
#include "glEnhancedSurfelModel.h"*/

sceneManager::sceneManager(void)
{
	eye.setX(0); eye.setY(0), eye.setZ(0);
	lookAt.setX(0);lookAt.setY(0);lookAt.setZ(1);

	cam.setToIdentity();
	cam.lookAt(eye, lookAt, QVector3D(0,-1,0));
	
//	displayables.push_back(new glInstance(new glExampleTriangles()));
}


sceneManager::~sceneManager(void)
{
	for(int i =0; i < displayables.size(); i++){
		delete displayables[i];
	}
}

void sceneManager::hideAll(){
	for (auto disp : displayables_by_name)
	{
		disp.second->setHidden(true);
	}
}

void sceneManager::setVisible(const std::string & name, bool what){
	auto cl = displayables_by_name.find(name);
	if (cl != displayables_by_name.end()){
		displayables_by_name[name]->setHidden(!what);
	}
}

void sceneManager::updateCam(QVector3D eye_, QVector3D lookAt_){
	eye = eye_; lookAt = lookAt_;
	cam.setToIdentity();
	cam.lookAt(eye, lookAt, QVector3D(0,-1,0));
}

void sceneManager::setCam(QMatrix4x4 cameraTransform){
	cam = cameraTransform;
}

QMatrix4x4 & sceneManager::getCam()
{
	return this->cam;
}

int sceneManager::numberDisplayables()
{
	return this->displayables.size();
}

glDisplayable * sceneManager::get(int i)
{
	return displayables[i];
}

glDisplayable * sceneManager::get(std::string name)
{
	auto cl = displayables_by_name.find(name);
	if(cl == displayables_by_name.end()){
		return NULL;
	}
	return displayables_by_name[name];
}

void sceneManager::displayables_push_back(std::string param1, glInstance * instance){
	displayables.push_back(instance);

	auto cl = displayables_by_name.find(param1);
	if(cl != displayables_by_name.end()){
		std::cout << "\nWARNING: multiple displayables have the same name! (" <<param1 << ") \n";
	}
	displayables_by_name[param1] = instance;

	if(displayables.size() > 1){
		QMatrix4x4 instance_2_world = displayables[0]->getInstance2World();
		instance->setInstance2World(instance_2_world);
	}

}

void sceneManager::addAndManage(glDisplayable * toDisplay)
{
	glInstance * inst = new glInstance(toDisplay);

	displayables.push_back(inst);
	if(displayables.size() > 1){
		QMatrix4x4 instance_2_world = displayables[0]->getInstance2World();
		inst->setInstance2World(instance_2_world);
	}
}


void sceneManager::onRotate(QMatrix4x4 & mat)
{
	for(int i = 0; i< displayables.size(); i++){
		displayables[i]->mult(mat);
	}
}


void sceneManager::setPosition(std::string displayable_name, QMatrix4x4 & mat){
	auto cl = displayables_by_name.find(displayable_name);
	if (cl != displayables_by_name.end()){
		displayables_by_name[displayable_name]->setInstance2World(mat);
	}
	
}

void sceneManager::resetAllObject2WorldMatrices(){
	for (int i = 0; i< displayables.size(); i++){
		QMatrix4x4 id;
		id.setToIdentity();
		displayables[i]->setInstance2World(id);
	}
}

void sceneManager::onCamMovement( QMatrix4x4 & mat)
{
	cam= mat * cam;
}


void sceneManager::updateCloud( std::string param1,std::vector<Eigen::Vector3f> & ps, std::vector<Eigen::Vector3f> & cols  )
{
	auto cl = clouds.find(param1);
	if(cl == clouds.end()){
		glPointCloud * newGlCloud = new glPointCloud(ps, cols);
		clouds[param1] = newGlCloud;
		displayables_push_back(param1, new glInstance(newGlCloud));
	}
	else{
		clouds[param1]->updateData(ps, cols);
	}
}

void sceneManager::updateCloud( std::string param1, 
							   const explainMe::Cloud::ConstPtr & cloud,
							   bool useColor )
{
	auto cl = clouds.find(param1);
	if(cl == clouds.end()){
		glPointCloud * newGlCloud = new glPointCloud(cloud, useColor);
		clouds[param1] = newGlCloud;

		displayables_push_back(param1, new glInstance(newGlCloud));
	}
	else{
		clouds[param1]->updateData(cloud, useColor);
	}
}

void sceneManager::updateCloud( std::string param1, 
							   const explainMe::Cloud::ConstPtr & cloud,
							   std::vector<Eigen::Vector3f> & cols )
{
	auto cl = clouds.find(param1);
	if(cl == clouds.end()){
		glPointCloud * newGlCloud = new glPointCloud(cloud, cols);
		clouds[param1] = newGlCloud;

		displayables_push_back(param1, new glInstance(newGlCloud));
	}
	else{
		clouds[param1]->updateData(cloud, cols);
	}
}


void sceneManager::updateCorrespondences(std::string  param1, std::vector<trajectory_R> & paths, std::vector<Eigen::Vector3f> & colors){
	auto cl = correspondences.find(param1);
	if(cl == correspondences.end()){
		glCorrespondences * glCorresp = new glCorrespondences(paths, colors);
		correspondences[param1] = glCorresp;
		displayables_push_back(param1, new glInstance(glCorresp));
	}
	else{
		correspondences[param1]->updateData(paths, colors);
	}
}

void sceneManager::updateCorrespondences( std::string param1, 
										 std::vector<pcl::PointXYZ> & cloud_a, 
										 std::vector<pcl::PointXYZ> & cloud_b )
{
	auto cl = correspondences.find(param1);
	if(cl == correspondences.end()){
		glCorrespondences * glCorresp = new glCorrespondences(cloud_a, cloud_b);
		correspondences[param1] = glCorresp;

		displayables_push_back(param1, new glInstance(glCorresp));
	}
	else{
		correspondences[param1]->updateData(cloud_a,cloud_b);
	}
}

void sceneManager::updateCorrespondences( std::string param1, 
										 std::vector<Eigen::Vector3f> & cloud_a, 
										 std::vector<Eigen::Vector3f> & cloud_b )
{
	auto cl = correspondences.find(param1);
	if(cl == correspondences.end()){
		glCorrespondences * glCorresp = new glCorrespondences(cloud_a, cloud_b);
		correspondences[param1] = glCorresp;

		displayables_push_back(param1, new glInstance(glCorresp));
	}
	else{
		correspondences[param1]->updateData(cloud_a,cloud_b);
	}
}

void sceneManager::updateCorrespondences(std::string param1,
	std::vector<Eigen::Vector3f> & cloud_a,
	std::vector<Eigen::Vector3f> & cloud_b,
	std::vector<Eigen::Vector3f> & color
	)
{
	auto cl = correspondences.find(param1);
	if (cl == correspondences.end()){
		glCorrespondences * glCorresp = new glCorrespondences(cloud_a, cloud_b, color);
		correspondences[param1] = glCorresp;

		displayables_push_back(param1, new glInstance(glCorresp));
	}
	else{
		correspondences[param1]->updateData(cloud_a, cloud_b, color);
	}
}

void sceneManager::updateTriMesh(std::string  param1,
	const explainMe::Cloud::ConstPtr & cloud,
	std::vector<int>& ind,
	std::string vshader,
	std::string fshader,
	std::string gshader,
	bool useColor)
{
	auto tm = trimeshs.find(param1);
	if (tm == trimeshs.end()){
		glTriMesh * newGlmesh = new glTriMesh(cloud, ind);
		newGlmesh->setShader(vshader, fshader, gshader);

		trimeshs[param1] = newGlmesh;
		displayables_push_back(param1, new glInstance(newGlmesh));

	}
	else{
		trimeshs[param1]->updateData(cloud, ind, useColor);
	}
}
/*void sceneManager::updateCloud( std::string param1, const explainMe::RawCloud::ConstPtr & cloud )
{
	auto cl = clouds.find(param1);
	if(cl == clouds.end()){
		glPointCloud * newGlCloud = new glPointCloud(cloud);
		clouds[param1] = newGlCloud;
		displayables_push_back(param1, new glInstance(newGlCloud));
	}
	else{
		clouds[param1]->updateData(cloud);
	}
}*/

/*void sceneManager::updateCloud( std::string param1,std::vector<pcl::PointXYZ> & ps, std::vector<pcl::PointXYZ> & cols  )
{
	auto cl = clouds.find(param1);
	if(cl == clouds.end()){
		glPointCloud * newGlCloud = new glPointCloud(ps, cols);
		clouds[param1] = newGlCloud;
		displayables_push_back(param1, new glInstance(newGlCloud));
	}
	else{
		clouds[param1]->updateData(ps, cols);
	}
}*/
/*void sceneManager::updateTriMesh( std::string  param1, 
				   const explainMe::Cloud::ConstPtr & cloud,
				   std::vector<int>& ind, 
				   bool useColor)
{
	auto tm = trimeshs.find(param1);
	if(tm == trimeshs.end()){
		glTriMesh * newGlmesh = new glTriMesh(cloud, ind);
		trimeshs[param1] = newGlmesh;
		displayables_push_back(param1, new glInstance(newGlmesh));

	}
	else{
		trimeshs[param1]->updateData(cloud, ind, useColor);
	}
}



void sceneManager::updateTriMesh(std::string  param1, 
					std::vector<Eigen::Vector3f> & pos,
					std::vector<Eigen::Vector3f> & col,
					std::vector<int> & ind)
{
	auto tm = trimeshs.find(param1);
	if(tm == trimeshs.end()){
		glTriMesh * newGlmesh = new glTriMesh(pos, col, ind);
		trimeshs[param1] = newGlmesh;
		displayables_push_back(param1, new glInstance(newGlmesh));
	}
	else{
		trimeshs[param1]->updateData(pos,col,ind);
	}
}

void sceneManager::updateSurfelCloud( std::string param1, pcl::PointCloud<Surfel>::Ptr & cloud )
{
	auto cl = surflets.find(param1);
	if(cl == surflets.end()){
		glSurflets * newGlCloud = new glSurflets(cloud);
		surflets[param1] = newGlCloud;
		
		displayables_push_back(param1, new glInstance(newGlCloud));

	}
	else{
		surflets[param1]->updateData(cloud);
	}
}

void sceneManager::updateSurfelModel(std::string  param1, enhancedSurfelModel::Ptr & model , 
		std::string vertShader, std::string fragShader, std::string geomShader)
{
	auto cl = surfelModels.find(param1);
	if(cl == surfelModels.end()){
		glEnhancedSurfelModel * newGlCloud = new glEnhancedSurfelModel(*model);
		surfelModels[param1] = newGlCloud;
		if(vertShader.size() != 0){
			newGlCloud->setShader(vertShader, fragShader, geomShader);
		}
		
		displayables_push_back(param1, new glInstance(newGlCloud));

	}
	else{
		surfelModels[param1]->updateData(*model);
	}
}

void sceneManager::updateSurfelModel(std::string  param1, enhancedSurfelModel::Ptr & model , 
		std::vector<Eigen::Matrix4f> &alignments_perObject, 
		std::string vertShader, std::string fragShader, std::string geomShader)
{
	auto cl = surfelModels.find(param1);
	if(cl == surfelModels.end()){
		glEnhancedSurfelModel * newGlCloud = new glEnhancedSurfelModel(*model, &alignments_perObject);
		surfelModels[param1] = newGlCloud;
		if(vertShader.size() != 0){
			newGlCloud->setShader(vertShader, fragShader, geomShader);
		}
		
		displayables_push_back(param1, new glInstance(newGlCloud));
	}
	else{
		surfelModels[param1]->updateData(*model, &alignments_perObject);
	}
}

void sceneManager::updateSurfelCloud( std::string param1, explainMe::SurfelCloud::Ptr & cloud ,
		std::string vertShader,  std::string fragShader, std::string geomShader )
{
	auto cl = surflets.find(param1);
	if(cl == surflets.end()){
		glSurflets * newGlCloud = new glSurflets(cloud);
		surflets[param1] = newGlCloud;
		if(vertShader.size() != 0){
			newGlCloud->setShader(vertShader, fragShader, geomShader);
		}
		
		displayables_push_back(param1, new glInstance(newGlCloud));

	}
	else{
		surflets[param1]->updateData(cloud);
	}
}

void sceneManager::updateSurfelCloud( std::string param1, std::vector<explainMe::SurfelCloud::Ptr> & clouds ,
		std::string vertShader,  std::string fragShader, std::string geomShader )
{
	auto cl = surflets.find(param1);

	if(cl == surflets.end()){
		glSurflets * newGlCloud = new glSurflets(clouds);
		surflets[param1] = newGlCloud;
		if(vertShader.size() != 0){
			newGlCloud->setShader(vertShader, fragShader, geomShader);
		}
		
		displayables_push_back(param1, new glInstance(newGlCloud));
	}
	else{
		surflets[param1]->updateData(clouds);
	}
}

void sceneManager::updateSurfelCloud( std::string param1, std::vector<enhancedSurfelCloud::Ptr> & clouds ,
		std::string vertShader,  std::string fragShader, std::string geomShader )
{
	auto cl = surflets.find(param1);

	if(cl == surflets.end()){
		glSurflets * newGlCloud = new glSurflets(clouds);
		surflets[param1] = newGlCloud;
		if(vertShader.size() != 0){
			newGlCloud->setShader(vertShader, fragShader, geomShader);
		}
		
		displayables_push_back(param1, new glInstance(newGlCloud));
	}
	else{
		surflets[param1]->updateData(clouds);
	}
}

void sceneManager::updateSurfelVariances(std::string  param1, explainMe::SurfelCloud::Ptr & cloud){
	auto cl = surfelVariances.find(param1);
	if(cl == surfelVariances.end()){
		glSurfelVariance * newGlCloud = new glSurfelVariance(cloud);
		surfelVariances[param1] = newGlCloud;
		
		displayables_push_back(param1, new glInstance(newGlCloud));

	}
	else{
		surfelVariances[param1]->updateData(cloud);
	}
}

void sceneManager::updateBBoxes( std::string param1, objectLibrary & lib )
{
	auto cl = bboxes.find(param1);
	if(cl == bboxes.end()){
		glBBoxes * newGlCloud = new glBBoxes(lib);
		bboxes[param1] = newGlCloud;
		
		displayables_push_back(param1, new glInstance(newGlCloud));

	}
	else{
		bboxes[param1]->updateBoxes(lib);
	}
}

void sceneManager::updateCorrespondences( std::string param1, 
	pcl::PointCloud<Surfel>::Ptr & cloud_a, 
	explainMe::Cloud::Ptr & cloud_b, 
	Eigen::MatrixXi & bToA )
{
	auto cl = correspondences.find(param1);
	if(cl == correspondences.end()){
		glCorrespondences * glCorresp = new glCorrespondences(cloud_a, cloud_b, bToA);
		correspondences[param1] = glCorresp;
		
		displayables_push_back(param1, new glInstance(glCorresp));

	}
	else{

	}
}



void sceneManager::updateCorrespondences(std::string  param1, std::vector<std::vector<pcl::PointXYZ>> & paths)
{
	auto cl = correspondences.find(param1);
	if(cl == correspondences.end()){
		glCorrespondences * glCorresp = new glCorrespondences(paths);
		correspondences[param1] = glCorresp;
		displayables_push_back(param1, new glInstance(glCorresp));
	}
}

void sceneManager::updateCorrespondences(std::string  param1, std::vector<std::vector<pcl::PointXYZ>> & paths, std::vector<Eigen::Vector3f> & colors){
	auto cl = correspondences.find(param1);
	if(cl == correspondences.end()){
		glCorrespondences * glCorresp = new glCorrespondences(paths, colors);
		correspondences[param1] = glCorresp;
		displayables_push_back(param1, new glInstance(glCorresp));
	}
	else{
		correspondences[param1]->updateData(paths, colors);
	}
}

void sceneManager::updateCorrespondences( std::string param1, explainMe::SurfelCloud::Ptr &cloud, std::vector<std::vector<int>> & corresp )
{
	auto cl = correspondences.find(param1);
	if(cl == correspondences.end()){
		glCorrespondences * glCorresp = new glCorrespondences(cloud, corresp);
		correspondences[param1] = glCorresp;
		
		displayables_push_back(param1, new glInstance(glCorresp));
	}
	else{
	}
}*/


