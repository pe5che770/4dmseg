#include <pcl/io/pcd_io.h>
#include <opencv2/opencv.hpp>

void
readPoseFile (const std::string &path,
              Eigen::Matrix4f &mat)
{
  std::ifstream file (path.c_str ());
  for (size_t i = 0; i < 4; ++i)
    for (size_t j = 0; j < 4; ++j)
      file >> mat (i, j);

  file.close ();
}

int
main (int argc,
      char **argv)
{
  std::vector<std::string> pcd_files;
  std::vector<std::string> pose_files;
  std::string dir = argv[1];

  boost::filesystem::directory_iterator end_itr;
  for (boost::filesystem::directory_iterator itr (dir); itr != end_itr; ++itr)
  {
    std::string extension = boost::algorithm::to_upper_copy
        (boost::filesystem::extension (itr->path ()));
    std::string basename = boost::filesystem::basename (itr->path ());
    std::string pathname = itr->path ().string ();
    if (extension == ".pcd" || extension == ".PCD")
    {
      pcd_files.push_back (pathname);
    }
    else if (extension == ".txt" || extension == ".TXT")
    {
      pose_files.push_back (pathname);
    }
  }
  std::sort (pcd_files.begin (), pcd_files.end ());
  std::sort (pose_files.begin (), pose_files.end ());


  std::string out_dir = argv[2];
  boost::filesystem::create_directories (out_dir);
  boost::filesystem::create_directories (out_dir + "/depth/");
  boost::filesystem::create_directories (out_dir + "/color");


  /// Put all the pointclouds together
  pcl::PointCloud<pcl::PointXYZRGBA> cloud_acc;
  std::ofstream txt_file ((out_dir + "/associate.txt").c_str ());
  for (size_t c_i = 0; c_i < pcd_files.size (); ++c_i)
  {
    std::cerr << "Reading cloud from " << pcd_files[c_i] << "\n   and pose from " << pose_files[c_i] << std::endl;
    Eigen::Matrix4f mat;
    readPoseFile (pose_files[c_i], mat);
    Eigen::Matrix4f mat_inv = mat.inverse ();
    Eigen::Affine3f tr (mat_inv);

    pcl::PointCloud<pcl::PointXYZRGBA> cloud;
    pcl::io::loadPCDFile (pcd_files[c_i], cloud);

    for (size_t v_i = 0; v_i < cloud.points.size (); ++v_i)
    {
      pcl::PointXYZRGBA p = cloud.points[v_i];
      p.getVector3fMap () = tr * cloud.points[v_i].getVector3fMap ();

      cloud_acc.push_back (p);
    }




    /// Save the data in the fastfusion format
    cv::Mat img_depth (cloud.height, cloud.width, CV_16UC1);
    cv::Mat img_color (cloud.height, cloud.width, CV_8UC3);
    for (size_t y = 0; y < cloud.height; ++y)
      for (size_t x = 0; x < cloud.width; ++x)
      {
//        img_depth.at<unsigned short> (cv::Point (cloud.width - 1 - x, y)) = cloud.points[y * cloud.width + x].z * 1000.;
//        img_color.at<cv::Vec3b> (cv::Point (cloud.width - 1 - x, y)) = cv::Vec3b (cloud.points[y * cloud.width + x].b,
//                                                                                  cloud.points[y * cloud.width + x].g,
//                                                                                  cloud.points[y * cloud.width + x].r);
        img_depth.at<unsigned short> (cv::Point (x, y)) = cloud.points[y * cloud.width + x].z * 1000.;
        img_color.at<cv::Vec3b> (cv::Point (x, y)) = cv::Vec3b (cloud.points[y * cloud.width + x].b,
            cloud.points[y * cloud.width + x].g,
            cloud.points[y * cloud.width + x].r);
      }

    char path_depth[512], path_color[512];
    sprintf (path_depth, "depth/depth_%05ld.png", c_i);
    sprintf (path_color, "color/color_%05ld.png", c_i);

    cv::imwrite (out_dir + "/" + path_depth, img_depth);
    cv::imwrite (out_dir + "/" + path_color, img_color);


    Eigen::Quaternionf quat (mat_inv.block<3, 3> (0, 0));
    quat.normalize ();
    Eigen::Vector3f trans (mat_inv.block<3, 1> (0, 3));
    txt_file << c_i << " " <<
                trans (0) << " " << trans (1) << " " << trans (2) << " " <<
                quat.x () << " " << quat.y () << " " << quat.z () << " " << quat.w () << " " <<
                c_i << " " << path_depth << " " << c_i << " " <<  path_color << std::endl;
  }
  txt_file.close ();

  pcl::io::savePCDFileBinary ("cloud_acc.pcd", cloud_acc);


  return (0);
}
