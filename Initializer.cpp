#include "Initializer.h"
#include <fstream>
#include <boost/lexical_cast.hpp>
#include <string>
#include <boost/tokenizer.hpp>


bool Initializer::abortInit = false;
bool ComputeWeights::abortComp = false;

Initializer::Initializer(void)
{
}


Initializer::~Initializer(void)
{
}


InitializeTrajectories::InitializeTrajectories(){}

InitializeTrajectories::~InitializeTrajectories(){}

void InitializeTrajectories::filterTrajectories(
		std::vector<trajectory_R> & paths,
		float topacceleration)
{
	
	Eigen::Vector3f zero = Eigen::Vector3f::Zero();
	bool valid = true;
	for(int i = 0, sz =paths.size(); i < sz; i++){

		if(paths[i].lastCloud() - paths[i].seedCloud() ==1){
			//ignore short paths.
			paths[i].removeAllAfter(paths[i].seedCloud());
		}
		//fwdfiltering
		for(int j = paths[i].seedCloud(), lastFrm = paths[i].lastCloud(); j+2 <= lastFrm; j++){
			// compute second derivative of total displacement distance
			valid = ((j+2 <= lastFrm ? (paths[i][j+2].getVector3fMap() - paths[i][j+1].getVector3fMap()): zero)
				-(paths[i][j+1].getVector3fMap() - paths[i][j].getVector3fMap())).norm() < topacceleration;

			if(!valid){
				paths[i].removeAllAfter(j);
				break;
			}
				
		}


		if (paths[i].seedCloud() - paths[i].firstCloud() == 1){
			//ignore short paths.
			paths[i].removeAllBefore(paths[i].seedCloud());
		}

		//bwdfiltering
		for (int j = paths[i].seedCloud(), firstFrm = paths[i].firstCloud(); j - 2 >= firstFrm; j--){
			// compute second derivative of total displacement distance
			valid = ((j-2 >= firstFrm ? (paths[i][j-2].getVector3fMap() - paths[i][j-1].getVector3fMap()): zero)
				-(paths[i][j-1].getVector3fMap() - paths[i][j].getVector3fMap())).norm() < topacceleration;

			if(!valid){
				paths[i].removeAllBefore(j);
				break;
			}

		}
	}
}

#include "Chronist.h"

void FromFileInitializer::computeInitialisation(std::vector<explainMe::Cloud::Ptr> & clouds_as_grids, 
		RangeSensorDescription & description,
		std::vector<trajectory_R> & target_traj, explainMe::MatrixXf_rm & target_seg_weights,
		stats * statistics)
	{
		if (statistics != NULL){
			statistics->total.restart();
			(*statistics)["Load State"].restart();
		}
		Chronist::load(file,target_traj,target_seg_weights);
		if (statistics != NULL){
			statistics->total.stop();
			(*statistics)["Load State"].stop();
		}
	}