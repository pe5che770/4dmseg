#include "BundleAdjustmentV2.h"
#include "CloudTools.h"
#include "nnByProjection.h"
//#include "glOffscreenRenderer.h"
#include "opencv2/opencv.hpp"
#include <fstream>

template<typename Eigen_T_weights>
BundleAdjustmentV2<Eigen_T_weights>::BundleAdjustmentV2(RangeSensorDescription * d)
{
/*	if (d != NULL){
		offscreenRenderer = new glOffscreenRenderer(d, false, "Resources/trimesh_render_indices.vert", "Resources/trimesh_render_indices_one_based.frag", "Resources/trimesh_render_indices.geom");
	}
	else{
		offscreenRenderer = NULL;
	}*/
	//render on screen.
	//offscreenRenderer = new glOffscreenRenderer(d, true, "Resources/trimesh_render_indices.vert", "Resources/trimesh_render_indices_to_screen.frag", "Resources/trimesh_render_indices.geom");

}

template<typename Eigen_T_weights>
BundleAdjustmentV2<Eigen_T_weights>::~BundleAdjustmentV2()
{
	/*if (offscreenRenderer != NULL){
		delete offscreenRenderer;
	}*/
}


std::vector<Eigen::Matrix4f> computeInvs(
	std::vector<Eigen::Matrix4f> & alignments)
{
	std::vector<Eigen::Matrix4f> a_inv;
	for (int i = 0; i < alignments.size(); i++){
		//apply transf -1
		Eigen::Matrix4f a_inv_;
		a_inv_ = alignments[i].inverse();
		a_inv.push_back(a_inv_);
	}
	return a_inv;
}

template<typename Eigen_T_weights>
std::vector<mySimpleKdTree_ndim<6>> BundleAdjustmentV2<Eigen_T_weights>::buildAllTrees(
	params &	_params,
	data &		_data)
{
	Vector6f scales; 
	scales << 1, 1, 1, _params.alpha_color, _params.alpha_color, _params.alpha_color;
	std::vector<mySimpleKdTree_ndim<6>> allTrees(_data.clouds.size(), mySimpleKdTree_ndim<6>(scales));
	const int nClouds = _data.clouds.size();
	#pragma omp parallel
	{
		#pragma omp for
		for (int i = 0; i <nClouds; i++)
		{
			simpleTreeTools::setData<explainMe::Cloud::Ptr>(allTrees[i], _data.clouds[i]);
			allTrees[i].buildTree();
		}
	}
	return allTrees;
}


template<typename Eigen_T_weights>
void BundleAdjustmentV2<Eigen_T_weights>::addConstraintToTotal(
	Matrix6f & ccT, 
	Vector6f & b, 
	int i, int j, 
	int referenceCloud, 
	Eigen::MatrixXf & total, 
	Eigen::VectorXf & total_b)
{
	int blocki = (i > referenceCloud ? i - 1 : i);//parameter block
	int blockj = (j > referenceCloud ? j - 1 : j);

	if (i == referenceCloud){
		total.block<6, 6>(blockj * 6, blockj * 6) += ccT;
		total_b.block<6, 1>(blockj * 6, 0) -= b;
	}
	if (j == referenceCloud){
		total.block<6, 6>(blocki * 6, blocki * 6) += ccT;
		total_b.block<6, 1>(blocki * 6, 0) += b;
	}
	else{
		total.block<6, 6>(blocki * 6, blocki * 6) += ccT;
		total.block<6, 6>(blocki * 6, blockj * 6) -= ccT;
		total.block<6, 6>(blockj * 6, blocki * 6) -= ccT;
		total.block<6, 6>(blockj * 6, blockj * 6) += ccT;

		total_b.block<6, 1>(blocki * 6, 0) += b;
		total_b.block<6, 1>(blockj * 6, 0) -= b;
	}
}


//returns true if the constraint was not ignored.
template<typename Eigen_T_weights>
template<typename NNFinder_cloud_i>
bool BundleAdjustmentV2<Eigen_T_weights>::pairwiseHessianAndB(
	explainMe::Cloud::Ptr cloud_i,
	explainMe::Cloud::Ptr cloud_j,
	Eigen_T_weights & weights_i,
	Eigen_T_weights & weights_j,
	NNFinder_cloud_i & nnFinder_i,
	Eigen::Matrix4f & a_i, Eigen::Matrix4f & a_inv_i,
	Eigen::Matrix4f & a_j, Eigen::Matrix4f & a_inv_j,
	params & _params, 
	int weight_idx,
	Eigen::Vector4f & centroid,
	bool possiblyIgnoreConstraint,
	Matrix6f &ccT_total,
	Vector6f & b_total,
	float & error_new_alginments,
	bool verbose)
{
	//points in cloud j need to be mapped to the coordinate frame of i for the nn search
	//that is a[i]*a_inv[j]
	Eigen::Matrix4f j_to_i = a_i * a_inv_j;
	const int clouds_j_size = cloud_j->size();
	explainMe::Point p, q;
	Matrix6f ccT;
	Vector6f b;
	ccT.setZero();
	b.setZero();


	//use maximally points with weights summing up to 2500.
	int ith = std::ceil(weights_j.col(weight_idx).sum() / 2500 + 0.1);
	float total_w_with_corresp = 0, total_w_without_corresp = 0, error = 0, numCorresp = 0;

#pragma omp parallel private(p,q) num_threads(20)//20
	{
		Matrix6f ccT_, ccT_p2p; Vector6f b_, b_p2p; ccT_.fill(0); b_.fill(0);
		Vector6f c; int idx;
		float w = 0, error_ = 0;
		float total_w_corresp_ = 0, total_w_no_corresp_ = 0, numCorresp_ = 0;
		//simpleNNFinder_ndim<6> nnFinder(myTree);
		NNFinder_cloud_i nnFinder = nnFinder_i;
#pragma omp for nowait
		for (int q_idx = 0; q_idx < clouds_j_size; q_idx++)
		{
			//ignore zero weight points:
			if (weights_j(q_idx, weight_idx) < 1e-3f){
				continue;
			}
			//subsampling
			if (!((q_idx /*+ rand()*/) % ith == 0)){
				continue;
			}

			//map q t o ps frame and find the nn
			q = cloud_j->at(q_idx);//*qit;
			q.getVector4fMap() = j_to_i * q.getVector4fMap();
			idx = nnFinder.findNN(simpleTreeTools::p(q.x, q.y, q.z, q.r, q.g, q.b), _params.culling_dist*_params.culling_dist).index;
			if (idx >= 0){
				//valid
				p = cloud_i->at(idx);

				//reset q
				q = cloud_j->at(q_idx);//*qit;
				//now p and q are found, but need to be mapped such that they can be used to compute an increment of the alignments
				//i.e. map to reference cloud and minus centroid.
				p.getVector4fMap() = a_inv_i * p.getVector4fMap() - centroid; p.getNormalVector3fMap() = a_inv_i.topLeftCorner<3, 3>() *p.getNormalVector3fMap();
				q.getVector4fMap() = a_inv_j * q.getVector4fMap() - centroid; q.getNormalVector3fMap() = a_inv_j.topLeftCorner<3, 3>() *q.getNormalVector3fMap();


				//weight/culling
				w = p.getNormalVector3fMap().dot(q.getNormalVector3fMap()); 
				w = (w < 0.5 ? 0 : w*w); w = (w * 0 == 0 ? w : 0.5);
				//w = (w< 0.7071 ? 0 : w); w = (w * 0 == 0 ? w : 0.5);
				w *= (1 - (q.getVector3fMap() - p.getVector3fMap()).norm() / _params.culling_dist);	w = w<0 ? 0 : w;

				//daring
				w *= std::max<float>(weights_i(idx, weight_idx), weights_j(q_idx, weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);
				//conservative:
				//w*= std::min<float>(weights[i](idx[0],weight_idx) , weights[j](q_idx,weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);

				//the point to plane equation
				c << p.getVector3fMap().cross(q.getNormalVector3fMap()), q.getNormalVector3fMap();
				ccT_ += c*c.transpose()*w*_params.alpha_point2plane;
				b_ += c*(p.getVector3fMap() - q.getVector3fMap()).dot(q.getNormalVector3fMap())*w*_params.alpha_point2plane;
				error_ += std::pow((p.getVector3fMap() - q.getVector3fMap()).dot(q.getNormalVector3fMap()),2)*w*_params.alpha_point2plane;


				//point to point...
				b_p2p << q.getVector3fMap().cross(p.getVector3fMap() - q.getVector3fMap()), p.getVector3fMap() - q.getVector3fMap();
				ccT_p2p << q.y*q.y + q.z*q.z, -q.x*q.y, -q.x*q.z, 0, -q.z, q.y,
					-q.y*q.x, q.x*q.x + q.z*q.z, -q.y*q.z, q.z, 0, -q.x,
					-q.z*q.x, -q.z*q.y, q.x*q.x + q.y*q.y, -q.y, q.x, 0,
					0, q.z, -q.y, 1, 0, 0,
					-q.z, 0, q.x, 0, 1, 0,
					q.y, -q.x, 0, 0, 0, 1;

				ccT_ += ccT_p2p * w * _params.alpha_point2point;
				b_ += b_p2p * w * _params.alpha_point2point;//*/
				error_ += (p.getVector3fMap() - q.getVector3fMap()).squaredNorm() * w * _params.alpha_point2point;

				total_w_corresp_ += w;
				numCorresp_ += weights_j(q_idx, weight_idx);

				if (ccT_.norm() * 0 != 0 || b_.norm() * 0 != 0){
					std::cout << "Bundle Adjustment: NANs !\n";
					std::cout << p << "\n" << q << "\n";
				}
			}
			else{
				w = weights_j(q_idx, weight_idx);
				total_w_no_corresp_ +=w;
			}

		}

#pragma omp critical
					{
						ccT += ccT_;
						b += b_;
						error += error_;
						total_w_with_corresp += total_w_corresp_;
						total_w_without_corresp += total_w_no_corresp_;
						numCorresp += numCorresp_;
					}
	}//end omp parallel (nn and pairwise matrix computation)
	//std::cout << total_w_with_corresp << " vs "<< total_w_without_corresp << "|";

	//if (total_w_with_corresp < 0.5*total_w_without_corresp && possiblyIgnoreConstraint){
	if (total_w_with_corresp < 0.1*numCorresp && possiblyIgnoreConstraint){
		if(verbose) 
			std::cout << "Ign\t";
		error_new_alginments += (total_w_with_corresp + total_w_without_corresp) / (total_w_with_corresp + 1)* error;
		return false;
	}
	else if (total_w_with_corresp <  200 || total_w_with_corresp < 2*total_w_without_corresp || total_w_with_corresp < 0.5*numCorresp){
	//else if (total_w_with_corresp <  200 || (total_w_with_corresp < 0.5*numCorresp && total_w_with_corresp <0.25* total_w_without_corresp)){
		//try to align to it, but not well aligned at all.
		error_new_alginments += (total_w_with_corresp + total_w_without_corresp) / (total_w_with_corresp + 1)* error;
		ccT_total += ccT;
		b_total += b;
		if (verbose) std::cout << "semi\t";//enforcing. (" << numCorresp << ")";
		return false;
	}
	else{
		error_new_alginments += (total_w_with_corresp + total_w_without_corresp) / (total_w_with_corresp + 1)* error;
		ccT_total += ccT;
		b_total += b;
		if (verbose) std::cout << "\t";
		return true;
	}
	

}


template<typename Eigen_T_weights>
template<typename NNFinder_cloud_i>
bool BundleAdjustmentV2<Eigen_T_weights>::pairwiseHessianAndB(
	float alpha_point2point, float alpha_point2plane,
	explainMe::Cloud::Ptr cloud_i,
	explainMe::Cloud::Ptr cloud_j,
	Eigen::VectorXf & weights_i,
	NNFinder_cloud_i & nnFinder_j,
	Eigen::Matrix4f & a_i, Eigen::Matrix4f & a_i_inv,
	Eigen::Matrix4f & a_j, Eigen::Matrix4f & a_j_inv,
	Eigen::Vector4f & centroid,

	Matrix6f &ccT_total,
	Vector6f & b_total)
{

	//points in cloud j need to be mapped to the coordinate frame of i for the nn search
	//that is a[i]*a_inv[j]
	Eigen::Matrix4f i_to_j = a_j* a_i_inv;
	const int clouds_i_size = cloud_i->size();
	explainMe::Point p, q;
	Matrix6f ccT;
	Vector6f b;
	ccT.setZero();
	b.setZero();


	//use maximally points with weights summing up to 2500.
	//int ith = std::ceil(weights_j.col(weight_idx).sum() / 2500 + 0.1);
	float total_w_with_corresp = 0, total_w_without_corresp = 0, error = 0, numCorresp = 0;

#pragma omp parallel private(p,q) num_threads(20)//20
	{
		Matrix6f ccT_, ccT_p2p; Vector6f b_, b_p2p; ccT_.fill(0); b_.fill(0);
		Vector6f c; int idx;
		float w = 0, error_ = 0;
		float total_w_corresp_ = 0, total_w_no_corresp_ = 0, numCorresp_ = 0;
		//simpleNNFinder_ndim<6> nnFinder(myTree);
		NNFinder_cloud_i nnFinder = nnFinder_j;
#pragma omp for nowait
		for (int p_idx = 0; p_idx < clouds_i_size; p_idx++)
		{
			//ignore zero weight points:
			if (weights_i(p_idx) < 1e-3f){
				continue;
			}
			//subsampling
			/*if (!((p_idx + rand()) % ith == 0)){
				continue;
			}*/

			//map p t o qs frame and find the nn
			p = cloud_i->at(p_idx);//*qit;
			p.getVector4fMap() = i_to_j * p.getVector4fMap();
			//idx = nnFinder.findNN(simpleTreeTools::p(p.x, p.y, p.z, p.r, p.g, p.b)/*, _params.culling_dist*_params.culling_dist*/).index;
			pxyz pt(p.x,p.y, p.z);
			idx = nnFinder.findNN(pt).index;
			if (idx >= 0){
				//valid
				q = cloud_j->at(idx);

				//reset p
				p = cloud_i->at(p_idx);//*qit;
				//now p and q are found, but need to be mapped such that they can be used to compute an increment of the alignments
				//i.e. map to reference cloud and minus centroid.
				p.getVector4fMap() = a_i_inv * p.getVector4fMap() - centroid; p.getNormalVector3fMap() = a_i_inv.topLeftCorner<3, 3>() *p.getNormalVector3fMap();
				q.getVector4fMap() = a_j_inv * q.getVector4fMap() - centroid; q.getNormalVector3fMap() = a_j_inv.topLeftCorner<3, 3>() *q.getNormalVector3fMap();


				//weight/culling
				w = p.getNormalVector3fMap().dot(q.getNormalVector3fMap());
				w = (w < 0.5 ? 0 : w*w); w = (w * 0 == 0 ? w : 0.5);
				//w = (w< 0.7071 ? 0 : w); w = (w * 0 == 0 ? w : 0.5);
				//w *= (1 - (q.getVector3fMap() - p.getVector3fMap()).norm() / _params.culling_dist);
				w *= std::exp(-(q.getVector3fMap() - p.getVector3fMap()).norm());
				w = w<0 ? 0 : w;

				
				w *= weights_i(p_idx);
				
				//the point to plane equation
				c << p.getVector3fMap().cross(q.getNormalVector3fMap()), q.getNormalVector3fMap();
				ccT_ += c*c.transpose()*w*alpha_point2plane;
				b_ += c*(p.getVector3fMap() - q.getVector3fMap()).dot(q.getNormalVector3fMap())*w*alpha_point2plane;
				error_ += std::pow((p.getVector3fMap() - q.getVector3fMap()).dot(q.getNormalVector3fMap()), 2)*w*alpha_point2plane;


				//point to point...
				b_p2p << q.getVector3fMap().cross(p.getVector3fMap() - q.getVector3fMap()), p.getVector3fMap() - q.getVector3fMap();
				ccT_p2p << q.y*q.y + q.z*q.z, -q.x*q.y, -q.x*q.z, 0, -q.z, q.y,
					-q.y*q.x, q.x*q.x + q.z*q.z, -q.y*q.z, q.z, 0, -q.x,
					-q.z*q.x, -q.z*q.y, q.x*q.x + q.y*q.y, -q.y, q.x, 0,
					0, q.z, -q.y, 1, 0, 0,
					-q.z, 0, q.x, 0, 1, 0,
					q.y, -q.x, 0, 0, 0, 1;

				ccT_ += ccT_p2p * w * alpha_point2point;
				b_ += b_p2p * w * alpha_point2point;//*/
				error_ += (p.getVector3fMap() - q.getVector3fMap()).squaredNorm() * w * alpha_point2point;

				total_w_corresp_ += w;
				numCorresp_ += weights_i(p_idx);

				if (ccT_.norm() * 0 != 0 || b_.norm() * 0 != 0){
					std::cout << "Bundle Adjustment: NANs !\n";
					std::cout << p << "\n" << q << "\n";
				}
			}
			else{
				w = weights_i(p_idx);
				total_w_no_corresp_ += w;
			}

		}

#pragma omp critical
					{
						ccT += ccT_;
						b += b_;
						error += error_;
						total_w_with_corresp += total_w_corresp_;
						total_w_without_corresp += total_w_no_corresp_;
						numCorresp += numCorresp_;
					}
	}//end omp parallel (nn and pairwise matrix computation)
	//std::cout << total_w_with_corresp << " vs "<< total_w_without_corresp << "|";

	/*if (total_w_with_corresp < 0.1*numCorresp && possiblyIgnoreConstraint){
		if (verbose)
			std::cout << "Ign\t";
		error_new_alginments += (total_w_with_corresp + total_w_without_corresp) / (total_w_with_corresp + 1)* error;
		return false;
	}
	else if (total_w_with_corresp <  200 || total_w_with_corresp < 2 * total_w_without_corresp || total_w_with_corresp < 0.5*numCorresp){
		//else if (total_w_with_corresp <  200 || (total_w_with_corresp < 0.5*numCorresp && total_w_with_corresp <0.25* total_w_without_corresp)){
		//try to align to it, but not well aligned at all.
		error_new_alginments += (total_w_with_corresp + total_w_without_corresp) / (total_w_with_corresp + 1)* error;
		ccT_total += ccT;
		b_total += b;
		if (verbose) std::cout << "semi\t";//enforcing. (" << numCorresp << ")";
		return false;
	}
	else{*/
	//	error_new_alginments += (total_w_with_corresp + total_w_without_corresp) / (total_w_with_corresp + 1)* error;
		ccT_total += ccT;
		b_total += b;
		//if (verbose) std::cout << "\t";
		return true;
	//}
}


void smoothenAlignments(std::vector<Eigen::Matrix4f> &alignments, int referenceCloud)
{
	Eigen::Quaternionf q1, q2, q3, b1, b2, res;
	float alpha = 0.5;//the smaller alpha is, the less smoothing;
	auto a = alignments;
	for (int i = 1; i < alignments.size() - 1; i++){
		if (i != referenceCloud){
			q1 = alignments[i - 1].topLeftCorner<3, 3>();
			q2 = alignments[i].topLeftCorner<3, 3>();
			q3 = alignments[i + 1].topLeftCorner<3, 3>();
			b1 = q1.slerp(0.5, q3);
			b2 = q2.slerp(0.5, b1);
			res = b1.slerp(0.5, b2);
			a[i].topLeftCorner<3, 3>() = res.toRotationMatrix();
			a[i].topRightCorner<3, 1>() = ((alignments[i - 1].topRightCorner<3, 1>() + alignments[i + 1].topRightCorner<3, 1>())*0.5*(alpha) + alignments[i].topRightCorner<3, 1>()*(1-alpha));
		}
	}
	alignments = a;
}

void smoothenGradient(std::vector<Eigen::Matrix4f> &alignments, int referenceCloud)
{
	auto a = alignments;
	for (int i = 0; i < a.size() - 1; i++){
		a[i] = alignments[i + 1]* alignments[i].inverse();
	}
	a.pop_back();
	smoothenAlignments(a, -1);
	smoothenAlignments(a, -1);
	auto a_reconstructed = alignments;
	for (int i = referenceCloud+1; i < a_reconstructed.size(); i++){
		a_reconstructed[i] = a[i]*a_reconstructed[i-1];
	}
	for (int i = referenceCloud - 1; i >=0; i--){
		a_reconstructed[i] = a[i].inverse() * a_reconstructed[i + 1];
	}
	alignments = a_reconstructed;

}

/*void dampMax(std::vector<Eigen::Matrix4f> &alignments, explainMe::Cloud::Ptr refCloud, int refFrame)
{
	Eigen::Quaternionf q1, q2, q3, b1, b2, res;
	float alpha = 0.5;//the smaller alpha is, the less smoothing;
	auto a = alignments;
	std::vector<float> maxMotions;
	float max_motion = 0;
	int max_i = 0;
	Eigen::Vector4f p_im1, p_i;
	for (int i = 1; i < alignments.size() - 1; i++){

		float max = 0, diff;
		for (auto p : refCloud->points){
			p_im1 = a[i - 1] * p.getVector3fMap();
			p_i = a[i] * p.getVector3fMap();
			diff = (p_i - p_im1).norm();
			max = (diff > max ? diff : max);
		}
		if (max > max_motion){
			max_motion = max;
			max_i = i;
		}
		//maxMotions.push_back(max);
		
	}

	if (max_i <= refFrame && max_i>0){
		Eigen::Matrix4f new_im1_i = a[max_i] * a[max_i - 1].inverse();
		Eigen::Quaternionf q1, id;
		q1 = new_im1_i.topLeftCorner<3, 3>();
		id = Eigen::Quaternionf::Identity();
		new_im1_i.topLeftCorner<3, 3>() = q1.slerp(0.5, id).toRotationMatrix();
		new_im1_i.topRightCorner<3, 1>() *= 0.5;

		for (int j = 0; j < max_i; j++)

	}

	alignments = a;

}*/

template<typename Eigen_T_weights>
Eigen::Matrix4f BundleAdjustmentV2<Eigen_T_weights>::computeAlignment(Vector6f & x_icp_total, Eigen::Vector4f & centroid){
	Eigen::Matrix4f  alignment;
	float cg = std::cos(x_icp_total(2)); float ca = std::cos(x_icp_total(0)); float cb = std::cos(x_icp_total(1));
	float sg = std::sin(x_icp_total(2)); float sa = std::sin(x_icp_total(0)); float sb = std::sin(x_icp_total(1));
	alignment << cg*cb, sg*cb, -sb, -x_icp_total(3),
		-sg*ca + cg*sb*sa, cg*ca + sg*sb*sa, cb*sa, -x_icp_total(4),
		sg*sa + cg*sb*ca, -cg*sa + sg*sb*ca, cb*ca, -x_icp_total(5),
		0, 0, 0, 1;

	alignment.topRightCorner<4, 1>() += centroid - alignment*centroid;
	return alignment;
}

//assumes that there are no nans int the clouds.
//heuristic spped ups: decreasing maximal radius in nn search
//multiresolution through multiresolution via subsampling.
template<typename Eigen_T_weights>
void BundleAdjustmentV2<Eigen_T_weights>::pointToPlane_LM(
	params &_params,
	data &_data,
	int weight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
	mesh* avg_mesh, //supposed to be in the reference coordinate frame.
	std::vector<int> * cloudsAlignedToAvg
)
{
	//smoothenAlignments(alignments, _data.referenceCloud);
	//smoothenAlignments(alignments, _data.referenceCloud);
	//smoothenAlignments(alignments, _data.referenceCloud);
	//smoothenGradient(alignments, _data.referenceCloud);
	std::cout << "Reference frame: " << _data.referenceCloud<< "\n";
	if ((alignments[_data.referenceCloud] - Eigen::Matrix4f::Identity()).cwiseAbs().maxCoeff() > 0.001){
		std::cout << "Error! reference matrix not id!\n" << alignments[_data.referenceCloud] << "\n";
		cv::waitKey();
	}

	Eigen::Vector4f centroid;
	centroid << CloudTools::compute3DCentroid(_data.clouds[_data.referenceCloud]), 0; 
	std::vector<Eigen::Matrix4f>		a_inv			= computeInvs(alignments);
	std::vector<mySimpleKdTree_ndim<6>> allTrees		= buildAllTrees(_params, _data);
	std::vector<Eigen::Matrix4f>		new_alginments	= alignments;
	std::vector<Eigen::Matrix4f>		new_inv_alginments = a_inv;
	bool								new_alignments_equal_alignments = true;

	//The bundle adjustment matrix
	Eigen::MatrixXf total(6 * (_data.clouds.size() - 1), 6 * (_data.clouds.size() - 1));
	Eigen::VectorXf total_b(6 * (_data.clouds.size() - 1));
	Eigen::VectorXf X(6 * (_data.clouds.size() - 1));


	//for visibility computation of the average mesh:
/*	if (avg_mesh != NULL && offscreenRenderer != NULL){
		offscreenRenderer->updateTriangles("avgMesh", avg_mesh->points, avg_mesh->triangle_indices);
	}*/
	//temps 
	Vector6f c, b; Matrix6f ccT;explainMe::Point p, q;
	auto & weights = _data.weights; float culling_dist = _params.culling_dist;
	//kdtree method parameters
	
	float error_newAlignments;
	float error_alignments = _params.last_error;
	if (_params.last_error < 0){
		error_alignments = 1e10f;
	}
	std::cout << "Initial error: " << error_alignments << "\n";
	float lambda_damping = _params.lambda;
	int ith;

	float x_max_coeff = 0;
	int iteration = 0;
	do{
		//resetting the BA matrix.
		total = Eigen::MatrixXf::Zero(6 * (_data.clouds.size() - 1), 6 * (_data.clouds.size() - 1));
		total_b.fill(0);
		error_newAlignments = 0;


		//1st compute the various 6x6 blocks of the ba matrix
		//The single blocks coincide with the std point to plane blocks.
		//compute all c's abd b's. This involves the association of cloud j to cloud i
		if (avg_mesh != NULL){// && offscreenRenderer != NULL){
			if (cloudsAlignedToAvg != NULL){
				cloudsAlignedToAvg->clear();
			}
			Eigen::Matrix<float, 6, 1> cls;
			cls << 1, 1, 1, 0, 0, 0;
			/*mySimpleKdTree_ndim<6> avgTree(cls);
			simpleTreeTools::setData<explainMe::Cloud::Ptr>(avgTree, avg_mesh->points);
			avgTree.buildTree();//*/

			int	i = _data.referenceCloud; //mesh supposed to be in this frame

			//explainMe::Cloud::Ptr	visible_points(new explainMe::Cloud());
			Eigen::MatrixXi	projected_index_map_onebased(_data.d->width(), _data.d->height());
			//Eigen::Matrix4f perturb = Eigen::Matrix4f::Identity();
			
			for (int j = 0; j < _data.clouds.size(); j++){
				if (j == _data.referenceCloud){
					if (cloudsAlignedToAvg != NULL){
						cloudsAlignedToAvg->push_back(j);
					}
					continue;
				}
				
				//perturb.topLeftCorner<3, 3>() = Eigen::AngleAxis<float>(Eigen::MatrixXf::Random(1,1)(0,0), Eigen::Vector3f::Random().normalized()).toRotationMatrix();
				/*
				//offscreenRenderer->setCamera(new_inv_alginments[j]);
				offscreenRenderer->setCamera(new_alginments[j]);
				//offscreenRenderer->setCamera(perturb*new_alginments[j]);
				offscreenRenderer->doOffscreenRendering2IntBuffer("avgMesh", projected_index_map_onebased.data());
				*/
				//cv::waitKey();
				//offscreenRenderer->setCamera(Eigen::Matrix4f::Identity()); //set via look at!
				//offscreenRenderer->updateCloud("currMesh", _data.clouds[j]);
				//offscreenRenderer->doOffscreenRendering2IntBuffer("currMesh", projected_index_map_onebased.data());
				//renders the mesh as seen from frame j.
				//to get a nn index:  The querry point is assumed to be in the coordinate frame of the mesh.
				//map the query point to frame j inside the nnFinder, do the projection. yields index.
				//debugging...-----
				//Reading the buffer with Eigen::Matrixxi.data produces the right ordering.
				/*std::ofstream ffs("D:/Temp/renderedIndices.m");
				ffs << " a = [";
				for (int mat_i = 0; mat_i < projected_index_map_onebased.rows(); mat_i++){
					for (int mat_j = 0; mat_j < projected_index_map_onebased.cols(); mat_j++){
						ffs << projected_index_map_onebased(mat_i, mat_j) << " ";
					}
					ffs << ";\n";
				}
				ffs << "];";
				ffs.close();*/
							
				//cv::waitKey();
				//continue;
				///----------------

				//debug....
				/*for (int test_i = 0; test_i < avg_mesh->points->size(); test_i++){
					//query point whould lie in the coord frame of avg_mesh.
					//Eigen::Vector4f mapped = new_alginments[j] * avg_mesh->points->at(test_i).getVector4fMap();
					Eigen::Vector4f mapped = avg_mesh->points->at(test_i).getVector4fMap();
					p_ndim<6> mapped_p_; mapped_p_.vec << mapped(0), mapped(1), mapped(2), 0, 0, 0;
					auto d_i =  nnByProj.findNN(mapped_p_, 1e5);
					if (d_i.index < 0 || d_i.dist_sqr > 0.0001){
					//	std::cout <<"nn(" << test_i << ") = " << d_i.index << ", ";
					}
					else{
						//std::cout << "nn(" << test_i << ") = " << d_i.index << ", ";
						//test if it is truly close

						//test if the index gets mapped on itself. Actually it doesn't have to, because points are in there with multiplicity- imprinting their index to 
						//a voronoi subregion of any triangle. at the vertex itself any of the equivalent vertices might win.
						int matched_index = d_i.index;
						mapped = avg_mesh->points->at(matched_index).getVector4fMap();
						mapped_p_.vec << mapped(0), mapped(1), mapped(2), 0, 0, 0;
						auto d_i2 = nnByProj.findNN(mapped_p_, 1e5);
						
						//now this should be zero
						if (d_i2.index < 0){
							//this should not happem---
							std::cout << avg_mesh->points->at(matched_index).getVector3fMap() << " Without projection! ("<< (d_i2.index == -1 ? "empty pixel " : "invalid position or value") << ")\n";
						}
						else{
							//now this should be often zero. Not always, due as a portion of the voronoi area can be visible without the vertex being visible
							//std::cout << (avg_mesh->points->at(test_i).getVector3fMap() - avg_mesh->points->at(d_i.index).getVector3fMap()).norm() << " | ";
							//std::cout << (avg_mesh->points->at(matched_index).getVector3fMap() - avg_mesh->points->at(d_i2.index).getVector3fMap()).norm() << " \n";
						}
						//std::cout << "nn(" << matched_index << ") = " << d_i.index << ", ";
					}
				}*/
				//-----
				float hack_cull_dist = _params.culling_dist;
				_params.culling_dist = 1.f;
				std::cout << "avg - " << j << " ";
				b = Vector6f::Zero();
				ccT = Matrix6f::Zero();
				Eigen::Matrix4f id = Eigen::Matrix4f::Identity();

				//explainMe::MatrixXf_rm ones = explainMe::MatrixXf_rm::Ones(avg_mesh->points->size(), _data.weights[j].cols());
				Eigen_T_weights ones = explainMe::MatrixXf_rm::Ones(avg_mesh->points->size(), _data.weights[j].cols());
				
				//nnByProjection<6> nnfinder(projected_index_map_onebased, avg_mesh->points, new_inv_alginments[j], _data.d);
				nnByProjection<6> nnfinder(projected_index_map_onebased, avg_mesh->points, new_alginments[j], _data.d);
				//nnByProjection<6> nnfinder(projected_index_map_onebased, avg_mesh->points, perturb*new_alginments[j], _data.d);
				bool wasUsed = pairwiseHessianAndB<nnByProjection<6>>(/*/
				simpleNNFinder_ndim<6> nnfinder(avgTree);
				bool wasUsed = pairwiseHessianAndB<simpleNNFinder_ndim<6>>(//*/
					avg_mesh->points, _data.clouds[j],
					ones,			_data.weights[j],
					nnfinder,
					id,					id,
					new_alginments[j],	new_inv_alginments[j],
					_params, weight_idx, centroid,
					true,
					//"return": ( += )
					ccT, b, error_newAlignments,
					true
					);
				_params.culling_dist = hack_cull_dist;
				std::cout << "\n";

				if (wasUsed&& cloudsAlignedToAvg != NULL){
					cloudsAlignedToAvg->push_back(j);
				}

				if (ccT.norm() * 0 != 0 || b.norm() * 0 != 0){
					std::cout << "Bundle Adjustment: NANs between: " << i << "," << j << "!\n";
				}

				//update the bundle adjustment matrix ( see notes)
				//ccT *= 100;
				//b *= 100;
				addConstraintToTotal(ccT, b, i, j, _data.referenceCloud, total, total_b);
			}
		}
				//error_newAlignments *= 100;
		
		for (int i = 0; i < enforce.size(); i++)
		{
			auto & myTree = allTrees[i];
			//use maximally points with weights summing up to 2500.
			ith = std::ceil(_data.weights[i].col(weight_idx).sum() / 2500 + 0.1);// + std::max<int>((int) std::floor((numIterations -1 - its) * 1.0/(numIterations-1) * 10+ 0.5), 1);

			for (int nbr = 0; nbr < enforce[i].size(); nbr++)
			{
				//constraint between cloud i and j.
				int j = enforce[i][nbr];
				b = Vector6f::Zero();
				ccT = Matrix6f::Zero();

				std::cout << "(" << i << ","<<j << ")" ;
				simpleNNFinder_ndim<6> nn_finder(myTree);
				pairwiseHessianAndB<simpleNNFinder_ndim<6>>(_data.clouds[i], _data.clouds[j], _data.weights[i], _data.weights[j],
				                                            nn_finder,
				                                            new_alginments[i], new_inv_alginments[i],
				                                            new_alginments[j], new_inv_alginments[j],
					_params, weight_idx, centroid,
					std::abs(j-i) > 2,
					//"return": ( += )
					ccT, b, error_newAlignments,
					true
					);

				if (ccT.norm() * 0 != 0 || b.norm() * 0 != 0){
					std::cout << "Bundle Adjustment: NANs between: " << i << "," << j << "!\n";
				}

				addConstraintToTotal(ccT, b, i, j, _data.referenceCloud, total, total_b);
				
			}
		}

		//damping:
		total = total + lambda_damping * Eigen::MatrixXf::Identity(6 * (_data.clouds.size() - 1), 6 * (_data.clouds.size() - 1));
	
		// The matrix is "often" positive semidefinite, in that provably it isn't always, but in practice I never experienced it not to be.
		auto ldlt = (total).ldlt();
		X = ldlt.solve(total_b);
		//resort to more robust, more expensive method if needed.X is left untouched if ldlt fails...
		float xnorm = X.norm();
		if (xnorm == 0 || !ldlt.isPositive()){
			std::cout << "\n Matrix not pos def!\n";
			X = total.fullPivLu().solve(total_b);
			xnorm = X.norm();
			std::cout << "X: " << X.norm() << " b " << total_b.norm() << " total.sum " << total.diagonal().sum() << "\n";
		}
		x_max_coeff = X.maxCoeff();
		std::cout << "Total Norm of lie-element vector: " << X.norm() << "\t(" << iteration << " " << ith << ")\n";
		std::cout << "\n\tNew and last error: " << error_newAlignments << "\t\t" << error_alignments << "\n";
		
		//decide on damping etc:
		//error_newAlignment was updated through the loop and now stores the error of
		//the alignments currently stored in new_alignment (which where computed one iteration earlier)
		if (error_newAlignments < error_alignments || new_alignments_equal_alignments /*&& x_max_coeff< 0.5*/){ //larger and the linearization is to unrelieable.
			error_alignments = error_newAlignments;
			//use new alignments
			alignments = new_alginments;
			a_inv = new_inv_alginments;

			//dbg
			if (!new_alignments_equal_alignments){
				for (int bla = 0; bla < 12; bla++){
					Eigen::AngleAxisf aa;
					aa.fromRotationMatrix(alignments[bla].block<3, 3>(0, 0));
					std::cout << bla << ": axis " <<aa.axis().transpose() << "\t angle " << aa.angle() <<"\n";
				}
			}
		
			float max_diff = (X.block(0, 0, 6 * (_data.clouds.size() - 2), 1) - X.block(6, 0, 6 * (_data.clouds.size() - 2), 1)).cwiseAbs().maxCoeff();
			std::cout << "max_diff: " << max_diff << "\t";
			//if (x_max_coeff < 0.5){//larger and the linearization is really too unrelieable.
			if (max_diff < 0.5){//larger and the linearization is really too unrelieable.
				lambda_damping *= 0.6f;
				//and we compute the next new alignments.
				//extract all alignments.
				for (int i_cloud = 0; i_cloud < alignments.size(); i_cloud++){
					if (i_cloud != _data.referenceCloud){
						int i = (i_cloud > _data.referenceCloud ? i_cloud - 1 : i_cloud);
						Vector6f x_icp_total = -X.block<6, 1>(i * 6, 0);

						//map linearized transform back
						Eigen::Matrix4f  alignment = computeAlignment(x_icp_total, centroid);
						/*float cg = std::cos(x_icp_total(2)); float ca = std::cos(x_icp_total(0)); float cb = std::cos(x_icp_total(1));
						float sg = std::sin(x_icp_total(2)); float sa = std::sin(x_icp_total(0)); float sb = std::sin(x_icp_total(1));
						alignment << cg*cb, sg*cb, -sb, -x_icp_total(3),
							-sg*ca + cg*sb*sa, cg*ca + sg*sb*sa, cb*sa, -x_icp_total(4),
							sg*sa + cg*sb*ca, -cg*sa + sg*sb*ca, cb*ca, -x_icp_total(5),
							0, 0, 0, 1;

						alignment.topRightCorner<4, 1>() += centroid - alignment*centroid;*/


						new_alginments[i_cloud] = alignments[i_cloud] * alignment;
						new_inv_alginments[i_cloud] = new_alginments[i_cloud].inverse();
					}
				}
				new_alignments_equal_alignments = false;
			}
			else{
				lambda_damping *= 10;
				new_alignments_equal_alignments = true;
			}

		}
		else{
			//discard new alignments
			//we do not recompute any alignments out of X as X was computed
			//with invalid new_alignments.
			lambda_damping *= 10;
			new_alginments = alignments;
			new_inv_alginments = a_inv;
			error_newAlignments = error_alignments;
			new_alignments_equal_alignments = true;
		}

		std::cout << "Damping: " << lambda_damping << "\t\t";
		std::cout << "XMax: " << x_max_coeff << "\n";

	} while (++iteration < _params.numIterations && x_max_coeff > 1e-4);

	_params.lambda = lambda_damping;
	_params.last_error = error_alignments;

}

template<typename Eigen_T_weights>
void BundleAdjustmentV2<Eigen_T_weights>::pointToPlane_first2second(
	params &_params,
	explainMe::Cloud::Ptr cloud_1,
	explainMe::Cloud::Ptr cloud_2,
	Eigen::VectorXf & weights_1,
	Eigen::Matrix4f & ref_to_cloud1,
	Eigen::Matrix4f & ref_to_cloud2
	)
{

	Eigen::Vector4f centroid;
	centroid << CloudTools::compute3DCentroid(cloud_2), 0;

	//the kd tree for cloud 2.
	Vector6f scales;
	scales << 1, 1, 1, _params.alpha_color, _params.alpha_color, _params.alpha_color;
	//mySimpleKdTree_ndim<6> tree_2 = mySimpleKdTree_ndim<6>(scales);
	//simpleTreeTools::setData<explainMe::Cloud::Ptr>(tree_2, cloud_2);
	//tree_2.buildTree();
	//simpleNNFinder_ndim<6> nnfinder_2(tree_2);
	mySimpleKdTree tree_2;
	tree_2.setData(cloud_2);
	tree_2.buildTree();
	simpleNNFinder nnfinder_2(tree_2);


	//the alignment to solve for,
	Eigen::Matrix4f new_alignment_2 = Eigen::Matrix4f::Identity();

	//temps 
	Vector6f b, X; Matrix6f ccT;
	float culling_dist = _params.culling_dist;
	//kdtree method parameters
	//float error_new_alignments;

	//the icp alg.
	int iteration = 0;
	Eigen::Matrix4f  alignment, alignment_inv, id, a_increment;
	alignment = alignment_inv = id = Eigen::Matrix4f::Identity();

	//initial guess:
	alignment = ref_to_cloud2 *ref_to_cloud1.inverse();
	do{
		b = Vector6f::Zero();
		ccT = Matrix6f::Zero();
		pairwiseHessianAndB(
			_params.alpha_point2point, _params.alpha_point2plane,
			cloud_1, cloud_2,
			weights_1,
			nnfinder_2,
			id, id,
			alignment, alignment_inv,
			centroid,
			ccT, b
			);

		//add an average mesh term: instead of id id:

		auto ldlt = (ccT).ldlt();
		X = ldlt.solve(b);
		//std::cout << X.transpose() << "\n";
		//check error decrease etc....

		a_increment = computeAlignment(X, centroid);
		alignment = alignment*a_increment;
		alignment_inv = alignment.inverse();
	} while (iteration++ < _params.numIterations);


	if ((alignment - Eigen::Matrix4f::Identity()).cwiseAbs().maxCoeff() > 0.3){
		std::cout << "The weights used ... " << weights_1.minCoeff() << ", " << weights_1.maxCoeff() << ", " << weights_1.mean() << ", nzs " << (weights_1.array() > 0.01).count() << "\n";
		std::cout << "\n*****Relative alignment:******\n " << alignment << "\n";
	}
	//new_alginments[j + 1] = alignment * new_alginments[j];
	ref_to_cloud2 = alignment * ref_to_cloud1;

//	Eigen::Matrix4f  ref_inv = ref_to_cloud1.inverse();
//	ref_to_cloud2 = ref_inv *ref_to_cloud2;
	/*for (Eigen::Matrix4f & a : new_alginments){
		a = ref_inv * a;
	}*/

	//alignments = new_alginments;
}

template<typename Eigen_T_weights>
void BundleAdjustmentV2<Eigen_T_weights>::pointToPlane_incremental(
	params &_params,
	data &_data,
	int weight_idx,
	std::vector<Eigen::Matrix4f> &alignments
	)
{
	if ((alignments[_data.referenceCloud] - Eigen::Matrix4f::Identity()).cwiseAbs().maxCoeff() > 0.001){
		std::cout << "REgistration: note: reference matrix not id!\n" << alignments[_data.referenceCloud] << "\n";
		//cv::waitKey();
	}

	Eigen::Vector4f centroid;
	std::vector<mySimpleKdTree_ndim<6>> allTrees = buildAllTrees(_params, _data);
	std::vector<Eigen::Matrix4f>		new_alginments(_data.clouds.size(), Eigen::Matrix4f::Identity());


	//temps 
	Vector6f b, X; Matrix6f ccT;
	auto & weights = _data.weights; float culling_dist = _params.culling_dist;
	//kdtree method parameters
	float error_new_alignments;

	for (int j = 0; j < _data.clouds.size() - 1; j++)
	{
		int iteration = 0;
		simpleNNFinder_ndim<6> nnfinder(allTrees[j]);
		centroid << CloudTools::compute3DCentroid(_data.clouds[j]), 0;
		Eigen::Matrix4f  alignment, alignment_inv, id, a_increment;
		alignment = alignment_inv = id = Eigen::Matrix4f::Identity();

		//initial guess:
		alignment = alignments[j + 1] * alignments[j].inverse();
		do{
			b = Vector6f::Zero();
			ccT = Matrix6f::Zero();
			pairwiseHessianAndB(_data.clouds[j], _data.clouds[j + 1],
				_data.weights[j], _data.weights[j + 1],
				nnfinder,
				id,id,
				alignment, alignment_inv,
				_params, weight_idx, centroid,
				true,
				//"return": ( += )
				ccT, b, error_new_alignments,
				false
				);

			//add an average mesh term: instead of id id:

			auto ldlt = (ccT).ldlt();
			X = ldlt.solve(b);
			//std::cout << X.transpose() << "\n";
			//check error decrease etc....

			a_increment = computeAlignment(X, centroid);
			alignment = alignment*a_increment;
			alignment_inv = alignment.inverse();
		} while (iteration++ < _params.numIterations);

		new_alginments[j + 1] = alignment * new_alginments[j];
	}

	Eigen::Matrix4f  ref_inv = new_alginments[_data.referenceCloud].inverse();
	for (Eigen::Matrix4f & a : new_alginments){
		a = ref_inv * a;
	}

	alignments = new_alginments;
}

template
class BundleAdjustmentV2<explainMe::MatrixXf_rm>;
template
class BundleAdjustmentV2<Eigen::MatrixXf>;
template
bool BundleAdjustmentV2<explainMe::MatrixXf_rm>::pairwiseHessianAndB<simpleNNFinder_ndim<6>>(
explainMe::Cloud::Ptr cloud_i,
explainMe::Cloud::Ptr cloud_j,
explainMe::MatrixXf_rm & weights_i,
explainMe::MatrixXf_rm & weights_j,
simpleNNFinder_ndim<6> & nnFinder,
Eigen::Matrix4f & a_i, Eigen::Matrix4f & a_i_inv,
Eigen::Matrix4f & a_j, Eigen::Matrix4f & a_j_inv,
params & _params,
int weight_idx,
Eigen::Vector4f & centroid,
bool possiblyIgnoreConstraint,
Matrix6f &ccT_target,
Vector6f & b_target,
float & error,
bool verbose);

template
bool BundleAdjustmentV2<Eigen::MatrixXf>::pairwiseHessianAndB<simpleNNFinder_ndim<6>>(
explainMe::Cloud::Ptr cloud_i,
explainMe::Cloud::Ptr cloud_j,
Eigen::MatrixXf & weights_i,
Eigen::MatrixXf & weights_j,
simpleNNFinder_ndim<6> & nnFinder,
Eigen::Matrix4f & a_i, Eigen::Matrix4f & a_i_inv,
Eigen::Matrix4f & a_j, Eigen::Matrix4f & a_j_inv,
params & _params,
int weight_idx,
Eigen::Vector4f & centroid,
bool possiblyIgnoreConstraint,
Matrix6f &ccT_target,
Vector6f & b_target,
float & error,
bool verbose);

/*
template
bool BundleAdjustmentV2<Eigen::MatrixXf>::pairwiseHessianAndB<simpleNNFinder_ndim<6>>(
float alpha_point2point, float alpha_point2plane,
explainMe::Cloud::Ptr cloud_i,
explainMe::Cloud::Ptr cloud_j,
Eigen::VectorXf & weights_i,
simpleNNFinder_ndim<6> & nnFinder_j,
Eigen::Matrix4f & a_i, Eigen::Matrix4f & a_i_inv,
Eigen::Matrix4f & a_j, Eigen::Matrix4f & a_j_inv,
Eigen::Vector4f & centroid,

Matrix6f &ccT_target,
Vector6f & b_target);
*/

template
bool BundleAdjustmentV2<Eigen::MatrixXf>::pairwiseHessianAndB<simpleNNFinder>(
float alpha_point2point, float alpha_point2plane,
explainMe::Cloud::Ptr cloud_i,
explainMe::Cloud::Ptr cloud_j,
Eigen::VectorXf & weights_i,
simpleNNFinder & nnFinder_j,
Eigen::Matrix4f & a_i, Eigen::Matrix4f & a_i_inv,
Eigen::Matrix4f & a_j, Eigen::Matrix4f & a_j_inv,
Eigen::Vector4f & centroid,

Matrix6f &ccT_target,
Vector6f & b_target);