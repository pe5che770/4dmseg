#include "statsKeeper.h"
#include <iostream>
void stats::print(){
	std::cout << name << "\t" << total.total() << "\n\t";
	details.printAll();
}

statsKeeper::statsKeeper() :init("init"), interpolate("interpolate"), motion("motion"), segmentation("segmentation"), 
other("other"), doc("doc")
{
	stats *all[] = {&init, &interpolate, &motion, &segmentation, &doc, &other};
	allStats.assign(all, all + 6);
	totalNumStep = 1;
	currentStep = 0;
	grandTotal.restart();
}


statsKeeper::~statsKeeper()
{
	grandTotal.stop();
	std::cout << "Grand Total time: " << grandTotal.total()/1000.f << "\n";
}

void statsKeeper::printAll(){
	for (auto s : allStats){
		s->print();
	}
}