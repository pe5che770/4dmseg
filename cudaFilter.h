#pragma once
#include "sensorDescriptionData.h"

class cudaFilter
{
private:
	int dev;
	float * dev_inSrc, *dev_inSrc0, *dev_outSrc, *dev_inSrc1, *dev_inSrc2;
	int * dev_mask;
	bool result_is_in_IN_buffer;
	sensorDescriptionData sensor_desc;
	float * buffer;
public:
	cudaFilter(sensorDescriptionData desc);
	~cudaFilter(void);

	//setup device
	void selectCudaDevice();
	void printCudaDeviceStats();

	//get and set data on device
	void dev_setMask(int * mask);
	void dev_setData(float * data);
	//assumes a data array layed out as (x,y,z,x,y,z,....)
	void dev_set3dData(float * data);
	void dev_getData(float * target);
	void dev_get3dData(float * target);

	//the filters
	void filterPyramid(){}
	void boxFilter(int width_2, bool useMask);
	void boxFilter3d(int width_2, bool useMask);
	void normalGuidedFilter(int width_2);
	void normalGuidedGradient(int width_2);
	void gaussFilter(){}
	
	void flyingPixels(int width_2);
};

