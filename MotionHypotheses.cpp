#include "MotionHypotheses.h"
#include "BundleAdjustment.h"
#include "BundleAdjustmentV2.h"
#include "mySimpleKdTree_ndim.h"
#include <omp.h>
#include "LoopClosureFinder.h"
#include "CloudTools.h"
#include <set>
#include "StopWatch.h"


int motion::nextFreeLabelId = 0;
MotionHypotheses::MotionHypotheses(int initialNumHyps, int numClouds, RangeSensorDescription * desc)
{
	d = desc;
	for(int i = 0; i < initialNumHyps; i++){
		motions.push_back(motion(numClouds));
	}

	initialGuess.resize(numClouds, Eigen::Matrix4f::Identity());
}


MotionHypotheses::~MotionHypotheses(void)
{
}

void MotionHypotheses::setInitialGuessToAvgMotion(
	std::vector<explainMe::Cloud::Ptr> & allClouds,
	std::vector<mySimpleKdTree> & allKdTrees,
	int central_cloud, //the coordinate system of this cloud is used as reference system
	//the parameters...
	int maxItICP,
	float alpha_point2plane,
	float alpha_point2point,
	float cullingDist,
	bool resetAllMotions)
{
	std::cout << "Computing average motion.";

	std::cout << " numIt: " << maxItICP << "...\n";

	int pyrLevel = 2;
	//float factor = (numLoopClosureSearches == 0 ? 1 : std::pow(0.1, 1.0 / (numLoopClosureSearches)));//(factor = 0.8)

	std::vector<std::vector<int>> enforce;
	std::vector<Eigen::Vector3f> allCentroids;
	std::vector<std::vector<int>> enforce_updated;
	std::vector<std::pair<int, int>> newConstraints;
	std::vector<explainMe::MatrixXf_rm>  dummySeg;
	for (int i = 0; i < allClouds.size(); i++){
		enforce.push_back(std::vector<int>());
		if (i < allClouds.size() - 1){
			enforce.back().push_back(i + 1);
		}
		if (i < allClouds.size() - 3){
			enforce.back().push_back(i + 3);
		}
	}
	enforce_updated = enforce;
	for (int cl = 0; cl < allClouds.size(); cl++){
		allCentroids.push_back(CloudTools::compute3DCentroid(allClouds[cl]));
		dummySeg.push_back(explainMe::MatrixXf_rm::Ones(allClouds[cl]->size(),1));
	}
	


	BundleAdjustment ba;
	BundleAdjustment::LM_params lm_params;
	for (pyrLevel = 1; pyrLevel >= 0; pyrLevel--){
		lm_params.last_error = std::numeric_limits<float>::max();
		lm_params.lambda = 1;

		ba.LM_BA_fast<explainMe::MatrixXf_rm, mySimpleKdTree, pxyz, simpleNNFinder>(
			allClouds,
			allKdTrees,
			dummySeg, 0,
			initialGuess, enforce_updated, central_cloud, maxItICP
			, alpha_point2plane, alpha_point2point,
			//0.1,
			1.0 / 255,
			cullingDist, *d, lm_params, 10000);
		
		
		LoopClosureFinder loopFinder;
		loopFinder.findConstraints(allClouds.size(), initialGuess, allCentroids, *d, newConstraints);//, 3* std::pow(2,i));		

		enforce_updated = enforce;
		for (int j = 0; j < newConstraints.size(); j++){
			enforce_updated[newConstraints[j].first].push_back(newConstraints[j].second);
		}//*/

	}

	if (resetAllMotions)
		for (int i = 0; i < motions.size(); i++){
			motions[i].alignments = initialGuess;
		}
}

void MotionHypotheses::set(std::vector<std::vector<Eigen::Matrix4f>> & alignments)
{
	

	motions.clear();
	//std::vector<my_range> maxRanges;// = ranges;
	//maxRanges.resize(alignments.size(), my_range(-1,-1));
	for (int hyp = 0; hyp < alignments.size(); hyp++){
		my_range range(0, alignments.size());
		motions.push_back(motion(alignments[hyp], range));
	}
	std::cout << "kk";
	//alignmentSets = alignments;
	//ranges = maxRanges;
}

void MotionHypotheses::set(std::vector<std::vector<Eigen::Matrix4f>> & alignments, 
		std::vector<explainMe::MatrixXf_rm> & denseSeg)
{
	if(alignments.size() != denseSeg[0].cols()){
		std::cout << "***Error: incompatible alignment and segmentation";
		throw std::runtime_error("incompatible alignment and segmentation");
	}

	std::vector<explainMe::RowVectorXf> frame_support; 
	frame_support.reserve(denseSeg.size());
	for(int i = 0; i < denseSeg.size(); i++){
		frame_support.push_back(denseSeg[i].colwise().sum());
	}

	motions.clear();
	//std::vector<my_range> maxRanges;// = ranges;
	//maxRanges.resize(alignments.size(), my_range(-1,-1));
	for(int hyp = 0; hyp < alignments.size(); hyp++){
	/*	//my_range range()
		int start = 0;
		while(start < frame_support.size()){
			if(frame_support[start](hyp) > 150){
				break;
			}
			else{
				start++;
			}
		}
		int stop = frame_support.size();
		//excluding stop.
		while(stop > start){
			if(frame_support[stop-1](hyp) > 150){
				break;
			}
			else{
				stop--;
			}
		}
		//maxRanges[hyp].start = start;
		//maxRanges[hyp].stop = stop;*/
		my_range range(-1, -1);
		motions.push_back(motion(alignments[hyp], range));
	}
	updateRangesFromSupport(frame_support);
	std::cout << "kk";
	//alignmentSets = alignments;
	//ranges = maxRanges;
}
void MotionHypotheses::remove(std::vector<int> to_remove){
	std::sort(to_remove.begin(), to_remove.end());
	int numHyp = numHypotheses();
	int new_num_hyp = numHyp - to_remove.size();

	//std::vector<std::vector<Eigen::Matrix4f>> alignmentSets_new(new_num_hyp);
	//std::vector<my_range> new_ranges;
	int j_new = 0;
	for(int j = 0, j_to_remove = 0; j <numHyp; j++){
		//j_new: last valid index.
		//index j should be removed? dont increment j new
		if(j_to_remove < to_remove.size() && to_remove[j_to_remove] == j){
			j_to_remove ++;
			continue;
		}
		//element should be kept? copy it to the next valid index
		else{
			//alignmentSets_new[j_new] = alignmentSets[j];
			//new_ranges.push_back(ranges[j]);
			motions.at(j_new) = motions.at(j);
			j_new++;
			continue;
		}
	}
	motions.resize(j_new, motion(0));

	//alignmentSets = alignmentSets_new;
	//ranges = new_ranges;
}

/*void MotionHypotheses::updateAlignmentsAndRange_tsdf(
	int label,
	std::vector<explainMe::Cloud::Ptr> & extractedClouds,
	std::vector<explainMe::Cloud::Ptr> & allClouds,
	std::vector<explainMe::MatrixXf_rm> & denseSeg,
	std::vector<mySimpleKdTree> & allKdTrees,
	int central_cloud,
	//the parameters...
	int maxItICP,
	float alpha_point2plane,
	float alpha_point2point,
	float cullingDist)
{


	std::vector<std::vector<int>> enforce;
	for (int i = 0; i < allClouds.size(); i++){
		enforce.push_back(std::vector<int>());
		//enforce.back().push_back(i+1);
		if (i >0){
			enforce.back().push_back(i - 1);
		}
		if (i < allClouds.size() - 1){
			enforce.back().push_back(i + 1);
		}
	}

	explainMe::Cloud::Ptr fusedCloud(new explainMe::Cloud());

	std::vector<std::pair<int, int>> newConstraints;
	std::vector<Eigen::Vector3f> allCentroids;
	std::vector<std::vector<int>> enforce_updated = enforce;
	for (int cl = 0; cl < allClouds.size(); cl++){
		allCentroids.push_back(CloudTools::compute3DCentroid(allClouds[cl], denseSeg[cl], label));
	}

	BundleAdjustment ba;
	int dim = denseSeg[0].cols();//numHypo

	//make sure there are enough alignment sets,
	//CHECK THIS.
	motions.clear();
	for (int i = motions.size(); i < dim; i++){
		//alignmentSets.push_back(std::vector<Eigen::Matrix4f>());
		//alignmentSets[i].resize(allClouds.size(), Eigen::Matrix4f::Identity());
		//ranges.push_back(my_range(-1, -1));
		motions.push_back(motion(allClouds.size()));
	}

	std::cout << "Bundle adjusting weight set " << label << " numIt: " << maxItICP << "...\n";
	StopWatch s;
	s.restart();
	int i = 0;
	int superIt = 0;
	int numClouds = allClouds.size();

	tsdfTracking t(10.f, d);
	for (superIt = 0; superIt < 10; superIt++){
		BundleAdjustment::LM_params lm_params;
		lm_params.last_error = std::numeric_limits<float>::max();
		lm_params.lambda = 1;


		ba.pointToPlane_Bundle_step_LM<explainMe::MatrixXf_rm>(
			allClouds,
			denseSeg, label,
			motions[label].alignments, enforce_updated, central_cloud, maxItICP
			, alpha_point2plane, alpha_point2point,
			//0.1,
			1.0 / 255,
			cullingDist *std::pow(0.8, i), *d, lm_params);


		//alg->getSegmentedOrganizedCloudsAndAlignments(label, extractedClouds, alignments);
		t.doFusion(extractedClouds, motions[label].alignments, fusedCloud, std::string("D:/Temp/deleteMe_l") + std::to_string(label) + "_it" + std::to_string(superIt) + ".stl");
		Eigen::Matrix4f firstToCenter = motions[label].alignments[central_cloud] * motions[label].alignments[0].inverse();
		//fusion returns cloud in 0th frame.
		CloudTools::apply<explainMe::Cloud::Ptr>(firstToCenter, fusedCloud);


		//remove the fused cloud....
		if (allClouds.size() > numClouds){
			enforce_updated.pop_back();
			allClouds.pop_back();
			alignmentSets[label].pop_back();
			denseSeg.pop_back();
		}

		LoopClosureFinder loopFinder;
		//loopFinder.findConstraints(10, alignmentSets[label], allCentroids, *d,newConstraints);		
		loopFinder.findConstraints(allClouds.size(), alignmentSets[label], allCentroids, *d, newConstraints,-1, 0.7);//, 3* std::pow(2,i));		
		enforce_updated = enforce;
		for (int j = 0; j < newConstraints.size(); j++){
			enforce_updated[newConstraints[j].first].push_back(newConstraints[j].second);
		}//


		//constraint to the fused cloud. TODO: It does not have good normals...
		//TODO experiment: are loops actually needed if I have the reference?
		//Tempo hierarchical something?
		enforce_updated.push_back(std::vector<int>());
		for (int j = 0; j < allClouds.size(); j++){
			enforce_updated.back().push_back(j);
			//this type of constraints make it super slow, and not a good idea;
			//in a constraint i,j the cloud i is searched for nns (bit contraintuitive, but thats how it is.
			//as the fused mesh can't be embedded in the single cloud, the fused cloud should be searched for nns
			//not the single cloud.
			//enforce_updated[j].push_back(allClouds.size());
		}
		allClouds.push_back(fusedCloud);
		alignmentSets[label].push_back(Eigen::Matrix4f::Identity());
		denseSeg.push_back(Eigen::MatrixXf::Ones(fusedCloud->size(), label + 1)*2);// *std::pow(2, superIt));
	}


	//remove the fused cloud....
	if (allClouds.size() > numClouds){
		allClouds.pop_back();
		alignmentSets[label].pop_back();
	}

	s.stop();

	std::cout << "Total time (motionHypotheses registration): " << s.total() << "\n";
	//update the range.... (bad)
	ranges[label].start = 0;
	ranges[label].stop = allClouds.size();

}*/

void MotionHypotheses::updateAlignmentsAndRange(
	int label,
	std::vector<explainMe::Cloud::Ptr> & allClouds,
	std::vector<explainMe::MatrixXf_rm> & denseSeg,
	std::vector<mySimpleKdTree> & allKdTrees,
	int central_cloud,
	//the parameters...
	int maxItICP,
	float alpha_point2plane,
	float alpha_point2point,
	float cullingDist,
	RegistrationType t, 
	int numLoopClosureSearches, int numPointsToSample, bool start_with_loopclosures)
{
	StopWatch s;
	s.restart();

	//bool start_with_loopclosures = false;
	//cloud correspondences to optimize matching on.
	std::vector<std::vector<int>> enforce;
	for (int i = 0; i < allClouds.size(); i++){
		enforce.push_back(std::vector<int>());
		//enforce.back().push_back(i+1);
		//allow to control if forward only!
		/*if (i >0){
			enforce.back().push_back(i - 1);
			}//*/
		if (i < allClouds.size() - 1){
			enforce.back().push_back(i + 1);
		}
		if (i < allClouds.size() - 3){
			enforce.back().push_back(i + 3);
		}
	}


	std::vector<std::pair<int, int>> newConstraints;
	std::vector<Eigen::Vector3f> allCentroids;
	std::vector<std::vector<int>> enforce_updated = enforce;
	for (int cl = 0; cl < allClouds.size(); cl++){
		allCentroids.push_back(CloudTools::compute3DCentroid(allClouds[cl], denseSeg[cl], label));
	}

	BundleAdjustment ba;
	int dim = denseSeg[0].cols();//numHypo

	//make sure there are enough alignment sets,
	//alignmentSets.clear(); motions.clear(); //this reinits them as identity.
	for (int i = motions.size(); i < dim; i++){
		//motions.push_back(motion(allClouds.size()));
		motions.push_back(motion(initialGuess, allClouds.size()));
	}


	//alternative: start with loop closures right away:
	if(start_with_loopclosures){	
		LoopClosureFinder loopFinder;
		//loopFinder.findConstraints(10, alignmentSets[label], allCentroids, *d,newConstraints);		
		//allow to contol num new constraints!!!! /2 here.
		loopFinder.findConstraints(allClouds.size(), motions.at(label).alignments, allCentroids, *d, newConstraints);//, 3* std::pow(2,i));		

		enforce_updated = enforce;
		for (int j = 0; j < newConstraints.size(); j++){
			enforce_updated[newConstraints[j].first].push_back(newConstraints[j].second);
		}//*/
	}


	//build6D trees
	float alpha_color = 1.0 / 255;
	typedef Eigen::Matrix<float, 6, 1> Vector6f;
	Vector6f scales; scales << 1, 1, 1, alpha_color, alpha_color, alpha_color;
	std::vector<mySimpleKdTree_ndim<6>> allTrees_6D(allClouds.size(), mySimpleKdTree_ndim<6>(scales));
#pragma omp parallel
	{
#pragma omp for
		for (int i = 0; i <allClouds.size(); i++)
		{
			simpleTreeTools::setData<explainMe::Cloud::Ptr>(allTrees_6D[i], allClouds[i]);
			allTrees_6D[i].buildTree();
		}
	}

	std::cout << "Bundle adjusting weight set " << label << "/" << dim << " numIt: " << maxItICP << "...\n";

	int i = 0;
	int pyrLevel = 2;
	float factor = (numLoopClosureSearches == 0 ? 1 : std::pow(0.1, 1.0 / (numLoopClosureSearches)));//(factor = 0.8)

	BundleAdjustment::LM_params lm_params;
	for (pyrLevel = numLoopClosureSearches; pyrLevel >= 0; pyrLevel--){
		lm_params.last_error = std::numeric_limits<float>::max();
		lm_params.lambda = 1;

		switch (t)
		{
		case MotionHypotheses::ICP_FANCY_6D:

			ba.LM_BA_fast<explainMe::MatrixXf_rm, mySimpleKdTree_ndim<6>, p_ndim<6>, simpleNNFinder_ndim<6>>(
				allClouds,
				allTrees_6D,
				denseSeg, label,
				motions.at(label).alignments, enforce_updated, central_cloud, maxItICP
				, alpha_point2plane, alpha_point2point,
				//0.1,
				1.0 / 255,
				cullingDist *std::pow(factor, i), *d, lm_params, numPointsToSample);
			break;
		case MotionHypotheses::ICP_FANCY_SLOW:
			ba.pointToPlane_Bundle_step_LM<explainMe::MatrixXf_rm>(
				allClouds,
				denseSeg, label,
				motions.at(label).alignments, enforce_updated, central_cloud, maxItICP
				, alpha_point2plane, alpha_point2point,
				//0.1,
				1.0 / 255,
				cullingDist *std::pow(factor, i), *d, lm_params);/**/
			break;
		default:
			ba.LM_BA_fast<explainMe::MatrixXf_rm, mySimpleKdTree, pxyz, simpleNNFinder>(
				allClouds,
				allKdTrees,
				denseSeg, label,
				motions.at(label).alignments, enforce_updated, central_cloud, maxItICP
				, alpha_point2plane, alpha_point2point,
				//0.1,
				1.0 / 255,
				cullingDist *std::pow(factor, i), *d, lm_params, numPointsToSample);
			break;
		}
		motions.at(label).quality = lm_params.avg_error;

		LoopClosureFinder loopFinder;
		//loopFinder.findConstraints(10, alignmentSets[label], allCentroids, *d,newConstraints);		
		//allow to contol num new constraints!!!! /2 here.
		loopFinder.findConstraints(allClouds.size() , motions.at(label).alignments, allCentroids, *d, newConstraints);//, 3* std::pow(2,i));		

		enforce_updated = enforce;
		for (int j = 0; j < newConstraints.size(); j++){
			enforce_updated[newConstraints[j].first].push_back(newConstraints[j].second);
		}//*/

		//decrease culling distance.
		i++;
	}
	s.stop();

	std::cout << "Total time (motionHypotheses registration): " << s.total() << "\n";
	//update the range.... (bad)... they are somewhat unknown...
	motions.at(label).range.start = 0;
	motions.at(label).range.stop = allClouds.size();

}

void MotionHypotheses::updateAlignmentsAndRange_incremental(
	int label,
	std::vector<explainMe::Cloud::Ptr> & allClouds,
	std::vector<explainMe::MatrixXf_rm> & denseSeg,
	std::vector<mySimpleKdTree> & allKdTrees,
	int central_cloud,
	//the parameters...
	int maxItICP,
	float alpha_point2plane,
	float alpha_point2point,
	float cullingDist)
{
	//cloud correspondences to optimize matching on.
	
	BundleAdjustmentV2<explainMe::MatrixXf_rm> ba(NULL);
	int dim = denseSeg[0].cols();//numHypo

	//make sure there are enough alignment sets,
	//alignmentSets.clear();
	for (int i = motions.size(); i < dim; i++){
		motions.push_back(motion(initialGuess,allClouds.size()));
	}

	std::cout << "Bundle adjusting weight set " << label << " numIt: " << maxItICP << "...\n";
	BundleAdjustmentV2<explainMe::MatrixXf_rm>::params params;
	BundleAdjustmentV2<explainMe::MatrixXf_rm>::data data;
		
		params.last_error = std::numeric_limits<float>::max();
		params.lambda = 1;
		params.alpha_point2point = alpha_point2point;
		params.alpha_point2plane = alpha_point2plane;
		params.alpha_color = 1.0 / 255;
		params.culling_dist = cullingDist;
		params.numIterations = maxItICP;

		data.clouds = allClouds;
		data.referenceCloud = central_cloud;
		data.weights = denseSeg;
		data.d = NULL;

	ba.pointToPlane_incremental(params, data, label, motions.at(label).alignments);

	//update the range.... (bad)
	motions.at(label).range.start = 0;
	motions.at(label).range.stop = allClouds.size();

}


void MotionHypotheses::updateAlignmentsAndRange_tweaked(
		int label,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		int central_cloud,
				//the parameters...
		int maxItICP,
		float alpha_point2plane,
		float alpha_point2point,
		float cullingDist,
		int numberOfLoopSearches
		)
{
	//cloud correspondences to optimize matching on.
	std::vector<std::vector<int>> enforce;
	for(int i = 0; i < allClouds.size()-1; i++){
		enforce.push_back(std::vector<int>());
		enforce.back().push_back(i+1);
	}
	enforce.push_back(std::vector<int>()); //empty constraint for last cloud.

	std::vector<std::pair<int,int>> newConstraints;
	std::vector<Eigen::Vector3f> allCentroids;
	std::vector<std::vector<int>> enforce_updated = enforce;
	for(int cl = 0; cl < allClouds.size(); cl++){
		allCentroids.push_back(CloudTools::compute3DCentroid(allClouds[cl],denseSeg[cl], label));
	}

	BundleAdjustment ba;
	int dim = denseSeg[0].cols();//numHypo

	//make sure there are enough alignment sets,
	//alignmentSets.clear();
	for(int i = motions.size(); i < dim; i++){
		motions.push_back(motion(initialGuess,allClouds.size()));
	}
	
	std::cout << "Bundle adjusting weight set " << label << " numIt: " << maxItICP << "...\n";

	StopWatch s;
	s.restart();
	for(int i = 0; i <numberOfLoopSearches; i++){
		std::cout << "BA : iteration" << i << "\n";
		/** /
		ba.pointToPlane_Bundle_step_r<explainMe::MatrixXf_rm>(
			allClouds,denseSeg, label, 
			alignmentSets[label],enforce_updated,central_cloud, maxItICP, alpha_point2plane, alpha_point2point,
			cullingDist );//*std::pow(0.8,i));			
		/*/
		ba.pointToPlane_Bundle_step_color<explainMe::MatrixXf_rm>(
			allClouds,denseSeg, label, 
			motions.at(label).alignments, enforce_updated, central_cloud, maxItICP, alpha_point2plane, alpha_point2point,
			//0.1,
			1.0/255,
			cullingDist *std::pow(0.8,i));/**/

		LoopClosureFinder loopFinder;
		//loopFinder.findConstraints(10, alignmentSets[label], allCentroids, *d,newConstraints);		
		loopFinder.findConstraints(allClouds.size(), motions.at(label).alignments, allCentroids, *d, newConstraints);//, 3* std::pow(2,i));		

		enforce_updated = enforce;
		for(int j = 0; j < newConstraints.size(); j++){
			enforce_updated[newConstraints[j].first].push_back(newConstraints[j].second);
		}

		//make it symmetric.
		/*auto tmp = enforce_updated;
		for(int j = 0; j < tmp.size(); j++){
			//std::cout << j << ":";
			for(int k= 0; k < tmp[j].size(); k++){
				enforce_updated[tmp[j][k]].push_back(j);
				//std::cout << 
			}
		}//*/

	}
	s.stop();

	std::cout << "Total time (motionHypotheses registration): " << s.total() << "\n";
	/*//trying out loopclosure detection.
	LoopClosureFinder loopFinder;
	//loopFinder.findConstraints(10, alignmentSets[label], allCentroids, *d,newConstraints);		
	loopFinder.findConstraints(allClouds.size(), alignmentSets[label], allCentroids, *d,newConstraints);		

	for(int j = 0; j < newConstraints.size(); j++){
		enforce_updated[newConstraints[j].first].push_back(newConstraints[j].second);
	}
	//using colors non-locally might not be smart.
	//ba.pointToPlane_Bundle_step_color<explainMe::MatrixXf_rm>(
	ba.pointToPlane_Bundle_step_r<explainMe::MatrixXf_rm>(
		allClouds,denseSeg, label, 
		alignmentSets[label],enforce_updated,central_cloud, maxItICP, alpha_point2plane, alpha_point2point,
	//	0.1,
		cullingDist);
	std::cout << "done computing alignments.\n";*/


	//update the range.... (bad)
	motions.at(label).range.start = 0;
	motions.at(label).range.stop = allClouds.size();

}

void MotionHypotheses::updateAlignmentsAndRange_of(
		int label,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		std::vector<std::vector<Eigen::MatrixXi>>& allI, 
		std::vector<std::vector<Eigen::MatrixXi>>& dI_dx, 
		std::vector<std::vector<Eigen::MatrixXi>>& dI_dy,
		std::vector<std::vector<Eigen::MatrixXf>>  smoothed_depth,
		bool usingDepthAsIntensities,
		int central_cloud,
				//the parameters...
		int maxItICP,
		float alpha_point2plane,
		float alpha_point2point,
		float alpha_of,
		float cullingDist,
		RangeSensorDescription & d)
{
	

	//cloud correspondences to optimize matching on.
	std::vector<std::vector<int>> enforce;
	for(int i = 0; i < allClouds.size(); i++){
		enforce.push_back(std::vector<int>());
		//enforce.back().push_back(i+1);
		if(i >0){
			enforce.back().push_back(i-1);
		}
		if(i < allClouds.size() -1){
			enforce.back().push_back(i+1);
		}
	}

	std::vector<std::pair<int,int>> newConstraints;
	std::vector<Eigen::Vector3f> allCentroids;
	std::vector<std::vector<int>> enforce_updated = enforce;
	for(int cl = 0; cl < allClouds.size(); cl++){
		allCentroids.push_back(CloudTools::compute3DCentroid(allClouds[cl],denseSeg[cl], label));
	}

	BundleAdjustment ba;
	int dim = denseSeg[0].cols();//numHypo

	//make sure there are enough alignment sets,
	motions.clear();
	for(int i = motions.size(); i < dim; i++){
		motions.push_back(motion(initialGuess,allClouds.size()));
	}
	
	std::cout << "Bundle adjusting weight set " << label << " numIt: " << maxItICP << "...\n";
	StopWatch s;
	s.restart();
	int i=0;
	int pyrLevel = 2;
			
	for(pyrLevel = allI[0].size() -1; pyrLevel>=0; pyrLevel--){
//	for(int i = 0; i <10; i++){
//		std::cout << "BA : iteration" << i << "\n";
		/** /
		ba.pointToPlane_Bundle_step_r<explainMe::MatrixXf_rm>(
			allClouds,denseSeg, label, 
			alignmentSets[label],enforce_updated,central_cloud, maxItICP, alpha_point2plane, alpha_point2point,
			cullingDist );//*std::pow(0.8,i));			
		/*/
		BundleAdjustment::LM_params lm_params;
		lm_params.last_error = std::numeric_limits<float>::max();
		lm_params.lambda = 1;

		//It should not matter if the loop is done inside or outside... but it does. Figure out why.
		//for(int anIt = 0; anIt <maxItICP; anIt ++)
		{
		ba.pointToPlane_Bundle_step_of<explainMe::MatrixXf_rm>(
			allClouds,allI, dI_dx, dI_dy, smoothed_depth, usingDepthAsIntensities,
			pyrLevel, 
			denseSeg, label, 
			motions.at(label).alignments,enforce_updated,central_cloud, maxItICP
			, alpha_point2plane, alpha_point2point, alpha_of,
			//0.1,
			1.0/255,
			cullingDist *std::pow(0.8,i), d, lm_params);/**/
		}

		LoopClosureFinder loopFinder;
		//loopFinder.findConstraints(10, alignmentSets[label], allCentroids, *d,newConstraints);		
		loopFinder.findConstraints(allClouds.size(), motions.at(label).alignments, allCentroids, d, newConstraints);//, 3* std::pow(2,i));		

		enforce_updated = enforce;
		for(int j = 0; j < newConstraints.size(); j++){
			enforce_updated[newConstraints[j].first].push_back(newConstraints[j].second);
		}//*/

	}
	s.stop();

	std::cout << "Total time (motionHypotheses registration): " << s.total() << "\n";
	/*//trying out loopclosure detection.
	LoopClosureFinder loopFinder;
	//loopFinder.findConstraints(10, alignmentSets[label], allCentroids, *d,newConstraints);		
	loopFinder.findConstraints(allClouds.size(), alignmentSets[label], allCentroids, *d,newConstraints);		

	for(int j = 0; j < newConstraints.size(); j++){
		enforce_updated[newConstraints[j].first].push_back(newConstraints[j].second);
	}
	//using colors non-locally might not be smart.
	//ba.pointToPlane_Bundle_step_color<explainMe::MatrixXf_rm>(
	ba.pointToPlane_Bundle_step_r<explainMe::MatrixXf_rm>(
		allClouds,denseSeg, label, 
		alignmentSets[label],enforce_updated,central_cloud, maxItICP, alpha_point2plane, alpha_point2point,
	//	0.1,
		cullingDist);
	std::cout << "done computing alignments.\n";*/


	//update the range.... (bad)
	motions.at(label).range.start = 0;
	motions.at(label).range.stop = allClouds.size();
}

void MotionHypotheses::step1_bundleAdjustAll(
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		int central_cloud,
				//the parameters...
		int maxItICP,
		float alpha_point2plane,
		float alpha_point2point,
		float cullingDist)
{
	//cloud correspondences to optimize matching on.
	std::vector<std::vector<int>> enforce;
	for(int i = 0; i < allClouds.size()-1; i++){
		enforce.push_back(std::vector<int>());
		enforce.back().push_back(i+1);
	}

	BundleAdjustment ba;
	int dim = denseSeg[0].cols();//numHypo
	motions.clear();
	for(int i = 0; i < dim; i++){
		motions.push_back(motion(initialGuess,allClouds.size()));
		
		std::cout << "Bundle adjusting weight set " << i << " numIt: " << maxItICP << "...\n";
		//ba.pointToPlane_Bundle_step_r<explainMe::MatrixXf_rm>(
		//	allClouds,denseSeg, i, 
		//	alignmentSets[i],enforce,central_cloud, maxItICP, alpha_point2plane, alpha_point2point,
		//	cullingDist);
		/*
		updateAlignmentsAndRange_tweaked(i,allClouds,denseSeg,allKdTrees,central_cloud,maxItICP, alpha_point2plane, alpha_point2point, cullingDist);
	}
/*/		ba.pointToPlane_Bundle_step_color<explainMe::MatrixXf_rm>(
			allClouds,denseSeg, i, 
			motions.at(i).alignments, enforce, central_cloud, maxItICP, alpha_point2plane, alpha_point2point,
			0.1,
			cullingDist);

			std::cout << "done.\n";
	}

	//trying out loopclosure detection.
	for(int i= 0; i < dim; i++){
		std::cout << "Loopclosures object " << i;
		LoopClosureFinder loopFinder;
		std::vector<std::pair<int,int>> newConstraints;
		std::vector<Eigen::Vector3f> allCentroids;
		std::vector<std::vector<int>> enforce_updated = enforce;
		for(int cl = 0; cl < allClouds.size(); cl++){
			allCentroids.push_back(CloudTools::compute3DCentroid(allClouds[cl],denseSeg[cl], i));
		}
		//loopFinder.findConstraints(10, alignmentSets[i], allCentroids, *d,newConstraints);		
		loopFinder.findConstraints(allClouds.size(), motions.at(i).alignments, allCentroids, *d, newConstraints);

		for(int j = 0; j < newConstraints.size(); j++){
			enforce_updated[newConstraints[j].first].push_back(newConstraints[j].second);
		}
		//using colors non-locally might not be smart.
		ba.pointToPlane_Bundle_step_r<explainMe::MatrixXf_rm>(
			allClouds,denseSeg, i, 
			motions.at(i).alignments, enforce_updated, central_cloud, maxItICP, alpha_point2plane, alpha_point2point,
			//0.1,
			cullingDist);
		std::cout << "done.\n";

	}//*/

}

std::vector<hyp_range_flow> MotionHypotheses::findMotionSegments(
	std::vector<Eigen::RowVectorXf>  &frame_support,
	std::vector<Eigen::MatrixXf>  &frame_shift_mat,
	std::vector<Eigen::MatrixXf> &frame_shift_mat_fwd
	)
{
	//parameter
	float threshold = 150;
	int numHyp = frame_support[0].cols();
	int numFrames = frame_support.size();

	std::cout << "finding motion segments: starting with " << numHyp << " hpys on " << numFrames << " frames.\n";
	// property to hold for a segment to be counted as alive in a frame
	auto isAlive = [&](int frm, int hyp)->bool{
		//points being born					//points dying
		return frame_support[frm](hyp) > threshold + frame_shift_mat[frm].col(hyp).sum() + frame_shift_mat_fwd[frm].col(hyp).sum();
	};

	//collect all temporal segments
	std::vector<hyp_range_flow> splitHyps;
	for (int hyp = 0; hyp < numHyp; hyp++){
		hyp_range_flow r;
		r.stop = -1;
		r.prev = NULL;
		r.next = NULL;
		while (r.stop < numFrames){
			r.start = r.stop + 1;
			//advance to first valid frame
			while (r.start < numFrames && !isAlive(r.start, hyp)){
				r.start++;
			}
			//advance to first unsupported frame
			r.stop = r.start;
			while (r.stop < numFrames && isAlive(r.stop, hyp)){
				r.stop++;
			}
			r.hyp = hyp;
			//we accept the segment if a segment has been found & it is long enough (second cond for very short sequences)
			if (r.start < numFrames && (r.stop - r.start > 3 || r.stop - r.start >numFrames/4)){
				splitHyps.push_back(r);
				std::cout << "Hyp " << hyp << "[" << r.start << "," << r.stop << "]\n";
			}
		}
	}

	//It is possible that the max shift comes from a hypothesis that was too short to be considered...
	//Short hyps need to be excluded due to noise in the support.
	Eigen::VectorXf multiplier_to_exclude_short_hyps = Eigen::VectorXf::Zero(frame_shift_mat[0].cols());
	for (auto & r : splitHyps){
		multiplier_to_exclude_short_hyps(r.hyp) = 1;
	}

	std::cout << "Found: " << splitHyps.size() << " motion subsegments\n";
	//link the hyps to previous and next best hyp.
	for (auto & r : splitHyps){

		std::cout << "linking : " << r.hyp << "[" << r.start << "," << r.stop << "]";
		Eigen::VectorXf flow_start, flow_end;
		flow_start = frame_shift_mat[r.start].col(r.hyp);

		for (int frm = std::max(0, r.start - 3); frm < std::min(r.start + 25, r.stop); frm++){
			flow_start = (flow_start.array() > frame_shift_mat[frm].col(r.hyp).array()).select(flow_start, frame_shift_mat[frm].col(r.hyp));
		}

		flow_end = frame_shift_mat_fwd[r.stop - 1].col(r.hyp);
		for (int frm = std::max(r.start, r.stop - 25); frm < std::min(numFrames, r.stop + 3); frm++){
			flow_end = (flow_end.array() > frame_shift_mat_fwd[frm].col(r.hyp).array()).select(flow_end, frame_shift_mat_fwd[frm].col(r.hyp));
		}

		//exclude
		flow_start.array() *= multiplier_to_exclude_short_hyps.array();
		flow_start.array() += (multiplier_to_exclude_short_hyps.array() - 1);
		flow_end.array() *= multiplier_to_exclude_short_hyps.array();
		flow_end.array() += (multiplier_to_exclude_short_hyps.array() - 1);

		int nextHyp, prevHyp, bestDist;
		flow_start.maxCoeff(&prevHyp);
		flow_end.maxCoeff(&nextHyp);

		std::cout << "Flows: \n" <<flow_start.transpose() << "\n";
		std::cout << flow_end.transpose() << "\n";
		std::cout << "<- : " << (r.start == 0 ? -1 : prevHyp) << ", ->: " << (r.stop == numFrames ? -1 : nextHyp) << "...";

		//link to best guess for previous hypothesis
		if (r.stop == numFrames){
			r.next = NULL;
		}
		else{
			bestDist = numFrames;
			for (auto & r_other : splitHyps){
				if (r_other.hyp == nextHyp){
					if (r_other.start <= r.stop && r_other.stop > r.stop){
						bestDist = 0;
						r.next = &r_other;
					}
					else if (std::min(std::abs(r.stop - r_other.start), std::abs(r.stop - r_other.stop)) < bestDist){
						bestDist = std::min(std::abs(r.stop - r_other.start), std::abs(r.stop - r_other.stop));
						r.next = &r_other;
					}
				}
			}
		}

		//link to best guess for next hypothesis
		if (r.start == 0){
			r.prev = NULL;
		}
		else{
			bestDist = numFrames;
			for (auto & r_other : splitHyps){
				if (r_other.hyp == prevHyp){
					if (r_other.start <= r.start && r_other.stop > r.start){
						bestDist = 0;
						r.prev = &r_other;
					}
					else if (std::min(std::abs(r.start - r_other.start), std::abs(r.start - r_other.stop)) < bestDist){
						bestDist = std::min(std::abs(r.start - r_other.start), std::abs(r.start - r_other.stop));
						r.prev = &r_other;
					}
				}
			}
		}
		std::cout << "...linked,";
	}
	std::cout << "done.";

	return splitHyps;
}

void MotionHypotheses::findTempShifts_and_updateDenseSeg(
	//the data
	std::vector<explainMe::Cloud::Ptr> & allClouds,
	std::vector<explainMe::MatrixXf_rm> & denseSeg,
	std::vector<mySimpleKdTree> & allKdTrees,
	int genK,
	float lambda_outlier
	)
{
	std::cout << "Detecting shifts to generate combined segments!";
	//First: detect shifts 

	//Second: List segments.

	//Third: Identify segment pairs.

	//Fourth: Like for outlier hyps: add new dense Layers.

	//There is quite a bit of duplicate computation: the need to compute statistics and segments. 
	//BUT: we'll add a few hyps (combinations) that will need to be combined
	//So for testing I'll compute everything twice.

	//buffers
	std::vector<Eigen::MatrixXf>  frame_shift_mat, frame_shift_mat_fwd;
	std::vector<Eigen::RowVectorXf>  frame_abs_shift;
	std::vector<Eigen::RowVectorXf>  frame_support;

	int numHyp = denseSeg[0].cols();
	int numFrames = denseSeg.size();

	std::cout << "start: shifts, Having " << numHyp << "hyps; Denseseg has " << numFrames << " frames\n";

	//Collect statistics;
	computeDwDt_etc(allClouds, denseSeg, allKdTrees,
		frame_shift_mat, frame_shift_mat_fwd, frame_abs_shift);
	computeSupport(denseSeg, frame_support);
	std::vector<hyp_range_flow> splitHyps = findMotionSegments(frame_support, frame_shift_mat, frame_shift_mat_fwd);

	std::vector<std::vector<int>> result_frame_from_to;
	find_top_k_labelShifts(
		frame_shift_mat,
		genK, lambda_outlier,
		result_frame_from_to);

	//make sure no more than some number of hyps will be generated
	// if: randomly select a few shifts.
	int max_num_hyps = 32;
	if (splitHyps.size() > max_num_hyps){
		result_frame_from_to.clear();
	}
	else if (splitHyps.size() + result_frame_from_to.size() > max_num_hyps){
		int numNew = max_num_hyps - splitHyps.size();
		std::random_shuffle(result_frame_from_to.begin(), result_frame_from_to.end());
		result_frame_from_to.resize(numNew);
	}


	//for each shift: find the two segments & generate new dense segmentation layer
	int oldDim = denseSeg[0].cols();
	int newHyp = 0;
	//allocate space for the new shift combinations.
	for (auto & seg : denseSeg){
		seg.conservativeResize(seg.rows(),oldDim + result_frame_from_to.size());
	}
	for (int shift_idx = 0; shift_idx < result_frame_from_to.size(); shift_idx++){
		int from = result_frame_from_to[shift_idx][1]; int to = result_frame_from_to[shift_idx][2];
		int frame = result_frame_from_to[shift_idx][0];

		//search the segments the shifts lies in.
		int bestDist_first = numFrames, bestDist_second = numFrames; 
		hyp_range_flow *first = NULL, *second = NULL;
		for (auto & seg : splitHyps){
			if (seg.hyp == from && (seg.start <= frame && seg.stop > frame || std::abs(frame - seg.stop) < bestDist_first)){
				bestDist_first = std::abs(frame - seg.stop);
				first = &seg;
			}
			if (seg.hyp == to && (seg.start <= frame && seg.stop > frame || std::abs(frame - seg.start) < bestDist_second)){
				bestDist_second = std::abs(frame - seg.start);
				second = &seg;
			}
		}

		//they were found & the combination would not be generated by split and combine.
		if (first != NULL && second != NULL && first->next != second && second->prev!= first){
			std::cout << "Shift: " << from << " => " << to << " @ " << frame << "Realized as: \n"
				<< first->hyp << "[" << first->start << "," << first->stop << "] => " << second->hyp << "[" << second->start << "," << second->stop << "]\n";
			for (int i = 0; i < denseSeg.size(); i++){
				//std::cout << result_frame_from_to[newHyp][0] << ":" << result_frame_from_to[newHyp][1] << "->" << result_frame_from_to[newHyp][2] << "\n";
				int take_over_from_label = (i < result_frame_from_to[newHyp][0] ? result_frame_from_to[newHyp][1] : result_frame_from_to[newHyp][2]);
				int other_label = (i < result_frame_from_to[newHyp][0] ? result_frame_from_to[newHyp][2] : result_frame_from_to[newHyp][1]);
				denseSeg[i].col(oldDim + newHyp) = denseSeg[i].col(take_over_from_label);

				//around shift set to max of the two.
				if (std::abs(i - result_frame_from_to[newHyp][0]) <= 1){
					denseSeg[i].col(oldDim + newHyp) = (denseSeg[i].col(oldDim + newHyp).array() > denseSeg[i].col(other_label).array()).select(denseSeg[i].col(oldDim + newHyp), denseSeg[i].col(other_label).array());
				}
			}
			newHyp++;
		}		
		else{
			std::cout << "Ignoring shift (should be handled by split and combine): " << from << " => " << to << " @ " << frame << "\n";
		}
	}
		
	//deallocate space not used for the new shift combinations.
	for (auto & seg : denseSeg){
		seg.conservativeResize(seg.rows(), oldDim + newHyp);
	}	
	std::cout << "End shifts " << numHypotheses() << "hyps; Denseseg has " << denseSeg[0].cols() << " Hypotheses\n";
}

std::vector<hyp_range_flow> MotionHypotheses::findAndLinkMotionsSegments(
	//the data
	std::vector<explainMe::Cloud::Ptr> & allClouds,
	std::vector<explainMe::MatrixXf_rm> & denseSeg,
	std::vector<mySimpleKdTree> & allKdTrees)
{

	//buffers
	std::vector<Eigen::MatrixXf>  frame_shift_mat, frame_shift_mat_fwd;
	std::vector<Eigen::RowVectorXf>  frame_abs_shift;
	std::vector<Eigen::RowVectorXf>  frame_support;

	int numHyp = denseSeg[0].rows();
	int numFrames = denseSeg.size();

	//Collect statistics;
	computeDwDt_etc(allClouds, denseSeg, allKdTrees,
		frame_shift_mat, frame_shift_mat_fwd, frame_abs_shift);
	computeSupport(denseSeg, frame_support);
	std::vector<hyp_range_flow> splitHyps = findMotionSegments(frame_support, frame_shift_mat, frame_shift_mat_fwd);

	subresults_ForVisualizationOnly.supports = frame_support; subresults_ForVisualizationOnly.supportThreshold = 150;
	subresults_ForVisualizationOnly.massFlows = frame_shift_mat; subresults_ForVisualizationOnly.massFlowsLoss = frame_shift_mat_fwd;

	/*// property to hold for a segment to be counted as alive in a frame
	auto & isAlive = [&](int frm, int hyp)->bool{
		//points being born					//points dying
		return frame_support[frm](hyp) > threshold + frame_shift_mat[frm].col(hyp).sum() + frame_shift_mat_fwd[frm].col(hyp).sum();
	};

	//collect all temporal segments
	std::vector<hyp_range_flow> splitHyps;
	for (int hyp = 0; hyp < numHyp; hyp++){
		hyp_range_flow r;
		r.stop = -1;
		while (r.stop < numFrames){
			r.start = r.stop + 1;
			//advance to first valid frame
			while (r.start < numFrames && !isAlive(r.start, hyp)){
				r.start++;
			}
			//advance to first unsupported frame
			r.stop = r.start;
			while (r.stop < numFrames && isAlive(r.stop, hyp)){
				r.stop++;
			}
			r.hyp = hyp;
			//we accept the segment if a segment has been found & 
			if (r.start < numFrames && r.stop - r.start > 3){
				splitHyps.push_back(r);
				std::cout << "Hyp " << hyp << "[" << r.start << "," << r.stop << "]\n";
			}
		}
	}

	//link the hyps to previous and next best hyp.
	for (auto & r : splitHyps){

		std::cout << "linking : " << r.hyp << "[" << r.start << "," << r.stop << "]";
		Eigen::VectorXf flow_start, flow_end;
		flow_start = frame_shift_mat[r.start].col(r.hyp);
		
		for (int frm = std::max(0, r.start - 3); frm < std::min(r.start + 25, r.stop); frm++){
			flow_start = (flow_start.array() > frame_shift_mat[frm].col(r.hyp).array()).select(flow_start, frame_shift_mat[frm].col(r.hyp));
		}
		
		flow_end = frame_shift_mat_fwd[r.stop-1].col(r.hyp);
		for (int frm = std::max(r.start, r.stop - 25); frm < std::min(numFrames, r.stop + 3); frm++){
			flow_end = (flow_end.array() > frame_shift_mat_fwd[frm].col(r.hyp).array()).select(flow_end, frame_shift_mat_fwd[frm].col(r.hyp));
		}


		int nextHyp, prevHyp, bestDist;
		flow_start.maxCoeff(&prevHyp);
		flow_end.maxCoeff(&nextHyp);

		std::cout << "<- : " << (r.start == 0 ? -1 : prevHyp) << ", ->: " << (r.stop == numFrames? -1 : nextHyp) << "...";

		//link to best guess for previous hypothesis
		if (r.stop == numFrames){
			r.next = NULL;
		}
		else{
			bestDist = numFrames;
			for (auto & r_other : splitHyps){
				if (r_other.hyp == nextHyp){
					if (r_other.start <= r.stop && r_other.stop > r.stop){
						bestDist = 0;
						r.next = &r_other;
					}
					else if (std::min(std::abs(r.stop - r_other.start), std::abs(r.stop - r_other.stop)) < bestDist){
						bestDist = std::min(std::abs(r.stop - r_other.start), std::abs(r.stop - r_other.stop));
						r.next = &r_other;
					}
				}
			}
		}

		//link to best guess for next hypothesis
		if (r.start == 0){
			r.prev = NULL;
		}
		else{
			bestDist = numFrames;
			for (auto & r_other : splitHyps){
				if (r_other.hyp == prevHyp){
					if (r_other.start <= r.start && r_other.stop > r.start){
						bestDist = 0;
						r.prev = &r_other;
					}
					else if (std::min(std::abs(r.start - r_other.start), std::abs(r.start - r_other.stop)) < bestDist){
						bestDist = std::min(std::abs(r.start - r_other.start), std::abs(r.start - r_other.stop));
						r.prev = &r_other;
					}
				}
			}
		}
		std::cout << "...linked,";
	}
	std::cout << "done.";*/
	return splitHyps;
}

void MotionHypotheses::splitAndCombineHypotheses_and_updataDenseSeg(
	//the data
	std::vector<explainMe::Cloud::Ptr> & allClouds,
	std::vector<explainMe::MatrixXf_rm> & denseSeg,
	std::vector<mySimpleKdTree> & allKdTrees
	)
{
	int numFrames = denseSeg.size();
	auto splitHyps = findAndLinkMotionsSegments(allClouds, denseSeg, allKdTrees);


	//for debug:: cout combinations.
	int idx_hyp = 0;
	for (auto & r : splitHyps)
	{
		if (r.prev != NULL && r.prev->next == &r){
			continue;
		}
		std::deque<hyp_range_flow> hyps;
		//search forward for correct segment
		std::cout << "-";
		std::cout << "Combining for " << r.hyp << "[" << r.start << ", " << r.stop << "]:\n";
		auto * curr = &r;
		int numTests = 0;
		int last_frame = curr->start;
		std::cout << ".";
		while (curr != NULL && numTests <splitHyps.size()){
			std::cout << "(";
			if (curr->stop > last_frame){
				hyps.push_back(*curr);
				last_frame = curr->stop;
				std::cout << "X";
			}
			std::cout << curr->hyp << "[" << curr->start << ", " << curr->stop << "]";
			std::cout << curr->next << ":" << (curr->next != NULL ? curr->next->hyp : -1) <<" \n";
			std::cout << ")";
			curr = curr->next;
			numTests++;
		}
		std::cout << "-";
		//search backward
		curr = r.prev;
		numTests = 0;
		if(curr != NULL) last_frame = curr->stop;
		while (curr != NULL && numTests <splitHyps.size()){
			if (curr->start < last_frame){
				hyps.push_front(*curr);
				last_frame = curr->start;
			}
			curr = curr->prev;
			numTests++;
		}

		std::cout << "New hyp no: " << idx_hyp << ".\n";
		for (auto & h : hyps){
			std::cout <<h.hyp << "[" << h.start << "," << h.stop << "] => ";
		}
		std::cout << " finished.\n";
		idx_hyp++;
	}

	//update the dense segmentation to correspond to combined hypotheses.
	//count the number of new Hyps:
	int numCombinedHyps = 0;
	for (auto & r : splitHyps){
		//others would create duplicates.
		if (r.prev == NULL || r.prev->next != &r){
			numCombinedHyps++;
		}
	}
	Eigen::MatrixXf newDenseSeg;
	for (int frm = 0; frm < numFrames; frm++){
		newDenseSeg = Eigen::MatrixXf(denseSeg[frm].rows(), numCombinedHyps);
		int new_hyp_idx = 0;
		for (auto & r : splitHyps){
			//vs duplicates
			if (r.prev != NULL && r.prev->next == &r){
				continue;
			}
			/*if (new_hyp_idx >= numCombinedHyps){
				continue;
			}*/

			newDenseSeg.col(new_hyp_idx).fill(0);

			//the current hypothesis.
			if (r.stop + 2 > frm && r.start - 2 <= frm){
				newDenseSeg.col(new_hyp_idx) =
					(newDenseSeg.col(new_hyp_idx).array() > denseSeg[frm].col(r.hyp).array()).select
					(newDenseSeg.col(new_hyp_idx), denseSeg[frm].col(r.hyp));
			}
			//search forward for correct segment
			if (r.start <= frm){
				auto * curr = &r;
				int numTests = 0;
				//to overlap the hyps: ALL hyps that come after a hyp stopping at the earliest two frames before before the frame can contribute...
				while (curr != NULL && curr->stop - 2 <= frm  && numTests < splitHyps.size()){
					curr = curr->next;
					if (curr != NULL && curr->stop + 2 > frm  /*&& curr->start -3 <= frm*/){
						newDenseSeg.col(new_hyp_idx) = 
							(newDenseSeg.col(new_hyp_idx).array() > denseSeg[frm].col(curr->hyp).array()).select
							(newDenseSeg.col(new_hyp_idx),			denseSeg[frm].col(curr->hyp));
					}
					numTests++;
				} 
				/*while (curr != NULL && curr->stop <= frm && numTests <splitHyps.size()){
					curr = curr->next;
					numTests++;
				}
				//no segment found
				if (curr == NULL || curr->stop <= frm){
					newDenseSeg.col(new_hyp_idx).fill(0);
				}
				else{
					newDenseSeg.col(new_hyp_idx) = denseSeg[frm].col(curr->hyp);
				}*/
			}
			//search backward
			if (r.start > frm){
				auto * curr = &r;
				int numTests = 0;
				//to overlap the hyps: ALL hyps that stop at the earliest two frames before before the frame
				while (curr != NULL && curr->start + 2 > frm  && numTests < splitHyps.size()){
					curr = curr->prev;
					if (curr != NULL && curr->start - 2 <= frm  /*&& curr->stp� +3 > frm*/){
						newDenseSeg.col(new_hyp_idx) =
							(newDenseSeg.col(new_hyp_idx).array() > denseSeg[frm].col(curr->hyp).array()).select
							(newDenseSeg.col(new_hyp_idx), denseSeg[frm].col(curr->hyp));
					}
					
					numTests++;
				}
				/*while (curr != NULL && curr->start > frm && numTests <splitHyps.size()){
					curr = curr->prev;
					numTests++;
				}
				//no segment found
				if (curr == NULL || curr->start > frm){
					newDenseSeg.col(new_hyp_idx).fill(0);
				}
				else{
					newDenseSeg.col(new_hyp_idx) = denseSeg[frm].col(curr->hyp);
				}*/
			}

			new_hyp_idx++;
		}
		denseSeg[frm] = newDenseSeg;
	}

	//update  the motions to match the number of new hypotheses.
	auto newMotions = std::vector<motion>();//motions; //makes no sense to keep the old motions.
	for (int i = newMotions.size(); i < numCombinedHyps; i++){
		newMotions.push_back(motion(initialGuess,allClouds.size()));
	}
	/*for (int r_idx = 0; r_idx < splitHyps.size(); r_idx++){
		newMotions[r_idx] = motions[splitHyps[r_idx].hyp];
	}
	for (int r_idx = 0; r_idx < splitHyps.size(); r_idx++){
		newMotions.at(r_idx).range.start = splitHyps[r_idx].start;
		newMotions.at(r_idx).range.stop = splitHyps[r_idx].stop;
	}*/

	motions = newMotions;
	std::cout << "End: splitAndCombineHypotheses_and_updataDenseSeg\n";
}

void MotionHypotheses::temporallySplitHypotheses_and_updateDenseSeg(
	//the data
	std::vector<explainMe::Cloud::Ptr> & allClouds,
	std::vector<explainMe::MatrixXf_rm> & denseSeg,
	std::vector<mySimpleKdTree> & allKdTrees)
{

	int numFrames = denseSeg.size();
	auto splitHyps = findAndLinkMotionsSegments(allClouds, denseSeg, allKdTrees);

//for debug:: cout combinations.
for (auto & r : splitHyps)
{
	std::cout << "Combination for " << r.hyp << "[" << r.start << "," << r.stop << "]:\n";
	std::deque<hyp_range_flow> hyps;
	//search forward for correct segment
	auto * curr = &r;
	int numTests = 0;
	int last_frame = curr->start;
	while (curr != NULL && numTests <splitHyps.size()){
		if (curr->stop > last_frame){
			hyps.push_back(*curr);
			last_frame = curr->stop;
		}
		curr = curr->next;
		numTests++;
	}

	//search backward
	std::cout << "backward...\n";
	curr = &r;
	numTests = 0;
	last_frame = curr->start;
	while (curr != NULL && numTests <splitHyps.size()){
		if (curr->start < last_frame){
			hyps.push_front(*curr);
			last_frame = curr->start;
		}
		curr = curr->prev;
		numTests++;
	}

	
	for (auto & h : hyps){
		std::cout << h.hyp << "[" << h.start << "," << h.stop << "] => ";
	}
}

	//update the dense segmentation to correspond to the split motions.
	Eigen::MatrixXf newDenseSeg;
	for (int i = 0; i < numFrames; i++){
		newDenseSeg = Eigen::MatrixXf(denseSeg[i].rows(), splitHyps.size());
		int r_idx = 0;
		for (auto & r : splitHyps){
			if (r.start <= i && i <= r.stop){
				newDenseSeg.col(r_idx) = denseSeg[i].col(r.hyp);
			}
			else{
				newDenseSeg.col(r_idx).fill(0);
			}
			r_idx++;
		}
		denseSeg[i] = newDenseSeg;
	}

	

	//update  the motions to match the number of new hypotheses.
	auto newMotions = motions;
	for (int i = newMotions.size(); i < splitHyps.size(); i++){
		//newMotions.push_back(motion(allClouds.size()));
		newMotions.push_back(motion(initialGuess,allClouds.size()));
	}
	for (int r_idx = 0; r_idx < splitHyps.size(); r_idx++){
		newMotions[r_idx] = motions[splitHyps[r_idx].hyp];
	}
	for (int r_idx = 0; r_idx < splitHyps.size(); r_idx++){
		newMotions.at(r_idx).range.start = splitHyps[r_idx].start;
		newMotions.at(r_idx).range.stop = splitHyps[r_idx].stop;
	}

	motions = newMotions;
}



void MotionHypotheses::step2_analyseDenseSegmentation(
		int genK,
		float lambda_outlier,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		std::vector<std::vector<int>> & result_frame_from_to,
		std::vector<hyp_range> & supportRanges,
		bool genHypByShift,
		bool genHypByCarryOver
	){
	std::vector<Eigen::MatrixXf>  frame_shift_mat, frame_shift_mat_fwd;
	std::vector<Eigen::RowVectorXf>  frame_abs_shift;
	std::vector<Eigen::RowVectorXf>  frame_support;

	//std::cout << "Collecting dwdt statistics...\n";
	computeDwDt_etc(allClouds, denseSeg, allKdTrees,
		frame_shift_mat, frame_shift_mat_fwd, frame_abs_shift);

	//std::cout << "done.Mat:\n";
	//std::cout << frame_shift_mat[3] << "\n shift per frame:\n" << frame_abs_shift[3] << "\n";
	//3.compute validity ranges of these segmentations. This already takes care of temporal undersegmentation.
	//std::cout << "compute ranges" << "\n";
	computeSupport(denseSeg, frame_support);
	//
	subresults_ForVisualizationOnly.supports = frame_support; subresults_ForVisualizationOnly.supportThreshold = 150; 
	subresults_ForVisualizationOnly.massFlows = frame_shift_mat; subresults_ForVisualizationOnly.massFlowsLoss = frame_shift_mat_fwd;
	supportRanges = computeValidityRanges(
		150,
		5,
		frame_support);
	//std::cout << "done.";


	//4. find new Hypotheses (with larger ranges) based on dwdt
	//std::cout << "Looking for label shifts....\n";
	if(genHypByShift){
		find_top_k_labelShifts(
			frame_shift_mat,
			genK, lambda_outlier,
			result_frame_from_to);
	}
	//std::cout << "done";

	//5. find new Hypotheses (with larger ranges) based on dying hypotheses.
	if(genHypByCarryOver){
		push_back_labelCarryOvers(frame_shift_mat, supportRanges,result_frame_from_to);
	}

	//remove exact duplicates.


}



void MotionHypotheses::computeHypothesesSetFromDenseSegmentation_segFirst(
	std::vector<explainMe::Cloud::Ptr> & allClouds,
	std::vector<explainMe::MatrixXf_rm> & denseSeg,
	std::vector<mySimpleKdTree> & allKdTrees,
	int central_cloud,
	//the parameters...
	int maxItICP,
	float alpha_point2plane,
	float alpha_point2point,
	float cullingDist,

	int genK, float lambda_outlier,
	bool genHypByShift,
	bool genHypByCarryOver,

	RegistrationType t
	){

	std::cout << "\n*****************************************************************************\n"
				   "| DOING MOTION ESTIMATION WITH SPLITandCOMBINE / NEEDS TO BE MADE PARAMETER |\n"
				   "*****************************************************************************\n";
	bool generateBySplitAndCombine = true; // new way => need to make it an argument...
	bool generateByShiftsBirthAndDeath = !generateBySplitAndCombine; // old way / arxiv submission.
	//generate and validate these hypotheses: 
	//2. compute supports and label shifts.
	if (generateBySplitAndCombine)
	{
		std::cout << "Having " << numHypotheses() << "hyps; Denseseg has " << denseSeg[0].cols() << " Hypotheses\n";
		this->findTempShifts_and_updateDenseSeg(
			allClouds, denseSeg, allKdTrees, genK, lambda_outlier
			);

		std::cout << "Before splitandcomb: Having " << numHypotheses() << "hyps; Denseseg has " << denseSeg[0].cols() << " Hypotheses\n";
		this->splitAndCombineHypotheses_and_updataDenseSeg(allClouds, denseSeg, allKdTrees);
		std::cout << "After splitandcomb:" << numHypotheses() << "hyps; Denseseg has " << denseSeg[0].cols() << " Hypotheses\n";
	}
	if (generateByShiftsBirthAndDeath)
	{
		std::vector<std::vector<int>> result_frame_from_to;
		std::vector<hyp_range> supportRanges;

		//std::cout << "Analyse seg";
		step2_analyseDenseSegmentation(
			genK,
			lambda_outlier,
			allClouds,
			denseSeg,
			allKdTrees,
			result_frame_from_to,
			supportRanges,
			genHypByShift,
			genHypByCarryOver
			);
		int max_num_hyps = 32;
		if (denseSeg[0].cols() > max_num_hyps){
			result_frame_from_to.clear();
		}
		else if (denseSeg[0].cols() + result_frame_from_to.size() > max_num_hyps){
			int numNew = max_num_hyps - denseSeg[0].cols();
			std::random_shuffle(result_frame_from_to.begin(), result_frame_from_to.end());
			result_frame_from_to.resize(numNew);
		}

		//std::cout << "Analyse seg done.";
		//create new labels
		int oldDim = denseSeg[0].cols();
		int newDim = oldDim + result_frame_from_to.size();
		auto oldSeg = denseSeg;
		for (int i = 0; i < denseSeg.size(); i++){
			denseSeg[i].resize(denseSeg[i].rows(), newDim);
			//copy the old segmentation data.
			for (int hyp = 0; hyp < oldDim; hyp++){
				denseSeg[i].col(hyp) = oldSeg[i].col(hyp);
			}
			std::cout << "-";
			for (int newHyp = 0; newHyp < result_frame_from_to.size(); newHyp++){
				int take_over_from_label = (i < result_frame_from_to[newHyp][0] ? result_frame_from_to[newHyp][1] : result_frame_from_to[newHyp][2]);
				int other_label = (i < result_frame_from_to[newHyp][0] ? result_frame_from_to[newHyp][2] : result_frame_from_to[newHyp][1]);
				denseSeg[i].col(oldDim + newHyp) = oldSeg[i].col(take_over_from_label);

				if (std::abs(i - result_frame_from_to[newHyp][0]) <= 1){
					denseSeg[i].col(oldDim + newHyp) = (denseSeg[i].col(oldDim + newHyp).array() > oldSeg[i].col(other_label).array()).select(denseSeg[i].col(oldDim + newHyp), oldSeg[i].col(other_label).array());
				}
			}
			std::cout << "done.";
		}

		for (int newHyp = 0; newHyp < result_frame_from_to.size(); newHyp++){
			motions.push_back(motion(allClouds.size()));
			motions.back().hist.set(history::SHIFT, result_frame_from_to[newHyp][1], result_frame_from_to[newHyp][2], result_frame_from_to[newHyp][0]);
		} //*/
	}

	//do a bundle adjustment to find new alignments.
	for (int l = 0; l < denseSeg[0].cols(); l++){
		switch (t)
		{
		case MotionHypotheses::ICP_INCREMENTAL:
			updateAlignmentsAndRange_incremental(l,
				allClouds,
				denseSeg,
				allKdTrees,
				central_cloud,
				maxItICP,
				alpha_point2plane,
				alpha_point2point,
				cullingDist);
			break;
		/*case MotionHypotheses::ICP_FANCY:
			updateAlignmentsAndRange(l,
				allClouds,
				denseSeg,
				allKdTrees,
				central_cloud,
				maxItICP,
				alpha_point2plane,
				alpha_point2point,
				cullingDist,t);
			break;*/
		default:
			/*updateAlignmentsAndRange_incremental(l,
				allClouds,
				denseSeg,
				allKdTrees,
				central_cloud,
				3,
				alpha_point2plane,
				alpha_point2point,
				cullingDist);
			updateAlignmentsAndRange(l,
				allClouds,
				denseSeg,
				allKdTrees,
				central_cloud,
				maxItICP,
				alpha_point2plane,
				alpha_point2point,
				cullingDist,t);*/
			updateAlignmentsAndRange(l,
				allClouds,
				denseSeg,
				allKdTrees,
				central_cloud,
				maxItICP, //50
				alpha_point2plane,
				alpha_point2point,//=> t .....
				cullingDist, MotionHypotheses::ICP_FANCY_6D, 1);
			break;
		}
	}

	std::vector<Eigen::RowVectorXf> frame_supports;
	computeSupport(denseSeg, frame_supports);
	updateRangesFromSupport(frame_supports);
}

void MotionHypotheses::updateRangesFromSupport(std::vector<Eigen::RowVectorXf> & frame_supports){
	/*ranges.clear();
	ranges.resize(frame_supports[0].cols(),my_range(frame_supports.size(),-1));
	for (int frm = 0; frm < frame_supports.size(); frm++){
		for (int i = 0; i < frame_supports[0].cols(); i++){
			if (frame_supports[frm](i) > 150 && ranges[i].start >frm){
				ranges[i].start = frm;
			}
			if (frame_supports[frm](i) > 150 && ranges[i].stop < frm){
				ranges[i].stop = frm;
			}
		}
	}*/
	//its excluding stop.

	for (int i = 0; i < motions.size(); i++){
		motions[i].range.stop = -1;
		motions[i].range.start = frame_supports.size();
	}

	for (int frm = 0; frm < frame_supports.size(); frm++){
//std::cout << "frame " << frm << "supports: " << frame_supports[frm] << "\n";
		for (int i = 0; i < frame_supports[0].cols(); i++){
			if (frame_supports[frm](i) > 150 && motions.at(i).range.start >frm){
				motions.at(i).range.start = frm;
			}
			if (frame_supports[frm](i) > 150 && motions.at(i).range.stop < frm+1){
				motions.at(i).range.stop = frm+1;
			}
		}
	}
}


void MotionHypotheses::computeHypothesesSetFromDenseSegmentation(
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		int central_cloud,
				//the parameters...
		int maxItICP,
		float alpha_point2plane,
		float alpha_point2point,
		float cullingDist,

		int genK, float lambda_outlier,
		bool genHypByShift,
		bool genHypByCarryOver

	){
		//1. do a bundle adjustment to find new alignments.
		step1_bundleAdjustAll(allClouds,
			denseSeg,
			allKdTrees,
			central_cloud,
			maxItICP,
			alpha_point2plane,
			alpha_point2point,
			cullingDist);


		
		//generate and validate these hypotheses: 
		//2. compute supports and label shifts.
		std::vector<std::vector<int>> result_frame_from_to;
		std::vector<hyp_range> supportRanges;

		step2_analyseDenseSegmentation(
			genK,
			lambda_outlier,
			allClouds,
			denseSeg,
			allKdTrees,
			result_frame_from_to,
			supportRanges,
			genHypByShift,
			genHypByCarryOver
		);


		//if(doOnlyTemporalSplitting){
		//	results_frame_from_to.clear();//don't combine...
		//}

		//6. split hypotheses with multiple ranges. 
		//Combine new hypotheses for each detected shift
		//update the alingmentSets and ranges to concur to this.
		
		
		(supportRanges,result_frame_from_to);


	}

void MotionHypotheses::computeDwDt_etc(
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		//out:
		std::vector<Eigen::MatrixXf> & frame_shift,
		std::vector<Eigen::MatrixXf> & frame_shift_fwd,
		std::vector<Eigen::RowVectorXf> & frame_total_shift
	)
{
	int nClouds = allClouds.size();
	frame_shift.clear(); frame_shift.reserve(nClouds);
	frame_shift_fwd.clear(); frame_shift_fwd.reserve(nClouds);
	frame_total_shift.clear(); frame_total_shift.reserve(nClouds);
	explainMe::MatrixXf_rm buff;

	for(int i = 0; i < nClouds; i++){
		frame_shift.push_back(Eigen::MatrixXf());
		frame_shift_fwd.push_back(Eigen::MatrixXf());
		frame_total_shift.push_back(Eigen::RowVectorXf());
		
		//std::cout << i <<"..";
		compute_dense_dWdT_via_nn(
			 allClouds, denseSeg, allKdTrees, i,
			 buff, frame_shift.back(), frame_shift_fwd.back(), frame_total_shift.back());
	}
		
}


void MotionHypotheses::computeSupport(std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<Eigen::RowVectorXf> & frame_support)
{
	int nClouds = denseSeg.size();
	frame_support.clear(); frame_support.reserve(nClouds);
	for(int i = 0; i < nClouds; i++){
		frame_support.push_back(Eigen::RowVectorXf());
		frame_support.back() =  denseSeg[i].colwise().sum();
	}
}

	//Interpolate weights of frame-1 and frame, take temporal derivative
	//with association via nn.
void MotionHypotheses::compute_dense_dWdT_via_nn(
	//in
	std::vector<explainMe::Cloud::Ptr> & allClouds,
	std::vector<explainMe::MatrixXf_rm> & denseSeg,
	std::vector<mySimpleKdTree> & allKdTrees,
	int cloud_idx,
	//out
	explainMe::MatrixXf_rm & result_dense_dwdt,
	Eigen::MatrixXf & result_shift_matrix_from_prev, 
	Eigen::MatrixXf & result_shift_matrix_to_next,
	Eigen::RowVectorXf & absoluteShift)
{
	
	int nHyp = denseSeg[0].cols();

//StopWatch interpolateWeightsTimer, computedwdtTimer;
	result_dense_dwdt.resize(allClouds[cloud_idx]->size(), nHyp);
	result_dense_dwdt.fill(0);
	result_shift_matrix_from_prev = Eigen::MatrixXf::Zero(nHyp, nHyp);
	result_shift_matrix_to_next = Eigen::MatrixXf::Zero(nHyp, nHyp);

	explainMe::MatrixXf_rm & w_f = denseSeg[cloud_idx];

	if(cloud_idx -1 < 0 || cloud_idx + 1 >= allClouds.size()){
		absoluteShift = Eigen::RowVectorXf::Zero(nHyp);//params.numHypo);
		return;
	}
	explainMe::MatrixXf_rm & w_fm1 = denseSeg[cloud_idx -1];
	explainMe::MatrixXf_rm & w_fp1 = denseSeg[cloud_idx + 1];

//computedwdtTimer.restart();
	mySimpleKdTree & tree = allKdTrees[cloud_idx-1];
	mySimpleKdTree & tree_p1 = allKdTrees[cloud_idx + 1];
#pragma omp parallel
{
	simpleNNFinder nn_search(tree);
	simpleNNFinder nn_search_p1(tree_p1);
	distSqr_idx nn, nn_p1;
	Eigen::MatrixXf	result_shift_matrix_ = Eigen::MatrixXf::Zero(nHyp, nHyp);
	Eigen::MatrixXf	result_shift_matrix_fp1_ = Eigen::MatrixXf::Zero(nHyp, nHyp);
	Eigen::RowVectorXf r;
#pragma omp for nowait
	for(int i = 0; i < allClouds[cloud_idx]->size(); i++)
	{
		auto & p = allClouds[cloud_idx]->at(i);
		pxyz pt(p.x, p.y, p.z);
		nn = nn_search.findNN(pt);
		nn_p1 = nn_search_p1.findNN(pt);

		result_dense_dwdt.row(i) = (w_f.row(i) - w_fm1.row(nn.index));
		r = result_dense_dwdt.row(i);
		//(loss1,...,lossn)'*(win1,....,winn)/normalized => Entry(i,j): how much jth label in fram f won from i-th label in frame f-1
		result_shift_matrix_ += -2*(r.array() < 0).select(r, 0).transpose() * (r.array() > 0).select(r, 0) * 1./(1e-10 +r.cwiseAbs().sum());

		//(loss1,...,lossn)'*(win1,....,winn)/normalized 
		//=> Entry(i,j): how much jth label in frame f won from i-th label in frame f+1 
		//<=> how much jth label in frame f looses to ith label in f+1
		r = (w_f.row(i) - w_fp1.row(nn_p1.index));
		result_shift_matrix_fp1_ += -2 * (r.array() < 0).select(r, 0).transpose() * (r.array() > 0).select(r, 0) * 1. / (1e-10 + r.cwiseAbs().sum());

	}
#pragma omp critical
	{
		result_shift_matrix_from_prev += result_shift_matrix_;
		result_shift_matrix_to_next += result_shift_matrix_fp1_;
	}
}
	//each matrix row measures a shift (w_1,...,w_n) -> (w_1',...,w_n')
	//also they sum to zero.
//	MatrixXf_rm shift_matrix = result_dense_dwdt.transpose() *result_dense_dwdt.cwiseAbs().rowwise().sum().cwiseInverse().asDiagonal() * result;

	//collect statistics:
	result_dense_dwdt = result_dense_dwdt.cwiseAbs();
	//the total shift in the frame(*2)
	absoluteShift = result_dense_dwdt.colwise().sum();

	//clamp dense flow at one.
	result_dense_dwdt = (result_dense_dwdt.array() > 1.f).select(1.f, result_dense_dwdt.array());
//computedwdtTimer.stop();	
//std::cout << "dwdt times: ipol|dwdt "  << interpolateWeightsTimer.total() << "\t" << computedwdtTimer.total() << "\n";

}




std::vector<hyp_range> MotionHypotheses::computeValidityRanges(
		float threshold,
		int min_range,
		std::vector<Eigen::RowVectorXf> & frame_supports){

	//int threshold = 200;

	// compute all the ranges during which a hypothesis has constantly large enough suppor.
	std::vector<hyp_range> ranges;
	for(int hyp = 0; hyp < frame_supports[0].cols(); hyp++)
	{
		int idx = 0;
		while(idx < frame_supports.size()){
			hyp_range r;
			if(frame_supports[idx](hyp) < threshold){
				idx ++;
			}
			else{
				r.start = idx; //frame_supports[stop](idx) > 200. for the first time
				r.hyp = hyp;
				while(idx < frame_supports.size()){
					//skip all frames that are over the threshold
					if(frame_supports[idx](hyp) >= threshold){
						idx ++;
						r.stop = idx;
					}
					else{
						//segment finished.  frame_supports[idx ](hyp) < 200. for the first time since start.
						r.stop = idx;
						break;
					}
				}
				if(r.start >= 0 && r.stop >= 0 && r.stop - r.start > min_range){
					ranges.push_back(r);
				}
			}
		}
	}

	return ranges;
	
}

void MotionHypotheses::find_top_k_labelShifts(
		//in
		std::vector<Eigen::MatrixXf> & frame_shift, 
		//params: find top k shifts that are lambda*sigma > shift mean.
		int k, float lambda_outlier,
		std::vector<std::vector<int>> &result_frame_from_to)
{
	auto frame_shift_mat = frame_shift; //make copy, because I'll filter it.
	int n_hyps = frame_shift_mat[0].cols();

	//first: compute standard deviation, mean. (could be done both in one pass....)
	Eigen::MatrixXf std(n_hyps, n_hyps), mean(n_hyps, n_hyps), tmp(n_hyps, n_hyps);
	std.fill(0); mean.fill(0);
	for(int idx = 0; idx < frame_shift_mat.size(); idx++){
		mean +=  frame_shift_mat[idx] /( frame_shift_mat.size());
	}
	for(int idx = 0; idx < frame_shift_mat.size(); idx++){
		//unbiased: n-1 = last_frame - first_frame
		tmp = ( frame_shift_mat[idx] -mean).array().pow(2) / (frame_shift_mat[idx].size()-1);
		std+=tmp;
	}
	std = std.cwiseSqrt();
	
//	std::cout << "mean " << mean << "\nstd " << std << "\n"; 

	//define simple struct to sort (frame i->j) triples by shift magnitude
	struct f_i_j_s {int frame; int i ; int j; float shift;
		f_i_j_s(int f_, int i_, int j_, float s_):frame(f_),i(i_),j(j_),shift(s_){}
	};
	struct {
		bool operator() (const f_i_j_s & i,const f_i_j_s & j) { return i.shift > j.shift;}
	} descending;

	std::vector<f_i_j_s> eligible_shifts;

	
	//filtering: (box filter)
	auto frame_shift_sums = frame_shift_mat; int f_sz = 2;//2;
	for(int i = 1; i < frame_shift_sums.size(); i++){
		frame_shift_sums[i] = frame_shift_sums[i-1]  + frame_shift_mat[i]; 
	}
	for(int i = 0; i < frame_shift_mat.size(); i++){
		frame_shift_mat[i] =
			((i +f_sz < frame_shift_mat.size()? frame_shift_sums[i+f_sz] :frame_shift_sums.back())-
			(i -f_sz -1>=0? frame_shift_sums[i-f_sz-1] :frame_shift_sums[0]*0))
			/(std::min<int>(i+f_sz,frame_shift_mat.size())-std::max<int>(i-f_sz -1,0));
	}



	//second: check for relevant magnitude and local maximality.
	for(int idx = 0; idx < frame_shift_mat.size(); idx++){
		for(int i = 0; i < n_hyps; i++){
			for(int j = 0; j < n_hyps; j++){
				bool isLargeEnough = (frame_shift_mat[idx](i,j) > (mean(i,j) + lambda_outlier *std(i,j)));
				bool isLocalMax = (idx != 0 &&  idx != frame_shift_mat.size()-1 &&
					(frame_shift_mat[idx-1](i,j) <  frame_shift_mat[idx](i,j)) &&
					(frame_shift_mat[idx+1](i,j) <  frame_shift_mat[idx](i,j)));

		//isLargeEnough = true;
				if(isLargeEnough && isLocalMax){
					//eligible_shifts.push_back(f_i_j_s(frame, i,j, frame_shift[frame-first_frame](i,j)));
					eligible_shifts.push_back(f_i_j_s(idx, i,j, frame_shift_mat[idx](i,j)));
				}

//				std::cout << "l>?" << (isLargeEnough? "y":"n") << "lm?" <<(isLocalMax? "y":"n")<<"\t";
			}
		}
	}

	//finally: sort by shift magnitude and extract the top k shifts.
	std::sort(eligible_shifts.begin(), eligible_shifts.end(), descending);

	result_frame_from_to.clear();
	std::cout << "Found: " << eligible_shifts.size() << "eligible shifts...\n";
	for(int i = 0; i < std::min<int>(k, (int) eligible_shifts.size()); i++){
//		std::cout << i << "\n";
		std::vector<int> res;
		res.push_back(eligible_shifts[i].frame);
		res.push_back(eligible_shifts[i].i);
		res.push_back(eligible_shifts[i].j);
//		std::cout << "Cloud: " << eligible_shifts[i].frame
//			<< " shift: " << eligible_shifts[i].i << "->" << eligible_shifts[i].j
//			<< "Amount: " << eligible_shifts[i].shift <<"\n";
		result_frame_from_to.push_back(res);
	}
}


void MotionHypotheses::push_back_labelCarryOvers(
	//in
		std::vector<Eigen::MatrixXf> & frame_shift_mat, 
		std::vector<hyp_range> & ranges,
	//push back to this vector.
		std::vector<std::vector<int>> & result_frame_from_to)
{

	//find two intersecting ranges, and a critical frame
	struct carryOver{
		hyp_range from, to;
		int frame;

		bool comp( const hyp_range &a, const hyp_range & b) const{
			return a.start < b.start || 
				(a.start == b.start && a.stop < b.stop) ||
				(a.start == b.start && a.stop == b.stop, a.hyp < b.hyp);
		}
		bool operator<( const carryOver & b) const{
			//ignore critical frame.
			return comp(from, b.from) || 
				(!(comp(from, b.from)|| comp(b.from, from)) && comp(to, b.to) );
		}
	};

	//get the largest target label (shift) in its dying days (and birthdays.)
	//std::vector<std::pair<hyp_range, hyp_range>> from_to;
	std::vector<carryOver> carryOvers;

	Eigen::VectorXf endShift(frame_shift_mat[0].cols());
	for(int i = 0; i < ranges.size(); i++){
		int start = ranges[i].start, stop = ranges[i].stop;
		int hyp = ranges[i].hyp;

		//better: find corresponding ranges.
		if(stop < frame_shift_mat.size() -1)
		{
			int start_measure = std::max<int>(start, stop -5);
			endShift.fill(0);
			for(int j = start_measure; j < stop; j++){
				endShift += frame_shift_mat[j].col(hyp);
			}
			int heir;
			endShift.maxCoeff(&heir);
			//find appropriate range
			for(int j = 0; j < ranges.size(); j++){
				if(ranges[j].hyp == heir && ranges[j].start < stop && ranges[j].stop > stop){
					carryOver co;
					co.from = ranges[i];
					co.to = ranges[j];
					co.frame = (std::max<int>(start_measure, co.to.start) + std::min<int>(co.from.stop, co.to.stop))/2;
					carryOvers.push_back(co);
					break;
				}
			}

			//std::cout << "Cloud: " << start_measure
			//	<< "death carryover: " << hyp << "->" << heir << "\n";
		}
		if(start > 0){
			int stop_measure = std::min<int>(start+5, stop);
			endShift.fill(0);
			for(int j = start; j < stop_measure; j++){
				endShift += frame_shift_mat[j].col(hyp);
			}
			int ancestor;
			endShift.maxCoeff(&ancestor);
			for(int j = 0; j < ranges.size(); j++){
				if(ranges[j].hyp == ancestor && ranges[j].start < start && ranges[j].stop > start){
					
					carryOver co;
					co.from = ranges[j];
					co.to = ranges[i];
					co.frame = (std::min<int>(stop_measure, co.from.stop) + std::max<int>(co.to.start, co.from.start))/2;
					carryOvers.push_back(co); 

					break;
				}
			}

			//std::cout << "Cloud: " << stop_measure
			//	<< " birth carryover: " << ancestor << "->" << hyp << "\n";
		}
		
	}
	
	
	//remove duplicates
	std::set<carryOver> s;
	s.insert(carryOvers.begin(), carryOvers.end());
	carryOvers.assign(s.begin(), s.end());

	//add to
	for(int i = 0; i < carryOvers.size(); i++){
		std::vector<int> frame_from_to;
		int frame = carryOvers[i].frame;
	
		frame_from_to.push_back(frame);
		frame_from_to.push_back(carryOvers[i].from.hyp);
		frame_from_to.push_back(carryOvers[i].to.hyp);
		result_frame_from_to. push_back(frame_from_to);
	//	std::cout << "Cloud: " << frame
	//			<< " carryover: " <<carryOvers[i].from.hyp << "->" << carryOvers[i].to.hyp << "\n";
	}
	

	//TODO get lines of those dying and beign born 


}


void MotionHypotheses::step3_generateFinalHypotheses(
	std::vector<hyp_range> supportRanges,
	std::vector<std::vector<int>> & frame_label_shift
	)
{
	std::vector<motion> new_alignmentSets; 	
	//std::vector<my_range> new_ranges;

		

	std::vector<std::vector<my_range>> ranges_by_old_hyps(motions.size());
	//split supportRanges. 
	for(int i = 0; i < supportRanges.size(); i++){
		auto & r = supportRanges[i];
		if(r.start >=0 && r.stop >= 0){
			new_alignmentSets.push_back(motions[r.hyp]);
			//new_ranges.push_back(my_range(r.start, r.stop));
			ranges_by_old_hyps.at(r.hyp).push_back(my_range(r.start, r.stop));
		}
		else{
			std::cout << "********invalid range in " << __FILE__ << " " << __LINE__ << "\n";
			std::cout <<  r.start << ","//should overlap!
			       << r.stop<<"\n";
			std::cerr << "invalid range in " << __FILE__ << " " << __LINE__ << "\n";
		}
	}


	//generate new fused hypotheses...
	for(int i = 0; i < frame_label_shift.size(); i++){
		//new hypothesis: assign label1 before frame, label2 after.
		int shift_frame = frame_label_shift[i][0];
		int label1 = frame_label_shift[i][1];
		int label2 = frame_label_shift[i][2];

		std::cout << "generating hypo: " << shift_frame << " : " << label1 << "->" << label2 << "\n";



		//fuse the ranges:
		//find them:
		my_range r_l1(-1,-1), r_l2(-1,-1);
		for(int i_r = 0; i_r < ranges_by_old_hyps[label1].size(); i_r ++){
			if(ranges_by_old_hyps[label1][i_r].start <= shift_frame && ranges_by_old_hyps[label1][i_r].stop > shift_frame){
				r_l1 = ranges_by_old_hyps[label1][i_r];
				break;
			}
		}
		for(int i_r = 0; i_r < ranges_by_old_hyps[label2].size(); i_r ++){
			if(ranges_by_old_hyps[label2][i_r].start <= shift_frame && ranges_by_old_hyps[label2][i_r].stop > shift_frame){
				r_l2 = ranges_by_old_hyps[label2][i_r];
				break;
			}
		}
		if(r_l1.start <0 || r_l1.stop < 0 || r_l2.start < 0 || r_l2.stop < 0 
			|| r_l1.stop < r_l2.start //should overlap!
			|| r_l1.start > r_l2.stop){
			std::cout << "\n***Error " <<__FILE__ << ", line " << __LINE__<< "\n";
			std::cout<< r_l1.start << ","<<  r_l1.stop << ", " <<  r_l2.start << ","//should overlap!
			         <<  r_l2.stop;
			//throw std::runtime_error("Error in MotionHypotheses.cpp: label supports dont make sense");
			continue;
		}
		//new_ranges.push_back(my_range(r_l1.start, r_l2.stop));
		my_range new_range(r_l1.start, r_l2.stop);

		//generate new Alignment set.
		if(shift_frame < 0){
			std::vector<Eigen::Matrix4f> new_hyp =  motions.at(label2).alignments;
			Eigen::MatrixXf delta = motions.at(label1).alignments[shift_frame].inverse()* motions.at(label2).alignments[shift_frame];
			for(int idx = 0; idx < shift_frame; idx++)
			{
				new_hyp[idx] = motions.at(label1).alignments[idx] * delta;
			} 
			new_alignmentSets.push_back(motion(new_hyp, new_range));
		}
		else if(shift_frame >=0){
			std::vector<Eigen::Matrix4f> new_hyp = motions.at(label1).alignments;
			Eigen::MatrixXf delta = motions.at(label2).alignments[shift_frame].inverse()*motions.at(label1).alignments[shift_frame];
			for(int idx = shift_frame ; idx < new_hyp.size(); idx++)
			{
				new_hyp[idx] = motions.at(label2).alignments[idx] * delta;
			}
			new_alignmentSets.push_back(motion(new_hyp,new_range));
		}

		
	}

	//ranges = new_ranges;
	motions = new_alignmentSets;
	std::cout << "There are now" << motions.size() << " alignment sets. Ranges\n";
	for(int i = 0; i < motions.size(); i++){
		std::cout << "\thyp" << i << "\t" << motions[i].range.start << " - " << motions[i].range.stop << "\n";
	}

}