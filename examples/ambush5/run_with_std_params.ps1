#define the data sequence
$dataFolder = "G:/PHD/DATA/unified_format/ambush5"
$start = 0
$end = 20
$df = 1
$datasetType = 100

#a few parameters
$numIt = 10
$loadState = 'false'
$gen_by_outliers = 'true'
$crf_minByMerge = 'true'
$perHypCost = 1.005

#governs the interpolation kernel which implies smoothness.
#to be eliminated soon
#currently: choose factor 2 with synthetic and 6 with standard sintel noise calibration
$crf_sigma_sp_factor = 2

#purely for documentation.
$suffix =  "run_std_1"

#std parameters to use:
$experimentfile = "..\_parameters\experiment_crf_params_2_11_CRF.txt"
$buildFolderFile = "..\_parameters\buildFolder.txt"

$buildFolder = (Get-Content $buildFolderFile)
echo "Build Folder: " + $buildFolder

	#create Folder for Results
        $folder = $PSScriptRoot + "\" + $suffix +"\"
		#$folder = ".\run_nocompat" + $suffix +"\"
        New-Item ($folder) -itemtype directory

	#all parameters will be documented in the experiment .txt file
        $filename = $folder + "experiment" + $suffix +  ".txt"
        $filename | echo

	#create file with all parameters
        (gc $experimentfile) | Foreach-Object {
            $_ -replace '\$start', ($start) `
		-replace '\$end', $end `
		-replace '\$df', $df `
	        -replace '\$dataFolder', $dataFolder `
		-replace '\$datasetType', $datasetType `
	        -replace '\$numIt', $numIt `
		-replace '#f_per_hyp_cost_factor', ('f_per_hyp_cost_factor ' +$perHypCost)`
 		-replace '#\$max_dist', 'f_max_data_distance 4.0' `
		-replace '\$crf_sigma_sp_factor', $crf_sigma_sp_factor `
           }|sc ($filename)

        pushd $buildFolder
		#$experimentfile = ($PSScriptRoot + "\" + $filename) 
		#$outfile = ($PSScriptRoot + "\" + $folder )
		$expfile = ($filename) -replace "\\", "/" 
		$outfile = ($folder)  -replace "\\", "/" 

		#run the c++ code
		$command_to_run = "$buildFolder/Release/explainMe_reduced.exe $expfile  -o $outfile"
     		echo $command_to_run
		./Release/explainMe_reduced.exe $expfile  -o $outfile
        popd

 pause