#pragma once
#include "explainMeDefinitions.h"
#include "Sensors.h"
#include "mySimpleKdTree_ndim.h"


//class glOffscreenRenderer;

template<typename Eigen_T_Weights>
class BundleAdjustmentV2
{
private:
	//glOffscreenRenderer * offscreenRenderer;
public:
	struct params{
		int numIterations;
		float alpha_point2plane;
		float alpha_point2point;
		float alpha_color;
		float culling_dist;
		float last_error;
		float lambda;
		params(){
			numIterations = 10;
			alpha_point2plane = 1;
			alpha_point2point = 0.001;
			alpha_color = 0;
			culling_dist = 0.1;
			last_error = 1e12;
			lambda = 1;
		}
	};
	//acts as initial guess too.
	struct data{
		std::vector<explainMe::Cloud::Ptr > clouds;
		std::vector<Eigen_T_Weights> weights;
		int referenceCloud; //to act as reference frame
		RangeSensorDescription * d;
	};
	struct mesh{
		explainMe::Cloud::Ptr points;
		std::vector<int> triangle_indices;
		mesh():points(new explainMe::Cloud()){}
	};
	typedef Eigen::Matrix<float, 6, 6> Matrix6f;
	typedef Eigen::Matrix<float, 6, 1> Vector6f;

	BundleAdjustmentV2(RangeSensorDescription * d);
	~BundleAdjustmentV2();

	void pointToPlane_LM(
		params &_params,
		data &_data,
		int weight_idx,
		std::vector<Eigen::Matrix4f> &alignments,
		std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
		mesh* avg_mesh = NULL,//supposed to be in the reference coordinate frame.
		std::vector<int> * cloudsAlignedToAvg = NULL
		);

	void pointToPlane_incremental(
		params &_params,
		data &_data,
		int weight_idx,
		std::vector<Eigen::Matrix4f> &alignments
		);

	//boring normal p2pl icp
	void pointToPlane_first2second(
		params &_params,
		explainMe::Cloud::Ptr cloud_1,
		explainMe::Cloud::Ptr cloud_2,
		Eigen::VectorXf & weights_1,
		//alignment ref_to_cloud2 is updated,
		//for convenience everything formulated to one reference cloud.
		Eigen::Matrix4f & ref_to_cloud1,
		Eigen::Matrix4f & ref_to_cloud2
		);

	template<typename NNFinder_cloud_i>
	bool pairwiseHessianAndB(
		explainMe::Cloud::Ptr cloud_i,
		explainMe::Cloud::Ptr cloud_j,
		Eigen_T_Weights & weights_i, 
		Eigen_T_Weights & weights_j,
		NNFinder_cloud_i & nnFinder,
		Eigen::Matrix4f & a_i, Eigen::Matrix4f & a_i_inv,
		Eigen::Matrix4f & a_j, Eigen::Matrix4f & a_j_inv,
		params & _params,
		int weight_idx,
		Eigen::Vector4f & centroid,
		bool possiblyIgnoreConstraint,
		Matrix6f &ccT_target,
		Vector6f & b_target,
		float & error_target, 
		bool verbose= false);


	template<typename NNFinder_cloud_i>
	bool pairwiseHessianAndB(
		float alpha_point2point, float alpha_point2plane,
		explainMe::Cloud::Ptr cloud_i,
		explainMe::Cloud::Ptr cloud_j,
		Eigen::VectorXf & weights_i,
		NNFinder_cloud_i & nnFinder_j,
		Eigen::Matrix4f & a_i, Eigen::Matrix4f & a_i_inv,
		Eigen::Matrix4f & a_j, Eigen::Matrix4f & a_j_inv,
		Eigen::Vector4f & centroid,
		
		Matrix6f &ccT_target,
		Vector6f & b_target);


private:
	std::vector<mySimpleKdTree_ndim<6>> buildAllTrees(params & _params,
		data & _data);

	Eigen::Matrix4f computeAlignment(Vector6f & x_icp, Eigen::Vector4f & centroid);

	void addConstraintToTotal(Matrix6f & ccT, Vector6f & b, int i, int j, int referenceCloud, Eigen::MatrixXf & total, Eigen::VectorXf & b_total);

};

