#include <QApplication>
#include <iostream>
#include <fstream>
//#include "../demo_global_features.h"
#include "../explainMe_algorithm_withFeatures.h"
#include "../Experiment.h"

#include "../CloudColorizer.h"

#include <boost/program_options.hpp>
#include "../pcdReader.h"
#include "../BonnReader.h"
#ifndef USE_NO_CUDA
#include "../rawDataReaderKinect2.h"
#endif
#include <tuple>
#include <boost/filesystem.hpp>
namespace po = boost::program_options;

#include <cv.h>
#include <highgui.h>
#include <cstdint>
#include "../Chronist.h"
#include <pcl/io/ply_io.h>

#include <opencv2/core/version.hpp>
#if CV_MAJOR_VERSION > 2
#include <opencv2/imgcodecs.hpp>
#endif

//#include "opencv2/hyperres/optflow.hpp"

#include "../UnifiedReader.h"
#include "../CloudTools.h"

typedef std::tuple<int,int,int,int> quadrupel;


#define TARGET_PNG 0
#define TARGET_PPM 1
#define TARGET_SEG2 2
#define TARGET_PLY 3
#define TARGET_SEG_VISUALIZATION 4
#define TARGET_DAT_FILE 5
#define TARGET_BONN_FORMAT 6
#define TARGET_COPY 7
#define TARGET_ONE_LARGE_PLY 8
#define TARGET_PLY_VALID_POINTS 9
#define TARGET_FLO 10
#define TARGET_MO2Vis 20
#define TARGET_SEG_EVALUATION 30
#define TARGET_CAST_CHAIR 2000


CloudReader::Ptr make_reader(std::string folder, int dataType){
	CloudReader::Ptr reader;
	if (dataType == 1){
		reader = CloudReader::Ptr(new pcdReader(folder));
	}
	else if (dataType == 2){
		reader = CloudReader::Ptr(new pcdReader(folder, pcdReader::VIRTUAL_SCANNER));
	}
	else if (dataType == 3){
		reader = CloudReader::Ptr(new BonnReader(folder));
	}
	else if(dataType == 10){
		reader = CloudReader::Ptr(new SintelReader(folder));
	}
	else if (dataType == 20){
		reader = CloudReader::Ptr(new Kin2PNGReader(folder));
	}
	else if (dataType == 100){
		reader = CloudReader::Ptr(new UnifiedReader(folder));
	}
	else if (dataType == 0){
#ifndef USE_NO_CUDA
		reader = CloudReader::Ptr(new rawDataReaderKinect2(folder));
#else
		std::cout << "Error! Kinect data can only be read when cuda is enabled";
		return;
#endif // !USE_NO_CUDA
	}
	return reader;
}


void copyData(int dataType, std::string & folder, std::string & outputFolder,
	explainMe_algorithm_withFeatures::Parameters &params)
{
	CloudReader::Ptr reader = make_reader(folder, dataType);
	boost::filesystem::create_directory(outputFolder);

	std::cout << "copying...";

	int i = 0;
	for (int frm = params.first_frame, frm_idx = 0; frm <= params.last_frame; frm+=params.delta_frame, frm_idx++){
		std::cout << ".";
		std::vector<std::string> names = reader->fileNames(frm);
		int j = 0;
		for (std::string nm : names){
			std::cout << "copying: " << nm << "\n";
			std::ifstream  src(nm, std::ios::binary);
			boost::filesystem::path path(nm);
			std::stringstream ss;
			ss << std::setw(5) << std::setfill('0') << i;
			std::ofstream  dst(outputFolder + "/data" + std::to_string(j) + "_frame" + ss.str() +path.extension().string(), std::ios::binary);

			dst << src.rdbuf();
			dst.close();
			j++;
		}
		i++;
	}

	std::ofstream  frustum(outputFolder + "/frustum.txt");
	std::stringstream ss;
	for (int i = 0; i < 4; i++){
		for (int j = 0; j < 4; j++){
			frustum << reader->getDescription()->frustum()(i,j) << " ";
		}
		frustum << "\n";
	}

}


void runIt(int dataType, int targetType,
		   std::string & folder, std::string & outputFolder, 
		   explainMe_algorithm_withFeatures::Parameters &params,
		   bool doSegmentation, std::string & segFile, std::string &moFiles,
		   int label,
		   bool writeCalib)
{
	
	CloudReader::Ptr reader = make_reader(folder, dataType);

	std::cout << "Initting alg...";
	//Really stupid overhead... 
	//explainMe_algorithm_withFeatures::Ptr alg( new explainMe_algorithm_withFeatures(reader, params));
	
	std::cout << "Initting folders...";
	boost::filesystem::path dir(outputFolder);
	boost::filesystem::create_directory(dir);

	/*if(doSegmentation){
		//std::string dummy = "";
		alg->initState(segFile, moFiles);
	}*/

	cv::Mat rgb, d, ir;
	Eigen::Matrix4f dummyMat;

	//load some frame to get dimensions.
	std::cout << "loading frame 0\n";
	reader->loadFrame(0,false,params.maxDistance, false);
	explainMe::Cloud::Ptr  cloud = reader->getPointCloud(), cloud_segmented(new explainMe::Cloud());
	std::cout << "hw" << cloud->height << "," << cloud->width <<"\n";
	rgb = cv::Mat(cloud->height,cloud->width, CV_8UC3);
	d = cv::Mat(cloud->height,cloud->width, CV_16UC1);
	ir = cv::Mat(cloud->height,cloud->width, CV_16UC1);

	//load all frames, and save pngs.
	//for(int frame = alg->first_frame; frame <= alg->last_frame; frame++){
	for (int idx = params.first_frame;
		idx <= params.last_frame; idx += params.delta_frame)
	{
		//int idx = alg->cloudIndex2ScannedFrameIndex(frame - alg->first_frame);
		std::cout << "Frame " << idx << "...\n";
		std::stringstream ss;
		ss << std::setw(5) << std::setfill('0') << idx;
		std::string i_to_string = ss.str();


		/*if(doSegmentation){
			alg->getSegmentedOrganizedCloudAndAlignment(label, frame - alg->first_frame,cloud, dummyMat);
		}
		else
		{*/
			reader->loadFrame(idx,false,params.maxDistance, false);
			cloud = reader->getPointCloud();
		//}

		cv::Vec3b color;
		for(int i = 0; i < cloud->height; i++){
			for(int j = 0; j < cloud->width; j++){
				
				d.at<uint16_t>(cv::Point(j,i)) = (uint16_t) std::floor((cloud->at(j,i).z * 0 == 0 ? cloud->at(j,i).z * 1000 : 0) + 0.5);
				color[2] = cloud->at(j,i).r;
				color[1] = cloud->at(j,i).g;
				color[0] = cloud->at(j,i).b;
				rgb.at<cv::Vec3b>(cv::Point(j,i)) = color;
			}
		}
		if(dataType == 0){
			rawDataReaderKinect2 & r =(rawDataReaderKinect2 &) *reader;
			std::vector<explainMe::UINT16> depth;
			std::vector<explainMe::UINT16> alignedIR;
			r.getRawBuffers(depth,alignedIR);					
			for(int i = 0; i < cloud->height; i++){
				for(int j = 0; j < cloud->width; j++){
					ir.at<uint16_t>(cv::Point(j,i)) = alignedIR.at(i*cloud->width + j); //yes, access is correct.
						//std::max(std::min( std::log(static_cast<float>(alignedIR.at(i*cloud->width + j)))/std::log(2) * 16 -128, 255.0),0.0);
				}
			}

			//16 bit png does work, pgm as well. even though opencv documentation does state it doesn't
			cv::imwrite(outputFolder + "/ir_" + i_to_string+ 
					(targetType == TARGET_PNG ? ".png" : ".pgm"), ir);
		}

		cv::imwrite(outputFolder + "/color_" + i_to_string + 
			(targetType == TARGET_PNG ? ".png" : ".ppm"), rgb);
		cv::imwrite(outputFolder + "/depth_" +i_to_string + 
			(targetType == TARGET_PNG ? ".png" : ".pgm"), d);
	}

	if(writeCalib){
		std::ofstream calibFile(outputFolder + "/calib.txt");
		//color & DEPTH intrinsics. for now the same as depth....
		auto & desc = * reader->getDescription();
		for(int i = 0; i < 2; i++){
			calibFile << desc.width() << " " << desc.height() << "\n";
			calibFile << desc.f_x() << " " << desc.f_y() << "\n";
			calibFile << desc.p_x() << " " << desc.p_y() << "\n\n";
		}
		//color to depth
		Eigen::Matrix4f id = Eigen::Matrix4f::Identity();
		calibFile << id.topLeftCorner<3,4>() <<"\n\n";
		//disparity calib
		calibFile << 0.001 << " " << 0;
		calibFile.close();
	}
}

void cast2Ply(int dataType, int targetType,
	std::string & folder, std::string & outputFolder,
	explainMe_algorithm_withFeatures::Parameters &params
	){
	std::cout << "casting Segmentation files...\n";

	std::cout << "Initting alg...";
	//Really stupid overhead... 
	CloudReader::Ptr reader = make_reader(folder, dataType);
	for (int i = params.first_frame;
		i <= params.last_frame; i += params.delta_frame)
	{
		reader->loadFrame(i, params.cleanUpClouds, params.maxDistance, false);
		for (auto & p : reader->getPointCloud()->points){
			if (!(p.getVector3fMap().norm() * 0 == 0)){
				p.getVector3fMap() << 0, 0, 0;
			}
			if (!(p.getNormalVector3fMap().norm() * 0 == 0)){
				p.getNormalVector3fMap() << 0, 0, 0;
			}
		}
		pcl::io::savePLYFile<explainMe::Point>(outputFolder + "/frame" + std::to_string(i) + ".ply", *reader->getPointCloud());

	}

}
void cast2Vis_motions(int dataType,
	std::string & folder, std::string & outputFolder,
	explainMe_algorithm_withFeatures::Parameters &params,
	std::string motionFile
	){
	if (motionFile.size() == 0){
		std::cout << "Error:  invlaid motionFile:" << motionFile;
	}
	std::cout << "Vizzing Motion files...\n";

	std::cout << "Initting alg...";
	CloudReader::Ptr reader = make_reader(folder, dataType);
	explainMe_algorithm_withFeatures::Ptr alg(new explainMe_algorithm_withFeatures(reader, params));

	boost::filesystem::path dir(outputFolder);
	boost::filesystem::create_directory(dir);

	std::vector<std::vector <Eigen::Matrix4f>> mos;
	Chronist::load(motionFile, mos);
	alg->motions.set(mos);


	int argc = 0; char arg[] = "bla"; char * args = arg;
	//create Qtapplication to get a window to get a opengl context.
	QApplication app(argc, &args);
	Chronist c(alg);
	c.document_alignmentSets_byMovingStuff(outputFolder);
}


void cast2Ply_onlyPointsWithNormalsInCut(int dataType,
	std::string & folder, std::string & outputFolder,
	explainMe_algorithm_withFeatures::Parameters &params
	){
	std::cout << "casting to PLY files...\n";

	CloudReader::Ptr reader = make_reader(folder, dataType);
	char str[12];
	int cloud_idx = 0;
	Eigen::Vector3f minm; minm << std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity();
	Eigen::Vector3f maxm; maxm << -std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity();
	for (int i = params.first_frame;
		i <= params.last_frame; i += params.delta_frame)
	{
		reader->loadFrame(i, params.cleanUpClouds, params.maxDistance, false);

		explainMe::Cloud::Ptr cloud(new explainMe::Cloud());
		explainMe::Cloud::Ptr cloud_withNans = reader->getPointCloud();
		cloud->reserve(cloud_withNans->size());
		std::vector<int> coord_i; coord_i.reserve(cloud_withNans->size());
		std::vector<int> coord_j; coord_j.reserve(cloud_withNans->size());
		int c_i, c_j;
		auto &  d = *reader->getDescription();
		int idx = 0;
		//Eigen::Vector3f minm; minm << std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity();
		//Eigen::Vector3f maxm; maxm << -std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity();
		std::cout << "cloud_withNans->size()" << cloud_withNans->size() << "\n";
		for (auto it = cloud_withNans->begin(); it != cloud_withNans->end(); it++, idx++)
		{
			if (it->z != 0 && it->z * 0 == 0 && it->getNormalVector3fMap().norm() > 0.99){
				/*if ((it->x - 0.2)*-0.0712 + (it->y - 0.85)*-0.9185 + (it->z - 1.4)*-0.3890> 0.15 &&
					((it->x + 0.11056)*0.22736 + (it->y + 1.18709)*-0.39230 + (it->z + -2.40503)*0.89130)<-0.1 &&
					(it->x + 0.85508)*0.90220 + (it->y + -0.72969)*0.06086 + (it->z + -1.35298)*-0.42700 > 0.15)*/
				if (it->z < params.maxDistance)
				{
					cloud->push_back(*it);
					minm = (minm.array() < it->getVector3fMap().array()).select(minm, it->getVector3fMap());
					maxm = (maxm.array() > it->getVector3fMap().array()).select(maxm, it->getVector3fMap());

					d.project(it->x, it->y, it->z, c_i, c_j);
					coord_i.push_back(c_i);
					coord_j.push_back(c_j);
				}
			}
		}
		//vs bounding box hack of PCM:
		/*explainMe::Point p;
		p.getVector3fMap() << 1.45, 0.8, 2.8;
		p.getNormalVector3fMap() << 0, 0, 1;
		p.r = p.g = p.b = 0;
		cloud->push_back(p);
		p.getVector3fMap() << -0.8, -1.23, 0.7;
		p.getNormalVector3fMap() << 0, 0, 1;
		p.r = p.g = p.b = 0;
		cloud->push_back(p);*/

		std::sprintf(str, "%05d", cloud_idx);
		std::cout << str;
		//std::cout << "BBox: " << minm << " \n and \n " << maxm;


		//pcl::io::savePLYFile<explainMe::Point>(outputFolder + "/frame" + std::string(str) + ".ply", *cloud);
		std::ofstream ostream(outputFolder + "/frame" + std::string(str) + ".ply");
		ostream << "ply\nformat ascii 1.0\nelement vertex " << cloud->size()<<"\nproperty   float   x\nproperty   float   y\nproperty   float   z\nproperty   float   nx\nproperty   float   ny\nproperty   float   nz\nproperty   uchar red\nproperty   uchar   green\nproperty   uchar  blue\nend_header\n";
		for (auto & p : cloud->points){
			ostream << p.x << " " << p.y << " " << p.z << " " << p.normal_x << " " << p.normal_y << " " << p.normal_z << " " << (int)p.r << " " << (int)p.g << " " << (int)p.b << "\n";
		}
		ostream << "\n";
		ostream.close();
		std::ofstream ostreamcoord(outputFolder + "/frame" + std::string(str) + ".idx");
		for (int i = 0; i < coord_i.size(); i++){
			ostreamcoord << i << " " << coord_i[i] << " " << coord_j[i] << "\n";
		}
		ostreamcoord << "\n";
		ostreamcoord.close();
		cloud_idx++;

	}

}

void cast2Ply_onlyPointsWithNormals(int dataType,
	std::string & folder, std::string & outputFolder,
	explainMe_algorithm_withFeatures::Parameters &params
	){
	std::cout << "casting to PLY files...\n";
		
	CloudReader::Ptr reader = make_reader(folder, dataType);
	char str[12];
	int cloud_idx = 0;
	for (int i = params.first_frame;
		i <= params.last_frame; i += params.delta_frame)
	{
		reader->loadFrame(i, params.cleanUpClouds, params.maxDistance, false);

		explainMe::Cloud::Ptr cloud(new explainMe::Cloud());
		explainMe::Cloud::Ptr cloud_withNans = reader->getPointCloud();
		cloud->reserve(cloud_withNans->size());
		int idx = 0;
		for (auto it = cloud_withNans->begin(); it != cloud_withNans->end(); it++, idx++)
		{
			if (it->z != 0 && it->z * 0 == 0 && it->getNormalVector3fMap().norm() > 0.99){
				cloud->push_back(*it);
			}
		}
		std::sprintf(str,"%05d", cloud_idx);
		std::cout << str;
		pcl::io::savePLYFile<explainMe::Point>(outputFolder + "/frame" + std::string(str) + ".ply", *cloud);
		cloud_idx++;

	}

}

void getSubclouds(int objHyp,
	explainMe_algorithm_withFeatures::Ptr alg,
	std::vector<explainMe::Cloud::Ptr> & extractedClouds)
{
	extractedClouds.clear();
	for (int i = 0; i < alg->allClouds.size(); i++){
		extractedClouds.push_back(explainMe::Cloud::Ptr(new explainMe::Cloud()));
	}
	
	//segment frames based on current knowledge. Extract one test object (PROTOTYPE)
	for (int i = 0; i < alg->allClouds.size(); i++){
		//extract_subcloud_from_denseSeg(frame, objHyp, extractedClouds[frame - alg->first_frame]);

		auto & cloud = alg->allClouds[i];
		auto & target = extractedClouds[i];
		target->clear();
		explainMe_algorithm_withFeatures::MatrixXf_rm  interpolated_weights 
			= alg->denseSegmentation_allClouds[i];
		for (int i = 0; i < cloud->size(); i++){
			if (interpolated_weights(i, objHyp) > 0.5){
				target->push_back(cloud->at(i));
			}
		}

	}



	//PROTOTYPE just apply the stuff.
	for (int j = 0; j< extractedClouds.size(); j++){
		Eigen::Matrix4f  alignment = alg->motions.getAlignmentSet(objHyp)[j].inverse();
		CloudTools::apply(alignment, extractedClouds[j]);
	}
}

void cast2OnePly(int dataType, int targetType,
	std::string & folder, std::string & outputFolder,
	explainMe_algorithm_withFeatures::Parameters &params,
	std::string & segFile,
	std::string & moFile
	){
	std::cout << "casting Segmentation files...\n";
	CloudReader::Ptr reader = make_reader(folder, dataType);
	std::cout << "Initting alg...";
	params.cleanUpClouds = true;
	explainMe_algorithm_withFeatures::Ptr alg(new explainMe_algorithm_withFeatures(reader, params));

	boost::filesystem::path dir(outputFolder);
	boost::filesystem::create_directory(dir);
	alg->initState(segFile, moFile);
	
	
	std::vector < explainMe::Cloud::Ptr> extractedClouds;
	for (int hyp = 0; hyp < alg->motions.numHypotheses(); hyp++){
		getSubclouds(hyp, alg,extractedClouds);
		explainMe::Cloud hugeCloud;
		for (auto & c : extractedClouds){
			hugeCloud.insert(hugeCloud.begin(), c->begin(), c->end());
		}
		float r, g, b;
		CloudColorizer::colorWheel(hyp, r, g, b);
		for (auto & p : hugeCloud.points){
			p.r *= r;
			p.g *= r;
			p.b *= r;
		}
		pcl::io::savePLYFile<explainMe::Point>(outputFolder + "/accum_cloud_hyp" + std::to_string(hyp) + ".ply", hugeCloud, true);
	}
	

		


}

void cast2Vis(int dataType, int targetType,
	std::string & folder, std::string & outputFolder,
	explainMe_algorithm_withFeatures::Parameters &params,
	std::string & segFile,
	std::string & moFile
	){
	std::cout << "casting Segmentation files...\n";

	std::cout << "Initting alg...";
	CloudReader::Ptr reader = make_reader(folder, dataType);
	explainMe_algorithm_withFeatures::Ptr alg(new explainMe_algorithm_withFeatures(reader, params));

	boost::filesystem::path dir(outputFolder);
	boost::filesystem::create_directory(dir);
	
	alg->initState(segFile, moFile);
	Chronist c(alg);
	c.document_denseWeights(outputFolder);
	//Experiment::documentSparseSegmentation(alg, outputFolder, false);

}

void cast2Eval(int dataType, int targetType,
	std::string & folder, std::string & outputFolder,
	explainMe_algorithm_withFeatures::Parameters &params,
	std::string & segFile,
	std::string & moFile
	){
	std::cout << "casting Segmentation files...\n";

	std::cout << "Initting alg...";
	CloudReader::Ptr reader = make_reader(folder, dataType);
	explainMe_algorithm_withFeatures::Ptr alg(new explainMe_algorithm_withFeatures(reader, params));

	boost::filesystem::path dir(outputFolder);
	boost::filesystem::create_directory(dir);

	alg->initState(segFile, moFile);

	auto & d = * alg->reader->getDescription();
	for (int frame = 0; frame < alg->denseSegmentation_allClouds.size(); frame ++){
		auto & dense_seg = alg->denseSegmentation_allClouds.at(frame);
		cv::Mat mat(d.height(), d.width(), CV_8U);
		for (int ii = 0; ii < d.height(); ii++) for (int jj = 0; jj < d.width(); jj++){
			mat.at<uint8_t>(ii, jj) = 0;
		}

		int i, j, seg;
		for (int idx = 0; idx < alg->allClouds.at(frame)->size(); idx++){
			auto & p = alg->allClouds.at(frame)->points[idx];
			d.project(p.x, p.y, p.z, i, j);
			dense_seg.row(idx).maxCoeff(& seg);
			mat.at<uint8_t>(j, i) = seg+1; //1 based, 0 means no label.			
		}
		cv::imwrite(outputFolder + "/labeling_frame" + std::to_string(alg->cloudIndex2ScannedFrameIndex(frame)) + ".png", mat);
	}
	//Experiment::documentSparseSegmentation(alg, outputFolder, false);

}

void cast2Dat(int dataType,
	std::string & folder, std::string & outputFolder,
	explainMe_algorithm_withFeatures::Parameters &params, bool useColors)
{
	std::cout << "#Casting to Dat #\n#=============#\n";
	CloudReader::Ptr reader = make_reader(folder, dataType);
	
	boost::filesystem::path dir(outputFolder);
	boost::filesystem::create_directory(dir);
	std::cout << params.first_frame << ":" << params.delta_frame << ":"<< params.last_frame<< "\n";
	int first_frame = params.first_frame;
	int last_frame = params.last_frame;
	for (int f = first_frame, of=0; f <= last_frame; f += params.delta_frame, of++)
	{
		reader->loadFrame(f, false, params.maxDistance, false);
		//std::cout << f;
		auto points = reader->getPointCloud()->points;

		std::ofstream s;
		s.open(outputFolder + "/out" + std::to_string(of) + ".dat");
		std::cout << outputFolder + "/out" + std::to_string(of) + ".dat" << "\n";
		for (auto p : points){
			if (p.z * 0 == 0){
				if (useColors)
				{
					s << p.x << " " << p.y << " " << p.z << " " << p.r * 1.0 / 255 << " " << p.g * 1.0 / 255 << " " << p.b * 1.0 / 255 << "\n";
				}
				else{
					s << p.x << " " << p.y << " " << p.z << "\n";
				}
			}
		}
		s.close();

	}
	
}

void castSegFiles(int dataType, int targetType,
	std::string & folder, std::string & outputFolder,
	explainMe_algorithm_withFeatures::Parameters &params,
	bool doSegmentation, std::string & segFile, 
	std::string & moFile
	){
	std::cout << "casting Segmentation files...\n";

	std::cout << "Initting alg...";
	//Really stupid overhead... 
	CloudReader::Ptr reader = make_reader(folder, dataType);
	explainMe_algorithm_withFeatures::Ptr alg(new explainMe_algorithm_withFeatures(reader, params));

	std::cout << "Initting folders...";
	boost::filesystem::path dir(outputFolder);
	boost::filesystem::create_directory(dir);

	alg->initState(segFile, moFile);
	
	Chronist::store_v2(outputFolder + "/", "output", alg->trajectories, alg->w, alg->motions.getAlignmentSets());

}


void cast2FloFiles(int dataType, int targetType,
	std::string & folder, std::string & outputFolder,
	explainMe_algorithm_withFeatures::Parameters &params,
	bool doSegmentation, std::string & segFile,
	std::string & moFile
	){
	std::cout << "casting Segmentation files...\n";

	std::cout << "Initting alg...";
	//Really stupid overhead... 
	CloudReader::Ptr reader = make_reader(folder, dataType);
	explainMe_algorithm_withFeatures::Ptr alg(new explainMe_algorithm_withFeatures(reader, params));

	std::cout << "Initting folders...";
	boost::filesystem::path dir(outputFolder);
	boost::filesystem::create_directory(dir);

	alg->initState(segFile, moFile);

	auto d = reader->getDescription();
	
	auto a = alg->motions.getAlignmentSets();
	auto a_inv = alg->motions.getAlignmentSetsInv();
	Eigen::Vector4f q;
	for (int cld = 0; cld < alg->allClouds.size()-1; cld++)
	{
		std::cout << "output cloud " << cld;
		auto & cloud = alg->allClouds[cld];
		cv::Mat myMat(d->width(), d->height(), CV_32FC2, cv::Scalar(1e9f));
		int i, j, label, ind = 0; float i_next, j_next;
		auto & seg = alg->denseSegmentation_allClouds[cld];
		std::string out(outputFolder + "/flow_" + std::to_string(alg->cloudIndex2ScannedFrameIndex(cld)) + ".dat");
		
		for (auto p : cloud->points){
			seg.row(ind).maxCoeff(&label);
			d->project(p.x, p.y, p.z, i, j);
			//std::cout << "(" << i << "," << j << ")" << label;
			q= a[label][cld + 1] * a_inv[label][cld] * p.getVector4fMap();
			d->project(q.x(), q.y(), q.z(), i_next, j_next);
			//std::cout << "=>" << i_next - i << "," << j_next - j; 
			myMat.at< cv::Vec2f>(i, j)[0] = i_next - i;
			myMat.at< cv::Vec2f>(i, j)[1] = j_next - j;
			ind++;
		}
		
		std::cout << "To file: " << out << "..";
		std::ofstream outStream(out);
		for (int j = 0; j < d->height(); j++){
			for (int i = 0; i < d->width(); i++){
				outStream << myMat.at< cv::Vec2f>(i, j)[0] << " ";
			}
			outStream << "\n";
		}
		for (int j = 0; j < d->height(); j++){
			for (int i = 0; i < d->width(); i++){
				outStream << myMat.at< cv::Vec2f>(i, j)[1] << " ";
			}
			outStream << "\n";
		}

		outStream.close();
		/*FILE * pFile;
		pFile = fopen(out.c_str(), "w");
		//std::cout << "file created..." << "\n";
		MiddleburyWriter::flo_write(myMat, pFile);
		std::cout << "done\n";*/
		//fclose(pFile);
		std::cout << "done\n";

		
	}

}

void cast2BonnFiles(int dataType,
	std::string & folder, std::string & outputFolder,
	explainMe_algorithm_withFeatures::Parameters &params
	){
	std::cout << "casting to Bonn Format...\n";

	std::cout << "Initting alg...";
	//Really stupid overhead... 
	CloudReader::Ptr reader = make_reader(folder, dataType);
	//explainMe_algorithm_withFeatures::Ptr alg(new explainMe_algorithm_withFeatures(reader, params));

	std::cout << "Initting folders...";
	boost::filesystem::path dir(outputFolder);
	boost::filesystem::path rgbdir(outputFolder + "/rgb");
	boost::filesystem::path depthdir(outputFolder + "/depth");
	boost::filesystem::create_directory(dir);
	boost::filesystem::create_directory(rgbdir);
	boost::filesystem::create_directory(depthdir);

	//init mats
	cv::Mat rgb, d, ir;
	Eigen::Matrix4f dummyMat;
	reader->loadFrame(0, false, params.maxDistance, false);
	explainMe::Cloud::Ptr  cloud = reader->getPointCloud(), cloud_segmented(new explainMe::Cloud());
	std::cout << "hw" << cloud->height << "," << cloud->width << "\n";
	rgb = cv::Mat(cloud->height, cloud->width, CV_8UC3);
	d = cv::Mat(cloud->height, cloud->width, CV_16UC1);
	ir = cv::Mat(cloud->height, cloud->width, CV_16UC1);

	std::stringstream rgb_txt;
	std::stringstream d_txt;;
	std::ofstream associations_txt(outputFolder + "/associations.txt");


	//load some frame to get dimensions.
	std::cout << "loading frame 0\n";
	
	for (int frame = params.first_frame; frame <= params.last_frame; frame+=params.delta_frame){
		reader->loadFrame(frame, false, params.maxDistance, false);
		//load all frames, and save pngs.
		std::cout << "Frame " << frame << "...\n";
		std::stringstream ss;
		ss << std::setw(5) << std::setfill('0') << frame;
		std::string i_to_string = ss.str();
			
		cloud = reader->getPointCloud();
			

		cv::Vec3b color;
		for (int i = 0; i < cloud->height; i++){
			for (int j = 0; j < cloud->width; j++){
				d.at<uint16_t>(cv::Point(j, i)) = (uint16_t)std::floor((cloud->at(j, i).z * 0 == 0 ? cloud->at(j, i).z * 5000 : 0) + 0.5);
				color[2] = cloud->at(j, i).r;
				color[1] = cloud->at(j, i).g;
				color[0] = cloud->at(j, i).b;
				rgb.at<cv::Vec3b>(cv::Point(j, i)) = color;
			}
		}

		cv::imwrite(outputFolder + "/rgb/" + i_to_string + ".png", rgb);
		cv::imwrite(outputFolder + "/depth/" + i_to_string + ".png", d);

		rgb_txt << std::to_string(frame) + " rgb/" + i_to_string + ".png\n";
		d_txt << std::to_string(frame) + " depth/" + i_to_string + ".png\n";
		associations_txt << std::to_string(frame) + " depth/" + i_to_string + ".png " + std::to_string(frame) + " rgb/" + i_to_string + ".png\n";
	}
	
	associations_txt.close();

	std::ofstream d_txt_f(outputFolder + "/depth.txt");
	d_txt_f << d_txt.str();
	d_txt_f.close();

	std::ofstream rgb_txt_f(outputFolder + "/rgb.txt");
	rgb_txt_f << rgb_txt.str();
	rgb_txt_f.close();

	//if (writeCalib){
		std::ofstream calibFile(outputFolder + "/sensor.txt");
		//color & DEPTH intrinsics. for now the same as depth....
		auto & desc = *reader->getDescription();
		//for (int i = 0; i < 2; i++){
		calibFile << "# format is: width height<newline> fx fy <newline> px py <newline> lambda <newline>\n"
			<< "# where lambda is the factor to get from the stored 16 bit depth values to meters.\n";
			calibFile << desc.width() << " " << desc.height() << "\n";
			calibFile << desc.f_x() << " " << desc.f_y() << "\n";
			calibFile << desc.p_x() << " " << desc.p_y() << "\n";
		//}
		//color to depth
		//Eigen::Matrix4f id = Eigen::Matrix4f::Identity();
		//calibFile << id.topLeftCorner<3, 4>() << "\n\n";
		//disparity calib
		calibFile << 0.0002 << "\n";
		calibFile.close();
	//}
	
}


int main (int argc, char ** arg)
{
	
	//char * arg[] = { "D:/VCProjects/explainMe_vs2013/build/Release/explainMe_cast.exe", "-c", "E:/cast_to_rgb_d/test_load_virtual2_segFiles/parameters.cfg" };
	//argc = 3;

	std::cout << "Casting files...";
	std::string config_file = "";
	std::string dataFolder, outputFolder, segFile, moFile; int dataType;
	explainMe_algorithm_withFeatures::Parameters params;
	bool segmentBeforeCast, writeCalib, useNormals;
	int targetType, label;

	po::options_description generic("Options");
	generic.add_options()
		("help", "display Help")
		("config,c", po::value<std::string>()->default_value(""), "Path to a configuration .cfg file")
	;

	po::options_description config("Configuration");
	config.add_options()
		("data.folder,D" , po::value<std::string>(&dataFolder)->required() , "specify the folder the 3d data lies")
		("output,O" , po::value<std::string>(&outputFolder)->required() , "specify the folder the 3d data lies")
		("data.type" , po::value<int>(&dataType)->required(), "specify if its kinect or pcl data")
		("data.d", po::value<int>(& params.delta_frame)->required(), "take each dth frame")
		("data.firstFrame",po::value<int>(& params.first_frame)->required(), "... for a total of fw frames after cf")
		("data.lastFrame",po::value<int>(& params.last_frame)->required(), "... for a total of bw frames before cf")
		("data.maxDist",po::value<float>(& params.maxDistance)->default_value(100), "... ignore data points further from the camera")
		("cast.cleanupClouds", po::value<bool>(&params.cleanUpClouds)->default_value(params.cleanUpClouds), "Clean flying pixels up?")
		("cast.type",po::value<int>(& targetType)->default_value(TARGET_PNG), "0: png 1: ppm pgm pair, 2: seg2 format, 3: plys, 4: seg vis, 5:Dat files")
		("cast.segment",po::value<bool>(& segmentBeforeCast)->default_value(false), "if true and a segmentation file is specified")
		("cast.segFile",po::value<std::string>(& segFile)->default_value(""), "... the data will be segmented, depth filled in with 0")
		("cast.motionFile", po::value<std::string>(&moFile)->default_value(""), "... the motionFile")
		("cast.segmentLabel",po::value<int>(& label)->default_value(0), "... the label to keep.")
		("cast.calibFile",po::value<bool>(& writeCalib)->default_value(false), "write a calib file?")
		("cast.useColors", po::value<bool>(&useNormals)->default_value(false), "use colors (dat file only)?")
	;


	po::options_description cmdline_options;
	cmdline_options.add(generic).add(config);

	po::options_description config_file_options;
	config_file_options.add(config);

	po::variables_map vm;
	po::store(po::parse_command_line(argc, arg, cmdline_options), vm);
	if(vm.count("help")){
		std::cout << cmdline_options <<  "\n";
		return 0;
	}

	
	try{
		config_file = vm["config"].as<std::string>();
		std::cout << "config file: " << config_file << "\n";
		if(config_file.size() > 0){
			std::ifstream ifs(config_file.c_str());
			if (!ifs)
			{
			   std::cout << "can not open config file: " << config_file << "\n";
				return 0;
			}
			else
			{
				std::cout << "Parsing config file...";
				po::store(po::parse_config_file(ifs, config_file_options), vm);
				po::notify(vm);
			}
		}

		po::notify(vm);
	}
	catch(std::exception & e){
		std::cout << e.what() << "\n";
		std::cout << "Usage:\n" << cmdline_options << "\n";
		return 0;
	}
	std::cout << "Casting to type " << targetType << "\n";
	std::cout << "Creating " << outputFolder << "...";
	boost::filesystem::create_directory(outputFolder);
	std::cout << "done.\n";

	if (targetType == TARGET_SEG2){
		if (moFile.length() == 0){
			std::cout << "In this mode a motionFile needs to be specified\n";
			std::cout << "Usage:\n" << cmdline_options << "\n";
			return 0;
		}
		castSegFiles(dataType, targetType, dataFolder, outputFolder, params, segmentBeforeCast, segFile, moFile);
	}
	else if (targetType == TARGET_FLO){
		if (moFile.length() == 0){
			std::cout << "In this mode a motionFile needs to be specified\n";
			std::cout << "Usage:\n" << cmdline_options << "\n";
			return 0;
		}
		cast2FloFiles(dataType, targetType, dataFolder, outputFolder, params, segmentBeforeCast, segFile, moFile);
	}
	else if (targetType == TARGET_PLY){
		cast2Ply(dataType, targetType, dataFolder, outputFolder, params);
	}
	else if (targetType == TARGET_PLY_VALID_POINTS)
	{
		cast2Ply_onlyPointsWithNormals(dataType, dataFolder, outputFolder, params);
	}
	else if (targetType == TARGET_SEG_VISUALIZATION){
		cast2Vis(dataType, targetType, dataFolder, outputFolder, params, segFile, moFile);
	}
	else if (targetType == TARGET_SEG_EVALUATION){
		cast2Eval(dataType, targetType, dataFolder, outputFolder, params, segFile, moFile);
	}
	else if (targetType == TARGET_DAT_FILE){
		cast2Dat(dataType, dataFolder, outputFolder, params, useNormals);
	}
	else if (targetType == TARGET_BONN_FORMAT){
		cast2BonnFiles(dataType, dataFolder, outputFolder, params);
	}
	else if (targetType == TARGET_COPY){
		//TODO: fix
		copyData(dataType, dataFolder, outputFolder, params);
	}
	else if (targetType == TARGET_ONE_LARGE_PLY){
		cast2OnePly(dataType, targetType, dataFolder, outputFolder, params, segFile, moFile);
	}
	else if (targetType == TARGET_MO2Vis){
		cast2Vis_motions(dataType, dataFolder, outputFolder, params, moFile);
	}
	else if (targetType == TARGET_CAST_CHAIR){
		cast2Ply_onlyPointsWithNormalsInCut(dataType, dataFolder, outputFolder, params);
	}
	else{
		runIt(dataType, targetType, dataFolder, outputFolder, params, segmentBeforeCast, segFile, moFile, label, writeCalib);
	}
	
	return 0;
}