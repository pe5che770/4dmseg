#pragma once
//#define FLANN_USE_CUDA
//#include <flann/flann.hpp>
//#include <flann/io/hdf5.h>

#ifndef Q_MOC_RUN
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#endif
#include <stdint.h>

namespace explainMe{
	typedef  pcl::PointXYZRGBNormal Point;
	typedef pcl::PointCloud<Point> Cloud;

	typedef Eigen::Matrix <bool, Eigen::Dynamic, Eigen::Dynamic> MatrixXb;	
	
	typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic,Eigen::RowMajor> MatrixXf_rm;
	typedef Eigen::Matrix<float, 1, Eigen::Dynamic> RowVectorXf;


	typedef uint16_t UINT16;
	typedef struct tagRGBQUAD {
		uint8_t    rgbBlue;
		uint8_t    rgbGreen;
		uint8_t    rgbRed;
		uint8_t    rgbReserved;
	} RGBQUAD;
}

//macro for some debugging in release mode...
#define CATCH_AND_RETHROW(expression, ifThrownExpression) 	try{expression}	catch(const std::exception & e){ std::cout << "\nException: " << e.what() << " in " << __FILE__ <<" line " << __LINE__ <<"\n";ifThrownExpression;throw e;}
//#define CATCH_AND_RETHROW(expression, ifThrownExpression) 	try{expression}	catch(const void & e){ std::cout << "\nException: " << e.what() << " in " << __FILE__ <<" line " << __LINE__ <<"\n";ifThrownExpression;throw e;} catch(void & e){ std::cout << "\nException: " << e.what() << " in " << __FILE__ <<" line " << __LINE__ <<"\n";ifThrownExpression;throw e;}