#include <ceres/ceres.h>

#include <pcl/common/common.h>
#include <pcl/console/parse.h>
#include <pcl/console/print.h>
#include <pcl/console/time.h>

#include <pcl/visualization/cloud_viewer.h>
#include <pcl/io/pcd_io.h>
#include <pcl/pcl_macros.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/impl/voxel_grid.hpp>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/segmentation/extract_clusters.h>

#include <boost/filesystem/convenience.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>


inline Eigen::Matrix<float, 3, 3>
cross (const Eigen::Vector3f &p)
{
  Eigen::Matrix3f cross;
  cross <<  0., p (2), -p (1),
      -p (2), 0., p (0),
      p (1), -p (0), 0.;
  return (cross);
}


struct Point2PlaneCorrespondence : public ceres::SizedCostFunction<1, 6, 6>
{
  Point2PlaneCorrespondence (pcl::PointXYZRGBNormal *p_src, pcl::PointXYZRGBNormal *p_tgt)
    : p_src_ (p_src), p_tgt_ (p_tgt)
  {
  }


  bool
  Evaluate (double const* const* params,
            double *residuals,
            double **jacobians) const
  {
    Eigen::Matrix3f rot_src = (Eigen::AngleAxisf (params[0][0], Eigen::Vector3f::UnitX ()) *
                               Eigen::AngleAxisf (params[0][1], Eigen::Vector3f::UnitY ()) *
                               Eigen::AngleAxisf (params[0][2], Eigen::Vector3f::UnitZ ())).matrix ();
    Eigen::Matrix3f rot_tgt = (Eigen::AngleAxisf (params[1][0], Eigen::Vector3f::UnitX ()) *
                               Eigen::AngleAxisf (params[1][1], Eigen::Vector3f::UnitY ()) *
                               Eigen::AngleAxisf (params[1][2], Eigen::Vector3f::UnitZ ())).matrix ();
    Eigen::Vector3f t_src (params[0][3], params[0][4], params[0][5]);
    Eigen::Vector3f t_tgt (params[1][3], params[1][4], params[1][5]);


    residuals[0] = (rot_src * p_src_->getNormalVector3fMap ()).transpose () *
                    (rot_src * p_src_->getVector3fMap () + t_src - rot_tgt * p_tgt_->getVector3fMap () - t_tgt);


    if (jacobians != NULL)
    {
      Eigen::Matrix<float, 1, 3> jac = p_src_->getNormalVector3fMap ().transpose () * cross (t_src).transpose () -
          p_src_->getNormalVector3fMap ().transpose () * cross (rot_tgt * p_tgt_->getVector3fMap () + t_tgt).transpose ();
      jacobians[0][0] = jac (0, 0);
      jacobians[0][1] = jac (0, 1);
      jacobians[0][2] = jac (0, 2);

      jac = (rot_src * p_src_->getNormalVector3fMap ()).transpose ();
      jacobians[0][3] = jac (0, 0);
      jacobians[0][4] = jac (0, 1);
      jacobians[0][5] = jac (0, 2);

      jac = (rot_src * p_src_->getNormalVector3fMap ()).transpose () * (- cross (p_tgt_->getVector3fMap ()));
      jacobians[1][0] = jac (0, 0);
      jacobians[1][1] = jac (0, 1);
      jacobians[1][2] = jac (0, 2);

      jac = (rot_src * p_src_->getNormalVector3fMap ()).transpose () * (-1.f);
      jacobians[1][3] = jac (0, 0);
      jacobians[1][4] = jac (0, 1);
      jacobians[1][5] = jac (0, 2);
    }

    return (true);
  }

  pcl::PointXYZRGBNormal *p_src_, *p_tgt_;
};


int
main (int argc,
      char **argv)
{
  std::string input_dir = "";
  pcl::console::parse_argument (argc, argv, "-input_dir", input_dir);

  /// Read in data
  std::vector<std::string> pcd_files;
  std::vector<std::string> pose_files;


  boost::filesystem::directory_iterator end_itr;
  for (boost::filesystem::directory_iterator itr (input_dir); itr != end_itr; ++itr)
  {
    std::string extension = boost::algorithm::to_upper_copy
      (boost::filesystem::extension (itr->path ()));
    std::string basename = boost::filesystem::basename (itr->path ());
    std::string pathname = itr->path ().string ();
    if (extension == ".pcd" || extension == ".PCD")
      pcd_files.push_back (pathname);
    else if (extension == ".txt" || extension == ".TXT")
      pose_files.push_back (pathname);
  }
  std::sort (pcd_files.begin (), pcd_files.end ());
  std::sort (pose_files.begin (), pose_files.end ());

  std::vector<Eigen::Matrix4f> poses (pose_files.size ());
  for (size_t i = 0; i < pose_files.size (); i++)
  {
    ifstream f (pose_files[i].c_str ());
    float v;
    Eigen::Matrix4f mat (Eigen::Matrix4f::Identity ());
    for (int y = 0; y < 3; y++)
    {
      for (int x = 0; x < 4; x++)
      {
        f >> v;
        mat (y,x) = static_cast<double> (v);
      }
    }
//    std::cerr << "transformation for camera " << i << "\n" << mat << std::endl;
    f.close ();

//    poses[i] = mat;
    //      if (invert)
    poses[i] = mat.inverse ();
  }

  PCL_INFO ("Reading clouds... ");
  std::vector<pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr> clouds;
  for (size_t i = 0; i < pcd_files.size (); ++i)
  {
    PCL_ERROR (".");
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGBNormal> ());
    pcl::io::loadPCDFile (pcd_files[i], *cloud);

    /// write a nicer pcd back
//    pcl::io::savePCDFileBinary (pcd_files[i], *cloud);
    clouds.push_back (cloud);
  }
  PCL_INFO ("Done reading\n");


  /// DONE READING


  /// Debug - accumulate point cloud and save it
  pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud_accum_init (new pcl::PointCloud<pcl::PointXYZRGBNormal> ());
  cloud_accum_init->reserve (clouds.size () * clouds[0]->size ());
  for (size_t i = 0; i < clouds.size (); ++i)
  {
    PCL_ERROR (".");
//    pcl::PointCloud<pcl::PointXYZRGBNormal> cloud_tr;
//    cloud_tr.reserve (clouds[i]->size ());
    for (size_t v_i = 0; v_i < clouds[i]->size (); ++v_i)
      if (pcl_isfinite ((*clouds[i])[v_i].x))
      {
        pcl::PointXYZRGBNormal v_tr;
        v_tr.getVector3fMap () = poses[i].block<3, 3> (0, 0) * (*clouds[i])[v_i].getVector3fMap () + poses[i].block<3, 1> (0, 3);
        v_tr.getNormalVector3fMap () = poses[i].block<3, 3> (0, 0) * (*clouds[i])[v_i].getNormalVector3fMap ();
        v_tr.rgb = (*clouds[i])[v_i].rgb;

        //      cloud_tr.push_back (v_tr);
        cloud_accum_init->push_back (v_tr);
      }

//    *cloud_accum_init += cloud_tr;
  }
  PCL_ERROR ("Saving.\n");
  pcl::VoxelGrid<pcl::PointXYZRGBNormal> voxel_grid;
  voxel_grid.setLeafSize (0.001, 0.001, 0.001);
  voxel_grid.setInputCloud (cloud_accum_init);
  voxel_grid.filter (*cloud_accum_init);
  pcl::io::savePCDFileBinary ("cloud_accum_init.pcd", *cloud_accum_init);


  PCL_ERROR ("Building kdtrees: ");
  /// Build the kdtrees for each point cloud
  std::vector<pcl::KdTreeFLANN<pcl::PointXYZRGBNormal>::Ptr> kdtrees;
  for (size_t i = 0; i < clouds.size (); ++i)
  {
    PCL_ERROR (".");
    pcl::KdTreeFLANN<pcl::PointXYZRGBNormal>::Ptr kdtree (new pcl::KdTreeFLANN<pcl::PointXYZRGBNormal> ());
    kdtree->setInputCloud (clouds[i]);
    kdtrees.push_back (kdtree);
  }
  PCL_ERROR ("Done\n");

  PCL_ERROR ("Building the incremental Ceres problem... ");

  ceres::Problem problem;
  std::vector<double> vars_rigid (6 * clouds.size ());
  /// Initialize the vars
  for (size_t i = 0; i < poses.size (); ++i)
  {
    Eigen::Vector3f angles = poses[i].block<3, 3> (0, 0).eulerAngles (0, 1, 2);
    for (size_t j = 0; j < 3; ++j)
    {
      vars_rigid[6 * i + j] = angles (j);
      vars_rigid[6 * i + 3 + j] = poses[i] (j, 3);
    }
  }


  /// TODO check how to multithread this without issues
  int count_constraints = 0;
  for (int i = 0; i < clouds.size () - 1; ++i)
  {
    PCL_ERROR (".");
    for (int j = i + 1; j <= i + 3 && j < clouds.size (); j += 2)
      if (i != j)
      {
        for (size_t v_i = 0; v_i < clouds[i]->size (); v_i += 10)
          if (pcl_isfinite ((*clouds[i])[v_i].x) &&
              pcl_isfinite ((*clouds[i])[v_i].normal_x))
          {
            Eigen::Matrix4f pose_diff = poses[j].inverse () * poses[i];
            pcl::PointXYZRGBNormal p_src = (*clouds[i])[v_i];
            p_src.getVector3fMap () = pose_diff.block<3, 3> (0, 0) * p_src.getVector3fMap () + pose_diff.block<3, 1> (0, 3);
            p_src.getNormalVector3fMap () = pose_diff.block<3, 3> (0, 0) * p_src.getNormalVector3fMap ();


            std::vector<int> nn_indices;
            std::vector<float> nn_dists;
            kdtrees[j]->nearestKSearch (p_src, 1, nn_indices, nn_dists);


            /// Check distance and normal compatibility
            pcl::PointXYZRGBNormal &p_tgt = (*clouds[j])[nn_indices[0]];

            if ((p_src.getVector3fMap () - p_tgt.getVector3fMap ()).norm () > 0.01)
              continue;

            float dot = p_src.getNormalVector3fMap ().dot (p_tgt.getNormalVector3fMap ());
            if (dot < cos (30. * M_PI / 180.))
              continue;

            /// A valid correspondence has been found ... create a constraint for it
            ceres::CostFunction *cost_function = new Point2PlaneCorrespondence (&(*clouds[i])[v_i], &(*clouds[j])[nn_indices[0]]);
            problem.AddResidualBlock (cost_function,
                                      NULL,
                                      &vars_rigid[6 * i], &vars_rigid[6 * j]);
            count_constraints ++;
          }
      }
  }
  PCL_ERROR ("Added %d constraints to the ceres problem.\n", count_constraints);

  ceres::Solver::Options options;
  options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
  options.minimizer_progress_to_stdout = true;
  options.function_tolerance = 1e-9;
  options.gradient_tolerance = 1e-9;
  options.max_num_iterations = 20;
  options.num_threads = 30;
  options.num_linear_solver_threads = 30;

  ceres::Solver::Summary summary;
  ceres::Solve (options, &problem, &summary);
  std::cout << summary.BriefReport () << std::endl;


  /// Put back the transformations from the vars
  for (size_t i = 0; i < poses.size (); ++i)
  {
    poses[i].block<3, 3> (0, 0) = (Eigen::AngleAxisf (vars_rigid[6 * i + 0], Eigen::Vector3f::UnitX ()) *
                                   Eigen::AngleAxisf (vars_rigid[6 * i + 1], Eigen::Vector3f::UnitY ()) *
                                   Eigen::AngleAxisf (vars_rigid[6 * i + 2], Eigen::Vector3f::UnitZ ())).matrix ();;
    poses[i].block<3, 1> (0, 3) = Eigen::Vector3f (vars_rigid[6 * i + 3], vars_rigid[6 * i + 4], vars_rigid[6 * i + 5]);
  }

  /// Debug - accumulate point cloud and save it
  pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud_accum_iter (new pcl::PointCloud<pcl::PointXYZRGBNormal> ());
  cloud_accum_iter->reserve (clouds.size () * clouds[0]->size ());
  for (size_t i = 0; i < clouds.size (); ++i)
  {
    PCL_ERROR (".");
//    pcl::PointCloud<pcl::PointXYZRGBNormal> cloud_tr;
//    cloud_tr.reserve (clouds[i]->size ());
    for (size_t v_i = 0; v_i < clouds[i]->size (); ++v_i)
      if (pcl_isfinite ((*clouds[i])[v_i].x))
      {
        pcl::PointXYZRGBNormal v_tr;
        v_tr.getVector3fMap () = poses[i].block<3, 3> (0, 0) * (*clouds[i])[v_i].getVector3fMap () + poses[i].block<3, 1> (0, 3);
        v_tr.getNormalVector3fMap () = poses[i].block<3, 3> (0, 0) * (*clouds[i])[v_i].getNormalVector3fMap ();
        v_tr.rgb = (*clouds[i])[v_i].rgb;

        //      cloud_tr.push_back (v_tr);
        cloud_accum_iter->push_back (v_tr);
      }

//    *cloud_accum_init += cloud_tr;
  }
  PCL_ERROR ("Saving.\n");
  voxel_grid.setLeafSize (0.001, 0.001, 0.001);
  voxel_grid.setInputCloud (cloud_accum_iter);
  voxel_grid.filter (*cloud_accum_iter);
  pcl::io::savePCDFileBinary ("cloud_accum_iter.pcd", *cloud_accum_iter);



  return (0);
}
