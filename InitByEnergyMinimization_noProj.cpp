#include "InitByEnergyMinimization_noProj.h"
#include "BundleAdjustment.h"

InitByEnergyMinimization_noProj::InitByEnergyMinimization_noProj(
	int numIt, float p1, float p2, float stepSize, int numHyp, int numClouds)
{
	this->numIt = numIt;
	this->p1 = p1;
	//this->p2 = p2;
	this->p2 = 2;
	this->stepSize = stepSize;
	this->numHyp = numHyp;
	this->numClouds = numClouds;
}


InitByEnergyMinimization_noProj::~InitByEnergyMinimization_noProj(void)
{
}


void compute_d_dw_E_w_pow_p_fast_incomplete_implicit(std::vector<trajectory_R> & paths,
											explainMe::MatrixXf_rm &w_sqr,
											//T[i_f][i_w]
											std::vector<std::vector<Eigen::Matrix4f>> &T,
											int numClouds, int reference_frame,	
											explainMe::MatrixXf_rm & d_dw_E,
											float p)
{
	//In principal I'd need the gradient of the pointwise error function. For now I will simply use
	//x-c(x) normed and as error the point 2 point error.

	//d_dw_E is the current error plus a weighted sum of error gradients

	p = p;

	int lastHyp = w_sqr.cols()-1;
	//assume that w_sqr(i, last_hyp) is 1-sum(other squareWeights. And positive.

	std::vector<std::vector<Eigen::Matrix4f>> ainv = T; //not huge, so probly ok as temp?
	for(int i = 0; i < ainv.size(); i++){
		for(int j =0; j <numClouds; j++){
			ainv[i][j] = T[i][j].inverse();
		}
	}

	float epsilon = 0.2;

	//derivative for each (weight-set, path) pair (i.e. each weight)
	for(int i_w =0; i_w < w_sqr.cols()-1; i_w++){
		for(int i = 0; i < paths.size(); i++)
		{
			//compute one single derivative:
			d_dw_E(i,lastHyp) = 0;
			d_dw_E(i,i_w) = 0;
			//the derivative is dependent of all hops(path length) and paths;
			//the first sum treats forward hops...
			for(int frm = paths[i].firstCloud(); frm <= paths[i].lastCloud(); frm++){
				if(paths[i].cloudInRange(frm))
				{
					//Eigen::Vector4f  p = paths[i][0].getVector4fMap();
					//current error to frame frm.
					d_dw_E(i,i_w) = d_dw_E(i,i_w) + p *pow(std::max(w_sqr(i,i_w),0.f),p-1) * 
						(T[i_w][frm] * ainv[i_w][paths[i].seedCloud()] * paths[i].seedEl().getVector4fMap() - paths[i][frm].getVector4fMap()).norm();

					d_dw_E(i,i_w) = d_dw_E(i,i_w) -p *pow(std::max(1+ w_sqr(i,lastHyp)*w_sqr(i,lastHyp) -w_sqr.row(i).squaredNorm() + 1e-3f ,1e-3f),p/2-1) *w_sqr(i,i_w) * 
						(T[lastHyp][frm] * ainv[lastHyp][paths[i].seedCloud()] * paths[i].seedEl().getVector4fMap() - paths[i][frm].getVector4fMap()).norm();
					
					//d_dw_E(i,i_w) = d_dw_E(i,i_w) + 100* std::pow(std::min(1-w_sqr(i,i_w),0.f),2);

					//and the main sum is left out.
				}
			}

		}
	}//end for.
}

void InitByEnergyMinimization_noProj::findWeights(
		std::vector<trajectory_R> & trajectories, 
		explainMe::MatrixXf_rm & w)
{
		//kill this one:
	int reference_frame =0;
	
	
	w.resize(trajectories.size(), numHyp);
	d_dw_E = w;
	d_dw_E.fill(1.f/numHyp);
	

	//init the ws
	//init rand, but concentrated around ones/numHyp
	w.fill(10);
	srand((unsigned int) 99638);
	for(int i = 0; i < w.rows(); i++){
		w.row(i) += explainMe::RowVectorXf::Random(numHyp);
		w.row(i) = w.row(i).cwiseAbs();
		w.row(i) = w.row(i)/w.row(i).sum();
		w.row(i).cwiseSqrt();
	}


	//the whole shabang.
	Eigen::Matrix4f id = Eigen::Matrix4f::Identity();
	std::vector<Eigen::Matrix4f> s(numClouds, id);
	//s.resize(first_frame, last_frame,id);

	//T[i_w][i_f] is the alignment 
	//			from ref frame to i_f.
	//			corresponding to the weight set i_w
	std::vector<std::vector<Eigen::Matrix4f>>  T;
	T.resize(numHyp,s);


	int n_min_1 = w.cols()-1;

	//for general reprojection:
	
	explainMe::RowVectorXf w_center = explainMe::RowVectorXf::Ones(numHyp)/numHyp, new_constraint;
	Eigen::MatrixXf constraints(numHyp, numHyp);// = Matrixnf::Zeros();
	Eigen::MatrixXf e = Eigen::MatrixXf::Identity(numHyp,numHyp);


	for(int it_step = 0; it_step < numIt; it_step ++){
		
		std::cout << it_step << "/" << numIt << "...";
		//compute alignments
		for(int hyp = 0; hyp < numHyp; hyp++){

			//used to reset alignments to identity. But might not need to.
			T[hyp] = s;

			//this does a fixed number of icp steps.
			//the reason that I don't use the closed formula is that alignments from 
			//the reference frame to each frame is computed. Not completely clear
			//how to solve for these with the closed formula. Dunno if appending the frame to frame 
			//solutions would be right.
			BundleAdjustment::pointToPlane_Bundle_step<explainMe::MatrixXf_rm>
				(trajectories, numClouds, reference_frame,w,hyp,T[hyp],p1/2);
		}
		
		
		//compute "gradient"

		//version for arbitrary paths
		compute_d_dw_E_w_pow_p_fast_incomplete_implicit(
			trajectories,	w,	T,		
			numClouds, reference_frame,
			d_dw_E, p2);

		//with temporal smoothnessterm
		/*compute_d_dw_E_w_pow_p_fast_incomplete_anyFrame(
			trajectories, N_start, N_end,	w,	T,		
			first_frame, last_frame, reference_frame,
			d_dw_E, params.p2,
			params.alpha_smootheness_w);*/


		/*explainMe::RowVectorXf w_tangent = explainMe::RowVectorXf::Ones(numHyp), tmp, w_proj, max_movement_possible, border_nrml;
		w_tangent.normalize();
		//d_dw_E_nonprojected = d_dw_E;

		
		for(int i = 0; i < d_dw_E.rows(); i++){
			
			//d_dw_E[i] = d_dw_E[i] - (w_tangent * d_dw_E[i].dot(w_tangent)); //reproject on w's tangential space.
			d_dw_E.row(i) = d_dw_E.row(i) - (w_tangent * d_dw_E.row(i).dot(w_tangent)); //reproject on w's tangential space.
		}*/

		for(int i = 0; i < w.rows(); i++){
			//gradient descent,
			w.row(i) =  w.row(i) - d_dw_E.row(i)*stepSize;

			//but d_dw_E for the last hyp is not defined, the last hyp is implicitely defined by the others
			w(i, n_min_1) = 0;
			w(i, n_min_1) = std::sqrt(std::max((1 - w.row(i).squaredNorm()),0.f));

			d_dw_E(i, n_min_1) =0; //just for vis. But actually this is the correct change that enforces w keeping sum =1 prop.
			d_dw_E(i, n_min_1) = - d_dw_E.row(i).sum();


			
			if(w.row(i).norm()*0 != 0){
				std::cout << "w IS out of the raster, see ? \n" << w.row(i) << "\n now say ok. Or ctr c";
				int a ;
				std::cin >> a;
			}
			//hack. Actually this is backprojection onto the valid range. but just a very bad one.
			//for numerics.
			w.row(i) = w.row(i).cwiseAbs();
			//w.row(i) = (w.row(i).array() < 0 ).select(0,w.row(i));
			w.row(i) = w.row(i) / w.row(i).norm(); //sum of w_i^2 = 1. 
			if(w.row(i).norm()*0 != 0){
				std::cout << "w went out of the raster, see ? \n" << w.row(i) << "\n now say ok. Or ctr c";
				int a ;
				std::cin >> a;
			}
			
		}
	}

}