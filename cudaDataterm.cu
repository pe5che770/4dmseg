#include "cudaDataterm.h"
#include <cuda.h>
#include "cudaHelper.h"
//#include "gpuBasicObjects.h"

#include <thrust/for_each.h>
#include <thrust/device_vector.h>
#include <thrust/random.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>
#include <thrust/copy.h>
#include <thrust/transform_reduce.h>
#include <math_functions.h>
#include <iostream>
#include <limits>
#include <algorithm>
#ifdef __GNUC__
#include <cfloat>
#endif


__constant__ float cam_unproject[16];
__constant__ float cam_frustum[16];
__constant__ int cam_width_height[2];

//#define USE_GEODESIC_CRF_WITH_STDVAR
#define INTERPOLATE_USING_SIGMA
#define SMOOTHEN_USING_SIGMA

//could promote methods to file (c style) gets rid of one pointer
struct gpu_cam
{

	gpu_cam * dev_ptr;
	gpu_cam(sensorDescriptionData & sd){
		HANDLE_ERROR(cudaMemcpyToSymbol(cam_unproject, sd._unproject, sizeof(float) * 16));
		HANDLE_ERROR(cudaMemcpyToSymbol(cam_frustum, sd._frustum, sizeof(float) * 16));
		int cpu_wh[2] = { sd._width, sd._height };
		HANDLE_ERROR(cudaMemcpyToSymbol(cam_width_height, cpu_wh, sizeof(int) * 2));

		//copy object to device for methods to be accessible.
		HANDLE_ERROR(cudaMalloc((void **)& dev_ptr, sizeof(gpu_cam)));
		HANDLE_ERROR(cudaMemcpy((void *) dev_ptr,(void * ) this, sizeof(gpu_cam),cudaMemcpyHostToDevice));
	}
	~gpu_cam(){
		cudaFree(dev_ptr);
	}

	 __device__ inline float unproj_i2x(int I, float Z){
		 return (-(cam_unproject[0] * (I)+cam_unproject[8] * (Z)+cam_unproject[12])* (Z));
		 //return 1;
	}

	__device__ inline float unproj_j2y(int J, float Z){
		return ((cam_unproject[5] * (J)+cam_unproject[9] * (Z)+cam_unproject[13])* (Z));
		//return 1;
	}

	__device__  inline bool isIn(int i, int j){
		return i >= 0 && j >= 0 && i < cam_width_height[0] && j < cam_width_height[1];
	}

	template<typename tuple_t>
	__device__ inline void proj(const tuple_t & query, int & i, int & j){
		float x = cam_frustum[0] * thrust::get<0>(query)  -cam_frustum[4] * thrust::get<1>(query) +cam_frustum[8] * thrust::get<2>(query) +cam_frustum[12];
		float y = cam_frustum[1] * thrust::get<0>(query) -cam_frustum[5] * thrust::get<1>(query) +cam_frustum[9] * thrust::get<2>(query) +cam_frustum[13];
		float w = cam_frustum[3] * thrust::get<0>(query) -cam_frustum[7] * thrust::get<1>(query) +cam_frustum[11] * thrust::get<2>(query) +cam_frustum[15];
		i = roundf((x / w) * cam_width_height[0] / 2.0 + (cam_width_height[0] - 1) / 2.0);
		j = roundf((y / w) * cam_width_height[1] / 2.0 + (cam_width_height[1] - 1) / 2.0);
		//i = 1; j = 1;
	}

};


template<>
__device__ inline void gpu_cam::proj<float3>(const float3 & query, int & i, int & j){
	float x = cam_frustum[0] * query.x - cam_frustum[4] * query.y + cam_frustum[8] * query.z + cam_frustum[12];
	float y = cam_frustum[1] * query.x - cam_frustum[5] * query.y + cam_frustum[9] * query.z + cam_frustum[13];
	float w = cam_frustum[3] * query.x - cam_frustum[7] * query.y + cam_frustum[11] * query.z + cam_frustum[15];
	i = roundf((x / w) * cam_width_height[0] / 2.0 + (cam_width_height[0] - 1) / 2.0);
	j = roundf((y / w) * cam_width_height[1] / 2.0 + (cam_width_height[1] - 1) / 2.0);
	//i = 1; j = 1;
}


struct gpu_frame
{
	float * dev_frame_data;
	gpu_frame * dev_ptr;
	cudaTextureObject_t frame;
	size_t pitch, pitch_2;
	int w, h;
	
	gpu_frame(std::vector<float> & depth, int width, int height){
		w = width; h = height;
		//copy depth to pitch aligned memory.k
		pitch_2 = width * sizeof(float);
		HANDLE_ERROR(cudaMallocPitch((void **)& dev_frame_data, &pitch, width*sizeof(float), height));
		cudaChannelFormatDesc desc = cudaCreateChannelDesc<float>();
		HANDLE_ERROR(cudaMemcpy2D(dev_frame_data,pitch, & depth[0],pitch_2, width*sizeof(float), height, cudaMemcpyHostToDevice));


		// create a 2D texture object, with pitch aligned memory.
		cudaTextureDesc texDesc;
		memset(&texDesc, 0, sizeof(texDesc));
		texDesc.addressMode[0] = texDesc.addressMode[1] =  cudaAddressModeBorder;
		texDesc.readMode = cudaReadModeElementType;

		//describe the raw buffer
		cudaResourceDesc resDesc;
		memset(&resDesc, 0, sizeof(resDesc));
		resDesc.resType = cudaResourceTypePitch2D;
		resDesc.res.pitch2D.devPtr = dev_frame_data;
		resDesc.res.pitch2D.desc = desc;
		resDesc.res.pitch2D.width = width;
		resDesc.res.pitch2D.height = height;
		resDesc.res.pitch2D.pitchInBytes = pitch;
		HANDLE_ERROR(cudaCreateTextureObject(&frame, &resDesc, &texDesc, NULL));
	
		//copy struct to device to be accessible on the gpu.
		HANDLE_ERROR(cudaMalloc((void **)& dev_ptr, sizeof(gpu_frame)));
		HANDLE_ERROR(cudaMemcpy((void *)dev_ptr, (void *) this, sizeof(gpu_frame), cudaMemcpyHostToDevice));

	}


	//constructor for a dummmy frame, alway returning 0
	gpu_frame(){
		int width = 1, height = 1;
		w = width; h = height;
		//copy depth to pitch aligned memory.k
		size_t pitch, pitch_2 = width * sizeof(float);
		HANDLE_ERROR(cudaMallocPitch((void **)& dev_frame_data, &pitch, width*sizeof(float), height));
		cudaChannelFormatDesc desc = cudaCreateChannelDesc<float>();
		float dummyDepth = 0;
		HANDLE_ERROR(cudaMemcpy2D(dev_frame_data, pitch, &dummyDepth, pitch_2, width*sizeof(float), height, cudaMemcpyHostToDevice));

		// create texture object
		cudaTextureDesc texDesc;
		memset(&texDesc, 0, sizeof(texDesc));
		texDesc.addressMode[0] = texDesc.addressMode[1] = cudaAddressModeBorder; //all 0...
		texDesc.readMode = cudaReadModeElementType;

		//describe the raw buffer
		cudaResourceDesc resDesc;
		memset(&resDesc, 0, sizeof(resDesc));
		resDesc.resType = cudaResourceTypePitch2D;
		resDesc.res.pitch2D.devPtr = dev_frame_data;
		resDesc.res.pitch2D.desc = desc;
		resDesc.res.pitch2D.width = width;
		resDesc.res.pitch2D.height = height;
		resDesc.res.pitch2D.pitchInBytes = pitch;
		HANDLE_ERROR(cudaCreateTextureObject(&frame, &resDesc, &texDesc, NULL));

		//copy struct to device to be accessible on the gpu.
		HANDLE_ERROR(cudaMalloc((void **)& dev_ptr, sizeof(gpu_frame)));
		HANDLE_ERROR(cudaMemcpy((void *)dev_ptr, (void *) this, sizeof(gpu_frame), cudaMemcpyHostToDevice));
	}

	~gpu_frame()
	{
		cudaDestroyTextureObject(frame);
		cudaFree(dev_frame_data);
		cudaFree(dev_ptr);
	}

	void copy2Gpu(std::vector<float> & data){
		//assume data has no additional pitch!
		HANDLE_ERROR(cudaMemcpy2D(dev_frame_data, pitch, &data[0], w*sizeof(float), w*sizeof(float), h, cudaMemcpyHostToDevice));
	}

	void copyThisTo(gpu_frame * other);

	void set2Const(float val)
	{
		//std::cout << "***Access gpu_buffer_Q : set2Const " << pitch / sizeof(float)* h << " floats\n";
		//HANDLE_ERROR(cudaMemset2D(dev_frame_data, pitch, val, w*sizeof(float), h));
		thrust::device_ptr<float> dev_ptr = thrust::device_pointer_cast(dev_frame_data);
		// use device_ptr in Thrust algorithms
		thrust::fill(dev_ptr, dev_ptr + (pitch/sizeof(float)*h), val);

		//gpuErrchk(cudaPeekAtLastError());
		//gpuErrchk(cudaDeviceSynchronize());
		//gpuErrchk(cudaPeekAtLastError());
	}

	void set2zero()
	{
		//gpuErrchk(cudaPeekAtLastError());
		//gpuErrchk(cudaDeviceSynchronize()); //illegal memory access was encountered.
		//gpuErrchk(cudaPeekAtLastError());

		HANDLE_ERROR(cudaMemset2D(dev_frame_data, pitch, 0, w*sizeof(float), h));
		//set2Const(0.f);
	}

	void copy2Cpu(std::vector<float> & data){
		HANDLE_ERROR(cudaMemcpy2D(&data[0], pitch_2, dev_frame_data, pitch, w*sizeof(float), h, cudaMemcpyDeviceToHost));
	}

	//note in a single kernel call either memory can be accessed as a texture OR be written to. Else there is undefined behaviour.
	__device__ inline void set(int i, int j, float val)
	{
		if ((i < w) && (j < h) && i>=0 && j >= 0){
			float *myrow = (float *)(((char *)dev_frame_data) + (j*pitch));
			myrow[i] = val;
		}
	}

	__host__ void inefficientSet(int i, int j, float val){
		thrust::device_ptr<float> dev_ptr = thrust::device_pointer_cast(dev_frame_data);
		dev_ptr[j*(pitch / sizeof(float)) + i] = val;
	}
	
	/*__device__ inline float3 normalAt(int i, int j, gpu_cam * cam){
		float3 xyz, xyz_kp1, xyz_lp1, nxyz;
		for (int k = -3; k <= 3; k++)for (int l = -3; l <= 3; l++){
			xyz.z = tex2D<float>(frame, i + k, j + l);
			//3. unproject
			xyz.x = cam->unproj_i2x(i + k, xyz.z);
			xyz.y = cam->unproj_j2y(j + l, xyz.z);
		
		}
	}*/

	__device__ inline bool nn(float3 & p, gpu_cam * cam, int2 & ij, float & bestDist_sqr, float clamp_dist_sqr){
		bool better, found = false;
		float dist_sqr;
		float3 xyz;
		int i, j;
		cam->proj<float3>(p, i, j);
		int best_i = i;
		int best_j = j;

		bestDist_sqr = clamp_dist_sqr;

		for (int k = -3; k <= 3; k++)for (int l = -3; l <= 3; l++){
			xyz.z = tex2D<float>(frame, i + k, j + l);
			//3. unproject
			xyz.x = cam->unproj_i2x(i + k, xyz.z);
			xyz.y = cam->unproj_j2y(j + l, xyz.z);
			dist_sqr = (xyz.x - p.x)*(xyz.x - p.x)
				+ (xyz.y - p.y)*(xyz.y - p.y)
				+ (xyz.z - p.z)*(xyz.z - p.z);

			better = (bestDist_sqr > dist_sqr) && (xyz.z > 0.0001);
			found = found || better;
			bestDist_sqr = (better ? dist_sqr : bestDist_sqr);
			best_i = (better ? i + k : best_i);
			best_j = (better ? j + l : best_j);

		}
		//xyz is filled in with zeros when nothing was found. zero depth is considered invalid.
		ij.x = (found ? best_i : -1);
		ij.y = (found ? best_j : -1);
		
		return found;
	}

	__device__ inline bool findNNValidData(int i, int j, gpu_frame * data, gpu_cam * cam, float & found_value){
		bool better, found = false;
		float dist_sqr;
		int best_i = i;
		int best_j = j;

		float3  xyz,p;
		p.z = tex2D<float>(frame, i, j );
		p.x = cam->unproj_i2x(i , p.z);
		p.y = cam->unproj_j2y(j , p.z);

		float bestDist_sqr = CUDART_INF_F;// 1.f / 0;
		found_value = 0;
		float val;
		for (int k = -3; k <= 3; k++)for (int l = -3; l <= 3; l++){
			xyz.z = tex2D<float>(frame, i + k, j + l);
			//3. unproject
			xyz.x = cam->unproj_i2x(i + k, xyz.z);
			xyz.y = cam->unproj_j2y(j + l, xyz.z);
			dist_sqr = (xyz.x - p.x)*(xyz.x - p.x)
				+ (xyz.y - p.y)*(xyz.y - p.y)
				+ (xyz.z - p.z)*(xyz.z - p.z);

			val = tex2D<float>(data->frame, i + k, j + l);

			better = (bestDist_sqr > dist_sqr) && (xyz.z > 0.0001) && (val*0 == 0);
			found = found || better;
			bestDist_sqr = (better ? dist_sqr : bestDist_sqr);
			best_i = (better ? i + k : best_i);
			best_j = (better ? j + l : best_j);
		}
		//xyz is filled in with zeros when nothing was found. zero depth is considered invalid.
		xyz.z = tex2D<float>(frame, best_i, best_j) * (found ? 1 : 0);
		xyz.x = cam->unproj_i2x(best_i, xyz.z);
		xyz.y = cam->unproj_j2y(best_j, xyz.z);
		found_value = tex2D<float>(data->frame, best_i, best_j);
		return found;
	}
};

__global__ void doACopy(cudaTextureObject_t from, gpu_frame * to){
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	to->set(i, j, tex2D<float>(from, i, j));
}

void gpu_frame::copyThisTo(gpu_frame * other){
	assert(other->w == this->w && other->h == this->h);
	doACopy << <dim3((w + 7) / 8, (h + 7) / 8), dim3(8, 8) >> >(frame, other->dev_ptr);
}


template <typename myType>
class shallowGPUCopy{
public:
	myType * dev_ptr;
	shallowGPUCopy(const myType & obj){
		cudaMalloc((void **)&dev_ptr, sizeof(myType));
		cudaMemcpy((void*)dev_ptr, (void*)&obj, sizeof(myType), cudaMemcpyHostToDevice);
	}
	~shallowGPUCopy(){
		cudaFree(dev_ptr);
	}

private:
	shallowGPUCopy(const shallowGPUCopy & that);
	shallowGPUCopy& operator=(shallowGPUCopy const&);
};

struct float4x4{
	float m00, m01, m02, m03, 
			m10,m11, m12, m13,
			m20, m21, m22, m23,
			m30, m31, m32, m33;

	float4x4(){
		m00 = m01 = m02 = m03 =
			m10= m11= m12= m13 =
			m20= m21= m22= m23=
			m30= m31= m32= m33 = 0;
	}
	__host__ __device__ float4x4(float * data){
		m00 = data[0]; m01 = data[4]; m02 = data[8]; m03 = data[12];
		m10 = data[1]; m11 = data[5]; m12 = data[9]; m13 = data[13];
		m20 = data[2]; m21 = data[6]; m22 = data[10]; m23 = data[14];
		m30 = data[3]; m31 = data[7]; m32 = data[11]; m33 = data[15];

	}
	__host__ __device__ ~float4x4(){

	}

	__device__ void mult(float3 & x, float3 & y){
		y.x = m00 * x.x + m01 * x.y + m02 * x.z + m03;
		y.y = m10 * x.x + m11 * x.y + m12 * x.z + m13;
		y.z = m20 * x.x + m21 * x.y + m22 * x.z + m23;
	}
	__device__ void mult_top3x3(float3 & x, float3 & y){
		y.x = m00 * x.x + m01 * x.y + m02 * x.z;
		y.y = m10 * x.x + m11 * x.y + m12 * x.z;
		y.z = m20 * x.x + m21 * x.y + m22 * x.z;
	}
};

struct gpu_alignments
{
	int num_frames;
	int num_hyps;

	//alignments by hypothesis.
	std::vector<thrust::device_vector<float4x4> > As_padded;
	std::vector<thrust::device_vector<float4x4> > As_inv_padded;

	//alignments per frame (duplicate, but allows easy access)
	std::vector<thrust::device_vector<float4x4> > As_per_frame;
	std::vector<thrust::device_vector<float4x4> > As_inv_per_frame;

	gpu_alignments(std::vector<std::vector<float*> > & a, std::vector<std::vector<float*> > & a_inv, int frame)
	{
		num_hyps = a.size();
		num_frames = a[0].size();
		thrust::host_vector<float4x4> A_cpu, A_cpu_inv;

		A_cpu.resize(num_frames+2);
		A_cpu_inv.resize(num_frames+2);
		As_padded.resize(num_hyps+2);
		As_inv_padded.resize(num_hyps+2);
		float buff_id[16] = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };
		for (int hyp = 0; hyp < num_hyps; hyp++){
			//padd with id matrix
			A_cpu[0] = float4x4(buff_id);
			A_cpu_inv[0] = float4x4(buff_id);
			for (int i = 0; i < num_frames; i++){
				//mult(a[hyp][frame], a_inv[hyp][i], buff);
				A_cpu[i+1] = float4x4(a[hyp][i]);
				A_cpu_inv[i+1] = float4x4(a_inv[hyp][i]);
			}
			A_cpu[num_frames+1] = float4x4(buff_id);
			A_cpu_inv[num_frames+1] = float4x4(buff_id);
			As_padded[hyp] = A_cpu;
			As_inv_padded[hyp] = A_cpu_inv;
		}

		As_per_frame.resize(num_frames);
		As_inv_per_frame.resize(num_frames);
		A_cpu.resize(num_hyps);
		A_cpu_inv.resize(num_hyps);
		for (int i = 0; i < num_frames; i++){
			for (int hyp = 0; hyp < num_hyps; hyp++){
				A_cpu[hyp] = float4x4(a[hyp][i]);
				A_cpu_inv[hyp] = float4x4(a_inv[hyp][i]);
			}
			As_per_frame[i] = A_cpu;
			As_inv_per_frame[i] = A_cpu_inv;
		}
	}

	~gpu_alignments(){

	}
};

//contains the dataterm for a frame- for all Hyps.
struct gpu_dataterm
{
	// keep entries for all dataterms and points on the gpu
	//this one is maybe slower at access but has a one time gpu<-->cpu overhead.
	//also this one needs to be initted once only
	std::vector<thrust::device_vector<float> > dterm, weight, crf_final_result;
	//to be used for what ever, for temporary use.
	thrust::device_vector<float> tmpbuffer;
	thrust::device_vector<float *> dterms_ptr, weights_ptr, results_ptr;
	int numPoints, numHyps;


	gpu_dataterm(int num_hyps, int num_queries, float base_penalty){
		numPoints = num_queries;
		numHyps = num_hyps;
		dterm.resize(num_hyps, thrust::device_vector<float>(num_queries, base_penalty));
		weight.resize(num_hyps, thrust::device_vector<float>(num_queries, 0.f));
		crf_final_result.resize(num_hyps, thrust::device_vector<float>(num_queries, 0.f));
		tmpbuffer.resize(num_queries);
		for (int i = 0; i < num_hyps; i++){
			dterms_ptr.push_back(thrust::raw_pointer_cast(dterm[i].data()));
			weights_ptr.push_back(thrust::raw_pointer_cast(weight[i].data()));
			results_ptr.push_back(thrust::raw_pointer_cast(crf_final_result[i].data()));
		}
	}

	void reset(float base_penalty){
		for (int i = 0; i < dterm.size(); i++){
			thrust::fill(dterm[i].begin(), dterm[i].end(), base_penalty);
			thrust::fill(weight[i].begin(), weight[i].end(), 0.f);
		}
	}

	void reset(int hyp, float base_penalty){
		thrust::fill(dterm[hyp].begin(), dterm[hyp].end(), base_penalty);
		thrust::fill(weight[hyp].begin(), weight[hyp].end(), 0.f);
	}

};

struct arg_max_functor {
	const int curentIdx;
	arg_max_functor(int i) : curentIdx(i) {}
	//current max, current val, current max idx
	__host__ __device__ void operator()(const thrust::tuple<float&, float&,int&> & mx_curr_argmx) const
	{
		bool currentValBigger = thrust::get<0>(mx_curr_argmx) < thrust::get<1>(mx_curr_argmx);
		thrust::get<2>(mx_curr_argmx) = (currentValBigger ? curentIdx : thrust::get<2>(mx_curr_argmx));
		thrust::get<0>(mx_curr_argmx) = (currentValBigger ? thrust::get<1>(mx_curr_argmx) : thrust::get<0>(mx_curr_argmx));
	}
};


struct gpu_queries
{
	thrust::device_vector<float> query_x;
	thrust::device_vector<float> query_y;
	thrust::device_vector<float> query_z;
	thrust::device_vector<float> query_nx;
	thrust::device_vector<float> query_ny;
	thrust::device_vector<float> query_nz;
	thrust::device_vector<int> query_frame;
	thrust::device_vector<int> label;
	thrust::device_vector<int> buffer_for_selected_queries;
	thrust::device_vector<int> buffer_for_selected_query_frm;


	gpu_queries(int num_queries){
		query_x.resize(num_queries);
		query_y.resize(num_queries);
		query_z.resize(num_queries);
		query_nx.resize(num_queries);
		query_ny.resize(num_queries);
		query_nz.resize(num_queries);
		query_frame.resize(num_queries);
		buffer_for_selected_queries.resize(num_queries);
		buffer_for_selected_query_frm.resize(num_queries);
		label.resize(num_queries);
	}

	void set(std::vector<pxyz_normal_frame> & queries){
		if (queries.size() != query_frame.size()){
			std::cout << "Error in " << __FILE__ << ", line " << __LINE__;
		}

		thrust::host_vector<float> to_copy(queries.size());
		thrust::host_vector<int> to_copy_i(queries.size());
		for (int i = 0; i < queries.size(); i++){
			to_copy[i] = queries[i].x;
		}
		query_x = to_copy;
		for (int i = 0; i < queries.size(); i++){
			to_copy[i] = queries[i].y;
		}
		query_y = to_copy;
		for (int i = 0; i < queries.size(); i++){
			to_copy[i] = queries[i].z;
		}
		query_z = to_copy;
		for (int i = 0; i < queries.size(); i++){
			to_copy[i] = queries[i].nx;
		}
		query_nx = to_copy;
		for (int i = 0; i < queries.size(); i++){
			to_copy[i] = queries[i].ny;
		}
		query_ny = to_copy;
		for (int i = 0; i < queries.size(); i++){
			to_copy[i] = queries[i].nz;
			to_copy_i[i] = queries[i].frame;
		}
		query_nz = to_copy;
		query_frame = to_copy_i;
	}

	void updateLabelsFromProbs(std::vector<thrust::device_vector<float> >& probabilities, thrust::device_vector<float>& tmpBuffer){
		thrust::fill(label.begin(), label.end(), 0);
		auto& mx = tmpBuffer;
		thrust::fill(mx.begin(), mx.end(), -1);
		int i = 0;
		for (auto& pr : probabilities){
			auto first = thrust::make_zip_iterator(thrust::make_tuple(mx.begin(), pr.begin(), label.begin()));
			auto last = thrust::make_zip_iterator(thrust::make_tuple(mx.end(), pr.end(), label.end()));
			thrust::for_each(first, last, arg_max_functor(i));
			i++;
		}
	}
};


struct eligible_for_frame
{
	int frame;
	
	eligible_for_frame(int frm){
		frame = frm;
	}

	
	__device__
		bool operator()(const thrust::tuple<int, int> & spl_id_frm)
	{
		int dist_seed = (thrust::get<1>(spl_id_frm) - frame);
		dist_seed = (dist_seed > 0 ? dist_seed : -dist_seed);
		//dist_seed = spl_id + 17;
		thrust::default_random_engine rng;
		rng.discard(thrust::get<0>(spl_id_frm));
		bool useFrame = dist_seed <= 4 ||
		(dist_seed <= 8 && (rng() % 4 == 0)) ||
		(dist_seed <= 16 && (rng() % 12 == 0)) ||
		rng() % 32 == 0;

		//bool useFrame = dist_seed % 2 == 0;
		return useFrame;
	}
};


#define P_X(zipped_tuple)  (thrust::get<0>(zipped_tuple))
#define P_Y(zipped_tuple)  (thrust::get<1>(zipped_tuple))
#define P_Z(zipped_tuple)  (thrust::get<2>(zipped_tuple))
#define P_NX(zipped_tuple)  (thrust::get<3>(zipped_tuple))
#define P_NY(zipped_tuple)  (thrust::get<4>(zipped_tuple))
#define P_NZ(zipped_tuple)  (thrust::get<5>(zipped_tuple))
#define P_DIST(zipped_tuple)  (thrust::get<6>(zipped_tuple))
#define P_WEIGHT(zipped_tuple)  (thrust::get<7>(zipped_tuple))
#define P_FRM(zipped_tuple)  (thrust::get<8>(zipped_tuple))

struct gpu_frame_dataterm_allQueries{

	//these are NOT device pointers....
	gpu_frame * prevframe;
	gpu_frame * frame;
	gpu_frame * nextframe;
	gpu_cam * cam;
	//pointer to alignment sets. They are padded, such that as[-1] = id and as[numFrames] = id;
	float4x4 * as, * as_inv;

	float clamp_dist_sqr;
	int frame_i;

	gpu_frame_dataterm_allQueries(gpu_frame * frm, gpu_frame * prevfrm, gpu_frame * nextfrm, gpu_cam * cm,
		//float4x4 * mt2Prev_devPtr, float4x4 * mt2Next_devPtr, 
		//gpu_alignments * aToFrame, 
		gpu_alignments * alignmentSet,
		float clamp_dist_square, 
		int hyp,
		int frame_idx)
	{
		cam = cm->dev_ptr; 
		frame = frm->dev_ptr;
		prevframe = prevfrm->dev_ptr;
		nextframe = nextfrm->dev_ptr;
		as = thrust::raw_pointer_cast(alignmentSet->As_padded[hyp].data()) + 1;
		as_inv = thrust::raw_pointer_cast(alignmentSet->As_inv_padded[hyp].data())+1;
		clamp_dist_sqr = clamp_dist_square;
		frame_i = frame_idx;

	}
	

	__device__ inline bool nn(float3 & p, gpu_frame * frame, float3 & xyz, float & bestDist_sqr){
		bool better, found = false;
		float dist_sqr;
		int i, j;
		cam->proj<float3>(p, i, j);
		int best_i = i;
		int best_j = j;

		bestDist_sqr = clamp_dist_sqr;

		for (int k = -3; k <= 3; k++)for (int l = -3; l <= 3; l++){
			xyz.z = tex2D<float>(frame->frame, i + k, j + l);
			//3. unproject
			xyz.x = cam->unproj_i2x(i + k, xyz.z);
			xyz.y = cam->unproj_j2y(j + l, xyz.z);
			dist_sqr = (xyz.x - p.x)*(xyz.x - p.x)
				+ (xyz.y - p.y)*(xyz.y - p.y)
				+ (xyz.z - p.z)*(xyz.z - p.z);

			better = (bestDist_sqr > dist_sqr) && (xyz.z > 0.0001);
			found = found || better;
			bestDist_sqr = (better ? dist_sqr : bestDist_sqr);
			best_i = (better ? i + k : best_i);
			best_j = (better ? j + l : best_j);

		}
		//xyz is filled in with zeros when nothing was found. zero depth is considered invalid.
		xyz.z = tex2D<float>(frame->frame, best_i, best_j) * (found? 1:0);
		xyz.x = cam->unproj_i2x(best_i, xyz.z);
		xyz.y = cam->unproj_j2y(best_j, xyz.z);
		return found;
	}

	
	//mapping the query to the frame and computing the dataterm 
	__device__ void operator() (const thrust::tuple<float&, float&, float&, float&, float&, float&, float&, float&, int &> & p){

		//cthe things to compute
		float dist_sqr_frame, dist_sqr_incr_n, dist_sqr_incr_p;
		float occlusion_prob;

		//temp vars.
		float3 Pxyz, Nxyz;
		float3 xyz, xyz2;
		//transform point coordinate and normal to frame
		Pxyz.x = P_X(p); Pxyz.y = P_Y(p); Pxyz.z = P_Z(p);
		as_inv[P_FRM(p)].mult(Pxyz, xyz);
		as[frame_i].mult(xyz, Pxyz);

		Nxyz.x = P_NX(p); Nxyz.y = P_NY(p); Nxyz.z = P_NZ(p);
		as_inv[P_FRM(p)].mult_top3x3(Nxyz, xyz);
		as[frame_i].mult_top3x3(xyz, Nxyz);

		//1.compute occlusion!
		int i = 0;
		int j = 0;
		cam->proj<float3>(Pxyz, i, j);
		//bool isIn = i >= 0 && j >= 0 && i < width_height[0] && j < width_height[1];
		bool isIn = cam->isIn(i, j);
		bool wasObserved = tex2D<float>(frame->frame, i, j) > 0.0001f;
		xyz.z = tex2D<float>(frame->frame, i, j);
		xyz.x = cam->unproj_i2x(i, xyz.z);
		xyz.y = cam->unproj_j2y(j, xyz.z);
		//cosine to view direction: -p.dot(n)/norm p
		auto & cos_view_dir = occlusion_prob;
		cos_view_dir = (-Nxyz.x*Pxyz.x - Nxyz.y*Pxyz.y - Nxyz.z*Pxyz.z) / sqrtf(Pxyz.x*Pxyz.x + Pxyz.y*Pxyz.y + Pxyz.z*Pxyz.z);
		//observation in fromt? occlusion prob will grow prop to square
		occlusion_prob = (xyz.z < Pxyz.z ? clamp_dist_sqr / (clamp_dist_sqr + (Pxyz.z - xyz.z)*(Pxyz.z - xyz.z)) : 1)	*
			(cos_view_dir > 0.3 ? 1 : (cos_view_dir > 0 ? cos_view_dir / 0.3 : 0));
		occlusion_prob = (1 - occlusion_prob)*isIn*wasObserved + (!wasObserved && isIn)* 0.9 + (!isIn) * 1;


		//2. find NN
		bool found_frm =
			nn(Pxyz, frame, xyz, dist_sqr_frame);


		//map it to the next frame and get dist
		auto & __ = Pxyz; //use as dummy argument.
		//mat2Next->mult(xyz, xyz2);
		as_inv[frame_i].mult(xyz, __);
		as[frame_i+1].mult(__, xyz2);
		
		bool found_next =
			nn(xyz2, nextframe, __, dist_sqr_incr_n);

		//map it to prev frame and get dist
		//mat2Prev->mult(xyz, xyz2);
		as_inv[frame_i].mult(xyz, __);
		as[frame_i - 1].mult(__, xyz2);
		bool found_prev =
			nn(xyz2, prevframe, __, dist_sqr_incr_p);

		float temp_dist_weight = (P_FRM(p) - frame_i);
		temp_dist_weight = (temp_dist_weight > 0 ? temp_dist_weight : -temp_dist_weight);
		//temp_dist_weight = (temp_dist_weight <= 2 ? 1 : 1 / (temp_dist_weight - 2));
		temp_dist_weight = (temp_dist_weight <= 3 ? 1 : 1 / (temp_dist_weight - 2));
		temp_dist_weight = (temp_dist_weight < 1 / 32.f ? 1 / 32.f : temp_dist_weight);
		//temp_dist_weight = (temp_dist_weight <= 4 ? 1 : 1 / (temp_dist_weight/2 - 2));
		//temp_dist_weight = (temp_dist_weight < 1 / 32.f ? 1 / 32.f : temp_dist_weight);
		//Look out for all border & non existant cases!
		//is clamped_dist_sqr if there was none.
		P_DIST(p) += dist_sqr_frame * (1 - occlusion_prob)*temp_dist_weight;
		P_DIST(p) += (found_frm ? (dist_sqr_incr_n + dist_sqr_incr_p) / 2 : clamp_dist_sqr)* (1 - occlusion_prob)*temp_dist_weight;

		P_WEIGHT(p) += (1 - occlusion_prob)*temp_dist_weight;
	}
};



struct gpu_frame_dataterm_allQueries_crf{

	//these are NOT device pointers....
	gpu_frame * prevframe;
	gpu_frame * frame;
	gpu_frame * stddev, * nx, * ny , * nz;
	gpu_frame * nextframe;
	gpu_frame * dense_Q_frame;
	gpu_cam * cam;
	//pointer to alignment sets. They are padded, such that as[-1] = id and as[numFrames] = id;
	float4x4 * as, *as_inv;

	float clamp_dist_sqr_fixed;
	float sigma_temp;
	int frame_i;

	gpu_frame_dataterm_allQueries_crf(gpu_frame * frm, gpu_frame * prevfrm, gpu_frame * nextfrm,
		gpu_frame * dense_Q_frm,
		gpu_frame * stddev_frame_,
		gpu_frame * nx_frame,
		gpu_frame * ny_frame,
		gpu_frame * nz_frame,
		gpu_cam * cm,
		//float4x4 * mt2Prev_devPtr, float4x4 * mt2Next_devPtr, 
		//gpu_alignments * aToFrame, 
		gpu_alignments * alignmentSet,
		float clamp_dist_square,
		float sigma_tmp,
		int hyp,
		int frame_idx)
	{
		cam = cm->dev_ptr;
		frame = frm->dev_ptr;
		prevframe = prevfrm->dev_ptr;
		nextframe = nextfrm->dev_ptr;
		dense_Q_frame = dense_Q_frm->dev_ptr;
		stddev = stddev_frame_->dev_ptr;
		nx = nx_frame->dev_ptr;
		ny = ny_frame->dev_ptr;
		nz = nz_frame->dev_ptr;

		as = thrust::raw_pointer_cast(alignmentSet->As_padded[hyp].data()) + 1;
		as_inv = thrust::raw_pointer_cast(alignmentSet->As_inv_padded[hyp].data()) + 1;
		//clamp_dist_sqr_fixed = clamp_dist_square;
		clamp_dist_sqr_fixed = clamp_dist_square;
		frame_i = frame_idx;
		sigma_temp = sigma_tmp;

//		std::cout << "gpu frame Clamp dist square: " << clamp_dist_square << "\n";
//stddev_frame_->set2Const(std::sqrtf(0.001));
gpuErrchk(cudaPeekAtLastError());
gpuErrchk(cudaDeviceSynchronize());
gpuErrchk(cudaPeekAtLastError());
	}


	__device__ inline bool nn(float3 & p, gpu_frame * frame, float3 & xyz, float & bestDist_sqr, int & best_i, int & best_j){
		bool better, found = false;
		float dist_sqr;
		int i, j;
		cam->proj<float3>(p, i, j);
		best_i = i;
		best_j = j;
		
		float clamp_dist_sqr = tex2D<float>(stddev->frame, i, j); clamp_dist_sqr *= clamp_dist_sqr; clamp_dist_sqr = (cam->isIn(i, j) && clamp_dist_sqr*0== 0 ? clamp_dist_sqr : clamp_dist_sqr_fixed);
		clamp_dist_sqr /= 4;
		bestDist_sqr = clamp_dist_sqr; // /(p.z > 1? p.z*p.z : 1);

		for (int k = -3; k <= 3; k++)for (int l = -3; l <= 3; l++){
			xyz.z = tex2D<float>(frame->frame, i + k, j + l);
			//3. unproject
			xyz.x = cam->unproj_i2x(i + k, xyz.z);
			xyz.y = cam->unproj_j2y(j + l, xyz.z);
			dist_sqr = (xyz.x - p.x)*(xyz.x - p.x)
				+ (xyz.y - p.y)*(xyz.y - p.y)
				+ (xyz.z - p.z)*(xyz.z - p.z);

			///dist_sqr /= xyz.z> 1 ? xyx.z * xyz.z : 1;

			better = (bestDist_sqr > dist_sqr) && (xyz.z > 0.0001);
			found = found || better;
			bestDist_sqr = (better ? dist_sqr : bestDist_sqr);
			best_i = (better ? i + k : best_i);
			best_j = (better ? j + l : best_j);

		}
		//xyz is filled in with zeros when nothing was found. zero depth is considered invalid.
		xyz.z = tex2D<float>(frame->frame, best_i, best_j) * (found ? 1 : 0);
		xyz.x = cam->unproj_i2x(best_i, xyz.z);
		xyz.y = cam->unproj_j2y(best_j, xyz.z);
		return found;
	}

	/*__device__ inline bool nn_noise_propto_dist(float3 & p, gpu_frame * frame, float3 & xyz, float & bestDist_sqr, int & best_i, int & best_j){
		bool better, found = false;
		float dist_sqr;
		int i, j;
		cam->proj<float3>(p, i, j);
		best_i = i;
		best_j = j;
float clamp_dist_sqr = tex2D<float>(stddev->frame, i, j); clamp_dist_sqr *= clamp_dist_sqr; clamp_dist_sqr = (cam->isIn(i, j) &&clamp_dist_sqr*0==0? clamp_dist_sqr : clamp_dist_sqr_fixed);
		bestDist_sqr = clamp_dist_sqr /(p.z > 1? p.z*p.z : 1);

		for (int k = -3; k <= 3; k++)for (int l = -3; l <= 3; l++){
			xyz.z = tex2D<float>(frame->frame, i + k, j + l);
			//3. unproject
			xyz.x = cam->unproj_i2x(i + k, xyz.z);
			xyz.y = cam->unproj_j2y(j + l, xyz.z);
			dist_sqr = (xyz.x - p.x)*(xyz.x - p.x)
				+ (xyz.y - p.y)*(xyz.y - p.y)
				+ (xyz.z - p.z)*(xyz.z - p.z);

			dist_sqr /= xyz.z> 1 ? xyz.z * xyz.z : 1;

			better = (bestDist_sqr > dist_sqr) && (xyz.z > 0.0001);
			found = found || better;
			bestDist_sqr = (better ? dist_sqr : bestDist_sqr);
			best_i = (better ? i + k : best_i);
			best_j = (better ? j + l : best_j);

		}
		//xyz is filled in with zeros when nothing was found. zero depth is considered invalid.
		xyz.z = tex2D<float>(frame->frame, best_i, best_j) * (found ? 1 : 0);
		xyz.x = cam->unproj_i2x(best_i, xyz.z);
		xyz.y = cam->unproj_j2y(best_j, xyz.z);
		return found;
	}*/
	//mapping the query to the frame and computing the dataterm 
	__device__ void operator() (const thrust::tuple<float&, float&, float&, float&, float&, float&, float&, float&, int &> & p){

		//cthe things to compute
		float dist_sqr_frame, dist_sqr_incr_n, dist_sqr_incr_p;
		float occlusion_prob;

		//temp vars.
		float3 Pxyz, Nxyz;
		//float Nxyz_observed; //TODO: Unused. Remove?
		float3 xyz, xyz2;
		//transform point coordinate and normal to frame
		Pxyz.x = P_X(p); Pxyz.y = P_Y(p); Pxyz.z = P_Z(p);
		as_inv[P_FRM(p)].mult(Pxyz, xyz);
		as[frame_i].mult(xyz, Pxyz);

		//compute its coordinate
		int i = 0;
		int j = 0;
		cam->proj<float3>(Pxyz, i, j);
		bool isIn = cam->isIn(i, j);
		bool wasObserved = tex2D<float>(frame->frame, i, j) > 0.0001f;

		//compute mapped normal
		Nxyz.x = P_NX(p); Nxyz.y = P_NY(p); Nxyz.z = P_NZ(p);
		as_inv[P_FRM(p)].mult_top3x3(Nxyz, xyz);
		as[frame_i].mult_top3x3(xyz, Nxyz);

		//compute observed normal
		//normals are optional & experimental....
		//--------------------------------------------
		float p_same = 0; //psame = 0 gives original error
		/*Nxyz_observed.x = tex2D<float>(nx->frame, i, j);
		Nxyz_observed.y = tex2D<float>(ny->frame, i, j);
		Nxyz_observed.z = tex2D<float>(nz->frame, i, j);

		p_same = Nxyz_observed.x* Nxyz.x + Nxyz_observed.y* Nxyz.y + Nxyz_observed.z* Nxyz.z; 
		p_same = (p_same > 0 ? p_same : 0);
		//p_same = ((!isIn ||!wasObserved )? 1 : p_same); //vs punishing being out or unobserved. makes it worse.
		//*/
		//--------------------------------------------


		//1.compute occlusion!
		float makeupFactor = 1;
		float clamp_dist_sqr = tex2D<float>(stddev->frame, i, j); clamp_dist_sqr *= clamp_dist_sqr; clamp_dist_sqr = (cam->isIn(i, j) && clamp_dist_sqr*0== 0 ? clamp_dist_sqr : clamp_dist_sqr_fixed); makeupFactor = 0.001f / clamp_dist_sqr;
		float occlustion_dist_sqr = clamp_dist_sqr;
		clamp_dist_sqr /= 4;

		xyz.z = tex2D<float>(frame->frame, i, j);
		xyz.x = cam->unproj_i2x(i, xyz.z);
		xyz.y = cam->unproj_j2y(j, xyz.z);
		//cosine to view direction: -p.dot(n)/norm p
		auto & cos_view_dir = occlusion_prob;
		cos_view_dir = (-Nxyz.x*Pxyz.x - Nxyz.y*Pxyz.y - Nxyz.z*Pxyz.z) / sqrtf(Pxyz.x*Pxyz.x + Pxyz.y*Pxyz.y + Pxyz.z*Pxyz.z);
		//observation in fromt? occlusion prob will grow prop to square
		occlusion_prob = (xyz.z < Pxyz.z ? occlustion_dist_sqr / (occlustion_dist_sqr + (Pxyz.z - xyz.z)*(Pxyz.z - xyz.z)) : 1)	*
			(cos_view_dir > 0.3 ? 1 : (cos_view_dir > 0 ? cos_view_dir / 0.3 : 0));
		occlusion_prob = (1 - occlusion_prob)*isIn*wasObserved + (!wasObserved && isIn)* 0.9 + (!isIn) * 1;
//alternative ways of treating occlusion.
//occlusion_prob = (1 - occlusion_prob)*isIn*wasObserved + (!wasObserved && isIn)* 1 + (!isIn) * 1; //tag:-4: not much diff.
//occlusion_prob =(occlusion_prob > 0.9 ? 0.9 : occlusion_prob);// 0.5 + occlusion_prob/2;//tag:-2 0.8. -5:0.9
//occlusion_prob = (1 - occlusion_prob)*isIn*wasObserved + (!wasObserved && isIn)* 0.5 + (!isIn) * 0.5;

		
		//2. find NN
		int nn_coord_i, nn_coord_j;
		bool found_frm =
			nn(Pxyz, frame, xyz, dist_sqr_frame, nn_coord_i, nn_coord_j);
			//nn_noise_propto_dist(Pxyz, frame, xyz, dist_sqr_frame, nn_coord_i, nn_coord_j);

		//get the hyp probability Q(hyp)
		float Q_hyp = tex2D<float>(dense_Q_frame->frame, nn_coord_i, nn_coord_j);


		//map it to the next frame and get dist
		auto & __ = Pxyz; //use as dummy argument.
		//mat2Next->mult(xyz, xyz2);
		as_inv[frame_i].mult(xyz, __);
		as[frame_i + 1].mult(__, xyz2);

		//bool found_next =
		nn(xyz2, nextframe, __, dist_sqr_incr_n, nn_coord_i, nn_coord_j);
		cam->proj<float3>(xyz2, nn_coord_i, nn_coord_j);
		//occlusion & validity nn_coord abused for xyz2s coords.
		__.z = tex2D<float>(nextframe->frame, nn_coord_i, nn_coord_j);
		float next_poormans_unocclusion_prob = (__.z < xyz2.z ? occlustion_dist_sqr / (occlustion_dist_sqr + (xyz2.z - __.z)*(xyz2.z - __.z)) : 1);
		bool next_valid = cam->isIn(nn_coord_i, nn_coord_j) && tex2D<float>(nextframe->frame, nn_coord_i, nn_coord_j) > 0.0001f;

		//map it to prev frame and get dist
		//mat2Prev->mult(xyz, xyz2);
		as_inv[frame_i].mult(xyz, __);
		as[frame_i - 1].mult(__, xyz2);
		nn(xyz2, prevframe, __, dist_sqr_incr_p, nn_coord_i, nn_coord_j);
		cam->proj<float3>(xyz2, nn_coord_i, nn_coord_j);
		__.z = tex2D<float>(prevframe->frame, nn_coord_i, nn_coord_j);
		float prev_poormans_unocclusion_prob = (__.z < xyz2.z ? occlustion_dist_sqr / (occlustion_dist_sqr + (xyz2.z - __.z)*(xyz2.z - __.z)) : 1);
		bool prev_valid = cam->isIn(nn_coord_i, nn_coord_j) && tex2D<float>(prevframe->frame, nn_coord_i, nn_coord_j) > 0.0001f;;

		float error_prev_next =
			//(prev_valid && next_valid) * (dist_sqr_incr_n + dist_sqr_incr_p) / 2 + //this was the standard. could weight them with occlusion probs
			(prev_valid && next_valid) * (dist_sqr_incr_n*next_poormans_unocclusion_prob + dist_sqr_incr_p*prev_poormans_unocclusion_prob) / (next_poormans_unocclusion_prob + prev_poormans_unocclusion_prob) +
			(prev_valid && ! next_valid )* dist_sqr_incr_p +
			(next_valid && ! prev_valid) * dist_sqr_incr_n +
			(!next_valid && !prev_valid) *clamp_dist_sqr;


		float temp_dist_weight = (P_FRM(p) - frame_i);
		temp_dist_weight = (temp_dist_weight > 0 ? temp_dist_weight : -temp_dist_weight);
		//temp_dist_weight = (temp_dist_weight <= 2 ? 1 : 1 / (temp_dist_weight - 2));
		
		//variant one
		/*temp_dist_weight = (temp_dist_weight <= 3 ? 1 : 1 / (temp_dist_weight - 2));
		temp_dist_weight = (temp_dist_weight < 1 / 32.f ? 1 / 32.f : temp_dist_weight);*/
		//variant two
		temp_dist_weight = exp(-temp_dist_weight*temp_dist_weight / sigma_temp / sigma_temp);

		//temp_dist_weight = (temp_dist_weight <= 4 ? 1 : 1 / (temp_dist_weight/2 - 2));
		//temp_dist_weight = (temp_dist_weight < 1 / 32.f ? 1 / 32.f : temp_dist_weight);
		//Look out for all border & non existant cases!
		//is clamped_dist_sqr if there was none.
		P_DIST(p) += (dist_sqr_frame) * (1 - occlusion_prob)*temp_dist_weight *Q_hyp*makeupFactor * (1 - p_same);
		P_DIST(p) += ((found_frm ? error_prev_next : clamp_dist_sqr))* (1 - occlusion_prob)*temp_dist_weight *Q_hyp*makeupFactor * (1 - p_same);
		P_DIST(p) += (Q_hyp < 1 ? 1 - Q_hyp : 0)*temp_dist_weight*(2 * (clamp_dist_sqr))* (1 - occlusion_prob)*makeupFactor * (1 - p_same);

		P_WEIGHT(p) += (1 - occlusion_prob)*temp_dist_weight;
	}
};

struct gpu_frame_dataterm_all{

	//these are NOT device pointers....
	gpu_frame * prevframe;
	gpu_frame * frame;
	gpu_frame * nextframe;
	gpu_cam * cam;

	//pointer to alignment sets. They are padded, such that as[-1] = id and as[numFrames] = id;
	float4x4 * as, *as_inv;
	//list of all queries ever.
	float * px, *py, *pz, *pnx, *pny, *pnz;
	int * p_frame;
	float * dterm_all, *weight_all;

	float clamp_dist_sqr;
	int frame_i;

	gpu_frame_dataterm_all(gpu_frame * frm, gpu_frame * prevfrm, gpu_frame * nextfrm, gpu_cam * cm,
		gpu_alignments * alignmentSet,
		gpu_queries * all_queries,
		gpu_dataterm * dterm,
		float clamp_dist_square,
		int hyp,
		int frame_idx)
	{
		cam = cm->dev_ptr;
		frame = frm->dev_ptr;
		prevframe = prevfrm->dev_ptr;
		nextframe = nextfrm->dev_ptr;
		as = thrust::raw_pointer_cast(alignmentSet->As_padded[hyp].data()) + 1;
		as_inv = thrust::raw_pointer_cast(alignmentSet->As_inv_padded[hyp].data()) + 1;
		clamp_dist_sqr = clamp_dist_square;
		frame_i = frame_idx;
		px = thrust::raw_pointer_cast(all_queries->query_x.data());
		py = thrust::raw_pointer_cast(all_queries->query_y.data());
		pz = thrust::raw_pointer_cast(all_queries->query_z.data());
		pnx = thrust::raw_pointer_cast(all_queries->query_nx.data());
		pny = thrust::raw_pointer_cast(all_queries->query_ny.data());
		pnz = thrust::raw_pointer_cast(all_queries->query_nz.data());
		p_frame = thrust::raw_pointer_cast(all_queries->query_frame.data());

		dterm_all = thrust::raw_pointer_cast(dterm->dterm[hyp].data());
		weight_all = thrust::raw_pointer_cast(dterm->weight[hyp].data());
	}


	__device__ inline bool nn(float3 & p, gpu_frame * frame, float3 & xyz, float & bestDist_sqr){
		bool better, found = false;
		float dist_sqr;
		int i, j;
		cam->proj<float3>(p, i, j);
		int best_i = i;
		int best_j = j;

		bestDist_sqr = clamp_dist_sqr;

		for (int k = -3; k <= 3; k++)for (int l = -3; l <= 3; l++){
			xyz.z = tex2D<float>(frame->frame, i + k, j + l);
			//3. unproject
			xyz.x = cam->unproj_i2x(i + k, xyz.z);
			xyz.y = cam->unproj_j2y(j + l, xyz.z);
			dist_sqr = (xyz.x - p.x)*(xyz.x - p.x)
				+ (xyz.y - p.y)*(xyz.y - p.y)
				+ (xyz.z - p.z)*(xyz.z - p.z);

			better = (bestDist_sqr > dist_sqr) && (xyz.z > 0.0001);
			found = found || better;
			bestDist_sqr = (better ? dist_sqr : bestDist_sqr);
			best_i = (better ? i + k : best_i);
			best_j = (better ? j + l : best_j);

		}
		//xyz is filled in with zeros when nothing was found. zero depth is considered invalid.
		xyz.z = tex2D<float>(frame->frame, best_i, best_j) * (found ? 1 : 0);
		xyz.x = cam->unproj_i2x(best_i, xyz.z);
		xyz.y = cam->unproj_j2y(best_j, xyz.z);
		return found;
	}


	//mapping the query to the frame and computing the dataterm 
	__device__ void operator() (int & query_idx){

		//cthe things to compute
		float dist_sqr_frame, dist_sqr_incr_n, dist_sqr_incr_p;
		float occlusion_prob;
		int query = query_idx;

		//temp vars.
		float3 Pxyz, Nxyz;
		float3 xyz, xyz2;
		//transform point coordinate and normal to frame
		Pxyz.x = px[query]; Pxyz.y = py[query]; Pxyz.z = pz[query];
		as_inv[p_frame[query]].mult(Pxyz, xyz);
		as[frame_i].mult(xyz, Pxyz);

		Nxyz.x = pnx[query]; Nxyz.y = pny[query]; Nxyz.z = pnz[query];
		as_inv[p_frame[query]].mult_top3x3(Nxyz, xyz);
		as[frame_i].mult_top3x3(xyz, Nxyz);

		//1.compute occlusion!
		int i = 0;
		int j = 0;
		cam->proj<float3>(Pxyz, i, j);
		//bool isIn = i >= 0 && j >= 0 && i < width_height[0] && j < width_height[1];
		bool isIn = cam->isIn(i, j);
		bool wasObserved = tex2D<float>(frame->frame, i, j) > 0.0001f;
		xyz.z = tex2D<float>(frame->frame, i, j);
		xyz.x = cam->unproj_i2x(i, xyz.z);
		xyz.y = cam->unproj_j2y(j, xyz.z);
		//cosine to view direction: -p.dot(n)/norm p
		auto & cos_view_dir = occlusion_prob;
		cos_view_dir = (-Nxyz.x*Pxyz.x - Nxyz.y*Pxyz.y - Nxyz.z*Pxyz.z) / sqrtf(Pxyz.x*Pxyz.x + Pxyz.y*Pxyz.y + Pxyz.z*Pxyz.z);
		//observation in fromt? occlusion prob will grow prop to square
		occlusion_prob = (xyz.z < Pxyz.z ? clamp_dist_sqr / (clamp_dist_sqr + (Pxyz.z - xyz.z)*(Pxyz.z - xyz.z)) : 1)	*
			(cos_view_dir > 0.3 ? 1 : (cos_view_dir > 0 ? cos_view_dir / 0.3 : 0));
		occlusion_prob = (1 - occlusion_prob)*isIn*wasObserved + (!wasObserved && isIn)* 0.9 + (!isIn) * 1;



		
		//2. find NN
		bool found_frm =
			nn(Pxyz, frame, xyz, dist_sqr_frame);


		//map it to the next frame and get dist
		auto & __ = Pxyz; //use as dummy argument.
		//mat2Next->mult(xyz, xyz2);
		as_inv[frame_i].mult(xyz, __);
		as[frame_i + 1].mult(__, xyz2);

		bool found_next =
			nn(xyz2, nextframe, __, dist_sqr_incr_n);

		//map it to prev frame and get dist
		//mat2Prev->mult(xyz, xyz2);
		as_inv[frame_i].mult(xyz, __);
		as[frame_i - 1].mult(__, xyz2);
		bool found_prev =
			nn(xyz2, prevframe, __, dist_sqr_incr_p);

		//Look out for all border & non existant cases!
		//is clamped_dist_sqr if there was none.
		dterm_all[query]	+= dist_sqr_frame * (1 - occlusion_prob)
					+ (found_frm ? (dist_sqr_incr_n + dist_sqr_incr_p) / 2 : clamp_dist_sqr)* (1 - occlusion_prob);
			
		weight_all[query] += (1 - occlusion_prob);

	}

};




cudaDataterm::cudaDataterm(sensorDescriptionData & sensor, float clamp_dist_squared, float basepenalty, int numFrames, int samples_per_frame)
	:spatial_crf(sensor._width, sensor._height)
{
	dummyFrame = new gpu_frame();
	cam = new gpu_cam(sensor);
	w = sensor._width;
	h = sensor._height;
	
	std::vector<float> dummyQ(w*h, 1.f);
	dummy_Q_frame = new gpu_frame(dummyQ, w, h);
	
	clamp_dist_sqr = clamp_dist_squared;
	base_penalty = basepenalty;
	dterm = NULL;
	gpuMotion = NULL;
	allQueries = new gpu_queries(numFrames * samples_per_frame);
}

cudaDataterm::~cudaDataterm(){
	for (auto & gframe : gpu_frames){
		delete gframe;
		gframe = NULL;
	}
	for (auto & gframe : gpu_frames_noise){
		delete gframe;
		gframe = NULL;
	}
	for (auto & gframe : gpu_frame_nx){
		delete gframe;
		gframe = NULL;
	}
	for (auto & gframe : gpu_frame_ny){
		delete gframe;
		gframe = NULL;
	}
	for (auto & gframe : gpu_frame_nz){
		delete gframe;
		gframe = NULL;
	}
	for (auto & gframe : gpu_frames_dense_Q){
		delete gframe;
		gframe = NULL;
	}
	for (auto & gframe : all_crf_sums){
		delete gframe;
		gframe = NULL;
	}
	for (auto & gframe : all_crf_maxs){
		delete gframe;
		gframe = NULL;
	}
	for (auto & gframe : all_crf_mins){
		delete gframe;
		gframe = NULL;
	}
	if (cam != NULL){
		delete cam;
		cam = NULL;
	}
	if (gpuMotion != NULL){
		delete gpuMotion;
		gpuMotion = NULL;
	}
	if (dterm != NULL){
		delete dterm;
		dterm = NULL;
	}
	delete allQueries;
	delete dummyFrame;
	delete dummy_Q_frame;
}

cudaDataterm::spatial_crf_singleFrameResults::spatial_crf_singleFrameResults(int w_, int h_):
	zeros(w_*h_, 0.f)
{
	lambda_spatial = sigma_spatial = -1;
	w = w_; h = h_;
	mins = new gpu_frame(zeros, w, h);
	sums = new gpu_frame(zeros, w, h);
	maxs = new gpu_frame(zeros, w, h);
	tempBuffer = new gpu_frame(zeros, w, h);

	generalpurposeBuffer = new gpu_frame(zeros, w, h);
	//dIdySAT = new gpu_frame(zeros, w, h);
	//valSAT = new gpu_frame(zeros, w, h);

	spatial_support_2 = 45;//15;
	
}
cudaDataterm::spatial_crf_singleFrameResults::~spatial_crf_singleFrameResults()
{
	for (auto & gframe : gpu_buffers_Q){
		delete gframe;
		gframe = NULL;
	}
	delete mins;
	delete maxs;
	delete sums;
	delete tempBuffer;
	delete generalpurposeBuffer;
	//delete dIdxSAT;
	//delete dIdySAT;
	//delete valSAT;
}

void cudaDataterm::spatial_crf_singleFrameResults::resize(int numhyp)
{
	
	//reduce size?
	//std::cout << "***Access gpu_buffer_Q (1): ";
	if (numhyp < gpu_buffers_Q.size())
	{
		//std::cout << "Decreasing number of buffers from " << gpu_buffers_Q.size() << " to " << numhyp << "\n";
		//free the resources
		for (int i = numhyp; i < gpu_buffers_Q.size(); i++){
			//delete and remove the pointer.
			delete gpu_buffers_Q.back();
			gpu_buffers_Q.pop_back();
		}
		//gpu_buffers_Q.resize(numhyp);
	}
	//increase size
	else if (numhyp > gpu_buffers_Q.size()){
		//std::cout << "Increasing number of buffers from " << gpu_buffers_Q.size() << " to " << numhyp << "\n";
		std::vector<float> dummyQ(w*h, 1.f);
		for (int i = gpu_buffers_Q.size(); i < numhyp; i++){
			gpu_buffers_Q.push_back(new gpu_frame(dummyQ, w, h));
		}
	}
}

void cudaDataterm::spatial_crf_singleFrameResults::getMinsAndSums(gpu_frame * mins_, gpu_frame * sums_, gpu_frame * maxs_target){
	mins->copyThisTo(mins_);
	sums->copyThisTo(sums_);
	if (maxs_target != NULL){
		maxs->copyThisTo(maxs_target);
	}
}

void cudaDataterm::pushCloud2Gpu(std::vector<float> & depth_frame, std::vector<float> & noise_frame,
	std::vector<float> & nx, std::vector<float> & ny, std::vector<float> & nz){
	gpu_frames.push_back(new gpu_frame(depth_frame, w, h));
	gpu_frames_noise.push_back(new gpu_frame(noise_frame, w, h));
	gpu_frames_dense_Q.push_back(new gpu_frame(depth_frame, w, h));
	gpu_frame_nx.push_back(new gpu_frame(nx, w, h));
	gpu_frame_ny.push_back(new gpu_frame(ny, w, h));
	gpu_frame_nz.push_back(new gpu_frame(nz, w, h));

	all_crf_sums.push_back(new gpu_frame(depth_frame, w, h));
	all_crf_maxs.push_back(new gpu_frame(depth_frame, w, h));
	all_crf_mins.push_back(new gpu_frame(depth_frame, w, h));
}

void cudaDataterm::setSparsePoints(std::vector<pxyz_normal_frame> & queries_all_frames)
{
	allQueries->set(queries_all_frames);
	frame_QueryRange.clear();
	if (queries_all_frames.size() == 0)
		return;

	//compute which queries belong to which frame.
	int i = 0, first = 0; 
	int frame = queries_all_frames[0].frame;
	assert(frame == 0);
	if (frame != 0){
		std::cout << "Assertion failed in " << __FILE__ << " " << __LINE__ << ": " << frame << "\n";
		exit(1);
	}
	while (i < queries_all_frames.size())
	{
		assert(queries_all_frames[i].frame >= frame);
		if (queries_all_frames[i].frame < frame){
			std::cout << "Assertion failed in " << __FILE__ << " " << __LINE__ << ": frames not sorted\n";
			exit(1);
		}

		if (queries_all_frames[i].frame > frame){
			frame = queries_all_frames[i].frame;
			frame_QueryRange.push_back(std::pair<int, int>(first, i));
			first = i;
		}
		i++;
	}
	frame_QueryRange.push_back(std::pair<int, int>(first, i));

	if (frame_QueryRange.size() != gpu_frames.size()){
		std::cout << "assertion failed in " << __FILE__ << " " << __LINE__ << "\n";
		std::cout << frame_QueryRange.size() << " VS " << gpu_frames.size() << "\n";
		exit(1);
	}
}

void cudaDataterm::setQ(int frame, std::vector<float> & probs){
	if (frame >= gpu_frames_dense_Q.size() || frame < 0){
		std::cout << "Assertion failed in " << __FILE__ << " " << __LINE__ << "\n";
		std::cout << frame << " >= " << gpu_frames_dense_Q.size();
		exit(1);
		return;
	}
	else{
		gpu_frames_dense_Q.at(frame)->copy2Gpu(probs);
	}
}

void cudaDataterm::setQ(int frame, float toConst)
{
	if (frame >= gpu_frames_dense_Q.size() || frame < 0){
		std::cout << "Assertion failed in " << __FILE__ << " " << __LINE__ << "\n";
		std::cout << frame << " >= " << gpu_frames_dense_Q.size();
		exit(1);
		return;
	}
	else{
		gpu_frames_dense_Q.at(frame)->set2Const(toConst);
	}
}

void cudaDataterm::setQ(int frame, int coord_x, int coord_y, float toConst)
{
	if (frame >= gpu_frames_dense_Q.size() || frame < 0){
		std::cout << "Assertion failed in " << __FILE__ << " " << __LINE__ << "\n";
		std::cout << frame << " >= " << gpu_frames_dense_Q.size();
		exit(1);
		return;
	}
	else{
		if (coord_x >= 0 && coord_y >= 0 && coord_x < w && coord_y < h){
			gpu_frames_dense_Q.at(frame)->inefficientSet(coord_x, coord_y,toConst);
		}
		
	}
}

void cudaDataterm::initSparseValues(int numhyp){
	//gpuErrchk(cudaPeekAtLastError());
	//gpuErrchk(cudaDeviceSynchronize());
	if (dterm != NULL){
		delete dterm;
	}
	//gpuErrchk(cudaPeekAtLastError());
	//gpuErrchk(cudaDeviceSynchronize());
	std::cout << "Cuda: preparing gpu for " << numhyp << " hypos with " << this->allQueries->query_x.size() << "sparse points";
	dterm = new gpu_dataterm(numhyp, this->allQueries->query_x.size(), base_penalty);

	spatial_crf.resize(numhyp);

	size_t mem_tot_0 = 0;
	size_t mem_free_0 = 0;
	cudaMemGetInfo(&mem_free_0, &mem_tot_0);
	std::cout << "\n\n \t Free GPU memory in GB: " << mem_free_0 / (1e9f) << "/" << mem_tot_0 / (1e9f) << "\n\n";
	gpuErrchk(cudaPeekAtLastError());
	gpuErrchk(cudaDeviceSynchronize());
}

void cudaDataterm::resetSparseValues(){
	//gpuErrchk(cudaPeekAtLastError());
	//gpuErrchk(cudaDeviceSynchronize());
	dterm->reset(base_penalty);
	//gpuErrchk(cudaPeekAtLastError());
	//gpuErrchk(cudaDeviceSynchronize());
	//gpuErrchk(cudaPeekAtLastError());
}

void cudaDataterm::resetSparseValuesTo(float what){
	dterm->reset(what);
}
void cudaDataterm::resetSparseValues(int l){
	dterm->reset(l,base_penalty);
}

void cudaDataterm::resetSparseValues(int l, float pen){
	dterm->reset(l, pen);
}

void cudaDataterm::getSparseValues_FilledIn(std::vector<std::vector<float>> & target, int frame){

		int firstQ = frame_QueryRange[frame].first;
		int lastQ = frame_QueryRange[frame].second;

		spatial_crf.fillInSparse2Dense(
			allQueries,
			dterm,
			firstQ, lastQ,
			gpu_frames[frame], 
			cam
			);

		for (int hyp = 0; hyp < target.size(); hyp++){
			spatial_crf.getAndNormalize(hyp, target[hyp], false);
		}

}

void cudaDataterm::setMotionSet(std::vector<std::vector<float*>> & motion, std::vector<std::vector<float*>> & motion_inv)
{
	if (gpuMotion != NULL){
		delete gpuMotion;
	}
	gpuMotion = new gpu_alignments(motion, motion_inv, -1);
}


void cudaDataterm::addDataTerm(int frame, std::vector<int> & queries_for_frame){
	if (gpuMotion->num_hyps != dterm->dterm.size() || dterm->dterm.size() != dterm->weight.size()){
		std::cout << "Error, incorrect gpu allocations (mismatch) in " << __FILE__ << "line" << __LINE__;
		exit(2);
	}

	thrust::host_vector<int> queries;
	queries.assign(queries_for_frame.begin(), queries_for_frame.end());
	thrust::device_vector<int> queries_gpu = queries;

		for (int hyp = 0; hyp < dterm->dterm.size(); hyp++)
		{

			gpu_frame_dataterm_all frame_projection(
				gpu_frames.at(frame),
				(frame > 0 ? gpu_frames.at(frame - 1) : dummyFrame),
				(frame + 1 < gpu_frames.size() ? gpu_frames.at(frame + 1) : dummyFrame),
				cam,
				gpuMotion,
				allQueries,
				dterm,
				clamp_dist_sqr,
				hyp,
				frame);

			thrust::for_each(
				queries_gpu.begin(),
				queries_gpu.end(),
				frame_projection);

		}
}

void cudaDataterm::addDataTerm(int frame)//, std::vector<int> & queries_for_frame)
{

	if (gpuMotion->num_hyps != dterm->dterm.size() || dterm->dterm.size() != dterm->weight.size()){
		std::cout << "Error, incorrect gpu allocations (mismatch) in " << __FILE__ << "line" << __LINE__;
		exit(2);
	}
		

	try
	{
		//The followin code fragment samples a set of indices describing the 
		//points for which the current frame should be taken into account- purely on gpu
		eligible_for_frame is_eligible(frame);
		thrust::counting_iterator<int> first(0);
		thrust::counting_iterator<int> last = first + allQueries->query_frame.size();
		auto zip_start = thrust::make_zip_iterator(thrust::make_tuple(
			allQueries->buffer_for_selected_queries.begin(), 
			allQueries->buffer_for_selected_query_frm.begin()));
		auto zip_end = thrust::copy_if(thrust::make_zip_iterator(thrust::make_tuple(first, allQueries->query_frame.begin())),
			thrust::make_zip_iterator(thrust::make_tuple(last, allQueries->query_frame.end())),
			zip_start,	is_eligible);
		int num = zip_end - zip_start;
		auto selected_queries_begin = allQueries->buffer_for_selected_queries.begin();
		auto selected_queries_end = selected_queries_begin + num;

	
		//add the frames contribution to the dataterm of the sampled elements.
//#define TIME_PURE_GPU_TIME
#ifdef TIME_PURE_GPU_TIME
		std::cout << "sampled: " << (selected_queries_end - selected_queries_begin) << "queries\n";
		cudaEvent_t start, stop;
		HANDLE_ERROR(cudaEventCreate(&start));
		HANDLE_ERROR(cudaEventCreate(&stop));
		HANDLE_ERROR(cudaEventRecord(start, 0));
#endif
		for (int hyp = 0; hyp < dterm->dterm.size(); hyp++)
		{

			gpu_frame_dataterm_all frame_projection(
				gpu_frames.at(frame),
				(frame > 0 ? gpu_frames.at(frame - 1) : dummyFrame),
				(frame + 1 < gpu_frames.size() ? gpu_frames.at(frame + 1) : dummyFrame),
				cam,
				gpuMotion,
				allQueries,
				dterm,
				clamp_dist_sqr,
				hyp,
				frame);

			thrust::for_each(
				selected_queries_begin,
				selected_queries_end,
				frame_projection);

		}
#ifdef TIME_PURE_GPU_TIME
		HANDLE_ERROR(cudaEventRecord(stop, 0));
		HANDLE_ERROR(cudaEventSynchronize(stop));
		std::cout << "called!\n";
		float elapsedTime;
		HANDLE_ERROR(cudaEventElapsedTime(&elapsedTime, start, stop));
		std::cout << "\n cudadataterm Took: " << elapsedTime;
		cudaEventDestroy(start);
		cudaEventDestroy(stop);
#endif

	}
	catch (thrust::system_error & e){
		std::cout << "Cought thrust error: " << __FILE__ << __LINE__<< "\n";
		std::cout << e.what();
		std::cout << "Exiting now --\n";
		exit(2);
	}

}

void cudaDataterm::addDataTerm_allQueries(int frame)
{

#ifdef TIME_PURE_GPU_TIME
	cudaEvent_t start, stop, stop2;
	HANDLE_ERROR(cudaEventCreate(&start));
	HANDLE_ERROR(cudaEventCreate(&stop));
	HANDLE_ERROR(cudaEventCreate(&stop2));
	HANDLE_ERROR(cudaEventRecord(start, 0));
#endif

	for (int hyp = 0; hyp < dterm->dterm.size(); hyp++)
	{
		gpu_frame_dataterm_allQueries frame_projection(
			gpu_frames.at(frame),
			(frame > 0 ? gpu_frames.at(frame - 1) : dummyFrame),
			(frame + 1 < gpu_frames.size() ? gpu_frames.at(frame + 1) : dummyFrame),
			cam,
			//gpuMat2Prev.dev_ptr, gpuMat2Next.dev_ptr, a_to_frame, 
			gpuMotion,
			clamp_dist_sqr,
			hyp,
			frame);

		thrust::for_each(
			thrust::make_zip_iterator(
			thrust::make_tuple(allQueries->query_x.begin(), allQueries->query_y.begin(), allQueries->query_z.begin(),
			allQueries->query_nx.begin(), allQueries->query_ny.begin(), allQueries->query_nz.begin(), dterm->dterm[hyp].begin(), dterm->weight[hyp].begin(), allQueries->query_frame.begin())),
			thrust::make_zip_iterator(
			thrust::make_tuple(allQueries->query_x.end(), allQueries->query_y.end(), allQueries->query_z.end(),
			allQueries->query_nx.end(), allQueries->query_ny.end(), allQueries->query_nz.end(), dterm->dterm[hyp].end(), dterm->weight[hyp].end(), allQueries->query_frame.end())),
			frame_projection);

	}
#ifdef TIME_PURE_GPU_TIME
	cudaEventRecord(stop2, 0);
	cudaEventSynchronize(stop2);
	float elapsedTime;
	HANDLE_ERROR(cudaEventElapsedTime(&elapsedTime, start, stop2));
	std::cout << "\n cudadataterm Took: " << elapsedTime;
	HANDLE_ERROR(cudaEventDestroy(start));
	HANDLE_ERROR(cudaEventDestroy(stop));
	HANDLE_ERROR(cudaEventDestroy(stop2));
#endif
}

void cudaDataterm::addDataTerm_allQueries_allFrames()
{
	cudaEvent_t start, stop, stop2;
	HANDLE_ERROR(cudaEventCreate(&start));
	HANDLE_ERROR(cudaEventCreate(&stop));
	HANDLE_ERROR(cudaEventCreate(&stop2));
	HANDLE_ERROR(cudaEventRecord(start, 0));
	for (int frame = 0; frame < gpu_frames.size(); frame++)
	{
		for (int hyp = 0; hyp < dterm->dterm.size(); hyp++)
		{
			gpu_frame_dataterm_allQueries frame_projection(
				gpu_frames.at(frame),
				(frame > 0 ? gpu_frames.at(frame - 1) : dummyFrame),
				(frame + 1 < gpu_frames.size() ? gpu_frames.at(frame + 1) : dummyFrame),
				cam,
				//gpuMat2Prev.dev_ptr, gpuMat2Next.dev_ptr, a_to_frame, 
				gpuMotion,
				clamp_dist_sqr,
				hyp,
				frame);

			thrust::for_each(
				thrust::make_zip_iterator(
				thrust::make_tuple(allQueries->query_x.begin(), allQueries->query_y.begin(), allQueries->query_z.begin(),
				allQueries->query_nx.begin(), allQueries->query_ny.begin(), allQueries->query_nz.begin(), dterm->dterm[hyp].begin(), dterm->weight[hyp].begin(), allQueries->query_frame.begin())),
				thrust::make_zip_iterator(
				thrust::make_tuple(allQueries->query_x.end(), allQueries->query_y.end(), allQueries->query_z.end(),
				allQueries->query_nx.end(), allQueries->query_ny.end(), allQueries->query_nz.end(), dterm->dterm[hyp].end(), dterm->weight[hyp].end(), allQueries->query_frame.end())),
				frame_projection);

		}
	}
	cudaEventRecord(stop2, 0);
	cudaEventSynchronize(stop2);

	float elapsedTime;
	HANDLE_ERROR(cudaEventElapsedTime(&elapsedTime, start, stop2));
	HANDLE_ERROR(cudaEventDestroy(start));
	HANDLE_ERROR(cudaEventDestroy(stop));
	HANDLE_ERROR(cudaEventDestroy(stop2));
	std::cout << "\n cudadataterm Took: " << elapsedTime;

}


void cudaDataterm::filter_dense2sparse_temporal_crf_oneHyp_no_Normalization(int hyp, float sigma_temporal)
{
	if (gpu_frames_dense_Q.size() < gpu_frames.size()){
		std::cout << "\n**** Q not initialized, using dummy Q values for meanfield step!!!! ****\n\n";
	}
	/*cudaEvent_t start, stop, stop2;
	HANDLE_ERROR(cudaEventCreate(&start));
	HANDLE_ERROR(cudaEventCreate(&stop));
	HANDLE_ERROR(cudaEventCreate(&stop2));
	HANDLE_ERROR(cudaEventRecord(start, 0));*/
	for (int frame = 0; frame < gpu_frames.size(); frame++)
	{

	//	std::cout <<" " << frame << " ";
		gpu_frame_dataterm_allQueries_crf frame_projection(
			gpu_frames.at(frame),
			(frame > 0 ? gpu_frames.at(frame - 1) : dummyFrame),
			(frame + 1 < gpu_frames.size() ? gpu_frames.at(frame + 1) : dummyFrame),
			(frame >= gpu_frames_dense_Q.size() ? dummy_Q_frame : gpu_frames_dense_Q.at(frame)),
			gpu_frames_noise.at(frame),
			gpu_frame_nx.at(frame), gpu_frame_ny.at(frame), gpu_frame_nz.at(frame),
			cam,
			//gpuMat2Prev.dev_ptr, gpuMat2Next.dev_ptr, a_to_frame, 
			gpuMotion,
			clamp_dist_sqr,
			sigma_temporal,
			hyp,
			frame);

		thrust::for_each(
			thrust::make_zip_iterator(
			thrust::make_tuple(allQueries->query_x.begin(), allQueries->query_y.begin(), allQueries->query_z.begin(),
			allQueries->query_nx.begin(), allQueries->query_ny.begin(), allQueries->query_nz.begin(), dterm->dterm[hyp].begin(), dterm->weight[hyp].begin(), allQueries->query_frame.begin())),
			thrust::make_zip_iterator(
			thrust::make_tuple(allQueries->query_x.end(), allQueries->query_y.end(), allQueries->query_z.end(),
			allQueries->query_nx.end(), allQueries->query_ny.end(), allQueries->query_nz.end(), dterm->dterm[hyp].end(), dterm->weight[hyp].end(), allQueries->query_frame.end())),
			frame_projection);

//gpuErrchk(cudaPeekAtLastError());
//gpuErrchk(cudaDeviceSynchronize());
	}
	/*cudaEventRecord(stop2, 0);
	cudaEventSynchronize(stop2);

	float elapsedTime;
	HANDLE_ERROR(cudaEventElapsedTime(&elapsedTime, start, stop2));
	HANDLE_ERROR(cudaEventDestroy(start));
	HANDLE_ERROR(cudaEventDestroy(stop));
	HANDLE_ERROR(cudaEventDestroy(stop2));

	std::cout << "\n cudadataterm Took: " << elapsedTime;*/
	//gpuErrchk(cudaPeekAtLastError());
	//gpuErrchk(cudaDeviceSynchronize());
	//gpuErrchk(cudaPeekAtLastError());

}


__global__ void crf_exp_normalization_sparse(float ** qPerHyp, float ** wPerHyp, int numHyp, int numVals, float lambda, float ** out)
{
	int idx = (blockIdx.x*blockDim.x) + threadIdx.x;
	if (idx < numVals)
	{
		float min = FLT_MAX, min2 = FLT_MAX, max = -FLT_MAX;
		float Z = 0;
		float tmp, exp_;
		float mean_w = 0;
		//float num = 0; //TODO: Unused. Remove?
		for (int h = 0; h < numHyp; h++){
			tmp = wPerHyp[h][idx];
			mean_w += tmp;
			//tmp = qPerHyp[h][idx] / (tmp > 1e-5 ? tmp : 1e-5);
		 	tmp = qPerHyp[h][idx] / (tmp > 1e-10 ? tmp : 1e-10);
			min2 = (tmp < min ? min : min2);
			min = (tmp < min ? tmp : min);
			max = (tmp > max ? tmp : max);
		}
//FUCKING FIDDLED: perpixel normalization.
//normalize by min
lambda /= (lambda/min * 0 == 0? min: 1e-10);
//variant 2: normalize by mean: NVM
//mean_w /= numHyp;
//lambda /= mean_w;
//variant 3: normalize by max - min. 
//lambda /= ((lambda / (max - min)) * 0 == 0 ? max - min : 1e-10);
//variant 4: normalize to have a minimal distance between best and second best.[best variant]
//lambda /= ((lambda / (min2 - min)) * 0 == 0 ? (min2 - min)*5 : 1e-10);
//HACK FUCKING FIDDLED1
/*mean_w /= numHyp*50;
lambda *= mean_w;
//HACK FUCKING FIDDLED2*/
/*mean_w = 0; num = 0;
for (int h = 0; h < numHyp; h++){
	tmp = wPerHyp[h][idx];
	//tmp = qPerHyp[h][idx] / (tmp > 1e-5 ? tmp : 1e-5);
	tmp = qPerHyp[h][idx] / (tmp > 1e-10 ? tmp : 1e-10);
	exp_ = exp(-lambda *(tmp - min));
	num += exp_;
	mean_w += exp_*wPerHyp[h][idx];
}
mean_w /= num * 50; lambda *= mean_w; */
//DEBUGGING<---
/*for (int h = 0; h < numHyp; h++){
	tmp = wPerHyp[h][idx];
	//tmp = qPerHyp[h][idx] / (tmp > 1e-5 ? tmp : 1e-5);
	tmp = qPerHyp[h][idx] / (tmp > 1e-10 ? tmp : 1e-10);
	exp_ = tmp / 0.0005;// / ((tmp - min) / (max - min) * 0 == 0 ? (tmp - min) / (max - min) : 1);
	out[h][idx] = exp_;
}
return;
//.---->*/


		for (int h = 0; h < numHyp; h++){
			tmp = wPerHyp[h][idx];
			//tmp = qPerHyp[h][idx] / (tmp > 1e-5 ? tmp : 1e-5);
			tmp = qPerHyp[h][idx] / (tmp > 1e-10 ? tmp : 1e-10);
			exp_ = exp(-lambda *(tmp - min));
			Z += exp_;
			out[h][idx] = exp_;
		}

		for (int h = 0; h < numHyp; h++){
			out[h][idx] = 1.f / Z * out[h][idx];
		}
	}
}

void cudaDataterm::crf_exp_normalization_for_sparse(float lambda){
	int THREADS_PER_BLOCK = 128;
	crf_exp_normalization_sparse << < (dterm->numPoints + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK >> >(
		thrust::raw_pointer_cast( dterm->dterms_ptr.data()),
		thrust::raw_pointer_cast(dterm->weights_ptr.data()),
		dterm->numHyps,
		dterm->numPoints,
		lambda,
		thrust::raw_pointer_cast(dterm->results_ptr.data())
		);
}

struct saxpy_functor { 
	const float a; 
	saxpy_functor(float _a) : a(_a) {} 
	__host__ __device__ float operator()(const float& x, const float& y) const { return a * x + y; } 
};

struct mx_axpy_functor {
	const float a;
	mx_axpy_functor(float _a) : a(_a) {}
	__host__ __device__ float operator()(const float& x, const float& y) const { return (a * x > y ? a*x : y); }
};

void cudaDataterm::crf_maxcompatibility_and_exp_normalization_for_sparse(std::vector<std::vector<float>> & compat, float lambda){

	int THREADS_PER_BLOCK = 128;
	
	//incorrect but lets try this one: (prepares probs for next step.)
	//1. do the normaliztaion
	crf_exp_normalization_sparse << < (dterm->numPoints + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK >> >(
		thrust::raw_pointer_cast(dterm->dterms_ptr.data()),
		thrust::raw_pointer_cast(dterm->weights_ptr.data()),
		dterm->numHyps,
		dterm->numPoints,
		lambda,
		thrust::raw_pointer_cast(dterm->results_ptr.data())
		);

	//2.do the compatibility transform and store the result in dterm
	//that is: out[i] = sum_j compat(i,j)*in[i]
	//or for(i=1....n)for(j = 1...n) out[i] =0; out[i]+= compat[i][j] in[j];
	for (int i = 0; i < compat.size(); i++)
	{
		thrust::fill(dterm->dterm[i].begin(), dterm->dterm[i].end(), 0);
		for (int j = 0; j < compat[i].size(); j++){
			//saxpy y a*x + y
			thrust::transform(dterm->crf_final_result[i].begin(), dterm->crf_final_result[i].end(),
				dterm->dterm[i].begin(), dterm->dterm[i].begin(), mx_axpy_functor(compat[i][j]));
			//dterm->dterm[i].begin(), dterm->dterm[i].begin(), saxpy_functor(compat[i][j] / compat[i].size()));
		}
	}

	//3. Copy the results back from the dterm to the crf results.
	for (int i = 0; i < compat.size(); i++)
	{
		thrust::copy(dterm->dterm[i].begin(), dterm->dterm[i].end(), dterm->crf_final_result[i].begin());
	}


	//gpuErrchk(cudaPeekAtLastError());
	//gpuErrchk(cudaDeviceSynchronize());

}

//void cudaDataterm::crf_compatibility_for_sparse(std::vector<std::vector<float>> & compat, float lambda){
void cudaDataterm::crf_compatibility_and_exp_normalization_for_sparse(std::vector<std::vector<float>> & compat, float lambda){

	int THREADS_PER_BLOCK = 128;
	//filtering the crf_result via compatibility, to be used as inputs.


	/*
	//results can be used as a buffer. So 1. copy dterm to results
	for (int i = 0; i < compat.size(); i++)
	{
		thrust::copy(dterm->dterm[i].begin(), dterm->dterm[i].end(), dterm->crf_final_result[i].begin());
	}

	//2.do the compatibility transform and store the result in dterm
	//that is: out[i] = sum_j compat(i,j)*in[i]
	//or for(i=1....n)for(j = 1...n) out[i] =0; out[i]+= compat[i][j] in[j];
	for (int i = 0; i < compat.size(); i++)
	{
		thrust::fill(dterm->dterm[i].begin(), dterm->dterm[i].end(), 0);
		for (int j = 0; j < compat[i].size(); j++){
			//saxpy y a*x + y
			thrust::transform(dterm->crf_final_result[i].begin(), dterm->crf_final_result[i].end(),
				dterm->dterm[i].begin(), dterm->dterm[i].begin(), saxpy_functor(compat[i][j]));
		}
	}

	//3. do the normaliztaion
	crf_exp_normalization_sparse << < (dterm->numPoints + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK >> >(
		thrust::raw_pointer_cast(dterm->dterms_ptr.data()),
		thrust::raw_pointer_cast(dterm->weights_ptr.data()),
		dterm->numHyps,
		dterm->numPoints,
		lambda,
		thrust::raw_pointer_cast(dterm->results_ptr.data())
		);*/
	

	//incorrect but lets try this one: (prepares probs for next step.)
	//1. do the normaliztaion
	crf_exp_normalization_sparse << < (dterm->numPoints + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK >> >(
		thrust::raw_pointer_cast(dterm->dterms_ptr.data()),
		thrust::raw_pointer_cast(dterm->weights_ptr.data()),
		dterm->numHyps,
		dterm->numPoints,
		lambda,
		thrust::raw_pointer_cast(dterm->results_ptr.data())
		);

	//2.do the compatibility transform and store the result in dterm
	//that is: out[i] = sum_j compat(i,j)*in[i]
	//or for(i=1....n)for(j = 1...n) out[i] =0; out[i]+= compat[i][j] in[j];
	for (int i = 0; i < compat.size(); i++)
	{
		thrust::fill(dterm->dterm[i].begin(), dterm->dterm[i].end(), 0);
		for (int j = 0; j < compat[i].size(); j++){
			//saxpy y a*x + y
			thrust::transform(dterm->crf_final_result[i].begin(), dterm->crf_final_result[i].end(),
				dterm->dterm[i].begin(), dterm->dterm[i].begin(), saxpy_functor(compat[i][j]));
				//dterm->dterm[i].begin(), dterm->dterm[i].begin(), saxpy_functor(compat[i][j] / compat[i].size()));
		}
	}

	//3. Copy the results back from the dterm to the crf results.
	for (int i = 0; i < compat.size(); i++)
	{
		thrust::copy(dterm->dterm[i].begin(), dterm->dterm[i].end(), dterm->crf_final_result[i].begin());
	}

	
	//gpuErrchk(cudaPeekAtLastError());
	//gpuErrchk(cudaDeviceSynchronize());
	
}

//variants for spatial filtering...

__global__ void crfFilter_bruteforce(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, gpu_frame * out, gpu_cam * cam, int kernel_width_2, float sigma)
{
	float sigma_sqr = sigma * sigma;

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;

	float pz = tex2D<float>(depth, i, j);
	float px = cam->unproj_i2x(i, pz);
	float py = cam->unproj_j2y(j, pz);
	float pz_o, px_o, py_o;

	float total = 0;
	float weight, sparse_val;
	float total_weight = 0;


	for (int k = -kernel_width_2; k <= kernel_width_2; k++){
		for (int l = -kernel_width_2; l <= kernel_width_2; l++){
			pz_o = tex2D<float>(depth, i + k, j+l);
			px_o = cam->unproj_i2x(i+k, pz_o);
			py_o = cam->unproj_j2y(j+l, pz_o);

			sparse_val = tex2D<float>(sparse_vals, i + k, j+l);

			weight = (px - px_o)*(px - px_o) + (py - py_o)*(py - py_o) + (pz - pz_o)*(pz - pz_o);
			weight = exp(-weight / sigma_sqr);
			weight *= (sparse_val > 0 && pz_o > 1e-3 ? 1 : 0);
			total += (1 - sparse_val) * weight;//Here
			total_weight += weight;
		}
	}

	out->set(i, j, (total_weight > 0 ? total : 0));
	//out->set(i,j, (total_weight > 0 ? total / total_weight : 0));
}

//for a geodesic filter. Assumes missed values where filtered in right to left/left to right/top to bottom or whatever appropriate.
__global__ void domainTransform_findDI(int di, int dj, cudaTextureObject_t depth, gpu_cam * cam, gpu_frame * dI){
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;

	float pz = tex2D<float>(depth, i, j);
	float px = cam->unproj_i2x(i, pz);
	float py = cam->unproj_j2y(j, pz);

	float qz = tex2D<float>(depth, i+di, j+dj);
	float qx = cam->unproj_i2x(i+di, pz);
	float qy = cam->unproj_j2y(j+dj, pz);
	//could devide dist by minimum of both zs to take into account non-uniform sampling and noise.
	//float dist = 1 + (pz > 0.0001 && qz > 0.0001 ? sqrtf((px - qx)*(px - qx) + (py - qy)*(py - qy) + (pz - qz)*(pz - qz)): 0);
	//very bad if every second value is missing! so assumed missing (zero) values are filtered in. Treat zero depth agressively as valid
	//observations => lead to high distance.
	//float dist = (pz > 0.0001 && qz > 0.0001 ? sqrtf((px - qx)*(px - qx) + (py - qy)*(py - qy) + (pz - qz)*(pz - qz)) : 0);
	float dist = ( sqrtf((px - qx)*(px - qx) + (py - qy)*(py - qy) + (pz - qz)*(pz - qz)));
	dI->set(i, j, dist);
	//dI->set(i, j, j);
	//dI->set(i, j, 1);


	
}

//could be done in blockwise manner. a laGPU efficient filters...
__global__ void domainTransform_summedAreaTable_vert(cudaTextureObject_t dI, int h, gpu_frame * table){
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	float sum =0;
	for (int j = 0; j < h; j++){
		sum += tex2D<float>(dI, i, j);
		table->set(i, j, sum);
	}
}

__global__ void fillInMissing_horiz(cudaTextureObject_t depth, int w, float * filledIn){
	/*int j = (blockIdx.x*blockDim.x) + threadIdx.x;
	float last = 0, current;
	for (int i = 0; i < w; i++){
		current = tex2D<float>(depth, i, j);
		current = (current >= 1e-3 ? current : last);
		last = current;
		filledIn->set(i, j, current);
	}
	filledIn->set(i, j, tex2D<float>(depth, i, j));*/

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	//filledIn->set(i, j, tex2D<float>(depth, i, j));
	filledIn[400 * i + j] = tex2D<float>(depth, i, j);
}

__global__ void fillInMissing_vert(cudaTextureObject_t depth, int h, gpu_frame * filledIn){
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	float last = 0, current;
	for (int j = 0; j < h; j++){
		current = tex2D<float>(depth, i, j);
		current = (current >= 1e-3 ? current : last);
		last = current;
		filledIn->set(i, j, current);
	}
}

//could be done in blockwise manner. a laGPU efficient filters...
__global__ void domainTransform_summedAreaTable_horiz(cudaTextureObject_t dI, int w, gpu_frame * table){
	int j = (blockIdx.x*blockDim.x) + threadIdx.x;
	float sum = 0;
	for (int i = 0; i < w; i++){
		sum += tex2D<float>(dI, i, j);
		table->set(i, j, sum);
	}
}

//could also be done in a blockwise manner; can use lower bound for lower ( see paper...)
__global__ void domainTransform_domainBoxFilter_vert(cudaTextureObject_t dist_SAtable, cudaTextureObject_t val_SATable, int h, float box_range, gpu_frame * filtered){
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = 0;

	int lower = 0;
	int upper = 0;
	float val_upper, val_lower;
	float current = val_lower = val_upper = tex2D<float>(dist_SAtable, i, j);


	for (int j = 0; j < h; j++)
	{
		current = tex2D<float>(dist_SAtable, i, j);
		//current = j;
		while (val_lower < current - box_range && lower < h){
			lower++;
			val_lower = tex2D<float>(dist_SAtable, i, lower);
		}
		while (val_upper < current + box_range && upper < h){
			upper++;
			val_upper = tex2D<float>(dist_SAtable, i, upper);
		}

		//lower = j - 5; upper = (j + 5 < h ? j + 5 : h);
		filtered->set(i, j,
			(tex2D<float>(val_SATable, i, upper - 1) - tex2D<float>(val_SATable, i, lower - 1)) / (box_range)
			);
	}
}

__global__ void domainTransform_domainBoxFilter_horiz(cudaTextureObject_t dist_SAtable, cudaTextureObject_t val_SATable, int w, float box_range, gpu_frame * filtered)
{
	int j = (blockIdx.x*blockDim.x) + threadIdx.x;

	int lower = 0;
	int upper = 0;
	float val_upper, val_lower;
	float current = val_lower = val_upper = tex2D<float>(dist_SAtable, 0, j);

	for (int i = 0; i < w; i++)
	{
		current = tex2D<float>(dist_SAtable, i, j);
		while (val_lower < current - box_range && lower < w){ //<w not needed, but good while debugging.
			lower++;
			val_lower = tex2D<float>(dist_SAtable, lower, j);
		}
		while (val_upper < current + box_range && upper < w){
			upper++;
			val_upper = tex2D<float>(dist_SAtable, upper, j);
		}
		//lower = i - 15; upper = (i + 15 < w ? i + 15: w);
		filtered->set(i, j,
			(tex2D<float>(val_SATable, upper - 1,j) - tex2D<float>(val_SATable, lower - 1,j)) / (box_range)
			);
	}
}

__global__ void nnPass(gpu_frame * sparse_vals, gpu_frame * depth, gpu_frame * out, gpu_cam * cam)
{
	float val;

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;

	bool found = depth->findNNValidData(i, j, sparse_vals, cam, val);

	out->set(i, j, (found ? val : CUDART_NAN_F));//0
	//out->set(i,j, (total_weight > 0 ? total / total_weight : 0));
}

__global__ void nnPass(gpu_frame * sparse_vals, gpu_frame * depth, gpu_frame * out, gpu_cam * cam, float fillIn)
{
	float val;

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;

	bool found = depth->findNNValidData(i, j, sparse_vals, cam, val);

	out->set(i, j, (found ? val : fillIn));//0
	//out->set(i,j, (total_weight > 0 ? total / total_weight : 0));
}

__global__ void crfFilter_vertical_pass1(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, gpu_frame * out, gpu_cam * cam, int kernel_width_2, float sigma)
{
	float sigma_sqr = sigma * sigma;

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	
	float pz = tex2D<float>(depth, i, j);
	float px = cam->unproj_i2x(i, pz);
	float py = cam->unproj_j2y(j, pz);
	float pz_o,px_o, py_o;
	
	float total = 0;
	float weight, sparse_val;
	float total_weight = 0;

	for (int k = -kernel_width_2; k <= kernel_width_2; k++){
		pz_o = tex2D<float>(depth, i + k, j);
		px_o = cam->unproj_i2x(i+k, pz_o);
		py_o = cam->unproj_j2y(j, pz_o);

		sparse_val = tex2D<float>(sparse_vals, i + k, j); //out of bound accesses zero.
		
		weight = (px - px_o)*(px - px_o) + (py - py_o)*(py - py_o) + (pz - pz_o)*(pz - pz_o); //exponential dropoff
		weight = exp(-weight / sigma_sqr);
		//weight *= (sparse_val > 0 && pz_o > 1e-3? 1 : 0);
		weight *= (sparse_val *0 ==0 && pz_o > 1e-3 ? 1 : 0);
		
		total += (sparse_val*0 == 0? sparse_val: 0) * weight;
		total_weight += weight;
	}

	out->set(i, j, (total_weight > 0 ? total : CUDART_NAN_F));//0
	//out->set(i,j, (total_weight > 0 ? total / total_weight : 0));
}

__global__ void crfFilter_horizontal_pass2(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, gpu_frame * out, gpu_cam * cam, int kernel_width_2, float sigma, float proxyValueToFillInForNANs)
{
	float sigma_sqr = sigma * sigma;

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;

	float pz = tex2D<float>(depth, i, j);
	float px = cam->unproj_i2x(i, pz);
	float py = cam->unproj_j2y(j, pz);
	float pz_o,px_o, py_o;
	
	float total = 0;
	float weight, sparse_val;
	float total_weight = 0;

	for (int k = -kernel_width_2; k <= kernel_width_2; k++){
		pz_o = tex2D<float>(depth, i , j+k);
		px_o = cam->unproj_i2x(i, pz_o);
		py_o = cam->unproj_j2y(j+k, pz_o);

		sparse_val = tex2D<float>(sparse_vals, i , j+k);
		
		weight = (px - px_o)*(px - px_o) + (py - py_o)*(py - py_o) + (pz - pz_o)*(pz - pz_o);
		weight = exp(-weight / sigma_sqr); //*lambda
		weight *= (sparse_val*0 == 0 && pz_o > 1e-3? 1 : 0);
		total += (sparse_val*0 == 0 ? sparse_val: 0) * weight; //was sparse_val*weight.
		total_weight += weight;
	}

	out->set(i, j, (total_weight > 0 ? total : proxyValueToFillInForNANs));
	//out->set(i,j, (total_weight > 0 ? total / total_weight : 0));
}

__global__ void crfFilterGeodesic_vertical_pass1(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, cudaTextureObject_t stddev, gpu_frame * out, gpu_cam * cam, int kernel_width_2)//, float sigma)
{
	float sigma;

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;

	float pz = tex2D<float>(depth, i, j);
	float px = cam->unproj_i2x(i, pz);
	float py = cam->unproj_j2y(j, pz);
	float pz_o, px_o, py_o;
	float pz_last, px_last, py_last;

	float total = 0;
	float weight, tmp_weight,sparse_val;
	float total_weight = 0;

	float factor = 1;

	pz_last = pz; px_last = px; py_last = py;
	weight = 1;
	for (int k = 0; k <= kernel_width_2; k++){
		pz_o = tex2D<float>(depth, i + k, j);
		px_o = cam->unproj_i2x(i + k, pz_o);
		py_o = cam->unproj_j2y(j, pz_o);
		sigma = tex2D<float>(stddev, i + k, j)*factor;

		sparse_val = tex2D<float>(sparse_vals, i + k, j); //out of bound accesses zero.

		tmp_weight = (px_last - px_o)*(px_last - px_o) + (py_last - py_o)*(py_last - py_o) + (pz_last - pz_o)*(pz_last - pz_o); //exponential dropoff
		tmp_weight = sqrtf(tmp_weight);
		tmp_weight = exp(-tmp_weight *1.4142f /sigma);
		weight *= (pz_o > 1e-3 ? tmp_weight:1);
		//weight *= (sparse_val > 0 && pz_o > 1e-3? 1 : 0);
		tmp_weight =weight* (sparse_val * 0 == 0 && pz_o > 1e-3 ? 1 : 0);

		total += (sparse_val * 0 == 0 ? sparse_val : 0) * tmp_weight;
		total_weight += tmp_weight;

		pz_last = (pz_o > 1e-3 ? pz_o : pz_last);
		px_last = (pz_o > 1e-3 ? px_o : px_last);
		py_last = (pz_o > 1e-3 ? py_o : py_last);
	}
	pz_last = pz; px_last = px; py_last = py; weight = 1;
	for (int k = 0; k >= kernel_width_2; k--){
		pz_o = tex2D<float>(depth, i + k, j);
		px_o = cam->unproj_i2x(i + k, pz_o);
		py_o = cam->unproj_j2y(j, pz_o);
		sigma = tex2D<float>(stddev, i + k, j)*factor;

		sparse_val = tex2D<float>(sparse_vals, i + k, j); //out of bound accesses zero.

		tmp_weight = (px_last - px_o)*(px_last - px_o) + (py_last - py_o)*(py_last - py_o) + (pz_last - pz_o)*(pz_last - pz_o); //exponential dropoff
		tmp_weight = sqrtf(tmp_weight);
		tmp_weight = exp(-tmp_weight *1.4142f / sigma);
		weight *= (pz_o > 1e-3 ? tmp_weight : 1);
		//weight *= (sparse_val > 0 && pz_o > 1e-3? 1 : 0);
		tmp_weight = weight* (sparse_val * 0 == 0 && pz_o > 1e-3 ? 1 : 0);

		total += (sparse_val * 0 == 0 ? sparse_val : 0) * tmp_weight;
		total_weight += tmp_weight;
		pz_last = (pz_o > 1e-3 ? pz_o : pz_last);
		px_last = (pz_o > 1e-3 ? px_o : px_last);
		py_last = (pz_o > 1e-3 ? py_o : py_last);
	}

	out->set(i, j, (total_weight > 0 ? total : CUDART_NAN_F));//0
	//out->set(i,j, (total_weight > 0 ? total / total_weight : 0));
}

__global__ void crfFilterGeodesic_horizontal_pass2(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, cudaTextureObject_t stddev, gpu_frame * out, gpu_cam * cam, int kernel_width_2,
	/*float sigma,*/ float proxyValueToFillInForNANs)
{
	float sigma;

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;

	float pz = tex2D<float>(depth, i, j);
	float px = cam->unproj_i2x(i, pz);
	float py = cam->unproj_j2y(j, pz);
	float pz_o, px_o, py_o;
	float pz_last, px_last, py_last;

	float total = 0;
	float weight, tmp_weight, sparse_val;
	float total_weight = 0;
	float factor = 1;

	pz_last = pz; px_last = px; py_last = py;
	weight = 1;
	for (int k = 0; k <= kernel_width_2; k++){
		pz_o = tex2D<float>(depth, i, j + k);
		px_o = cam->unproj_i2x(i, pz_o);
		py_o = cam->unproj_j2y(j + k, pz_o);
		sigma = tex2D<float>(stddev, i, j + k)*factor;
		sparse_val = tex2D<float>(sparse_vals, i, j + k); //out of bound accesses zero.

		tmp_weight = (px_last - px_o)*(px_last - px_o) + (py_last - py_o)*(py_last - py_o) + (pz_last - pz_o)*(pz_last - pz_o); //exponential dropoff
		tmp_weight = sqrtf(tmp_weight);
		tmp_weight = exp(-tmp_weight *1.4142f / sigma);
		weight *= (pz_o > 1e-3 ? tmp_weight : 1);
		//weight *= (sparse_val > 0 && pz_o > 1e-3? 1 : 0);
		tmp_weight = weight* (sparse_val * 0 == 0 && pz_o > 1e-3 ? 1 : 0);

		total += (sparse_val * 0 == 0 ? sparse_val : 0) * tmp_weight;
		total_weight += tmp_weight;

		pz_last = (pz_o > 1e-3 ? pz_o : pz_last);
		px_last = (pz_o > 1e-3 ? px_o : px_last);
		py_last = (pz_o > 1e-3 ? py_o : py_last);
	}
	pz_last = pz; px_last = px; py_last = py; weight = 1;
	for (int k = 0; k >= kernel_width_2; k--){
		pz_o = tex2D<float>(depth, i, j + k);
		px_o = cam->unproj_i2x(i, pz_o);
		py_o = cam->unproj_j2y(j + k, pz_o);
		sigma = tex2D<float>(stddev, i, j + k)*factor;
		sparse_val = tex2D<float>(sparse_vals, i, j + k); //out of bound accesses zero.

		tmp_weight = (px_last - px_o)*(px_last - px_o) + (py_last - py_o)*(py_last - py_o) + (pz_last - pz_o)*(pz_last - pz_o); //exponential dropoff
		tmp_weight = sqrtf(tmp_weight);
		tmp_weight = exp(-tmp_weight *1.4142f / sigma);
		weight *= (pz_o > 1e-3 ? tmp_weight : 1);
		//weight *= (sparse_val > 0 && pz_o > 1e-3? 1 : 0);
		tmp_weight = weight* (sparse_val * 0 == 0 && pz_o > 1e-3 ? 1 : 0);

		total += (sparse_val * 0 == 0 ? sparse_val : 0) * tmp_weight;
		total_weight += tmp_weight;
		pz_last = (pz_o > 1e-3 ? pz_o : pz_last);
		px_last = (pz_o > 1e-3 ? px_o : px_last);
		py_last = (pz_o > 1e-3 ? py_o : py_last);
	}

	out->set(i, j, (total_weight > 0 ? total : proxyValueToFillInForNANs));
	//out->set(i,j, (total_weight > 0 ? total / total_weight : 0));
}

#ifdef INTERPOLATE_USING_SIGMA
__global__ void interpolate_vertical_pass1(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, cudaTextureObject_t stdev, gpu_frame * out, gpu_frame * weight_sum_out, gpu_cam * cam, int kernel_width_2, float factor)
{
	
#else
__global__ void interpolate_vertical_pass1(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, gpu_frame * out, gpu_frame * weight_sum_out, gpu_cam * cam, int kernel_width_2, float sigma)
{
#endif
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
#ifdef INTERPOLATE_USING_SIGMA
	float sigma = tex2D<float>(stdev, i, j)*factor;
#endif
	float sigma_sqr = sigma * sigma;
	
	float pz = tex2D<float>(depth, i, j);
	float px = cam->unproj_i2x(i, pz);
	float py = cam->unproj_j2y(j, pz);
	float pz_o, px_o, py_o;

	float total = 0;
	float weight, sparse_val;
	float total_weight = 0;

	for (int k = -kernel_width_2; k <= kernel_width_2; k++){
		pz_o = tex2D<float>(depth, i + k, j);
		px_o = cam->unproj_i2x(i + k, pz_o);
		py_o = cam->unproj_j2y(j, pz_o);

		sparse_val = tex2D<float>(sparse_vals, i + k, j); //out of bound accesses zero.

		weight = (px - px_o)*(px - px_o) + (py - py_o)*(py - py_o) + (pz - pz_o)*(pz - pz_o); //exponential dropoff
		weight = exp(-weight / sigma_sqr);
		//weight *= (sparse_val > 0 && pz_o > 1e-3 ? 1 : 0); //bad hack: using zero as "no data"
		weight *= (sparse_val * 0 == 0 && pz_o > 1e-3 ? 1 : 0); //using NANS as "no data"
		weight = (weight * 0 == 0 ? weight : 0); //vs nan sigmas.

		total += (sparse_val* 0 == 0 ? sparse_val : 0)* weight; //guard against NANs
		total_weight += weight;
	}


	out->set(i, j, (total_weight > 0 ? total : CUDART_NAN_F));
	weight_sum_out->set(i, j, total_weight);

//for debug: value pass through.
//out->set(i, j, tex2D<float>(sparse_vals, i, j));
}

//allows the input sparse values to already be weighted-
#ifdef INTERPOLATE_USING_SIGMA
__global__ void interpolate_vertical_pass1(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, cudaTextureObject_t weights, cudaTextureObject_t stddev, gpu_frame * out, gpu_frame * weight_sum_out, gpu_cam * cam, int kernel_width_2, float factor)
#else
__global__ void interpolate_vertical_pass1(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, cudaTextureObject_t weights, gpu_frame * out, gpu_frame * weight_sum_out, gpu_cam * cam, int kernel_width_2, float sigma)
#endif
{
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
#ifdef INTERPOLATE_USING_SIGMA
	float sigma = tex2D<float>(stddev, i, j)*factor;
#endif
	float sigma_sqr = sigma * sigma;


	float pz = tex2D<float>(depth, i, j);
	float px = cam->unproj_i2x(i, pz);
	float py = cam->unproj_j2y(j, pz);
	float pz_o, px_o, py_o;

	float total = 0;
	float weight, sparse_val;
	float total_weight = 0;

	for (int k = -kernel_width_2; k <= kernel_width_2; k++){
		pz_o = tex2D<float>(depth, i + k, j);
		px_o = cam->unproj_i2x(i + k, pz_o);
		py_o = cam->unproj_j2y(j, pz_o);

		sparse_val = tex2D<float>(sparse_vals, i + k, j); //out of bound accesses zero.

		weight = (px - px_o)*(px - px_o) + (py - py_o)*(py - py_o) + (pz - pz_o)*(pz - pz_o); //exponential dropoff
		weight = exp(-weight / sigma_sqr) * tex2D<float>(weights, i + k, j);
		//weight *= (sparse_val > 0 && pz_o > 1e-3 ? 1 : 0); //bad hack: used zero as "no data"
		weight *= (sparse_val * 0 == 0 && pz_o > 1e-3 ? 1 : 0); //using NANS as "no data"
		weight = (weight * 0 == 0 ? weight : 0); //vs nan sigmas.

		total += (sparse_val * 0 == 0 ? sparse_val : 0)* weight; //guard against NANs
		total_weight += weight;
	}

	out->set(i, j, (total_weight > 0 ? total : CUDART_NAN_F));
	weight_sum_out->set(i, j, total_weight);

	//for debug: value pass trhough.
//out->set(i, j, tex2D<float>(sparse_vals, i, j));
}

#ifdef INTERPOLATE_USING_SIGMA
__global__ void interpolate_horizontal_pass2(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, cudaTextureObject_t weights, cudaTextureObject_t stdev, gpu_frame * out, gpu_cam * cam, int kernel_width_2, float factor, float fillInProxyValueForMissing)
#else
__global__ void interpolate_horizontal_pass2(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, cudaTextureObject_t weights, gpu_frame * out, gpu_cam * cam, int kernel_width_2, float sigma, float fillInProxyValueForMissing)
#endif
{
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
#ifdef INTERPOLATE_USING_SIGMA
	float sigma = tex2D<float>(stdev, i, j)*factor;//tex2D<float>(stdev, i, j)*factor;
#endif

	float sigma_sqr = sigma * sigma;

	float pz = tex2D<float>(depth, i, j);
	float px = cam->unproj_i2x(i, pz);
	float py = cam->unproj_j2y(j, pz);
	float pz_o, px_o, py_o;

	float total = 0;
	float weight, sparse_val;
	float total_weight = 0;

	for (int k = -kernel_width_2; k <= kernel_width_2; k++){
		pz_o = tex2D<float>(depth, i, j + k);
		px_o = cam->unproj_i2x(i, pz_o);
		py_o = cam->unproj_j2y(j + k, pz_o);

		sparse_val = tex2D<float>(sparse_vals, i, j + k);

		weight = (px - px_o)*(px - px_o) + (py - py_o)*(py - py_o) + (pz - pz_o)*(pz - pz_o);
		weight = exp(-weight / sigma_sqr); //*lambda
		//weight *= (sparse_val > 0 && pz_o > 1e-3 ? 1 : 0);
		weight *= (sparse_val * 0 == 0 && pz_o > 1e-3 ? 1 : 0); //using NANS as "no data"
		weight = (weight * 0 == 0 ? weight : 0); //vs nan sigmas.

		total += (sparse_val * 0 == 0 ? sparse_val : 0)* weight; //guard against NANs
		total_weight += weight * tex2D<float>(weights, i, j + k);;
	}


	out->set(i, j, (total_weight > 0 && total * 0 == 0 ? total / total_weight : fillInProxyValueForMissing));

//for debug: value pass trhough
//out->set(i, j, tex2D<float>(sparse_vals, i, j));
}

__global__ void smoothen_vertical_pass1_adaptiveSig(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, cudaTextureObject_t stdev, gpu_frame * out, gpu_frame * weight_sum_out, gpu_cam * cam, int kernel_width_2, float factor)
{
	

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	float sigma = tex2D<float>(stdev, i, j)*factor;
	float sigma_sqr = sigma * sigma;

	float pz = tex2D<float>(depth, i, j);
	float px = cam->unproj_i2x(i, pz);
	float py = cam->unproj_j2y(j, pz);
	float pz_o, px_o, py_o;

	float total = 0;
	float weight, sparse_val;
	float total_weight = 0;

	for (int k = -kernel_width_2; k <= kernel_width_2; k++){
		pz_o = tex2D<float>(depth, i + k, j);
		px_o = cam->unproj_i2x(i + k, pz_o);
		py_o = cam->unproj_j2y(j, pz_o);

		sparse_val = tex2D<float>(sparse_vals, i + k, j); //out of bound accesses zero.

		weight = (px - px_o)*(px - px_o) + (py - py_o)*(py - py_o) + (pz - pz_o)*(pz - pz_o); //exponential dropoff
		weight = exp(-weight / sigma_sqr);
		weight = (weight * 0 == 0 ? weight : 0); //vs nan sigmas.
		//weight *= (sparse_val > 0 && pz_o > 1e-3 ? 1 : 0); //bad hack: using zero as "no data"
		//weight *= (sparse_val * 0 == 0 && pz_o > 1e-3 ? 1 : 0); //using NANS as "no data"

		total += (sparse_val * 0 == 0 ? sparse_val : 0)* weight; //guard against NANs
		total_weight += weight;
	}


	out->set(i, j, total);//(total_weight > 0 ? total : CUDART_NAN_F));
	weight_sum_out->set(i, j, total_weight);

}

__global__ void smoothen_horizontal_pass2_adaptiveSig(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, cudaTextureObject_t stdev, cudaTextureObject_t weights, gpu_frame * out, gpu_cam * cam, int kernel_width_2, float factor, float fillInProxyValueForMissing)
{
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	float sigma = tex2D<float>(stdev, i, j)*factor;
	float sigma_sqr = sigma * sigma;

	float pz = tex2D<float>(depth, i, j);
	float px = cam->unproj_i2x(i, pz);
	float py = cam->unproj_j2y(j, pz);
	float pz_o, px_o, py_o;

	float total = 0;
	float weight, sparse_val;
	float total_weight = 0;

	for (int k = -kernel_width_2; k <= kernel_width_2; k++){
		pz_o = tex2D<float>(depth, i, j + k);
		px_o = cam->unproj_i2x(i, pz_o);
		py_o = cam->unproj_j2y(j + k, pz_o);

		sparse_val = tex2D<float>(sparse_vals, i, j + k);

		weight = (px - px_o)*(px - px_o) + (py - py_o)*(py - py_o) + (pz - pz_o)*(pz - pz_o);
		weight = exp(-weight / sigma_sqr); //*lambda
		weight = (weight * 0 == 0 ? weight : 0); //vs nan sigmas.
		//weight *= (sparse_val > 0 && pz_o > 1e-3 ? 1 : 0);
		//weight *= (sparse_val * 0 == 0 && pz_o > 1e-3 ? 1 : 0); //using NANS as "no data"

		total += (sparse_val * 0 == 0 ? sparse_val : 0)* weight; //guard against NANs
		total_weight += weight * tex2D<float>(weights, i, j + k);;
	}


	out->set(i, j, (total_weight > 0 && total * 0 == 0 ? total / total_weight : fillInProxyValueForMissing));
}



__global__ void smoothen_vertical_pass1(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, gpu_frame * out, gpu_frame * weight_sum_out, gpu_cam * cam, int kernel_width_2, float sigma)
{
	float sigma_sqr = sigma * sigma;

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;

	float pz = tex2D<float>(depth, i, j);
	float px = cam->unproj_i2x(i, pz);
	float py = cam->unproj_j2y(j, pz);
	float pz_o, px_o, py_o;

	float total = 0;
	float weight, sparse_val;
	float total_weight = 0;

	for (int k = -kernel_width_2; k <= kernel_width_2; k++){
		pz_o = tex2D<float>(depth, i + k, j);
		px_o = cam->unproj_i2x(i + k, pz_o);
		py_o = cam->unproj_j2y(j, pz_o);

		sparse_val = tex2D<float>(sparse_vals, i + k, j); //out of bound accesses zero.

		weight = (px - px_o)*(px - px_o) + (py - py_o)*(py - py_o) + (pz - pz_o)*(pz - pz_o); //exponential dropoff
		weight = exp(-weight / sigma_sqr);
		//weight *= (sparse_val > 0 && pz_o > 1e-3 ? 1 : 0); //bad hack: using zero as "no data"
		//weight *= (sparse_val * 0 == 0 && pz_o > 1e-3 ? 1 : 0); //using NANS as "no data"

		total += (sparse_val * 0 == 0 ? sparse_val : 0)* weight; //guard against NANs
		total_weight += weight;
	}


	out->set(i, j, total);//(total_weight > 0 ? total : CUDART_NAN_F));
	weight_sum_out->set(i, j, total_weight);

}

__global__ void smoothen_horizontal_pass2(cudaTextureObject_t sparse_vals, cudaTextureObject_t depth, cudaTextureObject_t weights, gpu_frame * out, gpu_cam * cam, int kernel_width_2, float sigma, float fillInProxyValueForMissing)
{
	float sigma_sqr = sigma * sigma;

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;

	float pz = tex2D<float>(depth, i, j);
	float px = cam->unproj_i2x(i, pz);
	float py = cam->unproj_j2y(j, pz);
	float pz_o, px_o, py_o;

	float total = 0;
	float weight, sparse_val;
	float total_weight = 0;

	for (int k = -kernel_width_2; k <= kernel_width_2; k++){
		pz_o = tex2D<float>(depth, i, j + k);
		px_o = cam->unproj_i2x(i, pz_o);
		py_o = cam->unproj_j2y(j + k, pz_o);

		sparse_val = tex2D<float>(sparse_vals, i, j + k);

		weight = (px - px_o)*(px - px_o) + (py - py_o)*(py - py_o) + (pz - pz_o)*(pz - pz_o);
		weight = exp(-weight / sigma_sqr); //*lambda
		//weight *= (sparse_val > 0 && pz_o > 1e-3 ? 1 : 0);
		//weight *= (sparse_val * 0 == 0 && pz_o > 1e-3 ? 1 : 0); //using NANS as "no data"

		total += (sparse_val * 0 == 0 ? sparse_val : 0)* weight; //guard against NANs
		total_weight += weight * tex2D<float>(weights, i, j + k);;
	}


	out->set(i, j, (total_weight > 0 && total * 0 == 0 ? total / total_weight : fillInProxyValueForMissing));
}
//can do this as a lambda. Look only at subset which comes from frame! 
//The values are already sorted by frames, just assume sparse_vals etc are pointers to the first element
//numVals is the number of vals
__global__ void fillIn_1mvals(float * sparse_vals, float * qx, float* qy, float* qz, int numVals, gpu_frame * frame, gpu_frame * out, gpu_cam * cam)
{
	
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int offset =  i;
	if (offset < numVals)
	{
		int2 nn;
		float3 q = float3{ qx[offset], qy[offset], qz[offset] };
		float dist = 1e-3, bestdist;
		frame->nn(q, cam, nn, bestdist, dist);
		//directly fill in 1-weight, no need to do it later
		//out->set(nn.x, nn.y, sparse_vals[offset]);
		out->set(nn.x, nn.y, 1 - sparse_vals[offset]);
	}
}


__global__ void fillIn_vals(float * sparse_vals, float * qx, float* qy, float* qz, int numVals, gpu_frame * frame, gpu_frame * out, gpu_cam * cam)
{

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int offset = i;
	if (offset < numVals)
	{
		int2 nn;
		float3 q = float3{ qx[offset], qy[offset], qz[offset] };
		float dist = 1e-3, bestdist;
		bool found = frame->nn(q, cam, nn, bestdist, dist);
		//directly fill in 1-weight, no need to do it later
		//out->set(nn.x, nn.y, sparse_vals[offset]);
		if (found)
		out->set(nn.x, nn.y, sparse_vals[offset]);
	}
}

__global__ void fillIn_vals_and_weights(float sigma, float * sparse_vals, float * qx, float* qy, float* qz, int * labels, int numVals, gpu_frame * frame, 
	float4x4 * a, float4x4 * a_inv, 
	gpu_frame * out, gpu_frame * weights_out, gpu_cam * cam)
{

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int offset = i;
	if (offset < numVals)
	{
		int2 nn;
		float3 q = float3{ qx[offset], qy[offset], qz[offset] };
		float3 q_;
		a_inv[labels[offset]].mult(q, q_);
		a[labels[offset]].mult(q_, q);
		
		//float dist = 1e-3, bestdist_sqr;
		float dist = 1, bestdist_sqr;
		bool found = frame->nn(q, cam, nn, bestdist_sqr, dist);
		//out->set(nn.x, nn.y, sparse_vals[offset]);
		if (found){
			out->set(nn.x, nn.y, sparse_vals[offset]);
			weights_out->set(nn.x, nn.y, exp(-bestdist_sqr / sigma)); 
		}

		
	}
}

__global__ void doMin(cudaTextureObject_t a, cudaTextureObject_t b, gpu_frame * out)
{

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	float a_ = tex2D<float>(a, i, j);
	float b_ = tex2D<float>(b, i, j);
	out->set(i, j, (a_ < b_ ? a_ : b_));
	
}

__global__ void doMax(cudaTextureObject_t a, cudaTextureObject_t b, gpu_frame * out)
{

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	float a_ = tex2D<float>(a, i, j);
	float b_ = tex2D<float>(b, i, j);
	out->set(i, j, (a_ > b_ ? a_ : b_));
}

__global__ void doExpSum(cudaTextureObject_t a, cudaTextureObject_t b, cudaTextureObject_t mins, float lambda, gpu_frame * out)
{

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	float a_ = tex2D<float>(a, i, j);
	float b_ = tex2D<float>(b, i, j);
	float min_ = tex2D<float>(mins, i, j);
	out->set(i, j, a_ + exp(- lambda * (b_-min_)));
}

__global__ void doExpSum(cudaTextureObject_t a, cudaTextureObject_t b, cudaTextureObject_t mins, cudaTextureObject_t maxs, float lambda, gpu_frame * out)
{

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	float a_ = tex2D<float>(a, i, j);
	float b_ = tex2D<float>(b, i, j);
	float min_ = tex2D<float>(mins, i, j);
	float max_ = tex2D<float>(maxs, i, j);
	float tmp = (min_ == max_ ? 1 : exp(-lambda * (b_ - min_)/max_));
	out->set(i, j, a_ + tmp);
}
__global__ void doExpAndNormalize(cudaTextureObject_t q, cudaTextureObject_t normalizer, cudaTextureObject_t mins, float lambda, gpu_frame * out)
{

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	float q_ = tex2D<float>(q, i, j);
	float normalizer_ = tex2D<float>(normalizer, i, j);
	float min_ = tex2D<float>(mins, i, j);
	out->set(i, j,  1.0/normalizer_ * exp(-lambda * (q_ - min_)));
}

__global__ void doExpAndNormalize(cudaTextureObject_t q, cudaTextureObject_t normalizer, cudaTextureObject_t mins, cudaTextureObject_t maxs, float lambda, gpu_frame * out)
{

	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	float q_ = tex2D<float>(q, i, j);
	float normalizer_ = tex2D<float>(normalizer, i, j);
	float min_ = tex2D<float>(mins, i, j);
	float max_ = tex2D<float>(maxs, i, j);
	float tmp = (min_ == max_ ? 1 : exp(-lambda * (q_ - min_) / max_));
	out->set(i, j, 1.0 / normalizer_ * tmp);
}

void cudaDataterm::spatial_crf_singleFrameResults::filterSparse2Dense_single_hyp(int hypothesis, gpu_queries * allQueries, gpu_dataterm * dterm, int firstQ, int lastQ,
	gpu_frame * depthFrame, gpu_frame * stddevFrame, gpu_cam * cam,
	float sigma, float lambda)
{
	int THREADS_PER_BLOCK = 128;
	sigma_spatial = sigma;
	lambda_spatial = lambda;
	dim3 w_h_8((w + 7) / 8, (h + 7) / 8);
	dim3 eights(8, 8);

	//0. initialize buffer with 0s. Needed as only a sparse set of values isfilled in
	//gpu_buffers_Q[hypothesis]->set2zero();
	gpu_buffers_Q[hypothesis]->set2Const(std::numeric_limits<float>::quiet_NaN());

	//1. fill in the sparse values. Seems to work, but make sure the sparse values were exped and normlaized first!

	fillIn_1mvals << < (lastQ - firstQ + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK >> >(
		//			thrust::raw_pointer_cast(dterm->dterm[hypothesis].data()) + firstQ,
		thrust::raw_pointer_cast(dterm->crf_final_result[hypothesis].data()) + firstQ,
		thrust::raw_pointer_cast(allQueries->query_x.data()) + firstQ,
		thrust::raw_pointer_cast(allQueries->query_y.data()) + firstQ,
		thrust::raw_pointer_cast(allQueries->query_z.data()) + firstQ,
		lastQ - firstQ,
		depthFrame->dev_ptr,
		gpu_buffers_Q[hypothesis]->dev_ptr,
		cam->dev_ptr);


	//2. filter the sparse values to get dense unnormalized values.

	//Approximating the filter by decomposing it and doing bruteforce along the single dimensions.
	int kernelwidth_2 = spatial_support_2;
#ifdef USE_GEODESIC_CRF_WITH_STDVAR
	crfFilterGeodesic_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, stddevFrame->frame, tempBuffer->dev_ptr, cam->dev_ptr, kernelwidth_2);// , sigma_spatial);
	crfFilterGeodesic_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, stddevFrame->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2,0);// , sigma_spatial, 0);//		
#else
	crfFilter_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame,  tempBuffer->dev_ptr, cam->dev_ptr, kernelwidth_2 , sigma_spatial);
	crfFilter_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame,  gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial, 0);//		
#endif

}

void cudaDataterm::spatial_crf_singleFrameResults::filterFillinSparse2Dense_NN_single_hyp(int hypothesis, gpu_queries * allQueries, gpu_dataterm * dterm, int firstQ, int lastQ,
	gpu_frame * depthFrame, gpu_frame * stddevFrame, gpu_cam * cam)
{
	int THREADS_PER_BLOCK = 128;
	dim3 w_h_8((w + 7) / 8, (h + 7) / 8);
	dim3 eights(8, 8);

	//0. initialize buffer with 0s. Needed as only a sparse set of values isfilled in
	//gpu_buffers_Q[hypothesis]->set2zero();
	gpu_buffers_Q[hypothesis]->set2Const(std::numeric_limits<float>::quiet_NaN());

	//1. fill in the sparse values. Seems to work, but make sure the sparse values were exped and normlaized first!

	fillIn_vals << < (lastQ - firstQ + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK >> >(
		//			thrust::raw_pointer_cast(dterm->dterm[hypothesis].data()) + firstQ,
		thrust::raw_pointer_cast(dterm->crf_final_result[hypothesis].data()) + firstQ,
		thrust::raw_pointer_cast(allQueries->query_x.data()) + firstQ,
		thrust::raw_pointer_cast(allQueries->query_y.data()) + firstQ,
		thrust::raw_pointer_cast(allQueries->query_z.data()) + firstQ,
		lastQ - firstQ,
		depthFrame->dev_ptr,
		gpu_buffers_Q[hypothesis]->dev_ptr,
		cam->dev_ptr);


	//2. filter the sparse values to get dense unnormalized values.
	nnPass << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->dev_ptr, depthFrame->dev_ptr,  tempBuffer->dev_ptr, cam->dev_ptr);
	nnPass << < w_h_8, eights >> >(tempBuffer->dev_ptr, depthFrame->dev_ptr, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, 0);

}

void cudaDataterm::spatial_crf_singleFrameResults::interpolateSparse2Dense_single_hyp(int hypothesis, gpu_queries * allQueries, gpu_dataterm * dterm, int firstQ, int lastQ,
	gpu_frame * depthFrame, gpu_frame * stdev, gpu_cam * cam,
	float sigma, float lambda, float sigma_factor)
{
	float fillInForMissing = 0;
	int THREADS_PER_BLOCK = 128;
	sigma_spatial = sigma;
	lambda_spatial = lambda;
	dim3 w_h_8((w + 7) / 8, (h + 7) / 8);
	dim3 eights(8, 8);
	//dim3 w_h_8((w + 15) / 16, (h + 15) / 16);
	//dim3 eights(16, 16);

	//0. initialize buffer with 0s. Needed as only a sparse set of values isfilled in
	//gpu_buffers_Q[hypothesis]->set2zero();
	gpu_buffers_Q[hypothesis]->set2Const(std::numeric_limits<float>::quiet_NaN());

	//1. fill in the sparse values. Seems to work, but make sure the sparse values were exped and normlaized first!

	fillIn_vals << < (lastQ - firstQ + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK >> >(
		//			thrust::raw_pointer_cast(dterm->dterm[hypothesis].data()) + firstQ,
		thrust::raw_pointer_cast(dterm->crf_final_result[hypothesis].data()) + firstQ,
		thrust::raw_pointer_cast(allQueries->query_x.data()) + firstQ,
		thrust::raw_pointer_cast(allQueries->query_y.data()) + firstQ,
		thrust::raw_pointer_cast(allQueries->query_z.data()) + firstQ,
		lastQ - firstQ,
		depthFrame->dev_ptr,
		gpu_buffers_Q[hypothesis]->dev_ptr,
		cam->dev_ptr);


	//2. filter the sparse values to get dense unnormalized values.

	//Approximating the filter by decomposing it and doing bruteforce along the single dimensions.
	int kernelwidth_2 = spatial_support_2;
	//interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial);
	//interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial);//		

	//splitting spatial filtering into more iterations.
	float root3_div_pow_4_3_m1 = 0.21821789;
#ifdef INTERPOLATE_USING_SIGMA
	//std::cout << "Filtering while scaling sigma with " << sigma_factor << "\n";
	interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, stdev->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2,  root3_div_pow_4_3_m1 * 4* sigma_factor);
	interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, stdev->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, root3_div_pow_4_3_m1 * 4 * sigma_factor, std::numeric_limits<float>::quiet_NaN());//		

	kernelwidth_2 = std::max<int>(kernelwidth_2/2,1);
	interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, stdev->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, root3_div_pow_4_3_m1 * 2 * sigma_factor);
	interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, stdev->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, root3_div_pow_4_3_m1 * 2 * sigma_factor, std::numeric_limits<float>::quiet_NaN());//		

	kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
	interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, stdev->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, root3_div_pow_4_3_m1* sigma_factor);
	interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, stdev->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, root3_div_pow_4_3_m1* sigma_factor, fillInForMissing);//
#else
	interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 4);
	interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 4, std::numeric_limits<float>::quiet_NaN());//		

	kernelwidth_2 = std::max<int>(kernelwidth_2/2,1);
	interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 2);
	interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 2, std::numeric_limits<float>::quiet_NaN());//		

	kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
	interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1);
	interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1, fillInForMissing);//		
#endif

}

//no reason to put that in spatial_crf_singleFrameResults::...
void cudaDataterm::filter_dense2dense_allFrames_currentData(
	float sigma_spatial, float sig_factor)
{
	float root3_div_pow_4_3_m1 = 0.21821789;
	int kernelwidth_2 = spatial_crf.spatial_support_2;
	dim3 w_h_8((w + 7) / 8, (h + 7) / 8);
	dim3 eights(8, 8);

	auto & outbuffer = (all_crf_sums[0]);
	auto & sums = (all_crf_mins[0]);
	for (int frame = 0; frame < gpu_frames.size(); frame++)
	{
		
		kernelwidth_2 = spatial_crf.spatial_support_2;

#ifdef SMOOTHEN_USING_SIGMA
		smoothen_vertical_pass1_adaptiveSig << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, gpu_frames_noise[frame]->frame, outbuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sig_factor* root3_div_pow_4_3_m1 * 4);
		smoothen_horizontal_pass2_adaptiveSig << <w_h_8, eights >> >(outbuffer->frame, gpu_frames[frame]->frame, gpu_frames_noise[frame]->frame, sums->frame, gpu_frames_dense_Q[frame]->dev_ptr, cam->dev_ptr, kernelwidth_2, sig_factor* root3_div_pow_4_3_m1 * 4, std::numeric_limits<float>::quiet_NaN());//		

		kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
		smoothen_vertical_pass1_adaptiveSig << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, gpu_frames_noise[frame]->frame, outbuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sig_factor* root3_div_pow_4_3_m1 * 2);
		smoothen_horizontal_pass2_adaptiveSig << <w_h_8, eights >> >(outbuffer->frame, gpu_frames[frame]->frame, gpu_frames_noise[frame]->frame, sums->frame, gpu_frames_dense_Q[frame]->dev_ptr, cam->dev_ptr, kernelwidth_2, sig_factor* root3_div_pow_4_3_m1 * 2, std::numeric_limits<float>::quiet_NaN());//		

		kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
		smoothen_vertical_pass1_adaptiveSig << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, gpu_frames_noise[frame]->frame, outbuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sig_factor* root3_div_pow_4_3_m1);
		smoothen_horizontal_pass2_adaptiveSig << <w_h_8, eights >> >(outbuffer->frame, gpu_frames[frame]->frame, gpu_frames_noise[frame]->frame, sums->frame, gpu_frames_dense_Q[frame]->dev_ptr, cam->dev_ptr, kernelwidth_2, sig_factor* root3_div_pow_4_3_m1, 0);
#else
		//interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, initialWeights->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 4);
		smoothen_vertical_pass1 << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, outbuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 4);
		smoothen_horizontal_pass2 << <w_h_8, eights >> >(outbuffer->frame, gpu_frames[frame]->frame, sums->frame, gpu_frames_dense_Q[frame]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 4, std::numeric_limits<float>::quiet_NaN());//		

		kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
		smoothen_vertical_pass1 << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, outbuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 2);
		smoothen_horizontal_pass2 << <w_h_8, eights >> >(outbuffer->frame, gpu_frames[frame]->frame, sums->frame, gpu_frames_dense_Q[frame]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 2, std::numeric_limits<float>::quiet_NaN());//		

		kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
		smoothen_vertical_pass1 << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, outbuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1);
		smoothen_horizontal_pass2 << <w_h_8, eights >> >(outbuffer->frame, gpu_frames[frame]->frame, sums->frame, gpu_frames_dense_Q[frame]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1, 0);//		*/
#endif
	}

}

void cudaDataterm::filter_sparse2dense_allFrames_currentData(
	int hypothesis,
	float sigma_spatial,
	float sig_factor)
{
	int THREADS_PER_BLOCK = 128;
	float root3_div_pow_4_3_m1 = 0.21821789;
	int kernelwidth_2 = spatial_crf.spatial_support_2;
	dim3 w_h_8((w + 7) / 8, (h + 7) / 8);
	dim3 eights(8, 8);

	auto & outbuffer = (all_crf_sums[0]);
	auto & sums = (all_crf_mins[0]);
	for (int frame = 0; frame < gpu_frames.size(); frame++)
	{
		int firstQ = frame_QueryRange[frame].first;
		int lastQ = frame_QueryRange[frame].second;

		gpu_frames_dense_Q[frame]->set2Const(0); //can think about that....

		fillIn_vals << < (lastQ - firstQ + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK >> >(
						thrust::raw_pointer_cast(dterm->dterm[hypothesis].data()) + firstQ,
			//thrust::raw_pointer_cast(dterm->crf_final_result[hypothesis].data()) + firstQ,
			thrust::raw_pointer_cast(allQueries->query_x.data()) + firstQ,
			thrust::raw_pointer_cast(allQueries->query_y.data()) + firstQ,
			thrust::raw_pointer_cast(allQueries->query_z.data()) + firstQ,
			lastQ - firstQ,
			gpu_frames[frame]->dev_ptr,
			gpu_frames_dense_Q[frame]->dev_ptr,
			cam->dev_ptr);

		kernelwidth_2 = spatial_crf.spatial_support_2;
#ifdef SMOOTHEN_USING_SIGMA
		smoothen_vertical_pass1_adaptiveSig << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, gpu_frames_noise[frame]->frame, outbuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sig_factor* root3_div_pow_4_3_m1 * 4);
		smoothen_horizontal_pass2_adaptiveSig << <w_h_8, eights >> >(outbuffer->frame, gpu_frames[frame]->frame, gpu_frames_noise[frame]->frame, sums->frame, gpu_frames_dense_Q[frame]->dev_ptr, cam->dev_ptr, kernelwidth_2, sig_factor* root3_div_pow_4_3_m1 * 4, std::numeric_limits<float>::quiet_NaN());//		

		kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
		smoothen_vertical_pass1_adaptiveSig << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, gpu_frames_noise[frame]->frame, outbuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sig_factor* root3_div_pow_4_3_m1 * 2);
		smoothen_horizontal_pass2_adaptiveSig << <w_h_8, eights >> >(outbuffer->frame, gpu_frames[frame]->frame, gpu_frames_noise[frame]->frame, sums->frame, gpu_frames_dense_Q[frame]->dev_ptr, cam->dev_ptr, kernelwidth_2, sig_factor* root3_div_pow_4_3_m1 * 2, std::numeric_limits<float>::quiet_NaN());//		

		kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
		smoothen_vertical_pass1_adaptiveSig << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, gpu_frames_noise[frame]->frame, outbuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sig_factor* root3_div_pow_4_3_m1);
		smoothen_horizontal_pass2_adaptiveSig << <w_h_8, eights >> >(outbuffer->frame, gpu_frames[frame]->frame, gpu_frames_noise[frame]->frame, sums->frame, gpu_frames_dense_Q[frame]->dev_ptr, cam->dev_ptr, kernelwidth_2, sig_factor* root3_div_pow_4_3_m1, 0);
#else
		//interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, initialWeights->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 4);
		smoothen_vertical_pass1 << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, outbuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 4);
		smoothen_horizontal_pass2 << <w_h_8, eights >> >(outbuffer->frame, gpu_frames[frame]->frame, sums->frame, gpu_frames_dense_Q[frame]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 4, std::numeric_limits<float>::quiet_NaN());//		

		kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
		smoothen_vertical_pass1 << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, outbuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 2);
		smoothen_horizontal_pass2 << <w_h_8, eights >> >(outbuffer->frame, gpu_frames[frame]->frame, sums->frame, gpu_frames_dense_Q[frame]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 2, std::numeric_limits<float>::quiet_NaN());//		

		kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
		smoothen_vertical_pass1 << < w_h_8, eights >> >(gpu_frames_dense_Q[frame]->frame, gpu_frames[frame]->frame, outbuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1);
		smoothen_horizontal_pass2 << <w_h_8, eights >> >(outbuffer->frame, gpu_frames[frame]->frame, sums->frame, gpu_frames_dense_Q[frame]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1, 0);//		*/
#endif
	}
}

void cudaDataterm::spatial_crf_singleFrameResults::interpolateSparse2Dense_surroundingFrames_single_hyp(
	int hypothesis,
	gpu_queries * allQueries,
	gpu_dataterm * dterm,
	gpu_alignments * als,
	std::vector<std::pair<int, int>> &frame_QueryRange,
	int frame, int delta,
	gpu_frame * depthFrame, gpu_frame * stdev, gpu_cam * cam,
	float sigma_spatial, float  sigma_factor)
{
	int THREADS_PER_BLOCK = 128;
	float fillInForMissing = 0;

	dim3 w_h_8((w + 7) / 8, (h + 7) / 8);
	dim3 eights(8, 8);
	//dim3 w_h_8((w + 15) / 16, (h + 15) / 16);
	//dim3 eights(16, 16);

	//std::cout << "\nok, doing single hyp " << hypothesis << " " << gpu_buffers_Q.size() << "/" << dterm->crf_final_result.size() << "\n";
	
	gpu_buffers_Q[hypothesis]->set2Const(std::numeric_limits<float>::quiet_NaN());
	
	auto & initialWeights = generalpurposeBuffer;
	initialWeights->set2zero();
	for (int f = std::max<int>(frame - delta, 0); f <= std::min<int>(frame + delta, frame_QueryRange.size() - 1); f++){
		//std::cout << f << " ";

		int firstQ = frame_QueryRange[f].first;
		int lastQ = frame_QueryRange[f].second;
		//std::cout << "(" << firstQ << "-" << lastQ << ")";
		if (f == frame){
			/*fillIn_vals << < (lastQ - firstQ + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK >> >(
				//			thrust::raw_pointer_cast(dterm->dterm[hypothesis].data()) + firstQ,
				thrust::raw_pointer_cast(dterm->crf_final_result[hypothesis].data()) + firstQ,
				thrust::raw_pointer_cast(allQueries->query_x.data()) + firstQ,
				thrust::raw_pointer_cast(allQueries->query_y.data()) + firstQ,
				thrust::raw_pointer_cast(allQueries->query_z.data()) + firstQ,
				lastQ - firstQ,
				depthFrame->dev_ptr,
				gpu_buffers_Q[hypothesis]->dev_ptr,
				cam->dev_ptr);*/ //WRONG! need to update weights obviously.

			fillIn_vals_and_weights << < (lastQ - firstQ + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK >> >(
				sigma_spatial,
				//			thrust::raw_pointer_cast(dterm->dterm[hypothesis].data()) + firstQ,
				thrust::raw_pointer_cast(dterm->crf_final_result[hypothesis].data()) + firstQ,
				thrust::raw_pointer_cast(allQueries->query_x.data()) + firstQ,
				thrust::raw_pointer_cast(allQueries->query_y.data()) + firstQ,
				thrust::raw_pointer_cast(allQueries->query_z.data()) + firstQ,
				thrust::raw_pointer_cast(allQueries->label.data()) + firstQ, //label: to choose what alignment should be used. Where is it updated?
				lastQ - firstQ,
				depthFrame->dev_ptr,
				thrust::raw_pointer_cast(als->As_per_frame[frame].data()),
				thrust::raw_pointer_cast(als->As_inv_per_frame[f].data()),
				gpu_buffers_Q[hypothesis]->dev_ptr,
				initialWeights->dev_ptr,
				cam->dev_ptr);//
		}
		else{

			//sums need to be updated AND used...
			fillIn_vals_and_weights << < (lastQ - firstQ + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK >> >(
				sigma_spatial,
				//			thrust::raw_pointer_cast(dterm->dterm[hypothesis].data()) + firstQ,
				thrust::raw_pointer_cast(dterm->crf_final_result[hypothesis].data()) + firstQ,
				thrust::raw_pointer_cast(allQueries->query_x.data()) + firstQ,
				thrust::raw_pointer_cast(allQueries->query_y.data()) + firstQ,
				thrust::raw_pointer_cast(allQueries->query_z.data()) + firstQ,
				thrust::raw_pointer_cast(allQueries->label.data()) + firstQ,
				lastQ - firstQ,
				depthFrame->dev_ptr,
				thrust::raw_pointer_cast(als->As_per_frame[frame].data()),
				thrust::raw_pointer_cast(als->As_inv_per_frame[f].data()),
				gpu_buffers_Q[hypothesis]->dev_ptr,
				initialWeights->dev_ptr,
				cam->dev_ptr);//
			}
	}


	//2. filter the sparse values to get dense unnormalized values.

	//Approximating the filter by decomposing it and doing bruteforce along the single dimensions.
	int kernelwidth_2 = spatial_support_2;
	//interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, spatial_support_2, sigma_spatial);
	//interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, spatial_support_2, sigma_spatial);//

	float root3_div_pow_4_3_m1 = 0.21821789;
#ifdef INTERPOLATE_USING_SIGMA

	interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, initialWeights->frame, stdev->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, root3_div_pow_4_3_m1 * 4 * sigma_factor);
	//interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 4);
	interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, stdev->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, root3_div_pow_4_3_m1 * 4 * sigma_factor, std::numeric_limits<float>::quiet_NaN());//		

	kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
	interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, stdev->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, root3_div_pow_4_3_m1 * 2 * sigma_factor);
	interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, stdev->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, root3_div_pow_4_3_m1 * 2 * sigma_factor, std::numeric_limits<float>::quiet_NaN());//		

	kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
	interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, stdev->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, root3_div_pow_4_3_m1* sigma_factor);
	interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, stdev->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, root3_div_pow_4_3_m1* sigma_factor, fillInForMissing);//		
#else
	interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, initialWeights->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 4);
	//interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 4);
	interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 4, std::numeric_limits<float>::quiet_NaN());//		

	kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
	interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 2);
	interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1 * 2, std::numeric_limits<float>::quiet_NaN());//		

	kernelwidth_2 = std::max<int>(kernelwidth_2 / 2, 1);
	interpolate_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, tempBuffer->dev_ptr, sums->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1);
	interpolate_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, sums->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial* root3_div_pow_4_3_m1, fillInForMissing);//		
#endif
	//gpuErrchk(cudaPeekAtLastError());
	//gpuErrchk(cudaDeviceSynchronize());
	//gpuErrchk(cudaPeekAtLastError());
	//std::cout << "done.\n";
}

void cudaDataterm::spatial_crf_singleFrameResults::fillInSparse2Dense(gpu_queries * allQueries, gpu_dataterm * dterm, int firstQ, int lastQ,
	gpu_frame * depthFrame, gpu_cam * cam)
{
	int THREADS_PER_BLOCK = 128;

	dim3 w_h(w, h);
	dim3 ones(1, 1);
	dim3 w_h_8((w + 7) / 8, (h + 7) / 8);
	dim3 eights(8, 8);

	//std::cout << lastQ - firstQ ;
	//innit min 


	/////////////////////

	//filter all hypotheses and store the result to buffer. This is done for all hypotheses in order to normalize the re^sulting values.
	//as memory consumption can be too high when storing the interpolated values for all dense hypotheses and all frames,
	//we store only the values for the hypothesis being filtered next.
	for (int hypothesis = 0; hypothesis < dterm->dterm.size(); hypothesis++){

		//0. initialize buffer with 0s. Needed as only a sparse set of values isfilled in
		gpu_buffers_Q[hypothesis]->set2zero();
		//gpu_buffers_Q[hypothesis]->set2Const(std::numeric_limits<float>::quiet_NaN());

		//1. fill in the sparse values. Seems to work, but make sure the sparse values were exped and normlaized first!

		fillIn_vals << < (lastQ - firstQ + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK >> >(
			//			thrust::raw_pointer_cast(dterm->dterm[hypothesis].data()) + firstQ,
			thrust::raw_pointer_cast(dterm->crf_final_result[hypothesis].data()) + firstQ,
			thrust::raw_pointer_cast(allQueries->query_x.data()) + firstQ,
			thrust::raw_pointer_cast(allQueries->query_y.data()) + firstQ,
			thrust::raw_pointer_cast(allQueries->query_z.data()) + firstQ,
			lastQ - firstQ,
			depthFrame->dev_ptr,
			gpu_buffers_Q[hypothesis]->dev_ptr,
			cam->dev_ptr);
	}
}

void cudaDataterm::spatial_crf_singleFrameResults::filterSparse2Dense(
	gpu_queries * allQueries, gpu_dataterm * dterm, int firstQ, int lastQ,
	gpu_frame * depthFrame, gpu_frame * stddevFrame, gpu_cam * cam,
	float sigma, float lambda, bool divByMax)
{
	int THREADS_PER_BLOCK = 128;

	dim3 w_h(w, h);
	dim3 ones(1, 1);
	dim3 w_h_8((w + 7) / 8, (h + 7) / 8);
	dim3 eights(8, 8);

	sigma_spatial = sigma;
	lambda_spatial = lambda;
	
	//std::cout << lastQ - firstQ ;
	//innit min 

		
	/////////////////////

	//filter all hypotheses and store the result to buffer. This is done for all hypotheses in order to normalize the re^sulting values.
	//as memory consumption can be too high when storing the interpolated values for all dense hypotheses and all frames,
	//we store only the values for the hypothesis being filtered next.
	for (int hypothesis = 0; hypothesis < dterm->dterm.size(); hypothesis++){
		
		//0. initialize buffer with 0s. Needed as only a sparse set of values isfilled in
		//gpu_buffers_Q[hypothesis]->set2zero();
		gpu_buffers_Q[hypothesis]->set2Const(std::numeric_limits<float>::quiet_NaN());

		//1. fill in the sparse values. Seems to work, but make sure the sparse values were exped and normlaized first!

		fillIn_1mvals << < (lastQ - firstQ + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK >> >(
//			thrust::raw_pointer_cast(dterm->dterm[hypothesis].data()) + firstQ,
			thrust::raw_pointer_cast(dterm->crf_final_result[hypothesis].data()) + firstQ,
			thrust::raw_pointer_cast(allQueries->query_x.data()) + firstQ,
			thrust::raw_pointer_cast(allQueries->query_y.data()) + firstQ,
			thrust::raw_pointer_cast(allQueries->query_z.data()) + firstQ,
			lastQ - firstQ,
			depthFrame->dev_ptr,
			gpu_buffers_Q[hypothesis]->dev_ptr,
			cam->dev_ptr);


		//2. filter the sparse values to get dense unnormalized values.

		//Approximating the filter by decomposing it and doing bruteforce along the single dimensions.
		int kernelwidth_2 = spatial_support_2;
#ifdef USE_GEODESIC_CRF_WITH_STDVAR
		crfFilterGeodesic_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, stddevFrame->frame, tempBuffer->dev_ptr, cam->dev_ptr, kernelwidth_2);// , sigma_spatial);
		crfFilterGeodesic_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame, stddevFrame->frame, gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2,0);// , sigma_spatial, 0);//
#else
		crfFilter_vertical_pass1 << < w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, tempBuffer->dev_ptr, cam->dev_ptr, kernelwidth_2 , sigma_spatial);
		crfFilter_horizontal_pass2 << <w_h_8, eights >> >(tempBuffer->frame, depthFrame->frame,  gpu_buffers_Q[hypothesis]->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial, 0);//
#endif
		//TODO integrate sigma s and sigma r correctly.
		//Domaintransform:

		//auto & tempBuffer2 = sums;
	
		/*fillInMissing_horiz << <w_h_8, eights >> >(depthFrame->frame, w, tempBuffer2->dev_frame_data);
		fillInMissing_horiz << <w_h_8, eights >> >(depthFrame->frame, w, tempBuffer2->dev_frame_data);*/
		//fillInMissing_horiz << <h, 1 >> >(depthFrame->frame, w, tempBuffer2->dev_ptr);
	//	domainTransform_findDI << <w_h, ones >> >(1, 0, tempBuffer2->frame, cam->dev_ptr, tempBuffer->dev_ptr);
	//	domainTransform_summedAreaTable_horiz << <h, 1 >> >(tempBuffer->frame, w, dIdxSAT->dev_ptr);
					
		
		//fillInMissing_vert << <w, 1 >> >(depthFrame->frame, h, tempBuffer2->dev_ptr);
	//	domainTransform_findDI << <w_h, ones >> >(0, 1, tempBuffer2->frame, cam->dev_ptr, tempBuffer->dev_ptr);
	//	domainTransform_summedAreaTable_vert << <w, 1 >> >(tempBuffer->frame, h, dIdySAT->dev_ptr);
	
		/*//one iteration of dtransffiltering.
		int num_dom_filter_it = 3;
		for (int domFilterIt = 0; domFilterIt < 3; domFilterIt++)
		{
			float sigma_H_i = sigma_spatial * sqrt(3) * pow(2, (num_dom_filter_it - (domFilterIt + 1.0)) / sqrt(pow(4, num_dom_filter_it) - 1));
			//% Compute the radius of the box filter with the desired variance.
			//The thing is I do not normalize!
			float box_radius = sqrt(3) * sigma_H_i; 

			domainTransform_summedAreaTable_horiz << <h, 1 >> >(gpu_buffers_Q[hypothesis]->frame, w, valSAT->dev_ptr);
			domainTransform_domainBoxFilter_horiz << <h, 1 >> >(dIdxSAT->frame, valSAT->frame, w, box_radius, gpu_buffers_Q[hypothesis]->dev_ptr);

			domainTransform_summedAreaTable_vert << <w, 1 >> >(gpu_buffers_Q[hypothesis]->frame, h, valSAT->dev_ptr);
			domainTransform_domainBoxFilter_vert << <w, 1 >> >(dIdySAT->frame, valSAT->frame, h, box_radius, gpu_buffers_Q[hypothesis]->dev_ptr);
		}
	gpuErrchk(cudaPeekAtLastError());
	gpuErrchk(cudaDeviceSynchronize());//*/

		/*/
		//sinlge pass brute force filter, with buffer swap:
		{
			crfFilter_bruteforce << <blocks, 1 >> >(gpu_buffers_Q[hypothesis]->frame, depthFrame->frame, tempBuffer->dev_ptr, cam->dev_ptr, kernelwidth_2, sigma_spatial);
			gpu_frame * tmp = gpu_buffers_Q[hypothesis];
			gpu_buffers_Q[hypothesis] = tempBuffer;
			tempBuffer = tmp;
		}//*/


		//to normalize: sum over the buffers and find minimum. Could merge kernels.
		if (hypothesis == 0){
			//init mins
			doMin << <w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, gpu_buffers_Q[hypothesis]->frame, mins->dev_ptr);
			doMax << <w_h_8, eights >> >(gpu_buffers_Q[hypothesis]->frame, gpu_buffers_Q[hypothesis]->frame, maxs->dev_ptr);
		}
		else{
			doMin << <w_h_8, eights >> >(mins->frame, gpu_buffers_Q[hypothesis]->frame, mins->dev_ptr);
			doMax << <w_h_8, eights >> >(maxs->frame, gpu_buffers_Q[hypothesis]->frame, maxs->dev_ptr);
		}
	}
	//store in buffer.
	//sums->copy2Gpu(zeros);
	sums->set2zero();
	for (int hypothesis = 0; hypothesis < dterm->dterm.size(); hypothesis++){
		if (divByMax){
			doExpSum << <w_h_8, eights >> >(sums->frame, gpu_buffers_Q[hypothesis]->frame, mins->frame, maxs->frame, lambda_spatial, sums->dev_ptr);
		}
		else{
			doExpSum << <w_h_8, eights >> >(sums->frame, gpu_buffers_Q[hypothesis]->frame, mins->frame, lambda_spatial, sums->dev_ptr);
		}
	}
		
}


void cudaDataterm::spatial_crf_singleFrameResults::interpolateSparse2Dense(
	gpu_queries * allQueries, gpu_dataterm * dterm, int firstQ, int lastQ,
	gpu_frame * depthFrame, gpu_frame * stdevFrame, gpu_cam * cam,
	float sigma, float lambda, float sigma_factor)
{
	sigma_spatial = sigma;
	lambda_spatial = lambda;


	/////////////////////

	//filter all hypotheses and store the result to buffer. This is done for all hypotheses in order to normalize the re^sulting values.
	//as memory consumption can be too high when storing the interpolated values for all dense hypotheses and all frames,
	//we store only the values for the hypothesis being filtered next.
	for (int hypothesis = 0; hypothesis < dterm->dterm.size(); hypothesis++){
		interpolateSparse2Dense_single_hyp(hypothesis, allQueries, dterm, firstQ, lastQ, depthFrame, stdevFrame, cam, sigma, lambda, sigma_factor);
	}
	sums->set2zero();

}

void cudaDataterm::spatial_crf_singleFrameResults::interpolateSparse2Dense_surroundingFrames(
	gpu_queries * allQueries, 
	gpu_dataterm * dterm, 
	gpu_alignments * als,
	std::vector<std::pair<int, int>> &frame_QueryRange,
	int frame, int delta,
	gpu_frame * depthFrame, gpu_frame * stdevFrame, gpu_cam * cam,
	float sigma_spatial, float sigma_factor)
{
	/*int THREADS_PER_BLOCK = 128;
	float fillInForMissing = 0;

	dim3 w_h(w, h);
	dim3 ones(1, 1);
	dim3 w_h_8((w + 7) / 8, (h + 7) / 8);
	dim3 eights(8, 8);*/
	
	/////////////////////
	//filter all hypotheses and store the result to buffer. This is done for all hypotheses in order to normalize the re^sulting values.
	//as memory consumption can be too high when storing the interpolated values for all dense hypotheses and all frames,
	//we store only the values for the hypothesis being filtered next.
	//for (int hypothesis = 0; hypothesis < dterm->dterm.size(); hypothesis++){
	for (int hypothesis = dterm->dterm.size() - 1; hypothesis >= 0; hypothesis--){
		interpolateSparse2Dense_surroundingFrames_single_hyp(
			hypothesis, allQueries,
			dterm,
			als,
			frame_QueryRange,
			frame, delta,
			depthFrame,stdevFrame, cam,
			sigma_spatial, sigma_factor);

	}
	sums->set2zero();
}

void cudaDataterm::spatial_crf_singleFrameResults::normalizeFilteredHypAndCopyTo(int hyp,  gpu_frame * target)
{
	assert(lambda_spatial > 0);
	if (lambda_spatial < 0){
		std::cout << "Error at " << __FILE__ << " " << __LINE__ << ", requesting aptaially filtered results before filtering\n";
		exit(1);
	}
	dim3 w_h_8((w + 7) / 8, (h + 7) / 8);
	dim3 eights(8, 8);
	doExpAndNormalize <<< w_h_8, eights >> >(gpu_buffers_Q[hyp]->frame, sums->frame, mins->frame, lambda_spatial, target->dev_ptr);
}

void cudaDataterm::spatial_crf_singleFrameResults::normalizeFilteredHypAndCopyTo(int hyp, gpu_frame * mins_, gpu_frame * sums_, gpu_frame * target){
	assert(lambda_spatial > 0);
	if (lambda_spatial < 0){
		std::cout << "Error at " << __FILE__ << " " << __LINE__ << ", requesting aptaially filtered results before filtering\n";
		exit(1);
	}
	dim3 w_h_8((w + 7) / 8, (h + 7) / 8);
	dim3 eights(8, 8);
	doExpAndNormalize << < w_h_8, eights >> >(gpu_buffers_Q[hyp]->frame, sums_->frame, mins_->frame, lambda_spatial, target->dev_ptr);
}

void cudaDataterm::spatial_crf_singleFrameResults::normalizeFilteredHypAndCopyTo(int hyp, gpu_frame * mins_, gpu_frame * maxs_, gpu_frame * sums_, gpu_frame * target){
	assert(lambda_spatial > 0);
	if (lambda_spatial < 0){
		std::cout << "Error at " << __FILE__ << " " << __LINE__ << ", requesting aptaially filtered results before filtering\n";
		exit(1);
	}
	dim3 w_h_8((w + 7) / 8, (h + 7) / 8);
	dim3 eights(8, 8);
	doExpAndNormalize << < w_h_8, eights >> >(gpu_buffers_Q[hyp]->frame, sums_->frame, mins_->frame, maxs_->frame, lambda_spatial, target->dev_ptr);
}

void cudaDataterm::spatial_crf_singleFrameResults::getAndNormalize(int hyp, std::vector<float> & target, bool normalize, bool div_by_q)
{
	target.resize(w*h);
	//dim3 blocks(w, h);
	dim3 w_h_8((w + 7) / 8, (h + 7) / 8);
	dim3 eights(8, 8);
	//std::cout << "***Access gpu_buffer_Q : copying...";
	if (normalize)
	{
		if (div_by_q){
			doExpAndNormalize << <w_h_8, eights >> >(gpu_buffers_Q[hyp]->frame, sums->frame, mins->frame,maxs->frame, lambda_spatial, tempBuffer->dev_ptr);
		}
		else{
			doExpAndNormalize << <w_h_8, eights >> >(gpu_buffers_Q[hyp]->frame, sums->frame, mins->frame, lambda_spatial, tempBuffer->dev_ptr);
		}
		tempBuffer->copy2Cpu(target);
	}
	else{
		gpu_buffers_Q[hyp]->copy2Cpu(target);
	}
	//////DEBUG!!!!
	//gpu_buffers_Q[hyp]->copy2Cpu(target);
	////////////
}

void cudaDataterm::getCurrentDenseQBuffer(int frame, std::vector<float> & target){
	target.resize(w*h);
	gpu_frames_dense_Q[frame]->copy2Cpu(target);
}
void cudaDataterm::getNoise(int frame, std::vector<float> & target){
	target.resize(w*h);
	gpu_frames_noise[frame]->copy2Cpu(target);
}


cudaDataterm::spatial_crf_singleFrameResults & cudaDataterm::filter_sparse2dense_spatial_crf_oneFrame(int frame, float sigma_spatial, float lambda_spatial,float sigma_factor, bool normalize, bool divByMax){
	int firstQ = frame_QueryRange[frame].first;
	int lastQ = frame_QueryRange[frame].second;
	
	if (normalize)
	{
		spatial_crf.filterSparse2Dense(allQueries,
			dterm,
			firstQ, lastQ,
			gpu_frames[frame], gpu_frames_noise[frame],
			cam, sigma_spatial, lambda_spatial, divByMax);
	}
	else{
		spatial_crf.interpolateSparse2Dense(allQueries,
			dterm,
			firstQ, lastQ,
			gpu_frames[frame], gpu_frames_noise[frame],
			cam, sigma_spatial, lambda_spatial, sigma_factor);
	}
	return spatial_crf;
}


cudaDataterm::spatial_crf_singleFrameResults & cudaDataterm::interpolate_sparse2dense_oneFrame_fromMultipleFrames(int frame, float sigma_spatial, float sigma_factor, int surroundingFrames){
	
	spatial_crf.interpolateSparse2Dense_surroundingFrames(allQueries,
		dterm,
		gpuMotion,
		frame_QueryRange,
		frame,
		surroundingFrames,
		gpu_frames[frame], gpu_frames_noise[frame],
		cam, sigma_spatial, sigma_factor);
	
	return spatial_crf;
}


cudaDataterm::spatial_crf_singleFrameResults & cudaDataterm::interpolate_sparse2dense_spatial_oneFrame_oneHyp(int frame, int hyp, float sigma_spatial, float factor){
	int firstQ = frame_QueryRange[frame].first;
	int lastQ = frame_QueryRange[frame].second;

	spatial_crf.interpolateSparse2Dense_single_hyp(hyp,
		allQueries,
		dterm,
		firstQ, lastQ,
		gpu_frames[frame], gpu_frames_noise[frame],
		cam, sigma_spatial, 1, factor);

	return spatial_crf;
}


void cudaDataterm::filter_sparse2dense_prepare_spatial_crf(float sigma_spatial, float lambda_spatial, bool divByMax)
{
	//gpuErrchk(cudaPeekAtLastError());
	//gpuErrchk(cudaDeviceSynchronize());
	//gpuErrchk(cudaPeekAtLastError());
	cudaEvent_t start, stop;
	HANDLE_ERROR(cudaEventCreate(&start));
	HANDLE_ERROR(cudaEventCreate(&stop));
	HANDLE_ERROR(cudaEventRecord(start, 0));

	for (int frame = 0; frame < gpu_frames.size(); frame++)
	{
		
		int firstQ = frame_QueryRange[frame].first;
		int lastQ = frame_QueryRange[frame].second;
		//std::cout << "DEBUG (" << frame << ", " << firstQ << ", " << lastQ << "\n";

		spatial_crf.filterSparse2Dense(allQueries,
			dterm,
			firstQ, lastQ,
			gpu_frames[frame], gpu_frames_noise[frame],
			cam, sigma_spatial, lambda_spatial, divByMax);
		spatial_crf.getMinsAndSums(all_crf_mins[frame], all_crf_sums[frame], (divByMax? all_crf_maxs[frame] : NULL));

	}

	//cudaEventRecord(stop, 0);
	//cudaEventSynchronize(stop);
	//float elapsedTime;
	//HANDLE_ERROR(cudaEventElapsedTime(&elapsedTime, start, stop));
	//std::cout << "Spatial filtering took : " << elapsedTime;
	HANDLE_ERROR(cudaEventDestroy(start));
	HANDLE_ERROR(cudaEventDestroy(stop));
}

void cudaDataterm::filter_sparse2dense_do_prepared_spatial_crf(int hyp, float sigma_spatial, float lambda_spatial, float sigma_factor, bool doNormalization, bool div_by_max){
	if (gpu_frames_dense_Q.size() < gpu_frames.size()){
		std::cout << "\n**** Q not initialized, using dummy Q values for meanfield step!!!! ****\n\n";
	}
	//ch for dev_pointers.
	//0. Assumptions (test them)
	// -allQueries &  are allocated and correctly initialized

	//After: denseQ is the dense Qs after a spatial update for the given hypothesis
	
	//SOME ASSERTS
	//assert(gpu_buffers_Q.size() > hyp);
	assert(frame_QueryRange.size() == gpu_frames.size());
	if (dterm->dterm.size() <= hyp){
		std::cout << "assertion failed in " << __FILE__ << " " << __LINE__ << "\t" << dterm->dterm.size() << " vs " << hyp << "\n";
		exit(1);
	}
	if (spatial_crf.gpu_buffers_Q.size() <= hyp){
		std::cout << "assertion failed in " << __FILE__ << " " << __LINE__ << "\n";
		exit(1);
	}
	if (frame_QueryRange.size() != gpu_frames.size()){
		std::cout << "assertion failed in " << __FILE__ << " " << __LINE__ << "\n";
		std::cout << frame_QueryRange.size() << " VS " << gpu_frames.size() << "\n";
		exit(1);
	}
	//--------------------
	
	

	//cudaEvent_t start, stop;
	//HANDLE_ERROR(cudaEventCreate(&start));
	//HANDLE_ERROR(cudaEventCreate(&stop));
	//HANDLE_ERROR(cudaEventRecord(start, 0));
		
	for (int frame = 0; frame < gpu_frames.size(); frame++)
	{
		int firstQ = frame_QueryRange[frame].first;
		int lastQ = frame_QueryRange[frame].second;
			
		if (doNormalization){
			spatial_crf.filterSparse2Dense_single_hyp(hyp, allQueries,
				dterm,
				firstQ, lastQ,
				gpu_frames[frame], gpu_frames_noise[frame],
				cam, sigma_spatial, lambda_spatial);
			if (div_by_max){
				spatial_crf.normalizeFilteredHypAndCopyTo(hyp, all_crf_mins[frame], all_crf_maxs[frame],all_crf_sums[frame], gpu_frames_dense_Q[frame]);
			}
			else{
				spatial_crf.normalizeFilteredHypAndCopyTo(hyp, all_crf_mins[frame], all_crf_sums[frame], gpu_frames_dense_Q[frame]);
			}
		}
		else{
			
			spatial_crf.interpolateSparse2Dense_single_hyp(hyp, allQueries,
				dterm,
				firstQ, lastQ,
				gpu_frames[frame], gpu_frames_noise[frame],
				cam, sigma_spatial, lambda_spatial, sigma_factor);
//fiddled
//spatial_crf.interpolateSparse2Dense_surroundingFrames_single_hyp(hyp, allQueries, dterm,
//	gpuMotion,
//	frame_QueryRange,
//	frame,
//	5,
//	gpu_frames[frame],
//	cam, sigma_spatial);
//
			spatial_crf.gpu_buffers_Q[hyp]->copyThisTo(gpu_frames_dense_Q[frame]);
		}

	}

	//cudaEventRecord(stop, 0);
	//cudaEventSynchronize(stop);
	//float elapsedTime;
	//HANDLE_ERROR(cudaEventElapsedTime(&elapsedTime, start, stop));
	//std::cout << "Spatial filtering took : " << elapsedTime;
	//HANDLE_ERROR(cudaEventDestroy(start));
	//HANDLE_ERROR(cudaEventDestroy(stop));
	
}

void cudaDataterm::filter_sparse2dense_viaNN(int hyp){
	if (gpu_frames_dense_Q.size() < gpu_frames.size()){
		std::cout << "\n**** Q not initialized, using dummy Q values for meanfield step!!!! ****\n\n";
	}
	
	//SOME ASSERTS
	//assert(gpu_buffers_Q.size() > hyp);
	assert(frame_QueryRange.size() == gpu_frames.size());
	if (dterm->dterm.size() <= hyp){
		std::cout << "assertion failed in " << __FILE__ << " " << __LINE__ << "\t" << dterm->dterm.size() << " vs " << hyp << "\n";
	}
	if (spatial_crf.gpu_buffers_Q.size() <= hyp){
		std::cout << "assertion failed in " << __FILE__ << " " << __LINE__ << "\n";
	}
	if (frame_QueryRange.size() != gpu_frames.size()){
		std::cout << "assertion failed in " << __FILE__ << " " << __LINE__ << "\n";
	}
	//--------------------

	for (int frame = 0; frame < gpu_frames.size(); frame++)
	{
		int firstQ = frame_QueryRange[frame].first;
		int lastQ = frame_QueryRange[frame].second;
		spatial_crf.filterFillinSparse2Dense_NN_single_hyp(hyp, allQueries,
				dterm,
				firstQ, lastQ,
				gpu_frames[frame], gpu_frames_noise[frame],
				cam);
			
	}
}

void cudaDataterm::getUnnormedDataterm(int hyp, std::vector<float> & dataterm, std::vector<float> & weights)
{
	dataterm.clear();
	thrust::host_vector<float > tmp = dterm->dterm[hyp];
	dataterm.assign(tmp.begin(), tmp.end());
	tmp = dterm->weight[hyp];
	weights.clear();
	weights.assign(tmp.begin(),tmp.end());
}

void cudaDataterm::getSparseCRFResult(std::vector<std::vector<float>> & dataterm)
{
	for (int hyp = 0; hyp < dataterm.size(); hyp++){
		dataterm[hyp].clear();
		thrust::host_vector<float > tmp = dterm->crf_final_result[hyp];
		dataterm[hyp].assign(tmp.begin(), tmp.end());
	}
}

void cudaDataterm::getUnnormedDataterm(int hyp, std::vector<float> & dataterm)
{
	dataterm.clear();
	thrust::host_vector<float > tmp = dterm->dterm[hyp];
	dataterm.assign(tmp.begin(), tmp.end());
}

void cudaDataterm::getSparseValues(int hyp, std::vector<float> & dataterm)
{
	dataterm.clear();
	thrust::host_vector<float > tmp = dterm->dterm[hyp];
	dataterm.assign(tmp.begin(), tmp.end());
	tmp = dterm->weight[hyp];
	for (int i = 0; i < dataterm.size(); i++){
		dataterm[i] /= (tmp[i]>1e-5 ? tmp[i] : 1e-5);
	}
}

struct devide_eps {
	__host__ __device__ float operator()(const float& x, const float& y) const { return x/(y>1e-5? y: 1e-5); }
};

void cudaDataterm::normalize_sparse_by_weights()
{
	devide_eps op;
	//thrust::divides<float> op;
	for (int hyp = 0; hyp < dterm->numHyps; hyp++){
		thrust::transform(dterm->dterm[hyp].begin(), dterm->dterm[hyp].end(),dterm->weight[hyp].begin(), dterm->dterm[hyp].begin(), op);
	}
}

float cudaDataterm::getSummedUpSparseValues(int sparse_channel, std::vector<int> & hyps, std::vector<int> & labels)
{
	//could also be put on gpu.
	thrust::host_vector<float > tmp = dterm->dterm[sparse_channel];
	thrust::host_vector<float > tmp_weight = dterm->weight[sparse_channel];
	float sum = 0;
	if (labels.size() != tmp.size()){
		std::cout << "Assertion Error in " << __FILE__ << "line " << __LINE__ << "\n";
		exit(1);
	}
	for (int j = 0; j < tmp.size(); j++){
		for (int hyp : hyps){
			if (hyp == labels[j]){
				sum += tmp[j] / tmp_weight[j];
				break;
			}
		}
	}
	return sum;
	//return thrust::reduce(dterm->dterm[hyp].begin(), dterm->dterm[hyp].end());
}

void cudaDataterm::setSparseValues(std::vector<float> & dataterm, int hyp){

	thrust::host_vector<float > tmp = dataterm;
	dterm->dterm[hyp] = tmp;
	dterm->crf_final_result[hyp] = dterm->dterm[hyp];
}
void cudaDataterm::setSparseValues(std::vector<std::vector<float>> & dataterm){
	int hyp = 0;
	for (auto & d : dataterm){
		thrust::host_vector<float > tmp = d;
		dterm->dterm[hyp] = tmp;
		hyp++;
	}
}

void cudaDataterm::updateLabelsForInterpolationFormMultipleFrames(){
	allQueries->updateLabelsFromProbs(dterm->dterm, dterm->tmpbuffer);
	/*for (int i = 0; i < allQueries->label.size(); i += 100){
		std::cout << allQueries->label[i] << "\t";
	}*/

}