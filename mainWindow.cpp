#include "mainWindow.h"

mainWindow::mainWindow(QGLFormat & format)
{
	myGlDisp = new glDisplay(format, this, 400, 400);
	glDisplay_bot1 = new glDisplay(format, this, 150, 150);
	glDisplay_bot2 = new glDisplay(format, this, 150, 150);
	glDisplay_bot2_2 = new glDisplay(format, this, 150, 150);
	glDisplay_bot3 = new glDisplay(format, this, 150, 150);

	this->setUpButtons();
	this->layoutGui();
	this->addAction();

	this->setWindowTitle("PCL QT baseproject");
	this->resize(600, 500);
	this->show();
}


mainWindow::~mainWindow(void)
{
}


void mainWindow::layoutGui()
{
	QVBoxLayout *buttons = new QVBoxLayout();
	buttons->addWidget(l_out);
	buttons->addWidget(b1);
	buttons->addWidget(b2);
	buttons->addWidget(b3);
	buttons->addWidget(b4);

	QVBoxLayout *helperDisplays = new QVBoxLayout();
	helperDisplays->addWidget(glDisplay_bot1,0);
	QHBoxLayout *helperDisplays2 = new QHBoxLayout();
	helperDisplays2->addWidget(glDisplay_bot2,0);
	helperDisplays2->addWidget(glDisplay_bot2_2,0);
	helperDisplays->addLayout(helperDisplays2);
	helperDisplays->addWidget(glDisplay_bot3,0);


	QHBoxLayout *mainLayout = new QHBoxLayout();
	mainLayout->addWidget(myGlDisp,1);
	mainLayout->addLayout(buttons);
	mainLayout->addLayout(helperDisplays,1);

	QWidget * mainWidget = new QWidget();
	mainWidget->setLayout(mainLayout);
	this->setCentralWidget(mainWidget);
}

void mainWindow::addAction()
{
	
}


sceneManager * mainWindow::getSceneManager()
{
	return myGlDisp->getSceneManager();
}

 glDisplay* mainWindow::getGLWidget()
{
	return myGlDisp;
}

 void mainWindow::setUpButtons()
 {
	b1 = new QPushButton(this);
	b1->setText("Action1");
	b2 = new QPushButton(this);
	b2->setText("Action2");
	b3 = new QPushButton(this);
	b3->setText("Action3");
	b4 = new QPushButton(this);
	b4->setText("Action4");

	l_out = new QLabel(this);
	l_out->setText("      ");
 }

 glDisplay* mainWindow::getGLWidget_bot1()
 {
	 return glDisplay_bot1;
 }

 glDisplay* mainWindow::getGLWidget_bot2()
 {
	 return glDisplay_bot2;
 }

 glDisplay* mainWindow::getGLWidget_bot2_2()
 {
	 return glDisplay_bot2_2;
 }

 glDisplay* mainWindow::getGLWidget_bot3()
 {
	 return glDisplay_bot3;
 }

 void mainWindow::printClicked()
 {
	 std::cout << "clicked";
 }

 QPushButton * mainWindow::getButton1()
 {
	 return b1;
 }

 QPushButton * mainWindow::getButton2()
 {
	 return b2;
 }

 QPushButton * mainWindow::getButton4()
 {
	 return b4;
 }

 QPushButton * mainWindow::getButton3()
 {
	 return b3;
 }


 void mainWindow::keyPressEvent(QKeyEvent* e){
	 l_out->setText(e->text());
	 bool ok;
	 int val = e->text().toInt(&ok);
	 if(ok){
		 Q_EMIT(numericalKeyPressed(val));
	 }
	 else{
		switch (e->text().at(0).toLatin1())
		{
		case 'q':
			Q_EMIT(numericalKeyPressed(10));
			break;
		case 'w':
			Q_EMIT(numericalKeyPressed(11));
			break;
		case 'e':
			Q_EMIT(numericalKeyPressed(12));
			break;
		case 'r':
			Q_EMIT(numericalKeyPressed(13));
			break;
		case 't':
			Q_EMIT(numericalKeyPressed(14));
			break;
		case 'z':
			Q_EMIT(numericalKeyPressed(15));
			break;
		case 'u':
			Q_EMIT(numericalKeyPressed(16));
			break;
		case 'i':
			Q_EMIT(numericalKeyPressed(17));
			break;
		case 'o':
			Q_EMIT(numericalKeyPressed(18));
			break;
		case 'p':
			Q_EMIT(numericalKeyPressed(19));
			break;
		}
		//Q_EMIT(charKeyPressed(e->text().at(0).toAscii()));
	 }
 }

 void mainWindow::printInfo(std::string info){
	 QString text(info.c_str());
	 l_out->setText(text);
 }