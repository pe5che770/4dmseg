#pragma once
#include "glDisplayable.h"
#include "explainMeDefinitions.h"
#include <boost/shared_ptr.hpp>
#include <vector>
#ifdef CAPITALIZED_GL_HEADER_NAMES
#include <GL/GLU.h>
#else
#include <GL/glu.h>
#endif

class glPointCloud:
	public glDisplayable
{
private:
	QGLBuffer m_posBuffer, m_indexBuffer, m_colBuffer;
	//std::vector<GLfloat> data;
	std::vector<tuple3f> pos;
	std::vector<tuple3f> col;
	std::vector<int> ind;
	bool isUpToDate;
public:
	typedef boost::shared_ptr<glPointCloud> Ptr;

	glPointCloud(const explainMe::Cloud::ConstPtr & cloud, bool useColor);
	glPointCloud(const explainMe::Cloud::ConstPtr & cloud, std::vector<Eigen::Vector3f> & cols);
	glPointCloud(std::vector<pcl::PointXYZ> & ps, std::vector<pcl::PointXYZ> & cols);
	glPointCloud(std::vector<Eigen::Vector3f> & ps, std::vector<Eigen::Vector3f> & cols);
	~glPointCloud(void);

	void updateData(const explainMe::Cloud::ConstPtr & cloud, bool useColor = true);
	void updateData(std::vector<pcl::PointXYZ> & ps, std::vector<pcl::PointXYZ> & cols);
	void updateData(std::vector<Eigen::Vector3f> & ps, std::vector<Eigen::Vector3f> & cols);
	void updateData( const explainMe::Cloud::ConstPtr & cloud, std::vector<Eigen::Vector3f> & cols);

	virtual void glPrepare();
	virtual bool isGlPrepared();
	virtual void glUpdate();
	virtual bool isGlUpToDate();
	virtual void glDraw();

};

