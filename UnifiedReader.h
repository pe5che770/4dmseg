#pragma once

#include "CloudReader.h"
#include "Sensors.h"

class cudaTool;

class UnifiedReader:public CloudReader
{
	std::vector<float> depthFloatBuffer, normalFloatBuffer, buffer_f;
	RangeSensorDescription * d;
	explainMe::Cloud::Ptr currentCloud;
	std::vector<std::string> ir_col_files;
	std::vector<std::string> d_files;
	std::string datafolder;
	cudaTool * cuda;
	bool has_ir;
public:
	UnifiedReader(const std::string & folder);
	~UnifiedReader();

	RangeSensorDescription * getDescription(){
		return d;
	}
	virtual size_t numberOfFrames(){
		return d_files.size();
	}

	virtual bool loadFrame(int frame, bool cleanUp, float maxDist, bool bilateralFilter, float sig_z = 0.05f);

	virtual std::vector<std::string> fileNames(int frame);

	virtual explainMe::Cloud::Ptr & getPointCloud(){
		return currentCloud;
	}
};

