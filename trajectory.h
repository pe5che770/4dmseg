#pragma once
#include <vector>
#include "explainMeDefinitions.h"

//trajectory allows efficient push front push back.
//and well defined seed element.


template<typename T>
class twoSidedSet_R
{
	
	//int _firstFrame, _lastFrame;
	//int _seedFrame;
	int _seedCloud;
	int _seedIdx;
	T seed;
	std::vector<T> p_fw, p_bw;
public:
	twoSidedSet_R(void){
		/*_firstFrame = 0; _lastFrame = -1;*/ //_seedFrame = 0; 
		_seedCloud = -1;
		_seedIdx=-1;
	}
	twoSidedSet_R(T & p, int seedCloudIdx, int seedIdx){
		init(p, seedCloudIdx, seedIdx);
	}

	~twoSidedSet_R(void){

	}

	void init(T & p, int seedCloudIdx, int seedIdx){
		p_fw.clear(); p_bw.clear();
		seed = p;
		_seedCloud = seedCloudIdx;
		_seedIdx = seedIdx;
	}

	int firstCloud(){ return _seedCloud - p_bw.size(); }
	int lastCloud(){ return _seedCloud + p_fw.size(); }
	int seedCloud(){ return _seedCloud; }
	int originalPointIdx(){
		if (_seedIdx < 0){
			std::cout << "****ERROR: requesting seed index but was not initialized;";
			exit(1);
		}
		return _seedIdx;
	} //in NAN-free cloud
	//int seedIndex(){ return _seedIndex; } //in NAN-free cloud.

	int numEl(){
		return p_fw.size() + p_bw.size() +1;
	}

	T& seedEl(){
		return seed;
	}

	bool cloudInRange(int cloudIdx){
		return firstCloud() <= cloudIdx && lastCloud() >= cloudIdx;
	}

	void clear(){
		p_fw.resize(0);
		p_bw.resize(0);
	}

	void reserve(int fwd, int bwd){
		p_fw.reserve(fwd);
		p_bw.reserve(bwd);
	}

	void resize(int firstCloud, int lastCloud){
		if (firstCloud > _seedCloud || lastCloud < _seedCloud){
			throw std::runtime_error("Trajectory: invalid resize call.");
		}
		p_bw.resize(_seedCloud - firstCloud);
		p_fw.resize(lastCloud - _seedCloud);
	}

	void resize(int firstCloud, int lastCloud, T & el){
		if (firstCloud > _seedCloud || lastCloud < _seedCloud){
			throw std::runtime_error("Trajectory: invalid resize call.");
		}
		p_bw.resize(_seedCloud - firstCloud, el);
		p_fw.resize(lastCloud - _seedCloud, el);
	}

	void push_front(T & p){
		p_bw.push_back(p);
	}

	void push_back(T & p){
		p_fw.push_back(p);
	}

	void removeAllAfter(int cloud){
		if (cloud < _seedCloud){
			std::cout<<"Trajectory: invalid removeAllAfter call.";
			throw std::runtime_error("Trajectory: invalid removeAllAfter call.");
		}
		p_fw.resize(cloud - _seedCloud);
	}

	void removeAllBefore(int cloud){
		if (cloud > _seedCloud){
			std::cout <<"Trajectory: invalid removeAllBefore call";
			throw std::runtime_error("Trajectory: invalid removeAllBefore call.");
		}
		p_bw.resize(_seedCloud - cloud);
	}

	T & back(){
		return p_fw.back();
	}

	T & front(){
		return p_bw.back();
	}

	//frame should be >= firstFrame and <= lastFrame
	T & operator []  (int cloud){

		if (cloud < firstCloud() || cloud > lastCloud()){
			std::cout << "Trajectory: index out of bounds. (" << firstCloud() << ", " << lastCloud() << ") vs " << cloud;
			throw std::runtime_error("Trajectory: index out of bounds.");
		}
		return (cloud < _seedCloud ?
					p_bw[(_seedCloud - cloud) - 1] :
				(cloud > _seedCloud ?
					p_fw[(cloud - _seedCloud) - 1] :
					seed));
	}

};

/*template < typename T>
class parameterizedSet
{
	int first_index;
	std::vector<Eigen::Matrix4f> data;
public:
	parameterizedSet(int first, int last){

	}
	~parameterizedSet(){

	}
};*/

typedef twoSidedSet_R<pcl::PointXYZINormal> trajectory_R;
//typedef twoSidedSet<Eigen::Matrix4f> alignmentSet;
