#include "glDebugging.h"
#include <QtOpenGL/qgl.h>
#ifdef CAPITALIZED_GL_HEADER_NAMES
#include <GL/GLU.h>
#else
#include <GL/glu.h>
#endif
#include <QDebug>
#include <iostream>
#include <string>

glDebugging::glDebugging(void)
{
}


glDebugging::~glDebugging(void)
{
}

bool glDebugging::didIDoWrong()
{
	GLenum errCode;
	std::string errString;
	errCode = glGetError();
	if (errCode != GL_NO_ERROR)
	{
		
		if(errCode == 1286){
			errString = "Invalid Framebuffer Operation";
		}
		else{
			char* errStr = (char *) gluErrorString(errCode);
			errString = (errStr == NULL ? "gluErrorString is NULL" : std::string( (char*) errStr));
		}
		qWarning() << "*** GLError: " << errString.c_str();
		std::cout << "*** GLError: " << errString.c_str();
		return true;
	}

	return false;
}
