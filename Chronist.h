#pragma once
#include <qobject.h>
#include <string>
#include "explainMe_algorithm_withFeatures.h"


class algorithmStateRenderer;

class Chronist:public QObject
{
	Q_OBJECT

private:
	//demo_global_features * renderer;
	algorithmStateRenderer * renderer;
	explainMe_algorithm_withFeatures::Ptr algToDocument;


public:
	typedef std::shared_ptr<Chronist> Ptr;
	//Chronist(demo_global_features * viewer, const std::string & out_folder);
	Chronist(explainMe_algorithm_withFeatures::Ptr alg);
	~Chronist();


	static void store(std::string file, std::vector<trajectory_R> & target_traj, explainMe::MatrixXf_rm & target_seg_weights);
	static void load(std::string file, std::vector<trajectory_R> & target_traj, explainMe::MatrixXf_rm & target_seg_weights);

	static void store(std::string file, std::vector<motion> & alignments);
	static bool load(std::string file, std::vector<std::vector<Eigen::Matrix4f>> & alignments);

	static void store_v2(std::string folder, std::string file, std::vector<trajectory_R> & target_traj, explainMe::MatrixXf_rm & target_seg_weights, std::vector<motion> & alignments);
	static void load_v2(const std::string & segfile, 
		const std::string & mofile, 
		std::vector<trajectory_R> & target_traj, explainMe::MatrixXf_rm & target_seg_weights, std::vector<std::vector<Eigen::Matrix4f>> & alignments);

private:
	void dump_dw_dt_stats_in_m_File(const std::string & folder,
		std::vector<explainMe_algorithm_withFeatures::RowVectorXf> & supports,
		std::vector<explainMe_algorithm_withFeatures::RowVectorXf> & shift_abs,
		std::vector<Eigen::MatrixXf> & shift_mats);
public Q_SLOTS:
	//refactored
	void document_denseWeights(const std::string & folder);
	void document_denseperPixelValues(const std::string & folder, std::vector<std::vector<float>>& vals, float scale);
	void document_w_as_hyp_probs(const std::string &);
	void document_insecurityOfW(const std::string &);
	void document_alignmentSets(const std::string & folder);
	void document_alignmentSets_byMovingStuff(const std::string & folder);
	void document_alignmentSets_byMovingStuff(const std::string & folder, int hyp);
		
	void document_weight(const std::string & folder, int hyp);
	//void document_stats(const std::string & folder);
		//not refactored
	void document_dw_dt(const std::string & folder);

	void document_seg_png_masks(const std::string & foldder);

};