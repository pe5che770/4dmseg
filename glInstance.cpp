#include "glInstance.h"



glInstance::glInstance(glDisplayable * other):
	glDisplayable("","")
{
	this->myInstance = other;
	instance2World.setToIdentity();
}


glInstance::~glInstance(void)
{
	delete myInstance;
}

void glInstance::mult(QMatrix4x4 & other)
{
	this->instance2World = other *this->instance2World ;
}

void glInstance::glPrepare()
{
	this->myInstance->glPrepare();
}

bool glInstance::isGlPrepared()
{
	return this->myInstance->isGlPrepared();
}

void glInstance::glUpdate()
{
	this->myInstance->glUpdate();
}

bool glInstance::isGlUpToDate()
{
	return this->myInstance->isGlUpToDate();
}

void glInstance::glDraw()
{
	this->myInstance->glDraw();
}

QMatrix4x4 glInstance::getModel2World()
{
	return instance2World * myInstance->getModel2World();
}

QGLShaderProgram & glInstance::get_m_shader()
{
	return myInstance->get_m_shader();
}

QMatrix4x4 glInstance::getInstance2World()
{
	return instance2World;
}

void glInstance::setInstance2World( QMatrix4x4 & other )
{
	instance2World = other;
}
