#pragma once
#include "explainMeDefinitions.h"
#include "trajectory.h"
#include "Sensors.h"
#include "mySimpleKdTree.h"

class BundleAdjustment
{
	typedef Eigen::Matrix<float,6,6> Matrix6f;
	typedef Eigen::Matrix<float,6,1> Vector6f;

	
public:
	struct LM_params{
		float lambda;
		float last_error;
		float avg_error;
		LM_params(){
			lambda = 100;
			last_error = -1;//std::numeric_limits<float>::max();
			avg_error = -1;
		}
	};
	BundleAdjustment(void);
	~BundleAdjustment(void);


	/*void pointToPlane_Bundle_step(
			std::vector<explainMe::Cloud::Ptr > &clouds,
			std::vector<Eigen::Matrix4f> &alignments,
			std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
			int referenceCloud, //acts as reference frame
			int numIterations);*/

	
	
	void pointToPlane_Bundle_step_r(
			std::vector<explainMe::Cloud::Ptr > &clouds,
			std::vector<Eigen::Matrix4f> &alignments,
			std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
			int referenceCloud, //acts as reference frame
			int numIterations);


	template<typename Eigen_T_weights>
	static void pointToPlane_Bundle_step(std::vector<trajectory_R> & trajs, 
		int numClouds, int ref_cloud,
		Eigen_T_weights & weights, int hyp, std::vector<Eigen::Matrix4f> &alignments, float p_1 = 1);

	template<typename Eigen_T_weights>
	void pointToPlane_Bundle_step_r(
		std::vector<explainMe::Cloud::Ptr > &clouds,
		//std::vector<std::vector<Vectornf >> &weights,
		std::vector<Eigen_T_weights> & weights,
		int wight_idx,
		std::vector<Eigen::Matrix4f> &alignments,
		std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
		int referenceCloud, //acts as reference frame
		int numIterations,
		float alpha_point2plane = 1.f,
		float alpha_point2point = 0.f,
		float cullind_dist = 0.15f
		);



	template<typename Eigen_T_weights>
	void pointToPlane_Bundle_step_color(
		std::vector<explainMe::Cloud::Ptr > &clouds,
		std::vector<Eigen_T_weights> &weights,
		int weight_idx,
		std::vector<Eigen::Matrix4f> &alignments,
		std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
		int referenceCloud, //acts as reference frame
		int numIterations,
		float alpha_point2plane,
		float alpha_point2point,
		float alpha_color,
		float culling_dist
		);


	template<typename Eigen_T_weights>
	void pointToPlane_Bundle_step_of(
		std::vector<explainMe::Cloud::Ptr > &clouds,
		std::vector<std::vector<Eigen::MatrixXi>> &I,
		std::vector<std::vector<Eigen::MatrixXi>> &dI_dx,
		std::vector<std::vector<Eigen::MatrixXi>> &dI_dy,
		std::vector<std::vector<Eigen::MatrixXf>> &smoothedDepth,
		bool usedDepthAsIntensity,
		int pyrLevel,
		std::vector<Eigen_T_weights> &weights,
		int weight_idx,
		std::vector<Eigen::Matrix4f> &alignments,
		std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
		int referenceCloud, //acts as reference frame
		int numIterations,
		float alpha_point2plane,
		float alpha_point2point,
		float alpha_of,
		float alpha_color,
		float culling_dist,
		RangeSensorDescription & d,
		LM_params & params //will be updated
		);


	template<typename Eigen_T_weights, typename Tree_type, typename Tree_query_type, typename Tree_nn_finder_type>
	void LM_BA_fast(
		std::vector<explainMe::Cloud::Ptr > &clouds,
		std::vector<Tree_type> & allTrees,
		std::vector<Eigen_T_weights> &weights,
		int weight_idx,
		std::vector<Eigen::Matrix4f> &alignments,
		std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
		int referenceCloud, //acts as reference frame
		int numIterations,
		float alpha_point2plane,
		float alpha_point2point,
		float alpha_color,
		float culling_dist,
		RangeSensorDescription & d,
		LM_params & params, //will be updated
		int numPointsToSample = 2500);

	template<typename Eigen_T_weights>
	void pointToPlane_Bundle_step_LM(
		std::vector<explainMe::Cloud::Ptr > &clouds,
		std::vector<Eigen_T_weights> &weights,
		int weight_idx,
		std::vector<Eigen::Matrix4f> &alignments,
		std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
		int referenceCloud, //acts as reference frame
		int numIterations,
		float alpha_point2plane,
		float alpha_point2point,
		float alpha_color,
		float culling_dist,
		RangeSensorDescription & d,
		LM_params & params //will be updated
		);

	//just for two clouds, to get it working,,,
	void pointToPlane_Bundle_step_of(
	explainMe::Cloud::Ptr cloud1,explainMe::Cloud::Ptr cloud2,
	Eigen::MatrixXi &I1,Eigen::MatrixXi &I2,
	Eigen::MatrixXi &dI1_dx, Eigen::MatrixXi &dI1_dy,
	Eigen::MatrixXi &dI2_dx, Eigen::MatrixXi &dI2_dy,
	int pyr_level,
	Eigen::Matrix4f &alignment_1_to2,
	float alpha_point2plane,
	float alpha_point2point,
	float alpha_of,
	float alpha_color,
	float culling_dist,
	RangeSensorDescription & d,
	std::vector<Eigen::Matrix<float,2,6>> & debug_info);

};

