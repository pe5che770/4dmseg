#include "crf_segmentation.h"
#include "cudaDataterm.h"
#include "cudaDatatermTools.h"
#include "StopWatch.h"
#include <functional>
#include <fstream>

//actually: with the merge pass it is better not to use merging
#define DEACTIVATE_MERGING_HEURISTIC_AND_GET_BAD_RESULTS

//not using eigen::Map because I want to access clouds in the same manner.
template <typename T, typename container>
class matrix
{
	//std::vector<T>
	container & data;
	bool horizFirst;
	bool horizForward;
	bool vertForward;
	int accessType;
public:
	int w, h;
	matrix(container & dat, int width, int height) : data(dat), w(width), h(height)
	{
		setAccessPattern(true, true, true);
	}

	void setAccessPattern(bool horizFirst_, bool horizForward_, bool vertForward_){
		horizFirst = horizFirst_;  horizForward = horizForward_; vertForward = vertForward_;
		accessType = (horizFirst_ ? 4:0) + (horizForward_ ? 2:0) + (vertForward_ ? 1:0);
	}
	void setAccessPattern(int pattern_id){
		accessType =
			(pattern_id > 7 ? 7 :
			pattern_id < 0 ? 0 :
			pattern_id);
		horizFirst = ((accessType & 4) != 0);
		horizForward = ((accessType & 2) != 0);
		vertForward = ((accessType & 1) != 0);

	}

	int dim1(){
		return horizFirst ? w : h;
	}

	int dim2(){
		return horizFirst ? h : w;
	}

	container getData(){
		return data;
	}

	//what could be wrong: assuming width major enumeration...
	T & operator ()(int i, int j){
		switch (accessType)
		{
		case 7://0b111: //horiz first: dim1 is horizontal, i is the horizontal coordinate in 0...., w-1, j 0.....h-1 
			return data[i + j*w];
		case 6:// 0b110://vert backward
			return data[i + (h - 1 - j)*w];
		case 5://0b101://horiz backward
			return data[(w - 1 - i) + j*w];
		case 4://0b100://both backward
			return data[(w - 1 - i) + (h - 1 - j)*w];
		case 3:// 0b011: //vert first: dim1 is vertical, i in 0...h-1, j = 0...w-1
			return data[i*w + j];
		case 2://0b010:
			return data[(h - 1 - i)*w + j];
		case 1://0b001:
			return data[i*w + (w - 1 - j)];
		case 0://0b000:
			return data[(h - 1 - i)*w + (w - 1 - j)];
		default://7
			return data[i + j*w];
		}
	}

	void toSpaceDelimFile(std::string filename, std::function<std::string(T)> entry_to_string){
		std::cout << "Outputting matrix to " << filename;
		std::ofstream outfile(filename);
		if (!outfile.is_open()){
			std::cout << "Could not open file!";
		}
		for (int i = 0; i < dim1(); i++){
			for (int j = 0; j < dim2(); j++){
				outfile << entry_to_string(this->operator()(i, j)) << "\t";
			}
			outfile << "\n";
		}
	}

	void cwise_combine_with(matrix<T,container> & other, std::function<void(T, T&)> combine){
		for (int i = 0; i < dim1(); i++){
			for (int j = 0; j < dim2(); j++){
				combine(other(i,j),this->operator()(i, j));
			}
		}
	}

	void cwise_apply(std::function<void(T&)> apply){
		for (int i = 0; i < dim1(); i++){
			for (int j = 0; j < dim2(); j++){
				apply(this->operator()(i, j));
			}
		}
	}

	matrix<T, container> & operator=(const matrix<T, container>& other) // copy assignment
	{
		if (this != &other) { // self-assignment check expected
			this->data = other.data;
		}
		return *this;
	}



};




class recursiveFilter
{
private:
	float sigma;
	float sqrt2;
	//float a,b. set a = 1, b = e
public:
	recursiveFilter(float sigma_):sigma(sigma_){
		sigma = sigma_;
		sqrt2 = std::sqrt(2);
	}

	/*void setSigma(float sig){
		//sigma = -std::log(b) / std::sqrt(2) * sig;
	}*/
	//assuming a constant filter with samples spaced at distance brdyDist.
	float boundaryValue(float const_, float bdryDist){
		//return const_*a / (1 - std::pow(b, bdryDist / sigma));
		return const_ / (1 - std::exp(-bdryDist*sqrt2/sigma));
	}
	/*float boundaryValue(float const_){
		return const_*a / (1 - b);
	}

	float operator()(float in_i, float last_val)
	{
		return a* in_i + b* last_val;
	}*/

	void setSigma(float sig){
		sigma = sig;
	}
	float estimatedEnergyScale2D(float avgDist){
		return boundaryValue(boundaryValue(1, avgDist), avgDist) * 2 + 2 * boundaryValue(1, avgDist);
	}

	float operator()(float in_i, float last_val, float dist)
	{
		//return a* in_i + std::pow<float>(b, dist / sigma)* last_val;
		return in_i + std::exp(-dist *sqrt2/sigma)* last_val;
	}

	friend std::ostream& operator<< (std::ostream& stream, const recursiveFilter& filter) {
		stream << "Recursive kernel with variance " << filter.sigma << ".";
		return stream;
	}
};


crf_segmentation::crf_segmentation(explainMe_algorithm_withFeatures::Ptr algo, int numIt)
{
	alg = algo;
	num_crf_iterations = numIt;
}


crf_segmentation::~crf_segmentation()
{
}



void crf_segmentation::copy_cpu_dense_segweights_to_gpu(cudaDataterm * cuda_dev, std::vector<explainMe::MatrixXf_rm> & perCloudVals, int hyp, std::vector<float> & buffer){
	auto & clouds_as_grids = alg->allClouds_asGrids;
	for (int frame = 0; frame < clouds_as_grids.size(); frame++) {
		int idx = 0, out_idx = 0;
		//crf needs dense values on a grid, i stored them unorganized....
		//So fill them in
		for (auto it = alg->allClouds_asGrids[frame]->begin(); it != alg->allClouds_asGrids[frame]->end(); it++, idx++)
		{
			if (it->z != 0 && it->z * 0 == 0 && it->getNormalVector3fMap().norm() > 0.99){
				buffer[idx] = perCloudVals[frame](out_idx, hyp);
				out_idx++;
			}
			else{
				buffer[idx] = 0;
			}
		}
		cuda_dev->setQ(frame, buffer);
	}
}

void crf_segmentation::filter_sparse2dense_spatial_crf_allHyp_but_store_on_cpu(cudaDataterm * cuda_dev, float sigma_spatial, float lambda_spatial, float simga_factor,
	std::vector<explainMe::MatrixXf_rm> & where_to_store,
	std::vector<float> & buffer,
	StopWatch & spatial_filter, StopWatch & spatial_cpy, bool doNormalization, int numSurroundingFrames)
{
	int numHyp = alg->motions.numHypotheses();
	spatial_cpy.restart();

	//important! update labeling to decide which motion is used to interpolate where.
	cuda_dev->updateLabelsForInterpolationFormMultipleFrames();

	for (int frame = 0; frame < alg->allClouds.size(); frame++){
		
		//spatial_filter.restart();
		//alternative without mutliple frames
//		auto & sp_crf_result = cuda_dev->filter_sparse2dense_spatial_crf_oneFrame(frame, sigma_spatial, lambda_spatial, doNormalization);
		//Alternative for frommultiple frames
		auto & sp_crf_result = cuda_dev->interpolate_sparse2dense_oneFrame_fromMultipleFrames(frame, sigma_spatial, simga_factor, numSurroundingFrames);
		//spatial_filter.stop();
		for (int hyp = 0; hyp < numHyp; hyp++){
			//version without multiple frames
//			sp_crf_result.getAndNormalize(hyp, buffer, doNormalization);
//			Alternative for frommultiple frames 
			sp_crf_result.getAndNormalize(hyp, buffer, false);

			int idx = 0, out_idx = 0;
			//crf object returns per grid values but the values are only stored for valid non nan points---
			//So fill it in
			for (auto it = alg->allClouds_asGrids[frame]->begin(); it != alg->allClouds_asGrids[frame]->end(); it++, idx++)
			{
				if (it->z != 0 && it->z * 0 == 0 && it->getNormalVector3fMap().norm() > 0.99){
					where_to_store[frame](out_idx, hyp) = buffer[idx];
					out_idx++;
				}
			}
		}
	}
	spatial_cpy.stop();
}

void integralSimilarity_frustum(std::vector<motion> & als, std::vector<std::vector<float>> & compatibility){
	//sig should be related to how accurately I can compute alignments.
	float sig = 0.01;

	float far = 5, near = 0.5f;
	//at  one m dist
	float top= 1, left=-1, right=1, bottom=-1;
	float delta_sqr;
	Eigen::Vector3f delta, rl_tb_1, abc;
	Eigen::Matrix3f diff; float integralDifference;
	Eigen::Matrix3f weights;
	weights << 1.0 / 3, 1.0 / 4, 1.0 / 2,
		1.0 / 4, 1.0 / 3, 1.0 / 2,
		1.0 / 2, 1.0 / 2, 1;

	float frustumVolume = (top - bottom)*(far-near)*(right - left)*(far-near)*(far - near)/3;
	
	compatibility.clear();
	compatibility.resize(als.size());
	for (int i = 0; i < als.size(); i++){
		for (int j = 0; j < als.size(); j++){
			auto & set1 = als[i];
			auto & set2 = als[j];
			//use max
			float mean = 0, max = 0;
			for (int al = 0; al < set1.size(); al++){
				auto & A = set1[al];
				auto & B = set2[al];
				delta = B.topRightCorner<3, 1>() - A.topRightCorner<3, 1>();
				delta_sqr = delta.squaredNorm();

				diff = B.topLeftCorner<3, 3>() - A.topLeftCorner<3, 3>();
				abc = diff.transpose()*delta;

				diff = diff.transpose() * diff;
				diff = diff.array()* weights.array();

				rl_tb_1 << (right - left), (top - bottom), 1;
				integralDifference = std::pow(far - near, 5) / 5 * (top - bottom)*(right - left) * rl_tb_1.transpose() * diff * rl_tb_1;
				integralDifference += delta_sqr * std::pow(far - near, 3)/3*(top - bottom) *(right - left)
					+ std::pow(far - near, 4) / 4 * (top - bottom) *(right - left) * (abc.x() * (right-left) + abc.y()*(top-bottom) + 2*abc.z());

				integralDifference /= frustumVolume;

				max = (integralDifference > max ? integralDifference : max);
				mean += integralDifference;
			}
			//interpretation: should be 1 for full compatibility and zero else.
			//usually simplest compatibility: [x_i neq x_j]. Or 1 - [x_i neq x_j], as I formulate it, see notes.

			//the max is the compatibility under maximal termporal consistency. But in fact during the crf iteration 
			//the temporal support for a decsision starts small and through propagation gets larger.
			//As an alternative I can compute a compatibility at a higher resoultion, in extreme case pixel wise.
			compatibility[i].push_back(exp(-max / sig));

			//compatibility[i].push_back(exp(-max / sig)*(j<i ? 0.5:1));
			//if there is a j < i which is large: set entry i small
			//e.g. distribute the weight to earlier labels: if a part was given to one subtract it.
						
			
			//asymmetric compatibilities more compatible to smaller labels.
			//compatibility[i].push_back(exp(-max / (j<i ? 2 * sig : sig)));
			//very asymmetric.
			//compatibility[i].push_back(exp(-max / sig) *(j>=i ?  2 : 1));
			//compatibility[i].push_back(exp(-max / sig) *(j >= i ? 20 : 1)); //encodes how much j will help i by not allowing lower to help upper: bias towards compatible lable with lowest index
		}
	}

	//could also directly remove motions that are too similar. Could speedup a lot (if o.5 of the motions are redundant: 2x)

	//interprete it as a redistribution, not filtering. That is
	/*for (int j = 0; j < als.size(); j++){
		float sum = 0;
		for (int i = 0; i < als.size(); i++){
			sum+=compatibility[i][j];
		}
		for (int i = 0; i < als.size(); i++){
			compatibility[i][j]/=sum;
		}
	}*/
	for (int i = 0; i < als.size(); i++){
		float sum = 0;
		for (int j = 0; j < als.size(); j++){
			sum += compatibility[i][j];
		}
		for (int j = 0; j < als.size(); j++){
			compatibility[i][j] /= sum;
		}
	}


	//let compatible guys hate themselves
	//bias in favor of lower indeces => to handle ambiguities
	for (int i = 0; i < als.size(); i++){
		float sum = 0;
		for (int j = 0; j < i; j++){
			sum += compatibility[i][j];
		}
		//unintended huge penalty
		/*for (int j = 0; j < als.size(); j++){
			compatibility[i][j] *= (1-sum);
		}*/
		compatibility[i][i] *= (1 - sum);
	}


}


void computeMergesAsCompatibilitiesAndKillSmall(float kill_threshold, cudaDataterm * cuda_dev,
	explainMe::MatrixXf_rm & out, 
	std::vector<std::vector<float>> & Q_sparse, 
	std::vector<std::vector<float>> & compatibility)
{
	int numHyp = out.cols();
	Eigen::MatrixXf compat(numHyp, numHyp);
	compat.setIdentity();

	cuda_dev->getSparseCRFResult(Q_sparse);
	for (int hyp = 0; hyp < numHyp; hyp++){
		for (int j = 0; j < Q_sparse[hyp].size(); j++){
			out(j, hyp) = Q_sparse[hyp][j];

		}
	}
		

	/////////////////////////////////////////////
	//decide on duplicate motions.
	Eigen::MatrixXf costs(out.cols(), out.cols());
	Eigen::MatrixXf allcosts(out.cols(), out.cols());
	Eigen::VectorXf winnerPs = out.rowwise().maxCoeff();
	Eigen::VectorXf hard_support(out.cols());
	//compute merge costs and segment statistics
	for (int hi = 0; hi < numHyp; hi++){

		Eigen::VectorXf labeled_i = (out.col(hi).array() == winnerPs.array()).select(out.col(hi), 0);
		int num = (out.col(hi).array() == winnerPs.array()).count();
		hard_support(hi) = num;
		std::cout << "hi has " << num << "sparse points\n\t";
		for (int hj = 0; hj < numHyp; hj++){
			costs(hi, hj) = std::numeric_limits<float>::infinity();
			allcosts(hi, hj) = 0;

			//compatibility[hi][hj] = 0;
			if (hi == hj){
				//compatibility[hi][hi] = 1;
				continue;
			}
			//only consider the guys that will be labeled hi. This gives average per point costs; can control here relative energy increase.
			//						(cho0se j even thought i chose i, has to be smaller 0.5)
			//								P(choose i | either i  or j and chose i)
			float p_hi_is_hj = 2 * (1 - (labeled_i.dot((out.col(hj) + out.col(hi)).cwiseInverse()) / num));

			//cost of merger: 
			Eigen::VectorXf label_i_as_j = (out.col(hi).array() == winnerPs.array()).select(out.col(hj), 0);
			allcosts(hi, hj) = (labeled_i - label_i_as_j).sum();
			std::cout << p_hi_is_hj<< " ";
			if (p_hi_is_hj > 0.9){
				//std::cout << " Could merge: " << hi << "to " << hj << "\tpval: " << p_hi_is_hj << "\tcosts: ";
				costs(hi, hj) = allcosts(hi, hj);
			}
		}
	}
	std::cout << "\n";
	//merge
	int mergei, toj;
	while (costs.minCoeff(&mergei, &toj) * 0 == 0){
		//std::cout << costs << "\n";
		std::cout << "Merge " << mergei << " into " << toj << "for " << costs(mergei, toj) << "\n";
		costs.col(mergei).fill(std::numeric_limits<float>::infinity());
		costs.row(mergei).fill(std::numeric_limits<float>::infinity());
		costs.row(toj) += allcosts.row(mergei); //update costs...

		compat.row(toj) += compat.row(mergei);
		compat.row(mergei).setZero();
		/*for (int hi = 0; hi < numHyp; hi++){
			compatibility[toj][hi] += compatibility[mergei][hi];
			compatibility[mergei][hi] = 0;
		}*/

		//out.col(toj) += out.col(mergei); //this is very very bad in unconverged regions!
		out.col(toj) = (out.col(toj).array() > out.col(mergei).array()).select(out.col(toj), out.col(mergei));
		out.col(mergei).fill(0);
	}
	//eliminate small segments
	hard_support = compat * hard_support;
	for (int hi = 0; hi < numHyp; hi++){
		if (hard_support(hi) < kill_threshold/*50 * 0.005 * 16000 /*yet hardcoded kill_threshold*/ && compat(hi, hi) >0){
			compat.row(hi).setZero();
			out.col(hi).setConstant(1e-5f/numHyp);
		}
	}

	//renormalize
	for (int i = 0; i < out.rows(); i++){
		out.row(i) *= 1.0 / out.row(i).sum();
	}
	/*for (int hi = 0; hi < numHyp; hi++){
		for (int hj = 0; hj < numHyp; hj++){
			std::cout << compatibility[hi][hj] << " ";
		}
		std::cout << "\n";
	}*/
	//std::cout << "support sizes after merge: " << hard_support << "\n\n";
	//std::cout << compat << "\n";
	
	//return compatibility.
	for (int hi = 0; hi < numHyp; hi++){
		for (int hj = 0; hj < numHyp; hj++){
			compatibility[hi][hj] = compat(hi, hj);
		}
	}
	//out = out.rowwise().sum().cwiseInverse().asDiagonal() * out;
	//std::cout << "Are this still kprobabilites?" << out.maxCoeff() << ", rows sum up " << out.rowwise().sum().mean() << ", max sum: " << out.rowwise().sum().maxCoeff() << "\n";
}

float meanNoise(explainMe::Cloud::Ptr cloud){
	float mean = 0;
	int num = 0;
	for (auto &c : cloud->points){
		if (c.z > 0.01){
			mean += c.curvature;
			num++;
		}
	}
	return mean / num;
}

float meanNoise(std::vector<explainMe::Cloud::Ptr> clouds){
	float mean = 0;
	for (auto & c : clouds){
		mean += meanNoise(c);
	}
	return mean / clouds.size();
}


float  crf_segmentation::energyI(
	int cuda_dev_channel,
	std::vector<int> & mergedLabels_lab,
	cudaDataterm * cuda_dev,
	std::vector<float> & buffer/* buffer*/,
	float sigma_spatial,
	float sigma_t,
	float sigma_sp_factor,
	std::vector<int> & sparse_labels)
{
	cuda_dev->resetSparseValues(cuda_dev_channel);
	copyCombinedDenseResults_alg2Gpu(mergedLabels_lab, buffer, cuda_dev);
	cuda_dev->filter_dense2dense_allFrames_currentData(sigma_spatial, sigma_sp_factor);
	cuda_dev->filter_dense2sparse_temporal_crf_oneHyp_no_Normalization(cuda_dev_channel, sigma_t);
	return cuda_dev->getSummedUpSparseValues(cuda_dev_channel, mergedLabels_lab, sparse_labels);
}

float  crf_segmentation::energyII(
	int lab,
	std::vector<int> & mergedLabels_lab,
	cudaDataterm * cuda_dev,
	std::vector<float> & buffer/* buffer*/,
	float sigma_spatial,
	float sigma_t,
	float sigma_sp_factor,
	std::vector<std::vector<float>> * per_pixel_energy,
	bool relaxed
	)
{

	if (per_pixel_energy != NULL){
		int cld_idx = 0;
		for (auto & vec : *per_pixel_energy){
			vec.resize(alg->allClouds[cld_idx]->size(),0);
			cld_idx++;
		}
	}

	float currEnergy=0;
	//std::cout << "energyII ...";
	cuda_dev->resetSparseValues(lab);
	if (relaxed){
		for (int frame = 0; frame < alg->allClouds_asGrids.size(); frame++){
			cuda_dev->setQ(frame, 1);
		}
	}
	else{
		copyCombinedDenseResults_alg2Gpu(mergedLabels_lab, buffer, cuda_dev);
	}
	cuda_dev->filter_dense2sparse_temporal_crf_oneHyp_no_Normalization(lab, sigma_t);
	cuda_dev->normalize_sparse_by_weights();
	cuda_dev->filter_sparse2dense_allFrames_currentData(lab, sigma_spatial, sigma_sp_factor);

	for (int frame = 0; frame < alg->allClouds.size(); frame++){
		//auto & sp_crf_result = cuda_dev->interpolate_sparse2dense_spatial_oneFrame_oneHyp(frame, lab, sigma_spatial);//interpolate_sparse2dense_oneFrame_fromMultipleFrames(frame, sigma_spatial, 5);
		//sp_crf_result.getAndNormalize(lab, buffer, false);
		cuda_dev->getCurrentDenseQBuffer(frame, buffer);
		//some uglyness due to the stupidity of keeping labels only for valid points.
		//sum up values.
		float tmp_e = 0;
		int idx = 0, valid_point_idx = 0, tmp_label;
		for (auto it = alg->allClouds_asGrids[frame]->begin(); it != alg->allClouds_asGrids[frame]->end(); it++, idx++)
		{
			if (it->z != 0 && it->z * 0 == 0 && it->getNormalVector3fMap().norm() > 0.99){
				alg->denseSegmentation_allClouds[frame].row(valid_point_idx).maxCoeff(&tmp_label);
				for (int label : mergedLabels_lab)
				{
					if (tmp_label == label){
						tmp_e += buffer[idx];

						if (per_pixel_energy != NULL){
							(*per_pixel_energy)[frame][valid_point_idx] = buffer[idx];
						}
						break;
					}
				}
				valid_point_idx++;
			}
		}
		//std::cout << tmp_e << "\n";
		currEnergy += tmp_e;
	}
	return currEnergy;
}


float  crf_segmentation::energy(
	int cuda_dev_channel,
	std::vector<int> & mergedLabels_lab,
	cudaDataterm * cuda_dev,
	std::vector<float> & buffer/* buffer*/,
	float sigma_spatial,
	float sigma_t,
	float sigma_sp_factor,
	std::vector<int> & sparse_labels)
{
	return energyII(cuda_dev_channel, mergedLabels_lab, cuda_dev,
		buffer/* buffer*/,
		sigma_spatial,
		sigma_t, sigma_sp_factor
		);// , sparse_labels);
}


std::vector<std::vector<int>> crf_segmentation::minimizeEnergyByMergingLabels(
	cudaDataterm * cuda_dev, 
	std::vector<float> & buffer/* buffer*/, 
	float sigma_spatial, 
	float sigma_t,
	float sigma_sp_factor,
	std::vector<int> & sparse_labels,
	explainMe::MatrixXf_rm & sparse_seg_to_fuse,
	float hypothesis_cost_factor,
	std::vector<std::pair<int,int>> mergeStrategy,
	bool set2Zero_instead_fuse
	)
{
	std::cout << "Minimizing Energy through merging...";
	
	int numLabels = alg->motions.numHypotheses();
	float total_energy_old = 0, newEnergy = 0, currEnergy;

	//greedy approach: try to merge into "low" labels before "high"
	if (mergeStrategy.size() == 0){
		for (int i = 0; i < numLabels; i++)
		for (int j = 0; j < numLabels; j++)
		{
			mergeStrategy.push_back(std::pair<int, int>(i, j));
		}
	}

	std::vector<std::vector<int>> mergedLabels(numLabels, std::vector<int>());
	std::vector<float> energy_per_labelGroup;

	//1. how to compute the energy: compute it once for each label.
	cuda_dev->resetSparseValues();
	std::cout << std::setw(6);
	for (int lab = 0; lab < numLabels; lab++){
		mergedLabels[lab].push_back(lab);
		
		
		//energy variant I: spatial filtering first, then temporal filtering
		/*copyCombinedDenseResults_alg2Gpu(mergedLabels[lab], buffer, cuda_dev);
		cuda_dev->filter_dense2dense_allFrames_currentData(sigma_spatial);
		cuda_dev->filter_dense2sparse_temporal_crf_oneHyp_no_Normalization(lab, sigma_t);
		currEnergy = cuda_dev->getSummedUpSparseValues(lab, mergedLabels[lab], sparse_labels);*/
		currEnergy = energy(lab, mergedLabels[lab], cuda_dev,
			buffer/* buffer*/,
			sigma_spatial,
			sigma_t, sigma_sp_factor,
			sparse_labels);

		//energy variant II:
		//load labeling. (unfiltered)
		//do temporal filtering. Take the sum. Note that this has a totally different scale than the above.
		

		//variant III,IV... separate both... no clue what that would be. crfed temporal filtering (correct scale) with normal spatial filtering afterwards
		//and/or crfed spatial filtering (correct scale) with temporal filtering afterwards
		
		std::cout << "Label(" << lab << "):" << currEnergy << "\n";
		
		energy_per_labelGroup.push_back(currEnergy);
		total_energy_old += energy_per_labelGroup.back();
	}
	
	
	//to compute the energy I actually need only two labels loaded: merged label & not merged label.

	bool redo_last_l = false, redid_last_l = false;
	//greedy approach: try to merge into "low" labels before "high"
	for (int l = 0; l < numLabels; l++){
	//int l = 2;{
		if (redo_last_l && !redid_last_l)
		{
			l--;
			redid_last_l = true;
		}
		else{
			redid_last_l = false;
		}redo_last_l = false;
		
		for (int l2 = 0; l2 < numLabels; l2++){
	//for (auto & pr : mergeStrategy){{
	//		int l = pr.first; int l2 = pr.second;
			//if the label l2 already was merged
			if (mergedLabels[l2].size() == 0 || mergedLabels[l].size() == 0 ||l == l2){
				continue;
			}
			std::vector<int> oldlabelsl = mergedLabels[l];//newlabels[l2];
			std::vector<int> oldlabelsl2 = mergedLabels[l2];//newlabels[l2];

			//merge the two?
			mergedLabels[l].insert(mergedLabels[l].end(),mergedLabels[l2].begin(), mergedLabels[l2].end());
			mergedLabels[l2].clear();
			
			/*cuda_dev->resetSparseValues(l);
			copyCombinedDenseResults_alg2Gpu(mergedLabels[l], buffer, cuda_dev);
			//std::cout << "copyied,...";
			//other way is faster to implement, just to try this out.
			cuda_dev->filter_dense2dense_allFrames_currentData(sigma_spatial);
			cuda_dev->filter_dense2sparse_temporal_crf_oneHyp_no_Normalization(l, sigma_t);
			newEnergy = cuda_dev->getSummedUpSparseValues(l,mergedLabels[l], sparse_labels);*/
			newEnergy = energy(l, mergedLabels[l], cuda_dev,
				buffer/* buffer*/,
				sigma_spatial,
				sigma_t, sigma_sp_factor,
				sparse_labels);
			
			float oldEnergy = energy_per_labelGroup[l] + energy_per_labelGroup[l2];// *hypothesis_cost_factor;
			std::cout << l << "," << l2 << ":\t" << std::setw(6) << newEnergy << "vs" << oldEnergy << "(" << energy_per_labelGroup[l] <<","<< energy_per_labelGroup[l2] <<")\n";
			

			if (newEnergy <= oldEnergy * hypothesis_cost_factor){//+ 0.001){
				
				std::cout << " { ";
				for (int lab : oldlabelsl)
					std::cout << lab << ",";
				std::cout << " } {";
				for (int lab : oldlabelsl2)
					std::cout << lab << ",";
				std::cout << " } ";
				
				std::cout << "Merged!***\n";
				
				energy_per_labelGroup[l] = newEnergy;
				energy_per_labelGroup[l2] = 0;
				redo_last_l = true;
			}
			else {
				mergedLabels[l] = oldlabelsl;
				mergedLabels[l2] = oldlabelsl2;
			}			
		}
	}

	float total_energy_new = 0;
	for (float e : energy_per_labelGroup){
		total_energy_new += e;
	}
	std::cout << "Reduced error from " << total_energy_old << " to " << total_energy_new << "\n";

	if (set2Zero_instead_fuse)
	{
		int numLabelsLeft = 0;
		for (std::vector<int> & ls: mergedLabels){
			if (ls.size() > 0){
				numLabelsLeft++;
			}
		}
		//redistribute the segmentation weights
		for (int l_killed = 0; l_killed < numLabels; l_killed++){
			if (mergedLabels[l_killed].size() == 0){ //is killed.
				//redistribute label l to survivers
				for (int l_survived = 0; l_survived < numLabels; l_survived++){
					if (mergedLabels[l_survived].size() > 0){ //survived.
						//redistribute
						sparse_seg_to_fuse.col(l_survived) += sparse_seg_to_fuse.col(l_killed)/numLabelsLeft;
						for (auto & cloud_seg : alg->denseSegmentation_allClouds){
							cloud_seg.col(l_survived) += cloud_seg.col(l_killed)/numLabelsLeft;
						}
					}
				}
			}
		}
	}
	else
	{
		//fuse the segmentation
		for (int l = 0; l < numLabels; l++){
			for (int l2 : mergedLabels[l]){
				if (l != l2)
				{
					//sparse_seg_to_fuse.col(l) += sparse_seg_to_fuse.col(l2);
					sparse_seg_to_fuse.col(l) = sparse_seg_to_fuse.col(l).cwiseMax(sparse_seg_to_fuse.col(l2));
					for (auto & cloud_seg : alg->denseSegmentation_allClouds){
						//cloud_seg.col(l) += cloud_seg.col(l2);
						cloud_seg.col(l) = cloud_seg.col(l).cwiseMax(cloud_seg.col(l2));

					}
				}
			}
		}
	}
	//set fused labels to zero.
	for (int l = 0; l < numLabels; l++){
		if (mergedLabels[l].size() == 0){
			sparse_seg_to_fuse.col(l).fill(0);
			for (auto & cloud_seg : alg->denseSegmentation_allClouds){
				cloud_seg.col(l).fill(0);
			}
			
		}
	}

	return mergedLabels;
}

std::vector<std::vector<float>> crf_segmentation::computeEnergyFrames(cudaDataterm * cuda_dev,
	float sigma_sp, float sigma_t, float sigma_sp_factor, bool relax
	)
{
	std::vector<std::vector<float>> result(alg->allClouds.size());
	std::vector<float> buffer(alg->allClouds_asGrids[0]->size(), 0);
	int numLabels = alg->motions.numHypotheses();
		
	//1. how to compute the energy: compute it once for each label.
	cuda_dev->resetSparseValues();
	for (int lab = 0; lab < numLabels; lab++){
		

		//energy variant I: spatial filtering first, then temporal filtering
		std::vector<int> bla(1,lab);
		energyII(lab,
				bla,
				cuda_dev,
				buffer,
				sigma_sp,
				sigma_t,
				sigma_sp_factor,
				&result,
				relax);
				
		//variant III,IV... separate both... no clue what that would be. crfed temporal filtering (correct scale) with normal spatial filtering afterwards
		//and/or crfed spatial filtering (correct scale) with temporal filtering afterwards	
	}


	return result;
}

std::vector<std::vector<float>> crf_segmentation::computeEnergyOneHypAlone(cudaDataterm * cuda_dev,
	float sigma_sp, float sigma_t, float sigma_sp_factor, int label
	)
{
	std::vector<std::vector<float>> result(alg->allClouds.size());
	std::vector<float> buffer(alg->allClouds_asGrids[0]->size(), 0);
	int numLabels = alg->motions.numHypotheses();

	//1. how to compute the energy: compute it once for each label.
	cuda_dev->resetSparseValues();

		//energy variant I: spatial filtering first, then temporal filtering
		std::vector<int> bla;
		for (int i = 0; i < numLabels; i++){
			bla.push_back(i);
		}
		energyII(label,
			bla,
			cuda_dev,
			buffer,
			sigma_sp,
			sigma_t,
			sigma_sp_factor,
			&result,
			true);

	
	return result;
}

std::vector<std::vector<float>> crf_segmentation::spatioTemporalSmear(cudaDataterm * cuda_dev,
	float sigma_sp, float sigma_t,
	explainMe::Point & pos, int frame, int motion
	)
{
	auto & mo = alg->motions.getAlignmentSet(motion);
	std::vector<Eigen::Matrix4f> mo_inv = alg->motions.getAlignmentSetInv(motion);
	std::vector<std::vector<float>> result(alg->allClouds.size());
	std::vector<float> buffer;
	for (int i = 0; i < alg->allClouds.size(); i++){
		result[i].resize(alg->allClouds[i]->size(), 0);
	}

	float seed_val = 1000;
	//temporal smearing, yo
	for (int i = frame; i < alg->allClouds.size(); i++){
		//std::cout << "smoothing to frame " << i;
		Eigen::Vector4f target = mo[i] * mo_inv[frame] * pos.getVector4fMap();
		pxyz pt(target.x(), target.y(), target.z());
		pxyz nn = alg->allClouds_kdTree[i].findNN(pt);
		//std::cout << "first nn " << nn.x << "," << nn.y << "," << nn.z << "\n";
		Eigen::Vector3f NN;
		NN << nn.x, nn.y, nn.z;
		float dist = (target.topLeftCorner<3, 1>() - NN).norm();
		//std::cout << "dist: " << dist << "\n";

		float val = seed_val * std::exp(-dist*dist / sigma_sp/sigma_sp) * std::exp(-std::pow(std::abs<float>(i-frame),2)/sigma_t/sigma_t);
		/*float val = seed_val  *std::exp(-dist*dist / sigma_sp / sigma_sp);
		seed_val = val*  std::exp(-1.0 / sigma_t / sigma_t);*/

		//std::cout << "val: " << val;
		int c_i, c_j;
		alg->reader->getDescription()->project(nn.x, nn.y, nn.z, c_i, c_j);
		//std::cout << " at " << c_i << "," << c_j << "\n";

		cuda_dev->setQ(i, 0);
		//std::cout << "Initted the q to zero, now for the single seed element,...";
		cuda_dev->setQ(i, c_i, c_j, val);
		//std::cout << "smoothing to frame " << i << "ok\n";

		
	}
	seed_val = 1000;
	for (int i = frame; i >=0; i--){
		//std::cout << "smoothing to frame " << i;
		Eigen::Vector4f target = mo[i] * mo_inv[frame] * pos.getVector4fMap();
		pxyz pt(target.x(), target.y(), target.z());
		pxyz nn = alg->allClouds_kdTree[i].findNN(pt);
		//std::cout << "first nn " << nn.x << "," << nn.y << "," << nn.z << "\n";
		Eigen::Vector3f NN;
		NN << nn.x, nn.y, nn.z;
		float dist = (target.topLeftCorner<3, 1>() - NN).norm();
		//std::cout << "dist: " << dist << "\n";

		float val = seed_val * std::exp(-dist*dist / sigma_sp/sigma_sp) * std::exp(-std::pow(std::abs<float>(i-frame),2)/sigma_t/sigma_t);
		/*float val = seed_val  *std::exp(-dist*dist / sigma_sp / sigma_sp);
		seed_val = val*  std::exp(-1.0 / sigma_t / sigma_t);*/
		//std::cout << "val: " << val;
		int c_i, c_j;
		alg->reader->getDescription()->project(nn.x, nn.y, nn.z, c_i, c_j);
		//std::cout << " at " << c_i << "," << c_j << "\n";

		cuda_dev->setQ(i, 0);
		//std::cout << "Initted the q to zero, now for the single seed element,...";
		cuda_dev->setQ(i, c_i, c_j, val);
		//std::cout << "smoothing to frame " << i << "ok\n";

	}
	std::cout << "spatial smoothing ";
	float std_dev = meanNoise(alg->allClouds);
	cuda_dev->filter_dense2dense_allFrames_currentData(std_dev*30,30);
	std::cout << "done";

	//copy filtered values back.
	
	for (int frm = 0; frm < alg->allClouds.size(); frm++){
		cuda_dev->getCurrentDenseQBuffer(frm, buffer);
		int idx = 0, valid_point_idx = 0;
		for (auto it = alg->allClouds_asGrids[frm]->begin(); it != alg->allClouds_asGrids[frm]->end(); it++, idx++)
		{
			if (it->z != 0 && it->z * 0 == 0 && it->getNormalVector3fMap().norm() > 0.99){
				result[frm][valid_point_idx] = buffer[idx];
				valid_point_idx++;
			}
		}
	}
	std::cout << "Smoothing finished!\n";
	return result;
}


void crf_segmentation::do_sparse_crf_helper(cudaDataterm * cuda_dev,
	float lambda_temp,
	float lambda_spatial,
	//how strongly to weight far away frames.
	float sigma_temporal,
	float sigma_sp,
	float sigma_sp_factor,
	float start_crf_factor,
	float max_crf_factor,
	std::vector<int> & labels_to_use,
	bool init_from_cpu,
	int numIt)
{
	bool doSpatialCRF = false;
	bool nn_instead_of_crf = false;
	bool decreaseTemperature = false;
	bool divByMax = false;

	std::vector<float> Q_dense(alg->allClouds_asGrids[0]->size(), 1.0);
	std::vector<std::vector<float>> compatibility;
	//integralSimilarity_frustum(alg->motions.getAlignmentSets(), compatibility);
	compatibility.clear();
	compatibility.resize(alg->motions.getAlignmentSets().size());
	for (int i = 0; i < alg->motions.getAlignmentSets().size(); i++){
		compatibility[i].resize(alg->motions.getAlignmentSets().size());
	}

	float lambda_temporal = lambda_temp;
	float sigma_spatial = sigma_sp;
	int numHyp = alg->motions.numHypotheses();
	auto & clouds_as_grids = alg->allClouds_asGrids;
	StopWatch total, spatial_filter, spatial_cpy, temporal, other;

//seq parameterization FUCKING FIDDLED
int numFramesToInterpolateFromInFinalPass = 5; //5 is std-
float extra_boost_factor_final_reconst = 1;
//float sigma_boost_factor = 2;//
float sigma_boost_factor = sigma_sp_factor;//
float scale_factor = 1;
bool reparameterize = false; 
sigma_spatial = meanNoise(clouds_as_grids);
//bool reparameterize = true; divByMax = true; sigma_spatial = meanNoise(clouds_as_grids);lambda_spatial = 5; doSpatialCRF = true; 
//temporal reparam.
reparameterize = true; lambda_temporal = lambda_spatial = start_crf_factor; decreaseTemperature = true;
//spatial reparam & crf.
//divByMax = true; sigma_spatial = meanNoise(clouds_as_grids); lambda_spatial = 5; doSpatialCRF = true;

//compute annealing steps.
float max_anneal_factor = max_crf_factor;
float anneal_factor = std::pow(max_anneal_factor / lambda_spatial, 1.0 / (std::max(numIt - 1, 1)));

std::cout << "annealing: " << lambda_temporal << " to " << max_anneal_factor << " multiplying by " << anneal_factor << "\n";

std::cout << " Mean noise : " <<  meanNoise(clouds_as_grids) << "\n";
	//use a heuristic to initialize the meanfield in a way that removes oversegmentations
	/*if (useMeanFilterInitializationHeuristic){
	//do one temporal pass, rank the labels and compute qs.
	//lambda_temporal *= 20;
	cuda_dev->resetSparseValues();
	for (int hyp = 0; hyp < numHyp; hyp++)
	{
	//std::cout << "crf filtering hyp (" << hyp << ") spatial step - ";
	//initialize the Qs
	other.restart();
	for (int i = 0; i < clouds_as_grids.size(); i++) {
	cuda_dev->setQ(i, 1.0 / numHyp);
	}
	temporal.restart();
	cuda_dev->filter_dense2sparse_temporal_crf_oneHyp_no_Normalization(hyp, sigma_temporal);
	temporal.stop();
	}
	//for temporal step
	temporal.restart();
	cuda_dev->crf_exp_normalization_for_sparse(lambda_temporal);
	temporal.stop();
	float scale_factor = 5;
	infiniteBoxKernelCRF_perFrame_storeResultsInAlg(
	cuda_dev,
	scale_factor*lambda_spatial
	);

	//also use this as parameter.
	//sigma_spatial = meanNoise(clouds_as_grids);
	}*/
	std::cout << "crf with" << numHyp << "hypos---\n";
	//do the crf iteraiton
	for (int it = 0; it < numIt; it++){
		std::cout << "CRF iteration " << it << "\n";
		//reset dataterm, but keeps last crf result.
		cuda_dev->resetSparseValues();// 0);
		
		if (it != 0){
			cuda_dev->filter_sparse2dense_prepare_spatial_crf(sigma_spatial, lambda_spatial*scale_factor, divByMax);
		}

		for (int hyp = 0; hyp < numHyp; hyp++)
		{
			//std::cout << "crf filtering hyp (" << hyp << ") spatial step - ";
			//initialize the Qs
			if (it == 0){
				if (init_from_cpu){
					copy_cpu_dense_segweights_to_gpu(cuda_dev, alg->denseSegmentation_allClouds, hyp, Q_dense);
				}
				else{
					for (int i = 0; i < clouds_as_grids.size(); i++) {
						//cuda_dev->setQ(i, 1.0 / numHyp);
						cuda_dev->setQ(i, 0);
						//brute force search for label.
						for (int hs : labels_to_use){
							if (hs == hyp){
								cuda_dev->setQ(i, 1.0 / labels_to_use.size());// 1.0 / numHyp);
							}
						}
					}
				}
			}
			else{
				//gpu variant
				if (nn_instead_of_crf){
					cuda_dev->filter_sparse2dense_viaNN(hyp);
				}
				else{
					cuda_dev->filter_sparse2dense_do_prepared_spatial_crf(hyp, sigma_spatial, 
						lambda_spatial*scale_factor, sigma_boost_factor, doSpatialCRF, divByMax);
				}
			}
			cuda_dev->filter_dense2sparse_temporal_crf_oneHyp_no_Normalization(hyp, sigma_temporal);
		}//end loop over hypotheses

		//GPU variant of normalization
			std::cout << "Normalization-";
			//for temporal step
#ifndef DEACTIVATE_MERGING_HEURISTIC_AND_GET_BAD_RESULTS
			//idea merge motions at some point and do some more iterations.
			//*
			if (it == 3){
				computeMergesAsCompatibilitiesAndKillSmall(kill_threshold, cuda_dev, out, Q_sparse, compatibility);
				cuda_dev->crf_maxcompatibility_and_exp_normalization_for_sparse(compatibility, lambda_temporal*scale_factor);
			}
			//if (use_compatibility){
			else if (use_compatibility){
				cuda_dev->crf_compatibility_and_exp_normalization_for_sparse(compatibility, lambda_temporal*scale_factor);
			}
			else{
#endif
				std::cout << "temporal crf normalization, lambda " << lambda_temporal*scale_factor << "\n";
				cuda_dev->crf_exp_normalization_for_sparse(lambda_temporal*scale_factor);
#ifndef DEACTIVATE_MERGING_HEURISTIC_AND_GET_BAD_RESULTS
			}
#endif
			std::cout << "done\n";
		
		// only copy to cpu in the last iteration. 
			if (it == numIt - 1)
		{
//DEBUG RESET HYP 16
//std::cout << "RESETTING SPARSE HYP NO 16 FOR DEBUGGING REASEONS!";
//cuda_dev->resetSparseValues(16, 0.5); numFramesToInterpolateFromInFinalPass = 0;
//DEBUG END
			filter_sparse2dense_spatial_crf_allHyp_but_store_on_cpu(cuda_dev, sigma_spatial, lambda_spatial, sigma_boost_factor*extra_boost_factor_final_reconst, alg->denseSegmentation_allClouds,
				Q_dense, spatial_filter, spatial_cpy, false, numFramesToInterpolateFromInFinalPass);// doSpaNormalization);
		}
			
		std::cout << "crf it done.";

		//		
		//		fiddled
		//if (it>=3)
		if (decreaseTemperature)
		{
			//			lambda_temporal *= 2; 
			
			if (reparameterize){
				scale_factor *= anneal_factor;
				//boost_anihilator += 1;
				//extra_boost = 20;// / boost_anihilator;
				//extra_boost /= boost_anihilator;
			}
			else{
				lambda_temporal *= 20; 
			}
		}
	}

}


void crf_segmentation::doSparseCrf(cudaDataterm * cuda_dev,
	std::vector<pcl::PointXYZINormal> & seed_points,
	std::vector<int> & seed_cloud_idx,
	std::vector<int> & seed_point_idx,
	float lambda_temp,
	float lambda_spatial,
	//how strongly to weight far away frames.
	float sigma_temporal,
	float sigma_sp,
	float sigma_sp_factor,
	float max_crf_factor,
	//how much support to ask for in the minimum
	float kill_threshold,
	float per_hyp_cost_factor,
	explainMe::MatrixXf_rm & out, int mainIteration)
{



	/*bool spatial_crf_on_gpu = true;
	bool use_compatibility = false;
	bool doSpatialCRF = false; //false: only do interpolation
	bool decreaseTemperature = false;
	//bool useMeanFilterInitializationHeuristic = true;
	bool useMeanFilterInitializationHeuristic = false;*/

	std::cout << "\n\nThe per Hyp cost factor is " << per_hyp_cost_factor << "\n";

	float lambda_temporal = lambda_temp;
	float sigma_spatial = sigma_sp;

	int numHyp = alg->motions.numHypotheses();
	auto & clouds_as_grids = alg->allClouds_asGrids;

	//seq parameterization FUCKING FIDDLED
	/*float max_anneal_factor = 30;
	float anneal_factor = std::pow(max_anneal_factor / lambda_spatial, 1.0 / (std::max(num_crf_iterations - 1, 1)));
	float scale_factor = 1;
	bool reparameterize = false; bool divByMax = false;
	//bool reparameterize = true; bool divByMax = true; lambda_spatial = 5;doSpatialCRF = true;sigma_spatial = meanNoise(clouds_as_grids);*/


	StopWatch total, spatial_filter, spatial_cpy, temporal, other;
	total.restart();

	std::vector<float> Q_dense(alg->allClouds_asGrids[0]->size(), 1.0);
	//std::vector<std::vector<float>> Q_sparse(numHyp), Q_sparse_new(numHyp), tmp(numHyp);
		
	other.restart();
	//setTheSparseQueries.

	std::cout << __FILE__ << ",  " << __LINE__ << "setting gpu\n";

	cudaDatatermTools::setGPUSparsePoints(cuda_dev, seed_points, seed_cloud_idx);
	cudaDatatermTools::setGpuMotion(cuda_dev, alg->motions);
	cuda_dev->initSparseValues(numHyp);
	other.stop();

	std::vector<int> allLabels;
	for (int lbl = 0; lbl < numHyp; lbl++){
		allLabels.push_back(lbl);
	}
	do_sparse_crf_helper(cuda_dev, lambda_temp, lambda_spatial, sigma_temporal, sigma_sp, sigma_sp_factor,5, max_crf_factor, allLabels, false, num_crf_iterations);

	//inite compatibility matrix...
	/*std::vector<std::vector<float>> compatibility;
	//integralSimilarity_frustum(alg->motions.getAlignmentSets(), compatibility);
	compatibility.clear();
	compatibility.resize(alg->motions.getAlignmentSets().size());
	for (int i = 0; i < alg->motions.getAlignmentSets().size(); i++){
		compatibility[i].resize(alg->motions.getAlignmentSets().size());
	}*/

	//use a heuristic to initialize the meanfield in a way that removes oversegmentations
	/*if (useMeanFilterInitializationHeuristic){
		//do one temporal pass, rank the labels and compute qs.
		//lambda_temporal *= 20;
		cuda_dev->resetSparseValues();
		for (int hyp = 0; hyp < numHyp; hyp++)
		{
			//std::cout << "crf filtering hyp (" << hyp << ") spatial step - ";
			//initialize the Qs
			other.restart();
			for (int i = 0; i < clouds_as_grids.size(); i++) {
				cuda_dev->setQ(i, 1.0 / numHyp);
			}
			temporal.restart();
			cuda_dev->filter_dense2sparse_temporal_crf_oneHyp_no_Normalization(hyp, sigma_temporal);
			temporal.stop();
		}
		//for temporal step
		temporal.restart();
		cuda_dev->crf_exp_normalization_for_sparse(lambda_temporal);
		temporal.stop();
		

		float scale_factor = 5;
		infiniteBoxKernelCRF_perFrame_storeResultsInAlg(
			cuda_dev,
			scale_factor*lambda_spatial
			);

		//also use this as parameter.
		//sigma_spatial = meanNoise(clouds_as_grids);

		
	}*/

	/*
	std::cout << "crf with" << numHyp << "hypos---\n";
	//do the crf iteraiton
	for (int it = 0; it < num_crf_iterations; it++){
		std::cout << "CRF iteration " << it << "\n";
		//reset dataterm, but keeps last crf result.
		other.restart();
		cuda_dev->resetSparseValues();
		other.stop();

		if (it != 0){
			//as the gpu miight not have enough memory to store all dense filtered results for all data
			//the spatial filtering will be done on the fly for each hypothesis. The facors used for normalization
			//are precomputed by this method.

			//crf_final_result=>crf_final_result
			//cuda_dev->crf_compatibility_for_sparse(compatibility, lambda_temporal);
			//std::cout << "Prepare spatial crf... \n";
			spatial_filter.restart();
			cuda_dev->filter_sparse2dense_prepare_spatial_crf(sigma_spatial, lambda_spatial*scale_factor, divByMax);
			spatial_filter.stop();
		}
		
		for (int hyp = 0; hyp < numHyp; hyp++)
		{
			//std::cout << "crf filtering hyp (" << hyp << ") spatial step - ";
			//initialize the Qs
			if (it == 0){
				other.restart();
				if (useMeanFilterInitializationHeuristic){
					copyDenseResults_alg2Gpu(hyp, Q_dense, cuda_dev);
				}
				else
				{
					for (int i = 0; i < clouds_as_grids.size(); i++) {
						cuda_dev->setQ(i, 1.0 / numHyp);
					}
				}
				other.stop();
			}
			else{
				//gpu variant
				if (spatial_crf_on_gpu)
				{
					//Variant1: filter sparse 2 dense once for each hypothesis (lots of duplicate work)
					spatial_filter.restart();
					cuda_dev->filter_sparse2dense_do_prepared_spatial_crf(hyp, sigma_spatial, lambda_spatial*scale_factor, doSpatialCRF, divByMax);
					spatial_filter.stop();

					//Variant 2: compute them all and copy them back to the cpu; here they only need to be copyied to the gpu.
					//spatial_cpy.restart();
					//copy_cpu_dense_segweights_to_gpu(cuda_dev, alg->denseSegmentation_allClouds, hyp, Q_dense);
					//spatial_cpy.stop();

				}
				else
				{
					//cpu variant
					for (int i = 0; i < clouds_as_grids.size(); i++) {
						if (it != 0){
							alg->crfFilterWeight_sparse2dense(Q_sparse, hyp, clouds_as_grids[i], Q_dense, [](float w)->float{return 1 - w; }, sigma_spatial, lambda_spatial, i, true);
						}
						cuda_dev->setQ(i, Q_dense);
					}

					//here U can
				}
			}
			
			//std::cout << "done. Temporal -";
			temporal.restart();
			cuda_dev->filter_dense2sparse_temporal_crf_oneHyp_no_Normalization(hyp, sigma_temporal);
			temporal.stop();
			//std::cout << "done\n";
		}//end loop over hypotheses

		//GPU variant of normalization
		if (spatial_crf_on_gpu)
		{
			std::cout << "Normalization-";
			//for temporal step
			temporal.restart();
#ifndef DEACTIVATE_MERGING_HEURISTIC_AND_GET_BAD_RESULTS
			//idea merge motions at some point and do some more iterations.
			//*
			if (it == 3){
				computeMergesAsCompatibilitiesAndKillSmall(kill_threshold,cuda_dev, out, Q_sparse, compatibility);
				cuda_dev->crf_maxcompatibility_and_exp_normalization_for_sparse(compatibility, lambda_temporal);
			}
			//if (use_compatibility){
			else if (use_compatibility){
				cuda_dev->crf_compatibility_and_exp_normalization_for_sparse(compatibility, lambda_temporal);
			}
			else{
#endif
				cuda_dev->crf_exp_normalization_for_sparse(lambda_temporal);
#ifndef DEACTIVATE_MERGING_HEURISTIC_AND_GET_BAD_RESULTS
			}
#endif
			temporal.stop();
			std::cout << "done\n";
		}
		//cpu variant of normalization.
		else{
			//Works but copies data to cpu and back.
			for (int hyp = 0; hyp < numHyp; hyp++){
				cuda_dev->getSparseValues(hyp, Q_sparse_new[hyp]);
				Q_sparse[hyp].resize(Q_sparse_new[hyp].size());
			}
			//normalize and cast to probabilities.
			for (int i = 0; i < Q_sparse_new[0].size(); i++){
				float z = 0;
				float min = std::numeric_limits<float>::max();
				for (int hyp = 0; hyp < Q_sparse_new.size(); hyp++){
					min = (min < Q_sparse_new[hyp][i] ? min : Q_sparse_new[hyp][i]);
				}
				for (int hyp = 0; hyp < Q_sparse_new.size(); hyp++){
					Q_sparse_new[hyp][i] = std::exp(-lambda_temporal *(Q_sparse_new[hyp][i] - min)); // *std::exp(-lambda *(min))
					z += Q_sparse_new[hyp][i]; // *std::exp(-lambda *(min))
				}
				for (int hyp = 0; hyp < Q_sparse.size(); hyp++){
					Q_sparse[hyp][i] = 1 / z*Q_sparse_new[hyp][i]; // factor std::exp(-lambda *(min)) cancels out, but numerically more stable.
				}
			}
			cuda_dev->setSparseValues(Q_sparse);
			
		}

		//spatial crf update (Variant2: compute them all and copy them back to the gpu)
		if (spatial_crf_on_gpu)
		{
			//variant 1: only copy to cpu in the last iteration. 
			if (it == num_crf_iterations - 1)
			{
				filter_sparse2dense_spatial_crf_allHyp_but_store_on_cpu(cuda_dev, sigma_spatial, lambda_spatial, alg->denseSegmentation_allClouds,
					Q_dense, spatial_filter, spatial_cpy, false);// doSpaNormalization);
			}
			//variant 2
			//filter_sparse2dense_spatial_crf_allHyp_but_store_on_cpu(cuda_dev, sigma_spatial, lambda_spatial, alg->denseSegmentation_allClouds, 
			//	Q_dense, spatial_filter, spatial_cpy);
		}
		std::cout << "crf it done.";

//		if (decreaseTemperature)
//		fiddled
//if (it>=3)
		{
//			lambda_temporal *= 2; 
			lambda_temporal *= 20;
if (reparameterize){
scale_factor *= anneal_factor;}
		}
	}//*/
	//set sparse values corresponding to the dense values stored in the alg class
	if (out.rows() != seed_cloud_idx.size() || seed_cloud_idx.size() != seed_point_idx.size()){
		std::cout << "Error! in " << __FILE__ << ", line " << __LINE__ << "assertion failed.";
		exit(1);
	}
	for (int j = 0; j < out.rows(); j++){
		if (seed_point_idx[j] >= alg->denseSegmentation_allClouds[seed_cloud_idx[j]].rows() || seed_point_idx[j]<0){
			std::cout << "Error with seedpoint idx! in " << __FILE__ << ", line " << __LINE__ << "assertion failed.";
			exit(1);
		}
		out.row(j) = alg->denseSegmentation_allClouds[seed_cloud_idx[j]].row(seed_point_idx[j]);
	}


	//computeMergesAsCompatibilities(cuda_dev, out, Q_sparse, compatibility);

	////////////////////////////////////////////////
	///missing spatial filtering step...?
	//spatial filtering on gpu:
	/*if (spatial_crf_on_gpu)
	{
		//Not needed in Variant 2.
	}
	else{
		//spatial filtering on cpu:
		cuda_dev->getSparseCRFResult(Q_sparse);
		for (int hyp = 0; hyp < numHyp; hyp++){
			for (int j = 0; j < Q_sparse[hyp].size(); j++){
				out(j, hyp) = Q_sparse[hyp][j];
			}
		}
		alg->w = out;
		for (int hyp = 0; hyp < numHyp; hyp++){
			for (int i = 0; i < alg->allClouds.size(); i++) {
				std::cout << "+";
				alg->crfFilterWeight_sparse2dense(Q_sparse, hyp, alg->allClouds[i], Q_dense, [](float w)->float{return 1 - w; }, sigma_spatial, lambda_spatial, i, true);
				for (int j = 0; j < alg->allClouds[i]->size(); j++)
				{
					alg->denseSegmentation_allClouds[i](j, hyp) = Q_dense[j];
				}
			}
		}
	}*/
	total.stop();


	//Debugging: No pixel wise renormalization after here.
	/*for (auto & f : alg->denseSegmentation_allClouds){
		f.col(16).fill(0.5);
	}*/



	if (alg->params.crf_min_by_merge){
		//weird experimental stuff i was trying out.
		bool split_prior_to_merge = false;
		bool redistributeInsteadOfFuseAndResolve = false;

		//This vector can give hints about which labels to greedely try to merge first
		//If empty use std strategy
		std::vector<std::pair<int, int>> merge_strategy;

		//split labels before merge.
		if (split_prior_to_merge){
			//store information about what labels have been split into what. 
			//Then merge temporally split labels in a good order:
			//first: try to merge into labels with the most temporal support/overlap
			//second: try to merge into labels without split
			//How this is implemented: there is a vector of label-vector pairs std::vector<std::pair<int, std::vector<int>>>
			//The loops in the merging become for(pair)l1 = pair.l1, l2:pair.labels
			
			std::cout << "Temporally splitting motions ...\n";
			int numMotions = alg->motions.numHypotheses();
			alg->motions.temporallySplitHypotheses_and_updateDenseSeg(alg->allClouds, alg->denseSegmentation_allClouds, alg->allClouds_kdTree);
			std::cout << "Temporal splitting done. Num Segments: " << numMotions << "=>" << alg->motions.numHypotheses() << "\n";

			//update sparse segmentation .
			out.conservativeResize(out.rows(), alg->motions.numHypotheses());
			for (int j = 0; j < out.rows(); j++){
				if (seed_point_idx[j] >= alg->denseSegmentation_allClouds[seed_cloud_idx[j]].rows() || seed_point_idx[j]<0){
					std::cout << "Error with seedpoint idx! in " << __FILE__ << ", line " << __LINE__ << "assertion failed.";
					exit(1);
				}
				out.row(j) = alg->denseSegmentation_allClouds[seed_cloud_idx[j]].row(seed_point_idx[j]);
			}

			//make sure all buffers are initted correctly
			std::cout << __FILE__ << ",  " << __LINE__ << "setting gpu\n";
			cudaDatatermTools::setGPUSparsePoints(cuda_dev, seed_points, seed_cloud_idx);
			cudaDatatermTools::setGpuMotion(cuda_dev, alg->motions);
			cuda_dev->initSparseValues(alg->motions.numHypotheses());

			//prepare greedy merge stratiegy
			//encoded by pairs.
			std::vector<Eigen::RowVectorXf> allSupports;
			alg->motions.computeSupport(alg->denseSegmentation_allClouds, allSupports);
			std::vector<std::tuple<int,int, float>> supportOverlaps;
			for (int ii = 0; ii < alg->motions.numHypotheses(); ii++){
				//supportOverlaps.push_back(std::vector<std::tuple<int,int, float>>());
				for (int jj = 0; jj < alg->motions.numHypotheses(); jj++){
					float overlap = 0;
					for (int ff = 0; ff < allSupports.size();ff++){
						overlap += std::min(allSupports[ff](ii), allSupports[ff](jj)) / (1 + allSupports[ff](jj));
					}
					supportOverlaps.push_back(std::tuple<int, int, float>(ii,jj, overlap));
				}
				//sort in descending manner.
				//std::sort(supportOverlapsii].begin(), supportOverlaps[ii].end(), [](const std::pair<int, float> &left, const std::pair<int, float> &right) {
				//	return left.second > right.second;
				//});
			}
			std::sort(supportOverlaps.begin(), supportOverlaps.end(), [](const std::tuple<int,int, float> &left, const std::tuple<int,int, float> &right) {
				return std::get<2>(left) > std::get<2>(right);
			});

			std::cout << "proposed merge strategy: \n";
			//std::vector<std::pair<int, int>> mergeStrategy;
			for (int ii = 0; ii < supportOverlaps.size(); ii++){
				merge_strategy.push_back(std::pair<int, int>());
				merge_strategy[ii].first = std::get<0>(supportOverlaps[ii]);
				merge_strategy[ii].second = std::get<1>(supportOverlaps[ii]);
				std::cout << ii << ": " << merge_strategy[ii].first << "<-" << merge_strategy[ii].second << "\n";
			}
			//sort for outer loop, e.g by support size.
			//std::sort(mergeStrategy.begin(), mergeStrategy.end(), [](const std::pair<int, float> &left, const std::pair<int, float> &right) {
			//	return left.second > right.second;
			//});
		}


		std::vector<int> sparse_labeling(seed_cloud_idx.size());
		for (int j = 0; j < out.rows(); j++){
			out.row(j).maxCoeff(&sparse_labeling[j]);
		}
				
		/*bool split_prior_to_merge = true;

		//split labels before merge.
		if (split_prior_to_merge){
			std::cout << "Temporally splitting motions ...\n";
			int numMotions = alg->motions.numHypotheses();
			alg->motions.temporallySplitHypotheses_and_updateDenseSeg(alg->allClouds, alg->denseSegmentation_allClouds, alg->allClouds_kdTree);
			std::cout << "Temporal splitting done. Num Segments: " << numMotions << "=>" << alg->motions.numHypotheses() << "\n";
		}*/

		auto mergedLabels = minimizeEnergyByMergingLabels(cuda_dev, Q_dense, sigma_spatial, sigma_temporal, sigma_sp_factor, sparse_labeling, out, per_hyp_cost_factor, merge_strategy, redistributeInsteadOfFuseAndResolve);


		//idea: set merged labels to zero and optimize for segmentation one mor time.
		if (redistributeInsteadOfFuseAndResolve)
		{
			std::vector<int> remainingLabels;
			for (int lbl = 0; lbl < mergedLabels.size(); lbl++){
				auto & superlab = mergedLabels[lbl];
				if (superlab.size() != 0){
					remainingLabels.push_back(lbl);
				}
			}
			std::cout << "Rerun with " << remainingLabels.size() << "\n";
			bool initFromCPU = true;
			int numIts = 1;
			//sett the merged labels to zero, reoptimize.
			do_sparse_crf_helper(cuda_dev, lambda_temp, lambda_spatial, sigma_temporal, sigma_sp, sigma_sp_factor, max_crf_factor, max_crf_factor, remainingLabels, initFromCPU, numIts);

			//set sparse values corresponding to the dense values stored in the alg class
			if (out.rows() != seed_cloud_idx.size() || seed_cloud_idx.size() != seed_point_idx.size()){
				std::cout << "Error! in " << __FILE__ << ", line " << __LINE__ << "assertion failed.";
				exit(1);
			}
			for (int j = 0; j < out.rows(); j++){
				if (seed_point_idx[j] >= alg->denseSegmentation_allClouds[seed_cloud_idx[j]].rows() || seed_point_idx[j] < 0){
					std::cout << "Error with seedpoint idx! in " << __FILE__ << ", line " << __LINE__ << "assertion failed.";
					exit(1);
				}
				out.row(j) = alg->denseSegmentation_allClouds[seed_cloud_idx[j]].row(seed_point_idx[j]);
			}
		}//*/
	}

	std::cout << "\nCRF Took " << total.total() / 1000.0 << " seconds: temporal " << temporal.total() / 1000.0 << " spatial: cpy " << spatial_cpy.total() / 1000.0 << " filter " << spatial_filter.total() /1000.f<< " other: " << other.total() / 1000.0 << "\n";
}


void crf_segmentation::infiniteBoxKernelCRF_perFrame_storeResultsInAlg(cudaDataterm * cuda_dev,
	float scale
	)
{
	//1. per frame.
	auto & clouds_as_grids = alg->allClouds_asGrids;
	int numHyp = alg->motions.numHypotheses();
	int w = clouds_as_grids[0]->width;
	int h = clouds_as_grids[0]->height;


	std::vector<std::vector<float>> Q_filled_in_sparse_values(numHyp, std::vector<float>(alg->allClouds_asGrids[0]->size(), 1.0));
	std::vector<std::vector<float>> Q_dense_perLabel(numHyp, std::vector<float>(alg->allClouds_asGrids[0]->size(), 1.0));
	std::vector<std::vector<float>> Q_buffer_perLabel(numHyp, std::vector<float>(alg->allClouds_asGrids[0]->size(), 1.0));

	Eigen::VectorXd globalsums(numHyp);
	globalsums.fill(0);
	for (int frame = 0; frame < clouds_as_grids.size(); frame++) {
		std::cout << frame << "/" << clouds_as_grids.size() << " ";
		//get the current sparse values
		cuda_dev->getSparseValues_FilledIn(Q_filled_in_sparse_values, frame); 

		
		if (frame == 0){
			std::cout << "sums before:";
		}
		Eigen::VectorXd sums(numHyp);
		for (int hyp = 0; hyp < numHyp; hyp++){		
			Eigen::Map<Eigen::VectorXf> data(Q_filled_in_sparse_values[hyp].data(), Q_filled_in_sparse_values[hyp].size());
			if (frame == 0){
				std::cout<< data.sum();
			}
			//do 1-q first
			sums(hyp) =  data.size()*1.0 - data.sum();
		}

		//a) per frame
		/*
		sums /= sums.maxCoeff();
		double min = sums.minCoeff();
		sums = -scale *(sums.array() - min);
		sums = sums.array().exp();
		sums /= sums.sum();
		if (frame == 0){
			std::cout << "crffed sums:" << sums << "\n";
		}
				
		
		for (int hyp = 0; hyp < numHyp; hyp++){
			alg->denseSegmentation_allClouds[frame].col(hyp).fill(sums(hyp));
		}
	}

	/*/	//b) globally
		globalsums += sums;
	}
	//b) globally
	globalsums /= globalsums.maxCoeff();
	double min = globalsums.minCoeff();
	globalsums = -scale *(globalsums.array() - min);
	globalsums = globalsums.array().exp();
	globalsums /= globalsums.sum();


	//ranked:
	std::vector<double> sorted(numHyp);
	for (int i = 0; i < numHyp; i++){
		sorted[i] = globalsums(i);
	}
	std::sort(sorted.begin(), sorted.end(),std::greater<double>());
	double percent = 0.015;
	double maxVal = percent / (1 - std::pow(1-percent, numHyp));
	Eigen::VectorXd rankedInit = globalsums;
	for (int i = 0; i < numHyp; i++){
		std::cout << sorted[i] << ">";
	}
	for (int i = 0; i < numHyp; i++){
		auto iter = std::lower_bound(sorted.begin(), sorted.end(), globalsums(i), std::greater<double>());
		int rank = int(iter - sorted.begin());
		rankedInit(i) = maxVal * std::pow(1-percent, rank);
		std::cout << globalsums(i) << "=> rank " << rank << "\n";
	}
	std::cout << "\nsum: " << rankedInit.sum() << "\n";
	rankedInit /= rankedInit.sum();
	std::cout << "globsums: " << globalsums << " \n";
	globalsums = rankedInit;
	std::cout << "ranks: " << globalsums << " \n";

	for (int frame = 0; frame < clouds_as_grids.size(); frame++) {
		for (int hyp = 0; hyp < numHyp; hyp++){
			alg->denseSegmentation_allClouds[frame].col(hyp).fill(globalsums(hyp));
		}
	}
	//*/
}

#include <omp.h>
void crf_segmentation::sparseSequentialCrf_perFrame_storeResultsInAlg(
	cudaDataterm * cuda_dev,
	float scale,
	float sigma_spatial,
	float factor_sigma
	/*std::vector<std::vector<float>> & Q_dense_perLabel,
	std::vector<std::vector<float>> & Q_buffer_perLabel,
	std::vector<std::vector<float>> & Q_filled_in_sparse_values
	std::vector<std::vector<float>> & a,
	std::vector<std::vector<float>> & b,
	std::vector<std::vector<float>> & c//*/
	){

	auto & clouds_as_grids = alg->allClouds_asGrids;
	int numHyp = alg->motions.numHypotheses();
	int w = clouds_as_grids[0]->width;
	int h = clouds_as_grids[0]->height;

	//estimate energy scale.
	float estimated_energyScale = 0;
	{
		std::vector<std::vector<float>> Q_filled_in_sparse_values(numHyp, std::vector<float>(alg->allClouds_asGrids[0]->size(), 1.0));
		std::vector<std::vector<float>> Q_dense_perLabel(numHyp, std::vector<float>(alg->allClouds_asGrids[0]->size(), 1.0));
		std::vector<std::vector<float>> Q_buffer_perLabel(numHyp, std::vector<float>(alg->allClouds_asGrids[0]->size(), 1.0));
		int frame = 0;
		cuda_dev->getSparseValues_FilledIn(Q_filled_in_sparse_values, frame); 
		//matrix wrappers for transparent accesspattern
		matrix<explainMe::Point, explainMe::Cloud> cloud_mat(*clouds_as_grids[frame], w, h);
		std::vector<matrix<float, std::vector<float>>> q_crf_in, q_crf_out, q_crf_tmp_out;
		for (int __hyp = 0; __hyp < Q_filled_in_sparse_values.size(); __hyp++){
			q_crf_in.push_back(matrix<float, std::vector<float>>(Q_filled_in_sparse_values[__hyp], w, h));
		}
		for (auto & q : Q_dense_perLabel){
			q_crf_out.push_back(matrix<float, std::vector<float>>(q, w, h));
		}
		for (auto & q : Q_buffer_perLabel){
			q_crf_tmp_out.push_back(matrix<float, std::vector<float>>(q, w, h));
		}
		auto myFilter = recursiveFilter(sigma_spatial);
		estimated_energyScale = spatial_sequential_crf(
			cloud_mat,
			q_crf_in,
			q_crf_tmp_out,
			scale,//lambda_spatial, // 15 /400.
			factor_sigma,
			myFilter
			);
		std::cout << "\n****Estimated energy scale: " << estimated_energyScale << " *****\n";
	}
	

#pragma omp parallel
	{
		std::vector<std::vector<float>> Q_filled_in_sparse_values(numHyp, std::vector<float>(alg->allClouds_asGrids[0]->size(), 1.0));
		std::vector<std::vector<float>> Q_dense_perLabel(numHyp, std::vector<float>(alg->allClouds_asGrids[0]->size(), 1.0));
		std::vector<std::vector<float>> Q_buffer_perLabel(numHyp, std::vector<float>(alg->allClouds_asGrids[0]->size(), 1.0));
#pragma omp for
		for (int frame = 0; frame < clouds_as_grids.size(); frame++) {
			std::cout << frame << "/" << clouds_as_grids.size() << " ";
			//get the current sparse values
#pragma omp critical
			{cuda_dev->getSparseValues_FilledIn(Q_filled_in_sparse_values, frame);}

			//matrix wrappers for transparent accesspattern
			matrix<explainMe::Point, explainMe::Cloud> cloud_mat(*clouds_as_grids[frame], w, h);
			std::vector<matrix<float, std::vector<float>>> q_crf_in, q_crf_out, q_crf_tmp_out;

			//dummy debug values..
			//std::vector<std::vector<float>> Q_dbg = Q_filled_in_sparse_values;
			//Q_dbg[__hyp][i] = (((i %w) / 60 + __hyp) % numHyp == 0 && ((i/w)/60 +__hyp) % numHyp == 0 && __hyp % 2 == 0 ? 1 : 0);

			for (int __hyp = 0; __hyp < Q_filled_in_sparse_values.size(); __hyp++){
				//q_crf_in.push_back(matrix<float, std::vector<float>>(Q_dbg[__hyp], w, h));
				q_crf_in.push_back(matrix<float, std::vector<float>>(Q_filled_in_sparse_values[__hyp], w, h));
			}

			for (auto & q : Q_dense_perLabel){
				q_crf_out.push_back(matrix<float, std::vector<float>>(q, w, h));
			}
			for (auto & q : Q_buffer_perLabel){
				q_crf_tmp_out.push_back(matrix<float, std::vector<float>>(q, w, h));
			}
			//if (frame == 0){
			//access pattern.
			for (int accesspattern = 0; accesspattern < 8; accesspattern++)
			{
				for (auto & q : q_crf_in){
					q.setAccessPattern(accesspattern);
				}
				for (auto & q : q_crf_out){
					q.setAccessPattern(accesspattern);
				}
				for (auto & q : q_crf_tmp_out){
					q.setAccessPattern(accesspattern);
				}
				cloud_mat.setAccessPattern(accesspattern);


				//std::cout << "Sequential crf pass!";
				auto myFilter = recursiveFilter(sigma_spatial);
				//myFilter.estimatedEnergyScale2D()
				spatial_sequential_crf(
					cloud_mat,
					q_crf_in,
					(accesspattern == 0 ? q_crf_out : q_crf_tmp_out),
					//FUCKING FIDDLED.
//					scale/estimated_energyScale,//lambda_spatial, // 15 /400.
scale,
					factor_sigma,
					//recursiveFilter(1, 0.9, 0.007 /*avg dist*/)//sigma_sp)
					myFilter
					);
				//std::cout << " done!\n";

				if (accesspattern != 0){
					for (int lab = 0; lab < numHyp; lab++)
						q_crf_out[lab].cwise_combine_with(q_crf_tmp_out[lab], [](float other, float & to){to += other; });
				}
			}
			for (int lab = 0; lab < numHyp; lab++)
				q_crf_out[lab].cwise_apply([](float & entry){entry /= 8; });
			//}
			//dbg file dumps.
			/*for (auto & q : q_crf_out){
					q.toSpaceDelimFile(std::string("G:/PHD/CRF/SequentialCRF/out") + std::to_string(frame) + "_hyp" + std::to_string(what), [](float val){return std::to_string(val); });
					what++;
					}
					*/

			copyDenseResults_Data2Alg(frame, q_crf_out);
		}
	}
}

void crf_segmentation::copyDenseResults_Data2Alg(int frame, std::vector<matrix<float, std::vector<float>>>& data){
	std::cout << "copy back...";
	for (int hyp = 0; hyp < data.size(); hyp++)
	{
		//store in ram!
		int idx = 0, out_idx = 0;
		//crf object returns per grid values but the values are only stored for valid non nan points---
		//So fill it in
		auto buff = data[hyp].getData();
		for (auto iter = alg->allClouds_asGrids[frame]->begin(); iter != alg->allClouds_asGrids[frame]->end(); iter++, idx++)
		{
			if (iter->z != 0 && iter->z * 0 == 0 && iter->getNormalVector3fMap().norm() > 0.99){
				alg->denseSegmentation_allClouds[frame](out_idx, hyp) = buff[idx];
				out_idx++;
			}
		}
	}
	std::cout << "done!\n";
}

void crf_segmentation::copyDenseResults_alg2Gpu(int hyp, std::vector<float> & buffer, cudaDataterm * cuda_dev)
{
	
	//crf object returns per grid values but the values are only stored for valid non nan points---
	//So fill it in
	for (int frame = 0; frame < alg->allClouds_asGrids.size(); frame++){
		
		int idx = 0, in_idx = 0;
		for (auto iter = alg->allClouds_asGrids[frame]->begin(); iter != alg->allClouds_asGrids[frame]->end(); iter++, idx++)
		{
			if (iter->z != 0 && iter->z * 0 == 0 && iter->getNormalVector3fMap().norm() > 0.99){
				buffer[idx] = alg->denseSegmentation_allClouds[frame](in_idx, hyp);
				in_idx++;
			}
			else{
				buffer[idx] = 0;
			}
		}
		cuda_dev->setQ(frame, buffer);
	}
	
}

void crf_segmentation::copyCombinedDenseResults_alg2Gpu(std::vector<int> & hyps, std::vector<float> & buffer, cudaDataterm * cuda_dev)
{

	//crf object returns per grid values but the values are only stored for valid non nan points---
	//So fill it in
	for (int frame = 0; frame < alg->allClouds_asGrids.size(); frame++){

		int idx = 0, in_idx = 0, maxCoeff;
		for (auto iter = alg->allClouds_asGrids[frame]->begin(); iter != alg->allClouds_asGrids[frame]->end(); iter++, idx++)
		{
			if (iter->z != 0 && iter->z * 0 == 0 && iter->getNormalVector3fMap().norm() > 0.99){
				buffer[idx] = 0;
				
				alg->denseSegmentation_allClouds[frame].row(in_idx).maxCoeff(&maxCoeff);
				for (int hyp : hyps){
					if (maxCoeff == hyp)
						buffer[idx] = 1;
				}
				
				if (buffer[idx] < 0){
					std::cout << "\n***ASSERTION FAILED!" << __FILE__ << "," << __LINE__ << ": " << buffer[idx] << "<0\n";
				}
				in_idx++;
			}
			else{
				buffer[idx] = 0;
			}
			//buffer[idx] = 1 - buffer[idx];
		}
		cuda_dev->setQ(frame, buffer);
	}

}


void crf_segmentation::doSparseSequentialCrf(cudaDataterm * cuda_dev,
	std::vector<pcl::PointXYZINormal> & seed_points,
	std::vector<int> & seed_cloud_idx,
	std::vector<int> & seed_point_idx,
	float lambda_temp,
	float lambda_spatial,
	//how strongly to weight far away frames.
	float sigma_temporal,
	float sigma_sp,
	float sigma_spatial_factor,
	float max_anneal_factor,
	//how much support to ask for in the minimum
	float kill_threshold,
	explainMe::MatrixXf_rm & out, int mainIteration)
{
	float anneal_factor = std::pow(max_anneal_factor / lambda_spatial, 1.0 / (std::max(num_crf_iterations - 1, 1)));
	std::cout << "Sparse sequential crf: lambda_spatial = " << lambda_spatial << ", sigma_spatial = " << sigma_sp << "max anneal_factor = " << max_anneal_factor << "factor" << anneal_factor <<"\n";
	std::cout << "\n\nUSING fixed per hyp cost factor!!!\n\n";
	float per_hyp_cost_factor = 1;
	bool doSpaNormalization = false;
	bool InfBoxKernelInit = false;

	float lambda_temporal = lambda_temp;
	float sigma_spatial = sigma_sp;

	float sigma_boost_factor = 5;
	float scale_factor = 1;

	int numHyp = alg->motions.numHypotheses();
	auto & clouds_as_grids = alg->allClouds_asGrids;

	StopWatch total, spatial_filter, spatial_cpy, temporal, other;
	total.restart();

	std::vector<float> Q_dense(alg->allClouds_asGrids[0]->size(), 1.0);
	std::vector<std::vector<float>> Q_sparse(numHyp), Q_sparse_new(numHyp), tmp(numHyp);

	
	other.restart();
	//setTheSparseQueries.
	std::cout << __FILE__ << ",  " << __LINE__ << "setting gpu\n";
	cudaDatatermTools::setGPUSparsePoints(cuda_dev, seed_points, seed_cloud_idx);
	cudaDatatermTools::setGpuMotion(cuda_dev, alg->motions);
	cuda_dev->initSparseValues(numHyp);
	other.stop();
	
	float mean_stdev_noise = meanNoise(clouds_as_grids);


	std::cout << "crf with" << numHyp << "hypos---\n";
	//do the crf iteraiton
	for (int it = 0; it < num_crf_iterations; it++){
		std::cout << "CRF iteration " << it << "/" << num_crf_iterations << "\n";
		//reset dataterm, but keeps last crf result.
		other.restart();
		cuda_dev->resetSparseValues();
		other.stop();

		if (it != 0){
			//do the spatial crf update.

//Fucking fiddled. Trying if biasing by choosing a more consistent start config is enough.
			if (it > 1 || !InfBoxKernelInit)
			{
				sparseSequentialCrf_perFrame_storeResultsInAlg(cuda_dev,
					scale_factor*lambda_spatial,
					sigma_spatial,
					sigma_spatial_factor);
			}
			else{
				infiniteBoxKernelCRF_perFrame_storeResultsInAlg(
					cuda_dev,
					scale_factor*lambda_spatial
					);
			}
		}

		for (int hyp = 0; hyp < numHyp; hyp++)
		{
			std::cout << "crf filtering hyp (" << hyp << ") - ";
			//initialize the Qs
			if (it == 0){
				std::cout << "crf initting Qs";
				other.restart();
				for (int i = 0; i < clouds_as_grids.size(); i++) {
					cuda_dev->setQ(i, 1.0 / numHyp);
				}
				other.stop();
			}
			else{
				copyDenseResults_alg2Gpu(hyp, Q_dense, cuda_dev);
			}
			
			temporal.restart();
			cuda_dev->filter_dense2sparse_temporal_crf_oneHyp_no_Normalization(hyp, sigma_temporal);
			temporal.stop();
		}//end loop over hypotheses

		//Normalization for temporal step
		std::cout << "Normalization-";
		temporal.restart();
		cuda_dev->crf_exp_normalization_for_sparse(lambda_temporal);
		temporal.stop();
		std::cout << "done\n";
			
		//do only filtering instead of crf in the last iteration. 
		std::cout << "Final seq crf pass:... ";
		if (it == num_crf_iterations - 1)
		{
			/*sparseSequentialCrf_perFrame_storeResultsInAlg(cuda_dev, 
				scale_factor * lambda_spatial,
				sigma_spatial,
				//buffers.
				Q_dense_perLabel, 
				Q_buffer_perLabel, 
				Q_filled_in_sparse_values);*/

			//alternative, much better.
			//instead of sigma spatial the correct parameter for interpol here should be used. as a fallback mean noise can be used.

			filter_sparse2dense_spatial_crf_allHyp_but_store_on_cpu(cuda_dev, mean_stdev_noise/*sigma_spatial*/, lambda_spatial, sigma_boost_factor, alg->denseSegmentation_allClouds,
				Q_dense, spatial_filter, spatial_cpy, doSpaNormalization);

		}
		std::cout << "crf it done.";
		//		fiddled
		if (it >= 0)
		{
//fucking fiddled:
//if (it == 0)
			lambda_temporal *= 20;
			scale_factor *= anneal_factor;//5;
		}
	}

	//Make sure sparse and dense values are consistent...
	//poor mans assert
	if (out.rows() != seed_cloud_idx.size() || seed_cloud_idx.size() != seed_point_idx.size()){
		std::cout << "Error! in " << __FILE__ << ", line " << __LINE__ << "assertion failed.";
		exit(1);
	}
	for (int j = 0; j < out.rows(); j++){
		if (seed_point_idx[j] >= alg->denseSegmentation_allClouds[seed_cloud_idx[j]].rows() || seed_point_idx[j]<0){
			std::cout << "Error with seedpoint idx! in " << __FILE__ << ", line " << __LINE__ << "assertion failed.";
			exit(1);
		}
		out.row(j) = alg->denseSegmentation_allClouds[seed_cloud_idx[j]].row(seed_point_idx[j]);
	}

//FUCKING FIDDLED! Minimizing via fusing stuff...
	std::vector<int> sparse_labeling(seed_cloud_idx.size());
	for (int j = 0; j < out.rows(); j++){
		out.row(j).maxCoeff(&sparse_labeling[j]);
	}
	if (alg->params.crf_min_by_merge){
		minimizeEnergyByMergingLabels(cuda_dev, Q_dense, sigma_spatial, sigma_temporal, 2, sparse_labeling, out, per_hyp_cost_factor,std::vector<std::pair<int, int>>());
	}

	total.stop();
	std::cout << "\nCRF Took " << total.total() / 1000.0 << " seconds: temporal " << temporal.total() / 1000.0 << " spatial: cpy " << spatial_cpy.total() / 1000.0 << " filter " << spatial_filter.total() / 1000.f << " other: " << other.total() / 1000.0 << "\n";
}


//#define SETRECFILTERSIGTOCURR recursive_filter.setSigma(cloud_as_grid(i,j).curvature );
#define SETRECFILTERSIGTOCURR recursive_filter.setSigma(cloud_as_grid(i,j).curvature*sigma_factor);
//void template
float crf_segmentation::spatial_sequential_crf(
	matrix<explainMe::Point, explainMe::Cloud> & cloud_as_grid, 
	std::vector<matrix<float, std::vector<float>>> & q_by_label_in,
	std::vector<matrix<float, std::vector<float>>> & q_by_label_out,
	float nrgScale, 
	float sigma_factor,
	recursiveFilter & recursive_filter){
	auto & cld = cloud_as_grid;
	int w = q_by_label_in[0].dim1(); int h = q_by_label_in[0].dim2();
	int numLabels = q_by_label_in.size();
	
	auto & q_by_label = q_by_label_out;

	
	float mean_energy_scale = 0;
	int num_mean_energy = 0;
	//dataterm
	//std::vector<Eigen::MatrixXf> dterm(q_by_label.begin(), q_by_label.end());
	//XXXXXXXXXX
	//XXXX<-----
	//XXXXXXXXXX
	std::vector<Eigen::MatrixXf> filtered_oldq_right2left(numLabels, Eigen::MatrixXf(w,h));// = q_by_label; //precompute anticausal filter on the old q values.
	for (auto & mat : filtered_oldq_right2left){
		mat.fill(0);
	}
	//XXXXXXXXXX
	//XXXX^XXXXX
	//--->|<----
	std::vector<Eigen::MatrixXf> filtered_oldq_up_wo_current(numLabels, Eigen::MatrixXf(w, h)); //precompute anticausal filter on the old q values.



	//need to work on 1-q
	int idx = 0;
	for (auto &q : q_by_label_out){
		//q *= -1;
		//q.array() += 1;
		for (int i = 0; i < q.dim1(); i++){
			for (int j = 0; j < q.dim2(); j++){
				q(i, j) = 1 - q_by_label_in[idx](i, j);
			}
		}

		idx++;
	}
	//std::cout << recursive_filter;
	float defaultDist = std::numeric_limits<float>::infinity();
	float meanDist = 0; int num_mean_dist=0;
	for (int i = 0; i < w-1; i++){
		for (int j = 0; j < h; j++){
			// && cld(i,j).z*0 == 0, but NANs are automatically taken care of by >.
			if (cld(i, j).z > 0.01 && cld(i + 1, j).z > 0.01){
				defaultDist = std::min(defaultDist,(cld(i, j).getVector3fMap() - cld(i + 1, j).getVector3fMap()).norm());
				meanDist += (cld(i, j).getVector3fMap() - cld(i + 1, j).getVector3fMap()).norm();
				num_mean_dist++;
			}
		}
	}
	meanDist /= num_mean_dist;

	//	nrgScale = nrgScale / recursive_filter.estimatedEnergyScale2D(meanDist); // /400;
	float mean_noise = 0;
	{int num = 0;
	for (auto & p : cloud_as_grid.getData().points){
		mean_noise += (p.z > 0.01 ? p.curvature : 0);
		num += (p.z > 0.01 ? 1 : 0);
	}
	//std::cout << "mean noise: " << mean_noise / num << "\n";
	mean_noise /= num;
	}


	float tmp, tmp_dist;
	float filtered_oldq_left2right;
	float complete_horiz_filtered_ij;

	//variables to handle missing data in the grid
	Eigen::Vector3f last_valid_point;
	float last_valid_value; bool is_first_valid_value;
	std::vector<Eigen::Vector3f> last_valid_points(w);
	std::vector<float> last_valid_values(w); 
	std::vector<bool> are_first_vertical_values(w);

	std::vector<Eigen::Vector3f> next_valid_point_up_buff(w*h);
	//to filter over holes: store the end of the hole....
	matrix<Eigen::Vector3f, std::vector<Eigen::Vector3f>> next_valid_point_up(next_valid_point_up_buff, w, h);
	for (int i = w - 1; i >= 0; i--)
	{
		next_valid_point_up(i,0) = cld(i, 0).getVector3fMap();
	}
	for (int j = 1; j <h; j++)
	{
		for (int i = w - 1; i >= 0; i--)
		{
			if (cld(i, j).z > 0.01){ // && cld(i,j).z*0 == 0, but NANs are automatically taken care of by >.
				next_valid_point_up(i, j) = cld(i, j).getVector3fMap();
			}
			else{
				next_valid_point_up(i, j) = next_valid_point_up(i, j - 1);
			}
		}
	}


	//backward filter the old (un-updated) values and store them
	for (int l = 0; l < numLabels; l++)
	{
		//vertical up pass.
		std::fill(are_first_vertical_values.begin(), are_first_vertical_values.end(), true);
		for (int j = h - 1; j >= 0; j--)
		{
			//horizontal anticausal pass.
			//boundary:
			is_first_valid_value = true;
			//filtered_oldq_right2left(i,j) will store:
			//a) the anticausal filtered result up to the point cld(i,j) if cld(i,j) is valid.
			//b) the anticausal result up to the point cld(i,^j), where ^j is is the smallest
			//	number larger j where cld(i,j) is valid. that point is stored in next_valid_point_up(i,j).
			for (int i = w - 1; i >= 0; i--)
			{
				if (cld(i, j).z > 0.01){

					SETRECFILTERSIGTOCURR

					//boundary conditions, last value & distance
					/*tmp = (i == w - 1 ? recursive_filter.boundaryValue(q_by_label[l](i, j),defaultDist) :
					filtered_oldq_right2left[l](i + 1, j));
					tmp_dist = (i == w - 1 ? defaultDist : (cld(i + 1, j).getVector3fMap() - cld(i,j).getVector3fMap()).norm());*/
					if ( is_first_valid_value){
						last_valid_value = recursive_filter.boundaryValue(q_by_label[l](i, j), defaultDist);
						is_first_valid_value = false;
						tmp_dist = defaultDist;
					}
					else{
						tmp_dist = (last_valid_point - cld(i, j).getVector3fMap()).norm();
					}
					
					
					//store the right->left result.
					filtered_oldq_right2left[l](i, j) = last_valid_value = recursive_filter(q_by_label[l](i, j), last_valid_value, tmp_dist);
					last_valid_point = cld(i, j).getVector3fMap();
					
				}
				//cannot filter to this position!
				else{
					//to treat holes appropriately in the final upfiltering:
					if (!is_first_valid_value){
						tmp_dist = (last_valid_point - next_valid_point_up(i, j)).norm();
						filtered_oldq_right2left[l](i, j) = recursive_filter(0,last_valid_value, tmp_dist);
					}
					else{
						//there should be no case where this value is relevant.
						filtered_oldq_right2left[l](i, j) =  std::numeric_limits<float>::quiet_NaN();
					}
				}
			}
			//horizontal causal pass & vertical pass
			//boundary
			is_first_valid_value = true;
			for (int i = 0; i < w; i++)
			{
				if (cld(i, j).z > 0.01){

					SETRECFILTERSIGTOCURR

					//1.the horizontal causal right2left contribution is computed on the fly
					/*tmp_dist = (i == 0 ? defaultDist : (cld(i, j).getVector3fMap() - cld(i - 1, j).getVector3fMap()).norm());*/
					if (is_first_valid_value){
						//boundary condition.
						filtered_oldq_left2right = recursive_filter.boundaryValue(q_by_label[l](i, j), defaultDist);
						tmp_dist = defaultDist;
						is_first_valid_value = false;
					}
					else{
						tmp_dist = (last_valid_point - cld(i, j).getVector3fMap()).norm();
					}

					filtered_oldq_left2right = recursive_filter(q_by_label[l](i, j), filtered_oldq_left2right, tmp_dist);
					last_valid_point = cld(i, j).getVector3fMap();

					//2.causal + anticausal (stored above):
					//lef2right takes the q value at j,i into account, as does right2left therefore we subtract one contribution
					complete_horiz_filtered_ij = (filtered_oldq_left2right + (filtered_oldq_right2left[l](i,j) - recursive_filter(q_by_label[l](i, j), 0, 0)));

					//3.vertical filtering:
					//vertical boundary condition:

					//problem: missing vertical data....
					if (are_first_vertical_values[i]){
						//tmp = recursive_filter.boundaryValue(q_by_label[l](i, j), defaultDist);
						//not sure about boundary value!
						tmp = recursive_filter.boundaryValue(complete_horiz_filtered_ij, defaultDist);
						tmp_dist = defaultDist;
						are_first_vertical_values[i] = false;
					}
					else{
						tmp = last_valid_values[i];
						tmp_dist = (last_valid_points[i] - cld(i, j).getVector3fMap()).norm(); //claim: when stuff is missing, i.e. the point is filled in, everything works correctly.
					}
					//update last_valid_points, or do i deen this ? ;
					//I believe I coudl refactor last_valid_points out.
					last_valid_points[i] = cld(i, j).getVector3fMap();
					 last_valid_values[i]= recursive_filter(complete_horiz_filtered_ij, tmp, tmp_dist);
					 filtered_oldq_up_wo_current[l](i, j) = recursive_filter(0, tmp, tmp_dist);
				}
				//cannot filter! as the point cld(i,j) is invalid...
				else{
					if (!is_first_valid_value && filtered_oldq_right2left[l](i, j) * 0 == 0)
					{
						tmp_dist = (last_valid_point - next_valid_point_up(i, j)).norm();

						//tmp = recursive_filter(0, filtered_oldq_left2right, tmp_dist);
						//}

						//current signal, q_by_label(i,j) makes no contribution, as it is invalid.  = 0
						complete_horiz_filtered_ij = (recursive_filter(0, filtered_oldq_left2right, tmp_dist) + (filtered_oldq_right2left[l](i, j)));//- recursive_filter(q_by_label[l](i, j), 0, 0)));

						//this will accumulate the horizontal contributions 
						//xxxxxxxx^xxxxxxx
						//xxxx--->|<-----xx
						//xx----->|<-----xx
						if (are_first_vertical_values[i]){
							tmp = recursive_filter.boundaryValue(complete_horiz_filtered_ij, defaultDist);
							are_first_vertical_values[i] = false;
							tmp_dist = defaultDist;
						}
						else{
							tmp = last_valid_values[i]; //filtered_oldq_up[l](i, j + 1); //accumulated values up make sure it is valid!!!!
							//tmp_dist = (next_valid_point_up(i, j) - next_valid_point_up(i, j + 1)).norm();
							tmp_dist = (next_valid_point_up(i, j) - last_valid_points[i]).norm();
						}
						last_valid_points[i] = next_valid_point_up(i, j);

						 last_valid_values[i] = recursive_filter(complete_horiz_filtered_ij, tmp, tmp_dist);
						 filtered_oldq_up_wo_current[l](i, j) = recursive_filter(0, tmp, tmp_dist);
					}
					else{
						//THIS might be problematic. (even though applies only to special cases
						filtered_oldq_up_wo_current[l](i, j) = 0;
						are_first_vertical_values[i] = true;
					}
				}
			}
		}
	}
	//dbg... to output the bw filtered stuff.
	/*for (int i = 0; i < w; i++){
		for (int j = 0; j < h; j++){
			if (cld(i, j).z > 0.01){
				float sm = 0;
				for (int lbl = 0; lbl < numLabels; lbl++){
					sm += filtered_oldq_up_wo_current[lbl](i, j);
				}
				for (int lbl = 0; lbl < numLabels; lbl++){
					q_by_label_out[lbl](i, j) = 1 - filtered_oldq_up_wo_current[lbl](i, j) / sm;
				}
			}
			else{
				for (int lbl = 0; lbl < numLabels; lbl++){
					q_by_label_out[lbl](i, j) = 0;
				}
			}
		}
	}
	return;//*/
	//the sequential crf filter:
	//on the fly left2 right values
	//XXXXXXXXXXXX
	//----->XXXXXX
	Eigen::VectorXf q_updated_filtered_l2r(numLabels); q_updated_filtered_l2r.fill(0);
	//last rows complete values
	//............
	//XXXXXXXXXXXX
	Eigen::MatrixXf q_updated_filtered_ijm1(numLabels, w); q_updated_filtered_ijm1.fill(0);
	//current rows complete values
	//---->|<-----
	//XXXXX.XXXXXX
	Eigen::MatrixXf q_updated_filtered_ij(numLabels, w); q_updated_filtered_ij.fill(0);

	//to handle holes correctly:
	//use the next_valid_point table to look up the next valid position.
	for (int i = w - 1; i >= 0; i--)
	{
		next_valid_point_up(i, h-1) = cld(i, h-1).getVector3fMap();
	}
	for (int j = h-2; j >=0; j--)
	{
		for (int i = w - 1; i >= 0; i--)
		{
			if (cld(i, j).z > 0.01){
				next_valid_point_up(i, j) = cld(i, j).getVector3fMap();
			}
			else{
				next_valid_point_up(i, j) = next_valid_point_up(i, j + 1);
			}
		}
	}
	
	float contribution_q_old, contribution_q_upd, q_min,q_max, Z_i;
	std::fill(are_first_vertical_values.begin(), are_first_vertical_values.end(), true);
	//boundary:
	q_updated_filtered_ijm1.fill(recursive_filter.boundaryValue(1 - 1 / numLabels, defaultDist));

	for (int j = 0; j < h; j++)
	{
		//boundary: here harder than before as the filtered value is unknown. rethink.
		q_updated_filtered_l2r.fill(recursive_filter.boundaryValue(1 - 1 / numLabels,defaultDist));
		is_first_valid_value = true; //first left2 right value for current h.
		for (int i = 0; i < w; i++)
		{
			//valid points:
			if (cld(i, j).z>0.01)
			{
				SETRECFILTERSIGTOCURR

				q_min = std::numeric_limits<float>::infinity();
				q_max = 0;
				for (int l = 0; l < numLabels; l++)
				{
					//old contribution
					//here q_by_label[l](i,j) is yet untouched.
					//Care needs to be taken that it is not part of any term (for the update eq forming a valid fixpoint eq).
					contribution_q_old = filtered_oldq_right2left[l](i, j) - recursive_filter(1 - q_by_label_in[l](i, j), 0,0) //recursive_filter(q_by_label[l](i,j),0) 
						+ filtered_oldq_up_wo_current[l](i, j);


					//treat: missing l2r & boundaries
					contribution_q_upd = 0;
					if (is_first_valid_value)
					{
						contribution_q_upd = recursive_filter(0, q_updated_filtered_l2r(l), defaultDist);
					}
					else{
						tmp_dist = (cld(i, j).getVector3fMap() - last_valid_point).norm();
						contribution_q_upd = recursive_filter(0, q_updated_filtered_l2r(l), tmp_dist);
					}

					//treat missing upper value . transparently via next_valid_point. correct?
					//tmp_dist = (j == 0 ? defaultDist : (cld(i, j).getVector3fMap() - cld(i, j - 1).getVector3fMap()).norm());
					tmp_dist = (j == 0 ? defaultDist : (cld(i, j).getVector3fMap() - next_valid_point_up(i, j - 1)).norm());
					contribution_q_upd += recursive_filter(0, q_updated_filtered_ijm1(l, i), tmp_dist);


					//crf update (subresults)
					//"dterm"
					q_by_label[l](i, j) = q_by_label_in[l](i, j) + contribution_q_upd + contribution_q_old;
					mean_energy_scale += q_by_label_in[l](i, j) + contribution_q_upd + contribution_q_old;
					num_mean_energy++;
					q_min = std::min(q_min, q_by_label_in[l](i, j) + contribution_q_upd + contribution_q_old);
					q_max = std::max(q_max, q_by_label_in[l](i, j) + contribution_q_upd + contribution_q_old);
				}

				//crf update: normalization
				Z_i = 0;
				for (int l = 0; l < numLabels; l++)
				{
//					q_by_label[l](i, j) = exp(-nrgScale*(q_by_label[l](i, j) - q_min));
//FUCKING FIDDLED.
//devide by q_max
if (q_min == q_max){
	q_by_label[l](i, j) = 1;
}
else { q_by_label[l](i, j) = exp(-nrgScale*((q_by_label[l](i, j) - q_min) / q_max)); }
if (q_max < 0){ std::cout << "leq 0!" << q_max; }
					Z_i = Z_i + q_by_label[l](i, j);
				}
				for (int l = 0; l < numLabels; l++)
				{
					q_by_label[l](i, j) = q_by_label[l](i, j) / Z_i;
					//on the fly causal filtering
					//q_updated_filtered_l2r is initialized to a sensible boundary constraint. correct?
					tmp_dist = (is_first_valid_value ? defaultDist:
						(cld(i, j).getVector3fMap() - last_valid_point).norm());
					q_updated_filtered_l2r(l) = recursive_filter(1 - q_by_label[l](i, j), q_updated_filtered_l2r(l), tmp_dist);
					//store the value
					q_updated_filtered_ij(l, i) = q_updated_filtered_l2r(l);
				}

				//have to be reset outside the label loops.
				last_valid_point = cld(i, j).getVector3fMap();
				is_first_valid_value = false;
			}
			//current point is invalid...
			else{
				//need only to treat q_updated. q_old will be correct for all valid pixels.
				//q_updated__filtered_l2r handles holes already.
				//but q_updated_filtered_ij needs to accumulate values for the horizontal filtering.
				for (int l = 0; l < numLabels; l++)
				{
					if (!is_first_valid_value)
					{
						tmp_dist = (next_valid_point_up(i, j) - last_valid_point).norm();
						q_updated_filtered_ij(l, i) = recursive_filter(0, q_updated_filtered_l2r(l), tmp_dist);
					}
					else{
						//correct?
						q_updated_filtered_ij(l, i) = std::numeric_limits<float>::quiet_NaN();
					}
					//set output to arbitrary value...
					q_by_label[l](i, j) = 0;// std::numeric_limits<float>::quiet_NaN();
				}
				
			}//*/
		}
		//debug
		//}return; {int j = 0;


		//anticausal pass on the last line of q_by_label(updated values, store in q_updated_filtered.
		//rename buffer. no need for more memory
		auto &  q_updated_filtered_r2l = q_updated_filtered_l2r;
		//boundary
		q_updated_filtered_r2l.fill(recursive_filter.boundaryValue(1 - 1 / numLabels, defaultDist));
		is_first_valid_value = true;
		for (int i = w - 1; i >= 0; i--){
			if (cld(i, j).z>0.01)
			{
				SETRECFILTERSIGTOCURR
				for (int l = 0; l < numLabels; l++)
				{

					if (is_first_valid_value)
					{
						tmp_dist = defaultDist;
					}
					else{
						tmp_dist = (cld(i, j).getVector3fMap() - last_valid_point).norm();
					}
					//left->right filtered val		right to left without current val
					q_updated_filtered_ij(l, i) = q_updated_filtered_ij(l, i) + recursive_filter(0, q_updated_filtered_r2l(l), tmp_dist);

					//anticausal (on the fly
					q_updated_filtered_r2l(l) = recursive_filter(1 - q_by_label[l](i, j), q_updated_filtered_r2l(l), tmp_dist);

					//vertical										//stores the current scanline filtered value
					if (are_first_vertical_values[i]){
						tmp_dist = defaultDist;
					}
					else{
						tmp_dist = (cld(i, j).getVector3fMap() - last_valid_points[i]).norm();
					}
					q_updated_filtered_ij(l, i) = recursive_filter(q_updated_filtered_ij(l, i), q_updated_filtered_ijm1(l, i), tmp_dist);

				}
				//needs to happen outside the label loop.
				last_valid_points[i] = last_valid_point = cld(i, j).getVector3fMap();
				are_first_vertical_values[i] = is_first_valid_value = false;
			}
			//cld i j invalid.
			else
			{

				//if horizontal filtering works,  I'll use it.
				//test if horiz filtering is possible: both directions needed??
				if (!is_first_valid_value && q_updated_filtered_ij(0, i) * 0 == 0){
					for (int l = 0; l < numLabels; l++)
					{
						//horizontal pass
						//interpolating r2l to the next_up position.
						tmp_dist = (next_valid_point_up(i, j) - last_valid_point).norm();
						q_updated_filtered_ij(l, i) = q_updated_filtered_ij(l, i) + recursive_filter(0, q_updated_filtered_r2l(l), tmp_dist);

						if (are_first_vertical_values[i]){
							tmp_dist = defaultDist;
						}
						else{
							tmp_dist = (next_valid_point_up(i, j) - last_valid_points[i]).norm();
						}

						q_updated_filtered_ij(l, i) = recursive_filter(q_updated_filtered_ij(l, i), q_updated_filtered_ijm1(l, i), tmp_dist);
					}
					are_first_vertical_values[i] = false;
					last_valid_points[i] = next_valid_point_up(i, j);
					//but do not update last valid point.
				}
				else{
					//horiz filtering not possible => don't do vertical filtering. That is keep value if possibble.
					for (int l = 0; l < numLabels; l++)
					{
						if (!are_first_vertical_values[i]){
							q_updated_filtered_ij(l, i) = q_updated_filtered_ijm1(l, i);
						}
						//correct??
						else{
							//like bdry...
							q_updated_filtered_ij(l, i) = q_updated_filtered_ijm1(l, i); //(1 - 1.0 / numLabels);
						}
					}
				}
				
			}//*/
		}//end causalanticausal horizontal + single vertical

		//dbg
		if (q_updated_filtered_ij.hasNaN()){
			std::cout << "line:" << __LINE__ << "\n";
			std::cout << "q_updated_f_ij has a NAN, j=" << j;
			exit(1);
		}
		/*for (int i = w - 1; i >= 0; i--){
			float sm = 0;
			for (int l = 0; l < numLabels; l++){
				q_by_label[l](i, j) = q_updated_filtered_ij(l, i);
				sm += q_by_label[l](i, j);
			}
			for (int l = 0; l < numLabels; l++){
				q_by_label[l](i, j) /= sm;
			}
		}*/
		q_updated_filtered_ijm1 = q_updated_filtered_ij;
	}

	//std::cout << "\nmean energy scale:" << mean_energy_scale / num_mean_energy<< "\n";
	return mean_energy_scale / num_mean_energy;
}

