#include "rawDataReaderKinect2.h"
#include "fileReader.h"

#include "cudaTool.h"
#include "cudaFilter.h"
#include <limits>
#include <sstream>
#undef min
#undef max
#include <cmath>

#include <boost/regex.hpp>
#include <boost/filesystem.hpp>

std::string findSensorFileKin2(std::string folder)
{
	
	std::string sensorFile = "";
	std::string query =  ".*\\.kin2";

	//find the file describing the sensor.

	const boost::regex my_filter( query );
	boost::filesystem::directory_iterator end_itr; // Default ctor yields past-the-end

	for( boost::filesystem::directory_iterator i( folder ); i != end_itr; ++i )
	{
		// Skip if not a file
		if( !boost::filesystem::is_regular_file( i->status() ) ) continue;

		boost::smatch what;

		auto str = i->path().filename().string();
		//Stop if match.
		if(boost::regex_match( str, what, my_filter ) ){
			sensorFile = str;
			std::stringstream ss2;
			ss2 << folder << "/" << sensorFile;
			return ss2.str();
		}
		
	}

	std::cout << "\nError: .sensor file not found!\n";
	std::stringstream ss2;
	ss2 << folder << "/" << sensorFile;
	return ss2.str();

	/*std::stringstream ss; 
	ss << folder << "/*.kin2";

	WIN32_FIND_DATA found_file;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	hFind = FindFirstFile(ss.str().c_str(), & found_file);

	if(hFind != INVALID_HANDLE_VALUE){
		sensorFile = (std::string(found_file.cFileName));

	}
	else{
		std::cout << "\nError: .sensor file not found!\n";
	}

	FindClose(hFind);
	
	std::stringstream ss2;
	ss2 << folder << "/" << sensorFile;
	return ss2.str();

	*/

	
}


rawDataReaderKinect2::rawDataReaderKinect2(std::string folder):
	cloud(new explainMe::Cloud())
{
	std::string sensor_file = findSensorFileKin2(folder);
	sensorDescription = new kinectV2Description();//new RangeSensorDescription(sensor_file/**desc*/, RangeSensorDescription::KIN2);
	sensorDescription->loadFromFileKin2(sensor_file);

	reader = new fileReader(folder, "depth_", "ir_");
	cuda = new cudaTool(sensorDescription->descriptionData());
	filter = new cudaFilter(sensorDescription->descriptionData());
	frame = 0;
	
	cloud->resize(reader->frameWidth() * reader->frameHeight());
	cloud->width = reader->frameWidth();
	cloud->height = reader->frameHeight();
}


rawDataReaderKinect2::~rawDataReaderKinect2(void)
{
	delete sensorDescription;
	delete reader;
	delete cuda;
	delete filter;
}

size_t rawDataReaderKinect2::numberOfFrames(){
	return reader->numberOfFrames();
}

bool rawDataReaderKinect2::loadFrame(int frame, bool cleanUp, float maxDist, bool bilateralFilter, float sig_z){
	bool gotData = false;
	
	if(frame < reader->numberOfFrames()){
		reader->readFrame(frame,depthBuffer,IRBuffer);
		frame++;
		gotData = true;
	}
	/////////////////////////////////////
	//compute normals, and float buffer.
	///////////////////////////////////////

	if(gotData){
		int width = reader->frameWidth();
		int numPx = reader->frameWidth()* reader->frameHeight();

		//ensure there is enough space allocated
		depthFloatBuffer.resize(numPx);
		normalFloatBuffer.resize(numPx*3);

		for(int i = 0; i < depthBuffer.size(); i++){
			depthFloatBuffer[i] = depthBuffer[i]/1000.f;
		}
		cuda->dev_setData(&depthFloatBuffer[0]);
		
		if (bilateralFilter){
			cuda->bilateralFilter(3, sig_z);
			cuda->dev_getData(&depthFloatBuffer[0]);
		}
		else{
			cuda->bilateralFilter();
		}
		//cuda->skipFiltering();
		cuda->computeNormals(cleanUp);
		cuda->dev_getNormals(&normalFloatBuffer[0]);


	}

	//std::cout << "clean up " << (cleanUp ? "true": "false");
	recomputeClouds();
	if(gotData ){
		int cleanUpSize = (cleanUp ? 10: 1);
		cleanUpFrame(cleanUpSize);
		//far away
		if(maxDist *0 == 0){
			float nan = std::numeric_limits<float>::quiet_NaN();
			for(int i = 0; i < cloud->size(); i++){
				if(cloud->at(i).z > maxDist){
					cloud->at(i).x = nan;
					cloud->at(i).y = nan;
					cloud->at(i).z = nan;
				}
			}
		}
	}
	return gotData;
	
}

std::vector<std::string> rawDataReaderKinect2::fileNames(int frame){
	return reader->fileNames(frame);
}

void rawDataReaderKinect2::cleanUpFrame(int width_2){
	auto& invalid = buffer_i;
	auto & points =  cloud->points;
	buffer_f.resize(points.size());
	invalid.resize(points.size());
	for(int i = 0; i < points.size(); i++)
	{
		invalid[i] = 
			(points[i].z == 0 || points[i].normal_z * 0 != 0 || 
			points[i].getNormalVector3fMap().squaredNorm() < 0.2f ? 1:0);
		//depth[i] = points[i].z;
	}

	filter->dev_setMask(invalid.data());
	filter->dev_setData(depthFloatBuffer.data());
	filter->flyingPixels(width_2);

	filter->dev_getData(buffer_f.data());

	for(int i = 0; i < points.size(); i++)
	{
		if(buffer_f[i] > 0.5f)
		{
			invalid[i] = 1;
			//points[i].r = points[i].g = points[i].b = 0;
			points[i].z = std::numeric_limits<float>::quiet_NaN();
		}
		else{
			invalid[i] = 0;
			//points[i].b = 0;
			//points[i].r = points[i].g = 255;

		}
	}

}

void rawDataReaderKinect2::recomputeClouds(){


	int width = reader->frameWidth();
	int numPx = reader->frameWidth()* reader->frameHeight();

	auto & points = cloud->points;
	float qnan = std::numeric_limits<float>::quiet_NaN();

	for(int i = 0; i < numPx; i++){
		auto & p = points[i];

		p.z = depthBuffer[i] / 1000.f;
		//p.x = CAST_I2X(i%width, p.z);
		//p.y = CAST_J2Y(i/width, p.z);
		sensorDescription->unproject(i%width,i/width,p.z,p.x,p.y);
		p.normal_x = normalFloatBuffer[i];
		p.normal_y = normalFloatBuffer[i+numPx];
		p.normal_z = normalFloatBuffer[i+numPx*2];

		//from the kinect2 example
		//byte intensity = std::max(std::min(rawIR[i]/65536.0 /(0.08*3), 1.0), 0.0)*255;
		//uint8_t intensity = std::max(std::min( std::log(static_cast<float>(IRBuffer[i]))/std::log(2) * 32 -256, 255.0),0.0);
		//std::max(std::min( std::log(static_cast<float>(ir[i* mat_ir.cols + j]))/std::log(2) * 16 -128, 255.0), 0.0);
		//seems to be good post processing...
		uint8_t intensity = std::max(std::min( std::log(static_cast<float>(IRBuffer[i]))/std::log(2) * 16 -128, 255.0),0.0);
		p.r = 	p.g = 	p.b = intensity;
		
		//p.curvature =static_cast<float>(IRBuffer[i]);//noise predicted somewhat using the intensity..
		p.curvature = sensorDescription->sigmaHat(i%sensorDescription->width(), i / sensorDescription->width(), p.z, IRBuffer[i], p.getNormalVector3fMap().dot(-p.getVector3fMap().normalized()));//noise predicted somewhat using the intensity..
	}
}