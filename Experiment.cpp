#include "Experiment.h"
#include <qdir.h>


#include <Eigen/Geometry>
#include "boost/filesystem.hpp"
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>
#include <boost/format.hpp>

#include "Initializer.h"
#include "PathsByOpticalFlow.h"
#include "InitByEnergyMinimization.h"
#include "InitByEnergyMinimization_smoothness.h"
#include "CloudTools.h"
#include "CloudColorizer.h"

#include "StopWatch.h"
#include "Chronist.h"

#include "cudaDataterm.h"
#include "cudaDatatermTools.h"
#include "gpu_sparse2denseInterpolation.h"
#include "crf_segmentation.h"
#include <csignal>

#ifdef _MSC_VER
#define sqrtf std::sqrtf
#define logf std::logf
#endif

bool Experiment::abortExperiment = false;

//macros for faster ctrl-c handling
#define DO_WITH_ABORT_CHECK(code) code; if (abortExperiment){return;}
#define DO_TIMED_WITH_ABORT_CHECK(code, timer) timer.restart();code; timer.stop(); if (abortExperiment){return;}
#define DO_TIMED_WITH_ABORT_CHECK_RETURN(code, timer, toreturn) timer.restart();code; timer.stop(); if (abortExperiment){return (toreturn);}
#define DO_TIMED_WITH_ABORT_CHECK2(code, timer1, timer2) timer1.restart();timer2.restart();code; timer2.stop(); timer1.stop(); if (abortExperiment){return;}

QExperimentApplication::QExperimentApplication(int argc, char ** arg) :QApplication(argc, arg)
{
	//parse & load parameters
	algorithmParameters params;
	experimentParameters exp_params;
	parse(argc, arg);

	std::string outputFolder = cmd_line_str["output_file"];
	std::string parameterFile = cmd_line_str["experiment_file"];

	std::cout << "Loading parameters...\n";
	parameterLoader::loadFromFile(parameterFile, exp_params, params);
	
	std::cout << "Loading data and initializing buffers...\n";
	//reader to handle the input data
	CloudReader::Ptr data = exp_params.createReader();
	//init buffers
	alg = explainMe_algorithm_withFeatures::Ptr(new explainMe_algorithm_withFeatures(data, params));

	//the actual algorithm
	experiment = Experiment::Ptr(new Experiment(outputFolder, exp_params, alg));

	//the chronist handles rendering and documentation. 
	//if triggered it will query the needed information from the alg,
	//and either dump the data or visualisations of the current state.
	chronist = Chronist::Ptr(new Chronist(alg));

	//gui with realtime info about the algorithm, Lives in GUI thread. 
	//the statswin will periodically query the state of the algorithm and
	// view some information.
	statsWindow * stats_win;

	//dispatch the experiment in own worker thread after setting up documentation in stats_win
	auto future = statsWindow::setupLiveStatsAndDispatch<Experiment>(*experiment, &stats_win);
	stats_win->setGeneralInfo(parameterLoader::to_long_string(exp_params, params));

	//Treatment of ctrl-c
	//It will set an abort flag and wait for the worker thread to see the flag and quit.
	catchInterruptSignals({ SIGINT, SIGTERM });
	experiment_returns.setFuture(future);
	//close windows upon completion
	connect(&experiment_returns, SIGNAL(finished()), this, SLOT(closeAllWindows()));


	//The chronist itself lives in the GUI thread, and documentation is triggered via qt signals.
	//Here: connect experiment signals to chronist slots in the right way.
	//(Ab)using qt connect with blocking connection for inter-thread communication
	//(execution of the experiment is halted during documentation in order not to document an
	//inconsistent state)
	connect(experiment.get(), SIGNAL(document_stats_png(const std::string &, const std::string &)),
		stats_win, SLOT(captureTimingsAndGraphsAsImage(const std::string &, const std::string &)),
		Qt::BlockingQueuedConnection);
	connect(experiment.get(), SIGNAL(document_alignments(const std::string &)),
		chronist.get(), SLOT(document_alignmentSets(const std::string &)),
		Qt::BlockingQueuedConnection);
	connect(experiment.get(), SIGNAL(document_alignments_fancy(const std::string &)),
		chronist.get(), SLOT(document_alignmentSets_byMovingStuff(const std::string &)),
		Qt::BlockingQueuedConnection);
	connect(experiment.get(), SIGNAL(document_weights(const std::string &)),
		chronist.get(), SLOT(document_denseWeights(const std::string &)),
		Qt::BlockingQueuedConnection);
	connect(experiment.get(), SIGNAL(document_weights_insecurity(const std::string &)),
		chronist.get(), SLOT(document_insecurityOfW(const std::string &)),
		Qt::BlockingQueuedConnection);
	connect(experiment.get(), SIGNAL(document_w_as_hyp_probs(const std::string &)),
		chronist.get(), SLOT(document_w_as_hyp_probs(const std::string &)),
		Qt::BlockingQueuedConnection);
	connect(experiment.get(), SIGNAL(document_w_i(const std::string &, int)),
		chronist.get(), SLOT(document_weight(const std::string &, int)),
		Qt::BlockingQueuedConnection);
	connect(experiment.get(), SIGNAL(document_per_pixel_values(const std::string &, std::vector<std::vector<float>> &, float)),
		chronist.get(), SLOT(document_denseperPixelValues(const std::string &, std::vector<std::vector<float>> &, float)),
		Qt::BlockingQueuedConnection);
	connect(experiment.get(), SIGNAL(document_seg_png(const std::string &)),
		chronist.get(), SLOT(document_seg_png_masks(const std::string &)),
		Qt::BlockingQueuedConnection);

}

void QExperimentApplication::parse(int argc, char ** arg){
	if (argc < 2){
		std::cout << "Usage: \nexplainMe.exe experiment_params.txt [-o outputFolder] [-v] [-xd]\n\t [-v]:\tshow life visualization"
			<< "\n\t [-xd]:\t dont document results";
		exit(0);
	}
	cmd_line_str["full_path_program"] = arg[0];
	cmd_line_str["experiment_file"] = arg[1];
	cmd_line_bool["show"] = false;
	cmd_line_bool["document_results"] = true;

	for (int i = 2; i< argc; i++){
		if (arg[i][0] == '-' && arg[i][1] == 'o'){
			if (argc > i + 1){
				cmd_line_str["output_file"] = arg[i + 1];
				i++;
			}
			else{
				std::cout << "Error parsing -o";
				std::cout << "Usage: \nexplainMe.exe experiment_params.txt [-o outputFolder] [-v]";
				exit(0);
			}
		}
		else if (arg[i][0] == '-' && arg[i][1] == 'v'){
			cmd_line_bool["show"] = true;
		}
		else if (arg[i][0] == '-' && arg[i][1] == 'x' && arg[i][2] == 'd'){
			cmd_line_bool["document_results"] = false;
		}
	}
}


void QExperimentApplication::catchInterruptSignals(const std::vector<int>& quitSignals,
	const std::vector<int>& ignoreSignals) {

	auto handler = [](int sig) ->void {
		printf("\nquit the application (user request signal = %d).\n", sig);
		Experiment::abort();
		explainMe_algorithm_withFeatures::abort();
		ComputeWeights::abort();
		InitByEnergyMinimization::abort();
		signal(sig, SIG_IGN);//oh just be patient ,please...
	};

	// all these signals will be ignored.
	for (int sig : ignoreSignals)
		signal(sig, SIG_IGN);

	// each of these signals calls the handler (quits the QCoreApplication).
	for (int sig : quitSignals)
		signal(sig, handler);
}

bool QExperimentApplication::notify(QObject *receiver, QEvent *event){
	try{
		return QApplication::notify(receiver, event);
	}
	catch (std::exception & e){
		std::cout << "QApp cought Excpetion...";
		std::cout << e.what();
		throw e;
	}
}



cudaDataterm * newcudaDataterm(explainMe_algorithm_withFeatures::Ptr alg){
	sensorDescriptionData sdd = alg->reader->getDescription()->descriptionData();
	auto * gpuDataterm = new cudaDataterm(sdd, alg->params.clamp_distSqr, alg->params.occlusion_basepenalty,
		alg->allClouds.size(), alg->params.samplesPerFrame);
	std::vector<float> buff(alg->reader->getDescription()->width() * alg->reader->getDescription()->height(),0), 
		buff2,buff_nx, buff_ny, buff_nz;
	buff2 = buff; buff_nx = buff; buff_ny = buff; buff_nz = buff;
	for (int i = 0; i < alg->allClouds_asGrids.size(); i++){
		auto & cld = *alg->allClouds_asGrids[i];
		for (int j = 0; j < cld.size(); j++)
		{
			buff[j] = ((cld.at(j).z  > 0.0001 && cld.at(j).z * 0 == 0) ? cld.at(j).z : 0.f);
			buff2[j] = cld.at(j).curvature;// *0.5;
			buff_nx[j] = cld.at(j).normal_x;
			buff_ny[j] = cld.at(j).normal_y;
			buff_nz[j] = cld.at(j).normal_z;
		//if (!(cld.at(j).curvature >= 0.03))
		//	{  buff2[j] = std::sqrtf(0.001); }
		}

		gpuDataterm->pushCloud2Gpu(buff, buff2, buff_nx, buff_ny, buff_nz);

	}
	return gpuDataterm;
}

void setGpuMotion(cudaDataterm * gpuDatatermComp, MotionHypotheses & motions){
	std::vector<std::vector<Eigen::Matrix4f>> all_al_inv = motions.getAlignmentSetsInv();
	std::vector<std::vector<float*>> a, a_inv;
	for (int i = 0; i < motions.numHypotheses(); i++)
	{
		auto & al = motions.getAlignmentSet(i);
		auto & al_inv = all_al_inv.at(i);
		a.push_back(std::vector<float*>(al.size()));
		a_inv.push_back(std::vector<float*>(al.size()));
		for (int j = 0; j < al.size(); j++){
			a[i][j] = al[j].data();
			a_inv[i][j] = al_inv[j].data();
		}
	}
	gpuDatatermComp->setMotionSet(a, a_inv);
}

Experiment::Experiment(const std::string & outputFolder_, experimentParameters & params, explainMe_algorithm_withFeatures::Ptr alg_)
{
	myParams = params;
	mystats = &dummyStats;//to transparently run with and without stats window.
	alg = alg_;
	outputFolder = outputFolder_;

	//initialize gpu data/push cloud data to the gpu.
	gpuDatatermComp = newcudaDataterm(alg);
	//meta information
	last_error = std::numeric_limits<float>::infinity();
	current_error = std::numeric_limits<float>::infinity();
	last_numHyps = 0, current_numHyps = 0;
}


Experiment::~Experiment(void)
{
	std::cout << "Experiment is killed....";
}


void Experiment::step5_graphcut_withDetailedDocumentation(explainMe_algorithm_withFeatures::Ptr alg,
														  int mainStep,
														  explainMe_algorithm_withFeatures::MatrixXf_rm & hypothesis_probability,
														  statsKeeper * stats)
{
	StopWatch & selectSeedsTimer = stats->getSegmentationStats()["Select Seeds"];
	StopWatch & dataTermTimer = stats->getSegmentationStats()["Dataterm"];
	StopWatch & graphcutTimer = stats->getSegmentationStats()["Graphcut"];
	StopWatch & segTimer = stats->getSegmentationStats().total;
	StopWatch & docTimer = stats->getDocStats().total;
	StopWatch & gcState = stats->getDocStats()["PreGC state"];
	segTimer.restart();

	std::vector<pcl::PointXYZINormal> seed_points;
	std::vector<Eigen::Vector3f> seed_normals;
	std::vector<int> seed_frame, seed_point_idx,labeling;
	
	//explainMe_algorithm_withFeatures::MatrixXf_rm hypothesis_probability;
	
	//HarrisSampler sampler1;
	//ISS3dSampler sampler1;
	//RandomPerLabelSampler sampler1,sampler2;
	RandomSampler sampler1;
	UseOldSamples sampler2;

	CloudSampler * samplerToUse;
	if(mainStep == 0){
		samplerToUse = &sampler1;
	}
	else{
		samplerToUse = &sampler2;
	}
	
	//resets w to random and initializes trajectories.
	DO_TIMED_WITH_ABORT_CHECK(
		alg->step5_1_select_seed_points(seed_points, seed_normals, seed_frame, seed_point_idx,samplerToUse);, selectSeedsTimer)

		//computes hyp probabilities and sets w to the max coeff.
	DO_TIMED_WITH_ABORT_CHECK(
		alg->step5_2_initialLabeling(seed_points, seed_normals, seed_frame, hypothesis_probability, labeling);, dataTermTimer)

	//Doc
	if (myParams.docLevel >= experimentParameters::LVL_4_FULL){
		docTimer.restart();
		gcState.restart();
		alg->step3_updateDenseSeg_from_sparse(true);
		Q_EMIT(document_w_as_hyp_probs(this->outputFolder + myParams.gitTag  + "_pregc_hrd" + std::to_string(mainStep)));
		Q_EMIT(document_alignments(this->outputFolder + myParams.gitTag  + "_asets" + std::to_string(mainStep)));
		docTimer.stop();
		gcState.stop();
	}

	//alg->step5_3_do_alpha_beta_expansion(seed_points,seed_normals,seed_frame,hypothesis_probability);
	DO_TIMED_WITH_ABORT_CHECK(
		alg->step5_3_do_alpha_beta_expansion_one_global(seed_points, seed_frame, seed_point_idx, hypothesis_probability);, graphcutTimer)

	segTimer.stop();
}


std::vector<std::vector<std::vector<float>>> Experiment::step5_crf_withDocumentation(
	explainMe_algorithm_withFeatures::Ptr alg,
	int mainStep,
	float kill_threshold,
	explainMe_algorithm_withFeatures::MatrixXf_rm & hypothesis_probability,
	statsKeeper * stats,
	bool addErrorbasedProposals)
{
	std::cout << "\nStarting CRF step in Experiment...\n";
	int numOutlierHyps = 5;
		
	StopWatch & selectSeedsTimer = stats->getSegmentationStats()["Select Seeds"];	StopWatch & crfTimer = stats->getSegmentationStats()["CRF"];
	StopWatch & segTimer = stats->getSegmentationStats().total;						StopWatch & docTimer = stats->getDocStats().total;
	StopWatch & unnecessaryAllocs = stats->getSegmentationStats()["cpu allocs"];
	segTimer.restart();

	//all kinds of allocs that could also be done once only.
	std::vector<pcl::PointXYZINormal> seed_points;
	std::vector<Eigen::Vector3f> seed_normals;
	std::vector<int> labeling, seed_cloud_idx, seed_point_index;
	explainMe_algorithm_withFeatures::MatrixXf_rm hypothesis_totalWeight;

	//HarrisSampler sampler1;//ISS3dSampler sampler1;//RandomPerLabelSampler sampler1,sampler2;
	RandomSampler sampler1;
	UseOldSamples sampler2;
	CloudSampler * samplerToUse = (mainStep == 0 ? (CloudSampler *)&sampler1 : (CloudSampler *)&sampler2);

	
	//resets w to random and initializes trajectories.
	std::cout << "Select Seeds...";
	DO_TIMED_WITH_ABORT_CHECK_RETURN(
		alg->step5_1_select_seed_points(seed_points, seed_normals, seed_cloud_idx, seed_point_index, samplerToUse); , selectSeedsTimer, std::vector<std::vector<std::vector<float>>>())

		std::cout << "done\n";
	//prepareing some temp databuffers.
	unnecessaryAllocs.restart();
	
	hypothesis_probability.resize(seed_points.size(), alg->motions.numHypotheses());
	hypothesis_totalWeight = hypothesis_probability;
	hypothesis_probability.fill(alg->params.occlusion_basepenalty);//1e-3 //0
	hypothesis_totalWeight.fill(0);

	unnecessaryAllocs.stop();

	std::cout << "Starting CRF...\n";
	//CRF based segmentation.
	crf_segmentation segmenter(alg, alg->params.maxItCRF);
#ifdef USE_SEQUENTIAL_CRF_AND_CRASH
	DO_TIMED_WITH_ABORT_CHECK_RETURN(
		segmenter.doSparseSequentialCrf(
		gpuDatatermComp, seed_points, seed_cloud_idx,seed_point_index,
		alg->params.lambda_smoothness_temporal,
		alg->params.lambda_crf_s,
		std::sqrtf(alg->params.sig_windowSize_sqr),
		alg->params.sigma_dist,
		alg->params.crf_sigma_sp_factor,
		alg->params.max_annealing_factor_crf,
		kill_threshold,
		hypothesis_probability,
		mainStep); , crfTimer,std::vector<std::vector<std::vector<float>>>())
#else
	DO_TIMED_WITH_ABORT_CHECK_RETURN(
		segmenter.doSparseCrf(
			gpuDatatermComp, seed_points, seed_cloud_idx, seed_point_index,
			alg->params.lambda_smoothness_temporal,
			alg->params.lambda_crf_s,
			sqrtf(alg->params.sig_windowSize_sqr),
			alg->params.sigma_dist,//dead parameter
			alg->params.crf_sigma_sp_factor,
			alg->params.max_annealing_factor_crf,
			kill_threshold,
			alg->params.per_hyp_cost_factor,
			hypothesis_probability,
			mainStep); , crfTimer, std::vector<std::vector<std::vector<float>>>())
#endif
	
	segTimer.stop();
	
	std::vector<std::vector<std::vector<float>>> proposals;
//Hacked in doc and outlier hypothesis computation:
	//make sure gpu buffers are all initialized curroectly.
	//cudaDatatermTools::setGPUSparsePoints(gpuDatatermComp, seed_points, seed_cloud_idx);
	//cudaDatatermTools::setGpuMotion(gpuDatatermComp, alg->motions);
	//gpuDatatermComp->initSparseValues(alg->motions.numHypotheses());

	//purely for documentation and debugging: per pixel error.
	std::vector<std::vector<float>> detailedEnergy = segmenter.computeEnergyFrames(gpuDatatermComp, alg->params.sigma_dist, sqrtf(alg->params.sig_windowSize_sqr), alg->params.crf_sigma_sp_factor);
float max = 0;
float sum = 0; int num = 0;
for (auto & vec : detailedEnergy){
	for (auto v : vec){
		max = (max > v ? max : v);
		sum += v;
		num++;
	}
}
stats->energies.push_back(sum);
last_error = current_error;
current_error = sum;

if (myParams.docLevel > experimentParameters::LVL_1_POST_RM)
{
	std::cout << "Visualization of ppE with  mean " << sum / num << " max " << max;
	Q_EMIT(document_per_pixel_values(this->outputFolder + myParams.gitTag + "_ppixEnergy" + std::to_string(mainStep) + "_" + std::to_string(max), detailedEnergy, 0.5*max));
}

//show energy of hyp 16 and 18. for debugging <----
//detailedEnergy = segmenter.computeEnergyOneHypAlone(gpuDatatermComp, alg->params.sigma_dist, std::sqrtf(alg->params.sig_windowSize_sqr), alg->params.crf_sigma_sp_factor, 16);
//Q_EMIT(document_per_pixel_values(this->outputFolder + myParams.gitTag  + "_ppixE" + std::to_string(16) + "_" + std::to_string(mainStep) + "_" + std::to_string(max), detailedEnergy, max));
//detailedEnergy = segmenter.computeEnergyOneHypAlone(gpuDatatermComp, alg->params.sigma_dist, std::sqrtf(alg->params.sig_windowSize_sqr), alg->params.crf_sigma_sp_factor, 18);
//Q_EMIT(document_per_pixel_values(this->outputFolder + myParams.gitTag  + "_ppixE" + std::to_string(18) + "_" + std::to_string(mainStep) + "_" + std::to_string(max), detailedEnergy, max));
//--------->

if (addErrorbasedProposals)
{
	//randomly select x samples based on the error.
	//but actually I could/should compute a relaxed variant of the errors for this, as I do not want to sample boundary pixels, but simply badly registered points. 
	//This would be like the detailed energy but setting all qs to one instead to the current map result. (temporal oversegmentation and spatial oversegmentation are handled...)
	std::cout << "Computing outlier hyps...";
	//compute the relaxed per pixel energy.
	detailedEnergy = segmenter.computeEnergyFrames(gpuDatatermComp, alg->params.sigma_dist, sqrtf(alg->params.sig_windowSize_sqr), alg->params.crf_sigma_sp_factor, true);
	Q_EMIT(document_per_pixel_values(this->outputFolder + myParams.gitTag  + "_ppixERelaxed" + std::to_string(mainStep) + "_" + std::to_string(max), detailedEnergy, max));

	
	//compute quantile, clamp low energy points and compute sum
	sum = 0;
	max = 0;
	int max_frm = 0, frm = 0;
	std::vector<float> quantiles;
	for (auto & vec : detailedEnergy){
		float tmp_max = 0;
		int p_idx = 0;
		for (auto v : vec){
			v/= alg->allClouds[frm]->at(p_idx).curvature;
			tmp_max = (tmp_max > v ? tmp_max : v);
			//sum += v;
			p_idx++;
		}
		if (max < tmp_max){
			max_frm = frm;
			max = tmp_max;
		}
		

		std::vector<float> bla = detailedEnergy[frm];
		//using integer arithmetic
		std::nth_element(bla.begin(), bla.begin() + bla.size() * 19 / 20, bla.end());
		float tmp_quantile = (bla[bla.size() * 19 / 20]);
		quantiles.push_back(tmp_quantile);

		frm++;
	}
	std::nth_element(quantiles.begin(), quantiles.begin() + quantiles.size() * 9 / 10, quantiles.end());
	float quantile = quantiles[quantiles.size() * 9 / 10];

		
	sum = 0;
	for (auto & vec : detailedEnergy){
		for (auto & v : vec){
			v = (v >quantile ? v : 0);
			sum += v;
		}
	}
	Q_EMIT(document_per_pixel_values(this->outputFolder + myParams.gitTag  + "_ppixEquantiled" + std::to_string(mainStep) + "_" + std::to_string(quantile), detailedEnergy, quantile));

	//generate outlier segments by sampling points based on their error magnitude and creating fuzzy tubes around them via filtering.

	std::vector<float> randoms;
	std::vector<int> res_c_x, res_c_y, res_f,res_label;
	std::vector<explainMe::Point> res_point;
	for (int i = 0; i < numOutlierHyps; i++){
		randoms.push_back((float)rand() / (RAND_MAX));
	}
	std::sort(randoms.begin(), randoms.end(),std::greater<float>());
	//*
	float curr = 0;
	for (int frm = 0; frm < detailedEnergy.size(); frm++){
		auto & vec = detailedEnergy[frm];
		for (int p_i = 0; p_i < alg->allClouds[frm]->size() ; p_i++){
			if (randoms.size() == 0){
				break;
			}
			float v = vec[p_i];
			curr += v;
			if (curr/sum >= randoms.back()){
				std::cout << "Proposing seed point: (" << randoms.back() << ")";
				randoms.pop_back();
				std::cout << alg->allClouds[frm]->at(p_i).x << "," << alg->allClouds[frm]->at(p_i).y << "," << alg->allClouds[frm]->at(p_i).z << ", ";
				int thispointslabel, cx, cy;
				alg->denseSegmentation_allClouds[frm].row(p_i).maxCoeff(&thispointslabel);
				std::cout << "labeled: " << thispointslabel;
				alg->reader->getDescription()->project(alg->allClouds[frm]->at(p_i).x, alg->allClouds[frm]->at(p_i).y, alg->allClouds[frm]->at(p_i).z, cx, cy);
				std::cout  << " at " << cx << "," << cy << " (frm " << frm << ")\n";
				res_c_x.push_back(cx);
				res_c_y.push_back(cy);
				res_f.push_back(frm);
				res_label.push_back(thispointslabel);
				res_point.push_back(alg->allClouds[frm]->at(p_i));
				if (randoms.size() == 0){
					break;
				}
			}
		}
		if (randoms.size() == 0){
			break;
		}
	}	
	//for each seed point do a large support tmpo-spatial filtering step & thresholding , multiplied with outlier mask (?), to create a segment proposal.
	//how: as with computeEnergyFrames, just with init at single pixel. That is: map the pixel to all frames, 
	//init the single pixel based on tempo-spatial distance (=temporal filtering)
	//do a spatial filtering pass on top of this.
	std::cout << "Creating spatial hyp labels...";
	for (int i = 0; i < res_f.size(); i++){
		std::cout << "(" << i << "/" << res_f.size() <<")";
		
		//detailedEnergy = segmenter.computeEnergyFrames(gpuDatatermComp, alg->params.sigma_dist, std::sqrtf(alg->params.sig_windowSize_sqr), res_f[i],res_c_x[i],res_c_y[i] );
		detailedEnergy = segmenter.spatioTemporalSmear(gpuDatatermComp, alg->params.sigma_dist, sqrtf(alg->params.sig_windowSize_sqr), res_point[i], res_f[i],res_label[i]);
		max = 0;
		for (auto & vec : detailedEnergy){
			for (auto v : vec){
				max = (max > v ? max : v);
			}
		}

		for (auto & vec : detailedEnergy){
			for (auto v : vec){
				v /= (max / 2);
			}
		}
		if (myParams.docLevel > experimentParameters::LVL_1_POST_RM)
		Q_EMIT(document_per_pixel_values(this->outputFolder + myParams.gitTag  + "_proposal" + std::to_string(mainStep) + "_" + std::to_string(i), detailedEnergy,1));

		proposals.push_back(detailedEnergy);
	}
	
}
	//Document noise.
	if (mainStep == 0 && myParams.docLevel > experimentParameters::LVL_1_POST_RM){
		float sum = 0;
		float max = 0, num = 0;
		std::vector<std::vector<float>> bla(alg->allClouds.size());
		for (int frm = 0; frm < bla.size(); frm++){
			for (int p_i = 0; p_i < alg->allClouds[frm]->size(); p_i++){
				bla[frm].push_back( alg->allClouds[frm]->points[p_i].curvature);
				max = (max > bla[frm][p_i] ? max : bla[frm][p_i]);
				sum += bla[frm][p_i];
				num++;
			}
		}
		std::cout << "Visualization of noise with  mean " << sum / num << " max " << max;
		Q_EMIT(document_per_pixel_values(this->outputFolder + myParams.gitTag + "_fittedNoise" + std::to_string(mainStep) + "_" + std::to_string(max), bla, max));

	}
	//<---- Doc: the crf results without additional fussing
	docTimer.restart();
	if (myParams.docLevel > experimentParameters::LVL_0_ONLYFINALRESULT){
		//documents MAP of densesegmentation.
		Q_EMIT(document_w_as_hyp_probs(this->outputFolder + myParams.gitTag +  "_crf_hrd" + std::to_string(mainStep)));
	}
	//---- per hypothesis and per frame visualization of probabiliteis.
	if (myParams.docLevel >= experimentParameters::LVL_2_POST_RM_MO_crfW)
	{
		for (int i = 0; i < alg->w.cols(); i++){
			Q_EMIT(document_w_i(this->outputFolder + myParams.gitTag  + "_crf_hard" + std::to_string(mainStep) + "_" + std::to_string(i), i));
		}
	}
	docTimer.stop();
	//---->

	//update 
	for (int i : seed_point_index){
		if (i < 0){
			std::cout << "\n\nWTF!!!\n\n";
			break;
		}
	}
	alg->set_sparse_w_to_MAP_update_Trajs(hypothesis_probability, seed_points, seed_cloud_idx, seed_point_index);
	std::cout << "Experiment: " << __LINE__;
	alg->checkTrajectorySeeds();
	return proposals;
	
	
}

void Experiment::step5_graphcut_gpudterm_withDoc(explainMe_algorithm_withFeatures::Ptr alg,
	int mainStep,
	float kill_threshold,
	explainMe_algorithm_withFeatures::MatrixXf_rm & hypothesis_probability,
	statsKeeper * stats)
{
	StopWatch & selectSeedsTimer = stats->getSegmentationStats()["Select Seeds"];	StopWatch & dataTermTimer = stats->getSegmentationStats()["Dataterm"];
	StopWatch & graphcutTimer = stats->getSegmentationStats()["Graphcut"];			StopWatch & segTimer = stats->getSegmentationStats().total;
	StopWatch & docTimer = stats->getDocStats().total;								StopWatch & gcState = stats->getDocStats()["PreGC state"];
	segTimer.restart();

	std::vector<pcl::PointXYZINormal> seed_points;
	std::vector<Eigen::Vector3f> seed_normals;
	std::vector<int> labeling, seed_cloud_idx, seed_point_idx;

	explainMe_algorithm_withFeatures::MatrixXf_rm hypothesis_totalWeight;
	//HarrisSampler sampler1;ISS3dSampler sampler1;RandomPerLabelSampler sampler1,sampler2;
	RandomSampler sampler1;
	UseOldSamples sampler2;

	CloudSampler * samplerToUse;
	if (mainStep == 0){
		samplerToUse = &sampler1;
	}
	else{
		samplerToUse = &sampler2;
	}

	//resets w to random and initializes trajectories.
	DO_TIMED_WITH_ABORT_CHECK(
		alg->step5_1_select_seed_points(seed_points, seed_normals, seed_cloud_idx, seed_point_idx, samplerToUse); , selectSeedsTimer)

	//ugly, allocates new float vector.
	dataTermTimer.restart();
	stats->getSegmentationStats()["cpu allocs"].restart();
	hypothesis_probability.resize(seed_points.size(), alg->motions.numHypotheses());//params.numHypo);
	hypothesis_totalWeight = hypothesis_probability;
	hypothesis_probability.fill(alg->params.occlusion_basepenalty);//1e-3 //0
	hypothesis_totalWeight.fill(0);
	
	stats->getSegmentationStats()["cpu allocs"].stop();


	stats->getSegmentationStats()["gpuAlloc"].restart();
		
	std::vector<float> dataterm_buffer;
	std::vector<pxyz_normal_frame> queries_for_frame;
	std::vector<int> query_idxes;

	cudaDatatermTools::setGPUSparsePoints(gpuDatatermComp, seed_points, seed_cloud_idx);
	cudaDatatermTools::setGpuMotion(gpuDatatermComp, alg->motions);
	gpuDatatermComp->initSparseValues(alg->motions.numHypotheses());
	std::cout << "gpu alloc done.";
	stats->getSegmentationStats()["gpuAlloc"].stop();

	//cheaper alternatives
	/*for (int frame = 0; frame < alg->allClouds.size(); frame++)
	{
		//works
		//gpuDatatermComp->addDataTerm(frame , query_idxes);
		//std::cout << "\ndataterm frame" << frame << "\n";
		stats->getSegmentationStats()["gpu dterm"].restart();
		gpuDatatermComp->addDataTerm(frame);
		//works
		//gpuDatatermComp->addDataTerm_allQueries(frame);

		stats->getSegmentationStats()["gpu dterm"].stop();
	}
	//works too: all frames & all hyps.
	/*/
	stats->getSegmentationStats()["gpu dterm"].restart();
	gpuDatatermComp->addDataTerm_allQueries_allFrames();
	stats->getSegmentationStats()["gpu dterm"].stop();//*/

	//copy dataterm back from gpu
	for (int i = 0; i < alg->motions.numHypotheses(); i++){
		gpuDatatermComp->getSparseValues(i, dataterm_buffer);
		for (int j = 0; j < dataterm_buffer.size(); j++){
			hypothesis_probability(j, i) = dataterm_buffer[j];
		}
	}

	//prepare the computed probs to be used in graphcuts (renormalization)
	alg->cast_distances_to_gc_unaries(hypothesis_probability, false);
	//for visualization:
	alg->set_sparse_w_to_MAP(hypothesis_probability);
	dataTermTimer.stop();

	//<---Doc
	if (myParams.docLevel >= experimentParameters::LVL_4_FULL){
		docTimer.restart();
		gcState.restart();
		alg->step3_updateDenseSeg_from_sparse(true);
		Q_EMIT(document_w_as_hyp_probs(this->outputFolder + myParams.gitTag +  "_pregc_hrd" + std::to_string(mainStep)));
		Q_EMIT(document_alignments(this->outputFolder + myParams.gitTag +  "_asets" + std::to_string(mainStep)));
		docTimer.stop();
		gcState.stop();
	}
	//-->
	//The graphcut.
	//alg->step5_3_do_alpha_beta_expansion(seed_points,seed_normals,seed_frame,hypothesis_probability);
	DO_TIMED_WITH_ABORT_CHECK(
		alg->step5_3_do_alpha_beta_expansion_one_global(seed_points, seed_cloud_idx,seed_point_idx, hypothesis_probability);, graphcutTimer)
	segTimer.stop();
}


void Experiment::run(){
	run_with_stats();
}


void Experiment::do_gc_based_EM_iteration(int mainStep){
	//get timers.
	StopWatch & docTime = mystats->getDocStats().total;					StopWatch & interpolateTime = mystats->getInterpolateStats().total;
	StopWatch & otherTime = mystats->getOtherStats().total;				StopWatch & killHypTime = mystats->getOtherStats()["Kill Hyps"];
	

	gpuInterpolation sparse2Dense(alg);
	explainMe_algorithm_withFeatures::MatrixXf_rm graphcut_unary;

	if (myParams.useGpuDataTerm)
	{
		//TODO disentangle CRF thing.
		DO_WITH_ABORT_CHECK(
			step5_graphcut_gpudterm_withDoc(alg, mainStep, alg->params.removeThreshold, graphcut_unary, mystats);)
	}
	else{
		DO_WITH_ABORT_CHECK(
			step5_graphcut_withDetailedDocumentation(alg, mainStep, graphcut_unary, mystats);)
	}

	for (int i = 0; i < alg->w.rows(); i++){
		int best;
		float mx = alg->w.row(i).maxCoeff(&best);
		alg->w.row(i).fill(0);
		alg->w(i, best) = 1;
	}

	//<----DOC. (labels after gc but beforee any removal or fussing around
	if (myParams.docLevel > experimentParameters::LVL_1_POST_RM){
		docTime.restart();
		//	alg->step3_updateDenseSeg_from_sparse();,
		DO_TIMED_WITH_ABORT_CHECK(sparse2Dense.prepareInterpolation(gpuDatatermComp, alg->w); , interpolateTime)
		for (int frame = 0; frame < alg->denseSegmentation_allClouds.size(); frame++)
		{
			DO_TIMED_WITH_ABORT_CHECK(sparse2Dense.doInterpolation(gpuDatatermComp, frame, alg->denseSegmentation_allClouds[frame]);, interpolateTime)
		}

		//document(alg, std::string("_post_rm") + std::to_string(mainStep));
		Q_EMIT(document_weights(this->outputFolder + myParams.gitTag +  std::string("_post_gc") + std::to_string(mainStep)));
		document_motions(alg, this->outputFolder + myParams.gitTag +  std::string("_post_gc") + std::to_string(mainStep));
		docTime.stop();
	}
	//--->

	//merge motions based on graphcut unaries.
	if (alg->params.mergeMotions){
		DO_TIMED_WITH_ABORT_CHECK2(
			alg->step_5_3_5_fuseClosebyHypotheses(alg->w, graphcut_unary, 0.03); , killHypTime, otherTime) //0.02
	}
	
	std::cout << "Removin Hypos...";
	DO_TIMED_WITH_ABORT_CHECK2(
		alg->step5_4_remove_spurious_hypos(alg->params.removeThreshold),
		killHypTime, otherTime)


	//update sparse to dense
	DO_TIMED_WITH_ABORT_CHECK(sparse2Dense.prepareInterpolation(gpuDatatermComp, alg->w); , interpolateTime);
	for (int frame = 0; frame < alg->denseSegmentation_allClouds.size(); frame++)
	{
		DO_TIMED_WITH_ABORT_CHECK(sparse2Dense.doInterpolation(gpuDatatermComp, frame, alg->denseSegmentation_allClouds[frame]);, interpolateTime)

			for (int i = 0; i < alg->denseSegmentation_allClouds[frame].rows(); i++){
				int best;
				alg->denseSegmentation_allClouds[frame].row(i).maxCoeff(&best);
				alg->denseSegmentation_allClouds[frame].row(i).fill(0);
				alg->denseSegmentation_allClouds[frame](i, best) = 1;
			}
	}

	//<---DOC
	if (myParams.docLevel >= experimentParameters::LVL_1_POST_RM){
		docTime.restart();
		Q_EMIT(document_weights(this->outputFolder + myParams.gitTag +  std::string("_post_rm") + std::to_string(mainStep)));
		document_motions(alg, this->outputFolder + myParams.gitTag +  std::string("_post_rm") + std::to_string(mainStep));
		docTime.stop();
	}
	//--->
	
}


void Experiment::do_crf_based_EM_iteration(int mainStep){
	bool addErrorbasedProposals = alg->params.generateHypotheses.byOutliers;//true;// (mainStep % 2 == 1);

	//get timers.
	StopWatch & docTime = mystats->getDocStats().total;					StopWatch & interpolateTime = mystats->getInterpolateStats().total;
	StopWatch & otherTime = mystats->getOtherStats().total;				StopWatch & killHypTime = mystats->getOtherStats()["Kill Hyps"];

	gpuInterpolation sparse2Dense(alg);
	float currentError;

	//crf based update
	explainMe_algorithm_withFeatures::MatrixXf_rm graphcut_unary;
	std::vector<std::vector<std::vector<float>>> proposals;
	DO_WITH_ABORT_CHECK(
		proposals = step5_crf_withDocumentation(alg, mainStep, alg->params.removeThreshold, graphcut_unary, mystats, addErrorbasedProposals);)
	
	//at this point dense W stores probs. needs to be set to MAP.
	//and update dense MAP. Actually should do this after removing spurious hyps.
	//investigate.
	for (int i = 0; i < alg->allClouds.size(); i++) {
		int winner;
		for (int j = 0; j < alg->allClouds[i]->size(); j++)
		{
			alg->denseSegmentation_allClouds[i].row(j).maxCoeff(&winner);
			alg->denseSegmentation_allClouds[i].row(j).setZero();
			alg->denseSegmentation_allClouds[i](j, winner) = 1;
		}
	}

	
	//This works  on the sparse segmentation-and mirrors the updates on the dense segmentation
	std::cout << "Removin Hypos...";
	DO_TIMED_WITH_ABORT_CHECK2(
		alg->step5_4_remove_spurious_hypos(alg->params.removeThreshold),
		killHypTime, otherTime)

	
	//Add outlier labels based on sampled error. 
	if (addErrorbasedProposals){
		std::cout << "\n*******************\n * generating outlier Hyps * \n ************************\n";
		alg->step_5_3_7_addDenseHyps(proposals);
		//current_numHyps = alg->motions.numHypotheses();
	}
	
}

//Using graphcuts
void Experiment::run_with_stats(){
	//parameter to be:
	bool add_outlier_hypo = true;

	//if motions are loaded, skip the motion computation step.
	bool skip_motions = myParams.loadState && (myParams.motionStateFile.size() != 0 && myParams.stateFile.size() == 0);

	std::cout << "Called experiment: run.";
	auto stats = mystats;
	//statistics
	//time all kinds of things
	StopWatch & motionTime = stats->getMotionStats().total;
	StopWatch & initTime = stats->getInitStats().total;
	StopWatch & docTime = stats->getDocStats().total;
	StopWatch & interpolateTime = stats->getInterpolateStats().total;
	StopWatch & otherTime = stats->getOtherStats().total;
	StopWatch & killHypTime = stats->getOtherStats()["Kill Hyps"];
	StopWatch & densityTime = stats->getOtherStats()["Density Maps"];
	std::vector<float> & energy = stats->energies;
	auto &doc_numHyps = stats->numHyps;
	auto &doc_hypQuality = stats->avgRegErr;
	
			
	auto &params = alg->params;

	//========== Initialization (Optional) =================
	
	//Initialization from both a given segmentation and a motion set.
	if (myParams.loadState && myParams.stateFile.size() != 0 && myParams.motionStateFile.size() != 0){
		DO_TIMED_WITH_ABORT_CHECK(
			alg->initState(myParams.stateFile, myParams.motionStateFile);,	initTime)
	}
	//Initialization from motions only
	else if (myParams.loadState && myParams.stateFile.size() == 0 && myParams.motionStateFile.size() != 0)
	{
		std::vector<std::vector <Eigen::Matrix4f>> mos;
		Chronist::load(myParams.motionStateFile, mos);
		alg->motions.set(mos);
		RandomSampler sampler1;
		std::vector<pcl::PointXYZINormal> seed_points;
		std::vector<Eigen::Vector3f> seed_normals;
		std::vector<int> seed_cloud, seed_point_idx;
		//resets w to random and initializes trajectories.
		alg->step5_1_select_seed_points(seed_points, seed_normals, seed_cloud, &sampler1);
		explainMe::MatrixXf_rm equalProb = explainMe::MatrixXf_rm::Ones(seed_points.size(), mos.size()) / mos.size();
		alg->set_sparse_w_to_MAP_update_Trajs(equalProb, seed_points, seed_cloud, seed_point_idx);
		DO_TIMED_WITH_ABORT_CHECK(
			alg->step3_updateDenseSeg_from_sparse();, interpolateTime)
	}
	else if (myParams.skipInit && myParams.stateFile.size() == 0){
		std::vector<std::vector <Eigen::Matrix4f>> mos;
		mos.push_back(std::vector<Eigen::Matrix4f>(alg->allClouds.size(), Eigen::Matrix4f::Identity()));
		alg->motions.set(mos);
		RandomSampler sampler1;
		std::vector<pcl::PointXYZINormal> seed_points;
		std::vector<Eigen::Vector3f> seed_normals;
		std::vector<int> seed_frame, seed_point_idx;
		//resets w to random and initializes trajectories.
		alg->step5_1_select_seed_points(seed_points, seed_normals, seed_frame, seed_point_idx,&sampler1);
		explainMe::MatrixXf_rm equalProb = explainMe::MatrixXf_rm::Ones(seed_points.size(),1);
		alg->set_sparse_w_to_MAP_update_Trajs(equalProb, seed_points, seed_frame, seed_point_idx);
		DO_TIMED_WITH_ABORT_CHECK(
			alg->step3_updateDenseSeg_from_sparse(); , interpolateTime)
	}
	else{
		Initializer::Ptr init;
		//algorithm building blocks for initialization
		PathsByOF::Ptr pb(new PathsByOF(params.minOFQuality, params.delta_seedFrame, params.topTrajAcceleration));
		InitByEnergyMinimization::Ptr cw(new InitByEnergyMinimization_smoothness(
			myParams.numIt_init, params.p, params.p2, params.stepSize, params.numHypo,
			alg->numClouds(), params.alpha_smootheness_w));//*/

		if (myParams.loadState && myParams.stateFile.size() != 0){
			init = Initializer::Ptr(new FromFileInitializer(myParams.stateFile));
		}
		else{
			init = Initializer::Ptr(new CompositeInitializer(pb, cw));
		}
		DO_WITH_ABORT_CHECK(
			alg->step_1_and_2(init, true, & stats->getInitStats());)

			//update the dense segmentation after init. could be part of step2.
		DO_TIMED_WITH_ABORT_CHECK(
			alg->step3_updateDenseSeg_from_sparse(); , interpolateTime)
		
	}

	//DOC <--
	std::cout << "Doc Init...";
	stats->setProgressXoutOfY(1, myParams.numIt_EM + 1);
	if (myParams.docLevel >=  experimentParameters::LVL_1_POST_RM){
		docTime.restart();
		Q_EMIT(document_weights(this->outputFolder + myParams.gitTag +  "_init"));
		docTime.stop();
	}
	std::cout << "done... Energy: ";
	DO_WITH_ABORT_CHECK(
		documentEnergy(alg, energy, stats->getDocStats());)
		doc_numHyps.push_back(alg->motions.numHypotheses());

	//-->
	std::cout << "done.\nInit done.\n";
	//tweak: compute the average using the whole frames, use it as initial guess in bundle adjustment.
	alg->motions.setInitialGuessToAvgMotion(alg->allClouds, alg->allClouds_kdTree,explainMe_algorithm_withFeatures::REFERENCE_CLOUD, 
		alg->params.maxItICP,alg->params.alpha_point2plane, alg->params.alpha_point2point, alg->params.icp_culling_dist,
		!(myParams.loadState && (myParams.motionStateFile.size() != 0)));
	//====end Initialization ==========

	//The main loop.
	for (int mainStep = 0; mainStep < myParams.numIt_EM; mainStep++){
		std::cout << "\n EM iteration " << mainStep << "/" << myParams.numIt_EM << "\n";

		//update motion models; this step is skipped e.g. if motions were loaded from file.
		if (!(skip_motions && mainStep == 0))
		{
			DO_TIMED_WITH_ABORT_CHECK(
				alg->step4_computeMotionHypothesesFromDenseSegmentation(false,
				alg->params.generateHypotheses.byLabelShifts,
				alg->params.generateHypotheses.byCarryOvers), motionTime)
			std::cout << "Doc alignments!";
			std::ostringstream stream;
			stream << this->outputFolder + myParams.gitTag + std::string("_mo") + std::to_string(mainStep);
			std::string tmp_string = stream.str();
			document_alignments_metaInformation(tmp_string, mainStep);
		
			//---- per hypothesis and per frame visualization of probabiliteis.
			if (myParams.docLevel >= experimentParameters::LVL_2_POST_RM_MO_crfW)
			{
				for (int i = 0; i < alg->motions.numHypotheses(); i++){
					Q_EMIT(document_w_i(this->outputFolder + myParams.gitTag +  "_postmo_hard" + std::to_string(mainStep) + "_" + std::to_string(i), i));
				}
			}
		}
		/*//hack for faster debugging of label combination
		else 
		{
			std::vector<std::vector<int>> dummy1;
			std::vector<hyp_range> dummy2;set_sparse_w_to_MAP_update_Trajs
			alg->motions.step2_analyseDenseSegmentation(0, 0, alg->allClouds, alg->denseSegmentation_allClouds, alg->allClouds_kdTree, dummy1, dummy2, false, false);
			document_alignments_metaInformation(this->outputFolder + myParams.gitTag +  std::string("_mo") + std::to_string(mainStep), mainStep);
		}*/

		if (mainStep > 0){
			std::cout << "Experiment:" << __LINE__; alg->checkTrajectorySeeds();
		}

		//<---- DOC
		Chronist::store(this->outputFolder + myParams.gitTag  + std::string("_mo") + std::to_string(mainStep) + std::string(".mo2"), alg->motions.getAlignmentSets());
		//number of hypotheses		
		doc_numHyps.push_back(alg->motions.numHypotheses());
		if (myParams.docLevel > experimentParameters::LVL_2_POST_RM_MO_crfW){
			docTime.restart();
			Q_EMIT(document_alignments_fancy(this->outputFolder + myParams.gitTag +  std::string("_mo") + std::to_string(mainStep)));
			docTime.stop();
		}
		//------>

		//update motion assignment (segmentation)
		if (myParams.segmentationMode == experimentParameters::SEGMENTATION_GC){
			do_gc_based_EM_iteration(mainStep);
		}
		else if (myParams.segmentationMode == experimentParameters::SEGMENTATION_CRF){
			//here also outliers are detected and according hypotheses are added.
			do_crf_based_EM_iteration(mainStep);
		}

		std::cout << "Experiment:" << __LINE__; alg->checkTrajectorySeeds();
		//<----DOC
		if (myParams.docLevel >= experimentParameters::LVL_1_POST_RM){
			docTime.restart();
			//document(alg, std::string("_post_rm") + std::to_string(mainStep));
			Q_EMIT(document_weights(this->outputFolder + myParams.gitTag +  std::string("_post_rm") + std::to_string(mainStep)));
			document_motions(alg, this->outputFolder + myParams.gitTag +  std::string("_post_rm") + std::to_string(mainStep));
			docTime.stop();
		}
		//------>


		Q_EMIT(document_seg_png(this->outputFolder + myParams.gitTag + std::string("_png4eval_") + std::to_string(mainStep)));
			
		//<--- Doc / update stats for livestats
		DO_WITH_ABORT_CHECK(
			documentEnergy(alg, energy, stats->getDocStats());)
			doc_numHyps.push_back(alg->motions.numHypotheses());
		documentMotionQuality(alg->motions, doc_hypQuality);
		stats->setProgressXoutOfY(1 + mainStep, myParams.numIt_EM );
		//--->
	}

	std::cout << "Finished running algorithm";

	//storing final results.
	std::cout << "store found trajectories and segmentation...";
	std::string dir_name = this->outputFolder + "/" + myParams.gitTag + "_result";
	Chronist::store_v2(dir_name, std::string("output"), alg->trajectories, alg->w, alg->motions.getAlignmentSets());

	//<--- DOC
	//--- final segmentation and runtimes
	Q_EMIT(document_weights(this->outputFolder + myParams.gitTag + "_post_2nd_gc_end"));
	//snapshot from the live stats
	Q_EMIT(document_stats_png(this->outputFolder, std::string("summary_") + myParams.gitTag  + ".png"));

	//--- Energy decrease in matlab compatible format.
	if (myParams.docLevel > experimentParameters::LVL_2_POST_RM_MO_crfW)
	{
		std::ofstream m_file;
		m_file.open(dir_name + "/energy.m");
		m_file << "\nEnergy = [";
		for (int i = 0; i < energy.size(); i++){
			m_file << energy[i] << "\n";
		}
		m_file << "];\n plot(Energy, '.-')";
		m_file.close();
	}
	//--->

	/*{
		//document tsdfs
		tsdfTracking tsdfs(8.0, alg->reader->getDescription(), 0.01f);
		for (int hyp = 0; hyp < alg->w.cols(); hyp++)
		{
			std::vector<explainMe::Cloud::Ptr> segClouds;
			std::vector<Eigen::Matrix4f> als;
			alg->getSegmentedOrganizedCloudsAndAlignments(hyp, segClouds, als);
			//sometimes throws out of memory!!!!
			tsdfs.doFusion(segClouds, alg->motions.getAlignmentSet(hyp), this->outputFolder + this->gitTag  + "_" + std::to_string(hyp) + ".stl");
		}
	}*/


	std::cout << "timings:\n ";
	stats->printAll();
	
}

void Experiment::documentEnergy(explainMe_algorithm_withFeatures::Ptr alg, std::vector<float> & energy, stats & timer)
{
	if (myParams.docLevel > experimentParameters::LVL_3_POST_RM_ENERGY_DWDT){
		timer.total.restart();
		timer.details.start("Energy");
		energy.push_back(alg->computeTotalEnergy());
		timer.details.stop("Energy");
		timer.total.stop();
	}
}

void Experiment::document_motions(explainMe_algorithm_withFeatures::Ptr alg, std::string folder){
	std::ofstream out(folder + "/motions.log");
	for (int i = 0; i < alg->motions.numHypotheses(); i++){
		auto & hist = alg->motions.getHistory(i);
		out << "# Motion " << i << " (unique id: " << alg->motions.uniqueID(i) << ")\n";
		out << "# " << hist.description << "\n\n";

		auto & mo = alg->motions.getAlignmentSet(i);
		for (int j = 0; j < mo.size(); j++)
		{
			out << mo[j] << "\n\n";
		}
	}
}
void  Experiment::documentMotionQuality(MotionHypotheses & hyps, std::vector<std::vector<float>> & doc){
	auto & motions = hyps.getAlignmentSets();
	doc.push_back(std::vector<float>());
	doc.back().resize(motions.size());
	for (int i = 0; i < motions.size(); i++){
		doc.back()[i] = motions[i].quality;
	}
}

void Experiment::document_alignments_metaInformation(std::string folder, int it)
{
	auto & supports = alg->motions.subresults_ForVisualizationOnly.supports;
	float threshold = alg->motions.subresults_ForVisualizationOnly.supportThreshold;
	auto & shift_mats = alg->motions.subresults_ForVisualizationOnly.massFlows;
	auto & shift_mats_fwd = alg->motions.subresults_ForVisualizationOnly.massFlowsLoss;
	int numHyp = supports[0].cols();
	
	std::stringstream supportSS,colorSS;
	supportSS << "[";
	colorSS << "[";
	int hypIdx = 0;
	float r, g, b;
	for (auto & supp : supports){
		supportSS << supp << ";\n";
		CloudColorizer::colorWheel(hypIdx,r,g,b);
		colorSS << r << " " << g << " " << b << ";";
		hypIdx++;
	}
	supportSS << "]';";
	colorSS << "];";

	std::string matlabScript =
		"basefolder = 'specify_inputSegFolder_for_it " + std::to_string(it) + "';\n"
		"outfile = 'specifyout.png'\n"
		"numHyp = " + std::to_string(numHyp) + ";\n"
		"supp = " + supportSS.str() + "\n"
		"colors = " + colorSS.str() + "\n"
		"threshold = " + std::to_string(threshold) + ";\n"
		"numframes = size(supp, 2);\n"

		"segframes = { [basefolder, '/seg_frm0.png'], ...\n"
		"[basefolder, '/seg_frm', num2str(floor(numframes / 5)), '.png'], ...\n"
		"[basefolder, '/seg_frm', num2str(floor(2 * numframes / 5)), '.png'], ...\n"
		"[basefolder, '/seg_frm', num2str(floor(3 * numframes / 5)), '.png'], ...\n"
		"[basefolder, '/seg_frm', num2str(floor(4 * numframes / 5)), '.png'], ...\n"
		"[basefolder, '/seg_frm', num2str(floor(5 * numframes / 5-1)), '.png']...\n"
		"};\n"

		"agglomImg = imread(segframes{ 1 });\n"
		"for k = 2 : size(segframes, 2)\n"
		"img = segframes{ k };\n"
		"kk = imread(img);\n"
		"agglomImg = cat(2, agglomImg, imread(img));\n"
		"end\n"
		"fig = figure;\n"
		"hold on\n"
		"subplot(numHyp * 2 + 5, 1, 1:5)\n"
		"imshow(agglomImg)\n"
		"for hyp = 1: numHyp\n"
		"	subplot(numHyp * 2 + 5, 1, (hyp)* 2 - 1 + 5)\n"
		"	support = supp(hyp, :);\n"
		"plot(support)\n"
		"	set(gca, 'XTick', []);\n"
		"set(gca, 'YTick', []);\n"
		"xlim([1 length(support)])\n"
		"	subplot(numHyp * 2 + 5, 1, (hyp)* 2 + 5)\n"
		"	bla(:, : , 1) = repmat((support>threshold), 2, 1)*colors(hyp, 1) + repmat((support<threshold), 2, 1);\n"
		"bla(:, : , 2) = repmat((support>threshold), 2, 1)*colors(hyp, 2) + repmat((support<threshold), 2, 1);\n"
		"bla(:, : , 3) = repmat((support>threshold), 2, 1)*colors(hyp, 3) + repmat((support<threshold), 2, 1);\n"
		"imshow(bla)\n"
		"	set(gca, 'XTick', []);\n"
		"set(gca, 'YTick', []);\n"
		"end\n"
		"	%%\n"
		"	saveas(fig, outfile); \n";

	boost::filesystem::path dir(folder);
	boost::filesystem::create_directory(dir);
	std::ofstream out(folder + "/ visualizeSupport.m");
	out << matlabScript;
	out.close();

	std::ofstream out2(folder + "/visualizeShifts.m");
	out2 << "%An entry (i,j) in the fth matrix denotes how much jth label in frame f won from i-th label in frame f-1\n";
	out2 << " shifts = {";
	for (auto & shiftMat : shift_mats){
		out2 << "[";
		for (int row = 0; row < shiftMat.rows(); row++){
			out2 << shiftMat.row(row) << ";";
		}
		out2 << "],...\n";
	}

	out2 << "};\n";
	out2 << "%An entry (i,j) in the fth matrix denotes how much jth label in frame f lost to the i-th label in frame f+1\n";
	out2 << " shifts_fwd = {";
	for (auto & shiftMat : shift_mats_fwd){
		out2 << "[";
		for (int row = 0; row < shiftMat.rows(); row++){
			out2 << shiftMat.row(row) << ";";
		}
		out2 << "],...\n";
	}

	out2 << "};\n";

	out2.close();
}






