#include "BundleAdjustment.h"
//#define FLANN_USE_CUDA
//#include <flann/flann.hpp>
//#include <flann/io/hdf5.h>

#include "CloudTools.h"
#include <fstream>
#include <omp.h>
#include "mySimpleKdTree.h"
#include "mySimpleKdTree_ndim.h"
#include <limits>

#undef max
#include "StopWatch.h"
#define _CRT_RAND_S
#include<stdlib.h>

#ifdef _MSC_VER
#define sqrtf std::sqrtf
#else
#define sqrtf sqrtf
#endif

BundleAdjustment::BundleAdjustment(void)
{
}


BundleAdjustment::~BundleAdjustment(void)
{
}



void BundleAdjustment::pointToPlane_Bundle_step_r(std::vector<explainMe::Cloud::Ptr > &clouds,
			std::vector<Eigen::Matrix4f> &alignments, //alignment[i] aligns the reference cloud to the cloud i. for i = reference it is the identity.
			std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
			int referenceCloud,//acts as reference frame
			int numIterations) //number of iterations (icp like)
{

	Eigen::Vector4f centroid;
	centroid <<	CloudTools::compute3DCentroid(clouds[referenceCloud]), 0;


	//for on the fly mapping- clouds are never transformed.
	//compute mapping of all clouds to the basecloud.
	std::vector<Eigen::Matrix4f> a_inv;
	for(int i = 0; i < clouds.size(); i++){
		//apply transf -1
		Eigen::Matrix4f a_inv_;
		a_inv_ = alignments[i].inverse();
		if(i == referenceCloud && !(a_inv_.trace() < 4.001 && a_inv_.trace() > 3.999)){
			std::cout << "in bundleadjustment, reference alignment should be identity!";
		}
		a_inv.push_back(a_inv_);
	}

	std::cout << "computing bs and cs...";


	//temps (for readability)
	Vector6f c,b; Matrix6f ccT;
	explainMe::Point p,q;
	float w;
	//kdtree method parameters
	std::vector<int> idx;
	std::vector<float> dist_sqr;
	
	for(int its = 0; its < numIterations; its++){
		std::vector<Matrix6f> cs;
		std::vector<Vector6f> bs;
	
		//1st compute the various 6x6 blocks of the ba matrix
		//The single blocks coincide with the std point to plane blocks.
		//compute all c's abd b's. This involves the association of cloud j to cloud i
		for(int i = 0; i < enforce.size(); i++)
		{
			//kdtree
			//pcl::KdTreeFLANN<explainMe::Point>::Ptr kdTree(new pcl::KdTreeFLANN<explainMe::Point>());
			//kdTree->setInputCloud(clouds[i]);

			//myKdtree
			mySimpleKdTree myTree;
			myTree.setData<explainMe::Cloud::Ptr>(clouds[i]);
			myTree.buildTree();


			simpleNNFinder nnFinder(myTree);

			for(int nbr = 0; nbr < enforce[i].size(); nbr++)
			{
				int j = enforce[i][nbr];

				b = Vector6f::Zero();
				ccT = Matrix6f::Zero();

				//points in cloud j need to be mapped to the coordinate frame of i for the nn search
				//that is a[i]*a_inv[j]
				Eigen::Matrix4f j_to_i = alignments[i]*a_inv[j];
				//for(auto qit = clouds[j]->begin(); qit!= clouds[j]->end(); qit++)

				const int clouds_j_size = clouds[j]->size();
				int q_idx;

#pragma omp parallel private(p,q,c,w,idx,dist_sqr) num_threads(20)
{
Matrix6f ccT_;Vector6f b_; ccT_.fill(0); b_.fill(0);
simpleNNFinder nnFinder(myTree);
#pragma omp for nowait
				for(q_idx = 0; q_idx < clouds_j_size; q_idx ++)
				{
					//now: if p and q are not yet aligned.
					q = clouds[j]->at(q_idx);//*qit;
					q.getVector4fMap() = j_to_i * q.getVector4fMap();
					//p = kdTree->NN(q);

					idx.resize(1,1);
					//this fuckers have a mutex!
					//kdTree->nearestKSearch(q,1,idx,dist_sqr);
					//p = clouds[i]->at(idx[0]);
					//octree.nearestKSearch (q,1,idx,dist_sqr);
					//p = clouds[i]->at(idx[0]);

					pxyz pt(q.x,q.y,q.z);
					idx[0] = nnFinder.findNN(pt).index;
					p = clouds[i]->at(idx[0]);

					//reset q
					q = clouds[j]->at(q_idx);//*qit;
					//now p and q are found, but need to be mapped such that they can be used to compute an increment of the alignments
					//i.e. map to reference cloud and minus centroid.
					p.getVector4fMap() = a_inv[i]*p.getVector4fMap() - centroid; p.getNormalVector3fMap() = a_inv[i].topLeftCorner<3,3>() *p.getNormalVector3fMap();
					q.getVector4fMap() = a_inv[j]*q.getVector4fMap() - centroid; q.getNormalVector3fMap() = a_inv[j].topLeftCorner<3,3>() *q.getNormalVector3fMap();


					//weight/culling
					w= p.getNormalVector3fMap().dot(q.getNormalVector3fMap()); w = (w< 0.7071? 0:w); w= (w*0==0?w:0.5); 
					w*= (1-(q.getVector3fMap() -p.getVector3fMap()).norm()/0.15);	w=w<0?0:w;
					
					//w=1;
					//the point to plane equation
					c << p.getVector3fMap().cross(q.getNormalVector3fMap()), q.getNormalVector3fMap();
					ccT_ += c*c.transpose()*w;
					b_ += c*(p.getVector3fMap()-q.getVector3fMap()).dot(q.getNormalVector3fMap())*w;
				}

				#pragma omp critical
				{
					ccT += ccT_;
					b += b_;
				}
}

				ccT += Matrix6f::Identity() *1e-4;

				cs.push_back(ccT);
				bs.push_back(b);

				//std::cout << "\n" << ccT << "\n\n"<< b << "\n";
			}

		}

		//setting up the bundle adjustment matrix (see notes)
		Eigen::MatrixXf total(6*(clouds.size()-1), 6*(clouds.size()-1));
		Eigen::VectorXf total_b(6*(clouds.size()-1));
		Eigen::VectorXf X(6*(clouds.size()-1));

		total = Eigen::MatrixXf::Zero(6*(clouds.size()-1), 6*(clouds.size()-1));
		total_b.fill(0);

		int ind =0;
		for(int i_cloud = 0; i_cloud < enforce.size(); i_cloud++)
		{
			for(int nbr = 0; nbr < enforce[i_cloud].size(); nbr++)
			{
				int j_cloud = enforce[i_cloud][nbr]; 
				int i = (i_cloud > referenceCloud ? i_cloud-1: i_cloud);//parameter block
				int j = (j_cloud > referenceCloud ? j_cloud-1: j_cloud);

				if(i_cloud==referenceCloud){
					total.block<6,6>(j*6,j*6) += cs[ind];
					total_b.block<6,1>(j*6,0) -= bs[ind];
				}
				if(j_cloud == referenceCloud){
					total.block<6,6>(i*6,i*6) += cs[ind];
					total_b.block<6,1>(i*6,0) += bs[ind];
				}
				else{
					total.block<6,6>(i*6,i*6) += cs[ind];
					total.block<6,6>(i*6,j*6) -= cs[ind];
					total.block<6,6>(j*6,i*6) -= cs[ind];
					total.block<6,6>(j*6,j*6) += cs[ind];
					
					total_b.block<6,1>(i*6,0) += bs[ind];
					total_b.block<6,1>(j*6,0) -= bs[ind];
				}

				ind ++;
			}
		}


		//storing the matrix in matlab format.
/*
		std::cout << "total matrix is set up.\n \n ";
		
		//for(int i = 0; i < total.rows(); i++){
		//	for(int j = 0; j < total.cols(); j++){
		//		std::cout << (total(i,j)!=0 ? "X": " ");
		//	}
		//	std::cout << "|\n";
		//}
		

		std::stringstream ss;
		std::stringstream ss2;
		ss << "Output/ba_matrix_r_";
		for(int c_i = 0; c_i < enforce.size(); c_i++){
			for(int c_j = 0; c_j < enforce[c_i].size(); c_j++){
				ss << c_i << enforce[c_i][c_j];
				ss2 << c_i << enforce[c_i][c_j];
			}
		}
		ss << ".txt";
		std::ofstream myfile;
		myfile.open (ss.str());
		auto & out  = myfile;
		//auto & out = std::cout;

		out << "T_r_" << ss2.str() << "=[";
		for(int i = 0; i < total.rows(); i++){
			for(int j = 0; j < total.cols(); j++){
				out << total(i,j)<< (j+1 == total.cols() ? "":",");
			}
			out << (i+1 == total.rows()? "": ";\n");
		}
		out << "];\n";
		out << "b_r_" << ss2.str() << " =[";
		for(int i = 0; i < total.rows(); i++){
			out << total_b(i) << (i+1 == total.rows() ? "":",");
		}
		out << "];\n";

		myfile.close();
*/

		//following does not work, i do not know why. thought QR should be the right thing!
		//X = total.fullPivHouseholderQr().solve(total_b);
	
		//works, cholesky on normal eq.
		//X = (total.transpose()*total).ldlt().solve(total.transpose() *total_b);
		//svd based solve:
		X =total.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(total_b);

		std::cout << "Total Norm of lie-element vector: " << X.norm() << "\n";
	//	std::cout << "X: " << X << "\n";
		
		//extract all alignments.
		for(int i_cloud = 0; i_cloud < alignments.size(); i_cloud++){
			if(i_cloud != referenceCloud){
				int i = (i_cloud > referenceCloud ? i_cloud -1 :i_cloud);
				Vector6f x_icp_total = -X.block<6,1>(i*6,0); 
				
				//map linearized transform back
				float cg = std::cos(x_icp_total(2)); float ca = std::cos(x_icp_total(0)); float cb = std::cos(x_icp_total(1));
				float sg = std::sin(x_icp_total(2)); float sa = std::sin(x_icp_total(0)); float sb = std::sin(x_icp_total(1));
				Eigen::Matrix4f  alignment;
				alignment << cg*cb,			sg*cb,				-sb,	-x_icp_total(3),
					-sg*ca + cg*sb*sa,	cg*ca + sg*sb*sa,	cb*sa,	-x_icp_total(4),
					sg*sa + cg*sb*ca,	-cg*sa + sg*sb*ca,	cb*ca,	-x_icp_total(5),
					0,			0,		0,					1;

				alignment.topRightCorner<4,1>() +=  centroid - alignment*centroid;

				alignments[i_cloud] = alignments[i_cloud]*alignment;
				a_inv[i_cloud] = alignments[i_cloud].inverse();
			}
		}
	}//end multiple iterations of ba.
}

template<typename Eigen_T_weights>
void BundleAdjustment::pointToPlane_Bundle_step(std::vector<trajectory_R> & trajs, 
	int numClouds, int ref_cloud,
	Eigen_T_weights & weights, int hyp, std::vector<Eigen::Matrix4f> &alignments, float p_1)
{
	int numIts = 5;

	//variables to set up the ba matrix.
	std::vector<Matrix6f> cs;
	std::vector<Vector6f> bs;
	Vector6f b,c, b_p2p, c_p2p;
	Matrix6f ccT, ccT_p2p;
	pcl::PointXYZINormal p,q;

	//The ba matrix, lefthand side, righthandsie.
	Eigen::MatrixXf total(6 * (numClouds - 1), 6 * (numClouds - 1));
	Eigen::VectorXf total_b(6 * (numClouds - 1));
	Eigen::VectorXf X(6 * (numClouds - 1));

	//compute centroid of ref Frame
	Eigen::Vector4f centroid;
	centroid.fill(0);
	float w_sum = 0;
	for(int i = 0; i < trajs.size(); i++){
		if(trajs[i].cloudInRange(ref_cloud)){
			centroid += trajs[i][ref_cloud].getVector4fMap() * weights(i, hyp);
			w_sum +=   weights(i,hyp);
		}
	}
	if(w_sum == 0){
		centroid.fill(0);
	}
	else{
		centroid = centroid / w_sum; centroid(3) = 0;
	}
	if(centroid.sum() * 0 != 0){
		std::cout << "invalid centroid : " <<centroid << "W_sum " << w_sum;
		int a = 0;
		std::cin >> a;
	}
	
	//compute inverses 
	std::vector<Eigen::Matrix4f> a_inv = alignments;
	for(int i = 0; i < numClouds; i++){
		a_inv[i] = alignments[i].inverse();
	}


	float w, w_total=0, last_xnorm = 1e10;
	for(int it = 0; it < numIts; it++){
		cs.clear(); bs.clear();
		 w_total=0;

		//1st compute the various 6x6 blocks of the ba matrix
		//The single blocks coincide with the std point to plane blocks.
		//compute all c's abd b's. This involves the association of cloud j to cloud i
		for(int i = 0; i < numClouds -1; i++)
		{
			int j = i+1;
			b = Vector6f::Zero();
			ccT = Matrix6f::Zero();
			const int numTrajs = trajs.size();
			int traj_idx;

#pragma omp parallel private(p,q,c,b_p2p,ccT_p2p,w) num_threads(20)
{
Matrix6f ccT_;Vector6f b_; ccT_.fill(0); b_.fill(0);
float total_w_  = 0;
#pragma omp for nowait
			for(traj_idx = 0; traj_idx < numTrajs; traj_idx ++)
			{
				//now: if p and q are not yet aligned.
				if(!(trajs[traj_idx].cloudInRange(i) && trajs[traj_idx].cloudInRange(j)))
				{
					continue;
				}


				q = trajs[traj_idx][j];//*qit;
				p = trajs[traj_idx][i];

				//now p and q are found, but need to be mapped such that they can be used to compute an increment of the alignments
				//i.e. map to reference cloud and minus centroid.
				p.getVector4fMap() = a_inv[i]*p.getVector4fMap() - centroid; p.getNormalVector3fMap() = a_inv[i].topLeftCorner<3,3>() *p.getNormalVector3fMap();
				q.getVector4fMap() = a_inv[j]*q.getVector4fMap() - centroid; q.getNormalVector3fMap() = a_inv[j].topLeftCorner<3,3>() *q.getNormalVector3fMap();


				//weight/culling
				w= weights(traj_idx,hyp);
				w= (w< 0 ? 0:w);
				w= std::pow(w, p_1);

				total_w_ += w;
				//total_w += w;
					
				//the point to plane equation
				c << p.getVector3fMap().cross(q.getNormalVector3fMap()), q.getNormalVector3fMap();
				ccT_ += c*c.transpose()*w;
				b_ += c*(p.getVector3fMap()-q.getVector3fMap()).dot(q.getNormalVector3fMap())*w;
				//ccT += c*c.transpose()*w;
				//b += c*(p.getVector3fMap()-q.getVector3fMap()).dot(q.getNormalVector3fMap())*w;


				//point2point term
				b_p2p << q.getVector3fMap().cross(p.getVector3fMap()- q.getVector3fMap()), p.getVector3fMap()- q.getVector3fMap();
				ccT_p2p <<	q.y*q.y + q.z*q.z,	-q.x*q.y,	-q.x*q.z,	0,		-q.z,	q.y,
							-q.y*q.x,	q.x*q.x + q.z*q.z,	-q.y*q.z,	q.z,	0,		-q.x,
							-q.z*q.x,	-q.z*q.y,	q.x*q.x + q.y*q.y,	-q.y,	q.x,	0,
							0,			q.z,		-q.y,				1,		0,		0,	
							-q.z,		0,			q.x,				0,		1,		0,
							q.y,		-q.x,		0,					0,		0,		1;

//				b+= b_p2p*w;
//				ccT+= ccT_p2p*w;
//				b_+= b_p2p*w;
//				ccT_+= ccT_p2p*w;
			}

				#pragma omp critical
				{
					ccT += ccT_;
					b += b_;
					w_total += total_w_;
				}
}

			ccT += Matrix6f::Identity(); //damping.
			cs.push_back(ccT);
			bs.push_back(b);
		}

		

		//setting up the bundle adjustment matrix (see notes)
		total.fill(0); total_b.fill(0);

		int ind =0;
		for(int i_cloud = 0; i_cloud < numClouds-1; i_cloud++)
		{
				int j_cloud = i_cloud+1; 
				int i = (i_cloud > ref_cloud ? i_cloud-1: i_cloud);//parameter block
				int j = (j_cloud > ref_cloud ? j_cloud - 1 : j_cloud);

				if (i_cloud == ref_cloud){
					total.block<6,6>(j*6,j*6) += cs[ind];
					total_b.block<6,1>(j*6,0) -= bs[ind];
				}
				else if (j_cloud == ref_cloud){
					total.block<6,6>(i*6,i*6) += cs[ind];
					total_b.block<6,1>(i*6,0) += bs[ind];
				}
				else{
					total.block<6,6>(i*6,i*6) += cs[ind];
					total.block<6,6>(i*6,j*6) -= cs[ind];
					total.block<6,6>(j*6,i*6) -= cs[ind];
					total.block<6,6>(j*6,j*6) += cs[ind];
					
					total_b.block<6,1>(i*6,0) += bs[ind];
					total_b.block<6,1>(j*6,0) -= bs[ind];
				}

				ind ++;
			
		}


		//when solve fails, it leaves X as it is. At least like that there is no arbitrary divergence
		X.fill(0); 
			
	/*	X = total.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(total_b);
		std::cout << it <<  "X: " << X.norm() << " b " << total_b.norm() << " total.sum " <<   total.diagonal().sum() << "\n";*/
	/*	X = total.fullPivHouseholderQr().solve(total_b);
		std::cout << it <<  "X: " << X.norm() << " b " << total_b.norm() << " total.sum " <<   total.diagonal().sum() << " w " << w_total << "\n";*/

		//cholesky, Fastest option. On normal equations instable because of bad condition
		// The matrix is "often" positive semidefinite, in that provably it isn't always, but in practice I never experienced it not to be.
		auto ldlt = (total).ldlt();
		X = ldlt.solve(total_b);
//		std::cout << it <<  "X: " << X.norm() << " b " << total_b.norm() << " total.sum " <<  total.diagonal().sum() << "\n";
		
		//resort to more robust, more expensive method if needed.X is left untouched if ldlt fails...
		float xnorm =X.norm();
		if(xnorm == 0 || ! ldlt.isPositive()){
			std::cout << "\n Matrix not pos def!\n";
			X = total.fullPivLu().solve(total_b);
			xnorm = X.norm();
			std::cout << it <<  "X: " << X.norm() << " b " << total_b.norm() << " total.sum " <<   total.diagonal().sum() << "\n";
		}

		if(xnorm >= last_xnorm && xnorm > 1e-5){
			std::cout << "**Not converging!: " << last_xnorm << " -> " << xnorm << "\n";
		}
		last_xnorm = xnorm;
				
		//extract and update all alignments.
		for(int frame = 0; frame < numClouds; frame++){
			if (frame != ref_cloud){
				int i = (frame > ref_cloud ? frame - 1 : frame);
				Vector6f x_icp_total = -X.block<6,1>(i*6,0); 
				
				//map linearized transform back
				float cg = std::cos(x_icp_total(2)); float ca = std::cos(x_icp_total(0)); float cb = std::cos(x_icp_total(1));
				float sg = std::sin(x_icp_total(2)); float sa = std::sin(x_icp_total(0)); float sb = std::sin(x_icp_total(1));
				Eigen::Matrix4f  alignment;
				alignment << cg*cb,			sg*cb,				-sb,	-x_icp_total(3),
					-sg*ca + cg*sb*sa,	cg*ca + sg*sb*sa,	cb*sa,	-x_icp_total(4),
					sg*sa + cg*sb*ca,	-cg*sa + sg*sb*ca,	cb*ca,	-x_icp_total(5),
					0,			0,		0,					1;

				alignment.topRightCorner<4,1>() +=  centroid - alignment*centroid;
				alignments[frame] = alignments[frame]*alignment;
				a_inv[frame ] = alignments[frame].inverse();
			}
		}

		if(last_xnorm < 1e-5){
			break;
		}
	}

	std::cout << ".";
}


//assumes that there are no nans int the clouds.
//heuristic spped ups: decreasing maximal radius in nn search
//multiresolution through multiresolution via subsampling.
template<typename Eigen_T_weights>
void BundleAdjustment::pointToPlane_Bundle_step_r(
	std::vector<explainMe::Cloud::Ptr > &clouds,
	std::vector<Eigen_T_weights> &weights,
	int weight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
	int referenceCloud, //acts as reference frame
	int numIterations,
	float alpha_point2plane,
	float alpha_point2point,
	float culling_dist)
{
	std::cout << ":";
	StopWatch buildTree, buildMatrix, solve,total;
	total.restart();
	Eigen::Vector4f centroid;
	centroid <<	CloudTools::compute3DCentroid(clouds[referenceCloud]), 0;
	std::cout << centroid.transpose() << "-";

	//for on the fly mapping- clouds are never transformed.
	//compute mapping of all clouds to the basecloud.
	std::vector<Eigen::Matrix4f> a_inv;
	for(int i = 0; i < clouds.size(); i++){
		std::cout  << i << ",";
		//apply transf -1
		Eigen::Matrix4f a_inv_;
		a_inv_ = alignments[i].inverse();
		if(i == referenceCloud && !(a_inv_.trace() < 4.001 && a_inv_.trace() > 3.999)){
			std::cout << "in bundleadjustment, reference alignment should be identity!";
		}
		a_inv.push_back(a_inv_);
	}

	std::cout << "\ncomputing bs and cs...";

	//temps (for readability)
	Vector6f c,b; Matrix6f ccT;
	explainMe::Point p,q;
	float w;
	//kdtree method parameters
	std::vector<int> idx;
	std::vector<float> dist_sqr;


	//build trees outside of the loop. (TODO push to algorithm, one time kdtree build)
	std::vector<mySimpleKdTree> allTrees(clouds.size());
	const int nClouds = clouds.size();

	buildTree.restart();
#pragma omp parallel
	{
#pragma omp for
		for(int i = 0; i <nClouds; i++)
		{
			allTrees[i].setData<explainMe::Cloud::Ptr>(clouds[i]);
			allTrees[i].buildTree();
		}
	}
	buildTree.stop();

	for(int its = 0; its < numIterations; its++){
		int ith = numIterations -its; //will keep each ith point only for registration
		//1 for its = numiteration -1. ith maximal = 10. (its = 0)
		ith = std::max<int>((int) std::floor((numIterations -1 - its) * 1.0/(numIterations-1) * 10+ 0.5), 1)  + 35;

		//Enforce a minimal convergence. (1e-2) Typically this is achieved in one iteration.
		float x_max_coeff = 0;
		int maxRepeats = 100;
		do{
			float total_err = 0;
			std::vector<Matrix6f> cs;
			std::vector<Vector6f> bs;
		
			buildMatrix.restart();		
			//1st compute the various 6x6 blocks of the ba matrix
			//The single blocks coincide with the std point to plane blocks.
			//compute all c's abd b's. This involves the association of cloud j to cloud i
			for(int i = 0; i < enforce.size(); i++)
			{
				//myKdtree
				//mySimpleKdTree myTree;
				//myTree.setData<explainMe::Cloud::Ptr>(clouds[i]);
				//myTree.buildTree();
				auto & myTree = allTrees[i];
			
				for(int nbr = 0; nbr < enforce[i].size(); nbr++)
				{
					int j = enforce[i][nbr];

					b = Vector6f::Zero();
					ccT = Matrix6f::Zero();
					float total_w = 0;

					//points in cloud j need to be mapped to the coordinate frame of i for the nn search
					//that is a[i]*a_inv[j]
					Eigen::Matrix4f j_to_i = alignments[i]*a_inv[j];
					//for(auto qit = clouds[j]->begin(); qit!= clouds[j]->end(); qit++)

					const int clouds_j_size = clouds[j]->size();
					//int q_idx;

	#pragma omp parallel private(p,q,c,w,idx,dist_sqr) num_threads(20)
	{
	Matrix6f ccT_,ccT_p2p;Vector6f b_, b_p2p; ccT_.fill(0); b_.fill(0);
	float total_w_ = 0, total_err_ = 0;
	simpleNNFinder nnFinder(myTree);
	//distSqr_idx tmp_distSqr_idx;
	#pragma omp for nowait
					for(int q_idx = 0; q_idx < clouds_j_size; q_idx ++)
					{
						//ignore zero weight points:
						if(weights[j](q_idx,weight_idx) < 1e-3f){
							continue;
						}
						//"multi res"
						//if(q_idx % (2*(numIterations -its)-1) != 0){ //%(numIterations -its)
						//if(!(q_idx % (numIterations -its) == 0) && !((q_idx+1) % (numIterations -its) == 0)){
						if(!(q_idx % ith == 0)){
							continue;
						}

						//now: if p and q are not yet aligned.
						q = clouds[j]->at(q_idx);//*qit;
						q.getVector4fMap() = j_to_i * q.getVector4fMap();
						//p = kdTree->NN(q);

						//tmp_distSqr_idx = nnFinder.findNN(pxyz(q.x,q.y,q.z), culling_dist*culling_dist/*/(2*its+1)*/);
						idx.resize(1,1);
						pxyz pt(q.x,q.y,q.z);
						idx[0] = nnFinder.findNN(pt, culling_dist*culling_dist/*/(2*its+1)*/).index;
						if(idx[0] < 0){
							//invalid
							continue;
						}
						//continue; //To test time consumption of lower part. (irrelevant)
						p = clouds[i]->at(idx[0]);

						//reset q
						q = clouds[j]->at(q_idx);//*qit;
						//now p and q are found, but need to be mapped such that they can be used to compute an increment of the alignments
						//i.e. map to reference cloud and minus centroid.
						p.getVector4fMap() = a_inv[i]*p.getVector4fMap() - centroid; p.getNormalVector3fMap() = a_inv[i].topLeftCorner<3,3>() *p.getNormalVector3fMap();
						q.getVector4fMap() = a_inv[j]*q.getVector4fMap() - centroid; q.getNormalVector3fMap() = a_inv[j].topLeftCorner<3,3>() *q.getNormalVector3fMap();

						//weight/culling
						w= p.getNormalVector3fMap().dot(q.getNormalVector3fMap()); w = (w< 0.7071? 0:w); w= (w*0==0?w:0.5); 
						w*= (1-(q.getVector3fMap() -p.getVector3fMap()).norm()/culling_dist);	w=w<0?0:w;
					
						//daring
						w*= std::max<float>(weights[i](idx[0],weight_idx) , weights[j](q_idx,weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);
						//conservative
						//w*= std::min<float>(weights[i](idx[0],weight_idx) , weights[j](q_idx,weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);

						total_w_ += w;
						total_err_+= (p.getVector3fMap() - q.getVector3fMap()).squaredNorm() * alpha_point2point + std::abs((p.getVector3fMap() - q.getVector3fMap()).dot(q.getNormalVector3fMap())) * alpha_point2plane;
						//w=1;
						//the point to plane equation
						c << p.getVector3fMap().cross(q.getNormalVector3fMap()), q.getNormalVector3fMap();
						ccT_ += c*c.transpose()*w*alpha_point2plane;
						b_ += c*(p.getVector3fMap()-q.getVector3fMap()).dot(q.getNormalVector3fMap())*w*alpha_point2plane;


						//point to point...
						//make it l1
						w *= 1/((p.getVector3fMap()-q.getVector3fMap()).norm() + 0.001);

						b_p2p << q.getVector3fMap().cross(p.getVector3fMap()- q.getVector3fMap()), p.getVector3fMap()- q.getVector3fMap(); //WRONG?!
						//b_p2p << q.getVector3fMap().cross(p.getVector3fMap()), p.getVector3fMap();
						ccT_p2p <<	
							 q.y*q.y + q.z*q.z,	-q.x*q.y,			-q.x*q.z,			0,		-q.z,	q.y,
							-q.y*q.x,			 q.x*q.x + q.z*q.z,	-q.y*q.z,			q.z,	0,		-q.x,
							-q.z*q.x,			-q.z*q.y,			 q.x*q.x + q.y*q.y,	-q.y,	q.x,	0,
							0,			q.z,		-q.y,				1,		0,		0,	
							-q.z,		0,			q.x,				0,		1,		0,
							q.y,		-q.x,		0,					0,		0,		1;

						ccT_ += /*0.1 **/ ccT_p2p * w * alpha_point2point;
						b_+= /*0.1 **/b_p2p * w * alpha_point2point;//*/
					}

					#pragma omp critical
					{
						ccT += ccT_;
						b += b_;
						total_w += total_w_;
						total_err += total_err_;
					}
	}//end omp parallel (nn and pairwise matrix computation)
	//dampen only for neighbors.
				if(std::abs<int>(enforce[i][nbr] - i) == 1)
					ccT += Matrix6f::Identity()*(total_w/100 * (numIterations - its)*1.0/numIterations  +1);//Hacked in large damping!!!
					
				//ccT += Matrix6f::Identity() *1.f/(1 +total_w/100) * std::max(ccT.maxCoeff(),1.f);//1e-1;//1e-4 1e-total_weight.
					//std::cout  << total_w << "\t" ;
					cs.push_back(ccT);
					bs.push_back(b);
					//std::cout << "\n" << ccT << "\n\n"<< b << "\n";
				}
			}
			buildMatrix.stop();
			//setting up the bundle adjustment matrix (see notes)
			Eigen::MatrixXf total(6*(clouds.size()-1), 6*(clouds.size()-1));
			Eigen::VectorXf total_b(6*(clouds.size()-1));
			Eigen::VectorXf X(6*(clouds.size()-1));

			total = Eigen::MatrixXf::Zero(6*(clouds.size()-1), 6*(clouds.size()-1));
			total_b.fill(0);

			int ind =0;
			for(int i_cloud = 0; i_cloud < enforce.size(); i_cloud++)
			{
				for(int nbr = 0; nbr < enforce[i_cloud].size(); nbr++)
				{
					int j_cloud = enforce[i_cloud][nbr]; 
					int i = (i_cloud > referenceCloud ? i_cloud-1: i_cloud);//parameter block
					int j = (j_cloud > referenceCloud ? j_cloud-1: j_cloud);

					if(i_cloud==referenceCloud){
						total.block<6,6>(j*6,j*6) += cs[ind];
						total_b.block<6,1>(j*6,0) -= bs[ind];
					}
					if(j_cloud == referenceCloud){
						total.block<6,6>(i*6,i*6) += cs[ind];
						total_b.block<6,1>(i*6,0) += bs[ind];
					}
					else{
						total.block<6,6>(i*6,i*6) += cs[ind];
						total.block<6,6>(i*6,j*6) -= cs[ind];
						total.block<6,6>(j*6,i*6) -= cs[ind];
						total.block<6,6>(j*6,j*6) += cs[ind];
					
						total_b.block<6,1>(i*6,0) += bs[ind];
						total_b.block<6,1>(j*6,0) -= bs[ind];
					}

					ind ++;
				}
			}

			solve.restart();
			//following solve does not work, i do not know why. thought QR should be the right thing!
			//X = total.fullPivHouseholderQr().solve(total_b);
	
			//works, cholesky on normal eq.
			//X = (total.transpose()*total).ldlt().solve(total.transpose() *total_b);
			//svd based solve:
			//X =total.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(total_b);

			// The matrix is "often" positive semidefinite, in that provably it isn't always, but in practice I never experienced it not to be.
			auto ldlt = (total).ldlt();
			X = ldlt.solve(total_b);
	//		std::cout << it <<  "X: " << X.norm() << " b " << total_b.norm() << " total.sum " <<  total.diagonal().sum() << "\n";
		
			std::cout << "LastErr\t" << std::setprecision(8) << total_err << "\t";
			//resort to more robust, more expensive method if needed.X is left untouched if ldlt fails...
			float xnorm =X.norm();
			if(xnorm == 0 || ! ldlt.isPositive()){
				std::cout << "\n Matrix not pos def!\n";
				X = total.fullPivLu().solve(total_b);
				xnorm = X.norm();
				std::cout << "X: " << X.norm() << " b " << total_b.norm() << " total.sum " <<   total.diagonal().sum() << "\n";
			}
		
			solve.stop();
		
			std::cout << "Total Norm of lie-element vector: " << X.norm() << "\t(" << its << " " <<ith << ")\n";
			if(X.norm() > 10){
				std::cout << " **** Xmax: " << X.maxCoeff() <<  "!\n";
				X = total.fullPivLu().solve(total_b);
				std::cout << "LU solve:: " << X.norm() << "\n";
				//X =total.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(total_b);
				//std::cout << "SVD solve:: " << X.norm() << "\n";better but so slow...
			}
		

			//extract all alignments.
			for(int i_cloud = 0; i_cloud < alignments.size(); i_cloud++){
				if(i_cloud != referenceCloud){
					int i = (i_cloud > referenceCloud ? i_cloud -1 :i_cloud);
					Vector6f x_icp_total = -X.block<6,1>(i*6,0); 
				
					//map linearized transform back
					float cg = std::cos(x_icp_total(2)); float ca = std::cos(x_icp_total(0)); float cb = std::cos(x_icp_total(1));
					float sg = std::sin(x_icp_total(2)); float sa = std::sin(x_icp_total(0)); float sb = std::sin(x_icp_total(1));
					Eigen::Matrix4f  alignment;
					alignment << cg*cb,			sg*cb,				-sb,	-x_icp_total(3),
						-sg*ca + cg*sb*sa,	cg*ca + sg*sb*sa,	cb*sa,	-x_icp_total(4),
						sg*sa + cg*sb*ca,	-cg*sa + sg*sb*ca,	cb*ca,	-x_icp_total(5),
						0,			0,		0,					1;

					alignment.topRightCorner<4,1>() +=  centroid - alignment*centroid;

					alignments[i_cloud] = alignments[i_cloud]*alignment;
					a_inv[i_cloud] = alignments[i_cloud].inverse();
				}
			}


			if(X.maxCoeff() < 1e-4){
				break;
			}
			///BAD BAD BAD hack.
			if(X.maxCoeff() >1e-2){
				int max_i;
				std::cout << "repeat.("  << X.maxCoeff(& max_i) ;
				std::cout << "|" << ((max_i %  6) < 3? "angle" : "trans") << ")";
			}
			//Ideas: increase damping between problematic thingies...
			x_max_coeff = X.maxCoeff();
		}while(x_max_coeff > 1e-2 && --maxRepeats > 0);
	}//end multiple iterations of ba.
	total.stop();

	std::cout << "buildTree: " << buildTree.total() << "\nbuilMatrix " << buildMatrix.total() << "\nsolve" <<solve.total() << "\n";
	std::cout << "Total: " << total.total() << "\n";
	std::cout << "Unaccounted: " << total.total() - buildTree.total() -buildMatrix.total() -solve.total() << "\n";

}


inline float interpolate(Eigen::MatrixXi & mat, float cx, float cy){

	/*float bla = mat((int)std::floor(cx),(int)std::floor(cy))* (std::ceil(cx) -cx)*(std::ceil(cy)-cy)
			+mat((int)std::floor(cx),(int)std::ceil(cy))* (std::ceil(cx) -cx)*(-std::floor(cy)+cy)
			+mat((int)std::ceil(cx),(int)std::floor(cy))* (-std::floor(cx) +cx)*(std::ceil(cy)-cy)
			+mat((int)std::ceil(cx),(int)std::ceil(cy))* (-std::floor(cx) +cx)*(-std::floor(cy)+cy)
			;
	if(bla != 0)
		std::cout << "[" << bla<< "]";
	else
		std::cout << "_";*/
	/*if(cx > mat.rows()-1 ){
		std::cout << "err in interpolate : " <<cx <<" vs " << mat.rows()-1 <<" " << cy <<" vs " << mat.cols() -1<< "\n";
		cx = mat.rows()-1;
	}
	if(cy > mat.cols() - 1){
		//cy = mat.cols()-1;
		std::cout << "err in interpolate : " <<cx <<" vs " << mat.rows()-1 <<" " << cy <<" vs " << mat.cols() -1<< "\n";
	}*/
	cx = (cx > mat.rows()-1 ? mat.rows()-1: 
					cx < 0 ? 0:
					cx);
	cy = (cy > mat.cols()-1 ? mat.cols()-1: 
					cy < 0 ? 0:
					cy);

	return mat((int)std::floor(cx),(int)std::floor(cy))* (std::ceil(cx) -cx)*(std::ceil(cy)-cy)
			+mat((int)std::floor(cx),(int)std::ceil(cy))* (std::ceil(cx) -cx)*(-std::floor(cy)+cy)
			+mat((int)std::ceil(cx),(int)std::floor(cy))* (-std::floor(cx) +cx)*(std::ceil(cy)-cy)
			+mat((int)std::ceil(cx),(int)std::ceil(cy))* (-std::floor(cx) +cx)*(-std::floor(cy)+cy)
			;
}

inline float interpolate(Eigen::MatrixXf & mat, float cx, float cy){
	cx = (cx > mat.rows()-1 ? mat.rows()-1: 
					cx < 0 ? 0:
					cx);
	cy = (cy > mat.cols()-1 ? mat.cols()-1: 
					cy < 0 ? 0:
					cy);

	return mat((int)std::floor(cx),(int)std::floor(cy))* (std::ceil(cx) -cx)*(std::ceil(cy)-cy)
			+mat((int)std::floor(cx),(int)std::ceil(cy))* (std::ceil(cx) -cx)*(-std::floor(cy)+cy)
			+mat((int)std::ceil(cx),(int)std::floor(cy))* (-std::floor(cx) +cx)*(std::ceil(cy)-cy)
			+mat((int)std::ceil(cx),(int)std::ceil(cy))* (-std::floor(cx) +cx)*(-std::floor(cy)+cy)
			;
}

template<typename Eigen_T_weights>
float totalError_of(
	std::vector<explainMe::Cloud::Ptr > &clouds,
	std::vector<std::vector<Eigen::MatrixXi>> &I,
	std::vector<std::vector<Eigen::MatrixXf>> &smoothedDepth,
	int pyr_level,
	std::vector<Eigen_T_weights> &weights,
	int weight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<Eigen::Matrix4f> &a_inv,
	std::vector<std::vector<int>> &enforce, 
	int referenceCloud, //acts as reference frame
	float alpha_point2plane,
	float alpha_point2point,
	float alpha_of,
	float culling_dist,
	RangeSensorDescription & d,
	std::vector<mySimpleKdTree_ndim<6>> & allTrees,
	bool usedDepthAsIntensity)
{
	//always compute and rate by the final error. i.e. level 0 error.
	pyr_level = 0;
	explainMe::Point p,q;
	float w;
	//kdtree method parameters
	std::vector<int> idx;
	std::vector<float> dist_sqr;
	float total_err = 0;
	for(int i = 0; i < enforce.size(); i++)
		{
			//myKdtree
			//mySimpleKdTree myTree;
			//myTree.setData<explainMe::Cloud::Ptr>(clouds[i]);
			//myTree.buildTree();
			auto & myTree = allTrees[i];
			int ith = std::ceil(weights[i].col(weight_idx).sum() / 2500 + 0.1);// + std::max<int>((int) std::floor((numIterations -1 - its) * 1.0/(numIterations-1) * 10+ 0.5), 1);
			
			for(int nbr = 0; nbr < enforce[i].size(); nbr++)
			{
				int j = enforce[i][nbr];
				float total_w = 0;

				//points in cloud j need to be mapped to the coordinate frame of i for the nn search
				//that is a[i]*a_inv[j]
				Eigen::Matrix4f j_to_i = alignments[i]*a_inv[j];
				//for(auto qit = clouds[j]->begin(); qit!= clouds[j]->end(); qit++)

				const int clouds_j_size = clouds[j]->size();
				//int q_idx;

								
#pragma omp parallel private(p,q,w,idx,dist_sqr) num_threads(20)//20
{

Eigen::Vector3f Rx_plus_T;
float total_err_ = 0, total_diffI_ = 0;
simpleNNFinder_ndim<6> nnFinder(myTree);
#pragma omp for nowait
				for(int q_idx = 0; q_idx < clouds_j_size; q_idx ++)
				{
					//ignore zero weight points:
					if(weights[j](q_idx,weight_idx) < 1e-3f){
						continue;
					}
					//"multi res"
					if(!((q_idx) % ith == 0)){
						continue;
					}

					//now: if p and q are not yet aligned.
					q = clouds[j]->at(q_idx);//*qit;
					q.getVector4fMap() = j_to_i * q.getVector4fMap();
					q.getNormalVector3fMap() = j_to_i.topLeftCorner<3,3>() * q.getNormalVector3fMap();
					//p = kdTree->NN(q);
					idx.resize(1,1);
					idx[0] = nnFinder.findNN(simpleTreeTools::p(q.x,q.y,q.z, q.r, q.g,q.b), culling_dist*culling_dist/*/(2*its+1)*/).index;
					if(idx[0] >= 0){
						//valid
						p = clouds[i]->at(idx[0]);
						
						//weight/culling
						w= p.getNormalVector3fMap().dot(q.getNormalVector3fMap()); w = (w< 0.7071? 0:w); w= (w*0==0?w:0.5); 
						w*= (1-(q.getVector3fMap() -p.getVector3fMap()).norm()/culling_dist);	w=w<0?0:w;
					
						//daring
						w*= std::max<float>(weights[i](idx[0],weight_idx) , weights[j](q_idx,weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);
						//conservative:
						//w*= std::min<float>(weights[i](idx[0],weight_idx) , weights[j](q_idx,weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);
						
						//point to point error
						total_err_ += w * (p.getVector3fMap()-q.getVector3fMap()).squaredNorm() * alpha_point2point;

						//point to plane errpr
						total_err_ += w * std::abs(p.getNormalVector3fMap().dot(q.getVector3fMap() - p.getVector3fMap())) *alpha_point2plane;						
					}
					else{
						total_err_ += culling_dist *(alpha_point2plane + alpha_point2point);
					}
					//Note: this can be done, no matter if next neighbors are found or not....
					//optical flow x
					//Add this only if I_x and IA_x are valid- and the meshes are neighbors ? (vs light change)
					//reset p,q
					//p = clouds[i]->at(idx[0]); //p not needed.
					q = clouds[j]->at(q_idx);

					Eigen::RowVector2f dI_x_y ; //= the dI at j_to_i*q in p's frame
					float I_x, I_Ax; //the I at q and I at j_to_i*q in p's frame
					float coord_x_q, coord_y_q, coord_x_Aq, coord_y_Aq;
					d.project(q.x,q.y,q.z,coord_x_q, coord_y_q);
					Rx_plus_T = (j_to_i * q.getVector4fMap()).topLeftCorner<3,1>();
					d.project(Rx_plus_T.x(),Rx_plus_T.y(),Rx_plus_T.z(),coord_x_Aq, coord_y_Aq);

					//not inside frustum?
					//not neighboring (because of ilumination changes..)- or stuff happens on the depth frames.
					if(coord_x_Aq <0.01 || coord_y_Aq < 0.01 ||coord_x_q<0.01 || coord_y_q < 0.01 ||
						coord_x_Aq > d.width()-1.01 || coord_y_Aq > d.height() -1.01||coord_x_q> d.width() -1.01|| coord_y_q > d.height() -1.01
						|| (std::abs(i-j) > 1.5 && ! usedDepthAsIntensity)
						)
					{
						continue;
					}

						
					//adapt to pyramid level:
					coord_x_q = coord_x_q / pow(2,pyr_level);
					coord_y_q = coord_y_q/ pow(2,pyr_level);
					coord_x_Aq =coord_x_Aq/ pow(2,pyr_level);
					coord_y_Aq =coord_y_Aq/ pow(2,pyr_level);
						
					w= weights[j](q_idx,weight_idx);
					//smoothed depth should not be much smaller than the current z. That is, if z- smoothed is positive, we are close to an occluder, the more positive, the stronger!
					w*= std::pow(10.f,
						-std::max<float>(
								Rx_plus_T.z()- interpolate(smoothedDepth[i][pyr_level], coord_x_Aq, coord_y_Aq) -0.03,
								0
							)
						*2.0/culling_dist);//*/
												
					I_x =interpolate(I[j][pyr_level],coord_x_q, coord_y_q );
					I_Ax = interpolate(I[i][pyr_level],coord_x_Aq, coord_y_Aq );;

					total_err_+= alpha_of * w * (std::pow<float>((I_x - I_Ax),2));
				}

				#pragma omp critical
				{
					total_err += total_err_;
				}
}//end omp parallel (nn and pairwise matrix computation)

			}
		}
		return total_err;
}

template<typename Eigen_T_weights>
float totalError(
	std::vector<explainMe::Cloud::Ptr > &clouds,
	std::vector<Eigen_T_weights> &weights,
	int weight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<Eigen::Matrix4f> &a_inv,
	std::vector<std::vector<int>> &enforce,
	int referenceCloud, //acts as reference frame
	float alpha_point2plane,
	float alpha_point2point,
	float culling_dist,
	std::vector<mySimpleKdTree_ndim<6>> & allTrees)
{
	//always compute and rate by the final error. i.e. level 0 error.
	explainMe::Point p, q;
	float w;
	//kdtree method parameters
	std::vector<int> idx;
	std::vector<float> dist_sqr;
	float total_err = 0;
	for (int i = 0; i < enforce.size(); i++)
	{
		//myKdtree
		//mySimpleKdTree myTree;
		//myTree.setData<explainMe::Cloud::Ptr>(clouds[i]);
		//myTree.buildTree();
		auto & myTree = allTrees[i];
		int ith = std::ceil(weights[i].col(weight_idx).sum() / 2500 + 0.1);// + std::max<int>((int) std::floor((numIterations -1 - its) * 1.0/(numIterations-1) * 10+ 0.5), 1);

		for (int nbr = 0; nbr < enforce[i].size(); nbr++)
		{
			int j = enforce[i][nbr];
			float total_w = 0;

			//points in cloud j need to be mapped to the coordinate frame of i for the nn search
			//that is a[i]*a_inv[j]
			Eigen::Matrix4f j_to_i = alignments[i] * a_inv[j];
			//for(auto qit = clouds[j]->begin(); qit!= clouds[j]->end(); qit++)

			const int clouds_j_size = clouds[j]->size();
			//int q_idx;


#pragma omp parallel private(p,q,w,idx,dist_sqr) num_threads(20)//20
			{

				Eigen::Vector3f Rx_plus_T;
				float total_err_ = 0, total_diffI_ = 0;
				simpleNNFinder_ndim<6> nnFinder(myTree);
#pragma omp for nowait
				for (int q_idx = 0; q_idx < clouds_j_size; q_idx++)
				{
					//ignore zero weight points:
					if (weights[j](q_idx, weight_idx) < 1e-3f){
						continue;
					}
					//"multi res"
					if (!((q_idx) % ith == 0)){
						continue;
					}

					//now: if p and q are not yet aligned.
					q = clouds[j]->at(q_idx);//*qit;
					q.getVector4fMap() = j_to_i * q.getVector4fMap();
					q.getNormalVector3fMap() = j_to_i.topLeftCorner<3, 3>() * q.getNormalVector3fMap();
					//p = kdTree->NN(q);
					idx.resize(1, 1);
					idx[0] = nnFinder.findNN(simpleTreeTools::p(q.x, q.y, q.z, q.r, q.g, q.b), culling_dist*culling_dist/*/(2*its+1)*/).index;
					if (idx[0] >= 0){
						//valid
						p = clouds[i]->at(idx[0]);

						//weight/culling
						w = p.getNormalVector3fMap().dot(q.getNormalVector3fMap()); w = (w < 0.7071 ? 0 : w); w = (w * 0 == 0 ? w : 0.5);
						w *= (1 - (q.getVector3fMap() - p.getVector3fMap()).norm() / culling_dist);	w = w < 0 ? 0 : w;

						//daring
						w *= std::max<float>(weights[i](idx[0], weight_idx), weights[j](q_idx, weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);
						//conservative:
						//w*= std::min<float>(weights[i](idx[0],weight_idx) , weights[j](q_idx,weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);

						//point to point error
						total_err_ += w * (p.getVector3fMap() - q.getVector3fMap()).squaredNorm() * alpha_point2point;

						//point to plane errpr
						total_err_ += w * std::abs(p.getNormalVector3fMap().dot(q.getVector3fMap() - p.getVector3fMap())) *alpha_point2plane;
					}
					else{
						total_err_ += culling_dist *(alpha_point2plane + alpha_point2point);
					}
#pragma omp critical
					{
						total_err += total_err_;
					}
				}
			}//end omp parallel (nn and pairwise matrix computation)
		}
	}
	return total_err;
}

//assumes that there are no nans int the clouds.
//heuristic spped ups: decreasing maximal radius in nn search
//multiresolution through multiresolution via subsampling.
template<typename Eigen_T_weights>
void BundleAdjustment::pointToPlane_Bundle_step_of(
	std::vector<explainMe::Cloud::Ptr > &clouds,
	std::vector<std::vector<Eigen::MatrixXi>> &I,
	std::vector<std::vector<Eigen::MatrixXi>> &dI_dx,
	std::vector<std::vector<Eigen::MatrixXi>> &dI_dy,
	std::vector<std::vector<Eigen::MatrixXf>> &smoothedDepth,
	bool usedDepthAsIntensity,
	int pyr_level,
	std::vector<Eigen_T_weights> &weights,
	int weight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
	int referenceCloud, //acts as reference frame
	int numIterations,
	float alpha_point2plane,
	float alpha_point2point,
	float alpha_of,
	float alpha_color,
	float culling_dist,
	RangeSensorDescription & d,
	LM_params & params)
{


	StopWatch buildTree, buildMatrix, solve,total;
	total.restart();

	Eigen::Vector4f centroid;
	centroid <<	CloudTools::compute3DCentroid(clouds[referenceCloud]), 0; //BAD: not of weighted points, as there need not be any in the reference cloud.

	//compute mapping of all clouds to the basecloud.
	std::vector<Eigen::Matrix4f> a_inv;
	for(int i = 0; i < clouds.size(); i++){
		//apply transf -1
		Eigen::Matrix4f a_inv_;
		a_inv_ = alignments[i].inverse();
		if(i == referenceCloud && !(a_inv_.trace() < 4.001 && a_inv_.trace() > 3.999)){
			std::cout << "in bundleadjustment, reference alignment should be identity!";
		}
		a_inv.push_back(a_inv_);
	}

	std::cout << "computing bs and cs...";

	//temps 
	Vector6f c,b; Matrix6f ccT;
	explainMe::Point p,q;
	float w;
	//kdtree method parameters
	std::vector<int> idx;
	std::vector<float> dist_sqr;

	//build trees outside of the loop. (TODO push to algorithm, one time kdtree build)
	Vector6f scales; scales << 1,1,1,alpha_color,alpha_color,alpha_color;
	std::vector<mySimpleKdTree_ndim<6>> allTrees(clouds.size(), mySimpleKdTree_ndim<6>(scales));
	const int nClouds = clouds.size();

	buildTree.restart();
#pragma omp parallel
	{
#pragma omp for
		for(int i = 0; i <nClouds; i++)
		{
			simpleTreeTools::setData<explainMe::Cloud::Ptr>(allTrees[i], clouds[i]);
			allTrees[i].buildTree();
		}
	}
	buildTree.stop();

	float last_error = params.last_error;
	if(params.last_error < 0){
		last_error =  totalError_of<Eigen_T_weights>(clouds,
					I,smoothedDepth, pyr_level,
					weights, weight_idx, 
					alignments, a_inv, enforce,
					referenceCloud,
					alpha_point2plane, alpha_point2point, alpha_of, culling_dist,d,allTrees,
					usedDepthAsIntensity);
	}
	std::cout << "Initial error: " << last_error << "\n";
	float lambda_damping = params.lambda;
	int ith ;//= std::max<int>((int) std::floor((numIterations -1 - its) * 1.0/(numIterations-1) * 10+ 0.5), 1);

	float x_max_coeff = 0;
	int its = 0;
//	for(int its = 0; its < numIterations; its++){
		//int ith = numIterations -its; //will keep each ith point only for registration
		//1 for its = numiteration -1. ith maximal = 10. (its = 0)
		do{
			std::vector<Matrix6f> cs;
			std::vector<Vector6f> bs;
			float total_diffI = 0;	

			buildMatrix.restart();		
			//1st compute the various 6x6 blocks of the ba matrix
			//The single blocks coincide with the std point to plane blocks.
			//compute all c's abd b's. This involves the association of cloud j to cloud i
			for(int i = 0; i < enforce.size(); i++)
			{
				auto & myTree = allTrees[i];
				ith = std::ceil(weights[i].col(weight_idx).sum() / 2500 + 0.1);// + std::max<int>((int) std::floor((numIterations -1 - its) * 1.0/(numIterations-1) * 10+ 0.5), 1);
			
				for(int nbr = 0; nbr < enforce[i].size(); nbr++)
				{
					//constraint between cloud i and j.
					int j = enforce[i][nbr];
					b = Vector6f::Zero();
					ccT = Matrix6f::Zero();
					float total_w = 0;

					//points in cloud j need to be mapped to the coordinate frame of i for the nn search
					//that is a[i]*a_inv[j]
					Eigen::Matrix4f j_to_i = alignments[i]*a_inv[j];
					const int clouds_j_size = clouds[j]->size();
					
					Eigen::Matrix<float, 3,3> K;
					//K << d.intrinsicK()
					auto K_  = d.intrinsicK();
					K(0,0) = K_(0,0);K(0,1) = K_(0,1);K(0,2) = K_(0,2);
					K(1,0) = K_(1,0);K(1,1) = K_(1,1);K(1,2) = K_(1,2);
					K(2,0) = K_(2,0);K(2,1) = K_(2,1);K(2,2) = K_(2,2);

					

	#pragma omp parallel private(p,q,c,w,idx,dist_sqr) num_threads(20)//20
	{
	Matrix6f ccT_,ccT_p2p;Vector6f b_, b_p2p; ccT_.fill(0); b_.fill(0);
	Eigen::Vector3f Rx_plus_T;
	Eigen::Matrix<float, 2,3> dg_dxy;
	Eigen::Matrix<float, 3,6> DRT_dalphat;
	float total_w_ = 0, total_diffI_ = 0;
	simpleNNFinder_ndim<6> nnFinder(myTree);
	#pragma omp for nowait
					for(int q_idx = 0; q_idx < clouds_j_size; q_idx ++)
					{
						//ignore zero weight points:
						if(weights[j](q_idx,weight_idx) < 1e-3f){
							continue;
						}
						//"multi res"
						//if(q_idx % (2*(numIterations -its)-1) != 0){ //%(numIterations -its)
						//if(!(q_idx % (numIterations -its) == 0) && !((q_idx+1) % (numIterations -its) == 0)){
						if(!((q_idx+ rand()) % ith == 0)){
							continue;
						}

						//now: if p and q are not yet aligned.
						q = clouds[j]->at(q_idx);//*qit;
						q.getVector4fMap() = j_to_i * q.getVector4fMap();
						//p = kdTree->NN(q);

						idx.resize(1,1);
						idx[0] = nnFinder.findNN(simpleTreeTools::p(q.x,q.y,q.z, q.r, q.g,q.b), culling_dist*culling_dist/*/(2*its+1)*/).index;
						if(idx[0] >= 0){
							//valid
							//continue; //To test time consumption of lower part. (irrelevant)
							p = clouds[i]->at(idx[0]);

							//reset q
							q = clouds[j]->at(q_idx);//*qit;
							//now p and q are found, but need to be mapped such that they can be used to compute an increment of the alignments
							//i.e. map to reference cloud and minus centroid.
							p.getVector4fMap() = a_inv[i]*p.getVector4fMap() - centroid; p.getNormalVector3fMap() = a_inv[i].topLeftCorner<3,3>() *p.getNormalVector3fMap();
							q.getVector4fMap() = a_inv[j]*q.getVector4fMap() - centroid; q.getNormalVector3fMap() = a_inv[j].topLeftCorner<3,3>() *q.getNormalVector3fMap();


							//weight/culling
							w= p.getNormalVector3fMap().dot(q.getNormalVector3fMap()); w = (w< 0.7071? 0:w); w= (w*0==0?w:0.5); 
							w*= (1-(q.getVector3fMap() -p.getVector3fMap()).norm()/culling_dist);	w=w<0?0:w;
					
							//daring
							w*= std::max<float>(weights[i](idx[0],weight_idx) , weights[j](q_idx,weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);
							//conservative:
							//w*= std::min<float>(weights[i](idx[0],weight_idx) , weights[j](q_idx,weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);
						
							total_w_ += w;
							//w=1;
							//the point to plane equation
							c << p.getVector3fMap().cross(q.getNormalVector3fMap()), q.getNormalVector3fMap();
							ccT_ += c*c.transpose()*w*alpha_point2plane;
							b_ += c*(p.getVector3fMap()-q.getVector3fMap()).dot(q.getNormalVector3fMap())*w*alpha_point2plane;


							//point to point...
							b_p2p << q.getVector3fMap().cross(p.getVector3fMap()- q.getVector3fMap()), p.getVector3fMap()- q.getVector3fMap();
							ccT_p2p <<	q.y*q.y + q.z*q.z,	-q.x*q.y,	-q.x*q.z,	0,		-q.z,	q.y,
								-q.y*q.x,	q.x*q.x + q.z*q.z,	-q.y*q.z,	q.z,	0,		-q.x,
								-q.z*q.x,	-q.z*q.y,	q.x*q.x + q.y*q.y,	-q.y,	q.x,	0,
								0,			q.z,		-q.y,				1,		0,		0,	
								-q.z,		0,			q.x,				0,		1,		0,
								q.y,		-q.x,		0,					0,		0,		1;

							ccT_ += ccT_p2p * w * alpha_point2point;
							b_+= b_p2p * w * alpha_point2point;//*/
						}
					
						//optical flow x
						//Add this only if I_x and IA_x are valid- and the meshes are neighbors ? (vs light change)
						//reset p,q
						q = clouds[j]->at(q_idx);

						Eigen::RowVector2f dI_x_y ; //= the dI at j_to_i*q in p's frame
						float I_x, I_Ax; //the I at q and I at j_to_i*q in p's frame
						float coord_x_q, coord_y_q, coord_x_Aq, coord_y_Aq;
						d.project(q.x,q.y,q.z,coord_x_q, coord_y_q);
						Rx_plus_T = (j_to_i * q.getVector4fMap()).topLeftCorner<3,1>();
						d.project(Rx_plus_T.x(),Rx_plus_T.y(),Rx_plus_T.z(),coord_x_Aq, coord_y_Aq);

												
						//not inside frustum?
						//not neighboring (because of ilumination changes..)- or stuff happens on the depth frames.
						if(coord_x_Aq <0.01 || coord_y_Aq < 0.01 ||coord_x_q<0.01 || coord_y_q < 0.01 ||
							coord_x_Aq > d.width()-1.01 || coord_y_Aq > d.height() -1.01||coord_x_q> d.width() -1.01|| coord_y_q > d.height() -1.01
							|| (std::abs(i-j) > 1.5 && ! usedDepthAsIntensity)
							)
						{
							continue;
						}

						//adapt to pyramid level:
						coord_x_q = coord_x_q / pow(2,pyr_level);
						coord_y_q = coord_y_q/ pow(2,pyr_level);
						coord_x_Aq =coord_x_Aq/ pow(2,pyr_level);
						coord_y_Aq =coord_y_Aq/ pow(2,pyr_level);
						
						w= weights[j](q_idx,weight_idx);
						//smoothed depth should not be much smaller than the current z. That is, if z- smoothed is positive, we are close to an occluder, the more positive, the stronger!
						w*= std::pow(10.f,
							-std::max<float>(
									Rx_plus_T.z()- interpolate(smoothedDepth[i][pyr_level], coord_x_Aq, coord_y_Aq) -0.03,
									0
								)
							*2.0/culling_dist);//*/
												
						//could try to further rate object mismatches...
						//this behaves strangely. diff on the order of 8.
						//std::cout << w << "," << weights[j](q_idx,weight_idx) << "," << Rx_plus_T.z()- interpolate(smoothedDepth[i][pyr_level], coord_x_Aq, coord_y_Aq) <<"\n";
						//std::cout << "\t q.z: " << q.z << "\t" << Rx_plus_T.z() << "\n\t" ;
						//	std::cout <<"\t ipol dpth" << interpolate(smoothedDepth[j][pyr_level], coord_x_q, coord_y_q) << "\t"<<interpolate(smoothedDepth[i][pyr_level], coord_x_Aq, coord_y_Aq)<< "\n";

						I_x =interpolate(I[j][pyr_level],coord_x_q, coord_y_q );
						I_Ax = interpolate(I[i][pyr_level],coord_x_Aq, coord_y_Aq );;
						dI_x_y << interpolate(dI_dx[i][pyr_level],coord_x_Aq, coord_y_Aq ),
							interpolate(dI_dy[i][pyr_level],coord_x_Aq, coord_y_Aq );
						dI_x_y /= pow(2,pyr_level); //as (f(x) - f(x+h)) / h and h dependent on pyr level.

						dI_x_y *= 0.25; //normalizing the sobel filter....
						
						//std::cout << dI_x_y << " i, i_ax " << I_x << " " << I_Ax <<"\n";
						
						Rx_plus_T = K * (j_to_i * q.getVector4fMap()).topLeftCorner<3,1>();//
						dg_dxy<< 1/ Rx_plus_T.z() ,0, -Rx_plus_T.x()/Rx_plus_T.z()/Rx_plus_T.z(),
							0, 1/ Rx_plus_T.z() , -Rx_plus_T.y()/Rx_plus_T.z()/Rx_plus_T.z();
						
						//invalid?
						if(dg_dxy(0,0)*0 !=0){
							std::cout << "-";
							continue;
						}

						//q.getVector4fMap() = a_inv[j]*q.getVector4fMap();
						q.getVector4fMap() = a_inv[j]*q.getVector4fMap() - centroid;
						/*DRT_dalphat		<<	 0,		q.z,	-q.y,	-1,	0,	0,
											-q.z,	0,		q.x,	0,	-1,	0,
											q.y,	-q.x,	0,		0,	0,	-1;/*/
						DRT_dalphat		<<	 0,		-q.z,	q.y,	-1,	0,	0,
											q.z,	0,		-q.x, 0,	-1,	0,
											-q.y,	q.x,	0,		0,	0,	-1;//*/

						DRT_dalphat = alignments[i].topLeftCorner<3,3>() * DRT_dalphat;
						
						c = 2 * dI_x_y * dg_dxy * K * DRT_dalphat;
						//in the case where projection error is minimized, there should be an additional term
						//(DRT_alphat).rowz.
						if(usedDepthAsIntensity){
							c-=2*DRT_dalphat.row(2);
						}
						
						//the equation is c'*params = -E, that is in least squates sense cc'*params = -Ec, which is gauss newton.
						b_-= alpha_of *w * (I_x - I_Ax)  *c;
						ccT_ += alpha_of*w*c*c.transpose();

						total_diffI_+= w*std::pow<float>((I_x - I_Ax),2);
					}

					#pragma omp critical
					{
						ccT += ccT_;
						b += b_;
						total_w += total_w_;
						total_diffI += total_diffI_;
					}
	}//end omp parallel (nn and pairwise matrix computation)

					cs.push_back(ccT);
					bs.push_back(b);
				}
			}
			buildMatrix.stop();
			//setting up the bundle adjustment matrix (see notes)
			Eigen::MatrixXf total(6*(clouds.size()-1), 6*(clouds.size()-1));
			Eigen::VectorXf total_b(6*(clouds.size()-1));
			Eigen::VectorXf X(6*(clouds.size()-1));

			total = Eigen::MatrixXf::Zero(6*(clouds.size()-1), 6*(clouds.size()-1));
			total_b.fill(0);

			int ind =0;
			for(int i_cloud = 0; i_cloud < enforce.size(); i_cloud++)
			{
				for(int nbr = 0; nbr < enforce[i_cloud].size(); nbr++)
				{
					int j_cloud = enforce[i_cloud][nbr]; 
					int i = (i_cloud > referenceCloud ? i_cloud-1: i_cloud);//parameter block
					int j = (j_cloud > referenceCloud ? j_cloud-1: j_cloud);

					if(i_cloud==referenceCloud){
						total.block<6,6>(j*6,j*6) += cs[ind];
						total_b.block<6,1>(j*6,0) -= bs[ind];
					}
					if(j_cloud == referenceCloud){
						total.block<6,6>(i*6,i*6) += cs[ind];
						total_b.block<6,1>(i*6,0) += bs[ind];
					}
					else{
						total.block<6,6>(i*6,i*6) += cs[ind];
						total.block<6,6>(i*6,j*6) -= cs[ind];
						total.block<6,6>(j*6,i*6) -= cs[ind];
						total.block<6,6>(j*6,j*6) += cs[ind];
					
						total_b.block<6,1>(i*6,0) += bs[ind];
						total_b.block<6,1>(j*6,0) -= bs[ind];
					}

					ind ++;
				}
			}

			//damping:
			total = total +  lambda_damping * Eigen::MatrixXf::Identity(6*(clouds.size()-1), 6*(clouds.size()-1));
			//total = total + Eigen::MatrixXf(total.diagonal().asDiagonal()) * lambda_damping;

			solve.restart();
			//following solve does not work, i do not know why. thought QR should be the right thing!
			//X = total.fullPivHouseholderQr().solve(total_b);
	
			//works, cholesky on normal eq.
			//X = (total.transpose()*total).ldlt().solve(total.transpose() *total_b);
			//svd based solve:
			//X =total.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(total_b);

			// The matrix is "often" positive semidefinite, in that provably it isn't always, but in practice I never experienced it not to be.
			auto ldlt = (total).ldlt();
			X = ldlt.solve(total_b);
			//resort to more robust, more expensive method if needed.X is left untouched if ldlt fails...
			float xnorm =X.norm();
			if(xnorm == 0 || ! ldlt.isPositive()){
				std::cout << "\n Matrix not pos def!\n";
				X = total.fullPivLu().solve(total_b);
				xnorm = X.norm();
				std::cout << "X: " << X.norm() << " b " << total_b.norm() << " total.sum " <<   total.diagonal().sum() << "\n";
			}
		
			solve.stop();
		
			std::cout << "Total Norm of lie-element vector: " << X.norm() << "\t(" << its << " " <<ith << ")\n";
			std::cout << "Total diff I extrapolated: " << total_diffI * ith << "\n";
			if(X.norm() > 10){
				std::cout << " **** Xmax: " << X.maxCoeff() <<  "!\n";
				X = total.fullPivLu().solve(total_b);
				std::cout << "LU solve:: " << X.norm() << "\n";
			}
		
			auto new_alginments = alignments, new_inv_alignments =a_inv;
			//extract all alignments.
			for(int i_cloud = 0; i_cloud < alignments.size(); i_cloud++){
				if(i_cloud != referenceCloud){
					int i = (i_cloud > referenceCloud ? i_cloud -1 :i_cloud);
					Vector6f x_icp_total = -X.block<6,1>(i*6,0); 
				
					//map linearized transform back
					float cg = std::cos(x_icp_total(2)); float ca = std::cos(x_icp_total(0)); float cb = std::cos(x_icp_total(1));
					float sg = std::sin(x_icp_total(2)); float sa = std::sin(x_icp_total(0)); float sb = std::sin(x_icp_total(1));
					Eigen::Matrix4f  alignment;
					alignment << cg*cb,			sg*cb,				-sb,	-x_icp_total(3),
						-sg*ca + cg*sb*sa,	cg*ca + sg*sb*sa,	cb*sa,	-x_icp_total(4),
						sg*sa + cg*sb*ca,	-cg*sa + sg*sb*ca,	cb*ca,	-x_icp_total(5),
						0,			0,		0,					1;

					alignment.topRightCorner<4,1>() +=  centroid - alignment*centroid;

					new_alginments[i_cloud] = alignments[i_cloud]*alignment;
					new_inv_alignments[i_cloud] = new_alginments[i_cloud].inverse();
				}
			}

			x_max_coeff = X.maxCoeff();

			//decide on damping etc:
			float new_error = totalError_of<Eigen_T_weights>(clouds,
				I,smoothedDepth, pyr_level,
				weights, weight_idx, 
				new_alginments, new_inv_alignments, enforce,
				referenceCloud,
				alpha_point2plane, alpha_point2point, alpha_of, culling_dist,d,allTrees,
				usedDepthAsIntensity);
			std::cout << "\n\tNew and last error: " << new_error << "\t\t" << last_error << "\n";
			if(new_error < last_error && x_max_coeff< 0.5){ //larger and the linearization is to unrelieable.
				last_error = new_error;
				lambda_damping *= 0.6f;
				//lambda_damping *= 0.9f;
				//use new alignments
				alignments = new_alginments;
				a_inv = new_inv_alignments;
				std::cout << "updatedError: " << last_error << "\n";
			}
			else{
				lambda_damping *= 10;
			//	lambda_damping *= 1.5;
				//discard new alignments
			}
			std::cout << "Damping: " << lambda_damping << "\n";
			std::cout << "XMax: " << x_max_coeff << "\n";

		}while(++its < numIterations && x_max_coeff > 1e-4);
	total.stop();

	params.lambda = lambda_damping;
	params.last_error = last_error;

}

/*
template<typename Eigen_T_weights>
void flannDummmyStub(
	std::vector<explainMe::Cloud::Ptr > &clouds,
	std::vector<Eigen_T_weights> &weights,
	int weight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
	int referenceCloud, //acts as reference frame
	int numIterations,
	float alpha_point2plane,
	float alpha_point2point,
	float alpha_color,
	float culling_dist,
	RangeSensorDescription & d,
	LM_params & params)
{
	StopWatch buildTree, buildMatrix, solve, total, otherMath, compError;
	const int nClouds = clouds.size();
	const int numPointsToUse = 2500;

	std::cout << " starting lm registration timing stub";
	total.restart();

	Eigen::Vector4f centroid;
	centroid << CloudTools::compute3DCentroid(clouds[referenceCloud]), 0; //BAD: not of weighted points, as there need not be any in the reference cloud.

	otherMath.restart();
	//compute mapping of all clouds to the basecloud.
	std::vector<Eigen::Matrix4f> a_inv;
	for (int i = 0; i < clouds.size(); i++){
		//apply transf -1
		Eigen::Matrix4f a_inv_;
		a_inv_ = alignments[i].inverse();
		if (i == referenceCloud && !(a_inv_.trace() < 4.001 && a_inv_.trace() > 3.999)){
			std::cout << "in bundleadjustment, reference alignment should be identity!";
		}
		a_inv.push_back(a_inv_);
	}
	otherMath.stop();

	bool useFLANN = true;
	int numPairs = 0;
	int totalNumPoints = 0;

	//stub for flannbased nn.
	if (useFLANN){
	std::vector<std::shared_ptr<flann::Matrix<float>>> flannData;
	std::vector<std::vector<float>> rawData;

	std::vector<std::shared_ptr<flann::KDTreeCuda3dIndex< flann::L2<float> >>> flannTrees;
	std::vector<flann::Matrix<float>> queryData;
	std::vector<std::vector<float>> rawQueryData;
	buildTree.restart();
	std::cout << "building trees...";
	for (int cloud_idx = 0; cloud_idx < nClouds; cloud_idx++){
	auto & cloud = clouds[cloud_idx];
	rawData.push_back(std::vector<float>());
	rawData.back().resize(cloud->size() * 3, 0);
	int p_idx = 0;
	for (auto p : cloud->points){
	rawData.back()[p_idx] = p.x; p_idx++;
	rawData.back()[p_idx] = p.y; p_idx++;
	rawData.back()[p_idx] = p.z; p_idx++;
	}
	flannData.push_back(std::shared_ptr<flann::Matrix<float>>(new flann::Matrix<float>(&(rawData.back()[0]), cloud->size(), 3)));
	}



	flannTrees.resize(nClouds);
	std::cout << "kk...";
	//#pragma omp parallel
	{
	//#pragma omp for
	for (int cloud_idx = 0; cloud_idx < nClouds; cloud_idx++){
	std::cout << cloud_idx;
	std::shared_ptr<flann::KDTreeCuda3dIndex< flann::L2<float> >> tree_pointer(new flann::KDTreeCuda3dIndex< flann::L2<float> >( *(flannData[cloud_idx])));
	tree_pointer->buildIndex();
	//flannTrees.push_back(flann::KDTreeCuda3dIndex< flann::L2<float> >(index)); //no copy constructor defined for the index.
	//flannTrees.push_back(tree_pointer);
	flannTrees[cloud_idx] = tree_pointer;

	}
	}
	buildTree.stop();
	std::cout << "built trees. \n";

	//build query
	//std::vector<std::vector<int>> allQueryIndices;
	//allQueryIndices.resize(nClouds, std::vector<int>());

	otherMath.restart();
	rawQueryData.resize(nClouds, std::vector<float>());
	#pragma omp parallel
	{
	#pragma omp for nowait
	for (int i = 0; i < nClouds; i++){
	//	auto & qi = allQueryIndices[i];
	auto & qi = rawQueryData[i];
	auto & q_points = clouds[i]->points;
	qi.reserve(numPointsToUse * 3 + 500);
	int sz = clouds[i]->size();
	int offset = 0;
	int ith = std::ceil(weights[i].col(weight_idx).sum() / numPointsToUse + 0.1);
	for (int q_idx = 0; q_idx < sz; q_idx += ith) // Q_idx++);
	{
	//rand has a state, so who knows what happens in multithreaded env. but i don't care, I just need a little bit of randomness, not good one.
	offset = rand() % ith;
	//ignore zero weight points:
	if (q_idx + offset >= sz || weights[i](q_idx + offset, weight_idx) < 1e-3f){
	continue;
	}
	//qi.push_back(q_idx + offset);
	qi.push_back(q_points[q_idx].x);
	qi.push_back(q_points[q_idx].y);
	qi.push_back(q_points[q_idx].z);
	}

	queryData.push_back(flann::Matrix<float>(&(qi[0]), qi.size()/3, 3));
	}
	}
	otherMath.stop();
	std::cout << "built queries... \n";

	//
	buildMatrix.restart();
	std::vector<int> result_ind_buffer(numPointsToUse + 500);
	std::vector<float> result_dist_buffer(numPointsToUse + 500);

	for (int i = 0; i < enforce.size(); i++)
	{
	for (int nbr = 0; nbr < enforce[i].size(); nbr++)
	{
	//constraint between cloud i and j.
	int j = enforce[i][nbr];
	auto & query = queryData[j];

	//points in cloud j need to be mapped to the coordinate frame of i for the nn search
	//that is a[i]*a_inv[j]
	Eigen::Matrix4f j_to_i = alignments[i] * a_inv[j];

	//apply this somehow to the query data.

	//reserve space for results
	result_dist_buffer.resize(query.rows);
	result_ind_buffer.resize(query.rows);

	// do a knn search, using 128 checks
	flann::Matrix<int> result_indices(&(result_ind_buffer[0]), query.rows, 1);
	flann::Matrix<float> result_dists(&(result_dist_buffer[0]), query.rows, 1);
	flannTrees[i]->knnSearch(queryData[j], result_indices, result_dists, 1 //1-nn
	, flann::SearchParams(128));

	numPairs++;
	totalNumPoints += query.rows;

	}
	}

	std::cout << "finished building matirx \n";
	buildMatrix.stop();

	}
	total.stop();

	std::cout << "Number of pairs & points registered...: " << numPairs << "\t" << totalNumPoints << "\n";
	std::cout << "Timings : " << total.total() << "\t" << buildTree.total() << "\t" << buildMatrix.total() << "\t" << solve.total() << "\t" << otherMath.total() << "\t" << compError.total() << "\n";
}*/
//assumes that there are no nans int the clouds.
//heuristic spped ups: decreasing maximal radius in nn search
//multiresolution through multiresolution via subsampling.
template<typename Eigen_T_weights, typename Tree_type, typename Tree_query_type, typename Tree_nn_finder_type>
void BundleAdjustment::LM_BA_fast(
	std::vector<explainMe::Cloud::Ptr > &clouds,
	std::vector<Tree_type> & allTrees,
	std::vector<Eigen_T_weights> &weights,
	int weight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
	int referenceCloud, //acts as reference frame
	int numIterations,
	float alpha_point2plane,
	float alpha_point2point,
	float alpha_color,
	float culling_dist,
	RangeSensorDescription & d,
	LM_params & params,
	int numPointsToSample)
{
	//Timers
	StopWatch buildTree, buildMatrix, solve, total, otherMath, compError;
	//Variables to be
	const int numPointsToUse = numPointsToSample;//2500;

	//variables
	const int nClouds = clouds.size();
	const int enforce_size = enforce.size();
	//statistic
	int numPairs = 0;
	int totalNumPoints = 0;
	//lm variables
	Eigen::MatrixXf undamped_matrix_used_to_compute_new_alignments;
	Eigen::VectorXf right_hand_side_used_to_compute_new_alignments;
	float lambda_damping = params.lambda;
	float alignments_error = params.last_error;
	float alignments_avg_error = params.avg_error;
	float new_error = 0, total_weight = 0;
	if (params.last_error < 0){
		alignments_error = std::numeric_limits<float>::infinity();
	}
	//statistic
	for (auto enf : enforce){
		numPairs += enf.size();
	}
	
	//std::cout << " starting lm registration timing stub";

	//precomputations://
	//compute centroid and mapping of all clouds to the basecloud.
	total.restart();
	otherMath.restart();
	Eigen::Vector4f centroid;
	centroid << CloudTools::compute3DCentroid(clouds[referenceCloud]), 0; //BAD: not of weighted points, as there need not be any in the reference cloud.

	//alignments and a_inv store the current best alignments, with the error alignments error
	//new ainv , new alignments and new error store the error of a new alignment hypothesis
	std::vector<Eigen::Matrix4f> inv_alignments, new_inv_alignments, new_alignments;
	for (int i = 0; i < clouds.size(); i++){
		//apply transf -1
		Eigen::Matrix4f a_inv_;
		a_inv_ = alignments[i].inverse();
		if (i == referenceCloud && !(a_inv_.trace() < 4.001 && a_inv_.trace() > 3.999)){
			std::cout << "in bundleadjustment, reference alignment should be identity!";
		}
		inv_alignments.push_back(a_inv_);
	}

	new_inv_alignments = inv_alignments;
	new_alignments = alignments;

	otherMath.stop();

	
	//Select points to use for registration://
	otherMath.restart();
	std::vector<int> queryIndices;
	std::vector<std::vector<int>> allQueryIndices;
	allQueryIndices.resize(nClouds, queryIndices);

#pragma omp parallel 
	{
#pragma omp for 
		for (int i = 0; i < nClouds; i++){
			auto & qi = allQueryIndices[i];
			qi.reserve(numPointsToUse + 500);
			int sz = clouds[i]->size();
			int offset = 0;
			int ith = std::ceil(weights[i].col(weight_idx).sum() / numPointsToUse + 0.1);
			//V1 faster but is it the problem?
			/*for (int q_idx = 0; q_idx < sz; q_idx += ith) // Q_idx++);
			{
				//rand has a state, so who knows what happens in multithreaded env. but i don't care, I just need a little bit of randomness, not good one.
				offset = rand() % ith;
				//ignore zero weight points:
				if (q_idx + offset >= sz || weights[i](q_idx + offset, weight_idx) < 1e-3f){
					continue;
				}
				qi.push_back(q_idx + offset);
			}
			//V2,3,4
			/*/for (int q_idx = 0; q_idx < sz; q_idx ++) // Q_idx++);
			{
				//rand has a state, so who knows what happens in multithreaded env. but i don't care, I just need a little bit of randomness, not good one.
				//ignore zero weight points:
				if (weights[i](q_idx + offset, weight_idx) < 1e-3f){
					continue;
				}
				if (((q_idx + rand())% ith) == 0){
					qi.push_back(q_idx);
				}
			}//*/
		}
	}
	otherMath.stop();
		
	//registration loop://
	//vars fortermination
	float x_max_coeff = 1;
	int its = 0;
	bool energyStillDecreases = true;
	do{
		std::vector<std::vector<Matrix6f>> cs;
		std::vector<std::vector<Vector6f>> bs;
		cs.resize(enforce_size);
		bs.resize(enforce_size);

		//merging two loops for more paralelization
		//fill in matrices => allocate storage.
		for (int i = 0; i < enforce_size; i++){
			auto & c_mat_vec = cs[i];
			c_mat_vec.resize(enforce[i].size());
		}
		for (int i = 0; i < enforce_size; i++){
			auto & bs_vec = bs[i];
			bs_vec.resize(enforce[i].size());
		}
		std::vector<std::pair<int, int>> nbrset_nbr;
		for (int i = 0; i < enforce_size; i++){
			for (int nbr = 0; nbr < enforce[i].size(); nbr++){
				nbrset_nbr.push_back(std::pair<int, int>(i, nbr));
			}
		}
		int numPairConstraints = nbrset_nbr.size(); 


		buildMatrix.restart();
		new_error = 0;
		total_weight = 0;

//#pragma omp parallel shared(totalNumPoints) num_threads(24) default(none)//20
#pragma omp parallel num_threads(24) shared(totalNumPoints,numPairConstraints,nbrset_nbr, allTrees, enforce, new_alignments, new_inv_alignments, allQueryIndices, clouds,culling_dist,centroid,weights, weight_idx, alpha_point2point, alpha_point2plane,std::cout,cs,bs,new_error,total_weight)  default(none)//20
		{
			float w = 1;
			int idx;
			Vector6f c_;// b; Matrix6f ccT;
			explainMe::Point p, q;
		//1st compute the various 6x6 blocks of the ba matrix
		//The single blocks coincide with the std point to plane blocks.
		//compute all c's abd b's. This involves the association of cloud j to cloud i
		
		//Paralelization scheme I- problem: there are i which are much much more work...
/*#pragma omp for
		for (int i = 0; i < enforce_size; i++)
		{
			//int ith = std::ceil(weights[i].col(weight_idx).sum() / numPointsToUse + 0.1);//v4
			for (int nbr = 0; nbr < enforce[i].size(); nbr++)
			{/*/
//Paralelization scheme II-merged loop for more paralelization.
#pragma omp for
		for (int pair_idx = 0; pair_idx < numPairConstraints; pair_idx++){
			{
				int i = nbrset_nbr[pair_idx].first;
				int nbr = nbrset_nbr[pair_idx].second;
				//*/
				//local vars.
				float new_error_ = 0, total_weight_ = 0;
				Matrix6f ccT_;/*, ccT_p2p*/; Vector6f b_, b_p2p;
				ccT_.fill(0); b_.fill(0);

				auto & myTree = allTrees[i];
				Tree_nn_finder_type nnFinder(myTree);
				//constraint between cloud i and j.
				int j = enforce[i][nbr];

				//points in cloud j need to be mapped to the coordinate frame of i for the nn search
				//that is a[i]*a_inv[j]
				Eigen::Matrix4f j_to_i = new_alignments[i] * new_inv_alignments[j];
				const int clouds_j_samples_size = allQueryIndices[j].size();
				auto & qi = allQueryIndices[j];
				auto & points = clouds[j]->points;
//v3
//int ith = std::ceil(qi.size() / numPointsToUse + 0.1);
				int q_idx;
				Tree_query_type query;
				for (int q_ii = 0; q_ii < clouds_j_samples_size; q_ii++)
				{
					q_idx = qi[q_ii];
//v4
//if (!((q_idx + rand()) % ith == 0)){ continue; }
					//now: if p and q are not yet aligned.
					q = points[q_idx];//*qit;
					q.getVector4fMap() = j_to_i * q.getVector4fMap();
					//p = kdTree->NN(q);
					//query.vec << q.x, q.y, q.z, q.r, q.g, q.b;
					//query.x = q.x; query.y = q.y; query.z = q.z;
					query.set(q.x, q.y, q.z, q.r, q.g, q.b);
					//idx = nnFinder.findNN(query, culling_dist*culling_dist, 128).index;
					//idx = nnFinder.findNN(query, culling_dist*culling_dist,256).index;
					//using the correct NN is very important....
//					idx = nnFinder.findNN(query, culling_dist*culling_dist).index;
					idx = nnFinder.findNN(query, culling_dist*culling_dist).index;

					if (idx >= 0){
						p = clouds[i]->at(idx);
						q = clouds[j]->at(q_idx);
						//now p and q are found, but need to be mapped such that they can be used to compute an increment of the alignments
						//i.e. map to reference cloud and minus centroid.
						//note: sadly I get a 30% speedup by not using Eigen here. the commented out more readable eigen using commands are just written out.
						//p.getVector4fMap() = new_inv_alignments[i] * p.getVector4fMap() - centroid; p.getNormalVector3fMap() = new_inv_alignments[i].topLeftCorner<3, 3>() *p.getNormalVector3fMap();
						//q.getVector4fMap() = new_inv_alignments[j] * q.getVector4fMap() - centroid; q.getNormalVector3fMap() = new_inv_alignments[j].topLeftCorner<3, 3>() *q.getNormalVector3fMap();
						auto & a = new_inv_alignments[i];
						float bla, bla1, bla2;
						bla = a(0, 0)*p.x + a(0, 1)*p.y + a(0, 2)*p.z + a(0, 3) - centroid(0);
						bla1 = a(1, 0)*p.x + a(1, 1)*p.y + a(1, 2)*p.z + a(1, 3) - centroid(1);
						bla2 = a(2, 0)*p.x + a(2, 1)*p.y + a(2, 2)*p.z + a(2, 3) - centroid(2);
						p.x = bla; p.y = bla1; p.z = bla2;
						bla = a(0, 0)*p.normal_x + a(0, 1)*p.normal_y + a(0, 2)*p.normal_z;
						bla1 = a(1, 0)*p.normal_x + a(1, 1)*p.normal_y + a(1, 2)*p.normal_z;
						bla2 = a(2, 0)*p.normal_x + a(2, 1)*p.normal_y + a(2, 2)*p.normal_z;
						p.normal_x = bla; p.normal_y = bla1; p.normal_z = bla2;
						auto & b = new_inv_alignments[j];
						bla  = b(0, 0)*q.x + b(0, 1)*q.y + b(0, 2)*q.z + b(0, 3) - centroid(0);
						bla1 = b(1, 0)*q.x + b(1, 1)*q.y + b(1, 2)*q.z + b(1, 3) - centroid(1);
						bla2 = b(2, 0)*q.x + b(2, 1)*q.y + b(2, 2)*q.z + b(2, 3) - centroid(2);
						q.x = bla; q.y = bla1; q.z = bla2;
						bla  = b(0, 0)*q.normal_x + b(0, 1)*q.normal_y + b(0, 2)*q.normal_z;
						bla1 = b(1, 0)*q.normal_x + b(1, 1)*q.normal_y + b(1, 2)*q.normal_z;
						bla2 = b(2, 0)*q.normal_x + b(2, 1)*q.normal_y + b(2, 2)*q.normal_z;
						q.normal_x = bla; q.normal_y = bla1; q.normal_z = bla2;

						//weight/culling
						//w = p.getNormalVector3fMap().dot(q.getNormalVector3fMap());
						w = p.normal_x * q.normal_x + p.normal_y * q.normal_y + p.normal_z*q.normal_z;
						w = (w< 0.7071 ? 0 : w); w = (w * 0 == 0 ? w : 0.5);
						//w *= (1 - (q.getVector3fMap() - p.getVector3fMap()).norm() / culling_dist);	
						w *= (1 - sqrtf((q.x - p.x)*(q.x - p.x) + (q.y - p.y)*(q.y - p.y) + (q.z - p.z)*(q.z - p.z)) / culling_dist);
						w = w<0 ? 0 : w;

						//daring
						w *= std::max<float>(weights[i](idx, weight_idx), weights[j](q_idx, weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);
						//conservative:
						//w*= std::min<float>(weights[i](idx[0],weight_idx) , weights[j](q_idx,weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);

						//the point to plane equation
						//c_ << p.getVector3fMap().cross(q.getNormalVector3fMap()), q.getNormalVector3fMap();
						c_(0) = p.y*q.normal_z - p.z*q.normal_y; 
						c_(1) = p.z*q.normal_x - p.x*q.normal_z; 
						c_(2) = p.x*q.normal_y - p.y*q.normal_x;
						c_(3) = q.normal_x;
						c_(4) = q.normal_y;
						c_(5) = q.normal_z;

						//ccT_ += c_*c_.transpose()*w*alpha_point2plane;
						ccT_(0, 0) += c_(0)*c_(0)*w*alpha_point2plane; ccT_(0, 1) += c_(0)*c_(1)*w*alpha_point2plane; ccT_(0, 2) += c_(0)*c_(2)*w*alpha_point2plane; ccT_(0, 3) += c_(0)*c_(3)*w*alpha_point2plane; ccT_(0, 4) += c_(0)*c_(4)*w*alpha_point2plane; ccT_(0, 5) += c_(0)*c_(5)*w*alpha_point2plane;
						ccT_(1, 0) += c_(1)*c_(0)*w*alpha_point2plane; ccT_(1, 1) += c_(1)*c_(1)*w*alpha_point2plane; ccT_(1, 2) += c_(1)*c_(2)*w*alpha_point2plane; ccT_(1, 3) += c_(1)*c_(3)*w*alpha_point2plane; ccT_(1, 4) += c_(1)*c_(4)*w*alpha_point2plane; ccT_(1, 5) += c_(1)*c_(5)*w*alpha_point2plane;
						ccT_(2, 0) += c_(2)*c_(0)*w*alpha_point2plane; ccT_(2, 1) += c_(2)*c_(1)*w*alpha_point2plane; ccT_(2, 2) += c_(2)*c_(2)*w*alpha_point2plane; ccT_(2, 3) += c_(2)*c_(3)*w*alpha_point2plane; ccT_(2, 4) += c_(2)*c_(4)*w*alpha_point2plane; ccT_(2, 5) += c_(2)*c_(5)*w*alpha_point2plane;
						ccT_(3, 0) += c_(3)*c_(0)*w*alpha_point2plane; ccT_(3, 1) += c_(3)*c_(1)*w*alpha_point2plane; ccT_(3, 2) += c_(3)*c_(2)*w*alpha_point2plane; ccT_(3, 3) += c_(3)*c_(3)*w*alpha_point2plane; ccT_(3, 4) += c_(3)*c_(4)*w*alpha_point2plane; ccT_(3, 5) += c_(3)*c_(5)*w*alpha_point2plane;
						ccT_(4, 0) += c_(4)*c_(0)*w*alpha_point2plane; ccT_(4, 1) += c_(4)*c_(1)*w*alpha_point2plane; ccT_(4, 2) += c_(4)*c_(2)*w*alpha_point2plane; ccT_(4, 3) += c_(4)*c_(3)*w*alpha_point2plane; ccT_(4, 4) += c_(4)*c_(4)*w*alpha_point2plane; ccT_(4, 5) += c_(4)*c_(5)*w*alpha_point2plane;
						ccT_(5, 0) += c_(5)*c_(0)*w*alpha_point2plane; ccT_(5, 1) += c_(5)*c_(1)*w*alpha_point2plane; ccT_(5, 2) += c_(5)*c_(2)*w*alpha_point2plane; ccT_(5, 3) += c_(5)*c_(3)*w*alpha_point2plane; ccT_(5, 4) += c_(5)*c_(4)*w*alpha_point2plane; ccT_(5, 5) += c_(5)*c_(5)*w*alpha_point2plane;

						//b_ += c_*(p.getVector3fMap() - q.getVector3fMap()).dot(q.getNormalVector3fMap())*w*alpha_point2plane;
						bla = ((p.x - q.x)*q.normal_x + (p.y - q.y)*q.normal_y + (p.z - q.z)*q.normal_z);
						b_ += c_*bla*w*alpha_point2plane;
						//update current error
						//new_error_ += std::pow((p.getVector3fMap()-q.getVector3fMap()).dot(q.getNormalVector3fMap()),2)*w*alpha_point2plane;
						new_error_ += bla*bla*w*alpha_point2plane;


						//point to point...
						//b_p2p << q.getVector3fMap().cross(p.getVector3fMap() - q.getVector3fMap()), p.getVector3fMap() - q.getVector3fMap();
						bla = p.x - q.x; bla1 = p.y - q.y; bla2 = p.z - q.z;
						b_p2p(0) = q.y*bla2 - q.z*bla1;
						b_p2p(1) = q.z*bla - q.x*bla2;
						b_p2p(2) = q.x*bla1 - q.y*bla;
						b_p2p(3) = bla;
						b_p2p(4) = bla1;
						b_p2p(5) = bla2;
						b_ += b_p2p * w * alpha_point2point;// 

						/*ccT_p2p << q.y*q.y + q.z*q.z, -q.x*q.y, -q.x*q.z, 0, -q.z, q.y,
						-q.y*q.x, q.x*q.x + q.z*q.z, -q.y*q.z, q.z, 0, -q.x,
						-q.z*q.x, -q.z*q.y, q.x*q.x + q.y*q.y, -q.y, q.x, 0,
						0, q.z, -q.y, 1, 0, 0,
						-q.z, 0, q.x, 0, 1, 0,
						q.y, -q.x, 0, 0, 0, 1;
						ccT_ += ccT_p2p * w * alpha_point2point;*/

						ccT_(0, 0) += (q.y*q.y + q.z*q.z) * w * alpha_point2point; ccT_(0, 1) += -q.x*q.y * w * alpha_point2point;		ccT_(0, 2) += -q.x*q.z * w * alpha_point2point;				/*ccT_(0, 3) += 0 * w * alpha_point2point;*/	ccT_(0, 4) += -q.z * w * alpha_point2point; ccT_(0, 5) += q.y * w * alpha_point2point;
						ccT_(1, 0) += -q.y*q.x * w * alpha_point2point;		ccT_(1, 1) += (q.x*q.x + q.z*q.z) * w * alpha_point2point;	ccT_(1, 2) += -q.y*q.z * w * alpha_point2point;				ccT_(1, 3) += q.z * w * alpha_point2point;	/*	ccT_(1, 4) += 0 * w * alpha_point2point;*/	ccT_(1, 5) += -q.x * w * alpha_point2point;
						ccT_(2, 0) += -q.z*q.x * w * alpha_point2point;		ccT_(2, 1) += -q.z*q.y * w * alpha_point2point;				ccT_(2, 2) += (q.x*q.x + q.y*q.y) * w * alpha_point2point;	ccT_(2, 3) += -q.y * w * alpha_point2point;		ccT_(2, 4) += q.x * w * alpha_point2point;	/*ccT_(2, 5) += 0;*/
						/*ccT_(3, 0) += 0 * w * alpha_point2point;*/		ccT_(3, 1) += q.z * w * alpha_point2point;					ccT_(3, 2) += -q.y * w * alpha_point2point;					ccT_(3, 3) += 1 * w * alpha_point2point;		// 0,										0;
						ccT_(4, 0) += -q.z * w * alpha_point2point;			/*ccT_(4, 1) += 0; * w * alpha_point2point*/				ccT_(4, 2) += q.x * w * alpha_point2point;					/*  * w * alpha_point2point0,*/					ccT_(4, 4) += 1 * w * alpha_point2point; // 0;
						ccT_(5, 0) += q.y * w * alpha_point2point;			ccT_(5, 1) += -q.x * w * alpha_point2point;					/*ccT_(5, 0) += 0,											0 * w * alpha_point2point,						 0, * w * alpha_point2point*/				ccT_(5, 5) += 1 * w * alpha_point2point;

						
						//update current error
						//new_error_ += (p.getVector3fMap() - q.getVector3fMap()).squaredNorm()*w*alpha_point2point;
						new_error_ += (bla*bla + bla1*bla1 + bla2*bla2)*w*alpha_point2point;;

						total_weight_ += w;
						/*
						if (ccT_.squaredNorm() * 0 != 0 || b_.squaredNorm() * 0 != 0){
							std::cout << "Bundle Adjustment: NANs between: " << i << "," << j << "!\n";
							std::cout << p << "\n" << q << "\n";
							int a = 0;
							std::cin >> a;
						}*/
					}
				}
				if (ccT_.norm() * 0 != 0 || b_.norm() * 0 != 0){
					std::cout << "Bundle Adjustment: NANs between: " << i << "," << j << "!\n";
				}
				//cs[i].push_back(ccT_);
				//bs[i].push_back(b_);

#pragma omp critical 
{
//#pragma omp flush(totalNumPoints,new_error, total_weight)
	//std::cout << omp_get_thread_num() << ":"<< new_error << " + " << new_error_ << " => ";
					totalNumPoints = totalNumPoints + qi.size();
					new_error += new_error_;
					total_weight += total_weight_;
					cs[i][nbr] = ccT_;
					bs[i][nbr] = b_;

	//std::cout << new_error << "\n";
					//ccT += ccT_;
					//b += b_;
#pragma omp flush(totalNumPoints,new_error,total_weight)
}

			}//end loop over nbr constraints

		}//end omp for loop over constraint sets..
		}//end omp parallel (nn and pairwise matrix computation)
		buildMatrix.stop();


		//setup new matrix to solve for new alignment
		Eigen::MatrixXf total(6 * (clouds.size() - 1), 6 * (clouds.size() - 1));
		Eigen::VectorXf total_b(6 * (clouds.size() - 1));
		Eigen::VectorXf X(6 * (clouds.size() - 1));

		solve.restart();
		//At this point I have the errors of "new_alignments",stored in new_error-
		if (new_error > alignments_error && undamped_matrix_used_to_compute_new_alignments.rows() > 0){
			energyStillDecreases = false;
			//the new alignments are bad. the damping has to be increased, the new_alignments are dropped 
			//and a new alignment based on the last good alignment is computed.

			//reset alignments.
			new_alignments = alignments;
			new_inv_alignments = inv_alignments;
			lambda_damping *= 10.f;
			std::cout << "N\t" << lambda_damping << "\n";

			//no need to bother using the newly computed matrices. 
			//instead use the matrices used to compute the last increment, with the stronger damping factor
			total = undamped_matrix_used_to_compute_new_alignments;
			total_b = right_hand_side_used_to_compute_new_alignments;

		}
		else{//if (new_error < alignments_error){
			energyStillDecreases = true;
			lambda_damping *= 0.6f;
			std::cout << "Y\t" << lambda_damping << "\t" << alignments_error << "=>" << new_error << "\n";
			alignments = new_alignments;
			inv_alignments = new_inv_alignments;
			alignments_error = new_error;
			alignments_avg_error = new_error / total_weight;

			//as the matrices where computed as increment of the valid alignments stored in alignments: 
			//use icp system to compute new new_alignments
			//setup the linear system//
			
			//put it into separate method.
			//setting up the bundle adjustment matrix (see notes)

			total = Eigen::MatrixXf::Zero(6 * (clouds.size() - 1), 6 * (clouds.size() - 1));
			total_b.fill(0);

			int ind = 0;
			for (int i_cloud = 0; i_cloud < enforce.size(); i_cloud++)
			{
				for (int nbr = 0; nbr < enforce[i_cloud].size(); nbr++)
				{
					int j_cloud = enforce[i_cloud][nbr];
					int i = (i_cloud > referenceCloud ? i_cloud - 1 : i_cloud);//parameter block
					int j = (j_cloud > referenceCloud ? j_cloud - 1 : j_cloud);

					if (i_cloud == referenceCloud){
						total.block<6, 6>(j * 6, j * 6) += cs[i_cloud][nbr];//cs[ind];
						total_b.block<6, 1>(j * 6, 0) -= bs[i_cloud][nbr];//[ind];
					}
					if (j_cloud == referenceCloud){
						total.block<6, 6>(i * 6, i * 6) += cs[i_cloud][nbr];//[ind];
						total_b.block<6, 1>(i * 6, 0) += bs[i_cloud][nbr];//[ind];
					}
					else{
						total.block<6, 6>(i * 6, i * 6) += cs[i_cloud][nbr];//[ind];
						total.block<6, 6>(i * 6, j * 6) -= cs[i_cloud][nbr];//[ind];
						total.block<6, 6>(j * 6, i * 6) -= cs[i_cloud][nbr];//[ind];
						total.block<6, 6>(j * 6, j * 6) += cs[i_cloud][nbr];//[ind];

						total_b.block<6, 1>(i * 6, 0) += bs[i_cloud][nbr];//[ind];
						total_b.block<6, 1>(j * 6, 0) -= bs[i_cloud][nbr];//[ind];
					}

					ind++;
				}
			}

			undamped_matrix_used_to_compute_new_alignments = total;
			right_hand_side_used_to_compute_new_alignments = total_b;
		}

		//add damping to the system:
		total = total + lambda_damping * Eigen::MatrixXf::Identity(6 * (clouds.size() - 1), 6 * (clouds.size() - 1));

		//solving..//
		// The matrix is "often" positive semidefinite, in that provably it isn't always, but in practice I never experienced it not to be.
		//auto ldlt = (total).ldlt();
		auto ldlt = (total).llt(); //trying llt for speed...
		X = ldlt.solve(total_b);
		//resort to more robust, more expensive method if needed.X is left untouched if ldlt fails...
		float xnorm = X.norm();
		if (xnorm == 0 || ldlt.info() != Eigen::Success){
		//if (xnorm == 0 || !ldlt.isPositive()){
			std::cout << "\n Matrix not pos def!\n";
			X = total.fullPivLu().solve(total_b);
			xnorm = X.norm();
			std::cout << "X: " << X.norm() << " b " << total_b.norm() << " total.sum " << total.diagonal().sum() << "\n";
		}

		solve.stop();

		//std::cout << "Total Norm of lie-element vector: " << X.norm() << "\t(" << its << " " << ith << ")\n";
		//std::cout << "Total diff I extrapolated: " << total_diffI * ith << "\n";
		if (X.norm() > 10){
			std::cout << " **** Xmax: " << X.maxCoeff() << "!\n";
			X = total.fullPivLu().solve(total_b);
			std::cout << "LU solve:: " << X.norm() << "\n";
		}

		otherMath.restart();
		//!!		auto new_alginments = alignments, new_inv_alignments = a_inv;
		//extract all alignments.
		for (int i_cloud = 0; i_cloud < alignments.size(); i_cloud++){
			if (i_cloud != referenceCloud){
				int i = (i_cloud > referenceCloud ? i_cloud - 1 : i_cloud);
				Vector6f x_icp_total = -X.block<6, 1>(i * 6, 0);

				//map linearized transform back
				float cg = std::cos(x_icp_total(2)); float ca = std::cos(x_icp_total(0)); float cb = std::cos(x_icp_total(1));
				float sg = std::sin(x_icp_total(2)); float sa = std::sin(x_icp_total(0)); float sb = std::sin(x_icp_total(1));
				Eigen::Matrix4f  alignment;
				alignment << cg*cb, sg*cb, -sb, -x_icp_total(3),
					-sg*ca + cg*sb*sa, cg*ca + sg*sb*sa, cb*sa, -x_icp_total(4),
					sg*sa + cg*sb*ca, -cg*sa + sg*sb*ca, cb*ca, -x_icp_total(5),
					0, 0, 0, 1;

				alignment.topRightCorner<4, 1>() += centroid - alignment*centroid;

				new_alignments[i_cloud] = alignments[i_cloud] * alignment;
				new_inv_alignments[i_cloud] = new_alignments[i_cloud].inverse();
			}
		}

		x_max_coeff = X.maxCoeff();
		otherMath.stop();
		//}

	} while (++its < numIterations && x_max_coeff > 1e-4 || energyStillDecreases && its < 200);
	params.lambda = lambda_damping;
	params.last_error = alignments_error;//*/
	params.avg_error = alignments_avg_error;//*/

	total.stop();

	//std::cout << "Number of pairs & points registered...: " << numPairs << "\t" << totalNumPoints << "\n";
	std::cout << "Timings : " << total.total() << "\t" << buildTree.total() << "\t" << buildMatrix.total() << "\t" << solve.total() << "\t" << otherMath.total() << "\t" << total.total()/numPairs << "\n";

}
//assumes that there are no nans int the clouds.
//heuristic spped ups: decreasing maximal radius in nn search
//multiresolution through multiresolution via subsampling.
template<typename Eigen_T_weights>
void BundleAdjustment::pointToPlane_Bundle_step_LM(
	std::vector<explainMe::Cloud::Ptr > &clouds,
	std::vector<Eigen_T_weights> &weights,
	int weight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
	int referenceCloud, //acts as reference frame
	int numIterations,
	float alpha_point2plane,
	float alpha_point2point,
	float alpha_color,
	float culling_dist,
	RangeSensorDescription & d,
	LM_params & params)
{


	StopWatch buildTree, buildMatrix, solve, total, otherMath, compError;
	total.restart();

	Eigen::Vector4f centroid;
	centroid << CloudTools::compute3DCentroid(clouds[referenceCloud]), 0; //BAD: not of weighted points, as there need not be any in the reference cloud.

	otherMath.restart();
	//compute mapping of all clouds to the basecloud.
	std::vector<Eigen::Matrix4f> a_inv;
	for (int i = 0; i < clouds.size(); i++){
		//apply transf -1
		Eigen::Matrix4f a_inv_;
		a_inv_ = alignments[i].inverse();
		if (i == referenceCloud && !(a_inv_.trace() < 4.001 && a_inv_.trace() > 3.999)){
			std::cout << "in bundleadjustment, reference alignment should be identity!";
		}
		a_inv.push_back(a_inv_);
	}
	otherMath.stop();
	
	//temps 
	Vector6f c, b; Matrix6f ccT;
	explainMe::Point p, q;
	float w;
	//kdtree method parameters
	std::vector<int> idx;
	std::vector<float> dist_sqr;

	//build trees outside of the loop. (TODO push to algorithm, one time kdtree build)
	Vector6f scales; scales << 1, 1, 1, alpha_color, alpha_color, alpha_color;
	std::vector<mySimpleKdTree_ndim<6>> allTrees(clouds.size(), mySimpleKdTree_ndim<6>(scales));
	const int nClouds = clouds.size();

	buildTree.restart();
#pragma omp parallel
	{
#pragma omp for
		for (int i = 0; i <nClouds; i++)
		{
			simpleTreeTools::setData<explainMe::Cloud::Ptr>(allTrees[i], clouds[i]);
			allTrees[i].buildTree();
		}
	}
	buildTree.stop();
	float alignments_avg_error = params.avg_error;
	float last_error = params.last_error;
	if (params.last_error < 0){
		last_error = 1e10f;
	}
	std::cout << "Initial error: " << last_error << "\n";
	float lambda_damping = params.lambda;
	int ith;//= std::max<int>((int) std::floor((numIterations -1 - its) * 1.0/(numIterations-1) * 10+ 0.5), 1);

	float x_max_coeff = 0;
	float total_w;
	int its = 0;
	do{
		std::vector<Matrix6f> cs;
		std::vector<Vector6f> bs;
		float total_diffI = 0;
		total_w = 0;
		buildMatrix.restart();
		//1st compute the various 6x6 blocks of the ba matrix
		//The single blocks coincide with the std point to plane blocks.
		//compute all c's abd b's. This involves the association of cloud j to cloud i
		for (int i = 0; i < enforce.size(); i++)
		{
			auto & myTree = allTrees[i];
			ith = std::ceil(weights[i].col(weight_idx).sum() / 2500 + 0.1);// + std::max<int>((int) std::floor((numIterations -1 - its) * 1.0/(numIterations-1) * 10+ 0.5), 1);

			for (int nbr = 0; nbr < enforce[i].size(); nbr++)
			{
				//constraint between cloud i and j.
				int j = enforce[i][nbr];
				b = Vector6f::Zero();
				ccT = Matrix6f::Zero();
				

				//points in cloud j need to be mapped to the coordinate frame of i for the nn search
				//that is a[i]*a_inv[j]
				Eigen::Matrix4f j_to_i = alignments[i] * a_inv[j];
				const int clouds_j_size = clouds[j]->size();

#pragma omp parallel private(p,q,c,w,idx,dist_sqr) num_threads(20)//20
				{
					Matrix6f ccT_, ccT_p2p; Vector6f b_, b_p2p; ccT_.fill(0); b_.fill(0);
					float total_w_ = 0;
					simpleNNFinder_ndim<6> nnFinder(myTree);
#pragma omp for nowait
					for (int q_idx = 0; q_idx < clouds_j_size; q_idx++)
					{
						//ignore zero weight points:
						if (weights[j](q_idx, weight_idx) < 1e-3f){
							continue;
						}
						//"multi res"
						//if(q_idx % (2*(numIterations -its)-1) != 0){ //%(numIterations -its)
						//if(!(q_idx % (numIterations -its) == 0) && !((q_idx+1) % (numIterations -its) == 0)){
						if (!((q_idx + rand()) % ith == 0)){
							continue;
						}

						//now: if p and q are not yet aligned.
						q = clouds[j]->at(q_idx);//*qit;
						q.getVector4fMap() = j_to_i * q.getVector4fMap();
						//p = kdTree->NN(q);

						idx.resize(1, 1);
						idx[0] = nnFinder.findNN(simpleTreeTools::p(q.x, q.y, q.z, q.r, q.g, q.b), culling_dist*culling_dist/*/(2*its+1)*/).index;
						if (idx[0] >= 0){
							//valid
							//continue; //To test time consumption of lower part. (irrelevant)
							p = clouds[i]->at(idx[0]);

							//reset q
							q = clouds[j]->at(q_idx);//*qit;
							//now p and q are found, but need to be mapped such that they can be used to compute an increment of the alignments
							//i.e. map to reference cloud and minus centroid.
							p.getVector4fMap() = a_inv[i] * p.getVector4fMap() - centroid; p.getNormalVector3fMap() = a_inv[i].topLeftCorner<3, 3>() *p.getNormalVector3fMap();
							q.getVector4fMap() = a_inv[j] * q.getVector4fMap() - centroid; q.getNormalVector3fMap() = a_inv[j].topLeftCorner<3, 3>() *q.getNormalVector3fMap();


							//weight/culling
							w = p.getNormalVector3fMap().dot(q.getNormalVector3fMap()); w = (w< 0.7071 ? 0 : w); w = (w * 0 == 0 ? w : 0.5);
							w *= (1 - (q.getVector3fMap() - p.getVector3fMap()).norm() / culling_dist);	w = w<0 ? 0 : w;

							//daring
							w *= std::max<float>(weights[i](idx[0], weight_idx), weights[j](q_idx, weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);
							//conservative:
							//w*= std::min<float>(weights[i](idx[0],weight_idx) , weights[j](q_idx,weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);

							total_w_ += w;
							//w=1;
							//the point to plane equation
							c << p.getVector3fMap().cross(q.getNormalVector3fMap()), q.getNormalVector3fMap();
							ccT_ += c*c.transpose()*w*alpha_point2plane;
							b_ += c*(p.getVector3fMap() - q.getVector3fMap()).dot(q.getNormalVector3fMap())*w*alpha_point2plane;


							//point to point...
							b_p2p << q.getVector3fMap().cross(p.getVector3fMap() - q.getVector3fMap()), p.getVector3fMap() - q.getVector3fMap();
							ccT_p2p << q.y*q.y + q.z*q.z, -q.x*q.y, -q.x*q.z, 0, -q.z, q.y,
								-q.y*q.x, q.x*q.x + q.z*q.z, -q.y*q.z, q.z, 0, -q.x,
								-q.z*q.x, -q.z*q.y, q.x*q.x + q.y*q.y, -q.y, q.x, 0,
								0, q.z, -q.y, 1, 0, 0,
								-q.z, 0, q.x, 0, 1, 0,
								q.y, -q.x, 0, 0, 0, 1;

							ccT_ += ccT_p2p * w * alpha_point2point;
							b_ += b_p2p * w * alpha_point2point;//*/

							if (ccT_.norm() * 0 != 0 || b_.norm() * 0 != 0){
								std::cout << "Bundle Adjustment: NANs between: " << i << "," << j << "!\n";
								std::cout << p << "\n" << q << "\n";
									int a = 0;
								std::cin >> a;
							}
						}

					}

#pragma omp critical
					{
						ccT += ccT_;
						b += b_;
						total_w += total_w_;
					}
				}//end omp parallel (nn and pairwise matrix computation)

				if (ccT.norm() * 0 != 0 || b.norm() * 0 != 0){
					std::cout << "Bundle Adjustment: NANs between: " << i << "," << j << "!\n";
				}
				cs.push_back(ccT);
				bs.push_back(b);
			}
		}
		buildMatrix.stop();
		otherMath.restart();
		//setting up the bundle adjustment matrix (see notes)
		Eigen::MatrixXf total(6 * (clouds.size() - 1), 6 * (clouds.size() - 1));
		Eigen::VectorXf total_b(6 * (clouds.size() - 1));
		Eigen::VectorXf X(6 * (clouds.size() - 1));

		total = Eigen::MatrixXf::Zero(6 * (clouds.size() - 1), 6 * (clouds.size() - 1));
		total_b.fill(0);

		int ind = 0;
		for (int i_cloud = 0; i_cloud < enforce.size(); i_cloud++)
		{
			for (int nbr = 0; nbr < enforce[i_cloud].size(); nbr++)
			{
				int j_cloud = enforce[i_cloud][nbr];
				int i = (i_cloud > referenceCloud ? i_cloud - 1 : i_cloud);//parameter block
				int j = (j_cloud > referenceCloud ? j_cloud - 1 : j_cloud);

				if (i_cloud == referenceCloud){
					total.block<6, 6>(j * 6, j * 6) += cs[ind];
					total_b.block<6, 1>(j * 6, 0) -= bs[ind];
				}
				if (j_cloud == referenceCloud){
					total.block<6, 6>(i * 6, i * 6) += cs[ind];
					total_b.block<6, 1>(i * 6, 0) += bs[ind];
				}
				else{
					total.block<6, 6>(i * 6, i * 6) += cs[ind];
					total.block<6, 6>(i * 6, j * 6) -= cs[ind];
					total.block<6, 6>(j * 6, i * 6) -= cs[ind];
					total.block<6, 6>(j * 6, j * 6) += cs[ind];

					total_b.block<6, 1>(i * 6, 0) += bs[ind];
					total_b.block<6, 1>(j * 6, 0) -= bs[ind];
				}

				ind++;
			}
		}

		//damping:
		total = total + lambda_damping * Eigen::MatrixXf::Identity(6 * (clouds.size() - 1), 6 * (clouds.size() - 1));
		//total = total + Eigen::MatrixXf(total.diagonal().asDiagonal()) * lambda_damping;
		otherMath.stop();

		solve.restart();
		// The matrix is "often" positive semidefinite, in that provably it isn't always, but in practice I never experienced it not to be.
		auto ldlt = (total).ldlt();
		X = ldlt.solve(total_b);
		//resort to more robust, more expensive method if needed.X is left untouched if ldlt fails...
		float xnorm = X.norm();
		if (xnorm == 0 || !ldlt.isPositive()){
			std::cout << "\n Matrix not pos def!\n";
			X = total.fullPivLu().solve(total_b);
			xnorm = X.norm();
			std::cout << "X: " << X.norm() << " b " << total_b.norm() << " total.sum " << total.diagonal().sum() << "\n";
		}

		solve.stop();

		//std::cout << "Total Norm of lie-element vector: " << X.norm() << "\t(" << its << " " << ith << ")\n";
		//std::cout << "Total diff I extrapolated: " << total_diffI * ith << "\n";
		if (X.norm() > 10){
			std::cout << " **** Xmax: " << X.maxCoeff() << "!\n";
			X = total.fullPivLu().solve(total_b);
			std::cout << "LU solve:: " << X.norm() << "\n";
		}

		otherMath.restart();
		auto new_alginments = alignments, new_inv_alignments = a_inv;
		//extract all alignments.
		for (int i_cloud = 0; i_cloud < alignments.size(); i_cloud++){
			if (i_cloud != referenceCloud){
				int i = (i_cloud > referenceCloud ? i_cloud - 1 : i_cloud);
				Vector6f x_icp_total = -X.block<6, 1>(i * 6, 0);

				//map linearized transform back
				float cg = std::cos(x_icp_total(2)); float ca = std::cos(x_icp_total(0)); float cb = std::cos(x_icp_total(1));
				float sg = std::sin(x_icp_total(2)); float sa = std::sin(x_icp_total(0)); float sb = std::sin(x_icp_total(1));
				Eigen::Matrix4f  alignment;
				alignment << cg*cb, sg*cb, -sb, -x_icp_total(3),
					-sg*ca + cg*sb*sa, cg*ca + sg*sb*sa, cb*sa, -x_icp_total(4),
					sg*sa + cg*sb*ca, -cg*sa + sg*sb*ca, cb*ca, -x_icp_total(5),
					0, 0, 0, 1;

				alignment.topRightCorner<4, 1>() += centroid - alignment*centroid;

				new_alginments[i_cloud] = alignments[i_cloud] * alignment;
				new_inv_alignments[i_cloud] = new_alginments[i_cloud].inverse();
			}
		}

		x_max_coeff = X.maxCoeff();
		otherMath.stop();

		compError.restart();
		
		//decide on damping etc:
		float new_error = totalError<Eigen_T_weights>(
			clouds,
			weights, weight_idx,
			new_alginments, new_inv_alignments, enforce,
			referenceCloud,
			alpha_point2plane, alpha_point2point, culling_dist, allTrees);
		compError.stop();

		std::cout << "New, last, upd.: " << new_error << " \t" << last_error << " \t";
		if (new_error < last_error && x_max_coeff< 0.5){ //larger and the linearization is to unrelieable.
			last_error = new_error;
			alignments_avg_error = new_error / total_w;
			lambda_damping *= 0.6f;
			//lambda_damping *= 0.9f;
			//use new alignments
			alignments = new_alginments;
			a_inv = new_inv_alignments;
			std::cout << last_error;
		}
		else{
			lambda_damping *= 10;
			//	lambda_damping *= 1.5;
			//discard new alignments
			std::cout << "N ";
		}
		std::cout << "\tL: " << lambda_damping << "/" << "X: " << x_max_coeff << "\n";

	} while (++its < numIterations && x_max_coeff > 1e-4);
	total.stop();

	params.lambda = lambda_damping;
	params.last_error = last_error;
	params.avg_error = alignments_avg_error;

	std::cout << "Timings : " << total.total() << "\t" << buildTree.total() << "\t" << buildMatrix.total() << "\t" << solve.total() <<"\t" << otherMath.total() <<"\t"<< compError.total()<<"\n";

}

//assumes that there are no nans int the clouds.
//heuristic spped ups: decreasing maximal radius in nn search
//multiresolution through multiresolution via subsampling.
void BundleAdjustment::pointToPlane_Bundle_step_of(
	explainMe::Cloud::Ptr cloud1,explainMe::Cloud::Ptr cloud2,
	Eigen::MatrixXi &I1,Eigen::MatrixXi &I2,
	Eigen::MatrixXi &dI1_dx, Eigen::MatrixXi &dI1_dy,
	Eigen::MatrixXi &dI2_dx, Eigen::MatrixXi &dI2_dy,
	int pyr_level,
	Eigen::Matrix4f &alignment_1_to2,
	float alpha_point2plane,
	float alpha_point2point,
	float alpha_of,
	float alpha_color,
	float culling_dist,
	RangeSensorDescription & d,
	std::vector<Eigen::Matrix<float,2,6>> & debug_info)
{
	debug_info.clear();
		
	Eigen::Vector4f centroid;
	centroid <<	CloudTools::compute3DCentroid(cloud1), 0; //BAD: not of weighted points, as there need not be any in the reference cloud.
	
	//for on the fly mapping- clouds are never transformed.
	//compute mapping of all clouds to the basecloud.
	Eigen::Matrix4f a_inv;
	a_inv = alignment_1_to2.inverse();
	

	std::cout << "computing bs and cs...";

	//temps (for readability)
	Vector6f c,b, b_p2p; Matrix6f ccT, ccT_p2p;
	explainMe::Point p,q;
	float w;
	//kdtree method parameters
	std::vector<int> idx;
	std::vector<float> dist_sqr;


	//build trees outside of the loop. (TODO push to algorithm, one time kdtree build)
	Vector6f scales; scales << 1,1,1,alpha_color,alpha_color,alpha_color;
	mySimpleKdTree_ndim<6> tree1(scales) ;mySimpleKdTree_ndim<6> tree2(scales) ;
	
	simpleTreeTools::setData<explainMe::Cloud::Ptr>(tree1, cloud1);
	tree1.buildTree();
	simpleTreeTools::setData<explainMe::Cloud::Ptr>(tree2, cloud2);
	tree2.buildTree();
	
	
	
	//for(int its = 0; its < 1; its++){
		int ith = 5; //will keep each ith point only for registration
		//1 for its = numiteration -1. ith maximal = 10. (its = 0)
				
			float total_diffI = 0;	
						
			auto & myTree = tree1;
			b = Vector6f::Zero();
			ccT = Matrix6f::Zero();
			float total_w = 0;

					//points in cloud j need to be mapped to the coordinate frame of i for the nn search
					Eigen::Matrix4f j_to_i = a_inv;
					//for(auto qit = clouds[j]->begin(); qit!= clouds[j]->end(); qit++)

					const int clouds_j_size = cloud2->size();
			
					Eigen::Matrix<float, 3,3> K; K.fill(0);
			
					//K << d.intrinsicK()
					auto K_  = d.intrinsicK();
					K(0,0) = K_(0,0);K(0,1) = K_(0,1);K(0,2) = K_(0,2);
					K(1,0) = K_(1,0);K(1,1) = K_(1,1);K(1,2) = K_(1,2);
					K(2,0) = K_(2,0);K(2,1) = K_(2,1);K(2,2) = K_(2,2);

					
					Eigen::Vector3f Rx_plus_T;
					Eigen::Matrix<float, 2,3> dg_dxy;
					Eigen::Matrix<float, 3,6> DRT_dalphat;
					simpleNNFinder_ndim<6> nnFinder(myTree);
					for(int q_idx = 0; q_idx < clouds_j_size; q_idx ++)
					{
						debug_info.push_back(Eigen::Matrix<float,2,6>());
						debug_info.back().fill(0);
												
						//"multi res"
						//if(q_idx % (2*(numIterations -its)-1) != 0){ //%(numIterations -its)
						//if(!(q_idx % (numIterations -its) == 0) && !((q_idx+1) % (numIterations -its) == 0)){
						if(!(q_idx % ith == 0)){
							continue;
						}

						//now: if p and q are not yet aligned.
						q = cloud2->at(q_idx);//*qit;
						q.getVector4fMap() = j_to_i * q.getVector4fMap();
						//p = kdTree->NN(q);

						idx.resize(1,1);
						idx[0] = nnFinder.findNN(simpleTreeTools::p(q.x,q.y,q.z, q.r, q.g,q.b), culling_dist*culling_dist/*/(2*its+1)*/).index;
						if(idx[0] < 0){
							//invalid
							continue;
						}
						//continue; //To test time consumption of lower part. (irrelevant)
						p = cloud1->at(idx[0]);

						//reset q
						q = cloud2->at(q_idx);//*qit;
						//now p and q are found, but need to be mapped such that they can be used to compute an increment of the alignments
						//i.e. map to reference cloud and minus centroid.
						p.getVector4fMap() = p.getVector4fMap() - centroid; p.getNormalVector3fMap() = p.getNormalVector3fMap();
						q.getVector4fMap() = a_inv*q.getVector4fMap() - centroid; q.getNormalVector3fMap() = a_inv.topLeftCorner<3,3>() *q.getNormalVector3fMap();


						//weight/culling
						w= p.getNormalVector3fMap().dot(q.getNormalVector3fMap()); w = (w< 0.7071? 0:w); w= (w*0==0?w:0.5); 
						w*= (1-(q.getVector3fMap() -p.getVector3fMap()).norm()/culling_dist);	w=w<0?0:w;
						total_w += w;
					
						//w=1;
						//the point to plane equation
						c << p.getVector3fMap().cross(q.getNormalVector3fMap()), q.getNormalVector3fMap();
						ccT += c*c.transpose()*w*alpha_point2plane;
						b += c*(p.getVector3fMap()-q.getVector3fMap()).dot(q.getNormalVector3fMap())*w*alpha_point2plane;


						//point to point...
						b_p2p << q.getVector3fMap().cross(p.getVector3fMap()- q.getVector3fMap()), p.getVector3fMap()- q.getVector3fMap();
						ccT_p2p <<	q.y*q.y + q.z*q.z,	-q.x*q.y,	-q.x*q.z,	0,		-q.z,	q.y,
							-q.y*q.x,	q.x*q.x + q.z*q.z,	-q.y*q.z,	q.z,	0,		-q.x,
							-q.z*q.x,	-q.z*q.y,	q.x*q.x + q.y*q.y,	-q.y,	q.x,	0,
							0,			q.z,		-q.y,				1,		0,		0,	
							-q.z,		0,			q.x,				0,		1,		0,
							q.y,		-q.x,		0,					0,		0,		1;

						ccT += ccT_p2p * w * alpha_point2point;
						b+= b_p2p * w * alpha_point2point;//*/

						//optical flow x
						//Add this only if I_x and IA_x are valid-
						//Probable mistakes: sign of y in projection. That is in K and more importantly dI_y.
						//sign of alpha beta gamma.
						//K

						//reset p,q
						p = cloud1->at(idx[0]);
						q = cloud2->at(q_idx);

						Eigen::RowVector2f dI_x_y ; //= the dI at j_to_i*q in p's frame
						float I_x, I_Ax; //the I at q and I at j_to_i*q in p's frame
						float coord_x_q, coord_y_q, coord_x_Aq, coord_y_Aq;
						d.project(q.x,q.y,q.z,coord_x_q, coord_y_q);
						Rx_plus_T = (j_to_i * q.getVector4fMap()).topLeftCorner<3,1>();
						d.project(Rx_plus_T.x(),Rx_plus_T.y(),Rx_plus_T.z(),coord_x_Aq, coord_y_Aq);
						
						if(coord_x_q <0 || coord_y_q < 0 ||
								coord_x_q > d.width() || coord_y_q > d.height()){

									std::cout <<"Erroneous coords! " << coord_x_q << " " << coord_y_q 
										<< " vs " << I1.rows() << " " <<I1.cols() 
										<< " vs " << d.width() << " " <<d.height() <<"\n";
						}
						//not inside frustum?
						if(coord_x_Aq <0.01 || coord_y_Aq < 0.01 ||coord_x_q<0.01 || coord_y_q < 0.01 ||
							coord_x_Aq > d.width()-1.01 || coord_y_Aq > d.height() -1.01||coord_x_q> d.width() -1.01|| coord_y_q > d.height() -1.01)
						{
							//std::cout << ":";
							continue;
						}

						//adapt to pyramid level:
						coord_x_q = coord_x_q / pow(2,pyr_level);
						coord_y_q = coord_y_q/ pow(2,pyr_level);
						coord_x_Aq =coord_x_Aq/ pow(2,pyr_level);
						coord_y_Aq =coord_y_Aq/ pow(2,pyr_level);


						I_x =interpolate(I2,coord_x_q, coord_y_q );
						I_Ax = interpolate(I1,coord_x_Aq, coord_y_Aq );;
						dI_x_y << interpolate(dI1_dx,coord_x_Aq, coord_y_Aq ),
							interpolate(dI1_dy,coord_x_Aq, coord_y_Aq );
						dI_x_y /= pow(2,pyr_level); //as (f(x) - f(x+h)) / h and h dependent on pyr level.

						dI_x_y *= 0.25;//0.125; //normalizing the sobel filter....
						
						//std::cout << dI_x_y << " i, i_ax " << I_x << " " << I_Ax <<"\n";
						
						Rx_plus_T = K * (j_to_i * q.getVector4fMap()).topLeftCorner<3,1>();//
						dg_dxy<< 1/ Rx_plus_T.z() ,0, -Rx_plus_T.x()/Rx_plus_T.z()/Rx_plus_T.z(),
							0, 1/ Rx_plus_T.z() , -Rx_plus_T.y()/Rx_plus_T.z()/Rx_plus_T.z();
					
						//invalid?
						if(dg_dxy(0,0)*0 !=0){
							std::cout << "bang!";
							continue;
						}
						else{
							//std::cout << "o";
						}

						//q.getVector4fMap() = a_inv[j]*q.getVector4fMap();
						q.getVector4fMap() = a_inv*q.getVector4fMap() - centroid;
						/*DRT_dalphat		<<	 0,		q.z,	-q.y,	-1,	0,	0,
											-q.z,	0,		q.x,	0,	-1,	0,
											q.y,	-q.x,	0,		0,	0,	-1;/*/
						DRT_dalphat		<<	 0,		-q.z,	q.y,	-1,	0,	0,
											q.z,	0,		-q.x, 0,	-1,	0,
											-q.y,	q.x,	0,		0,	0,	-1;//*/ //in what direction q gets rotated around the centroid: makes a lot of sense!

						DRT_dalphat = /*alignments[i].topLeftCorner<3,3>() **/ DRT_dalphat;


						debug_info.back() = DRT_dalphat.topLeftCorner<2,6>()*300;// dg_dxy * K * DRT_dalphat;
						//debug_info.back().setZero(); debug_info.back()(0,0) = 30;debug_info.back()(1,1) = 30;
						
						
						//the equation is c'*params = -E, that is in least squates sense cc'*params = -Ec, which is gauss newton.
						//b-= alpha_of * pow<float>((I_x - I_Ax),2)  *c;
						//c = 2*(I_x - I_Ax) * dI_x_y * dg_dxy * K * DRT_dalphat;
						c = 2 * dI_x_y * dg_dxy * K * DRT_dalphat;
						b-= alpha_of * (I_x - I_Ax)  *c;
						ccT += alpha_of*c*c.transpose();

						total_diffI+= std::pow<float>((I_x - I_Ax),2);
					}
					
			
			
			
			//setting up the bundle adjustment matrix (see notes)
			
			
			// The matrix is "often" positive semidefinite, in that provably it isn't always, but in practice I never experienced it not to be.
			auto ldlt = (ccT).ldlt();
			Vector6f X = ldlt.solve(b);
	//		std::cout << it <<  "X: " << X.norm() << " b " << total_b.norm() << " total.sum " <<  total.diagonal().sum() << "\n";
		
			//resort to more robust, more expensive method if needed.X is left untouched if ldlt fails...
			float xnorm =X.norm();
			if(xnorm == 0 || ! ldlt.isPositive()){
				std::cout << "\n Matrix not pos def!\n";
				X = ccT.fullPivLu().solve(b);
				xnorm = X.norm();
			}
			std::cout <<  "\nX" << X.transpose() << "\n";
		
			
		
			std::cout << "X Norm: " << X.norm() << "\t("  <<ith << ")\t\t";
			std::cout << "diffI: " << total_diffI << "\n";
			if(X.norm() > 10){
				std::cout << " **** Xmax: " << X.maxCoeff() <<  "!\n";
				X = ccT.fullPivLu().solve(b);
				std::cout << "LU solve:: " << X.norm() << "\n";
			
			}
		

			//extract alignment.
			Vector6f x_icp_total = X; 
				
			//map linearized transform back
			float cg = std::cos(x_icp_total(2)); float ca = std::cos(x_icp_total(0)); float cb = std::cos(x_icp_total(1));
			float sg = std::sin(x_icp_total(2)); float sa = std::sin(x_icp_total(0)); float sb = std::sin(x_icp_total(1));
			Eigen::Matrix4f  alignment_;
			alignment_ << cg*cb,			sg*cb,				-sb,	-x_icp_total(3),
				-sg*ca + cg*sb*sa,	cg*ca + sg*sb*sa,	cb*sa,	-x_icp_total(4),
				sg*sa + cg*sb*ca,	-cg*sa + sg*sb*ca,	cb*ca,	-x_icp_total(5),
				0,			0,		0,					1;

			alignment_.topRightCorner<4,1>() +=  centroid - alignment_*centroid;

			alignment_1_to2= alignment_1_to2*alignment_;
			//a_inv[i_cloud] = alignments[i_cloud].inverse();

			
	
}



//assumes that there are no nans int the clouds.
//heuristic spped ups: decreasing maximal radius in nn search
//multiresolution through multiresolution via subsampling.
template<typename Eigen_T_weights>
void BundleAdjustment::pointToPlane_Bundle_step_color(
	std::vector<explainMe::Cloud::Ptr > &clouds,
	std::vector<Eigen_T_weights> &weights,
	int weight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
	int referenceCloud, //acts as reference frame
	int numIterations,
	float alpha_point2plane,
	float alpha_point2point,
	float alpha_color,
	float culling_dist)
{
	StopWatch buildTree, buildMatrix, solve,total;
	total.restart();
	Eigen::Vector4f centroid;
	centroid <<	CloudTools::compute3DCentroid(clouds[referenceCloud]), 0; //BAD: not of weighted points, as there need not be any in the reference cloud.


	//for on the fly mapping- clouds are never transformed.
	//compute mapping of all clouds to the basecloud.
	std::vector<Eigen::Matrix4f> a_inv;
	for(int i = 0; i < clouds.size(); i++){
		//apply transf -1
		Eigen::Matrix4f a_inv_;
		a_inv_ = alignments[i].inverse();
		if(i == referenceCloud && !(a_inv_.trace() < 4.001 && a_inv_.trace() > 3.999)){
			std::cout << "in bundleadjustment, reference alignment should be identity!";
		}
		a_inv.push_back(a_inv_);
	}

	std::cout << "computing bs and cs...";

	//temps (for readability)
	Vector6f c,b; Matrix6f ccT;
	explainMe::Point p,q;
	float w;
	//kdtree method parameters
	std::vector<int> idx;
	std::vector<float> dist_sqr;


	//build trees outside of the loop. (TODO push to algorithm, one time kdtree build)
	Vector6f scales; scales << 1,1,1,alpha_color,alpha_color,alpha_color;
	std::vector<mySimpleKdTree_ndim<6>> allTrees(clouds.size(), mySimpleKdTree_ndim<6>(scales));
	const int nClouds = clouds.size();

	buildTree.restart();
#pragma omp parallel
	{
#pragma omp for
		for(int i = 0; i <nClouds; i++)
		{
			simpleTreeTools::setData<explainMe::Cloud::Ptr>(allTrees[i], clouds[i]);
			allTrees[i].buildTree();
		}
	}
	buildTree.stop();
	
	int maxRepeats = 100;
	
	for(int its = 0; its < numIterations; its++){
		int ith = numIterations -its; //will keep each ith point only for registration
		//1 for its = numiteration -1. ith maximal = 10. (its = 0)
		ith = std::max<int>((int) std::floor((numIterations -1 - its) * 1.0/(numIterations-1) * 10+ 0.5), 1) + 35;
		
		//Enforce a minimal convergence. (1e-2) Typically this is achieved in one iteration.
		float x_max_coeff = 0;
		do{
			std::vector<Matrix6f> cs;
			std::vector<Vector6f> bs;
		
			buildMatrix.restart();		
			//1st compute the various 6x6 blocks of the ba matrix
			//The single blocks coincide with the std point to plane blocks.
			//compute all c's abd b's. This involves the association of cloud j to cloud i
			for(int i = 0; i < enforce.size(); i++)
			{
				//myKdtree
				//mySimpleKdTree myTree;
				//myTree.setData<explainMe::Cloud::Ptr>(clouds[i]);
				//myTree.buildTree();
				auto & myTree = allTrees[i];
			
				for(int nbr = 0; nbr < enforce[i].size(); nbr++)
				{
					int j = enforce[i][nbr];

					b = Vector6f::Zero();
					ccT = Matrix6f::Zero();
					float total_w = 0;

					//points in cloud j need to be mapped to the coordinate frame of i for the nn search
					//that is a[i]*a_inv[j]
					Eigen::Matrix4f j_to_i = alignments[i]*a_inv[j];
					//for(auto qit = clouds[j]->begin(); qit!= clouds[j]->end(); qit++)

					const int clouds_j_size = clouds[j]->size();
					//int q_idx;

	#pragma omp parallel private(p,q,c,w,idx,dist_sqr) num_threads(20)
	{
	Matrix6f ccT_,ccT_p2p;Vector6f b_, b_p2p; ccT_.fill(0); b_.fill(0);
	float total_w_ = 0;
	simpleNNFinder_ndim<6> nnFinder(myTree);
	#pragma omp for nowait
					for(int q_idx = 0; q_idx < clouds_j_size; q_idx ++)
					{
						//ignore zero weight points:
						if(weights[j](q_idx,weight_idx) < 1e-3f){
							continue;
						}
						//"multi res"
						//if(q_idx % (2*(numIterations -its)-1) != 0){ //%(numIterations -its)
						//if(!(q_idx % (numIterations -its) == 0) && !((q_idx+1) % (numIterations -its) == 0)){
						if(!(q_idx % ith == 0)){
							continue;
						}

						//now: if p and q are not yet aligned.
						q = clouds[j]->at(q_idx);//*qit;
						q.getVector4fMap() = j_to_i * q.getVector4fMap();
						//p = kdTree->NN(q);

						idx.resize(1,1);
						idx[0] = nnFinder.findNN(simpleTreeTools::p(q.x,q.y,q.z, q.r, q.g,q.b), culling_dist*culling_dist/*/(2*its+1)*/).index;
						if(idx[0] < 0){
							//invalid
							continue;
						}
						//continue; //To test time consumption of lower part. (irrelevant)
						p = clouds[i]->at(idx[0]);

						//reset q
						q = clouds[j]->at(q_idx);//*qit;
						//now p and q are found, but need to be mapped such that they can be used to compute an increment of the alignments
						//i.e. map to reference cloud and minus centroid.
						p.getVector4fMap() = a_inv[i]*p.getVector4fMap() - centroid; p.getNormalVector3fMap() = a_inv[i].topLeftCorner<3,3>() *p.getNormalVector3fMap();
						q.getVector4fMap() = a_inv[j]*q.getVector4fMap() - centroid; q.getNormalVector3fMap() = a_inv[j].topLeftCorner<3,3>() *q.getNormalVector3fMap();


						//weight/culling
						w= p.getNormalVector3fMap().dot(q.getNormalVector3fMap()); w = (w< 0.7071? 0:w); w= (w*0==0?w:0.5); 
						w*= (1-(q.getVector3fMap() -p.getVector3fMap()).norm()/culling_dist);	w=w<0?0:w;
					
						//w*= pow(0.8

						//daring
						w*= std::max<float>(weights[i](idx[0],weight_idx) , weights[j](q_idx,weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);
						//conservative:
						//w*= std::min<float>(weights[i](idx[0],weight_idx) , weights[j](q_idx,weight_idx));// weights[i](idx[0],weight_idx) * weights[j](q_idx,weight_idx);
						
						total_w_ += w;
						//w=1;
						//the point to plane equation
						c << p.getVector3fMap().cross(q.getNormalVector3fMap()), q.getNormalVector3fMap();
						ccT_ += c*c.transpose()*w*alpha_point2plane;
						b_ += c*(p.getVector3fMap()-q.getVector3fMap()).dot(q.getNormalVector3fMap())*w*alpha_point2plane;


						//point to point...
						//make it l1
						w *= 1/((p.getVector3fMap()-q.getVector3fMap()).norm() + 0.001);

						b_p2p << q.getVector3fMap().cross(p.getVector3fMap()- q.getVector3fMap()), p.getVector3fMap()- q.getVector3fMap(); 
						int s = 1;
						ccT_p2p <<	(q.y*q.y + q.z*q.z),	-q.x*q.y,	-q.x*q.z,		0,		s*-q.z,	s*q.y,
							-q.y*q.x,	(q.x*q.x + q.z*q.z),	-q.y*q.z,				s*q.z,	0,		s*-q.x,
							-q.z*q.x,	-q.z*q.y,	(q.x*q.x + q.y*q.y),				s*-q.y,	s*q.x,	0,
							0,			s*q.z,		s*-q.y,				1,		0,		0,	
							s*-q.z,		0,			s*q.x,				0,		1,		0,
							s*q.y,		s*-q.x,		0,					0,		0,		1;

						ccT_ += /*0.1 **/ ccT_p2p * w * alpha_point2point;
						b_+= /*0.1 **/b_p2p * w * alpha_point2point;//*/
					}

					#pragma omp critical
					{
						ccT += ccT_;
						b += b_;
						total_w += total_w_;
					}
	}//end omp parallel (nn and pairwise matrix computation)

					//dampen only for neighbors.
					if(std::abs<int>(enforce[i][nbr] - i) == 1)
						ccT += Matrix6f::Identity()*(total_w/100 * (numIterations - its)*1.0/numIterations  +1);//Hacked in large damping!!!
					//ccT += Matrix6f::Identity() *1.f/(1 +total_w/100) * std::max(ccT.maxCoeff(),1.f);//1e-1;//1e-4 1e-total_weight.
					//std::cout  << total_w << "\t" ;
					cs.push_back(ccT);
					bs.push_back(b);
					//std::cout << "\n" << ccT << "\n\n"<< b << "\n";
				}
			}
			buildMatrix.stop();

			//setting up the bundle adjustment matrix (see notes)
			Eigen::MatrixXf total(6*(clouds.size()-1), 6*(clouds.size()-1));
			Eigen::VectorXf total_b(6*(clouds.size()-1));
			Eigen::VectorXf X(6*(clouds.size()-1));

			total = Eigen::MatrixXf::Zero(6*(clouds.size()-1), 6*(clouds.size()-1));
			total_b.fill(0);

			int ind =0;
			for(int i_cloud = 0; i_cloud < enforce.size(); i_cloud++)
			{
				for(int nbr = 0; nbr < enforce[i_cloud].size(); nbr++)
				{
					int j_cloud = enforce[i_cloud][nbr]; 
					int i = (i_cloud > referenceCloud ? i_cloud-1: i_cloud);//parameter block
					int j = (j_cloud > referenceCloud ? j_cloud-1: j_cloud);

					if(i_cloud==referenceCloud){
						total.block<6,6>(j*6,j*6) += cs[ind];
						total_b.block<6,1>(j*6,0) -= bs[ind];
					}
					if(j_cloud == referenceCloud){
						total.block<6,6>(i*6,i*6) += cs[ind];
						total_b.block<6,1>(i*6,0) += bs[ind];
					}
					else{
						total.block<6,6>(i*6,i*6) += cs[ind];
						total.block<6,6>(i*6,j*6) -= cs[ind];
						total.block<6,6>(j*6,i*6) -= cs[ind];
						total.block<6,6>(j*6,j*6) += cs[ind];
					
						total_b.block<6,1>(i*6,0) += bs[ind];
						total_b.block<6,1>(j*6,0) -= bs[ind];
					}

					ind ++;
				}
			}

			solve.restart();
			//following solve does not work, i do not know why. thought QR should be the right thing!
			//X = total.fullPivHouseholderQr().solve(total_b);
	
			//works, cholesky on normal eq.
			//X = (total.transpose()*total).ldlt().solve(total.transpose() *total_b);
			//svd based solve:
			//X =total.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(total_b);

			// The matrix is "often" positive semidefinite, in that provably it isn't always, but in practice I never experienced it not to be.
			auto ldlt = (total).ldlt();
			X = ldlt.solve(total_b);
	//		std::cout << it <<  "X: " << X.norm() << " b " << total_b.norm() << " total.sum " <<  total.diagonal().sum() << "\n";
		
			//resort to more robust, more expensive method if needed.X is left untouched if ldlt fails...
			float xnorm =X.norm();
			if(xnorm == 0 || ! ldlt.isPositive()){
				std::cout << "\n Matrix not pos def!\n";
				X = total.fullPivLu().solve(total_b);
				xnorm = X.norm();
				std::cout << "X: " << X.norm() << " b " << total_b.norm() << " total.sum " <<   total.diagonal().sum() << "\n";
			}
		
			solve.stop();
		
			std::cout << "Total Norm of lie-element vector: " << X.norm() << "\t(" << its << " " <<ith << ")\n";
			if(X.norm() > 10){
				std::cout << " **** Xmax: " << X.maxCoeff() <<  "!\n";
				X = total.fullPivLu().solve(total_b);
				std::cout << "LU solve:: " << X.norm() << "\n";
				//X =total.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(total_b);
				//std::cout << "SVD solve:: " << X.norm() << "\n";better but so slow...
			}
		

			//extract all alignments.
			for(int i_cloud = 0; i_cloud < alignments.size(); i_cloud++){
				if(i_cloud != referenceCloud){
					int i = (i_cloud > referenceCloud ? i_cloud -1 :i_cloud);
					Vector6f x_icp_total = -X.block<6,1>(i*6,0); 
				
					//map linearized transform back
					float cg = std::cos(x_icp_total(2)); float ca = std::cos(x_icp_total(0)); float cb = std::cos(x_icp_total(1));
					float sg = std::sin(x_icp_total(2)); float sa = std::sin(x_icp_total(0)); float sb = std::sin(x_icp_total(1));
					Eigen::Matrix4f  alignment;
					alignment << cg*cb,			sg*cb,				-sb,	-x_icp_total(3),
						-sg*ca + cg*sb*sa,	cg*ca + sg*sb*sa,	cb*sa,	-x_icp_total(4),
						sg*sa + cg*sb*ca,	-cg*sa + sg*sb*ca,	cb*ca,	-x_icp_total(5),
						0,			0,		0,					1;

					alignment.topRightCorner<4,1>() +=  centroid - alignment*centroid;

					alignments[i_cloud] = alignments[i_cloud]*alignment;
					a_inv[i_cloud] = alignments[i_cloud].inverse();
				}
			}


			if(X.maxCoeff() < 1e-4){
				break;
			}
			///BAD BAD BAD hack.
			if(X.maxCoeff() >1e-2){
				std::cout << "repeat.(" <<X.maxCoeff() << ")";
			}
			//Ideas: increase damping between problematic thingies...
			x_max_coeff = X.maxCoeff();
		}while(x_max_coeff > 1e-2 && --maxRepeats>0);
	}//end multiple iterations of ba.
	total.stop();

	std::cout << "buildTree: " << buildTree.total() << "\nbuilMatrix " << buildMatrix.total() << "\nsolve" <<solve.total() << "\n";
	std::cout << "Total: " << total.total() << "\n";
	std::cout << "Unaccounted: " << total.total() - buildTree.total() -buildMatrix.total() -solve.total() << "\n";
}

/*
template
void BundleAdjustment::pointToPlane_Bundle_step_r<Eigen::Vector3f>(
	std::vector<explainMe::Cloud::Ptr > &clouds,
	std::vector<std::vector<Eigen::Vector3f >> &weights,
	int wight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
	int referenceCloud, //acts as reference frame
	int numIterations);
*/

//#include "numberOfHypotheses.h"

//typedef Eigen::Matrix<float,NUM_HYPO,1>  Vectornf;

template
void BundleAdjustment::pointToPlane_Bundle_step_r<Eigen::Matrix<float,Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(
	std::vector<explainMe::Cloud::Ptr > &clouds,
	std::vector<Eigen::Matrix<float,Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> &weights,
	int wight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
	int referenceCloud, //acts as reference frame
	int numIterations, float alpha_point2plane, float alpha_point2point, float cull_dist);

template
void BundleAdjustment::pointToPlane_Bundle_step_color<Eigen::Matrix<float,Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(
	std::vector<explainMe::Cloud::Ptr > &clouds,
	std::vector<Eigen::Matrix<float,Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> &weights,
	int wight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
	int referenceCloud, //acts as reference frame
	int numIterations, float alpha_point2plane, float alpha_point2point, float alpha_color, float cull_dist);

template
void BundleAdjustment::pointToPlane_Bundle_step_of<Eigen::Matrix<float,Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(
	std::vector<explainMe::Cloud::Ptr > &clouds,
	std::vector<std::vector<Eigen::MatrixXi>> &I,
	std::vector<std::vector<Eigen::MatrixXi>> &dI_dx,
	std::vector<std::vector<Eigen::MatrixXi>> &dI_dy,
	std::vector<std::vector<Eigen::MatrixXf>> &smoothedDepth,
	bool usedDepthAsIntensity,
	int pyr_level,
	std::vector<Eigen::Matrix<float,Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> &weights,
	int wight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
	int referenceCloud, //acts as reference frame
	int numIterations, float alpha_point2plane, float alpha_point2point, float alpha_color, float alpha_of, float cull_dist,
	RangeSensorDescription & d,
	LM_params & params);

template
void BundleAdjustment::pointToPlane_Bundle_step_LM<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(
	std::vector<explainMe::Cloud::Ptr > &clouds,
	std::vector<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> &weights,
	int weight_idx,
	std::vector<Eigen::Matrix4f> &alignments,
	std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
	int referenceCloud, //acts as reference frame
	int numIterations,
	float alpha_point2plane,
	float alpha_point2point,
	float alpha_color,
	float culling_dist,
	RangeSensorDescription & d,
	LM_params & params //will be updated
	);

template
void BundleAdjustment::LM_BA_fast<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>, mySimpleKdTree, pxyz, simpleNNFinder>(
std::vector<explainMe::Cloud::Ptr > &clouds,
std::vector<mySimpleKdTree> & allTrees,
std::vector<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> &weights,
int weight_idx,
std::vector<Eigen::Matrix4f> &alignments,
std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
int referenceCloud, //acts as reference frame
int numIterations,
float alpha_point2plane,
float alpha_point2point,
float alpha_color,
float culling_dist,
RangeSensorDescription & d,
LM_params & params, //will be updated
int numPointsToSpl);

template
void BundleAdjustment::LM_BA_fast<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>, mySimpleKdTree_ndim<6>, p_ndim<6>, simpleNNFinder_ndim<6>>(
std::vector<explainMe::Cloud::Ptr > &clouds,
std::vector<mySimpleKdTree_ndim<6>> & allTrees,
std::vector<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> &weights,
int weight_idx,
std::vector<Eigen::Matrix4f> &alignments,
std::vector<std::vector<int>> enforce, //enforce[i] is the set of clouds aligned to i.
int referenceCloud, //acts as reference frame
int numIterations,
float alpha_point2plane,
float alpha_point2point,
float alpha_color,
float culling_dist,
RangeSensorDescription & d, //will be updated
LM_params & params, //will be updated
int numPointsToSpl);

template
	void BundleAdjustment::pointToPlane_Bundle_step<Eigen::Matrix<float,Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(std::vector<trajectory_R> & trajs, int numFrames, int ref_frame,
	Eigen::Matrix<float,Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> & weights, int hyp, std::vector<Eigen::Matrix4f> &alignments, float p1);