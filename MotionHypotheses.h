#pragma once
#include <vector>
#include "explainMeDefinitions.h"
#include "mySimpleKdTree.h"
#include "Sensors.h"


//ATM implements two ways of computing alignments (the computeHyp methods) and
//wraps a few ways to use the bundle adjustment stuff.

struct my_range{int start, stop; my_range(int s,int e){start =s; stop = e;}};
struct hyp_range{int hyp, start, stop;};
struct hyp_range_flow;
struct hyp_range_flow{ int hyp, start, stop; Eigen::VectorXf massFlow_start, massFlow_end; hyp_range_flow  * prev, *next; };

//information for debugging
class history{
public:
	enum creation_type { NEW, SHIFT, SPLIT, MERGE };
	creation_type type;
	std::vector<int> parent_labels;
	std::vector<hyp_range> parent_ranges;
	std::string description;

	history(){
		set(NEW);
		
	}

	void set(creation_type t, int from = -1, int to=-1, int frame = -1){
		switch (t){
		case NEW:
			description = std::string("NEW");
			break;
		case SHIFT:
			description = std::string("SHIFT: ") + std::to_string(from) + "->" +std::to_string(to) + " frm[" + std::to_string(frame) + "]";
			break;
		case SPLIT:
			description = std::string("SPLIT: ") + std::to_string(from) + "|" + std::to_string(to) + " frm[" + std::to_string(frame) + "]";
			break;
		case MERGE:
			description = std::string("MERGE: ") + std::to_string(from) + "&" + std::to_string(to);
			break;
		}
	}
};

class motion{
public:
	//validity range: from start, exclusive stop.
	history hist;
	my_range range;
	float quality; 
	std::vector<Eigen::Matrix4f> alignments;
private:
	int uniquelabel;
	static int nextFreeLabelId;
public:

	motion(std::vector<Eigen::Matrix4f> & a, my_range & r):range(r){ 
		uniquelabel = nextFreeLabelId;
		nextFreeLabelId++;
		quality = -1;
		alignments = a;
	}
	motion( int numClouds) :range(0, numClouds){
		uniquelabel = nextFreeLabelId;
		nextFreeLabelId++;
		quality = -1;
		alignments.resize(numClouds, Eigen::Matrix4f::Identity());
	}
	motion(std::vector<Eigen::Matrix4f> & a,int numClouds) :range(0, numClouds){
		uniquelabel = nextFreeLabelId;
		nextFreeLabelId++;
		quality = -1;
		alignments=a;
	}
	~motion(){}

	bool isInRange(int cloudIdx){
		return range.start <= cloudIdx && cloudIdx < range.stop;
	}

	Eigen::Matrix4f & operator[](size_t idx)
	{
		return alignments[idx];
	}

	size_t size(){
		return alignments.size();
	}

	int unique_label(){
		return uniquelabel;
	}
};

class MotionHypotheses
{
private :
	//refactor: put single alignmentset, label, range, last_error nad quality into one motion obj.
	/*std::vector<std::vector<Eigen::Matrix4f>> alignmentSets; 
	//validity range: from start, exclusive stop.
	std::vector<my_range> ranges;
	std::vector<float> quality;*/
	std::vector<motion> motions;
	RangeSensorDescription * d;

	std::vector<Eigen::Matrix4f> initialGuess; //identities, or average motion.
		
public:
	struct {
		std::vector<Eigen::RowVectorXf> supports;
		std::vector<Eigen::MatrixXf> massFlows, massFlowsLoss;
		float supportThreshold;
	} subresults_ForVisualizationOnly;

	enum RegistrationType {ICP_INCREMENTAL, ICP_FANCY, ICP_FANCY_SLOW, ICP_FANCY_6D};

	MotionHypotheses(int initialNumHyps, int numClouds, RangeSensorDescription * desc);
	~MotionHypotheses(void);

	//check, if this is needed...
	//compute the ranges out of the denseSeg.
	void set(std::vector<std::vector<Eigen::Matrix4f>> & alignments, 
		std::vector<explainMe::MatrixXf_rm> & denseSeg);
	//uses dummy ranges.
	void set(std::vector<std::vector<Eigen::Matrix4f>> & alignments);

	void setInitialGuessToAvgMotion(
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<mySimpleKdTree> & allKdTrees,
		int central_cloud, //the coordinate system of this cloud is used as reference system
		//the parameters...
		int maxItICP,
		float alpha_point2plane,
		float alpha_point2point,
		float cullingDist,
		bool resetAllMotions);

	//GETTERS
	std::vector<Eigen::Matrix4f> & getAlignmentSet(int hyp){
		return motions.at(hyp).alignments;
	}
	std::vector<Eigen::Matrix4f> getAlignmentSetInv(int hyp){
		auto & al = motions.at(hyp).alignments;;
		std::vector<Eigen::Matrix4f> inv =  al;
		for (int i = 0; i < inv.size(); i++){
			inv[i] = al[i].inverse();
		}
		return inv;
	}
	std::vector<std::vector<Eigen::Matrix4f>> getAlignmentSetsInv(){
		std::vector<std::vector<Eigen::Matrix4f>> all_inv;
		for (int i = 0; i < numHypotheses(); i++){
			all_inv.push_back(getAlignmentSetInv(i));
		}
		return all_inv;
	}


	std::vector<motion> & getAlignmentSets(){	
		return motions; 
	}
	my_range & getRange(int i)
	{	
		return motions.at(i).range; 
	}

	history & getHistory(int hyp){
		return motions.at(hyp).hist;
	}

	int uniqueID(int hyp){
		return motions.at(hyp).unique_label();
	}
	int supportSize(int hyp){ 
		return std::min<int>(motions.at(hyp).range.stop - motions.at(hyp).range.start, motions.at(hyp).size()); 
	}

	bool isHypInRange(int hyp, int cloudIdx){
		return motions.at(hyp).isInRange(cloudIdx);
	}
	int numHypotheses(){
		return motions.size();
	}
	void push_back(std::vector<Eigen::Matrix4f> set, my_range range){
		motions.push_back(motion(set, range));
	}

		
	//removes the elements in the list, while keeping the relative ordering of hyps
	void remove(std::vector<int> hypoList); //by value, gets sorted internally.


	//update the alignmentsets and ranges based on the segmentations.
	//generates new alignment sets by appending them
	void computeHypothesesSetFromDenseSegmentation(
		//the data
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		int central_cloud, //the coordinate system of this cloud is used as reference system
		//the parameters...
		int maxItICP,
		float alpha_point2plane,
		float alpha_point2point,
		float cullingDist,

		int genK, float lambda_outlier,
		bool genHypByShift,
		bool genHypByCarryOver
		);

	//update the alignmentsets and ranges based on the segmentations.
	//generates new alignment sets by solving for a new motion. better.
	void computeHypothesesSetFromDenseSegmentation_segFirst(
		//the data
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		int central_cloud, //the coordinate system of this cloud is used as reference system
		//the parameters...
		int maxItICP,
		float alpha_point2plane,
		float alpha_point2point,
		float cullingDist,

		int genK, float lambda_outlier,
		bool genHypByShift,
		bool genHypByCarryOver,

		RegistrationType t
		);



	std::vector<hyp_range_flow> findMotionSegments(
		std::vector<Eigen::RowVectorXf>  &frame_support,
		std::vector<Eigen::MatrixXf>  &frame_shift_mat, 
		std::vector<Eigen::MatrixXf> &frame_shift_mat_fwd
		);
	void findTempShifts_and_updateDenseSeg(
		//the data
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		int genK,
		float lambda_outlier
		);

	std::vector<hyp_range_flow> findAndLinkMotionsSegments(
		//the data
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees		
		);

	void temporallySplitHypotheses_and_updateDenseSeg(
		//the data
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees);

	void splitAndCombineHypotheses_and_updataDenseSeg(
		//the data
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees
		);

	
	void updateAlignmentsAndRange(
		int label,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		int central_cloud,
				//the parameters...
		int maxItICP,
		float alpha_point2plane,
		float alpha_point2point,
		float cullingDist,
		RegistrationType t, 
		int numLoopClosureSearches = 1, int numPointsToUse = 2500, 
		bool startWithLoopClosures = false);

	void updateAlignmentsAndRange_incremental(
		int label,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		int central_cloud,
		//the parameters...
		int maxItICP,
		float alpha_point2plane,
		float alpha_point2point,
		float cullingDist);

	void updateAlignmentsAndRange_tweaked(
		int label,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		int central_cloud,
				//the parameters...
		int maxItICP,
		float alpha_point2plane,
		float alpha_point2point,
		float cullingDist,
		int numberOfLoopSearches =10);

	/*void updateAlignmentsAndRange_tsdf(
		int label,
		std::vector<explainMe::Cloud::Ptr> & extractedClouds, //because my interfaces suck.
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		int central_cloud,
		//the parameters...
		int maxItICP,
		float alpha_point2plane,
		float alpha_point2point,
		float cullingDist);*/

	void updateAlignmentsAndRange_of(
		int label,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		std::vector<std::vector<Eigen::MatrixXi>>& allI, 
		std::vector<std::vector<Eigen::MatrixXi>>&dI_dx, 
		std::vector<std::vector<Eigen::MatrixXi>>& dI_dy,
		std::vector<std::vector<Eigen::MatrixXf>>  smoothed_depth,
		bool usingDepthAsIntensities,
		int central_cloud,
				//the parameters...
		int maxItICP,
		float alpha_point2plane,
		float alpha_point2point,
		float alpha_of,
		float cullingDist,
		RangeSensorDescription & d);

	//deprecated?.
	void step1_bundleAdjustAll(
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		int central_cloud,
				//the parameters...
		int maxItICP,
		float alpha_point2plane,
		float alpha_point2point,
		float cullingDist);

	
	void step2_analyseDenseSegmentation(
		int genK,
		float lambda_outlier,
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		std::vector<std::vector<int>> & result_frame_from_to,
		std::vector<hyp_range> & result_supportRanges,
		bool genHypByShift,
		bool genHypByCarryOver
	);
	//void generateNewHypotheses();

	//do merging of alignments by appending them.
	void step3_generateFinalHypotheses(
		std::vector<hyp_range> supportRanges,
		std::vector<std::vector<int>> & frame_from_to
		);


private:
	void updateRangesFromSupport(std::vector<Eigen::RowVectorXf> & frame_supports);
	//these methods with the huge signatures should belong to a class
	//managing the clouds, kdtrees and segmentation...
public:
	void computeDwDt_etc(
		//in
			std::vector<explainMe::Cloud::Ptr> & allClouds,
			std::vector<explainMe::MatrixXf_rm> & denseSeg,
			std::vector<mySimpleKdTree> & allKdTrees,
		//out
			std::vector<Eigen::MatrixXf> & frame_shift,
			std::vector<Eigen::MatrixXf> & frame_shift_fwd,
			std::vector<Eigen::RowVectorXf> & frame_total_shift
	);

	void computeSupport(std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<Eigen::RowVectorXf> & frame_support);

	void updateRanges(std::vector<explainMe::MatrixXf_rm> & denseSeg)
	{
		std::vector<Eigen::RowVectorXf> frame_support;
		computeSupport(denseSeg, frame_support);
		updateRangesFromSupport(frame_support);
	}

private:
	void compute_dense_dWdT_via_nn(
			//in
		std::vector<explainMe::Cloud::Ptr> & allClouds,
		std::vector<explainMe::MatrixXf_rm> & denseSeg,
		std::vector<mySimpleKdTree> & allKdTrees,
		int cloud_idx,
		//out
		explainMe::MatrixXf_rm & result_dense_dwdt,
		Eigen::MatrixXf & result_shift_matrix_from_prev, 
		Eigen::MatrixXf & result_shift_matrix_to_next,
		Eigen::RowVectorXf & absoluteShift);



	std::vector<hyp_range> computeValidityRanges(
		float threshold,
		int min_range,
		std::vector<Eigen::RowVectorXf> & frame_support);

	void find_top_k_labelShifts(
			//in
			std::vector<Eigen::MatrixXf> & frame_shift_mat, 
			//params: find top k shifts that are lambda*sigma > shift mean.
			int genK, float lambda_outlier,
			std::vector<std::vector<int>> &result_frame_from_to);

	void push_back_labelCarryOvers(
		//in
		std::vector<Eigen::MatrixXf> & frame_shift_mat, 
		std::vector<hyp_range> & ranges,
	//push back to this vector.
		std::vector<std::vector<int>> & result_frame_from_to);

};
