#include "IROS16.h"
#include "statsWindow.h"
#include "explainMeDefinitions.h"
#include "algorithmParameters.h"
#include "BundleAdjustmentV2.h"
#include "maxflowMincut/graph.h"
#include "motionAnalysis.h"
#include <Eigen/Dense>
#include <qdatastream.h>

#ifdef _MSC_VER
#define sqrtf std::sqrtf
#define logf std::logf
#endif

ExperimentIros::ExperimentIros(CloudReader::Ptr data_, algorithmParameters  &params_, std::string &subresultFolde)
{
	outputFolder = subresultFolde;
	params = params_;
	data = data_;
	start_with_frame = 1;
}

ExperimentIros::~ExperimentIros()
{
}


QDataStream &operator<<(QDataStream &out, const std::vector<int> & vec)
{
	//out << painting.title() << painting.artist()
	//	<< quint32(painting.year());
	out << quint32(vec.size());
	for (int i = 0; i < vec.size(); i++){
		out << vec[i];
	}
	return out;
}
QDataStream &operator>>(QDataStream & in,  std::vector<int> & vec){
	quint32 sz;
	in >> sz;
	vec.resize(sz);
	for (int i = 0; i < sz; i++){
		in >> vec[i];
	}
	return in;
}

QDataStream &operator<<(QDataStream &out, const explainMe::Cloud::Ptr & cloud)
{
	out << quint32(cloud->width);
	out << quint32(cloud->height);
	auto & ps = cloud->points;
	for (auto & p: ps){
		out << p.x << p.y << p.z << p.normal_x << p.normal_y << p.normal_z << p.r << p.g << p.b << p.curvature;
	}
	return out;
}

QDataStream &operator>>(QDataStream & in, explainMe::Cloud::Ptr & cloud){
	quint32 w,h;
	in >> w;
	in >> h;
	cloud->resize(w*h);
	cloud->width = w;
	cloud->height = h;
	auto & ps = cloud->points;
	for (auto & p : ps){
		in >> p.x >> p.y >> p.z >> p.normal_x >> p.normal_y >> p.normal_z >> p.r >> p.g >> p.b >> p.curvature;
	}
	return in;
}

QDataStream &operator<<(QDataStream &out, const std::vector<explainMe::Cloud::Ptr> & vec)
{
	out << quint32(vec.size());
	for (int i = 0; i < vec.size(); i++){
		out << vec[i];
	}
	return out;
}
QDataStream &operator>>(QDataStream &in, std::vector<explainMe::Cloud::Ptr> & vec)
{
	quint32 sz;
	in >> sz;
	vec.reserve(sz);
	for (int i = 0; i <sz; i++){
		vec.push_back(explainMe::Cloud::Ptr(new explainMe::Cloud()));
		in >> vec[i];
	}
	return in;
}

QDataStream &operator<<(QDataStream &out, const std::vector<Eigen::Matrix4f> & vec)
{
	out << quint32(vec.size());
	QMatrix4x4 mat;
	for (int i = 0; i < vec.size(); i++){
		mat = QMatrix4x4(vec[i](0, 0), vec[i](0, 1), vec[i](0, 2), vec[i](0, 3),
			vec[i](1, 0), vec[i](1, 1), vec[i](1, 2), vec[i](1, 3),
			vec[i](2, 0), vec[i](2, 1), vec[i](2, 2), vec[i](2, 3),
			vec[i](3, 0), vec[i](3, 1), vec[i](3, 2), vec[i](3, 3));
		out <<mat;
	}
	return out;
}
QDataStream &operator>>(QDataStream &in, std::vector<Eigen::Matrix4f> & vec)
{
	quint32 sz;
	in >> sz;
	vec.resize(sz);
	QMatrix4x4 mat;
	for (int i = 0; i < vec.size(); i++){
		in >> mat;
		vec[i] << mat(0, 0), mat(0, 1), mat(0, 2), mat(0, 3),
			mat(1, 0), mat(1, 1), mat(1, 2), mat(1, 3),
			mat(2, 0), mat(2, 1), mat(2, 2), mat(2, 3),
			mat(3, 0), mat(3, 1), mat(3, 2), mat(3, 3);
		
	}
	return in;
}

QDataStream &operator<<(QDataStream &out, const std::vector<std::vector<Eigen::Matrix4f>> & vec)
{
	out << quint32(vec.size());
	for (int i = 0; i < vec.size(); i++){
		out << vec[i];
	}
	return out;
}
QDataStream &operator>>(QDataStream &in, std::vector<std::vector<Eigen::Matrix4f>> & vec)
{
	quint32 sz;
	in >> sz;
	vec.resize(sz);
	for (int i = 0; i < vec.size(); i++){
		in >> vec[i];
	}
	return in;
}
void ExperimentIros::store(std::string & f){

	QFile file(f.c_str());
	file.open(QIODevice::WriteOnly);
	QDataStream out(&file);
	

	//header
	out << (quint32)0xA072940A;
	out << (qint32)100;
	out.setVersion(QDataStream::Qt_5_0);
	//data
	out << currentMotions;
	out << hardLabels;
	out << trajectories;

}

void ExperimentIros::restore(std::string & f){

	QFile file(f.c_str());
	file.open(QIODevice::ReadOnly);
	QDataStream in(&file);


	//header
	quint32 magic;
	in >> magic;
	if (magic != 0xA072940A){
		std::cout << "Error, bad file format" << f << "\n";

		exit(1);
	}

	qint32 version;
	in >> version;;
	if (version == 100){
		in.setVersion(QDataStream::Qt_5_0);

		in >> currentMotions;
		in >> hardLabels;
		in >> trajectories;

		sampledPoints_zero = trajectories[0];
		start_with_frame = trajectories.size();
	}
	else{
		std::cout << "Unsupported Version" << version<<"!\n";
	}

}

void ExperimentIros::loadAllClouds(){
	for (int frm = params.first_frame; frm <= params.last_frame; frm++)
	{

		data->loadFrame(frm, false/*params.cleanUpClouds*/, params.maxDistance, true, 0.02);


		auto loadedCloudBuff = data->getPointCloud();;
		explainMe::Cloud::Ptr newCloud = explainMe::Cloud::Ptr(new explainMe::Cloud());

		newCloud->reserve(loadedCloudBuff->size());
		//work with NANfree clouds.
		for (auto p : loadedCloudBuff->points){
			if (p.z * 0 == 0 && p.z != 0 && p.getNormalVector3fMap().norm() > 0.99)
				newCloud->push_back(p);
		}

		allClouds.push_back(newCloud);

	}
	//now build KDtrees
	int nClouds = allClouds.size();
	allClouds_kdTree.resize(nClouds);
#ifndef _DEBUG
#pragma omp parallel
#endif
	{
#ifndef _DEBUG
#pragma omp for
#endif
		for (int i = 0; i <nClouds; i++)
		{
			allClouds_kdTree[i].setData<explainMe::Cloud::Ptr>(allClouds[i]);
			allClouds_kdTree[i].buildTree();
		}
	}
}

void ExperimentIros::sampleCloud0(){
	sampledPoints_zero = explainMe::Cloud::Ptr(new explainMe::Cloud());
	//int hundredth_sample = 0;
	float percent = 1;
	
	for (auto p : allClouds[0]->points){
		if (p.z != 0 && p.z * 0 == 0 && p.getNormalVector3fMap().norm() > 0.99){
			if (rand()%1000 <=percent*10){
				sampledPoints_zero->push_back(p);
			}
		}
	}

	std::cout << "Sampled " << sampledPoints_zero->size() << " points in first cloud. ";
}

void ExperimentIros::initTrajectories(explainMe::Cloud::Ptr baseSamples){
	trajectories.push_back(baseSamples);
	//labelWeights.push_back(std::vector<float>());
	//labelWeights.back().resize(baseSamples->size(), 1);
	hardLabels.resize(baseSamples->size(), 0);
	//to have consistent dimensions, for f frames there 
	//will be f transformations; the transformation to the first frame is
	//fixed to be the identity.
	currentMotions.push_back(std::vector<Eigen::Matrix4f>(1, Eigen::Matrix4f::Identity()));
}


void ExperimentIros::updateTrajectories_fromLabelsAndMos_between(
	std::vector<std::vector<Eigen::Matrix4f>> & motions_by_frame_label, 
	int from_frame, int to_frame, bool reproject)
{
	for (int f = from_frame + 1; f <= to_frame; f++){
		auto  traj_f_min_1 = trajectories[f - 1];
		auto & nextFrame = allClouds[f];
		auto  nextFrameKdTree = allClouds_kdTree[f];

		auto & traj_nextframe = trajectories[f]->points;

		//update the correspondences

		auto currentMotions_fmin1_inv = motions_by_frame_label[f - 1];
		for (int i = 0; i < currentMotions_fmin1_inv.size(); i++){
			currentMotions_fmin1_inv[i] = currentMotions_fmin1_inv[i].inverse();
		}

		simpleNNFinder nnFinder(nextFrameKdTree);
		int idx = 0;
		Eigen::Vector4f mapped_p;
		Eigen::Vector4f mapped_n,n;
		for (auto & p : traj_f_min_1->points){
			//map point to the next frame and update the point to its nearest nbr.
			mapped_p = motions_by_frame_label[f][hardLabels[idx]] * currentMotions_fmin1_inv[hardLabels[idx]] * p.getVector4fMap();
			n = p.getNormalVector4fMap();
			n.w() = 0;
			mapped_n = motions_by_frame_label[f][hardLabels[idx]] * currentMotions_fmin1_inv[hardLabels[idx]] * n;
			if (reproject)
			{
				pxyz query = pxyz(mapped_p.x(), mapped_p.y(), mapped_p.z());
				auto dist_idx = nnFinder.findNN(query);
				traj_nextframe[idx] = (nextFrame->points[dist_idx.index]);
			}
			else{
				traj_nextframe[idx].getVector4fMap() = mapped_p;
				traj_nextframe[idx].getNormalVector3fMap() = mapped_n.topLeftCorner<3, 1>();
			}
			idx++;
		}
	}
}

void ExperimentIros::extendTrajectories(int frame)
{

	trajectories.push_back(explainMe::Cloud::Ptr(new explainMe::Cloud()));
	auto  traj_end = trajectories[frame-1];
	auto & nextFrame = allClouds[frame];
	auto  nextFrameKdTree = allClouds_kdTree[frame];
	auto traj_extension = trajectories[frame];

	traj_extension->reserve(traj_end->size());
	int idx = 0;
	
	simpleNNFinder nnFinder(nextFrameKdTree);
	for (auto & p : traj_end->points){
		pxyz query = pxyz(p.x, p.y, p.z);
		auto dist_idx = nnFinder.findNN(query);
		traj_extension->push_back(nextFrame->points[dist_idx.index]);
	}


	//currently: init new motions just with identity.
	//VARIANT: Initialize/Extend the current Motions via ICP
	//could directly do motion & corresp update
	int num_labels = numLabels();
	currentMotions.push_back(std::vector<Eigen::Matrix4f>(num_labels, Eigen::Matrix4f::Identity()));

}

Eigen::MatrixXf ExperimentIros::gramSchmidt(Eigen::MatrixXf & V)
{
	Eigen::MatrixXf U = V.householderQr().householderQ();
	return U;
}

Eigen::MatrixXf ExperimentIros::null_space(Eigen::MatrixXf & V_tranposed){
	Eigen::FullPivLU<Eigen::MatrixXf> lu(V_tranposed);
	Eigen::MatrixXf A_null_space = lu.kernel();

	Eigen::FullPivHouseholderQR<Eigen::MatrixXf> QR(A_null_space);

	A_null_space=  QR.matrixQ();
	//this is a kernel, but it is not an orthonormal basis
	/*std::cout << "norms...";
	for (int i = 0; i < A_null_space.cols(); i++){
		std::cout <<  A_null_space.col(i).norm() << ", \t";
		//A_null_space.col(i).array() /= A_null_space.col(i).norm();
		if (i % 5 == 0){
			std::cout << "\n";
		}

	}
	std::cout << "\npairwise dot first basis with others: ";
	for (int i = 0; i < A_null_space.cols(); i++){
		std::cout << "<0," << i << ">" << A_null_space.col(0).transpose() * A_null_space.col(i) << ", \t";
		if (i % 5 == 0){
			std::cout << "\n";
		}
	}

	std::cout << "\npairwise dot original with first: ";
	for (int i = 0; i < V_tranposed.rows(); i++){
		std::cout << "<" << i << ",0>" << V_tranposed.row(i) * A_null_space.col(0) << ", \t";
		if (i % 5 == 0){
			std::cout << "\n";
		}
	}
	std::cout << "\n";
	waitForCMDok(-1, true, true);*/
	return A_null_space;
}

Eigen::VectorXf ExperimentIros::mvnpdf_unnormalized_around0(Eigen::MatrixXf &x, Eigen::MatrixXf &sigma, float & normalization_factor, float & normalization_factor_2_exponent)
{
	int dim_complement = sigma.rows();
	
	Eigen::VectorXf pdf = Eigen::VectorXf::Zero(x.cols());
	
	//VARIANT: don't explicitely compute sigma_inv but compute sigma_inv *x by solving sigma*res = x;

	//Eigen::MatrixXf sigma_inv = sigma.inverse();
	

	//the treatment of directions with 0 variance needs to be careful.
	Eigen::SelfAdjointEigenSolver<Eigen::MatrixXf> eigensolver(sigma);
	Eigen::MatrixXf evs = eigensolver.eigenvectors();

	//problem: the sigma is not pos semidef! probably due to numerical issues.
	Eigen::VectorXf evals = eigensolver.eigenvalues().cwiseAbs();
	evals.array() += 1e-6; //vs very small.
	//inverse by inverting the evals
	Eigen::VectorXf evals_inv = 1.0 / (evals.array());
	Eigen::MatrixXf sigma_inv = evs*evals_inv.asDiagonal() * evs.transpose();

	//float determinant = sigma.determinant(); //numerically to unstable
	printStats("sigma_inv stats ", __LINE__, sigma_inv);

	//float normalisation_factor = 1.0 / (std::sqrt(std::pow(2 * M_PI, dim_complement)*std::abs(determinant))+1e-);
	//float normalisation_factor = 1.0 / (std::sqrt(std::pow(2 * M_PI, dim_complement)*evals.cwiseAbs().minCoeff()) + 1e-8);
	float _2pi_pow_q =(std::pow(2 * M_PI, dim_complement));


	//robust computation of the normalization factor:
	//the determinant is the product eigenvalues. This can be small for everyone. to compute this robustly,
	//split the exponent from the computation; probly would have been enough to use doubles.
	float significand_prod = 1;
	int exponent_sum=0;
	for (int i = 0; i < evals.size(); i++){
		int binary_exp;
		float significand = frexp(evals(i), &binary_exp);
		significand_prod *= significand;
		exponent_sum += binary_exp;


		/*if (significand * 0 != 0 || significand_prod * 0 != 0){
			std::cout << "Weirdness in significand: " << significand << ", " << significand_prod << "\n";
			std::cout << "evals(" << i << ") " << evals(i) << "\n";
			std::cout << "exponent sum " << exponent_sum << "\n";
			printStats("evals ", __LINE__, evals, true);
			waitForCMDok(-1, true, true);
		}*/

		//take away further zeros,,,
		significand_prod = frexp(significand_prod, &binary_exp);
		exponent_sum += binary_exp;

	}

	if (significand_prod * 0 != 0){
		std::cout << "Weirdness in significand: "  << significand_prod << "\n";
		//std::cout << "evals(" << i << ") " << evals(i) << "\n";
		std::cout << "exponent sum " << exponent_sum << "\n";
		printStats("evals ", __LINE__, evals, true);
		waitForCMDok(-1, true, true);
	}

	//_2pi_pow_q is constant for all spaces anyhow, also
	//for high dimensions, e.g. 100-300 frames it might become inf
	//thus we ignore this factor
	/*std::cout << "*** Real determinant is = " << significand_prod << "* 2^" << exponent_sum << "\n";
	int binary_exp;
	float significand = frexp(_2pi_pow_q, &binary_exp);
	significand_prod *= significand;
	exponent_sum += binary_exp;

	if (significand * 0 != 0 || significand_prod * 0 != 0){
		std::cout << "Weirdness in significand aftermultiplying with pi....: " << significand << ", " << significand_prod << "\n";
		std::cout << "_2pi_pow_q " << _2pi_pow_q << "\n";
		std::cout << "pow(2 * " << M_PI << "," << dim_complement << ")\n";
		std::cout << "exponent sum " << exponent_sum << "\n";
		printStats("evals ", __LINE__, evals, true);
		waitForCMDok(-1, true, true);
	}*/


	normalization_factor = 1.0 / (sqrtf(std::abs(significand_prod))+ 1e-6);
	normalization_factor_2_exponent = -0.5*exponent_sum;

	/*if (normalization_factor * 0 != 0){
		std::cout << "Weirdness in normalization computation: " << normalization_factor << "\n";
		printStats("evals ", __LINE__, evals, true);
		std::cout << "significand_prod " << significand_prod << "\n";
		std::cout << "exponent_sum " << exponent_sum << "\n";
		waitForCMDok(-1, true, true);
		std::cout << "Eigenvals ...\n " << evals.transpose() << "\n";
		std::cout << "Eigenvals approx inv ...\n " << evals_inv.transpose() << "\n";
		waitForCMDok(-1, true, true);
	}*/

	std::cout << "*** Normalization factor is = " << normalization_factor << "* 2^" << normalization_factor_2_exponent << "\n";
	

//alternative: numerically more stable exp.
//extract a common exponent from exp(-0.5DSigD)
//Eigen::MatrixXf UD_k = evs.transpose() * x;
//Eigen::VectorXi pdf_exponents; pdf_exponents.resizeLike(pdf);

	//approach one: just computing the exponential
	/*
	for (int x_idx = 0; x_idx < x.cols(); x_idx++){
		float x_sigInv_x = x.col(x_idx).transpose()*sigma_inv *x.col(x_idx);
		float bla = std::exp(-0.5*x_sigInv_x);
		pdf(x_idx) = bla;

	}*/

	//Approach 2: make sure std::exp(-0.5*x_sigInv_x) is numerically stable!
	//That is: do two paths: store all distances
	// find the minimum (or something more robust)
	for (int x_idx = 0; x_idx < x.cols(); x_idx++){
		float x_sigInv_x = x.col(x_idx).transpose()*sigma_inv *x.col(x_idx);
		pdf(x_idx) = 0.5*x_sigInv_x;
	}
	{

		float minDistance = pdf.minCoeff();
		int minExp = std::floor(minDistance);
		//stable computation of e^-minExp
		double e = std::exp(1.0);
		float exp_prod = 1; int exp_exponent_sum = 0, binary_exp;
		for (int i = 1; i < minDistance; i++){
			exp_prod /= e;
			exp_prod = frexp(exp_prod, &binary_exp);
			exp_exponent_sum += binary_exp;
		}
		std::cout << "e^-" << minExp << "=" << exp_prod << "*2^" << exp_exponent_sum << "\n";

		pdf.array() -= minExp;
		for (int x_idx = 0; x_idx < x.cols(); x_idx++){
			//stable approach (together with exp_exponentsum)
			float bla = std::exp(-pdf(x_idx))*exp_prod;/* * 2^exp_exponent_sum */
			pdf(x_idx) = bla;
			/*//first approach.
			float x_sigInv_x = x.col(x_idx).transpose()*sigma_inv *x.col(x_idx);
			bla = std::exp(-0.5*x_sigInv_x);
			std::cout << bla << " [= epx(-0.5 " << x_sigInv_x << ")] *2^" << normalization_factor_2_exponent;
			std::cout << " vs \t";
			std::cout << pdf(x_idx) << "*2^" << normalization_factor_2_exponent + exp_exponent_sum << "\n";
			std::cout << "\n";
			
			if (x_idx % 30 == 0){
				waitForCMDok(-1, true, true);
			}*/
		}

		normalization_factor_2_exponent += exp_exponent_sum;
	}
	//waitForCMDok(-1, true, true);


	if (!pdf.allFinite() || normalization_factor * 0 != 0){//inf here already... but dk_n and sigma_k where fine
		std::cout << "Detected infinite pdf or normalization factor => p_n_giv_ks during normalpdf...\n";
		std::cout << "Normalizsation factor " << normalization_factor << "\n";
		printStats("pdf", __LINE__, pdf, true);
		std::cout << "sigma k is " << dim_complement << "x" << dim_complement << " matrix.\n";
		printStats("Sigma_k", __LINE__, sigma, true);
		printStats("Dk_n[k]", __LINE__, x, true);
		printStats("Sigma_k_inv", __LINE__, sigma_inv, true);
		printStats("eigenvals", __LINE__, evals, true);
		printStats("eigenvals_approx_inv", __LINE__, evals_inv, true);
		std::cout << "Eigenvalue computation did ";
		if (eigensolver.info() != Eigen::Success){
			std::cout << " NOT ";
		}
		std::cout << "succeed\n";
		if (eigensolver.info() != Eigen::Success){
			std::cout << " ( " << (eigensolver.info() == 1 ? "The provided data did not satisfy the prerequisites":
				(eigensolver.info() == 2 ? "Iterative procedure did not converge" :
				"The inputs are invalid, or the algorithm has been improperly called."
				" When assertions are enabled, such errors trigger an assert." )) << ")\n";
		}
		std::cout << "Will print out eigenvalues after next ok...\n";
		waitForCMDok(-1, true, true);
		std::cout << "Eigenvals ...\n " << evals.transpose() << "\n";
		std::cout << "Eigenvals approx inv ...\n " << evals_inv.transpose() << "\n";
		waitForCMDok(-1, true, true);
	}

	return pdf;
}


void matlabPrint(std::string name, Eigen::MatrixXf  mat)
{

	std::cout << name << "=[";
	for (int i = 0; i < mat.rows(); i++){
		for (int j = 0; j < mat.cols(); j++){
			std::cout << mat(i, j)<< ",";
			if (j % 5 == 0){
				std::cout << "...\n";
			}
		}
		std::cout << ";...\n";
	}
	std::cout << "];\n";
}
void ExperimentIros::EMsubspaceClustering(){

	//for dgb: step by step.
	bool debug_step_by_step = false;

	int num_labels = numLabels();
	//actually for f frames there are f mats - thoguth there are only f-1 relevant alignments
	//The first matrix is the identity. this makes indexing easier
	int num_frames = currentMotions.size();
	int num_trajs = numTrajectories();


	//update newsigma = alpha*old sigma + new_sigma(1-alpha). probly stupid idea.
	bool try_smooth_sigma_update = false; 
	float try_smooth_sigma_update_alpha_old = 0.75;

	int max_num_labels = 4; //4//10;
	int numIterations = 15;
	int numEMIt_final_pass = 45;//45;
	float sigma_eps = 0; // for posdeftnesss; added other guards
	bool vis_n_giv_k = true; //if fals then vis k_giv_n
	//bool recluster_from_scratch = false || num_frames % 5 == 0; //use the current motions and number of motions as initial guess or don't use it.
	bool recluster_from_scratch = false;
	bool initFromLabeling = true;
	
	bool normalize_pdf = true;

	//theoretically the subspaces have dimension 4. In practice 
	//use of higher dim might be useful.
	int num_dim_B = 4;// +std::min(std::max(0, num_frames - 2), 8);

	//two options: initialize clustering with k labelse from current motions, or
	//always start with a single label
	if (recluster_from_scratch){
		num_labels = 1;
	}

	std::cout << "EM clustering ,labels:  " << num_labels << ", trajs: " << num_trajs << ", frames:" << num_frames << "\n";
	//for simplicity: represent the trajectories in a matrix
	//each trajectoriy is stored in a column of this matrix
	//this gives a num_frames*3 x numTrajectories matrix
	Eigen::MatrixXf traj_matrix = Eigen::MatrixXf::Zero(num_frames * 3, num_trajs);
	for (int f = 0; f < num_frames; f++){
		auto &traj_points_frame_f = trajectories[f]->points;
		for (int tr = 0; tr < num_trajs; tr++){
			traj_matrix.block<3, 1>(f * 3, tr) = traj_points_frame_f[tr].getVector3fMap();
		}
	}


	//<------------------ Initialization ---------------
	//Using the current motions initialize Bk (ortho bases), where k is the label k.
	//Bk's size: 3 * numFrames x 4
	//std::vector<Eigen::MatrixXf> B;
	Eigen::MatrixXf Bk = Eigen::MatrixXf::Zero(num_frames * 3, 4), bk_transp;

	//B_ortho span the complement space to B
	//size, typcially (if B has rank 4) 3 * numFrames x(3 * numFrames - 4)
	//if B has rank 3 it gives one dim more.
	std::vector<Eigen::MatrixXf> B_ortho;
	B_ortho.resize(num_labels);

	//init each bk_ortho by stacking the motions  taking the ortho complement
	//doing gramschmidt on Bk would give the actual Bk...
	for (int k = 0; k < num_labels; k++)
	{
		for (int f = 0; f < num_frames; f++)
		{
			Bk.block<3, 4>(3 * f, 0) = currentMotions[f][k].topLeftCorner<3, 4>();
		}

		/*std::cout << "Bk = [";
		for (int j = 0; j < Bk.rows(); j++){
			std::cout << Bk.row(j) << ";...\n";
		}
		std::cout << "];";*/
		//if you want the actual Bk: do a gram-schmidt step on top of the Bk
		//B[k] = gramSchmidt(Bk);

		//
		bk_transp = Bk.transpose();
		B_ortho[k] = null_space(bk_transp);

		/*std::cout << "Bk_ortho = [";
		for (int j = 0; j < B_ortho[k].rows(); j++){
			std::cout << B_ortho[k].row(j) << ";...\n";
		}
		std::cout << "];";*/
		if (!B_ortho[k].allFinite()){
			std::cout << "ERROR, invalid args as input to EM clustering\n";
			std::cout << "Invalid motion " << k << "\n";
			printStats("Bkortho", __LINE__, B_ortho[k]);
			waitForCMDok(-1, true, true);
		}
	}

	//initialize the p_k_giv_n i.e. the per trajectory label probability
	//VARIANT: use the current hard labeling as initialization
	Eigen::MatrixXf p_k_giv_n = Eigen::MatrixXf::Ones(num_labels, num_trajs)* 1.0 / num_labels;
	if (initFromLabeling){
		if (num_labels > 1){
			for (int tr = 0; tr < num_trajs; tr++){
				p_k_giv_n.col(tr).fill(0);
				p_k_giv_n(hardLabels[tr], tr) = 1;

			}

			//guard against redundant labels.
			Eigen::VectorXf label_distribution = p_k_giv_n.rowwise().sum();
			std::cout << "initial label distribution: " << label_distribution.transpose() << "\n";
			if (label_distribution.minCoeff() < 20){
				std::cout << "There is a label under minimum size in the input to EM clustering!\nKilling";
				std::vector<int> killableLabels, otherLabels;
				for (int l = 0; l < num_labels; l++){
					if (label_distribution(l) < 20){
						killableLabels.push_back(l);
						std::cout << l << ", ";
					}
					else{
						otherLabels.push_back(l);
					}
				}
				std::cout << "\n";

				num_labels -= killableLabels.size();
				for (int l_i = killableLabels.size() - 1; l_i >= 0; l_i--){
					B_ortho.erase(B_ortho.begin() + killableLabels[l_i]);
				}

				//resize p_k_giv_n
				Eigen::MatrixXf p_k_giv_n_new = Eigen::MatrixXf::Ones(num_labels, num_trajs)* 1.0 / num_labels;
				int idx = 0;
				for (int l : otherLabels){
					p_k_giv_n_new.row(idx) = p_k_giv_n.row(l);
					idx++;
				}

				p_k_giv_n = p_k_giv_n_new;
				std::cout << "Killing done.\n";
				waitForCMDok(-1, true, true);
			}
		}
	}

	//initialization part 2: init p_k, Dk_n and Sigma_k. The initializations are
	//consistent with an update step.
	//p_k: marginalize the n(trajs) away
	Eigen::VectorXf p_k = (p_k_giv_n.rowwise().sum()) *1.0 / num_trajs;

	//The Dk_n: projection of the trajectories on the space ortho to Bk
	//B1_ortho'*trajectories(:,i): projection of trajecotry on orthogonal space
	//for a single trajectory this will be a vector of size(4 * numFrames - 4)x1
	//doing it for all vectors will be a matrix of size
	// (3 * numFrames - 4) x numPoints
	//This is done once per label, resulting in num_label matrices
	std::vector<Eigen::MatrixXf> Dk_n(num_labels, Eigen::MatrixXf::Zero(3 * num_frames - 4, num_trajs));
	for (int k = 0; k < num_labels; k++)
	{
		Dk_n.at(k) = B_ortho[k].transpose()*traj_matrix;
	}


	//last, the sigmas are initialized, they are variance matrices to describe the 
	//distributioin of the projected trajectories
	std::vector<Eigen::MatrixXf> Sigma_k;
	for (int k = 0; k < num_labels; k++)
	{
		int dim_complement = B_ortho[k].cols();
		Eigen::MatrixXf sigma = Eigen::MatrixXf::Zero(dim_complement, dim_complement);
		//auto & pk_n = p_k_giv_n(k, :);
		for (int traj = 0; traj < num_trajs; traj++)
		{
			sigma = sigma + p_k_giv_n(k, traj)*Dk_n[k].col(traj)*Dk_n[k].col(traj).transpose();
		}

		Sigma_k.push_back(sigma / p_k_giv_n.row(k).sum());
		Sigma_k[k] += Eigen::MatrixXf::Identity(dim_complement, dim_complement) *Sigma_k[k].maxCoeff() * sigma_eps; //pos deftness
	}

	//---------------Initialization------------------>
	std::cout << "initialisation for EM step complete....\n";// Example sig\n";
	//std::cout << Sigma_k[0]<< "\n";


	//--------------The EM + cluster addition algorithm ----------------

	bool has_unoptimized_labels = true;
	int num_labels_added_now = 0;

	Eigen::MatrixXf p_n_giv_k;
	bool keep_on_emIng = true;
	while (keep_on_emIng)
	{
		keep_on_emIng = has_unoptimized_labels && num_labels < max_num_labels;// && num_labels_added_now <= 4;
		std::cout << "\n-------------------------------\n";
		std::cout <<   "**------Doing EM with--------**\n" 
				<<  "\t\t" << num_labels <<  "\n" 
				<<"\n--------------labels---------- - \n";
		//iterate between E and M until convergence, then add a cluster....
		
		int emIteration = 0;
		p_n_giv_k = Eigen::MatrixXf::Zero(num_trajs, num_labels);
		Eigen::MatrixXf p_n_giv_k_last = Eigen::MatrixXf::Zero(num_trajs, num_labels);

		auto didImprove = [&p_n_giv_k, &p_n_giv_k_last]()->bool{
			return ((p_n_giv_k.array() + 1e-3) / (p_n_giv_k_last.array() + 1e-3) - 1).abs().maxCoeff() > 1e-3; };
		while (emIteration == 0 || (num_labels > 2 || (didImprove() && num_labels <3)) && emIteration < numIterations || (!keep_on_emIng && emIteration < numEMIt_final_pass))
		{
			std::cout << "\nEM iteration " << emIteration << "\n";
			p_n_giv_k_last = p_n_giv_k;
			emIteration++;
			//<------------- E step -------------------------
			/* estimate the conditional probability p(label | trajectory) = p_k_giv_n
			* (numLabel x numTry)
			* p_n_giv_k: p(trajectory | has label k) (numTraj x numLabel)
			* p_n_giv_k = 1 / sqrt((2 * pi) ^ dim_complement* abs(det(Sigma_k(:, : , 1))))* exp(-0.5 * Dkn'*sigma_k_inv*Dkn);
			* normal distribution evaluated for each traj.*/
			p_n_giv_k = Eigen::MatrixXf::Zero(num_trajs, num_labels);
			Eigen::VectorXf factor = Eigen::VectorXf::Ones(num_labels), factor_orig;
			Eigen::VectorXf factor_pow2 = Eigen::VectorXf::Ones(num_labels), factor_pow2_orig;
			for (int k = 0; k < num_labels; k++)
			{
				p_n_giv_k.col(k) = mvnpdf_unnormalized_around0(Dk_n[k], Sigma_k[k], factor(k), factor_pow2(k)).array();


				if (!p_n_giv_k.allFinite()){//inf here already... but dk_n and sigma_k where fine
					std::cout << "Detected infinite p_n_giv_ks after processing label = " << k << "...\n";
					printStats("p_n_giv_k", __LINE__, p_n_giv_k, true);
					printStats("Sigma_k", __LINE__, Sigma_k[k], true);
					printStats("Dk_n[k]", __LINE__, Dk_n[k], true);
					waitForCMDok(-1, true, true);
				}
			}

			
			if (normalize_pdf)
			{
				//the relative sizes of p_n_giv_k will be correct
				//robustify against zero determinants...
				std::vector<float> exponent_list;
				factor_orig = factor;
				factor_pow2_orig = factor_pow2;
				for (int k = 0; k < num_labels; k++)
				{
					/*if (!(factor(k) * 0 == 0))
					{
						factor(k) = 0.1;
						factor_pow2(k) = factor_pow2.maxCoeff();
					}*/
					exponent_list.push_back(factor_pow2(k));
				}

				//now if some determinants get very small this leads to instability.
				//so we compute the median and allow a range of +- 3 magnitudes.

				//factor_pow2.array() -= factor_pow2.minCoeff();

				//robuster variant?
				std::nth_element(exponent_list.begin(), exponent_list.begin() + num_labels / 2, exponent_list.end());
				int min_exp = std::max(exponent_list[num_labels / 2] - 10, factor_pow2.minCoeff());
				int max_exp = exponent_list[num_labels / 2] + 10;
				
				factor_pow2 = (factor_pow2.array() < min_exp).select(min_exp, factor_pow2.array());
				factor_pow2 = (factor_pow2.array() > max_exp).select(max_exp, factor_pow2.array());
				factor_pow2.array() -= min_exp;
				std::cout << "min_exp " << min_exp << ", max exp " << max_exp << ", orig min " 
					<< factor_pow2_orig.minCoeff() << ", orig max " << factor_pow2_orig.maxCoeff() << "median " << exponent_list[num_labels / 2] <<"\n";
				printStats(" robustified factor_pow2 ", __LINE__, factor_pow2, true);
				if (factor_pow2.minCoeff() == factor_pow2.maxCoeff() && num_labels > 2){
					std::cout << "Weirdness with powers!\n";
					std::cout << "origs: \n" << factor_pow2_orig.transpose() << "\n";
					std::cout << "after: \n" << factor_pow2.transpose() << "\n";
					waitForCMDok(num_frames-1, true);
				}

				for (int k = 0; k < num_labels; k++)
				{
					p_n_giv_k.col(k) *= factor(k)* std::pow(2, factor_pow2(k));

					if (!p_n_giv_k.allFinite()){
						std::cout << "Detected infinite p_n_giv_ks after normalizing label = " << k << "...\n";
						printStats("p_n_giv_k", __LINE__, p_n_giv_k, true);
						printStats("factor", __LINE__, factor, true);
						printStats("factor_pow2", __LINE__, factor_pow2, true);
						waitForCMDok(-1, true, true);
					}
				}
			}
			//printStats("p_n_giv_k", __LINE__, p_n_giv_k);
			//printStats("p_k", __LINE__, p_k);

			//now we can compute/update p_k_giv_n
			for (int k = 0; k < num_labels; k++)
			{
				p_k_giv_n.row(k) = p_k(k) *p_n_giv_k.col(k).transpose();
			}
			p_k_giv_n.array() += 1e-9;//+ eps for numeric problems...

			for (int n = 0; n < num_trajs; n++)
			{
				p_k_giv_n.col(n) /= p_k_giv_n.col(n).sum();
			}

			//-------------- E step ------------------------>

			//<<<<<<-------- M step -------------------------
			//now update the probability model: that is
			//Bk_ortho, p_k, Dk_n, sigma

			//Bk_ortho should habe dimensionality 3 * numFrames x(3 * numFrames - 4)
			// x_n*x_n' is a 3*numFrames x 3*numFrames matrix
			//a robust faster variant would be to take the EVs of the 4 smallest evs and
			//compute the ortho complement.

			//printStats("traj_matrix", __LINE__, traj_matrix);

			printStats("p_k_giv_n", __LINE__, p_k_giv_n);
			for (int k = 0; k < num_labels; k++)
			{

				Eigen::MatrixXf sum_xnxn = Eigen::MatrixXf::Zero(3 * num_frames, 3 * num_frames);
				for (int traj = 0; traj < num_trajs; traj++)
				{
					sum_xnxn = sum_xnxn + p_k_giv_n(k, traj) * traj_matrix.col(traj) * traj_matrix.col(traj).transpose();
				}

				//VARIANT: use arpack to only extract the five largest EVS as this is enough.
				//[V, D] = eigs(sum_xnxn, 5);

				//printStats("matrix to EVD: sum_xnxn", __LINE__, sum_xnxn);


				Eigen::SelfAdjointEigenSolver<Eigen::MatrixXf> eigensolver(sum_xnxn);
				if (eigensolver.info() != Eigen::Success){
					std::cout << "\nWarning! EMclustering: eigensolver failed.\n";
					std::cout << "Tried svd on \n";
					printStats("sum_xnxn", __LINE__, sum_xnxn, true);
					waitForCMDok(-1, true, true);
					std::cout << sum_xnxn << "\n";
					std::cout << "Further stats:\n";
					printStats("p_k_giv_n", __LINE__, p_k_giv_n, true);
					printStats("p_n_giv_k", __LINE__, p_n_giv_k, true);
					printStats("p_k", __LINE__, p_k, true);
					printStats("factor", __LINE__, factor, true);
					printStats("factor_pow2", __LINE__, factor_pow2	, true);
					printStats("factor_orig", __LINE__, factor_orig, true);
					printStats("factor_pow2_orig", __LINE__, factor_pow2_orig, true);
					printStats("Dk_n[k]", __LINE__, Dk_n[k], true);
					printStats("Sigma_k[k]", __LINE__, Sigma_k[k], true);
					waitForCMDok(-1, true, true);
				}
				//in ascending order
				//std::cout << "The eigenvalues of A are:\n" << eigensolver.eigenvalues() << "\n";
				//printStats("eigenvalues", __LINE__, eigensolver.eigenvalues());

				//~ Bk_ortho = null(sum_xnxn), the evs are in ascending order.
				B_ortho[k] = eigensolver.eigenvectors().topLeftCorner(3 * num_frames, 3 * num_frames - num_dim_B);

				//std::cout << "The eigenvalues of label " << k << " are:\n";//<< eigensolver.eigenvalues().transpose() << "\n";
				if (debug_step_by_step&&false)
					matlabPrint(std::string("evs_") + std::to_string(k), eigensolver.eigenvalues().transpose());

				printStats(std::string("B_ortho[") + std::to_string(k) + "]", __LINE__, eigensolver.eigenvalues());
			}

			//update pk by marginalization
			p_k = (p_k_giv_n.rowwise().sum()) *1.0 / num_trajs;

			//update the Dk_n: projection of the trajectories on the space ortho to Bk
			//std::vector<Eigen::MatrixXf> Dk_n(num_labels, Eigen::MatrixXf::Zero(3 * num_frames - 4, num_trajs));
			for (int k = 0; k < num_labels; k++)
			{
				//numbases x numTraj =		numBases x numFrame*3		numFrames*3xnumTrajs
				Dk_n.at(k) =				B_ortho[k].transpose()		*traj_matrix;
				//std::cout << "The weighted projection means for label " << k << " are \n";
				//			1xnumTraj			
				//std::cout << (p_k_giv_n.row(k)*Dk_n.at(k).transpose())*(1 / p_k_giv_n.row(k).sum()) << "\n";
				if (debug_step_by_step && false)
				{
					matlabPrint(std::string("D_projection_means_label") + std::to_string(k), (p_k_giv_n.row(k)*Dk_n.at(k).transpose())*(1 / p_k_giv_n.row(k).sum()));

					matlabPrint(std::string("D_projection_means_all_label") + std::to_string(k), (Dk_n.at(k).rowwise().sum().transpose()));
				}
			}

			

			//update sigma
			for (int k = 0; k < num_labels; k++)
			{
				int dim_complement = B_ortho[k].cols();
				Eigen::MatrixXf sigma = Eigen::MatrixXf::Zero(dim_complement, dim_complement);
				//auto & pk_n = p_k_giv_n(k, :);
				for (int traj = 0; traj < num_trajs; traj++)
				{
					sigma = sigma + p_k_giv_n(k, traj)*Dk_n[k].col(traj)*Dk_n[k].col(traj).transpose();
				}
				
				//Sigma_k[k] = (sigma / p_k_giv_n.row(k).sum());
				if (try_smooth_sigma_update){
					Sigma_k[k] = try_smooth_sigma_update_alpha_old*Sigma_k[k].array() + (1 - try_smooth_sigma_update_alpha_old)*sigma.array() / p_k_giv_n.row(k).sum();
				}
				else{
					Sigma_k[k] = (sigma / p_k_giv_n.row(k).sum());
				}
			}

			/*std::cout << "Example sig after N step\n";
				std::cout << Sigma_k[0] << "\n";*/

			/*std::cout << "\nb0 ortho stats updated \n" <<
				B_ortho[0].maxCoeff() << ", " << B_ortho[0].minCoeff() << ", " << B_ortho[0].mean() << "\n";*/

			//printStats("Sigma_k[0]", __LINE__, Sigma_k[0]);
			//printStats("B_ortho[0]", __LINE__, B_ortho[0]);
			//-------------- M step ------------------->>>>>>

			//here: visualize clusters!
			//visualize clustering.//wait
			{
				for (int traj = 0; traj < num_trajs; traj++){
					if (vis_n_giv_k){
						p_n_giv_k.row(traj).maxCoeff(&(hardLabels[traj]));
					}
					else{
						p_k_giv_n.col(traj).maxCoeff(&(hardLabels[traj]));
					}

				}
				Q_EMIT(documentLabeledTrajectories(false, ""));
				std::cout << "Label distribution\n" << p_k.transpose() << "\n";
				waitForCMDok(num_frames, false);

				if (debug_step_by_step)
				{
					waitForCMDok(-1, true, true);
					for (int i_label = 0; i_label < num_labels; i_label++)
					{
						std::cout << "p_k_giv_n for label" << i_label << "\n";
						Q_EMIT(documentWeightedTrajectories_row(false, p_k_giv_n, i_label, std::string("p_k_giv_n_label") + std::to_string(i_label) + "_it" + std::to_string(emIteration)));
						//std::string("frm") + std::to_string(trajectories.size() - 1) + "pngivk_" + std::to_string(label_alpha) +
						//+"vs" + std::to_string(label_beta)));
						waitForCMDok(-1, true, true);
					}
					for (int i_label = 0; i_label < num_labels; i_label++)
					{
						std::cout << "p_n_giv_g for label" << i_label << "\n";
						Q_EMIT(documentWeightedTrajectories_col(false, p_n_giv_k, i_label, std::string("p_n_giv_k_label") + std::to_string(i_label) + "_it" + std::to_string(emIteration)));
						waitForCMDok(-1, true, true);
					}
				}
			}
		}

		//<------------- Adding clusters ----------------
		//via a grubbs like test. this is a bit hacky and unsure....

		//1. compute distances normalized by sigma. take the max distance.
		Eigen::VectorXf weighted_dist = Eigen::VectorXf::Zero(num_trajs);
		for (int k = 0; k < num_labels; k++)
		{
			//VARIANT: don't compute inverse....
			Eigen::MatrixXf sig_k_inv = Sigma_k[k].inverse();
			for (int traj = 0; traj < num_trajs; traj++)
			{
				//max of absolute of(3 * numFrames - 4) normalized distances
				weighted_dist(traj) = weighted_dist(traj) + p_k_giv_n(k, traj)*
					std::pow((Dk_n[k].col(traj).array()*(sig_k_inv* Dk_n[k].col(traj)).array()).abs().maxCoeff(), 0.5);
			}
		}

		int n = num_trajs;
		float t_ = 4.3; //for gurpp test should be inv of student t tinv(1 - 0.05 / 2 / n, n - 2); % 10;
		float confidence_value = (n - 1) / std::pow(n, 0.5)*std::sqrt(t_ *t_ / (n - 2 + t_ *t_));

		Eigen::VectorXf additionalConstraint = Eigen::VectorXf::Zero(num_trajs);
		auto is_outlier = [&weighted_dist, &confidence_value, &additionalConstraint](int i)->bool{
			return weighted_dist(i) > confidence_value && additionalConstraint(i) < 0.1; };
		//subplot(1, 3, 3); hold off; plot(weighted_dist); hold on; plot(ones(size(weighted_dist))*confidence_value); title('Outliertest')

		printStats(std::string("for outlier detection: >") + std::to_string(confidence_value), __LINE__, weighted_dist, true);

		// Adding an outlier cluster: need to reestimate
		//	D_k_n, B_k_orth and Sigma_k and p_k.
		//  we init p_k_giv_n(newLable, is_oulier) = 1 for outliers, zero else. From this
		//  B_k_ortho is computed - as well as D_k_n, sigma_k and p_k

		int numOutliers = 0;
		for (int traj = 0; traj < num_trajs; traj++){
			numOutliers += (is_outlier(traj) ? 1 : 0);
		}

		//when to add the new hyp
		if (numOutliers > 15 && keep_on_emIng)
		{

			std::cout << "Found" << numOutliers << " outliers ";
			// guard against too many outliers...
			bool too_many_outliers = false;
			additionalConstraint = Eigen::VectorXf::Zero(num_trajs);
			for (int labl = 0; labl < num_labels; labl++)
			{
				float sum_pk_giv_n_labl = 0; float sum_pk_giv_n_labl_outlier = 0;
				for (int traj = 0; traj < num_trajs; traj++){
					sum_pk_giv_n_labl += p_k_giv_n(labl, traj);
					sum_pk_giv_n_labl_outlier += (is_outlier(traj) ? p_k_giv_n(labl, traj) : 0);
				}
				if (sum_pk_giv_n_labl_outlier > 0.5 *sum_pk_giv_n_labl)
				{
					too_many_outliers = true;
				}
			}

			std::cout << "Adding and outlier label with %d outliers \n";

			
			//prepare new model for subspace
			Eigen::RowVectorXf p_k_giv_n_new = Eigen::RowVectorXf::Zero(p_k_giv_n.cols());
			Eigen::MatrixXf b_ortho_new;
			Eigen::MatrixXf D_k_new, sigma_k_new;

			//new p_k_giv_�n
			for (int traj = 0; traj < num_trajs; traj++){
				if (is_outlier(traj))
				{
					p_k_giv_n_new(traj) = (too_many_outliers ? 0.5 : 1);
				}
			}

			//compute new subspace B_k, projections Dkn, as in M step
			Eigen::MatrixXf sum_xnxn = Eigen::MatrixXf::Zero(3 * num_frames, 3 * num_frames);
			for (int traj = 0; traj < num_trajs; traj++)
			{
				sum_xnxn = sum_xnxn + p_k_giv_n_new(traj) * traj_matrix.col(traj) * traj_matrix.col(traj).transpose();
			}
			Eigen::SelfAdjointEigenSolver<Eigen::MatrixXf> eigensolver(sum_xnxn);
			if (eigensolver.info() != Eigen::Success){
				std::cout << "\nWarning! EMclustering: eigensolver failed.\n";
			}

			b_ortho_new = eigensolver.eigenvectors().topLeftCorner(3 * num_frames, 3 * num_frames - num_dim_B);
			D_k_new = b_ortho_new.transpose()*traj_matrix;

			//new sigma:
			int dim_complement = b_ortho_new.cols();
			sigma_k_new = Eigen::MatrixXf::Zero(dim_complement, dim_complement);
			for (int traj = 0; traj < num_trajs; traj++)
			{
				sigma_k_new = sigma_k_new + p_k_giv_n_new(traj)*D_k_new.col(traj)*D_k_new.col(traj).transpose();
			}
			sigma_k_new.array() /= p_k_giv_n_new.sum();
			sigma_k_new += Eigen::MatrixXf::Identity(dim_complement, dim_complement) *sigma_k_new.maxCoeff() * sigma_eps;


			//now add the new model:
			float minDistToExistingSpaces = minimalMaxPrincipleAngle(b_ortho_new, B_ortho);
			std::cout << "minimal distance to existing spaces: " << minDistToExistingSpaces << "\n";
			if (minDistToExistingSpaces > 0.004)
			{
				std::cout << "Displaying outliers:\n";
				Eigen::MatrixXf bla = (weighted_dist.array() < confidence_value).select(weighted_dist * 0, 1);
				Q_EMIT(documentWeightedTrajectories_col(false, bla, 0, std::string("spawning_outliers_") + std::to_string(num_frames - 1) + "_" + std::to_string(num_labels - 1)));

				int newLabel = num_labels;
				p_k_giv_n.conservativeResize(p_k_giv_n.rows() + 1, p_k_giv_n.cols());
				p_k_giv_n.row(newLabel).fill(0);
				for (int traj = 0; traj < num_trajs; traj++){
					if (is_outlier(traj) && too_many_outliers)
					{
						p_k_giv_n.col(traj) /= 2;
					}
					else if (is_outlier(traj)){
						p_k_giv_n.col(traj).fill(0);
					}
				}
				p_k_giv_n.row(newLabel) = p_k_giv_n_new;
				B_ortho.push_back(b_ortho_new);
				Dk_n.push_back(D_k_new);
				Sigma_k.push_back(sigma_k_new);
				//p_k
				p_k = (p_k_giv_n.rowwise().sum()) *1.0 / num_trajs;
				num_labels = num_labels + 1;
				num_labels_added_now++;
			}
			else{
				std::cout << "Outlier label rejected, will exit E;\n";
				has_unoptimized_labels = false;
			}

			/*int newLabel = num_labels;
			p_k_giv_n.conservativeResize(p_k_giv_n.rows() + 1, p_k_giv_n.cols());
			p_k_giv_n.row(newLabel).fill(0);
			for (int traj = 0; traj < num_trajs; traj++){
				if (is_outlier(traj) && too_many_outliers)
				{
					p_k_giv_n.col(traj) /= 2;
					p_k_giv_n(newLabel, traj) = 0.5;					
				}
				else if (is_outlier(traj)){
					p_k_giv_n.col(traj).fill(0);
					p_k_giv_n(newLabel, traj) = 1;
				}
			}
			

			B_ortho.push_back(Eigen::MatrixXf());
			B_ortho[newLabel] = eigensolver.eigenvectors().topLeftCorner(3 * num_frames, 3 * num_frames - num_dim_B);
			Dk_n.push_back(Eigen::MatrixXf());
			Dk_n.at(newLabel) = B_ortho[newLabel].transpose()*traj_matrix;

			//new sigma
			int dim_complement = B_ortho[newLabel].cols();
			Eigen::MatrixXf sigma = Eigen::MatrixXf::Zero(dim_complement, dim_complement);
			for (int traj = 0; traj < num_trajs; traj++)
			{
				sigma = sigma + p_k_giv_n(newLabel, traj)*Dk_n[newLabel].col(traj)*Dk_n[newLabel].col(traj).transpose();
			}
			Sigma_k.push_back(Eigen::MatrixXf());
			Sigma_k[newLabel] = (sigma / p_k_giv_n.row(newLabel).sum());
			//for pos deftness
			Sigma_k[newLabel] += Eigen::MatrixXf::Identity(dim_complement, dim_complement) *Sigma_k[newLabel].maxCoeff() * sigma_eps;

			

			//update the models.
			//p_k
			p_k = (p_k_giv_n.rowwise().sum()) *1.0 / num_trajs;

			//only if label is to be added!Double checking is missing
			num_labels = num_labels + 1;
			num_labels_added_now++;
			*/


			//<------------- Testing angles between clusters.
			//std::cout << "Display outliers\n";
			/*
			//std::string("frm") + std::to_string(trajectories.size() - 1) + "pngivk_" + std::to_string(label_alpha) +
			//+"vs" + std::to_string(label_beta)));
			waitForCMDok(-1, true, true);

			Eigen::MatrixXf B_k, B_l;
			for (int j = 0; j < num_labels-1 ; j++)
			{
				//for (int jj = j; jj < num_labels; jj++)
				//{

					B_k = B_ortho[j].transpose();
					B_k = null_space(B_k);
					B_l = B_ortho[num_labels].transpose();
					B_l = null_space(B_l);

					//compute angle between them all:
					Eigen::JacobiSVD<Eigen::MatrixXf> svd(B_k.transpose()*B_l);
				
					float minimal_cosine = svd.singularValues().minCoeff();
					float sin_max_angular_dist = std::sqrt(std::abs(1 - minimal_cosine*minimal_cosine));
					//std::cout << "Distance: " << j << "<->" << jj<< ": " << sin_max_angular_dist << "\n";
					std::cout << "Distance: " << j << "<->" << num_labels -1 << ": " << sin_max_angular_dist << "\n";

					if (sin_max_angular_dist < 0.004){
						std::cout << "Redundant label, should not be added!";
					}
				//}

			}
			waitForCMDok(-1, true, true);*/
			//---------testing angles done ------------->

		}
		else
		{
			std::cout << "No outliers found, exiting EM\n";
			has_unoptimized_labels = false; //break our of iteration
		}

		//visualize clustering.//wait
		for (int traj = 0; traj < num_trajs; traj++){
			if (vis_n_giv_k){
				p_n_giv_k.row(traj).maxCoeff(&(hardLabels[traj]));
			}
			else{
				p_k_giv_n.col(traj).maxCoeff(&(hardLabels[traj]));
			}
			//hardLabels[traj] = 0;
		}
		Q_EMIT(documentLabeledTrajectories(false, ""));
		std::cout << "Label distribution\n" << p_k.transpose() << "\n";//p_k_giv_n.rowwise().sum().transpose() << "\n";
		waitForCMDok(num_frames, false);
		//Q_EMIT(documentTrajectories());


	
	}

	trajectory_p_traj_giv_label = p_n_giv_k;
	
	Q_EMIT(documentLabeledTrajectories(false, std::string("postclustering_upto_") + std::to_string(num_frames-1)));
	std::cout << "Finished EM setp! Label distribution\n" << p_k_giv_n.rowwise().sum().transpose() << "\n";
	waitForCMDok(num_frames, false);
	//------------- Adding clusters ---------------->



}

float ExperimentIros::minimalMaxPrincipleAngle(Eigen::MatrixXf & new_space_ortho, std::vector<Eigen::MatrixXf> & old_spaces_ortho)
{
	Eigen::MatrixXf B_k, B_l;

	B_l = new_space_ortho.transpose();
	B_l = null_space(B_l);

	float minimal_dist = 1;
	for (int j = 0; j < old_spaces_ortho.size(); j++)
	{
		B_k = old_spaces_ortho[j].transpose();
		B_k = null_space(B_k);

		//compute angle between them all:
		Eigen::JacobiSVD<Eigen::MatrixXf> svd(B_k.transpose()*B_l);

		float minimal_cosine = svd.singularValues().minCoeff();
		float sin_max_angular_dist = std::sqrt(std::abs(1 - minimal_cosine*minimal_cosine));

		minimal_dist = (minimal_dist < sin_max_angular_dist ? minimal_dist : sin_max_angular_dist);
		//std::cout << "Distance: " << j << "<->" << jj<< ": " << sin_max_angular_dist << "\n";
		std::cout << "Distance: " << j << "<->" << old_spaces_ortho.size() << ": " << sin_max_angular_dist << "\n";

	}
	return std::asin(minimal_dist);
}

void ExperimentIros::printStats(std::string name, int line, Eigen::MatrixXf mat, bool doit){
	if (doit)
	{
		std::cout << name << "(line " << line << "):mx, mn, avg\t\t" <<
			mat.maxCoeff() << ", " << mat.minCoeff() << ", " << mat.mean() << "\n";
	}
}

void ExperimentIros::gcOptForLabels(){

	//setup the nn graph via the knn in euclidean space
	//or rather only based on image coordinates (probably closer to paper)
	bool setupNNviaCoordinates = true;
	bool use_directive_distance = false; //how to compute the distance between two trajs. avg posistion dist or average motion dist.
	bool normalize_p_per_trajectory = false; //false is better

	if (normalize_p_per_trajectory)
	{
		for (int traj = 0; traj < trajectories[0]->size(); traj++)
		{
			trajectory_p_traj_giv_label.row(traj) /= (1e-6 + trajectory_p_traj_giv_label.row(traj).sum());
		}
	}
	else
	{
		trajectory_p_traj_giv_label.array() /= trajectory_p_traj_giv_label.maxCoeff();
	}
	//*/
	//do alpha-beta swap:
	//should do this until (near) convergence, not fixed amount of times.
	auto & w_tr_l = trajectory_p_traj_giv_label;
	int num_labels = w_tr_l.cols();

	int k_nn = 16;

	auto & labeling = hardLabels; //used as initial guess (map result).

	float sigma_sqr_dist = 1e-2;
	float sigma_sqr_depth = 1e-2;


	float lambda_dterm=1, lamdba_sm=1;
	float avg_smoothness_strength = 0, avg_dterm_strength = 0;
	int num_dterm_strength, num_smoothness_strength;

	for (int it_alpha_beta_swap = 0; it_alpha_beta_swap <5; it_alpha_beta_swap++){
		for (int label_alpha = 0; label_alpha < num_labels - 1; label_alpha++)
		{
			for (int label_beta = label_alpha + 1; label_beta < num_labels; label_beta++){

				typedef Graph<float, float, float> GraphType;
				GraphType *g = new GraphType(trajectories[0]->size(), trajectories[0]->size() * k_nn);


				//data terms,
				avg_dterm_strength = 0; //stats for tweaking.
				num_dterm_strength = 0;
				for (int i = 0; i < trajectories[0]->size(); i++){
					if (labeling[i] == label_alpha || labeling[i] == label_beta){
						g->add_node();
						g->add_tweights(i, lambda_dterm* -logf(1e-10 + w_tr_l(i, label_alpha)),
							lambda_dterm* -logf(1e-10 + w_tr_l(i, label_beta)));

						avg_dterm_strength += -logf(1e-10 + w_tr_l(i, label_beta)) + -logf(1e-10 + w_tr_l(i, label_alpha));
						if (avg_dterm_strength * 0 != 0 || avg_dterm_strength < 0){
							std::cout << "Weirdness in dataterm: #ind\n"<<
								w_tr_l(i, label_beta) << "," << w_tr_l(i, label_alpha) << "\n";
							waitForCMDok(-1, true, true);
						}
						num_dterm_strength++;
					}
					else{
						g->add_node();
						//idc
						g->add_tweights(i, 1.f, 0.f);
					}
				}
				avg_dterm_strength /= num_dterm_strength;

				//pairwise terms and graph structure.
				mySimpleKdTree kdTree;
				auto  d= data->getDescription();
				//
				//set to coordinates.
				if (setupNNviaCoordinates){
					simpleTreeTools::set2dData<explainMe::Cloud::Ptr>(kdTree, trajectories[0], d);
				}
				else{
					kdTree.setData<explainMe::Cloud::Ptr>(trajectories[0]);
				}
				simpleNNFinder nn(kdTree);
				std::vector<distSqr_idx> dist_sqr_idx;
				std::vector<float> smoothnesses(k_nn+1,0);

				auto &points = trajectories[0]->points;
				pxyz query;
				//<--stats---
				num_smoothness_strength = 0;
				avg_smoothness_strength = 0;
				///--->
				for (int i = 0; i < trajectories[0]->size(); i++){
					if (setupNNviaCoordinates){
						simpleTreeTools::to_2d_point<explainMe::Point>(d, points[i], query);
					}
					else{
						query.x = points[i].x;
						query.y = points[i].y;
						query.z = points[i].z;
					}
					//the query point is part of the kdtree an will be rturned.
					nn.findkNN(query, k_nn + 1, dist_sqr_idx);

					
					for (int j = 0; j < dist_sqr_idx.size(); j++){
						//ignore the point if it is itself.
						if (dist_sqr_idx[j].index == i ||
							!(labeling[i] == label_alpha || labeling[i] == label_beta) ||
							!(labeling[dist_sqr_idx[j].index] == label_alpha || labeling[dist_sqr_idx[j].index] == label_beta)){
							continue;
						}
						/*float p_same = sigma_dist / (sigma_dist + dist_sqr_idx[j].dist_sqr);
						p_same *= points_frame[i].getNormalVector3fMap().dot(points_frame[dist_sqr_idx[j].index].getNormalVector3fMap());
						p_same = -std::logf(std::max(1 - p_same, 0.f) + 1e-10);*/
						float smoothness = 0;
						int nbr_idx = dist_sqr_idx[j].index;
						float dist_traj = 0;
						float dist_depth = 0;
						for (int frm = 0; frm < trajectories.size(); frm++){

							if (use_directive_distance)
							{
								if (frm > 0){
									dist_traj += (trajectories[frm]->at(i).getVector3fMap() - trajectories[frm - 1]->at(i).getVector3fMap()
										- trajectories[frm]->at(nbr_idx).getVector3fMap() + trajectories[frm - 1]->at(nbr_idx).getVector3fMap()).squaredNorm();
								}
							}
							else{
								dist_traj += (trajectories[frm]->at(i).getVector3fMap() - trajectories[frm]->at(nbr_idx).getVector3fMap()).squaredNorm();
							}
							dist_depth += std::pow(trajectories[frm]->at(i).z - trajectories[frm]->at(nbr_idx).z, 2);
						}
						dist_traj /= trajectories.size();
						dist_depth /= trajectories.size();
						smoothness = std::exp(-dist_depth / sigma_sqr_depth) + std::exp(-dist_traj / sigma_sqr_dist);
						//smoothness = std::exp(-dist_depth / sigma_sqr_depth) * std::exp(-dist_traj / sigma_sqr_dist);
						smoothnesses[j] = smoothness + 1e-3;
					}

					
					//renormalize the smoothness term
					float max = 1;
					for (int j = 0; j < dist_sqr_idx.size(); j++){
						if (dist_sqr_idx[j].index == i ||
							!(labeling[i] == label_alpha || labeling[i] == label_beta) ||
							!(labeling[dist_sqr_idx[j].index] == label_alpha || labeling[dist_sqr_idx[j].index] == label_beta)){
							continue;
						}
						max = (max < smoothnesses[j] ? smoothnesses[j] : max);
					}
					
					
					for (int j = 0; j < dist_sqr_idx.size(); j++){
						if (dist_sqr_idx[j].index == i ||
							!(labeling[i] == label_alpha || labeling[i] == label_beta) ||
							!(labeling[dist_sqr_idx[j].index] == label_alpha || labeling[dist_sqr_idx[j].index] == label_beta)){
							continue;
						}
						float smoothness = smoothnesses[j]/max;
						g->add_edge(i, dist_sqr_idx[j].index, smoothness, smoothness);
						avg_smoothness_strength += smoothness;
						num_smoothness_strength++;
					}
					
				}
				avg_smoothness_strength /= num_smoothness_strength;

				std::cout << "GC swap step " << label_alpha << "-" << label_beta << "\n";
				std::cout << "avg dataterm magnitude: " << avg_dterm_strength << "avg smoothness magnitude: " << avg_smoothness_strength << "\n";

				std::cout << "done\n Running graphcut...";
				//max flow, min cut,
				float flow = g->maxflow();
				printf("Flow = %f\n", flow);

				//update labels..
				for (int i = 0; i < labeling.size(); i++){
					if (!(labeling[i] == label_alpha || labeling[i] == label_beta)){
						continue;
					}
					if (g->what_segment(i) == GraphType::SOURCE){
						labeling[i] = label_beta;
					}
					else{
						labeling[i] = label_alpha;
					}
				}

				delete g;

				
				//Q_EMIT(documentLabeledTrajectories(false, ""));
				//waitForCMDok(-1, true, true);
				// visualize -log(w_tr_l) for both label_alph and label_beta
				if (it_alpha_beta_swap == 0)
				{
					Eigen::MatrixXf used_weights = -(w_tr_l.array() + 1e-10).log();// -std::logf(1e-10 + w_tr_l(i, label_alpha)
					Q_EMIT(documentWeightedTrajectoriesVS(false, used_weights, label_alpha, label_beta,
						std::string("frm") + std::to_string(trajectories.size() - 1) + "pngivk_" + std::to_string(label_alpha) +
						+"vs" + std::to_string(label_beta)));
					//waitForCMDok(-1, true, true);
				}

			} //end graphcut for one pair
		}//end loop over first label
	}//end loop of alphabeta swaps.


	std::cout << "Graphcut complete...\n";

	//kill small labels, that lead to erroneous motions and bad EM....
	std::vector<int> supports(num_labels, 0);
	std::vector<int> map_found_labels_to_consecutive_range(num_labels, 0);
	for (int l : labeling){
		supports[l]++;
	}
	std::cout << "Label supportsizes: ";
	int new_l = 0;
	for (int i = 0; i < num_labels; i++){
		if (supports[i] > 20){
			map_found_labels_to_consecutive_range[i] = new_l;
			new_l++;
		}
		std::cout << supports[i] << ", ";
	}

	std::cout << "Killing small labels...\n";
	for (int i = 0; i < hardLabels.size(); i++){
		hardLabels[i] = map_found_labels_to_consecutive_range[hardLabels[i]];
	}

	std::cout << "\n";

	//on the set of trajectories. 4-5 nieghbors
	Q_EMIT(documentLabeledTrajectories(false, std::string("postgc_upto_") + std::to_string(trajectories.size()-1)));
	//Q_EMIT(documentLabeledTrajectories(false, ""));
}
//motion estimation and analysis
void ExperimentIros::motionEstimation(int frame){

	//update correspondences and motion between frame -1 and frame, using the full frame frame
	//and the sampled points from frame-1

	//=> refactor into method.
	BundleAdjustmentV2<Eigen::MatrixXf> ba(NULL);
	BundleAdjustmentV2<Eigen::MatrixXf>::params params;
	BundleAdjustmentV2<Eigen::MatrixXf>::data data;

	params.last_error = std::numeric_limits<float>::max();
	params.lambda = 1;
	params.alpha_point2point = 0.1;
	params.alpha_point2plane = 1;
	params.alpha_color = 1.0 / 255;
	params.culling_dist = 3e-1;
	params.numIterations = 100;
#ifdef _DEBUG
	params.numIterations = 10;
#endif

	//TODO adapt: last cloud should prob be full cloud.
	bool resolve_for_motions_only_based_on_given_corresps = false;
	bool register_against_full_clouds = !resolve_for_motions_only_based_on_given_corresps;


	bool reproject_trajectories_onto_clouds_after_update = true;
	//alignment_between.push_back(trajectories[frame - 1]);
	//alignment_between.push_back(trajectories[frame]);
	//segmentation.push_back(segmentation_as_weights);
	//segmentation.push_back(segmentation_as_weights);

	float motion_analysis_noise_sig = 1e-5; //for dummy data the epsilon to compute a robust rank was related to the noise on the motions.

	//current num labels.
	int num_labels = 0;
	for (int l : hardLabels){
		num_labels = std::max(l + 1, num_labels);
	}

	// newmotion[label][frame]
	/*std::vector<std::vector<Eigen::Matrix4f>> new_motions;
	for (int l = 0; l < num_labels; l++){
		new_motions.push_back(std::vector<Eigen::Matrix4f>());
		for (int k = 0; k < frame + 1; k++)
		{
			new_motions[l].push_back(Eigen::Matrix4f::Identity());
		}
	}*/

	// newmotion[frame][label]
	std::vector<std::vector<Eigen::Matrix4f>> new_motions_f_l;
	for (int k = 0; k < frame + 1; k++)
	{
		new_motions_f_l.push_back(std::vector<Eigen::Matrix4f>());
		for (int l = 0; l < num_labels; l++)
		{
			new_motions_f_l[k].push_back(Eigen::Matrix4f::Identity());
		}
	}

	if (resolve_for_motions_only_based_on_given_corresps){

		Eigen::MatrixXf segmentation_as_weights = Eigen::MatrixXf::Zero(numTrajectories(), num_labels);
		for (int i = 0; i < hardLabels.size(); i++){
			segmentation_as_weights(i, hardLabels[i]) = 1;
		}

		std::vector<Eigen::MatrixXf> segmentation;
		std::vector<explainMe::Cloud::Ptr> alignment_between;
		for (int i = 0; i < frame + 1; i++){
			segmentation.push_back(segmentation_as_weights);
			alignment_between.push_back(trajectories[i]);
		}


		//alignment_between.push_back(allClouds[frame]);

		data.clouds = alignment_between;
		data.referenceCloud = 0;
		data.weights = segmentation;
		data.d = NULL;
		for (int label = 0; label < num_labels; label++){
			//std::vector<Eigen::Matrix4f>  als;
			//auto & als = new_motions[label];

			std::vector<Eigen::Matrix4f> als;
			for (int i = 0; i < new_motions_f_l.size(); i++){
				als.push_back(new_motions_f_l[i][label]);
			}
			//to allow an arbitrary reference frame: the first matrix
			//maps the reference frame to the first cloud, the second matrix
			//is an initial guess and solved for.
			//als.push_back(Eigen::Matrix4f::Identity());
			//als.push_back(Eigen::Matrix4f::Identity());

			ba.pointToPlane_incremental(params, data, label, als);

			std::cout << "Bundle adjusted weight set " << label << " numIt: " << params.numIterations << "...\n";
			std::cout << als[1] << "\n";

			for (int i = 0; i < new_motions_f_l.size(); i++){
				new_motions_f_l[i][label] = als[i];
			}
		}
	}
	else if (register_against_full_clouds)
	{
		Eigen::MatrixXf segmentation_as_weights = Eigen::MatrixXf::Zero(numTrajectories(), num_labels);
		for (int i = 0; i < hardLabels.size(); i++){
			segmentation_as_weights(i, hardLabels[i]) = 1;
		}

		for (int f = 1; f < frame + 1; f++){
			for (int label = 0; label < num_labels; label++){
		//for (int label = 0; label < num_labels; label++){
		//	for (int f = 1; f < frame + 1; f++){
				//new_motions[label][f] = new_motions[label][f - 1];
				new_motions_f_l[f][label] = new_motions_f_l[f - 1][label];
								
				Eigen::VectorXf weights = segmentation_as_weights.col(label);
				ba.pointToPlane_first2second(params, trajectories[f - 1], allClouds[f], weights,
					new_motions_f_l[f - 1][label], new_motions_f_l[f][label]
					);

				std::cout << "Bundle adjusted weight set " << label << " frame: " << f << "...\n";
				std::cout << new_motions_f_l[f][label] << "\n";
				//new_motions[label][f] = als[1];

				//step by step visualization for dbg				
				//updateTrajectories_fromLabelsAndMos_between(new_motions_f_l,f-1, f, true);
				//Q_EMIT(documentLabeledTrajectories(false, ""));
				//std::cout << "Motion of label " << label << "to frame " << f <<"\n";
				//waitForCMDok(-1,true,true);
			}
			//update the correspondences in frame!
			updateTrajectories_fromLabelsAndMos_between(new_motions_f_l, f - 1, f, true);
		}
	}

	//waitForCMDok();
	//then do motion analysis. makes only sense with more than 12 frames

	//TODO refactor away.
	// newmotion[label][frame]
	std::vector<std::vector<Eigen::Matrix4f>> new_motions;
	for (int l = 0; l < num_labels; l++){
		new_motions.push_back(std::vector<Eigen::Matrix4f>());
		for (int k = 0; k < frame + 1; k++)
		{
		new_motions[l].push_back(new_motions_f_l[k][l]);
		}
	}


	/*if (trajectories.size() > 12){
		for (int i = 0; i < new_motions.size(); i++)
		{
			motionAnalysis analysis(new_motions[i], motion_analysis_noise_sig);
		}
		waitForCMDok(frame);
		for (int i = 0; i < new_motions.size(); i++)
		{
			
			for (int j = 0; j < new_motions.size(); j++)
			{
				std::cout << "Motion " << i << " vs " << j << "...\n";
				motionAnalysis analysis(new_motions[i], new_motions[j], motion_analysis_noise_sig);
			}
			waitForCMDok(frame);
		}
	}*/

	//then repeat

	//then update all motions-
	currentMotions.resize(frame + 1);
	for (int f = 0; f < frame + 1; f++){
		currentMotions[f].resize(num_labels);
		for (int l = 0; l < num_labels; l++){
			currentMotions[f][l] = new_motions[l][f];
		}
	}


	//and finally update the correspondences---
	updateTrajectories_fromLabelsAndMos_between(currentMotions,0, frame, reproject_trajectories_onto_clouds_after_update);
	Q_EMIT(documentLabeledTrajectories(false, ""));
	std::cout << "Finished motion estimation step, updated corresps...\n";
	waitForCMDok(frame);

}

void ExperimentIros::run_with_stats()
{
	bool restore_for_debug = false;
	bool stepThroughAlg = false;
	std::cout << "Running the IROS16 code. Loading data\n";
	
	loadAllClouds();

	if (restore_for_debug){
		std::cout << "Restoring...";
		stepThroughAlg = true;

		std::ostringstream stream;
		stream << outputFolder << "02_01plus_fixedErrorInRegistration" << "/_up_to_" << 26 << ".iros16";
		std::string tmp_str = stream.str();
		restore(tmp_str);
		std::cout << "done\n";
		std::string loaded_string("Loaded_");
		Q_EMIT(documentLabeledTrajectories(false, loaded_string));
	}
	else{
		std::cout << "Sampling points in first cloud....";
		sampleCloud0();
		//These are used as basepoints for the trajectories.
		initTrajectories(sampledPoints_zero);
		Q_EMIT(documentSampledPoints());
	}
	std::cout << "rendering sampled cloud...";
	


	

	int first_frame = start_with_frame;

	for (int frame = first_frame; frame < allClouds.size();frame++){
		//find NNs, and for the current labeling estimate an alignment to this new frame, as
		//initial transformation guess
		extendTrajectories(frame); 

		Q_EMIT(documentLabeledTrajectories(false, ""));
		std::cout << "Extended Trajectories...\n";
		waitForCMDok(frame, stepThroughAlg, stepThroughAlg);

		
		EMsubspaceClustering();//currently sets the hard labeling to the map result.
		waitForCMDok(frame, stepThroughAlg, stepThroughAlg);
		gcOptForLabels(); //currently just use the map result. 
		waitForCMDok(frame, stepThroughAlg, stepThroughAlg);

		motionEstimation(frame);

		Q_EMIT(documentLabeledTrajectories(false, std::string("postmotion_upto_") + std::to_string(frame)));
		std::cout << "Motions reestimated\n";
		waitForCMDok(frame, stepThroughAlg, stepThroughAlg);

		std::cout << "Storing...";
		std::ostringstream stream;
		stream << outputFolder << "_up_to_" << std::to_string(frame) << ".iros16";
		std::string tmp_string = stream.str();
		store(tmp_string);
		std::cout << "done.\n";

	}

	Q_EMIT(documentTrajectories());
	int a;
	std::cin >> a;

}

void ExperimentIros::waitForCMDok(int frame, bool wait, bool wait_caused_by_error)
{
	if (wait_caused_by_error )//|| wait && (frame > 11 && frame %20 == 0))
	{
		std::cout << "Hit a key to continue...\n";
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		int hmpf;
		std::cin >> hmpf;
	}
}


void IROSChronist::documentSampledPoints(){
	renderer->clear();
	QVector3D zero_vector(0, 0, 0);
	QVector3D e_vector_z(0, 0, 1);
	renderer->setCamera(zero_vector, e_vector_z);
	renderer->setUpRenderingForCloud(
		experimentToDocument->sampledPoints_zero);

	renderer->recordImage(outputFolder, std::string("sampledFrame0") + ".png");
	
}
void IROSChronist::documentCloud(explainMe::Cloud::Ptr cloud)
{
	//renderer->clear();
	//renderer->setCamera(QVector3D(0, 0, 0), QVector3D(0, 0, 1));
	renderer->setUpRenderingForCloud(cloud, false);
	renderer->recordImage(outputFolder, std::string("trajectories") + ".png");
}

void IROSChronist::documentTrajectories(){
	renderer->clear();
	QVector3D zero_vector(0, 0, 0);
	QVector3D e_vector_z(0, 0, 1);
	renderer->setCamera(zero_vector, e_vector_z);
	renderer->setUpRenderingForLines(experimentToDocument->trajectories);
	renderer->recordImage(outputFolder, std::string("trajectories") + ".png");
}

void IROSChronist::documentLabeledTrajectories(bool resetViewAndSize, std::string filename){
	renderer->clear(resetViewAndSize);
	if (resetViewAndSize){
		QVector3D zero_vector(0, 0, 0);
		QVector3D e_vector_z(0, 0, 1);
		renderer->setCamera(zero_vector, e_vector_z);
	}

	renderer->setUpRenderingForLines(experimentToDocument->trajectories, experimentToDocument->hardLabels, resetViewAndSize);
	if (filename.length()!=0)
		renderer->recordImage(outputFolder, filename + std::string("_traj") + ".png");

}

void IROSChronist::documentWeightedTrajectoriesVS(bool resetViewAndSize, 
	Eigen::MatrixXf & weights, int i, int j,std::string filename){

	Eigen::VectorXf col = weights.col(i) - weights.col(j);
	//TODO implement vis
	renderer->clear(resetViewAndSize);
	if (resetViewAndSize) {
		QVector3D zero_vector(0, 0, 0);
		QVector3D e_vector_z(0, 0, 1);
		renderer->setCamera(zero_vector, e_vector_z);
	}

	renderer->setUpRenderingForLines(experimentToDocument->trajectories, col, resetViewAndSize);
	if (filename.length() != 0)
		renderer->recordImage(outputFolder, filename + std::string("_traj") + ".png");

}


void IROSChronist::documentWeightedTrajectories_row(bool resetViewAndSize,
	Eigen::MatrixXf & weights, int i, std::string filename){

	Eigen::VectorXf col = weights.row(i).transpose() ;
	//TODO implement vis
	renderer->clear(resetViewAndSize);
	if (resetViewAndSize) {
		QVector3D zero_vector(0, 0, 0);
		QVector3D e_vector_z(0, 0, 1);
		renderer->setCamera(zero_vector, e_vector_z);
	}

	renderer->setUpRenderingForLines(experimentToDocument->trajectories, col, resetViewAndSize);
	if (filename.length() != 0)
		renderer->recordImage(outputFolder, std::string("weights_") + filename + ".png");

}

void IROSChronist::documentWeightedTrajectories_col(bool resetViewAndSize,
	Eigen::MatrixXf & weights, int i, std::string filename) {

	Eigen::VectorXf col = weights.col(i);
	//TODO implement vis
	renderer->clear(resetViewAndSize);
	if (resetViewAndSize){
		QVector3D zero_vector(0, 0, 0);
		QVector3D e_vector_z(0, 0, 1);
		renderer->setCamera(zero_vector, e_vector_z);
	}

	renderer->setUpRenderingForLines(experimentToDocument->trajectories, col, resetViewAndSize);
	if (filename.length() != 0)
		renderer->recordImage(outputFolder, std::string("weights_") + filename + ".png");

}

QIROS16Application::QIROS16Application(int argc, char ** arg,
	std::map<std::string, std::string> & cmd_line_str, //already parsed
	std::map<std::string, bool> & cmd_line_bool //already parsed
	) :QApplication(argc, arg)
{
	


	//lots of the parameters that potentially can be loaded are actually irrelevant for this method. 
	algorithmParameters params;
	experimentParameters exp_params;
	std::string outputFolder = cmd_line_str["output_file"];
	std::string parameterFile = cmd_line_str["experiment_file"];
	parameterLoader::loadFromFile(parameterFile, exp_params, params);
	CloudReader::Ptr data = exp_params.createReader();
	
	statsWindow * stats_win;
	experiment = ExperimentIros::Ptr(new ExperimentIros(data, params, outputFolder));
	chronist = IROSChronist::Ptr(new IROSChronist(experiment, outputFolder));

	//setting up documentation. Rendering is on a different thread than
	//computation, thus documentations happens via signals.
	connect(experiment.get(), SIGNAL(documentSampledPoints()),
		chronist.get(), SLOT(documentSampledPoints()),
		Qt::BlockingQueuedConnection);

	connect(experiment.get(), SIGNAL(documentTrajectories()),
		chronist.get(), SLOT(documentTrajectories()),
		Qt::BlockingQueuedConnection);

	connect(experiment.get(), SIGNAL(documentLabeledTrajectories(bool, std::string)),
		chronist.get(), SLOT(documentLabeledTrajectories(bool, std::string)),
		Qt::BlockingQueuedConnection);

	connect(experiment.get(), SIGNAL(documentCloud(explainMe::Cloud::Ptr)),
		chronist.get(), SLOT(documentCloud(explainMe::Cloud::Ptr)),
		Qt::BlockingQueuedConnection);


	connect(experiment.get(), SIGNAL(documentWeightedTrajectoriesVS(bool, Eigen::MatrixXf &, int, int, std::string)),
		chronist.get(), SLOT(documentWeightedTrajectoriesVS(bool ,Eigen::MatrixXf &, int, int, std::string)),
		Qt::BlockingQueuedConnection);
	
	connect(experiment.get(), SIGNAL(documentWeightedTrajectories_row(bool, Eigen::MatrixXf &, int, std::string)),
		chronist.get(), SLOT(documentWeightedTrajectories_row(bool, Eigen::MatrixXf &, int, std::string)),
		Qt::BlockingQueuedConnection);

	connect(experiment.get(), SIGNAL(documentWeightedTrajectories_col(bool, Eigen::MatrixXf &, int, std::string)),
		chronist.get(), SLOT(documentWeightedTrajectories_col(bool, Eigen::MatrixXf &, int, std::string)),
		Qt::BlockingQueuedConnection);

	auto future = statsWindow::setupLiveStatsAndDispatch<ExperimentIros>(*experiment, &stats_win);

	//stats_win->setGeneralInfo(parameterLoader::to_long_string(exp_params, params));


	//make sure ctrl-c works. It will set an abort flag and wait for the worker thread
	//to see the flag and quit.
	//catchInterruptSignals({ SIGINT, SIGTERM });
	//experiment_returns.setFuture(future);

	//keep the wins open at end, only close them on ctrl c
	//connect(&experiment_returns, SIGNAL(finished()), this, SLOT(closeAllWindowsIfExperimentWasAborted()));
	//always close them.
	//connect(&experiment_returns, SIGNAL(finished()), this, SLOT(closeAllWindows()));
}


