#pragma once
#include "glDisplayable.h"
#include "explainMeDefinitions.h"

class glTriMesh:
	public glDisplayable
{
private:
	QGLBuffer m_posBuffer, m_indexBuffer, m_colBuffer;
	std::vector<tuple3f> pos;
	std::vector<tuple3f> col;
	std::vector<int> ind;
	bool isUpToDate;
public:
	glTriMesh(const explainMe::Cloud::ConstPtr & cloud, std::vector<int> & inds);
	glTriMesh(std::vector<Eigen::Vector3f> & pos, std::vector<Eigen::Vector3f> &col,std::vector<int> & ind);

	~glTriMesh(void);

	void updateData(const explainMe::Cloud::ConstPtr & cloud, std::vector<int> & inds, bool useColor = true);
	void updateData(std::vector<Eigen::Vector3f> & pos, std::vector<Eigen::Vector3f> &col,std::vector<int> & ind);

	virtual void glPrepare();
	virtual bool isGlPrepared();
	virtual void glUpdate();
	virtual bool isGlUpToDate();
	virtual void glDraw();
};


