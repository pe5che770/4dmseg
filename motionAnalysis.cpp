#include "motionAnalysis.h"
#include <iostream>

motionAnalysis::signature motionAnalysis::computeMotionSignature()
{

	M = Eigen::MatrixXf::Zero(motion.size(), 12);
	Eigen::Matrix3f m_rot;
	Eigen::Vector3f t;
	for (int idx = 0; idx < motion.size(); idx++)
	{
		m_rot = motion[idx].topLeftCorner(3,3) - Eigen::Matrix3f::Identity();
		t = motion[idx].topRightCorner(3, 1);
		M.row(idx) << m_rot.col(0).transpose(), m_rot.col(1).transpose(), m_rot.col(2).transpose(), t.transpose();
	}

	if (motion.size() < 12){
		std::cout << "motion analysis not possible, need at least 12 alignments\n";
		return signature();
	}
	//
	//std::cout << "M...\n";
	//std::cout << M << "\n";
	Eigen::JacobiSVD<Eigen::MatrixXf> svd_M(M);
	Eigen::JacobiSVD<Eigen::MatrixXf> svd_M1to9(M.topLeftCorner(M.rows(),9));

	absolute_eps = std::min<float>(svd_M.singularValues().maxCoeff(), 1) * relative_eps;
	std::cout << "Absolute eps " << absolute_eps;
	std::cout << "Singular values M\n" << svd_M.singularValues().transpose() << "\n";
	std::cout << "Singular values M1to9\n" << svd_M1to9.singularValues().transpose() << "\n";

	int s_r = 0;
	//robust rank computation
	for (int i = 0; i < 9; i++){
		s_r += (svd_M1to9.singularValues()(i) > absolute_eps ? 1 : 0);
	}
	int s_t = 0;
	//robust rank computation
	for (int i = 0; i < 12; i++){
		s_t += (svd_M.singularValues()(i) > absolute_eps ? 1 : 0);
	}
	s_t = s_t - s_t;

	//set signature
	signature s_;
	s_.set(s_r, s_t);
	return s_;
}

void motionAnalysis::coutSignature(){
	//td::cout << "Signature: \t" << s.axes_r << "(" << s.s_r << ")" << s.s_t << "\n";
	coutSignature(s);
}

void motionAnalysis::coutSignature(motionAnalysis::signature & s_){
	std::cout << "Signature: \t" << s_.axes_r << "(" << s_.s_r << ")" << s_.s_t << "\n";
}