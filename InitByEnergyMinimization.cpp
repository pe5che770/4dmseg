#include "InitByEnergyMinimization.h"
#include "BundleAdjustment.h"
#include "mySimpleKdTree.h"

InitByEnergyMinimization::InitByEnergyMinimization(int numIt, float p1, float p2, 
	float stepSize, int numHyp, int numClouds, e_type energy)
{
	this->numIt = numIt;
	this->p1 = p1;
	this->p2 = p2;
	this->stepSize = stepSize;
	this->numHyp = numHyp;
	this->numClouds = numClouds;
	this->energyType = energy;
}

InitByEnergyMinimization::~InitByEnergyMinimization(){}



void InitByEnergyMinimization::findWeights(std::vector<trajectory_R> & trajectories, 
	explainMe::MatrixXf_rm & w)
{
	//kill this one:
	int reference_frame =0;
	
	
	w.resize(trajectories.size(), numHyp);
	d_dw_E = w;
	d_dw_E.fill(1.f/numHyp);
	

	//init the ws
	//init rand, but concentrated around ones/numHyp
	w.fill(10);
	srand((unsigned int) 99638);
	for(int i = 0; i < w.rows(); i++){
		w.row(i) += explainMe::RowVectorXf::Random(numHyp);
		w.row(i) = w.row(i).cwiseAbs();
		w.row(i) = w.row(i)/w.row(i).sum();
	}



	//the whole shabang.
	Eigen::Matrix4f id = Eigen::Matrix4f::Identity();
	//alignmentSet s(id, reference_frame);
	//s.resize(first_frame, last_frame,id);
	std::vector<Eigen::Matrix4f> s(numClouds, id);

	//T[i_w][i_f] is the alignment 
	//			from ref frame to i_f.
	//			corresponding to the weight set i_w
	std::vector<std::vector<Eigen::Matrix4f>> T;
	T.resize(numHyp,s);


	//for general reprojection:
	
	explainMe::RowVectorXf w_center = explainMe::RowVectorXf::Ones(numHyp)/numHyp, new_constraint;
	Eigen::MatrixXf constraints(numHyp, numHyp);// = Matrixnf::Zeros();
	Eigen::MatrixXf e = Eigen::MatrixXf::Identity(numHyp,numHyp);
	int min_coeff_idx; float total_moved,max_movement;


	for(int it_step = 0; it_step < numIt; it_step ++){
		if (abortComp){
			return;
		}
		std::cout << ".";
		//std::cout << it_step << "/" << numIt << "...";
		//compute alignments
		for(int hyp = 0; hyp < numHyp; hyp++){

			//used to reset alignments to identity. But might not need to.
			T[hyp] = s;

			//this does a fixed number of icp steps.
			//the reason that I don't use the closed formula is that alignments from 
			//the reference frame to each frame is computed. Not completely clear
			//how to solve for these with the closed formula. Dunno if appending the frame to frame 
			//solutions would be right.
			BundleAdjustment::pointToPlane_Bundle_step<explainMe::MatrixXf_rm>
				(trajectories, numClouds, reference_frame,w,hyp,T[hyp]);
		}
		
		
		//compute "gradient"

		//version for arbitrary paths
		compute_d_dw_E_w_pow_p_fast_incomplete_anyFrame(
			trajectories,	w,	T,		
			numClouds, reference_frame,
			d_dw_E, p2);

		//with temporal smoothnessterm
		/*compute_d_dw_E_w_pow_p_fast_incomplete_anyFrame(
			trajectories, N_start, N_end,	w,	T,		
			first_frame, last_frame, reference_frame,
			d_dw_E, params.p2,
			params.alpha_smootheness_w);*/


		explainMe::RowVectorXf w_tangent = explainMe::RowVectorXf::Ones(numHyp), tmp, w_proj, max_movement_possible, border_nrml;
		w_tangent.normalize();
		//d_dw_E_nonprojected = d_dw_E;

		
		for(int i = 0; i < d_dw_E.rows(); i++){
			
			//d_dw_E[i] = d_dw_E[i] - (w_tangent * d_dw_E[i].dot(w_tangent)); //reproject on w's tangential space.
			d_dw_E.row(i) = d_dw_E.row(i) - (w_tangent * d_dw_E.row(i).dot(w_tangent)); //reproject on w's tangential space.
		}

		for(int i = 0; i < w.rows(); i++){
			//gradient descent,
			w.row(i) =  w.row(i) - d_dw_E.row(i)*stepSize;

			
			//"infinite step"
			//w[i] = Eigen::Vector3f::Ones()/3 - d_dw_E[i]/d_dw_E[i].norm() * 2.44948974278/3;//sqrt(6)/3


			// projection procedure: projection onto nd tetrahedron. see matlab implementation.
			//todo put in own method.
			//auto w_orig = w[i];
			//	w_proj = w[i]; 
			if(w.row(i).minCoeff() <0 ){ //if reprojection is needed.
				w_proj = w_center;
				tmp = w.row(i) - w_proj;
				constraints.fill(0);
				constraints.col(0) = w_tangent;
				total_moved = 0;

				for(int it_proj = 0; it_proj < numHyp-1 && total_moved < (1-1e-5); it_proj++){
					//this positive component w�se factor times tmp(i) can be added to w_proj(i) without it becoming negative
					max_movement_possible = - w_proj.cwiseQuotient(tmp); 
					max_movement_possible = (tmp.array() >=-1e-5).select(1e10,max_movement_possible.array()); //for pos or very small dims "infinite" movement ok.				

					//find coefficient which will first be zero when moving in direction tmp.
					max_movement = max_movement_possible.minCoeff(&min_coeff_idx);
					max_movement = std::min(max_movement, 1-total_moved);
					//move in this diection

					w_proj += tmp * max_movement;
					//now tmp needs to be reprojected, as one coordinate is zero.
					new_constraint = e.col(min_coeff_idx) - constraints * constraints.transpose() * e.col(min_coeff_idx);
					constraints.col(it_proj +1) = new_constraint.normalized();
					tmp = tmp - constraints * constraints.transpose() *tmp;

					total_moved = total_moved + max_movement;


				}

				w.row(i) = w_proj;
				
				if(w_proj(0)*0 != 0){
					std::cout << "AAAAH, projection nan:\n " << w.row(i) << " \nprojected to\n" << w_proj;
					int a;
					std::cin >> a;
				}
			}

			//for numerics
			w.row(i) = (w.row(i).array() < 0 ).select(0,w.row(i));
			w.row(i) = w.row(i) / w.row(i).sum(); 
			
		}
	}
}



void InitByEnergyMinimization::compute_d_dw_E_w_pow_p_fast_incomplete_anyFrame(std::vector<trajectory_R> & paths,
											explainMe::MatrixXf_rm &w,
											//T[i_f][i_w]
											std::vector<std::vector<Eigen::Matrix4f>> &T,
											int numClouds, int reference_frame,	
											explainMe::MatrixXf_rm & d_dw_E,
											float p)
{
	//In principal I'd need the gradient of the pointwise error function. For now I will simply use
	//x-c(x) normed and as error the point 2 point error.

	//d_dw_E is the current error plus a weighted sum of error gradients


	std::vector<std::vector<Eigen::Matrix4f>> ainv = T; //not huge, so probly ok as temp?
	for(int i = 0; i < ainv.size(); i++){
		for(int j = 0; j < ainv[i].size(); j++){
			ainv[i][j] = T[i][j].inverse();
		}
	}

	//derivative for each (weight-set, path) pair (i.e. each weight)
	for(int i_w =0; i_w < w.cols(); i_w++){
		for(int i = 0; i < paths.size(); i++)
		{
			//compute one single derivative:
			d_dw_E(i,i_w) = 0;
			//the derivative is dependent of all hops(path length) and paths;
			//the first sum treats forward hops...
			for(int frm = 0; frm <numClouds; frm++){
				if(paths[i].cloudInRange(frm))
				{
					//Eigen::Vector4f  p = paths[i][0].getVector4fMap();
					//current error to frame frm.
					if(energyType == POINT_TO_POINT){
					d_dw_E(i,i_w) = d_dw_E(i,i_w) + p *pow(w(i,i_w),p-1) * 
						(T[i_w][frm] * ainv[i_w][paths[i].seedCloud()] * paths[i].seedEl().getVector4fMap() - paths[i][frm].getVector4fMap()).norm();
					}
					else{
					//and the main sum is left out.
					d_dw_E(i,i_w) = d_dw_E(i,i_w) + p *pow(w(i,i_w),p-1) * 
						std::abs((T[i_w][frm] * ainv[i_w][paths[i].seedCloud()] * paths[i].seedEl().getVector4fMap() -paths[i][frm].getVector4fMap()).topLeftCorner<3,1>().dot( paths[i][frm].getNormalVector3fMap()));
					}
				}
			}

		}
	}//end for.
}
