#include "utils.h"
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>

#include <boost/filesystem.hpp>

bool
readMotionFile (const std::string &filename,
                int &num_frames,
                std::map<std::string, std::vector<Eigen::Matrix4d> > &motion)
{
  std::ifstream file (filename);
  if (!file.is_open ())
  {
    PCL_ERROR ("Error reading motion file at %s.\n", filename.c_str ());
    return (false);
  }

  file >> num_frames;
  int num_objects;
  file >> num_objects;

  PCL_ERROR ("Motion file has %d frames and %d objects.\n", num_frames, num_objects);
  for (size_t o_i = 0; o_i < num_objects; ++o_i)
  {
    std::string obj_name;
    file >> obj_name;
    obj_name = obj_name.substr (1);
    PCL_ERROR ("Reading transformations for object: %s\n", obj_name.c_str ());

    std::vector<Eigen::Matrix4d> transforms (num_frames);
    for (size_t f_i = 0; f_i < num_frames; ++f_i)
    {
      for (size_t i = 0; i < 4; ++i)
        for (size_t j = 0; j < 4; ++j)
          file >> transforms[f_i] (j, i);

//      std::cerr << obj_name << " " << f_i << std::endl << transforms[f_i] << std::endl;
    }

    motion.insert (std::make_pair (obj_name, transforms));
  }


  return (true);
}


pcl::PointCloud<pcl::PointXYZRGBA>::Ptr
convertDepthAndColorMapToPCL (const cv::Mat &img_depth, const cv::Mat &img_render,
                              const af::CameraParams &cam_params,
                              const float &z_near, const float &z_far)
{
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGBA> ());
  cloud->width = cam_params.width;
  cloud->height = cam_params.height;
  cloud->points.resize (cam_params.width * cam_params.height);

  for (int x = 0; x < img_depth.cols; ++x)
    for (int y = 0; y < img_depth.rows; ++y)
    {
      pcl::PointXYZRGBA &p = cloud->points[y * img_depth.cols + x];

      /// Set the depth
      float z_b = img_depth.at<float> (cv::Point (x, y));
      if (fabs (z_b - 1.f) < 1e-3)
      {
        p.x = p.y = p.z = std::numeric_limits<float>::quiet_NaN ();
      }
      else
      {
        float z_n = 2.0 * z_b - 1.0;
        float z_e = 2.0 * z_near * z_far / (z_far + z_near - z_n * (z_far - z_near));
        p.x = (x - cam_params.cx) / cam_params.fx * z_e;
        p.y = (y - cam_params.cy) / cam_params.fy * z_e;
        p.z = z_e;
      }

      /// Set the color
      cv::Vec3b color = img_render.at<cv::Vec3b> (cv::Point (x, y));
      p.r = color[2];
      p.g = color[1];
      p.b = color[0];
    }

  return (cloud);
}





int
main (int argc,
	  char **argv)
{
  std::string in_dir = "";
  pcl::console::parse_argument (argc, argv, "-in_dir", in_dir);

  std::string out_dir = "";
  pcl::console::parse_argument (argc, argv, "-out_dir", out_dir);


  /// Load all the meshes
  std::vector<std::string> meshes_files;
  af::getFilesFromDirectory (in_dir, ".OBJ", meshes_files);
  std::vector<af::Mesh<double>::Ptr> meshes;
  std::vector<std::string> mesh_names;
  for (size_t i = 0; i < meshes_files.size (); ++i)
  {
    af::Mesh<double>::Ptr mesh (new af::Mesh<double> ());
    af::Mesh<double>::readMeshOBJ (meshes_files[i], *mesh);

    af::Mesh<double>::Ptr mesh_tri (new af::Mesh<double> ());
    af::Mesh<double>::convertToTriangleMesh (*mesh, *mesh_tri);

    mesh_tri->computeNormalsUsingConnectivity ();
    meshes.push_back (mesh_tri);

    mesh_names.push_back (af::getBaseName (meshes_files[i]));
  }

  std::vector<std::string> texture_files;
  af::getFilesFromDirectory (in_dir, ".jpg", texture_files);
  std::vector<cv::Mat> texture_images (texture_files.size ());
  for (size_t i = 0; i < texture_files.size (); ++i)
  {
    texture_images[i] = cv::imread (texture_files[i]);
    std::cerr << texture_images[i].channels () << " " << texture_images[i].type () << " " << CV_8UC3 << std::endl;
  }

  af::LabelGeneratorFBO label_gen;
  af::RenderWithTextureFBO render_gen;
  render_gen.setTextureImages (texture_images);

  /// Approx Kinect camera parameters
  af::CameraParams cam_params;
  cam_params.fx = 525.;
  cam_params.fy = 525.;
  cam_params.cx = 320.;
  cam_params.cy = 240.;
  cam_params.width = 640;
  cam_params.height = 480;
  cam_params.pose = Eigen::Affine3d::Identity ();

  label_gen.setCameraParams (cam_params, 1e-1, 1e3);
  render_gen.setCameraParams (cam_params, 1e-1, 1e2);

//  Eigen::Matrix3d K = cam_params.getK ();

  /// TODO set the motion stuff
  std::map<std::string, std::vector<Eigen::Matrix4d> > motion;
  int num_frames;
  readMotionFile (in_dir + "/motion.txt", num_frames, motion);


  boost::filesystem::create_directories (out_dir);
  for (int f_i = 0; f_i < num_frames; ++f_i)
  {
    std::vector<af::Mesh<double>::Ptr> meshes_tr;
    PCL_ERROR ("Rendering frame %d / %d\n", f_i, num_frames);
    /// Transform the meshes
    for (size_t m_i = 0; m_i < mesh_names.size (); ++m_i)
      if (motion.count (mesh_names[m_i]) != 0)
      {
        af::Mesh<double>::Ptr mesh_tr (new af::Mesh<double> ());
        meshes[m_i]->transform (motion[mesh_names[m_i]][f_i], *mesh_tr);
        meshes_tr.push_back (mesh_tr);

//        char str[512];
//        sprintf (str, "%s_%03d.obj", mesh_names[m_i].c_str (), f_i);
//        af::Mesh<double>::writeMeshOBJ (str, *mesh_tr);
      }

    label_gen.setMeshes (meshes_tr);
    cv::Mat img_labels;
    label_gen.getRenderImage (img_labels);
    cv::imshow ("labels", img_labels);

    cv::Mat img_depth;
    label_gen.getDepthMap (img_depth);
    cv::imshow ("depth", img_depth);


    render_gen.setMeshes (meshes_tr);
    cv::Mat img_render;
    render_gen.getRenderImage (img_render);
    cv::imshow ("render", img_render);


    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud =  convertDepthAndColorMapToPCL (img_depth, img_render,
                                                                                   cam_params, label_gen.z_near_, label_gen.z_far_);
    char str[512];
    sprintf (str, "%s/cloud_%03d.pcd", out_dir.c_str (), f_i);
    pcl::io::savePCDFileBinaryCompressed (str, *cloud);

    /*
    /// Do an artificial render
    cv::Mat img_debug (cv::Mat::zeros (cam_params.height, cam_params.width, CV_8UC3));
    for (size_t m_i = 0; m_i < mesh_names.size (); ++m_i)
      if (motion.count (mesh_names[m_i]) != 0)
      {
        for (size_t v_i = 0; v_i < meshes_tr[m_i]->vertices_.cols (); ++v_i)
        {
          Eigen::Vector3d p = K * meshes_tr[m_i]->vertices_.col (v_i);
          p /= p (2);

          cv::circle (img_debug, cv::Point (p (0), p (1)), 5, cv::Scalar (0, 0, 255));
        }
      }
    cv::imshow ("image debug", img_debug);
*/


    cv::waitKey (3);
    sprintf (str, "%s/labels_%03d.png", out_dir.c_str (), f_i);
    cv::imwrite (str, img_labels);
    sprintf (str, "%s/render_%03d.png", out_dir.c_str (), f_i);
    cv::imwrite (str, img_render);
  }


  return (0);
}
