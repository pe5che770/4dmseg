#include "utils.h"

#include <boost/filesystem.hpp>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

void
display ()
{}


GLuint af::GeneratorFBO::fbo_id_ = 0;
GLuint af::GeneratorFBO::rb_id_ = 0;
GLuint af::GeneratorFBO::tex_id_ = 0;
GLuint af::GeneratorFBO::depth_id_ = 0;
int af::GeneratorFBO::win_id_ = 0;

void
af::GeneratorFBO::setCameraParams (const CameraParams &params,
                                   float zNear, float zFar)
{
  proj_matrix_ = Eigen::Matrix4f::Zero ();
  proj_matrix_ (0, 0) = 2. * params.fx / static_cast<float> (params.width);
  proj_matrix_ (0, 2) = (static_cast<float> (params.width) - 2. * params.cx) / static_cast<float> (params.width);
  proj_matrix_ (1, 1) = -2. * params.fy / static_cast<float> (params.height);
  proj_matrix_ (1, 2) = (static_cast<float> (params.height) - 2. * params.cy) / static_cast<float> (params.height);
  proj_matrix_ (2, 2) = (-zFar - zNear) / (zFar - zNear);
  proj_matrix_ (2, 3) = -2. * zFar * zNear / (zFar - zNear);
  proj_matrix_ (3, 2) = -1.;

  model_matrix_ = params.pose.inverse ().matrix ().cast<float> ();
  model_matrix_.row (2) *= -1.;

  width_ = params.width;
  height_ = params.height;
  z_near_ = zNear;
  z_far_ = zFar;

  printf ("set camera params\n");
}

void
af::GeneratorFBO::getRenderImage (cv::Mat &render)
{
  /// Check if GLUT was initialized already (by ourselves or by VTK)
  /// Don't do it a second time
  if (glutGetWindow () == 0)
  {
    printf ("Creating glut window\n");
    int argc = 0;
    char** argv = nullptr;
    glutInit (&argc, argv);
    glutInitWindowSize (320, 320);
    win_id_ = glutCreateWindow ("glut dummy window render_with_texture");
#ifndef __APPLE__
    glewInit ();
#endif
    glutDisplayFunc (display);
  }

  win_id_ = glutGetWindow ();
  glutSetWindow (win_id_);
  glCheckError ();

  if (rb_id_ == 0)
  {
    glGenFramebuffers (1, &fbo_id_);
    glGenRenderbuffers (1, &rb_id_);
    glGenTextures (1, &tex_id_);
    glBindFramebuffer (GL_FRAMEBUFFER, fbo_id_);

    glBindTexture (GL_TEXTURE_2D, tex_id_);
    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, width_, height_, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_id_, 0);

    glBindRenderbuffer (GL_RENDERBUFFER, rb_id_);
    glRenderbufferStorage (GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width_, height_);

    glFramebufferRenderbuffer (GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rb_id_);

    GLenum status = glCheckFramebufferStatus (GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
      printf ("Error with our framebuffer.\n");
  }

  glCheckError ();

  glViewport (0, 0, width_, height_);

  glCheckError ();

  draw ();

  glCheckError ();

  /// Get the data from the texture into a cv::Mat
  glReadBuffer (GL_COLOR_ATTACHMENT0);

  glCheckError ();

  render = cv::Mat::zeros (height_, width_, CV_8UC3);
  glPixelStorei (GL_PACK_ALIGNMENT, (render.step & 3) ? 1 : 4);
  glPixelStorei (GL_PACK_ROW_LENGTH, render.step / render.elemSize ());
  glReadPixels (0, 0, width_, height_, GL_BGR, GL_UNSIGNED_BYTE, render.data);
  cv::flip (render, render, 0);

  glCheckError ();

  glDeleteTextures (1, &tex_id_);
  glDeleteRenderbuffers (1, &rb_id_);
  glBindFramebuffer (GL_FRAMEBUFFER, 0);
  glDeleteFramebuffers (1, &fbo_id_);
  tex_id_ = rb_id_ = fbo_id_ = 0;
  glCheckError ();
  printf ("Got render image...\n");
}


void
af::GeneratorFBO::getDepthMap (cv::Mat &depth_map)
{
  /// Check if GLUT was initialized already (by ourselves or by VTK)
  /// Don't do it a second time
  if (glutGetWindow () == 0)
  {
    printf ("Creating glut window\n");
    int argc = 0;
    char** argv = nullptr;
    glutInit (&argc, argv);
    glutInitWindowSize (320, 320);
    win_id_ = glutCreateWindow ("glut dummy window render_with_texture");
#ifndef __APPLE__
    glewInit ();
#endif
    glutDisplayFunc (display);
  }

  win_id_ = glutGetWindow ();
  glutSetWindow (win_id_);
  glCheckError ();

  if (rb_id_ == 0)
  {
    /*
glBindRenderbuffer(GL_RENDERBUFFER, color_buffer);
glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, width, height);
glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, color_buffer);

glBindRenderbuffer(GL_RENDERBUFFER, depth_buffer);
glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height);
glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth_buffer);

glGenTextures(1, &color_texture);
glBindTexture(GL_TEXTURE_2D, color_texture);
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color_texture, 0);

glGenTextures(1, &depth_texture);
glBindTexture(GL_TEXTURE_2D, depth_texture);
glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth_texture, 0);
*/

    glGenFramebuffers (1, &fbo_id_);
    glGenRenderbuffers (1, &rb_id_);
    glGenTextures (1, &depth_id_);
    glBindFramebuffer (GL_FRAMEBUFFER, fbo_id_);

    glBindTexture (GL_TEXTURE_2D, depth_id_);
    glTexImage2D (GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width_, height_, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glFramebufferTexture2D (GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth_id_, 0);

    glBindRenderbuffer (GL_RENDERBUFFER, rb_id_);
    glRenderbufferStorage (GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width_, height_);
    glFramebufferRenderbuffer (GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rb_id_);

    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    GLenum status = glCheckFramebufferStatus (GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
      printf ("Error with our framebuffer.\n");
  }

  glCheckError ();

  glViewport (0, 0, width_, height_);

  glCheckError ();

  draw ();

  glCheckError ();

  /// Get the data from the texture into a cv::Mat
  glReadBuffer (GL_DEPTH_ATTACHMENT);

  glCheckError ();

  depth_map = cv::Mat::zeros (height_, width_, CV_32FC1);
  glPixelStorei (GL_PACK_ALIGNMENT, (depth_map.step & 3) ? 1 : 4);
  glPixelStorei (GL_PACK_ROW_LENGTH, depth_map.step / depth_map.elemSize ());
  glReadPixels (0, 0, width_, height_, GL_DEPTH_COMPONENT, GL_FLOAT, depth_map.data);
  cv::flip (depth_map, depth_map, 0);

  glCheckError ();

  glDeleteTextures (1, &depth_id_);
  glDeleteRenderbuffers (1, &rb_id_);
  glBindFramebuffer (GL_FRAMEBUFFER, 0);
  glDeleteFramebuffers (1, &fbo_id_);
  tex_id_ = rb_id_ = fbo_id_ = 0;
  glCheckError ();
  printf ("Got depth map...\n");
}




void
af::SelectionBufferGeneratorFBO::draw ()
{
  //glCheckError ();

  /// Do the actual drawing
  glDisable (GL_TEXTURE_2D);
  glDisable (GL_BLEND);
  glDisable (GL_COLOR_MATERIAL);
  glClearColor (0., 0., 0., 0.);
  glClearDepth (1.);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glDisable (GL_LIGHT0);
  glDisable (GL_LIGHTING);

  //glCheckError ();

  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();
  glMultMatrixf (proj_matrix_.data ());

  glMatrixMode (GL_MODELVIEW);
  glLoadIdentity ();
  glMultMatrixf (model_matrix_.data ());

  //glCheckError ();
  glShadeModel (GL_FLAT);

  //glCheckError ();

  // if (!wireframe_)
  glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
  // else
  // glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

  glBegin (GL_QUADS);
  for (int quad_i = 0; quad_i < mesh_->quad_vertices_.cols (); ++quad_i)
  {
    for (size_t v_i = 0; v_i < 4; ++v_i)
    {
      glVertex3f (mesh_->vertices_ (0, mesh_->quad_vertices_ (v_i, quad_i)),
                  mesh_->vertices_ (1, mesh_->quad_vertices_ (v_i, quad_i)),
                  mesh_->vertices_ (2, mesh_->quad_vertices_ (v_i, quad_i)));
    }
  }
  glEnd ();

  //glCheckError ();

  glBegin (GL_TRIANGLES);
  for (int tri_i = 0; tri_i < mesh_->tri_vertices_.cols (); ++tri_i)
  {
    unsigned char r, g, b;
    size_t temp = tri_i + 1; /// to avoid confusion with the black background
    r = temp / (256 * 256);
    g = (temp % (256 * 256)) / 256;
    b = temp % 256;

    glColor3ub (r, g, b);
    for (size_t v_i = 0; v_i < 3; ++v_i)
    {
      glVertex3f (mesh_->vertices_ (0, mesh_->tri_vertices_ (v_i, tri_i)),
                  mesh_->vertices_ (1, mesh_->tri_vertices_ (v_i, tri_i)),
                  mesh_->vertices_ (2, mesh_->tri_vertices_ (v_i, tri_i)));
    }
  }
  glEnd ();

  //glCheckError ();

  glColor3ub (255, 255, 255);
}


void
af::LabelGeneratorFBO::draw ()
{
  glCheckError ();
  /// Do the actual drawing
  glDisable (GL_TEXTURE_2D);
  glDisable (GL_BLEND);
  glDisable (GL_COLOR_MATERIAL);
  glClearColor (0., 0., 0., 0.);
  glClearDepth (1.);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glDisable (GL_LIGHT0);
  glDisable (GL_LIGHTING);

  glCheckError ();

  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();
  glMultMatrixf (proj_matrix_.data ());

  glMatrixMode (GL_MODELVIEW);
  glLoadIdentity ();
  glMultMatrixf (model_matrix_.data ());

  glCheckError ();
  glShadeModel (GL_FLAT);

  glCheckError ();

  // if (!wireframe_)
  glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
  // else
  // glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );


  for (size_t m_i = 0; m_i < meshes_.size (); ++m_i)
  {
    unsigned char r, g, b;
    size_t temp = (m_i + 1) * 256 * 256 * 255 / meshes_.size (); /// to avoid confusion with the black background
    r = temp / (256 * 256);
    g = (temp % (256 * 256)) / 256;
    b = temp % 256;

    glColor3ub (r, g, b);
    printf ("Mesh %zu has color: %ld %ld %ld\n", m_i, r, g, b);

    glBegin (GL_QUADS);
    for (int quad_i = 0; quad_i < meshes_[m_i]->quad_vertices_.cols (); ++quad_i)
    {
      for (size_t v_i = 0; v_i < 4; ++v_i)
      {
        printf ("vertex quad\n");
        glVertex3f (meshes_[m_i]->vertices_ (0, meshes_[m_i]->quad_vertices_ (v_i, quad_i)),
                    meshes_[m_i]->vertices_ (1, meshes_[m_i]->quad_vertices_ (v_i, quad_i)),
                    meshes_[m_i]->vertices_ (2, meshes_[m_i]->quad_vertices_ (v_i, quad_i)));
      }
    }
    glEnd ();

    glCheckError ();

    glBegin (GL_TRIANGLES);
    for (int tri_i = 0; tri_i < meshes_[m_i]->tri_vertices_.cols (); ++tri_i)
    {
      for (size_t v_i = 0; v_i < 3; ++v_i)
      {
        glVertex3f (meshes_[m_i]->vertices_ (0, meshes_[m_i]->tri_vertices_ (v_i, tri_i)),
                    meshes_[m_i]->vertices_ (1, meshes_[m_i]->tri_vertices_ (v_i, tri_i)),
                    meshes_[m_i]->vertices_ (2, meshes_[m_i]->tri_vertices_ (v_i, tri_i)));
      }
    }
    glEnd ();
  }

  glCheckError ();

  glColor3ub (255, 255, 255);
}


std::vector<GLuint> af::RenderWithTextureFBO::texture_ids_ = std::vector<GLuint> ();
void
af::RenderWithTextureFBO::draw ()
{
  //glCheckError ();
  /// Do the actual drawing
  glClearColor (0., 0., 0., 0.);
  glClearDepth (1.);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable (GL_DEPTH_TEST);

  //glCheckError ();



  for (size_t m_i = 0; m_i < meshes_.size (); ++m_i)
  {
    glEnable (GL_LIGHT0);
    glEnable (GL_LIGHTING);
    glEnable (GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, texture_ids_[m_i]);

    GLfloat pos[4] = {light_pos_ (0), light_pos_ (1), light_pos_ (2), 1.0};
    glLightfv (GL_LIGHT0, GL_POSITION, pos);

    // const GLfloat material_color[4] = {0.55, 0.75, 0.97, 1.};
    const GLfloat black[4] = {0., 0., 0., 1.};
    const GLfloat white[4] = {3.2, 3.2, 3.2, 1.};
    glLightfv(GL_LIGHT0, GL_SPECULAR, black);
    glLightfv(GL_LIGHT0, GL_AMBIENT, white);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, black);

    // Enable GL textures
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // Nice texture coordinate interpolation
    glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );

    //glCheckError ();

    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glMultMatrixf (proj_matrix_.data ());

    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
    glMultMatrixf (model_matrix_.data ());

    glShadeModel (GL_SMOOTH);

    //glCheckError ();

    glBegin (GL_TRIANGLES);
    for (int tri_i = 0; tri_i < meshes_[m_i]->tri_vertices_.cols (); ++tri_i)
    {
      for (size_t v_i = 0; v_i < 3; ++v_i)
      {
        glNormal3f (meshes_[m_i]->normals_ (0, meshes_[m_i]->tri_vertices_ (v_i, tri_i)),
                    meshes_[m_i]->normals_ (1, meshes_[m_i]->tri_vertices_ (v_i, tri_i)),
                    meshes_[m_i]->normals_ (2, meshes_[m_i]->tri_vertices_ (v_i, tri_i)));
        glTexCoord2f (meshes_[m_i]->tex_coords_ (0, meshes_[m_i]->tri_tex_coords_ (v_i, tri_i)),
                      meshes_[m_i]->tex_coords_ (1, meshes_[m_i]->tri_tex_coords_ (v_i, tri_i)));
        glVertex3f (meshes_[m_i]->vertices_ (0, meshes_[m_i]->tri_vertices_ (v_i, tri_i)),
                    meshes_[m_i]->vertices_ (1, meshes_[m_i]->tri_vertices_ (v_i, tri_i)),
                    meshes_[m_i]->vertices_ (2, meshes_[m_i]->tri_vertices_ (v_i, tri_i)));
      }
    }
    glEnd ();

    //glCheckError ();
  }
}


void
af::RenderWithTextureFBO::getRenderImage (cv::Mat &render)
{
  /// Check if GLUT was initialized already (by ourselves or by VTK)
  /// Don't do it a second time
  if (glutGetWindow () == 0)
  {
    printf ("Creating glut window\n");
    int argc = 0;
    char** argv = nullptr;
    glutInit (&argc, argv);
    glutInitWindowSize (5, 5);
    win_id_ = glutCreateWindow ("glut dummy window render_with_texture");
#ifndef __APPLE__
    glewInit ();
#endif
    glutDisplayFunc (display);
  }

  win_id_ = glutGetWindow ();
  glutSetWindow (win_id_);
  //glCheckError ();

  if (rb_id_ == 0)
  {
    glGenFramebuffers (1, &fbo_id_);
    glGenRenderbuffers (1, &rb_id_);
    glGenTextures (1, &tex_id_);
    glBindFramebuffer (GL_FRAMEBUFFER, fbo_id_);

    texture_ids_.resize (texture_images_.size ());
    for (size_t t_i = 0; t_i < texture_ids_.size (); ++t_i)
    {
      glGenTextures (1, &texture_ids_[t_i]);
      glBindTexture (GL_TEXTURE_2D, texture_ids_[t_i]);
      glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, texture_images_[t_i].cols, texture_images_[t_i].rows,
                    0, GL_BGR, GL_UNSIGNED_BYTE, texture_images_[t_i].ptr ());
      glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
    glBindTexture (GL_TEXTURE_2D, tex_id_);
    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, width_, height_, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_id_, 0);

    glBindRenderbuffer (GL_RENDERBUFFER, rb_id_);
    glRenderbufferStorage (GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width_, height_);

    glFramebufferRenderbuffer (GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rb_id_);

    GLenum status = glCheckFramebufferStatus (GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
      printf ("Error with our framebuffer.\n");
  }

  //glCheckError ();

  glViewport (0, 0, width_, height_);

  //glCheckError ();

  draw ();

  /// Get the data from the texture into a cv::Mat
  glReadBuffer (GL_COLOR_ATTACHMENT0);

  //glCheckError ();

  render = cv::Mat::zeros (height_, width_, CV_8UC3);
  glPixelStorei (GL_PACK_ALIGNMENT, (render.step & 3) ? 1 : 4);
  glPixelStorei (GL_PACK_ROW_LENGTH, render.step / render.elemSize ());
  glReadPixels (0, 0, width_, height_, GL_BGR, GL_UNSIGNED_BYTE, render.data);
  cv::flip (render, render, 0);

  //glCheckError ();

  glDeleteTextures (1, &tex_id_);
  for (size_t t_i = 0; t_i < texture_ids_.size (); ++t_i)
    glDeleteTextures (1, &texture_ids_[t_i]);
  glDeleteRenderbuffers (1, &rb_id_);
  glBindFramebuffer (GL_FRAMEBUFFER, 0);
  glDeleteFramebuffers (1, &fbo_id_);
  tex_id_ = rb_id_ = fbo_id_ = 0;
  //glCheckError ();
}




std::string
af::getBaseName (const std::string &filename)
{
  size_t slash_pos = filename.find_last_of ('/');
  size_t dot_pos = filename.find_last_of (".");
  std::string base_name = filename.substr (slash_pos + 1, dot_pos - slash_pos - 1);

  return (base_name);
}

void
af::getFilesFromDirectory (const std::string path_dir,
                           const std::string extension,
                           std::vector<std::string> &files)
{
  if (path_dir != "" && boost::filesystem::exists (path_dir))
  {
    boost::filesystem::directory_iterator end_itr;
    for (boost::filesystem::directory_iterator itr (path_dir); itr != end_itr; ++itr)
      if (!is_directory (itr->status ()) &&
          boost::algorithm::to_upper_copy (boost::filesystem::extension (itr->path ())) == boost::algorithm::to_upper_copy (extension))
      {
        files.push_back (itr->path ().string ());
      }
  }
  sort (files.begin (), files.end ());
}


