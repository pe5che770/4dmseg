import maya.cmds as cmds

file_out = open('/Users/alex/Work/motion.txt', 'w')


# Get mesh positions
shapeList = cmds.ls(typ='mesh')
transformList = cmds.listRelatives(shapeList, parent=True, fullPath=True)

file_out.write ('100\n')
file_out.write (str (len (transformList)) + '\n')
for transform in transformList:
	file_out.write (transform)
	file_out.write ('\n')
	for t in range (1, 101):
		mat = cmds.getAttr(transform + '.worldMatrix', time = t)
		for i in range (0, 16):
			file_out.write (str (mat[i]))
			file_out.write (' ')
		file_out.write ('\n')

file_out.close ()