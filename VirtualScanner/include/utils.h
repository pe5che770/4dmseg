#pragma once


#pragma once

#ifndef __APPLE__
  #include <GL/glew.h>
#endif

//#include "common.h"
//#include "image_processing.h"
#include "mesh.h"
#include <opencv2/opencv.hpp>

#ifdef __APPLE__
  #include <GLUT/glut.h>
#endif



namespace af
{

struct CameraParams
{
  std::string image_path;
  int height, width;
  double fx, fy, cx, cy;
  double skew, k1, k2, k3, p1, p2;
  Eigen::Affine3d pose;

  void
  print () const;

  inline Eigen::Matrix3d
  getK () const
  {
    Eigen::Matrix3d K (Eigen::Matrix3d::Identity ());
    K (0, 0) = fx;
    K (1, 1) = fy;
    K (0, 2) = cx;
    K (1, 2) = cy;
    return (K);
  }

};


class GeneratorFBO
{
public:
  GeneratorFBO ()
  {}

  void
  setCameraParams (const CameraParams &params,
                   float zNear = 0.01,
                   float zFar = 10.);

  void
  setMeshes (std::vector<af::Mesh<double>::Ptr> &meshes)
  { meshes_ = meshes; }

  virtual void
  draw () = 0;

  void
  getRenderImage (cv::Mat &render);

  void
  getDepthMap (cv::Mat &depth_map);

  float z_near_, z_far_;

protected:
  Eigen::Matrix4f proj_matrix_;
  Eigen::Matrix4f model_matrix_;

  static GLuint fbo_id_, rb_id_, tex_id_, depth_id_;
  static int win_id_;
  int width_, height_;


  std::vector<af::Mesh<double>::Ptr> meshes_;
};


class SelectionBufferGeneratorFBO : public GeneratorFBO
{
public:
  SelectionBufferGeneratorFBO ()
  {}

  void
  setMesh (af::Mesh<double>::ConstPtr mesh)
  { mesh_ = mesh; }

  void
  draw ();

  void
  getProjectedIds (Eigen::MatrixXi &buffer);

//  void
//  getRenderImage (cv::Mat &render);

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

protected:
  af::Mesh<double>::ConstPtr mesh_;
};



class LabelGeneratorFBO : public GeneratorFBO
{
public:
  LabelGeneratorFBO ()
  {}

  void
  draw ();


  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};



class RenderWithTextureFBO : public GeneratorFBO
{
public:
  RenderWithTextureFBO ()
  {light_pos_ = Eigen::Vector3f (0., 0., 100.); }

  void
  setOrthoCameraParams (const int width, const int height,
                        const float scale, const Eigen::Matrix4f &pose);

  void
  setLightPosition (const Eigen::Vector3f &light_pos)
  { light_pos_ = light_pos; }

  void
  draw ();

  void
  setTextureImages (std::vector<cv::Mat> &textures)
  {
    texture_images_.resize (textures.size ());
    for (size_t i = 0; i < textures.size (); ++i)
    {
      cv::flip (textures[i], texture_images_[i], 0);
//      cv::imshow ("texture", texture_images_[i]);
//      cv::waitKey ();
    }
  }

  void
  getRenderImage (cv::Mat &render);

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

protected:
  static std::vector<GLuint> texture_ids_;
  std::vector<cv::Mat> texture_images_;

  Eigen::Vector3f light_pos_;
};

std::string
getBaseName (const std::string &filename);


void
getFilesFromDirectory (const std::string path_dir,
                       const std::string extension,
                       std::vector<std::string> &files);

}




/// check errors for OpenGL
static inline const char* ErrorString(GLenum error)
{
  const char* msg;

  switch (error) {
#define Case(Token) case Token: msg = #Token; break;
  Case(GL_INVALID_ENUM);
  Case(GL_INVALID_VALUE);
  Case(GL_INVALID_OPERATION);
  Case(GL_INVALID_FRAMEBUFFER_OPERATION);
  Case(GL_NO_ERROR);
  Case(GL_OUT_OF_MEMORY);
#undef Case
  }
  return msg;
}

static inline void _glCheckError(const char* file, int line) {
  GLenum error;
  while ((error = glGetError()) != GL_NO_ERROR) {
    fprintf(stderr, "ERROR: file %s, line %i: %s.\n", file, line,
            ErrorString(error));
  }
}

#ifndef NDEBUG
#define glCheckError() _glCheckError(__FILE__, __LINE__)
#else
#define glCheckError() ((void)0)
#endif

