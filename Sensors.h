#pragma once

#ifdef OLD_QT
//TODO figure out exactly which version of QT had that header name
#include <QMatrix4x4.h>
#else
#include <QMatrix4x4>
#endif

#include "sensorDescriptionData.h"
#include <iostream>
#ifdef _MSC_VER
#define sqrtf std::sqrtf
#endif

class RangeSensorDescription
{
	//width and height in pixel
	int _width;
	int _height;
	//distortion parameters
	float k2,k4,k6, px, py, fx, fy;
	float a_0, a_1, a_2, b_1, b_2, c;
	float numUnitsPerMeter;

	//diagonal distance between two samples on the plane orth(0,0,1) at (0,0,1).
	//needed to estimate the sample density at some depth z
	float _sample_distance;

	//Frustum
	QMatrix4x4 _frustum;
	QMatrix4x4 _viewPort;
	QMatrix4x4 _unproject;

	QMatrix3x4 K; //the usual camera intrinsics matrix. this is redundant with respect to the frustum and viewport rep
//	QMatrix4x4 _project;

	bool is_synthetic;

public:
	enum fileType {SENSOR, KIN2, UNIFIED};

	RangeSensorDescription(int width, int height, QMatrix4x4 &frustum){
		k2 = k4 = k6 = 0;
		px = py = 0;
		fx = fy = 0;
		a_0=a_1=a_2=b_1=b_2= c = 0;
		numUnitsPerMeter = 1000;
		is_synthetic = false;
		setup(width, height, frustum);
	}

	RangeSensorDescription(int width, int height, 
		float left, float right, float bottom, float top, 
		float near_, float far_){
			k2 = k4 = k6 = 0;
			px = py = 0;
			fx = fy = 0;
			a_0 = a_1 = a_2 = b_1 = b_2 = c = 0;
			numUnitsPerMeter = 1000;
			is_synthetic = false;
			QMatrix4x4 frustum;
			frustum.setToIdentity();
			frustum.frustum(left,right,bottom,top,near_,far_);
			setup(width,height,frustum);
	}

	RangeSensorDescription(std::string fileName, fileType ft = SENSOR){
		is_synthetic = false;
		a_0 = a_1 = a_2 = b_1 = b_2 = c = 0;
		if(ft == SENSOR){
			k2 = k4 = k6 = 0;
			px = py = 0;fx = fy = 0;
			loadFromFile(fileName);
		}
		else if(ft== KIN2)
		{
			loadFromFileKin2(fileName);
		}
		else if (ft == UNIFIED){
			k2 = k4 = k6 = 0;
			px = py = 0; fx = fy = 0;
			loadFromUnifiedFile(fileName);
		}
	}

	virtual ~RangeSensorDescription(void){}

	//hack in order not to mix qt and cuda compiler
	sensorDescriptionData descriptionData(){
		sensorDescriptionData data;
		data._width = _width;
		data._height = _height;
		for(int i = 0; i < 16; i++){
			data._unproject[i] = _unproject.data()[i];
			data._frustum[i] =  _frustum.data()[i];
		}
		return data;
	}

	/*
	* Find the x,y coordinates corresponding to the depth value z at pixel position  
	* i (width), j (height).
	*/
	void unproject(int i, int j, float z, float& target_x, float & target_y)
	{

		QVector4D tmp = QVector4D(i,j,z,1);
		/*if(k2!=0 || k4!=0 ||k6!=0){
			//estimate r iteratively. (accuracy ~5e-3 pixels)
			float r = 0;
			float ci_u = i, cj_u = j; //the undistorted ij coordinates.
			for(int i = 0; i < 8; i++){
				r= std::sqrt((ci_u- px)/fx*(ci_u - px)/fx + (cj_u- py)/fy*(cj_u- py)/fy);
				ci_u = (tmp.x() -px) / (1 + k2*pow(r,2) + k4 * pow(r,4) + k6 * pow(r,6)) + px;
				cj_u = (tmp.y() -py) / (1 + k2*pow(r,2) + k4 * pow(r,4) + k6 * pow(r,6)) + py;
			}
			tmp.setX(ci_u);
			tmp.setY(cj_u);
		}*/

		tmp = _unproject * tmp;
		target_x = -tmp.x()*z;///tmp.w();
		target_y = tmp.y()*z;///tmp.w();

		
	}

	//project and unproject are such that  i,j will be the image coordinates, or rather the pcl_cloud_as_grid coordinates.
	//pcl clouds are accessed column first. that is i will be a coordinate in [0,width] and j in [0,height]
	void project(float x, float y, float z, int&i, int&j){
		QVector4D tmp = _frustum * QVector4D(x,-y,z,1);
		tmp = tmp / tmp.w();
		tmp = _viewPort * tmp;
		/*if(k2!=0 || k4!=0 ||k6!=0){
			float r= std::sqrt((tmp.x()- px)/fx*(tmp.x() - px)/fx + (tmp.y()- py)/fy*(tmp.y()- py)/fy);
			tmp.setX(  (tmp.x() -px) * (1 + k2*pow(r,2) + k4 * pow(r,4) + k6 * pow(r,6)) + px);
			tmp.setY(  (tmp.y() -py) * (1 + k2*pow(r,2) + k4 * pow(r,4) + k6 * pow(r,6)) + py);
		}*/

		i = std::floor(tmp.x() + 0.5);
		j = std::floor(tmp.y() + 0.5);
	}

	//unrounded coordinates
	void project(float x, float y, float z, float&i, float&j){
		QVector4D tmp = _frustum * QVector4D(x,-y,z,1);
		tmp = tmp / tmp.w();
		tmp = _viewPort * tmp;
		/*if(k2!=0 || k4!=0 ||k6!=0){
			float r= std::sqrt((tmp.x()- px)/fx*(tmp.x() - px)/fx + (tmp.y()- py)/fy*(tmp.y()- py)/fy);
			tmp.setX(  (tmp.x() -px) * (1 + k2*pow(r,2) + k4 * pow(r,4) + k6 * pow(r,6)) + px);
			tmp.setY(  (tmp.y() -py) * (1 + k2*pow(r,2) + k4 * pow(r,4) + k6 * pow(r,6)) + py);
		}
		else{
			//these two give exactly the same, which is nice....
			//std::cout << "projection using focals etc : " << x/z*fx +px << ", " << y/z*fy + py << "\n";
			//std::cout << "projection using stupid stuf: " << tmp.x() << ", " << tmp.y() << "\n";
			//int a = 0;
			//std::cin>>a;
		}*/
		i = tmp.x();
		j = tmp.y();
	}

	virtual float sampleDistance(int i, int j, float z, float cos_normal_view_angle){
		float xip1, yjp1, xi, yj;
		unproject(i, j, z, xi, yj);
		unproject(i + 1, j + 1, z, xip1, yjp1);
		float inclination_factor = 1.0 / (std::abs(cos_normal_view_angle)+ 1e-10);
		inclination_factor = (inclination_factor < 3 ? inclination_factor : 3);// std::min<float>(inclination_factor, 3);
		return sqrtf((xip1 - xi)*(xip1 - xi) + (yjp1 - yj)*(yjp1 - yj))*inclination_factor;//2;//for slanted surfaces: *2. Covers surfaces up to pi/3 oriented rel to view
	}

	float sigmaHat(int i, int j, float z, float intensity, float cos_normal_view_angle){
		return std::max(
			(is_synthetic?3:1)* //for synthetic noiseless data weight sample distance more.
			sampleDistance(i, j, z, cos_normal_view_angle), 
			std_dev_noise(i, j, z, intensity))*1.7320 /*sqrt3, for 3d*/;
	}

	virtual bool has_std_dev_noise(){
		return false;
	}

	virtual float std_dev_noise(int i, int j, float z, float intensity)
	{
		//std::cout << "warning, noise of sensor not known";
		//return 1e-3f;

		float dist = pow(pow((i - _width / 2)*height()*1.0 / width(), 2) + pow(j - _height / 2, 2), 0.5);// / 364.0;
		return a_0 + a_1*z + a_2 * z*z + b_1*std::pow(std::max<float>(dist - b_2, 0),2) + c * 1 / (256+intensity);
	}

	virtual void coutNoiseModel(){
		std::cout << a_0 << "+" << a_1 << "*z +" << a_2 << "*z*z + " 
			<< b_1 << "*std::max<float>(dist -" << b_2 << ", 0)^2 + " << c << "* 1 / (1 + intensity);";
	}

	int height(){return _height;}
	int width(){return _width;}
	float f_x(){return fx;}
	float f_y(){return fy;}
	float p_x(){return px;}
	float p_y(){return py;}
	bool isCoordValid(int i, int j){
		return i >= 0 && i < _width && j >= 0 && j < _height;
	}
	QMatrix4x4 frustum(){ return  QMatrix4x4(-1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)*
		_frustum * QMatrix4x4(-1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
	QMatrix4x4 unproject(){ return _unproject; }
	QMatrix4x4 viewPort(){return _viewPort;}
	QMatrix3x4 intrinsicK(){return K;}
	float unitSampleDistance(){return _sample_distance;}
	float depthUnitsPerMeter(){ return numUnitsPerMeter; }
	void extract(float & px, float & py, float &fx, float & fy);

	void loadFromFile(std::string file);
	void loadFromUnifiedFile(std::string file);
	void loadFromFileKin2(std::string file);
	void saveToFile(std::string file);

private:
	void setup(int width, int height, QMatrix4x4 &frustum);
};




//////////////////////////////////////////////////////////////////////////////////
//Hard codded descriptors
//////////////////////////////////////////////////////////////////////////////////

class unifiedSensorDescription : public RangeSensorDescription
{
public:
	unifiedSensorDescription(std::string sensorFile) :RangeSensorDescription(sensorFile, RangeSensorDescription::UNIFIED){}
	/*float sampleDistance(int i, int j, float z, float cos_normal_view_angle){
		return RangeSensorDescription::sampleDistance(i, j, z, cos_normal_view_angle) * 3;
	}*/
};

class kinectV1Description: public RangeSensorDescription
{
public:
	//kinectV1Description():RangeSensorDescription(640,480,-0.056002690847f,0.056002690847f,-0.042002,0.042002,0.1,100){}
	kinectV1Description():RangeSensorDescription(640,480,0.056002690847f,-0.056002690847f,-0.042002f,0.042002f,0.1f,100){}
	float std_dev_noise(int i, int j, float z, float intensity){
		return 1e-3;
	}
};


class kinectV2Description: public RangeSensorDescription
{
public:
	//the constructor fills in dummy values, but they are overwritten by loading a kin2 file with better calibration.
	kinectV2Description():RangeSensorDescription(512,424,-0.06972f,0.069712f,-0.0577f,0.0577f,0.1f,100){}

	bool has_std_dev_noise(){
		return true;
	}

	float std_dev_noise(int i, int j, float z, float intensity){
		//mean intensity: 7.1746e3
		float dist = pow(pow((i - 276.1)*height()*1.0 / width(), 2) + pow(j - 203.094, 2), 0.5) / 364.0;
		float f_color = 1.0/(1 +intensity/256.0);
		//std::cout << std::max<float>(-0.00532 +  0.0004713*z*z +  0.055473 *dist -0.19498*dist*dist + 0.203671 * dist*dist*dist +  0.019551* f_color, 0.0005) << "\n";
		//parameters learned by fitting a linear model to observed noise.
		//return std::max<float>(-0.00532 +  0.0004713*z*z +  0.055473 *dist -0.19498*dist*dist + 0.203671 * dist*dist*dist +  0.019551* f_color, 0.0005);
		//simpler model, better fit:
		
		return std::max<float>(0.0064113 - 0.0094837*z + 0.003132*z*z + 0.264133* std::pow(std::max<float>(dist - 0.5205, 0), 2) + 0.022015*f_color, 0.00005);
		//good model
		//return std::max<float>(-0.006206 + 0.001288*z + 0.10281*dist - 0.776686 *dist*dist + 2.50081*dist*dist*dist - 3.62336* dist* dist*dist*dist + 1.98132*dist*dist*dist*dist*dist + 0.020746* f_color, 0.0005);
	}

	
};

class virtualCamera: public RangeSensorDescription
{
public:
	//virtualCamera():RangeSensorDescription(640,480,-32.f/525,32.f/525,-24.f/525,24.f/525,0.1f,100){}
	virtualCamera():RangeSensorDescription(640,480,0.06105,-0.06085,-0.0458f,0.04562,0.1f,100){}
	float std_dev_noise(int i, int j, float z, float intensity){
		return 1e-3;
	}
};

class asusXtionProDescription: public RangeSensorDescription
{
public:
	//asusXtionProDescription():RangeSensorDescription(640,480,-0.01625197138f,0.01625197138f,-0.0125655136f,0.0125655136f,0.1f,100){}
	asusXtionProDescription():RangeSensorDescription(640,480,0.06095,-0.06095,-0.0457f,0.0457,0.1f,100){}

	bool has_std_dev_noise(){
		return true;
	}

	float std_dev_noise(int i, int j, float z, float intensity){
		return 0.000446988 + 0.003338572*z*z;
	}
};

class BonnAsusXtionProDescription : public RangeSensorDescription
{
public:
	//asusXtionProDescription():RangeSensorDescription(640,480,-0.01625197138f,0.01625197138f,-0.0125655136f,0.0125655136f,0.1f,100){}
	BonnAsusXtionProDescription() :RangeSensorDescription(640, 480, 0.0561, -0.0561, -0.0421f, 0.0421, 0.1f, 100){}

	bool has_std_dev_noise(){
		return true;
	}

	float std_dev_noise(int i, int j, float z, float intensity){
		return 0.000446988 + 0.003338572*z*z;
	}
};

class EVADescription: public RangeSensorDescription
{
public:
	//EVADescription():RangeSensorDescription(350, 400, -0.0186,0.0196,-.022,.0268, 0.1,100){}
	//EVADescription():RangeSensorDescription(350, 500, -0.0186f,0.0196f,-.022f,.0268f, 0.1f,100){}
	//width needs to be a multiple of 64...
	EVADescription():RangeSensorDescription(192, 192, -0.0196f,0.0186f,-.0268f, .022f, 0.1f,100){}
	float std_dev_noise(int i, int j, float z, float intensity){
		return 1e-3f;
	}
};

class SintelCamera : public RangeSensorDescription
{
public:
	SintelCamera() :RangeSensorDescription(1024, 436, -0.04567, 0.04576, -0.01942f, 0.019509, 0.1f, 100){}

	bool has_std_dev_noise(){
		return true;
	}

	float std_dev_noise(int i, int j, float z, float intensity){
		//technically there is no noise as the data is synthetic,
		//but the precision is in mm, inducing some noise.
		return 1e-3f;
	}

	float sampleDistance(int i, int j, float z, float cos_normal_view_angle){
		return RangeSensorDescription::sampleDistance(i, j, z, cos_normal_view_angle) * 3;
	}
};