#pragma once

#include "explainMeDefinitions.h"
#include "Sensors.h"
#include <boost/shared_ptr.hpp>

//Interface for a simple point cloud reader-
class CloudReader
{
public:
	typedef boost::shared_ptr<CloudReader> Ptr;

	CloudReader(void){}
	virtual ~CloudReader(void){}


	virtual RangeSensorDescription * getDescription() = 0;
	virtual size_t numberOfFrames() = 0;

	virtual bool loadFrame(int frame, bool cleanUp, float maxDist, bool bilateralFilter_input, float sig_z = 0.05f) = 0;

	virtual std::vector<std::string> fileNames(int frame) = 0;
	
	virtual explainMe::Cloud::Ptr & getPointCloud() = 0;
};

