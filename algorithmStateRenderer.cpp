#include "algorithmStateRenderer.h"
#include "CloudColorizer.h"
#include "boost/filesystem.hpp"
algorithmStateRenderer::algorithmStateRenderer(int w, int h)
{

	win = new QMainWindow();
	QGLFormat glFormat;
	glFormat.setVersion(4, 1);
	glFormat.setProfile(QGLFormat::CoreProfile); // Requires >=Qt-4.8.0
	//glFormat.setSampleBuffers(true);

	gldisplay = new glDisplay(glFormat, win, w, h);
	//gldisplay->resize(2000, 1200);

	win->setCentralWidget(gldisplay);

	//undo this.
	//win->resize(640, 480);
	win->adjustSize();
	win->show();
}


algorithmStateRenderer::~algorithmStateRenderer()
{
	//no clue if i have to do this.
	delete win;
}


void algorithmStateRenderer::clear(bool resetMatrices){
	gldisplay->getSceneManager()->hideAll();
	if (resetMatrices)
	{
		gldisplay->getSceneManager()->resetAllObject2WorldMatrices();
	}
}

void algorithmStateRenderer::setCloudPosition(Eigen::Matrix4f & mat){
	QMatrix4x4 new_position(
			mat(0, 0), mat(0, 1), mat(0, 2), mat(0, 3),
			mat(1, 0), mat(1, 1), mat(1, 2), mat(1, 3),
			mat(2, 0), mat(2, 1), mat(2, 2), mat(2, 3),
			mat(3, 0), mat(3, 1), mat(3, 2), mat(3, 3)
	);
	gldisplay->getSceneManager()->setPosition("cloud", new_position);
}

void algorithmStateRenderer::setCamera(QVector3D & eye, QVector3D & lookat){
	gldisplay->getSceneManager()->updateCam(eye, lookat);
}

void algorithmStateRenderer::setPreferredProjection(QMatrix4x4 & proj, int w, int h){
	preferredProj = proj;
	preferred_w = w; preferred_h = h;
}

void algorithmStateRenderer::recordImage(const std::string & folder, const std::string & file){

	boost::filesystem::path dir(folder);
	boost::filesystem::create_directory(dir);
	//maybe dispatch thred tp store img if this is the bottleneck.
	//std::cout << "updateGL?";
	gldisplay->updateGL();
	glFlush();
	gldisplay->makeCurrent();
	gldisplay->swapBuffers();
	auto img = gldisplay->grabFrameBuffer();
	
	std::cout << (img.save((folder + "/" + file).c_str())? "+": "error saving img...");
	gldisplay->updateGL();
	//std::cout << (img.save((folder + "/dens_" + base_name + "_" + std::to_string(i) + ".png").c_str()) ? "-" : "error saving image");

}


void algorithmStateRenderer::setUpRenderingForCloud(explainMe::Cloud::Ptr cloud, explainMe::MatrixXf_rm & w){

	clear();
	gldisplay->getSceneManager()->setVisible("cloud");

	CloudColorizer::color_viz_weights(w, colBuffer, true);
	///gldisplay->resize(1000, 600);
	//win->resize(640, 480);
	win->adjustSize();
	gldisplay->makeCurrent();
	gldisplay->resizeGL(preferred_w, preferred_h, preferredProj);
	gldisplay->makeCurrent();
	gldisplay->getSceneManager()->updateCloud("cloud", cloud, colBuffer);
	gldisplay->updateGL();
}


void algorithmStateRenderer::setUpRenderingForCloud(explainMe::Cloud::Ptr cloud, std::vector<float> & vals, float scale)
{
	clear();
	gldisplay->getSceneManager()->setVisible("cloud");
	float mx = scale;
	CloudColorizer::color_viz_log_magnitude(vals, colBuffer, mx);
	///gldisplay->resize(1000, 600);
	//win->resize(640, 480);
	win->adjustSize();
	gldisplay->makeCurrent();
	gldisplay->resizeGL(preferred_w, preferred_h, preferredProj);
	gldisplay->makeCurrent();
	gldisplay->getSceneManager()->updateCloud("cloud", cloud, colBuffer);
	gldisplay->updateGL();
}

void algorithmStateRenderer::setUpRenderingForCloud(explainMe::Cloud::Ptr cloud, bool resizeToPreferredSize){

	clear(resizeToPreferredSize);
	gldisplay->getSceneManager()->setVisible("cloud");

	///gldisplay->resize(1000, 600);
	//win->resize(640, 480);
	if (resizeToPreferredSize)
	{
		win->adjustSize();
		gldisplay->makeCurrent();
		gldisplay->resizeGL(preferred_w, preferred_h, preferredProj);
	}
	gldisplay->makeCurrent();
	gldisplay->getSceneManager()->updateCloud("cloud", cloud);
	gldisplay->updateGL();
}

void algorithmStateRenderer::setUpRenderingForCloud_wmax(explainMe::Cloud::Ptr cloud, explainMe::MatrixXf_rm & w){
	//CloudColorizer::color_viz_weightsmax(w, colBuffer);
	clear();
	gldisplay->getSceneManager()->setVisible("cloud");

	//used to be this, always.
	//CloudColorizer::color_viz_weights(w, colBuffer, false);

	CloudColorizer::color_viz_weightsmax(w, colBuffer);

	///gldisplay->resize(1000, 600);
	//win->resize(640, 480);
	win->adjustSize();
	gldisplay->makeCurrent();
	gldisplay->resizeGL(preferred_w, preferred_h, preferredProj);
	gldisplay->makeCurrent();
	gldisplay->getSceneManager()->updateCloud("cloud", cloud, colBuffer);
	gldisplay->updateGL();
}

void algorithmStateRenderer::setUpRenderingForCloud_wsec_to_wmax(explainMe::Cloud::Ptr cloud, explainMe::MatrixXf_rm & w){
	//CloudColorizer::color_viz_weightsmax(w, colBuffer);
	clear();
	gldisplay->getSceneManager()->setVisible("cloud");

	//used to be this, always.
	//CloudColorizer::color_viz_weights(w, colBuffer, false);

	CloudColorizer::color_viz_weights2tow_to_max(w, colBuffer);

	///gldisplay->resize(1000, 600);
	//win->resize(640, 480);
	win->adjustSize();
	gldisplay->makeCurrent();
	gldisplay->resizeGL(preferred_w, preferred_h, preferredProj);
	gldisplay->makeCurrent();
	gldisplay->getSceneManager()->updateCloud("cloud", cloud, colBuffer);
	gldisplay->updateGL();
}

void algorithmStateRenderer::setUpRenderingForCloud_log_wi(explainMe::Cloud::Ptr cloud, explainMe::MatrixXf_rm & w, int w_i){
	clear();
	gldisplay->getSceneManager()->setVisible("cloud");
	CloudColorizer::color_viz_log_magnitude(w, colBuffer, w_i, false);//false
	///gldisplay->resize(1000, 600);
	//win->resize(640, 480);
	win->adjustSize();
	gldisplay->makeCurrent();
	gldisplay->resizeGL(preferred_w, preferred_h, preferredProj);
	gldisplay->makeCurrent();
	gldisplay->getSceneManager()->updateCloud("cloud", cloud, colBuffer);
	gldisplay->updateGL();
}

void algorithmStateRenderer::setUpRenderingForLines(std::vector<Eigen::Matrix4f> &alignments){
	std::vector<Eigen::Vector3f> seeds, target;
	int num = 50;
	seeds.reserve(alignments.size() * num);
	target.reserve(alignments.size() * num);
	Eigen::Matrix4f first_to_ref = alignments[0].inverse();
	for (int i = 0; i < num; i++){
		Eigen::Vector3f p = Eigen::Vector3f::Random();
		p.z() += 1;
		p *= 4;
		p = first_to_ref.topLeftCorner<3, 3>() * p + first_to_ref.topRightCorner<3, 1>();
		for (int al = 1; al < alignments.size(); al++){
			seeds.push_back(alignments[al-1].topLeftCorner<3, 3>() * p + alignments[al-1].topRightCorner<3, 1>());
			target.push_back(alignments[al].topLeftCorner<3, 3>() * p + alignments[al].topRightCorner<3, 1>());
		}
	}

	clear();
	gldisplay->getSceneManager()->setVisible("corresp");
	//win->resize(640, 480);
	win->adjustSize();
	gldisplay->makeCurrent();
	gldisplay->resizeGL(preferred_w, preferred_h, preferredProj);
	gldisplay->getSceneManager()->updateCorrespondences("corresp", seeds, target);
	gldisplay->updateGL();
}


void algorithmStateRenderer::setUpRenderingForLines(std::vector<explainMe::Cloud::Ptr> &lines, Eigen::VectorXf pos_neg,
	bool resize)
{
	std::vector<Eigen::Vector3f> seeds, target;
	std::vector<Eigen::Vector3f> colors;
	int num = lines[0]->size()*lines.size();
	seeds.reserve(num);
	target.reserve(num);
	colors.reserve(num);
	Eigen::Vector3f col;
	float alpha;

	float max = pos_neg.cwiseAbs().maxCoeff();
	for (int cloud = 0; cloud < lines.size() - 1; cloud++)
	{
		auto & ps = lines[cloud]->points;
		auto & qs = lines[cloud + 1]->points;
		Eigen::Vector3f p, q;
		alpha = (1 - cloud*1.0 / lines.size());
		for (int traj = 0; traj < lines[0]->size(); traj++){
			p = ps[traj].getVector3fMap();
			q = qs[traj].getVector3fMap();
			seeds.push_back(p);
			target.push_back(q);
			//CloudColorizer::colorWheel(labels_by_line[traj], col.x(), col.y(), col.z());
			CloudColorizer::colorBySignedFunction(pos_neg(traj), max/2, col.x(), col.y(), col.z());
			colors.push_back(col);
		}
		
	}

	clear(resize);
	gldisplay->makeCurrent();
	gldisplay->getSceneManager()->setVisible("correspCL");
	//win->resize(640, 480);
	gldisplay->makeCurrent();
	if (resize)
	{
		win->adjustSize();
		gldisplay->resizeGL(preferred_w, preferred_h, preferredProj);
	}
	gldisplay->getSceneManager()->updateCorrespondences("correspCL", seeds, target, colors);
	gldisplay->updateGL();

}

void algorithmStateRenderer::setUpRenderingForLines(std::vector<explainMe::Cloud::Ptr> &lines,
	std::vector<int> & labels_by_line, bool resize)
{
	std::vector<Eigen::Vector3f> seeds, target;
	std::vector<Eigen::Vector3f> colors;
	int num = lines[0]->size()*lines.size();
	seeds.reserve(num);
	target.reserve(num);
	colors.reserve(num);
	Eigen::Vector3f col;
	float alpha;
	for (int cloud = 0; cloud < lines.size() - 1; cloud++)
	{
		auto & ps = lines[cloud]->points;
		auto & qs = lines[cloud + 1]->points;
		Eigen::Vector3f p, q;
		alpha = (1 - cloud*1.0 / lines.size());
		for (int traj = 0; traj < lines[0]->size(); traj++){
			p = ps[traj].getVector3fMap();
			q = qs[traj].getVector3fMap();
			seeds.push_back(p);
			target.push_back(q);
			CloudColorizer::colorWheel(labels_by_line[traj], col.x(), col.y(), col.z());
			colors.push_back(col* (1 - 0.5* alpha));
		}
	}

	clear(resize);
	gldisplay->makeCurrent();
	gldisplay->getSceneManager()->setVisible("correspCL");
	//win->resize(640, 480);
	gldisplay->makeCurrent();
	if (resize)
	{
		win->adjustSize();
		gldisplay->resizeGL(preferred_w, preferred_h, preferredProj);
	}
	gldisplay->getSceneManager()->updateCorrespondences("correspCL", seeds, target, colors);
	gldisplay->updateGL();
}

void algorithmStateRenderer::setUpRenderingForLines(std::vector<explainMe::Cloud::Ptr> &lines)
{

	std::vector<Eigen::Vector3f> seeds, target;
	std::vector<Eigen::Vector3f> colors;
	int num = lines[0]->size()*lines.size();
	seeds.reserve(num);
	target.reserve(num);
	colors.reserve(num);
	gldisplay->makeCurrent();
	Eigen::Vector3f from_color, to_color;
	from_color << 0, 0, 0;
	to_color << 0, 0, 1;
	float alpha;
	for (int cloud = 0; cloud < lines.size() - 1; cloud++)
	{
		auto & ps = lines[cloud]->points;
		auto & qs = lines[cloud+1]->points;
		Eigen::Vector3f p, q;
		alpha = (1 - cloud*1.0 / lines.size());
		for (int traj = 0; traj < lines[0]->size(); traj++){
			p = ps[traj].getVector3fMap();
			q = qs[traj].getVector3fMap();
			seeds.push_back(p);
			target.push_back(q);
			colors.push_back(from_color *alpha + to_color * (1 - alpha));
		}
	}

	clear();
	gldisplay->getSceneManager()->setVisible("correspCL");
	//win->resize(640, 480);
	win->adjustSize();
	gldisplay->makeCurrent();
	gldisplay->resizeGL(preferred_w, preferred_h, preferredProj);
	gldisplay->getSceneManager()->updateCorrespondences("correspCL", seeds, target, colors);
	gldisplay->updateGL();
}