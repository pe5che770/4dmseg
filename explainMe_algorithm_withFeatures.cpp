#include "explainMe_algorithm_withFeatures.h"

#include "CloudTools.h"
#include "maxflowMincut/graph.h"

//#undef min
//#undef max
#include <cmath>
#include <iostream>
#include "mySimpleKdTree.h"
#include <omp.h>
#include "StopWatch.h"
#include "Chronist.h"
#include "CloudSampler.h"

#ifdef _MSC_VER
#define logf std::logf
#endif

bool explainMe_algorithm_withFeatures::abortComputation = false;

explainMe_algorithm_withFeatures::explainMe_algorithm_withFeatures(CloudReader::Ptr reader_, Parameters & params_):
	c1(new explainMe::Cloud()),
	c2(new explainMe::Cloud()),
	motions(params_.numHypo, 0, reader_->getDescription())
{
	reader = reader_;
	params = params_;

	segmentation_c1 = Eigen::ArrayXXi(reader->getDescription()->width(), reader->getDescription()->height());
	
	//setup 
	//first_frame = -params.max_pathlength_bwd +1;
	//last_frame = params.max_pathlength_fwd -1;
	//reference_frame = 0;

	loadAllClouds();
	motions = MotionHypotheses(params.numHypo, numClouds(), reader_->getDescription());

}


explainMe_algorithm_withFeatures::~explainMe_algorithm_withFeatures(void)
{
}

bool hasEnding(std::string const &fullString, std::string const &ending) {
	if (fullString.length() >= ending.length()) {
		return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
	}
	else {
		return false;
	}
}

void explainMe_algorithm_withFeatures::initState(std::string & segFile, std::string & motionFile){
	if (hasEnding(segFile, "seg2") && hasEnding(motionFile, "mo2")){
		std::vector<std::vector<Eigen::Matrix4f>> mos;
		Chronist::load_v2(segFile, motionFile, trajectories, w, mos);
		step3_updateDenseSeg_from_sparse();
		std::cout << "dense seg initted. " << "Have " << denseSegmentation_allClouds.size() << " frames " << mos.size() << "mosets, and " << mos[0].size() << "matrisces per mo.\n";
		motions.set(mos, denseSegmentation_allClouds);
	}
	else{
		Chronist::load(segFile, trajectories, w);
		
		std::vector<std::vector<Eigen::Matrix4f>> mos;
		std::cout << "initin denseSeg....\n";
		step3_updateDenseSeg_from_sparse();
		if (motionFile.length() > 0){
			std::cout << "initin Motions....\n";
			Chronist::load(motionFile, mos);
			motions.set(mos, denseSegmentation_allClouds);
		}
	}
}

void explainMe_algorithm_withFeatures::step_1_and_2(Initializer::Ptr init_method, bool winnerTakesAll, stats * statistics){
	if (abortComputation){
		return;
	}

	init_method->computeInitialisation(allClouds_asGrids, *reader->getDescription(),trajectories,w, statistics);
		
	std::cout << "Max and min of the init seg: \n" <<
		w.colwise().maxCoeff() << "\n" <<
		w.colwise().minCoeff() << "\n";
	int dim = w.cols();//numHypo
	motions = MotionHypotheses(dim, numClouds(), reader->getDescription());

	if (winnerTakesAll){
		int winner; float val;
		for (int i = 0; i < w.rows(); i++){
			val = w.row(i).maxCoeff(&winner);
			w.row(i).setZero();
			w(i, winner) = 1;
		}
	}
}



void explainMe_algorithm_withFeatures::interpolateWeights_helper(explainMe::Cloud::Ptr cloud,
				MatrixXf_rm & interpolated_weights,
				MatrixXf_rm & w,//weights per trajectory, to interpolate.
				int cloud_idx, bool useSeedsOnly)
{
	if (cloud_idx < 0 || cloud_idx >= numClouds()){
		return;
	}
	

	//compute cloud of paths at frame.
	pcl::PointCloud<pcl::PointXYZINormal>::Ptr path_cloud(new pcl::PointCloud<pcl::PointXYZINormal>());
	std::vector<int> idx;
	path_cloud->reserve(trajectories.size());
	idx.reserve(trajectories.size());
	interpolated_weights.resize(cloud->size(), w.cols());
	
	for(int i = 0; i < trajectories.size(); i++){
		if(useSeedsOnly){
			if(trajectories[i].seedCloud() == cloud_idx){
				path_cloud->push_back(trajectories[i][cloud_idx]);
				idx.push_back(i);
			}
		}
		else if (trajectories[i].cloudInRange(cloud_idx) && trajectories[i].numEl() >= 1){
			path_cloud->push_back(trajectories[i][cloud_idx]);
			idx.push_back(i);
		}
	}


	if(path_cloud->size() < 10){
		//no weights to interpolate...
		for(int i = 0; i < cloud->size(); i++){
			interpolated_weights.row(i).setConstant(0.1);
		}
		return; //done, nothing to interpolate.
	}

	//interpolate the weights from the paths.
	mySimpleKdTree myTree;
	myTree.setData<pcl::PointCloud<pcl::PointXYZINormal>::Ptr>(path_cloud);
	myTree.buildTree();


#pragma omp parallel
{
	simpleNNFinder nnFinder(myTree);
	std::vector<distSqr_idx> nns;
	pcl::PointXYZ p;
	RowVectorXf w_current(w.cols());

	float sum, weight;
#pragma omp for
	for(int i = 0; i < cloud->size(); i++){
		interpolated_weights.row(i).fill(0);
		p.getVector4fMap() = cloud->at(i).getVector4fMap();
		if(p.z==0 || (p.z*0 != 0))
		{
			continue;
		}
		pxyz pt(p.x, p.y, p.z);
		nnFinder.findkNN(pt, 5, nns, 128);

		w_current.fill(0);
		sum = 0;
		for(int n = 0; n < nns.size(); n++){
			weight = 1;
			//weight = pow(2,-sq_dist[n]/0.1);
			sum+= weight;
			weight = pow(2.0,-nns[n].dist_sqr/0.1);
			//w_current = w_current + w[idx[nbrs[n]]]*weight;
			w_current = w_current + w.row(idx[nns[n].index])*weight;
		}
		w_current/= (sum + 1e-3);
		
		interpolated_weights.row(i) = w_current;
	}
}
}

void explainMe_algorithm_withFeatures::step3_updateDenseSeg_from_sparse(bool use_seeds_only){
	if (abortComputation){
		return;
	}
	//...and compute all weights, based on the current path weights.
	//for(int frm = first_frame, idx = 0; frm <= last_frame; frm++, idx++){
	for (int idx = 0; idx < numClouds(); idx++)
	{
		auto & ws = denseSegmentation_allClouds[idx];
		interpolateWeights_helper(allClouds[idx], ws,w, idx,use_seeds_only);
		std::cout << (idx == numClouds()-1 ? "!\n":".");
	}
}


void explainMe_algorithm_withFeatures::interpolateWeights_onCloud(
		int cloudIdx,  
		MatrixXf_rm & interpolated_weights,
		MatrixXf_rm & w_to_interpolate,bool seeds_only)
{
	if (abortComputation){
		return;
	}
	if (cloudIdx < 0 || cloudIdx >= numClouds()){
		return;
	}

	*c1 = *allClouds_asGrids[cloudIdx];
	interpolateWeights_helper(c1, interpolated_weights, w_to_interpolate, cloudIdx, seeds_only);
}

void extract(explainMe::Cloud::Ptr cloud, Eigen::ArrayXXi & seg, int who, explainMe::Cloud::Ptr target)
{

	target->clear();
	for(int i = 0; i < seg.rows(); i++){
		for(int j = 0; j < seg.cols(); j++){
			if(seg(i,j) == who && cloud->at(i,j).z*0 == 0 &&cloud->at(i,j).z !=0 
				&& cloud->at(i,j).getNormalVector3fMap().squaredNorm() * 0 == 0){
				target->push_back(cloud->at(i,j));
			}
		}
	}
}


void explainMe_algorithm_withFeatures::loadAllClouds(){
	//loading the specified cloud set.
	
	std::cout << "Loading all frames...";
	allClouds.clear();
	allClouds_asGrids.clear();
	allClouds_kdTree.clear();

	//allDepthFrames.resize(max_pathlength_fwd + max_pathlength_bwd + 1);
	//load all clouds , remove NAN's
	//for(int frm = -(params.max_pathlength_bwd-1), frm_idx = 0; frm < params.max_pathlength_fwd; frm++, frm_idx++){

	//int numClouds = (params.last_frame - params.first_frame) / params.delta_frame + 1;
	for (int frm = params.first_frame; frm <= params.last_frame; frm += params.delta_frame)
	{
		if (abortComputation){
			return;
		}

		//reader->loadFrame(params.central_frame + frm*params.delta_frame,params.cleanUpClouds, params.maxDistance, false);
		reader->loadFrame(frm, params.cleanUpClouds, params.maxDistance, false);

		explainMe::Cloud::Ptr cloud(new explainMe::Cloud());
		explainMe::Cloud::Ptr cloud_withNans = reader->getPointCloud();
		cloud-> reserve(cloud_withNans->size());
		int idx =0;
		for(auto it = cloud_withNans->begin(); it != cloud_withNans->end(); it++, idx++)
		{
			if(it->z != 0 && it->z*0 == 0 && it->getNormalVector3fMap().norm() > 0.99){
				cloud->push_back(*it);
			}
		}

		allClouds.push_back(cloud);
		allClouds_asGrids.push_back(explainMe::Cloud::Ptr(new explainMe::Cloud()));
		allClouds_asGrids.back()->resize(cloud_withNans->size());
		*(allClouds_asGrids.back()) = * cloud_withNans;

	}
	
	//paralelized build of all kdtrees:
	const int nClouds = numClouds();

	denseSegmentation_allClouds.resize(nClouds);

	allClouds_kdTree.resize(nClouds);
	#pragma omp parallel
	{
	#pragma omp for
		for(int i = 0; i <nClouds; i++)
		{
			allClouds_kdTree[i].setData<explainMe::Cloud::Ptr>(allClouds[i]);
			allClouds_kdTree[i].buildTree();
		}
	}


	std::cout << "Loaded all frames.\n";
	
	motions = MotionHypotheses(params.numHypo, numClouds(), reader->getDescription());
}

void explainMe_algorithm_withFeatures::step4_computeMotionHypothesesFromDenseSegmentation(
		bool updateDenseSegFromSparseSegFirst,
		bool genHypByLabelShift,
		bool genHypByCarryOver)
{
	if (abortComputation){
		return;
	}
	StopWatch computeHyposTimer;
	computeHyposTimer.restart();
	if(updateDenseSegFromSparseSegFirst){
		step3_updateDenseSeg_from_sparse();
	}

	//motions.computeHypothesesSetFromDenseSegmentation(
	CATCH_AND_RETHROW( motions.computeHypothesesSetFromDenseSegmentation_segFirst(
		allClouds,
		denseSegmentation_allClouds, allClouds_kdTree, REFERENCE_CLOUD,
		params.maxItICP, params.alpha_point2plane, params.alpha_point2point,
		params.icp_culling_dist, params.k_generated_hyps,params.lambda_generate_hyp,
		genHypByLabelShift, genHypByCarryOver, params.icp_type); , ;)

	computeHyposTimer.stop();
	std::cout << "Time of the whole MotionHypo shabang " << computeHyposTimer.total();
}


//compute the likelihood of a point being occluded.
float inline occlusion_probability(const Eigen::Vector3f & new_p, 
	const Eigen::Vector3f & new_n, RangeSensorDescription & d,
	const explainMe::Cloud::Ptr organized_cloud,
	const float & clamp_distsqr)
{
	float occlusion_prob;
	float coord_i, coord_j;
	int floor_i, floor_j;
	d.project(new_p.x(), new_p.y(), new_p.z(), coord_i, coord_j);
	floor_i = std::floor(coord_i); floor_j = std::floor(coord_j);
	if (floor_i >= 0 && floor_j >= 0 && floor_i < d.width() && floor_j< d.height())
	{
		if (organized_cloud->at(floor_i, floor_j).z > 0.0001)
		{
			float observed_z = organized_cloud->at(floor_i, floor_j).z;
			float cos_view_angle = -new_n.dot(new_p) / new_p.norm();
			//p_unoccluded = //(observed_z - stddev_z < new_p.z ?		1				:0) *
			occlusion_prob = 
				(observed_z < new_p.z() ? clamp_distsqr / (clamp_distsqr + (new_p.z() - observed_z)*(new_p.z() - observed_z)) : 1)*
				//if the normal points not enough to the camera direction...
				(cos_view_angle >0.3 ? 1 : (cos_view_angle  > 0 ? cos_view_angle / 0.3 : 0));
			
			//occlusion_prob = 1 - p_unoccluded;
			occlusion_prob = 1 - occlusion_prob;

		}
		else{
			//missing data...... use some constant:
			occlusion_prob = 0.9;//1;//0.5						
		}
	}
	else{
		//outside frustum
		occlusion_prob = 1; //or something... distance to frustum, e.g.
	}

	return occlusion_prob;
}
//computes the occlusion probability as well as the alignment error to the next potential correspondence
//if following the alignments of said label.
//the error is absolute projection error in the previous frame * alpha + incremental mapping error to next frame.
//This makes sense because both distances are clamped. That is, if all the motion are off by too much anyhow
//it will constantly add the maximal absolute error, and the motions will be discriminated only by the
//incremental mapping error.
void explainMe_algorithm_withFeatures::computeMotionEnergy_with_reprojection(
	std::vector<pcl::PointXYZINormal> & points,
	//the index of the clouds each point was sampled from.
	std::vector<int> & point_cloudIdx,
	std::vector<Eigen::Matrix4f> & motion,
	//kdtrees containing int references to the points in the unstructured cloud
	//that are allowed to be treated as correpondences. 
	std::vector<mySimpleKdTree> & Data_label,
	std::vector<explainMe::Cloud::Ptr> & allClouds,
	//clouds, organized (as grid), to determne occlusions
	RangeSensorDescription & d,
	std::vector<explainMe::Cloud::Ptr> & allClouds_asGrids,
	bool random_sample,
	explainMe_algorithm_withFeatures::Parameters & params,

	Eigen::VectorXf & point_motionError,
	std::vector<float> & total_observation_weight) 
{
	//std::cout << points.size();
	//std::cout << points.size();

	//this should be yet another parameter.
	float alpha_absolute_dist = 1;
	bool use_sqrt = false;
	int maxNNTests = 128;

	float clamp_distsqr = params.clamp_distSqr;

	auto motion_inv = motion;
	for (int i = 0; i < motion_inv.size(); i++){
		motion_inv[i] = motion[i].inverse();
	}
	total_observation_weight.clear();
	total_observation_weight.resize(points.size(), 0.f);
	point_motionError.resize(points.size());
	point_motionError.fill(0);
	
#pragma omp parallel
	{

	//iterate over frames.
#pragma omp for
	for (int alignment = 0; alignment < motion.size(); alignment++)
	{

		
		auto & last_tree = (alignment>0 ? Data_label[alignment - 1] : Data_label[alignment]);
		auto & kdTree = Data_label[alignment];
		auto & next_tree = (alignment + 1 <motion.size() ?
			Data_label[alignment + 1] : Data_label[alignment]);

		//std::cout << alignment <<",";


			simpleNNFinder nnfinder(kdTree), nnfinder_last(last_tree), nnfinder_next(next_tree);
			distSqr_idx nn;
			float occlusion_prob;
			float new_point_motion_error;
			Eigen::Vector3f new_n, new_p;
			Eigen::Matrix4f mat;

			for (int i = 0; i < points.size(); i++)
			{
				
				if (point_cloudIdx[i] == alignment){
					//easy, no error anyhow
					continue;
				}
				//sample frames only to get rid of squared complexity.
				//the sampling introduces bias or a weighting that favors how well the motion fits locally,
				//else the samples would have to be weighted by their probability.
				//so this actually induces a weighting relative to seed frame closeness.
				unsigned int dist_seed = std::abs(point_cloudIdx[i] - alignment);
				bool useFrame;
				if(random_sample)
					useFrame = dist_seed <= 4 || (dist_seed <= 8 && (rand() % 4 == 0)) || (dist_seed <= 16 && (rand() % 12 == 0)) || rand() % 32 == 0;
				else
					useFrame = (dist_seed <= 4 || dist_seed == 8 || dist_seed == 16 || dist_seed == 32 || dist_seed % 64 == 0);

				if (!useFrame){
					continue;
				}
				//compute occlusion probability using the predicted position.
				//==============
				//transform to frame.
				mat = motion[alignment] * motion_inv[point_cloudIdx[i]];
				new_p = (mat * points[i].getVector4fMap()).topLeftCorner<3, 1>();
				new_n = mat.topLeftCorner<3, 3>() * points[i].getNormalVector3fMap();

				occlusion_prob = occlusion_probability(new_p, new_n, d, allClouds_asGrids[alignment], clamp_distsqr);


				float dist_previous_frame = 0;
				float dist_increment = 0;
				//update distance measure, using distance where the error to the last mapping has been subtracted.
				//=======================
				//"subtract" error from frame closer to the seed frame by first mapping the prediction in that frame to the nn. and then
				//mapping this to the next frame. (next= the frame closer to the points frame.)
				if (point_cloudIdx[i] < alignment){
					//nn in last frame: 
					mat = motion[alignment - 1] * motion_inv[point_cloudIdx[i]];//alignmentSets[hyp][seed_frames[i] + central_cloud].inverse();
					new_p = (mat * points[i].getVector4fMap()).topLeftCorner<3, 1>();
					pxyz pt(new_p.x(), new_p.y(), new_p.z());
					nn = nnfinder_last.findNN(pt, clamp_distsqr, maxNNTests);

					dist_previous_frame = nn.dist_sqr;
					dist_increment = clamp_distsqr;
					//map nn to current frame.
					//only interested in nn.dist_sqr, this is the clamp_distance if index < 0, which is ok.
					if (nn.index >= 0)
					{
						mat = motion[alignment] * motion_inv[alignment - 1];
						new_p = (mat * allClouds[alignment - 1]->at(nn.index).getVector4fMap()).topLeftCorner<3, 1>();
						//new_n = mat.topLeftCorner<3, 3>() * allClouds[alignment - 1]->at(nn.index).getNormalVector3fMap();
						pxyz pt(new_p.x(), new_p.y(), new_p.z());
						nn = nnfinder.findNN(pt, clamp_distsqr, maxNNTests);
						dist_increment = nn.dist_sqr;
					}
				}
				else if (point_cloudIdx[i] > alignment){

					//nn in next frame
					mat = motion[alignment + 1] * motion_inv[point_cloudIdx[i]];//alignmentSets[hyp][seed_frames[i] + central_cloud].inverse();
					new_p = (mat * points[i].getVector4fMap()).topLeftCorner<3, 1>();
					pxyz pt(new_p.x(), new_p.y(), new_p.z());
					nn = nnfinder_next.findNN(pt, clamp_distsqr, maxNNTests);

					dist_previous_frame = nn.dist_sqr;
					dist_increment = clamp_distsqr;
					//map nn to current frame.
					//only interested in nn.dist_sqr, this is the clamp_distance if index < 0, which is ok.
					if (nn.index >= 0)
					{
						mat = motion[alignment] * motion_inv[alignment + 1];
						new_p = (mat * allClouds[alignment + 1]->at(nn.index).getVector4fMap()).topLeftCorner<3, 1>();
						//new_n = mat.topLeftCorner<3, 3>() * allClouds[alignment + 1]->at(nn.index).getNormalVector3fMap();
						pxyz pt(new_p.x(), new_p.y(), new_p.z());
						nn = nnfinder.findNN(pt, clamp_distsqr, maxNNTests);
						dist_increment = nn.dist_sqr;
					}
				}
				//NOTE: nn.index might be undefined! (else remove radius in nn search.)
				//float dists_of_normals = (1- std::pow(allClouds[alignment]->at(nn.index).getNormalVector3fMap().dot(new_n),2))* 0.001;
				//dists_of_normals = (dists_of_normals * 0 == 0? dists_of_normals:0);
				//clamped distance increment (very robust)

				new_point_motion_error = (dist_increment >clamp_distsqr ? clamp_distsqr : dist_increment)* (1 - occlusion_prob);
				new_point_motion_error += alpha_absolute_dist*
					(dist_previous_frame > clamp_distsqr ? clamp_distsqr : dist_previous_frame)* (1 - occlusion_prob);

				float & point_motionError_i = point_motionError(i);
#pragma omp atomic
				point_motionError_i += new_point_motion_error;

				float & total_observation_weight_i = total_observation_weight[i];
#pragma omp atomic
				total_observation_weight_i += (1 - occlusion_prob);


				if (occlusion_prob * 0 != 0 || total_observation_weight[i] * 0 != 0 || occlusion_prob>1){
					std::cout << "occlusion prob with occl fails! \noccl prob \t" << occlusion_prob
						<< "\tnn dist " << nn.dist_sqr << "\t p " << new_p << "\t total w " << total_observation_weight[i] << "\n";// " \tnormal " << new_n <<
					throw std::runtime_error("Error in occl err");
				}
			}
		}
	}//end pragma omp
	
	for (int i = 0; i < points.size(); i++)
	{
		//"estimated" total error (the weight can be looked at as an estimation of the error being reliable, the errors are thus weighted by their relative reliability)
		point_motionError(i) /= (total_observation_weight[i]>1e-5 ? total_observation_weight[i] : 1e-5);
	}
}


float explainMe_algorithm_withFeatures::computeTotalEnergy(){
	std::cout << "Computing Energy...";
	if (w.size() == 0){
		return std::numeric_limits<float>::max();
	}
	//for each label
	Eigen::VectorXf per_point_errors;
	std::vector<float> per_point_obs_weights;
	RowVectorXf meanLabelError(this->denseSegmentation_allClouds[0].cols());
	Eigen::RowVectorXi	per_label_numsamples(this->denseSegmentation_allClouds[0].cols());

	//after alignments are computed, the denseseg is overlapping
	for (int label = 0; label < this->denseSegmentation_allClouds[0].cols(); label++)
	{
		if (abortComputation){
			return -1;
		}
		std::cout << "err(" << label;
		//build kdtrees
		std::vector<mySimpleKdTree> Data_label;
		Data_label.resize(allClouds.size());
		#pragma omp parallel
		{
		#pragma omp for
			for (int i = 0; i <allClouds.size(); i++)
			{
				Data_label[i].setData<explainMe::Cloud::Ptr, explainMe::MatrixXf_rm>(allClouds[i], denseSegmentation_allClouds[i], label);
				Data_label[i].buildTree();
			}
		}
						
		std::cout << ")";
		//sample points with that label
		std::vector<pcl::PointXYZINormal> points;
		std::vector<Eigen::Vector3f> normals;
		std::vector<int> cloud_idx;
		RandomLabelSampler sampler(label);
		sampler.sample(64000,
			allClouds, denseSegmentation_allClouds,
			points, normals, cloud_idx);

		std::cout << "=";
		//compute error

		if (points.size() == 0){
			meanLabelError(label) = 0;
		}
		else
		{

			CATCH_AND_RETHROW(
				computeMotionEnergy_with_reprojection(
				points, cloud_idx,
				motions.getAlignmentSet(label),
				Data_label, allClouds,
				*reader->getDescription(), allClouds_asGrids,
				true, params,
				//output
				per_point_errors, per_point_obs_weights); , ;)
			meanLabelError(label) = per_point_errors.mean();
		}
		std::cout << meanLabelError(label) << ", ";
	}
	//total error is a weigted sum of per label errors.
	per_label_numsamples.fill(0);
	int winner;
	for (int i = 0; i < this->denseSegmentation_allClouds.size(); i++){
		for (int j = 0; j < denseSegmentation_allClouds[i].rows(); j++){
			denseSegmentation_allClouds[i].row(j).maxCoeff(&winner);
			per_label_numsamples(winner) += 1;
		}
	}
	float total_error = per_label_numsamples.cast<float>().dot(meanLabelError);
	std::cout << "\nmean label errors: \t" << meanLabelError << "\n";
	std::cout << "label supportsizes:\t" << per_label_numsamples << "\n";
	std::cout << "Total error: " << total_error;
	return total_error;
}


void explainMe_algorithm_withFeatures::cast_distances_to_gc_unaries(MatrixXf_rm & hypothesis_probability, bool winner_takes_all)
{
	auto & alignmentSets = motions.getAlignmentSets();

	for (int hyp = 0; hyp < alignmentSets.size(); hyp++){
		float factor = (1 + (((int) allClouds.size()) - motions.supportSize(hyp)) *1.0 / allClouds.size());
		hypothesis_probability.col(hyp) *= factor;
	}

	RowVectorXf ones = RowVectorXf::Ones(alignmentSets.size());//params.numHypo);

	//1-prediction error / total error = hyp probability.
	for (int i = 0; i < hypothesis_probability.rows(); i++)
	{
		int best_hyp;
		hypothesis_probability.row(i) /= (1e-10 + hypothesis_probability.row(i).sum());
		hypothesis_probability.row(i) = (ones - hypothesis_probability.row(i)) / (alignmentSets.size() - 1);//(params.numHypo- 1);
		if (winner_takes_all){
			//do a hard segmentation, in that each one can only contribute to a single hyp
			float mx = hypothesis_probability.row(i).maxCoeff(&best_hyp);
			if (mx * 0 != 0){
				std::cout << "hypo prob with occl fails! \n" << "mx " << mx << " \t hypo_prob " << hypothesis_probability.row(i) << "\n";
				int a;
				std::cin >> a;
			}
			hypothesis_probability.row(i).fill(0);
			hypothesis_probability(i, best_hyp) = std::min(mx* alignmentSets.size() / 2, 1.f);// params.numHypo /2, 1.f) ; // mx;
		}
	}

	if (!winner_takes_all && hypothesis_probability.rows() >0){
		//renormalize 
		RowVectorXf min, max;
		min = max = hypothesis_probability.row(0);

		for (int i = 0; i < hypothesis_probability.rows(); i++){
			min = (min.array() < hypothesis_probability.row(i).array()).select(min, hypothesis_probability.row(i));
			max = (max.array() > hypothesis_probability.row(i).array()).select(max, hypothesis_probability.row(i));
		}
		//normalization while keeping relative errors...
		hypothesis_probability = (hypothesis_probability.array() - min.minCoeff()) / (max.maxCoeff() - min.minCoeff());

		std::cout << "cast_to_gc_unaries: Renormalized dataterm: [" << max.maxCoeff() << " " << min.minCoeff() << "] => [0,1]\n";
	}
}

void explainMe_algorithm_withFeatures::step5_2_compute_hypo_probs_gc_R(
	std::vector<pcl::PointXYZINormal> & seed_points,
	std::vector<int> & seed_cloud_idx,
	MatrixXf_rm & hypothesis_probability,
	bool winner_takes_all
	)
{
	StopWatch buildTreeTimer, preProcessTimer, datatermTimer, totalTimer;

	//TAKE RANGE INTO ACCOUNT! And maybe densities...

	totalTimer.restart();
	preProcessTimer.restart();
	
	//uglyness.
	/*auto seed_cloud_idx = seed_frames;
	for (int i = 0; i < seed_cloud_idx.size(); i++){
		seed_cloud_idx[i] -= first_frame;
	}*/
			
	auto & alignmentSets = motions.getAlignmentSets();

	//PROTOTYPE don't always realloc.
	std::vector<float> occlusion_prob;
	occlusion_prob.resize(seed_points.size());
	std::vector<float> totalWeight;
	totalWeight.resize(seed_points.size(),0);	
	auto & s = * reader->getDescription();
	hypothesis_probability.resize(seed_points.size(), alignmentSets.size());//params.numHypo);
	hypothesis_probability.fill( params.occlusion_basepenalty) ;//1e-3 //0

	preProcessTimer.stop();

	//for each hypothesis: 
	for(int hyp = 0; hyp <  alignmentSets.size(); hyp++){
		if (abortComputation){
			return;
		}
		//compute predicted trajectories and projected.
		std::cout << "Computing dataterm for hypo " << hyp << "...";

		totalWeight.clear();
		totalWeight.resize(seed_points.size(),0);

		Eigen::VectorXf per_point_error;
		computeMotionEnergy_with_reprojection(seed_points, seed_cloud_idx, alignmentSets[hyp].alignments, allClouds_kdTree, allClouds, s, allClouds_asGrids, true, params, per_point_error, totalWeight);
		std::cout << "!";
		hypothesis_probability.col(hyp) = per_point_error;
		
	}
	
	cast_distances_to_gc_unaries(hypothesis_probability, winner_takes_all);

	
	totalTimer.stop();

	std::cout << "\nDataterm times: pre" << preProcessTimer.total() << "\nbuildTree " << buildTreeTimer.total()
		<< "\nompLopp(nn+ comp): " << datatermTimer.total() << "\nTotal: " << totalTimer.total() 
		<< " unaccounted " << totalTimer.total()- preProcessTimer.total() - buildTreeTimer.total() - datatermTimer.total() << "\n\n";
}


void explainMe_algorithm_withFeatures::step5_1_select_seed_points(
	std::vector<pcl::PointXYZINormal> &seed_points, 
	std::vector<Eigen::Vector3f> &seed_normals,
	std::vector<int> & seed_frame,
	CloudSampler * sampler
	){
		//choose seed points
	seed_points.reserve(trajectories.size());
	seed_normals.reserve(trajectories.size());
	seed_frame.reserve(trajectories.size());
	std::cout << "explainMe_algorithm_withFeatures.cpp" << __LINE__ << " resampling Trajectories \n";
	sampler->sample(params.samplesPerFrame, allClouds, denseSegmentation_allClouds, trajectories, seed_points, seed_normals, seed_frame);
	


	//trajs, wsinitialized to correct sizes. //Todo: fix this to set it to the old segmentation.
	w.resize(trajectories.size(), motions.numHypotheses());//alignmentSets.size());//params.numHypo);
	for(int i = 0; i < w.rows(); i++){
		w.row(i) = RowVectorXf::Random(motions.numHypotheses());
		w.row(i) = w.row(i).array().abs();
		w.row(i) = w.row(i)/w.row(i).sum();
	}
	
}

void explainMe_algorithm_withFeatures::checkTrajectorySeeds(){
	std::cout << "\n***Trajectory point idxs are...";
	for (auto & t : trajectories){
		t.originalPointIdx();
	}
	std::cout << "ok!\n";
}
void explainMe_algorithm_withFeatures::step5_1_select_seed_points(
	std::vector<pcl::PointXYZINormal> &seed_points,
	std::vector<Eigen::Vector3f> &seed_normals,
	std::vector<int> & seed_frame,
	std::vector<int> & seed_point_index,
	CloudSampler * sampler
	){
	//choose seed points
	seed_points.reserve(trajectories.size());
	seed_normals.reserve(trajectories.size());
	seed_frame.reserve(trajectories.size());

	std::cout << "explainMe_algorithm_withFeatures.cpp" << __LINE__ << " resampling Trajectories \n";
	sampler->sample(params.samplesPerFrame, allClouds, denseSegmentation_allClouds, trajectories, seed_points, seed_normals, seed_frame, seed_point_index);



	//trajs, wsinitialized to correct sizes. //Todo: fix this to set it to the old segmentation.
	w.resize(trajectories.size(), motions.numHypotheses());//alignmentSets.size());//params.numHypo);
	for (int i = 0; i < w.rows(); i++){
		w.row(i) = RowVectorXf::Random(motions.numHypotheses());
		w.row(i) = w.row(i).array().abs();
		w.row(i) = w.row(i) / w.row(i).sum();
	}
	
}

void explainMe_algorithm_withFeatures::step5_2_initialLabeling(
		std::vector<pcl::PointXYZINormal> &seed_points, 
		std::vector<Eigen::Vector3f> &seed_normals,
		std::vector<int> & seed_cloud,
		MatrixXf_rm & hypothesis_probability,
		std::vector<int> & labeling
	){

	//possibility two (data term)
	CATCH_AND_RETHROW(
		step5_2_compute_hypo_probs_gc_R(seed_points, seed_cloud, hypothesis_probability, false); , ;)

		if (abortComputation){
			return;
		}
	
	labeling.resize(seed_points.size());
	//w = hypothesis_probability;
	//do the initial dataterm based labeling.
	for(int i = 0; i < seed_points.size(); i++){
		int bla;	
		hypothesis_probability.row(i).maxCoeff(& bla);
		labeling[i] = bla;
		w.row(i).setZero();
		w(i, bla) = 1;
	}

}

void explainMe_algorithm_withFeatures::set_sparse_w_to_MAP(MatrixXf_rm & hypothesis_probability)
{
	for (int i = 0; i < hypothesis_probability.rows(); i++){
		int bla;
		hypothesis_probability.row(i).maxCoeff(&bla);
		w.row(i).setZero();
		w(i, bla) = 1;
	}
}

void explainMe_algorithm_withFeatures::step5_3_do_alpha_beta_expansion(
		std::vector<pcl::PointXYZINormal> &seed_points, 
		std::vector<Eigen::Vector3f> &seed_normals,
		std::vector<int> & seed_cloud,
		std::vector<int> & seed_point_idx,
		MatrixXf_rm & hypothesis_probability
){

	//params
	int k_nn = params.k_nn;// 10;//3;
	float sigma_dist =params.sigma_dist;// 5e-2; //how fast the smoothness term decays
	float lambda_dataterm = params.lambda_dataTerm; //weight of the data term.

	int numHyps = motions.numHypotheses();//alignmentSets.size();// params.numHypo

	//for each frame, do alpha-beta swapping, separately:

	std::vector<pcl::PointXYZINormal> points_frame;
	std::vector<int> point_index, labeling;
	//MatrixXf_rm hyp_prob_frame; 

	//1. Gather seedpoints in that frame
	for(int cloud = 0; cloud < numClouds(); cloud++){
		points_frame.clear();
		point_index.clear();
		labeling.clear();
				
		//Relevant point set and initial labeling quess:
		for(int i = 0; i < seed_points.size(); i++){
			if (seed_cloud[i] == cloud)
			{
				points_frame.push_back(seed_points[i]);
				//hyp_prob_frame.push_back(hypothesis_probability.row(i));
				point_index.push_back(i);
				int bla;
				hypothesis_probability.row(i).maxCoeff(& bla);
				labeling.push_back(bla);
			}
		}

		//do alpha-beta swap:
		//should do this until (near) convergence, not fixed amount of times.
		for(int it_alpha_beta_swap  = 0; it_alpha_beta_swap <5; it_alpha_beta_swap++){
		for(int label_alpha = 0; label_alpha < numHyps -1; label_alpha++)
		{
			for(int label_beta = label_alpha +1; label_beta < numHyps; label_beta++){
		

				typedef Graph<float,float,float> GraphType;
				GraphType *g = new GraphType( points_frame.size(), points_frame.size()*4);


				//data terms, hyp_prob not well choosen.
				for(int i = 0; i < points_frame.size(); i++){
					if(labeling[i] == label_alpha || labeling[i] == label_beta){
						g -> add_node(); 
						g -> add_tweights(i, lambda_dataterm*-logf(1e-10 + std::max(hypothesis_probability(point_index[i],label_alpha),0.f)),
											lambda_dataterm*-logf(1e-10 + std::max(hypothesis_probability(point_index[i],label_beta),0.f)));

						//about adding penalty of non alpha beta neighbors, as in Boykov paper. The additional penalty
						// would be the same for both labels, so it will not change anythin
					}
					else{
						g -> add_node(); 
						//idc
						g -> add_tweights(i, 1.f, 0.f);
					}
				}
		

				//pairwise terms and graph structure.
				mySimpleKdTree kdTree;
				kdTree.setDataT<pcl::PointXYZINormal>(points_frame);
				simpleNNFinder nn(kdTree);
				std::vector<distSqr_idx> dist_sqr_idx;
				for(int i = 0; i < points_frame.size(); i++){

					//the query point is part of the kdtree an will be rturned.
					pxyz pt(points_frame[i].x,points_frame[i].y,points_frame[i].z);
					nn.findkNN(pt,k_nn+1,dist_sqr_idx);

					for(int j = 0; j < dist_sqr_idx.size(); j++){
						//ignore the point if it is itself.
						if(dist_sqr_idx[j].index == i || 
							!(labeling[i] == label_alpha || labeling[i] == label_beta) || 
							!(labeling[dist_sqr_idx[j].index ] == label_alpha || labeling[dist_sqr_idx[j].index ] == label_beta)){
							continue;
						}
						float p_same = sigma_dist/(sigma_dist + dist_sqr_idx[j].dist_sqr);
						p_same *= points_frame[i].getNormalVector3fMap().dot(points_frame[dist_sqr_idx[j].index].getNormalVector3fMap());
						p_same = -logf( std::max(1-p_same, 0.f) + 1e-10);
						g -> add_edge(i, dist_sqr_idx[j].index, p_same,p_same);
					}
				}


				//std::cout << "done\n Running graphcut...";
				//max flow, min cut,
				float flow = g->maxflow();
				//printf("Flow = %f\n", flow);

				//update labels..
				for(int i = 0; i < point_index.size(); i++){
					if(!(labeling[i] == label_alpha || labeling[i] == label_beta)){
						continue;
					}
					if(g ->what_segment(i) == GraphType::SOURCE){
						labeling[i] = label_beta;
					}
					else{
						labeling[i] = label_alpha;
					}
				}

				delete g;
			} //end graphcut for one pair
		}//end loop over first label
		}//end loop of alphabeta swaps.

		//alpha beta swap finished. update weights. (hard seg)

		for(int i = 0; i < point_index.size(); i++){
			hypothesis_probability.row(point_index[i]).fill(0);
			hypothesis_probability(point_index[i],labeling[i]) = 1;
		}
	}

	//recompute trajectories (simply use the prediction)
	//these define the sparse segmentation.
	std::cout << "explainMe_algorithm_withFeatures.cpp" << __LINE__ << " recomputing Trajectories \n";
	trajectories.clear();

	for(int i = 0; i < hypothesis_probability.rows(); i++)
	{
		//paths.push_back(std::vector<pcl::PointXYZ>());
		trajectories.push_back(trajectory_R(seed_points[i], seed_cloud[i], seed_point_idx[i]));
		//trajectories[i].resize(first_frame, last_frame);

		//the trajectories are only used for interpolation, restrict their lengths...
		trajectories[i].resize(std::max(0, seed_cloud[i] - 3*params.delta_seedFrame), 
			std::min(numClouds()-1,  seed_cloud[i] + 3*params.delta_seedFrame));
						
		int best_hyp;
		hypothesis_probability.row(i).maxCoeff(& best_hyp);

		auto & alignmentSet_best = motions.getAlignmentSet(best_hyp);//alignmentSets[best_hyp]
		for(int cl = 0; cl < allClouds.size(); cl++){
			if (!trajectories[i].cloudInRange(cl)){
				continue;
			}
			if(!motions.isHypInRange(best_hyp,cl))
			{
				continue;
			}
			Eigen::Matrix4f seed_to_cl =alignmentSet_best[cl] * alignmentSet_best[seed_cloud[i]].inverse();
			trajectories[i][cl].getVector4fMap() = /*alignmentSets[best_hyp][cl] */ seed_to_cl * seed_points[i].getVector4fMap();
			trajectories[i][cl].getNormalVector3fMap() = seed_to_cl.topLeftCorner<3,3>() * seed_points[i].getNormalVector3fMap();
		}
	}
	w= hypothesis_probability;

}


void computeNNGraph(std::vector<pcl::PointXYZINormal> &seed_points, 
	std::vector<int> & seed_cloud, int numClouds,
	int k_spatial, int k_temporal,
	//output
	std::vector<std::vector<int>> & nns,
	std::vector<std::vector<int>> & nns_temporal
	){

	nns.resize(seed_points.size()); //store the indices of the neighboring seed points.
	nns_temporal = nns;

	std::vector<pxyz> points_frame, points_lastFrame, points_nextFrame;
	mySimpleKdTree tree, next_tree, last_tree;
	std::vector<distSqr_idx> dist_sqr_idx;

	//initialize kdtree
	if(numClouds>0){
		for(int i = 0; i < seed_points.size(); i++){
			if(seed_cloud[i] == 0/*first cloud*/)
			{
				points_nextFrame.push_back(pxyz(seed_points[i].x,seed_points[i].y,seed_points[i].z,i));
			}
		}
		next_tree.setData(points_nextFrame);
		next_tree.buildTree();
	}

	for (int cloud = 0; cloud < numClouds; cloud++){
		
		last_tree = tree; points_lastFrame = points_frame;
		tree = next_tree; points_frame = points_nextFrame;

		//Compute stuff for next frame:
		points_nextFrame.clear();
		if(cloud != numClouds-1){
			for(int i = 0; i < seed_points.size(); i++){
				if(seed_cloud[i] == cloud +1)
				{
					points_nextFrame.push_back(pxyz(seed_points[i].x,seed_points[i].y,seed_points[i].z,i));
				}
			}
			next_tree.setData(points_nextFrame);
			next_tree.buildTree();
		}

		//gather nns in last frame, if existent
		if(cloud> 0){
			simpleNNFinder nnFinder(last_tree);
			for(int i = 0; i < points_frame.size(); i++){
				nnFinder.findkNN(points_frame[i],k_temporal, dist_sqr_idx);
				for(int j = 0; j < dist_sqr_idx.size(); j++){
					nns_temporal[points_frame[i].index].push_back(dist_sqr_idx[j].index);

					//assert
					if (seed_cloud[points_frame[i].index] - 1 != seed_cloud[dist_sqr_idx[j].index]){
						std::cout << __FILE__ << "  line " << __LINE__ << "\n";
						throw std::runtime_error("index mismatch bug");
					}
				}
			}
		}
		//gather nns in this frame
		{
			simpleNNFinder nnFinder(tree);
			for(int i = 0; i < points_frame.size(); i++){
												//will find itself!...
				nnFinder.findkNN(points_frame[i],k_spatial+1, dist_sqr_idx);
				for(int j = 0; j < dist_sqr_idx.size(); j++){
					nns[points_frame[i].index].push_back(dist_sqr_idx[j].index);
				}
			}
		}
		//gather nns in next frame
		if(cloud +1 < numClouds){
			simpleNNFinder nnFinder(next_tree);
			for(int i = 0; i < points_frame.size(); i++){
				nnFinder.findkNN(points_frame[i],k_temporal, dist_sqr_idx);
				for(int j = 0; j < dist_sqr_idx.size(); j++){
					nns_temporal[points_frame[i].index].push_back(dist_sqr_idx[j].index);

					//assert
					if (seed_cloud[points_frame[i].index] + 1 != seed_cloud[dist_sqr_idx[j].index]){
						std::cout << __FILE__ << "  line " << __LINE__ << "\n";
						throw std::runtime_error("index mismatch bug");
					}
				}
			}
		}
	}
}

void explainMe_algorithm_withFeatures::step5_3_do_alpha_beta_expansion_one_global(
	std::vector<pcl::PointXYZINormal> &seed_points, 
	std::vector<int> & seed_cloud,
	std::vector<int> & seed_point_idx,
	MatrixXf_rm & hypothesis_probability){

	StopWatch maxFlowMinCutTimer, nbrhoodTimer, graphSetupTimer, totalTimer, stuffTimer;	
	nbrhoodTimer.restart();
	totalTimer.restart();
		//params to be.
	int k_nn = params.k_nn;// 10;//3;
	//float sigma_dist = 5e-3;//5e-2
	float sigma_dist = params.sigma_dist;// 5e-2;
	float lambda_dataterm = params.lambda_dataTerm;// 5; //weight of the data term.
	float lambda_smoothness_temporal = params.lambda_smoothness_temporal; //1
	float lambda_smoothness_spatial = params.lambda_smoothness_spatial;//1;
	

	int numHyps = motions.numHypotheses();//alignmentSets.size();// params.numHypo
	//for each frame, do alpha-beta swapping, separately:

	std::vector<int> labeling;
	labeling.resize(seed_points.size());

	std::vector<std::vector<int>> nns,nns_temporal;

	computeNNGraph(seed_points, 
		seed_cloud, numClouds(),
		k_nn, k_nn,
	//output
	 nns,nns_temporal);
	
	//graphcut, initial labeling
	for(int i = 0; i < seed_points.size(); i++){
		int bla;
		hypothesis_probability.row(i).maxCoeff(& bla);
		labeling.push_back(bla);
	}
	nbrhoodTimer.stop();
		//do alpha-beta swap:
		//should do this until (near) convergence, not fixed amount of times.
	for(int it_alpha_beta_swap  = 0; it_alpha_beta_swap <5; it_alpha_beta_swap++){
		for(int label_alpha = 0; label_alpha < numHyps -1; label_alpha++)
		{
			for(int label_beta = label_alpha +1; label_beta < numHyps; label_beta++){
				if (abortComputation){
					return;
				}
		
				typedef Graph<float,float,float> GraphType;
				GraphType *g = new GraphType( seed_points.size(), seed_points.size()*k_nn*4);


				//data terms.
				for(int i = 0; i < seed_points.size(); i++){
					if(labeling[i] == label_alpha || labeling[i] == label_beta){
						g -> add_node(); 
						g -> add_tweights(i, lambda_dataterm *-logf(1e-10 + std::max(hypothesis_probability(i,label_alpha),0.f)),
											lambda_dataterm *-logf(1e-10 + std::max(hypothesis_probability(i,label_beta),0.f)));

						//about adding penalty of non alpha beta neighbors, as in Boykov paper. The additional penalty
						// would be the same for both labels, so it will not change anythin
					}
					else{
						g -> add_node(); 
						//i dont care
						g -> add_tweights(i, 1.f, 0.f);
					}
				}
				graphSetupTimer.restart();

				//pairwise terms and graph structure.
				for(int i = 0; i < seed_points.size(); i++){
					auto & p = seed_points[i];
					//spacial neighbors
					for(int j = 0; j < nns[i].size(); j++){
						//ignore the point if it is itself.
						if(nns[i][j] == i || 
							!(labeling[i] == label_alpha || labeling[i] == label_beta) || 
							!(labeling[nns[i][j]] == label_alpha || labeling[nns[i][j]] == label_beta)){
							continue;
						}
						auto & q = seed_points[nns[i][j]];
						float p_same = sigma_dist/(sigma_dist + (p.getVector3fMap() - q.getVector3fMap()).squaredNorm());//dist_sqr_idx[j].dist_sqr);
					//	p_same *= p.getNormalVector3fMap().dot(q.getNormalVector3fMap());
						p_same *= (params.gc_use_normals? (0.5f + p.getNormalVector3fMap().dot(q.getNormalVector3fMap())/2): 1.f);

						p_same = -logf( std::max(1-p_same, 0.f) + 1e-10);
						g -> add_edge(i, nns[i][j], lambda_smoothness_spatial* p_same,lambda_smoothness_spatial*p_same);

						if (seed_cloud[nns[i][j]] != seed_cloud[i]){
							std::cout << "spatial Seed frame mismatch! " << seed_cloud[nns[i][j]] << " vs " << seed_cloud[i];
							throw std::runtime_error("Assertion failed in global graphcut");
						}
					}
					//temporal neighbors
					for(int j = 0; j < nns_temporal[i].size(); j++){
						//ignore the point if it is itself.
						if(
							!(labeling[i] == label_alpha || labeling[i] == label_beta) || 
							!(labeling[nns_temporal[i][j]] == label_alpha || labeling[nns_temporal[i][j]] == label_beta)){
							continue;
						}
						auto & q = seed_points[nns_temporal[i][j]];
						float p_same = sigma_dist/(sigma_dist + (p.getVector3fMap() - q.getVector3fMap()).squaredNorm());//dist_sqr_idx[j].dist_sqr);
						p_same *= p.getNormalVector3fMap().dot(q.getNormalVector3fMap());
						p_same = -logf( std::max(1-p_same, 0.f) + 1e-10);
						g -> add_edge(i, nns_temporal[i][j], lambda_smoothness_temporal* p_same, lambda_smoothness_temporal* p_same);

						//assert
						if (seed_cloud[nns_temporal[i][j]] != seed_cloud[i] - 1 && seed_cloud[nns_temporal[i][j]] != seed_cloud[i] + 1){
							std::cout << "temporal seed frame mismatch! " << seed_cloud[nns_temporal[i][j]] << " vs " << seed_cloud[i];
							std::cout << __FILE__ << "  line " << __LINE__ << "\n";
							throw std::runtime_error("Assertion failed in global graphcut");
						}
					}
					//far away neighbor. based on the current labeling.
					//NN(transformation(randFrame, label[i])*seed[i],
				}
				graphSetupTimer.stop();
				maxFlowMinCutTimer.restart();
				//std::cout << "done\n Running graphcut...";
				//max flow, min cut,
				std::cout << "-";
				float flow = g->maxflow();
				//printf("Flow = %f\n", flow);
				maxFlowMinCutTimer.stop();

				stuffTimer.restart();
				//update labels..
				for(int i = 0; i < seed_points.size(); i++){
					if(!(labeling[i] == label_alpha || labeling[i] == label_beta)){
						continue;
					}
					if(g ->what_segment(i) == GraphType::SOURCE){
						labeling[i] = label_beta;
					}
					else{
						labeling[i] = label_alpha;
					}
				}

				delete g;
				stuffTimer.stop();
				std::cout << ".";
			} //end graphcut for one pair
		}//end loop over first label
	}//end loop of alphabeta swaps.

	stuffTimer.restart();
	//alpha beta swap finished. update weights.
	w = hypothesis_probability;
	for(int i = 0; i < seed_points.size(); i++){
		w.row(i).fill(0);
		w(i,labeling[i]) = 1;
	}
	
	
	//compute trajectories (simply use the prediction)
	//used to define a sparse labeling.
	std::cout << "\nexplainMe_algorithm_withFeatures.cpp" << __LINE__ << " recomputing Trajectories \n";
	trajectories.clear();

	for(int i = 0; i < hypothesis_probability.rows(); i++)
	{
		//paths.push_back(std::vector<pcl::PointXYZ>());
		trajectories.push_back(trajectory_R(seed_points[i], seed_cloud[i], seed_point_idx[i]));

		//the trajectories are only used for interpolation, restrict their lengths...incompatible segmentation
		trajectories[i].resize(std::max(0, seed_cloud[i] - 3 * params.delta_seedFrame),
			std::min(numClouds() - 1, seed_cloud[i] + 3 * params.delta_seedFrame));
						
		int best_hyp = labeling[i];
		//hypothesis_probability.row(i).maxCoeff(& best_hyp);

		
		auto & alignmentSet_best = motions.getAlignmentSet(best_hyp);//alignmentSets[best_hyp]
		for(int cl = 0; cl < allClouds.size(); cl++){
			if(! trajectories[i].cloudInRange(cl)){
				continue;
			}
			if(!motions.isHypInRange(best_hyp,cl))
			{
				continue;
			}
			Eigen::Matrix4f seed_to_cl = alignmentSet_best[cl] * alignmentSet_best[seed_cloud[i]].inverse();
			trajectories[i][cl].getVector4fMap() = /*alignmentSets[best_hyp][cl] */ seed_to_cl * seed_points[i].getVector4fMap();
			trajectories[i][cl].getNormalVector3fMap() = seed_to_cl.topLeftCorner<3,3>() * seed_points[i].getNormalVector3fMap();
		}

		trajectories[i].originalPointIdx();
	}
		
	stuffTimer.stop();
	totalTimer.stop();

	std::cout << "\nGraphcut timings: \n" << graphSetupTimer.total() << "\t" << nbrhoodTimer.total() 
		<< "\t" << maxFlowMinCutTimer.total() << "\t" << stuffTimer.total() 
		<< "\ntotal, unacc" << totalTimer.total() << "\t" << 
		totalTimer.total() -graphSetupTimer.total() - nbrhoodTimer.total() 
		- maxFlowMinCutTimer.total() - stuffTimer.total() << "\n";
}



void explainMe_algorithm_withFeatures::set_sparse_w_to_MAP_update_Trajs(
	explainMe::MatrixXf_rm & hypothesis_probability,
	std::vector<pcl::PointXYZINormal> &seed_points,
	std::vector<int> & seed_cloud,
	std::vector<int> & seed_point_idx)
{
	w = hypothesis_probability;
	int maxIdx;
	for (int i = 0; i < hypothesis_probability.rows(); i++){
		w.row(i).fill(0);
		hypothesis_probability.row(i).maxCoeff(&maxIdx);
		w(i, maxIdx) = 1;
	}


	//compute trajectories (simply use the prediction)
	//used to define a sparse labeling.
	std::cout << "explainMe_algorithm_withFeatures.cpp" << __LINE__ << " recomputing Trajectories \n";
	trajectories.clear();

	for (int i = 0; i < hypothesis_probability.rows(); i++)
	{
		trajectories.push_back(trajectory_R(seed_points[i], seed_cloud[i], seed_point_idx[i]));
		
		//the trajectories are only used for interpolation, restrict their lengths...incompatible segmentation
		trajectories[i].resize(std::max(0, seed_cloud[i] - 3 * params.delta_seedFrame),
			std::min(numClouds()-1, seed_cloud[i] + 3 * params.delta_seedFrame));

		int best_hyp;  hypothesis_probability.row(i).maxCoeff(&best_hyp);
	
		auto & alignmentSet_best = motions.getAlignmentSet(best_hyp);//alignmentSets[best_hyp]
		for (int cl = 0; cl < allClouds.size(); cl++){
			if (!trajectories[i].cloudInRange(cl)){
				continue;
			}
			if (!motions.isHypInRange(best_hyp, cl))
			{
				continue;
			}
			Eigen::Matrix4f seed_to_cl = alignmentSet_best[cl] * alignmentSet_best[seed_cloud[i]].inverse();
			trajectories[i][cl].getVector4fMap() = /*alignmentSets[best_hyp][cl] */ seed_to_cl * seed_points[i].getVector4fMap();
			trajectories[i][cl].getNormalVector3fMap() = seed_to_cl.topLeftCorner<3, 3>() * seed_points[i].getNormalVector3fMap();
			
		}
	}
	
}


void explainMe_algorithm_withFeatures::step_5_3_7_addDenseHyps(std::vector<std::vector<std::vector<float>>> & hyps)
{
	float thr = 0.2;
	std::cout << "Checking for outlier labels!\n";
	int num = motions.numHypotheses();
	int newNum = hyps.size();
	//Eigen::VectorXf outlier = label_likelihoods.rowwise().minCoeff().array() / label_likelihoods.rowwise().maxCoeff().array();
	//outlier = (outlier.array() < thr).select(0, Eigen::VectorXf::Ones(w.rows()));

	if (newNum == 0){
		return;
	}

	for (int j = 0; j < denseSegmentation_allClouds.size(); j++){
		denseSegmentation_allClouds[j].conservativeResize(denseSegmentation_allClouds[j].rows(), denseSegmentation_allClouds[j].cols() + newNum);
		for (int l = 0; l < newNum; l++){
			if (hyps[l][j].size() != denseSegmentation_allClouds[j].rows() || hyps[l].size() != denseSegmentation_allClouds.size()){
				std::cout << "error! size mismatch at " << __FILE__ << "," << __LINE__ << "\n";
				exit(1);
			}
		}
		for (int k = 0; k < denseSegmentation_allClouds[j].rows(); k++){
			for (int l = 0; l < newNum; l++){
				denseSegmentation_allClouds[j](k, l + num) = hyps[l][j][k];
			}
		}
	}
	w.conservativeResize(w.rows(), w.cols() + newNum);
	std::cout << "\n" << __FILE__ << "," << __LINE__ << " bad hack! dense and sparse are inconsistent now . " << "\n";
	for (int l = 0; l < newNum; l++){
		w.col(num + l).fill(0);
		motions.push_back(std::vector<Eigen::Matrix4f>(allClouds.size(), Eigen::Matrix4f::Identity()), my_range(0, allClouds.size()));
	}

}


void explainMe_algorithm_withFeatures::step_5_3_5_fuseClosebyHypotheses(MatrixXf_rm & segmentation, MatrixXf_rm & dataterm, float thr)
{
	int numHyp = segmentation.cols();
	if (numHyp != dataterm.cols()){
		std::cout << "Error in " << __FILE__ << ", line " << __LINE__ << ", Incompatible segmentation and dataterm.";
		assert(false);
	}

	//1st compute average cost of assigning label j to points labeled i,
	//or hyp to points labeled hyp_orig.
	Eigen::MatrixXf avg_cost_label_i_as_j(segmentation.cols(), dataterm.cols());
	for (int hyp_orig = 0; hyp_orig < numHyp; hyp_orig++)
	{
		std::cout << "Label switching probs " << hyp_orig << ": ";
		for (int hyp = 0; hyp < dataterm.cols(); hyp++){
			//total assignment cost for hyp_orig => hyp:
			//If hyp_orig is binary its just the sum of all dterm costs
			//of assigning hyp to the points of hyp_orig, here instead a weighted sum.
			float totalCost = (dataterm.col(hyp).array() * segmentation.col(hyp_orig).array()).sum();
			//this is normalized to a per unit weight measure, to factor out segment sizes.
			avg_cost_label_i_as_j(hyp_orig, hyp) = totalCost / segmentation.col(hyp_orig).sum();
			std::cout << totalCost << "/" << avg_cost_label_i_as_j(hyp_orig, hyp) << "  ";
		}
		std::cout << "\n";

	}

	//decide which hypotheses to fuse: set i_likes_j to one if label i would like to become part of j.
	Eigen::MatrixXi i_likes_j(numHyp, numHyp);
	i_likes_j.fill(0);
	for (int hyp_orig = 0; hyp_orig < numHyp; hyp_orig++)
	{
		//the hypothesis must already be well explained, i.e. the dataterm must be close to one.
		//if it is to badly explained any decision would be random anyhow. So better leave it.
		float reliability_hyp_orig = avg_cost_label_i_as_j(hyp_orig, hyp_orig);
		if (reliability_hyp_orig > 1 - std::max(1.f / (numHyp * 2),0.05f))//1.f / (numHyp * 2))
		{
			for (int hyp = 0; hyp < numHyp; hyp++){
				//if it is well explained, and nearly equally well explained by someone else, flag merge readiness
				if (hyp != hyp_orig && abs(avg_cost_label_i_as_j(hyp_orig, hyp) - reliability_hyp_orig) <thr){
					i_likes_j(hyp_orig, hyp) = 1;
				}
				else{
					i_likes_j(hyp_orig, hyp) = 0;
				}
			}
		}
	}

	std::cout << i_likes_j;
	//now, while there are labels who want to be merged:
	std::vector<int> winners;
	std::vector<std::vector<int>> groupies;
	while (i_likes_j.sum() > 0){

		//greedily decide who to merge with: Merge to the label that is the wanted partner for most others
		int winner;
		std::cout << "Sum: " << i_likes_j.colwise().sum().maxCoeff(&winner);
		std::cout << " Winner: " << winner << "\nGroupies: ";

		winners.push_back(winner);
		groupies.push_back(std::vector<int>());
		//a winner will not be merged with anyone. We do not do recursive merges.
		i_likes_j.row(winner).setZero();
		for (int hyp = 0; hyp < numHyp; hyp++){
			if (i_likes_j(hyp, winner) > 0) {
				//delete merge request of this hyp
				i_likes_j.row(hyp).setZero();
				std::cout << hyp << " ";
				groupies.back().push_back(hyp);
			}
		}
		std::cout << "\n";
	}

	//finally: do the fusion
	for (int i = 0; i < winners.size(); i++){
		for (int j = 0; j < groupies[i].size(); j++){
			segmentation.col(winners[i]) += segmentation.col(groupies[i][j]);
			segmentation.col(groupies[i][j]).setZero();
		}
	}

}

void explainMe_algorithm_withFeatures::step5_4_remove_spurious_hypos(float threshold)
{
	if (abortComputation){
		return;
	}
	float minSupport= std::max(trajectories.size() * 5e-3f,50.f) ;
	minSupport = (threshold < 0 ? minSupport : threshold);
	std::cout << "killing threshold: " << minSupport << "\n";
	RowVectorXf hyp_support= w.colwise().sum();
	std::vector<int> to_remove;
	int numHyp_new=0;
	for(int i = 0; i < hyp_support.cols(); i++){
		if(hyp_support(i) <  minSupport){
			std::cout << "killing hyp " << i << ",";
			to_remove.push_back(i);				
		}
		else{
			numHyp_new ++;
		}
	}



	std::cout << "\nRemoving hypos\n Old hyp support" << hyp_support << "\n";
	MatrixXf_rm w_new(w.rows(), numHyp_new);

	//Remove the labels from the motion set
	motions.remove(to_remove);

	for(int j = 0, j_to_remove = 0, j_new=0; j < w.cols(); j++){
		if(j_to_remove < to_remove.size() && to_remove[j_to_remove] == j){
			j_to_remove ++;
			continue;
		}else{
			w_new.col(j_new) = w.col(j);

			//act on the dense seg too.. that is update it if it has appropriate sizes.
			for (int i = 0; i < denseSegmentation_allClouds.size(); i++){
				if (denseSegmentation_allClouds[i].cols() == w.cols()){
					denseSegmentation_allClouds[i].col(j_new) = denseSegmentation_allClouds[i].col(j);
				}
			}

			j_new++;
			continue;
		}
	}

	std::cout << "\nNew hyp support" << w_new.colwise().sum() << "(" << motions.numHypotheses() << ")\n";

	//act on the dense seg too.. that is update it if it has appropriate sizes.
	for (int i = 0; i < denseSegmentation_allClouds.size(); i++){
		if (denseSegmentation_allClouds[i].cols() == w.cols()){
			denseSegmentation_allClouds[i].conservativeResize(denseSegmentation_allClouds[i].rows(), w_new.cols());
		}
	}

	w = w_new;
	std::cout << "Killing done.\n";
	
}

/*void explainMe_algorithm_withFeatures::step5_attribute_graphcut(){

	//via alpha_beta swaps.

	std::vector<pcl::PointXYZINormal> seed_points;
	std::vector<Eigen::Vector3f> seed_normals;
	std::vector<int> seed_frame, labeling;
	MatrixXf_rm hypothesis_probability; //TODO bad to realloc!

	RandomSampler sampler;
	step5_1_select_seed_points(seed_points,seed_normals,seed_frame, & sampler);
	if (abortComputation){
		return;
	}
	step5_2_initialLabeling(seed_points, seed_normals,seed_frame,hypothesis_probability, labeling);
	if (abortComputation){
		return;
	}
	//step5_3_do_alpha_beta_expansion(seed_points,seed_normals,seed_frame,hypothesis_probability);
	step5_3_do_alpha_beta_expansion_one_global(seed_points,seed_frame,hypothesis_probability);
	if (abortComputation){
		return;
	}
}*/

void explainMe_algorithm_withFeatures::getSegmentedOrganizedCloudAndAlignment(int hyp, 
		int i,
		explainMe::Cloud::Ptr & targetcloud, 
		Eigen::Matrix4f & alignment)
{
	auto & d = * reader->getDescription();
	int w = d.width(), h = d.height(); // targetcloud->width, h = targetcloud->height; 
	auto & points = allClouds[i]->points;
	auto & seg = denseSegmentation_allClouds[i];
	auto & organizedSeg =this->segmentation_c1;


	alignment = motions.getAlignmentSet(hyp)[i];

	organizedSeg.resize(w, h);
	organizedSeg.fill(0);
	targetcloud->resize(allClouds_asGrids[i]->size());
	* targetcloud = *allClouds_asGrids[i];
	

	int coord_i, coord_j; float ci,cj;
	for(int iseg =  0; iseg < seg.rows(); iseg++)
	{
		if(seg(iseg, hyp) > 0.5)
		{
			auto & p = points[iseg];
			d.project(p.x, p.y, p.z,ci, cj);
			coord_i = floor(ci + 0.5);
			coord_j = floor(cj+0.5);

			if(coord_i < w && coord_j < h && coord_i >=0 && coord_j >= 0){
				organizedSeg(coord_i,coord_j) = 1;
			}
			else{
				std::cout << " coords bad: " << coord_i << " vs " << w << "||" << coord_j << " vs " << h << "\n";
			}

		}
	}

	for(int i_c = 0; i_c < w; i_c++){
		for(int j_c = 0; j_c < h; j_c++){
			if(organizedSeg(i_c,j_c) != 1 || !motions.isHypInRange(hyp,i)){
				auto & p = targetcloud->at(i_c,j_c);
				p.x = p.y = p.z = std::numeric_limits<float>::quiet_NaN();
			}
		}
	}
}


void explainMe_algorithm_withFeatures::getSegmentedOrganizedCloudsAndAlignments(int hyp, 
		std::vector<explainMe::Cloud::Ptr> & clouds,
		std::vector<Eigen::Matrix4f> & alignments)
{
	clouds.clear();
	alignments.clear();
	for(int i = 0; i < allClouds_asGrids.size(); i++){
		clouds.push_back(explainMe::Cloud::Ptr(new explainMe::Cloud()));
		alignments.push_back(Eigen::Matrix4f::Identity());
		getSegmentedOrganizedCloudAndAlignment(hyp,i,clouds.back(), alignments.back());
	}
}