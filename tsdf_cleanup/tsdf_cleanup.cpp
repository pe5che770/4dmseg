#include <iostream>
#include "../explainMe_algorithm_withFeatures.h"
#include <boost/program_options.hpp>
#include "../pcdReader.h"
#ifndef USE_NO_CUDA
#include "../rawDataReaderKinect2.h"
#include "../BonnReader.h"
#endif
#include <tuple>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <fstream>
#include "../Experiment.h"
#include "../tsdfTracking.h"

namespace po = boost::program_options;

CloudReader::Ptr make_reader(std::string folder, int dataType){
	CloudReader::Ptr reader;
	if (dataType == 1){
		reader = CloudReader::Ptr(new pcdReader(folder));
	}
	else if (dataType == 3){
		reader = CloudReader::Ptr(new BonnReader(folder));
	}
	else if (dataType == 2){
		reader = CloudReader::Ptr(new pcdReader(folder, pcdReader::VIRTUAL_SCANNER));
	}
	else if (dataType == 0){
#ifndef USE_NO_CUDA
		reader = CloudReader::Ptr(new rawDataReaderKinect2(folder));
#else
		std::cout << "Error! Kinect data can only be read when cuda is enabled";
		return;
#endif // !USE_NO_CUDA
	}

	return reader;
}

void fuseHypos(explainMe_algorithm_withFeatures::Ptr alg, float thr, std::string outputFolder){
	std::vector<pcl::PointXYZINormal> seed_points;
	std::vector<Eigen::Vector3f> seed_normals;
	std::vector<int> seed_frame, labeling;
	explainMe_algorithm_withFeatures::MatrixXf_rm hypothesis_probability, origSampleWeights;
	UseOldSamples sampler1; //Better: keep old labeled samples.

	alg->params.samplesPerFrame = 16000;
	origSampleWeights = alg->w;
	std::cout << "orig weights colwise sums: " << origSampleWeights.colwise().sum()<< "\n";
	std::cout << "Motion ranges: \n";
	{
		int idx = 0;
		for (auto & mo : alg->motions.getAlignmentSets()){
			std::cout << idx << ": " << mo.range.start << " " << mo.range.stop << "\n";
		}
	}
	sampler1.sample(
			//input
			alg->params.samplesPerFrame,
            alg->allClouds,
            alg->denseSegmentation_allClouds,
            //output
            alg->trajectories,
            seed_points,
            seed_normals,
            seed_frame);// , origSampleWeights);

	//will resize the trajectories. and sample new ones, according to sampler1. And reinit w to random. Should be fixed.
	alg->step5_1_select_seed_points(seed_points, seed_normals, seed_frame, &sampler1);
	//we set the sparse segmentation to the original values.Should happen in select seed points
	alg->w = origSampleWeights;

	std::cout << "Experiment: alg has " << alg->motions.numHypotheses() << "alignment sets\n";
	alg->step5_2_initialLabeling(seed_points, seed_normals, seed_frame, hypothesis_probability, labeling);

	Chronist c(alg);
	std::cout << "doc it...";
	alg->w = hypothesis_probability;
	alg->step3_updateDenseSeg_from_sparse();
	for (int i = 0; i < alg->w.cols(); i++){
		c.document_weight(outputFolder + "/w_hyp_prob/", i);
	}
	alg->w = origSampleWeights;
	alg->step3_updateDenseSeg_from_sparse();

	std::cout << "\n hyp max" << hypothesis_probability.colwise().maxCoeff() << "\n hyp min" << hypothesis_probability.colwise().minCoeff() << "\n";
	//for each hypothesis, that is originalSampleWeights colum.
	int numHyp = origSampleWeights.cols();
	Eigen::MatrixXf reliability(origSampleWeights.cols(), hypothesis_probability.cols());
	for (int hyp_orig = 0; hyp_orig < origSampleWeights.cols(); hyp_orig++)
	{
		std::cout << "Label switching probs " << hyp_orig << ": ";
		for (int hyp = 0; hyp < hypothesis_probability.cols(); hyp++){
			//compute dataterm costs from relabeling hyp_orig with hyp. If hyp_orig is binary its just the sum of all dterm costs
			//of assigning hyp to the points of hyp_orig
			float changeProb = (hypothesis_probability.col(hyp).array() * origSampleWeights.col(hyp_orig).array()).sum();
			//this is normalized to a per unit weight measure, to factor out segment sizes.
			reliability(hyp_orig, hyp) = changeProb / origSampleWeights.col(hyp_orig).sum();
			std::cout << changeProb << "/" << reliability(hyp_orig, hyp) << "  ";
		}
		std::cout << "\n";

	}

	//decide which hypotheses to fuse: count how many would like to fuse to each hyp,
	Eigen::MatrixXi i_likes_j(numHyp, numHyp);
	i_likes_j.fill(0);
	for (int hyp_orig = 0; hyp_orig < origSampleWeights.cols(); hyp_orig++)
	{
		//the hypothesis must already be well explained.
		//if it is to badly explained we let it unfused such that it can find a better label.
		float reliability_hyp_orig = reliability(hyp_orig, hyp_orig);
		//if (reliability_hyp_orig > 1 - 1.f / (10))
		if (reliability_hyp_orig > 1 - 1.f / (10))
		{
			for (int hyp = 0; hyp < hypothesis_probability.cols(); hyp++){
				//if it is well explained, and nearly equally well explained by someone else, flag merge readiness
				if (hyp != hyp_orig && abs(reliability(hyp_orig, hyp) - reliability_hyp_orig) <thr){
					i_likes_j(hyp_orig, hyp) = 1;
				}
				else{
					i_likes_j(hyp_orig, hyp) = 0;
				}
			}
		}
	}

	std::cout << i_likes_j;
	//now, while there are numbers who want to be merged:
	std::vector<int> winners;
	std::vector<std::vector<int>> groupies;
	while (i_likes_j.sum() > 0){

		//greedily decide who to merge with: Merge to the label that is the wanted partner for most others
		int winner;
		std::cout << "Sum: " << i_likes_j.colwise().sum().maxCoeff(&winner);
		std::cout << " Winner: " << winner << "\nGroupies: ";

		winners.push_back(winner);
		groupies.push_back(std::vector<int>());
		//a winner will not be merged with anyone. We do not do recursive merges.
		i_likes_j.row(winner).setZero();
		for (int hyp = 0; hyp < numHyp; hyp++){
			if (i_likes_j(hyp, winner) > 0) {
				i_likes_j.row(hyp).setZero();
				std::cout << hyp << " ";
				groupies.back().push_back(hyp);
			}
		}
		std::cout << "\n";
	}

	//do the fusion
	for (int i = 0; i < winners.size(); i++){
		for (int j = 0; j < groupies[i].size(); j++){
			alg->w.col(winners[i]) += alg->w.col(groupies[i][j]);
			alg->w.col(groupies[i][j]).setZero();
		}
	}
	alg->step5_4_remove_spurious_hypos();

	alg->step3_updateDenseSeg_from_sparse();
	std::cout << "Finished Clean up";
}

void runIt(int dataType, std::string & folder, std::string & outputFolder,
	explainMe_algorithm_withFeatures::Parameters &params, std::string & segFile,
	std::string & motionFile, int label)
{
	std::cout << "Qapp...";
	char b;
	char * bl = & b;
	int bla1 =0;
	QApplication app(bla1, &bl);
	std::cout << "initted Qapp...";

	CloudReader::Ptr reader = make_reader(folder, dataType);
	explainMe_algorithm_withFeatures::Ptr alg(new explainMe_algorithm_withFeatures(reader, params));
	std::vector<explainMe::Cloud::Ptr> extractedClouds;
	std::vector<Eigen::Matrix4f> extractedAs;

	alg->initState(segFile, motionFile);
	std::cout << "initted Alg...";
	
	Chronist c(alg);
	std::cout << "doc it...";
	c.document_w_as_hyp_probs(outputFolder + "/w_before_fuse/");

	boost::filesystem::path dir(outputFolder);

	boost::filesystem::create_directory(dir);
		
	for (int i = 0; i < alg->motions.numHypotheses(); i++){
		std::cout << "Range: " << i << " " << alg->motions.getRange(i).start << " " << alg->motions.getRange(i).stop << "\n";
	}

	fuseHypos(alg,0.03f, outputFolder); //0.01
	alg->motions.updateRanges(alg->denseSegmentation_allClouds);

	c.document_w_as_hyp_probs(outputFolder + "/fusedSegmentation/");
	//Experiment::documentSparseSegmentation(alg, outputFolder + "/fusedSegmentation/", false);

	c.store_v2(outputFolder + "/fusedSegmentation/", "fusedoutput", alg->trajectories, alg->w, alg->motions.getAlignmentSets());
	
	tsdfTracking t(10, reader->getDescription(), 0.01);
	explainMe::Cloud::Ptr fusedCloud(new explainMe::Cloud());
		
	for (label = 0; label < alg->motions.getAlignmentSets().size(); label++){
	alg->getSegmentedOrganizedCloudsAndAlignments(label, extractedClouds, extractedAs);
	t.doFusion(extractedClouds, extractedAs, fusedCloud, outputFolder + "/cleanup_label" + std::to_string(label) + ".stl", alg->allClouds_asGrids, true, outputFolder + "/before_cleanup_label" + std::to_string(label) + ".stl");
	}
	
	

	auto mos = alg->motions.getAlignmentSets();
	for (int hyp = 0; hyp < mos.size(); hyp++){
		for (int al = 0; al < mos[hyp].size(); al++){
			if (!alg->motions.isHypInRange(hyp, al)){
				mos[hyp][al].fill(std::numeric_limits<float>::quiet_NaN());
			}
		}
	}

	c.store(outputFolder + "/motionsNANs.mo2", mos);

}




int main(int argc, char ** arg)
{
	std::cout << "TSDF cleanup of an object files...";
	std::string config_file = "";
	std::string dataFolder, outputFolder, segFile, motionFile; int dataType;
	explainMe_algorithm_withFeatures::Parameters params;
	int label;

	po::options_description generic("Options");
	generic.add_options()
		("help", "display Help")
		("config,c", po::value<std::string>()->default_value(""), "Path to a configuration .cfg file")
		;

	po::options_description config("Configuration");
	config.add_options()
		("data.folder,D", po::value<std::string>(&dataFolder)->required(), "specify the folder the 3d data lies")
		("output,O", po::value<std::string>(&outputFolder)->required(), "specify the folder the 3d data lies")
		("label,L", po::value<int>(&label)->required(), "specify the label to register")
		("data.type", po::value<int>(&dataType)->required(), "specify if its kinect or pcl data")
		("data.d", po::value<int>(&params.delta_frame)->required(), "take each dth frame")
		("data.firstFrame", po::value<int>(&params.first_frame)->required(), "... for a total of fw frames after cf")
		("data.lastFrame", po::value<int>(&params.last_frame)->required(), "... for a total of bw frames before cf")
		("data.maxDist", po::value<float>(&params.maxDistance)->default_value(100), "... ignore data points further from the camera")
		("init.seg", po::value<std::string>(&segFile)->required(), "the segmentation file")
		("init.motion", po::value<std::string>(&motionFile)->required(), "the motion file")
		;

	params.cleanUpClouds = false;

	po::options_description cmdline_options;
	cmdline_options.add(generic).add(config);

	po::options_description config_file_options;
	config_file_options.add(config);

	po::variables_map vm;
	po::store(po::parse_command_line(argc, arg, cmdline_options), vm);
	if (vm.count("help")){
		std::cout << cmdline_options << "\n";
		return 0;
	}


	try{
		config_file = vm["config"].as<std::string>();
		std::cout << "config file: " << config_file << "\n";
		if (config_file.size() > 0){
			std::ifstream ifs(config_file.c_str());
			if (!ifs)
			{
				std::cout << "can not open config file: " << config_file << "\n";
				return 0;
			}
			else
			{
				std::cout << "Parsing config file...";
				po::store(po::parse_config_file(ifs, config_file_options), vm);
				po::notify(vm);
			}
		}

		po::notify(vm);
	}
	catch (std::exception & e){
		std::cout << e.what() << "\n";
		std::cout << "Usage:\n" << cmdline_options << "\n";
		return 0;
	}

	runIt(dataType, dataFolder, outputFolder, params, segFile, motionFile, label);

	return 0;//*/
}