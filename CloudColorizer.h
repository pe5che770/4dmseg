#pragma once
#include "explainMeDefinitions.h"
#undef max
#undef min
#include <cmath>
#include <map>

class CloudColorizer
{
public:
	static int col[64][3];
	CloudColorizer(void);
	~CloudColorizer(void);

	static void colorWheel(int i, float & r, float & g, float & b){
		//int col[][3] = { { 255, 0, 0 }, { 0, 255, 0 }, { 0, 0, 255 }, { 20, 20, 20 }, { 255, 255, 0 }, { 255, 0, 255 }, { 0, 255, 255 }, { 200, 200, 200 }, { 20, 120, 220 }, { 20, 220, 120 }, { 120, 220, 20 }, { 120, 20, 220 }, { 220, 20, 120 }, { 220, 120, 20 } };
		r = col[i % 64][0] / 255.f;
		g = col[(i) % 64][1] / 255.f;
		b = col[(i) % 64][2] / 255.f;
	}

	static void colorizeRegions(explainMe::Cloud::Ptr cloud, Eigen::ArrayXXi & regions, bool map0ToBlack){
		//int col[] = {0, 100, 200, 50, 100,0,250,0,160,100};
		auto & points = cloud->points;
		float r, g, b;
		for(int i = 0; i < points.size(); i++){
			//points[i].r = col[(regions(i)+10)%10];
			//points[i].g = col[(regions(i)+11)%10];
			//points[i].b = col[(regions(i)+12)%10];
			colorWheel(regions(i), r, g, b);
			points[i].r = r * 255;
			points[i].g = g * 255;
			points[i].b = b * 255;

			if(map0ToBlack&&(regions(i)==0)){
				points[i].r =points[i].g =points[i].b = 0;
			}
		}
	}

	static void colorizeRegions2(explainMe::Cloud::Ptr cloud, Eigen::ArrayXXi & regions, bool map0ToBlack){
		int col[] = {0, 100, 200, 50, 180,30,250,220,160,120, 75};
		auto & points = cloud->points;

		for(int i = 0; i < points.size(); i++){
			points[i].r = col[((regions(i)+11)*7)%11];
			points[i].g = col[((regions(i)+11)*5)%11];
			points[i].b = col[(regions(i)+02)%11];

			if(map0ToBlack&&(regions(i)==0)){
				points[i].r =points[i].g =points[i].b = 0;
			}
		}
	}

	static void colorizeRegionsByDirection(explainMe::Cloud::Ptr cloud, Eigen::ArrayXXi & regions, std::map<int,Eigen::Vector3f> & vecs,
		bool map0ToBlack)
	{
		int col[] = {0, 100, 200, 50, 100,0,250,0,160,100};
		auto & points = cloud->points;
		Eigen::Vector3f vec;
		for(int i = 0; i < points.size(); i++){
			vec = vecs[regions(i)];
			vec.normalize();
			points[i].r = (vec.x()/2+0.5)*255;
			points[i].g = (vec.y()/2+0.5)*255;
			points[i].b = (vec.z()/2+0.5)*255;

			if(map0ToBlack&&(regions(i)==0)){
				points[i].r =points[i].g =points[i].b = 0;
			}
		}
	}

	static void colorizeRegionsByVector(explainMe::Cloud::Ptr cloud, Eigen::ArrayXXi & regions, std::map<int,Eigen::Vector3f> & vecs,
		float boost,
		bool map0ToBlack)
	{
		int col[] = {0, 100, 200, 50, 100,0,250,0,160,100};
		auto & points = cloud->points;
		Eigen::Vector3f vec;
		float l, tgt;
		for(int i = 0; i < points.size(); i++){
			vec = vecs[regions(i)];
			l = vec.norm() /points[i].z;
			tgt = log(1+l*boost);
			tgt = (tgt > 0.1f? tgt : 0);
			vec = vec * tgt/l;
			points[i].r = (vec.x()/2+0.5)*255;
			points[i].g = (vec.y()/2+0.5)*255;
			points[i].b = (vec.z()/2+0.5)*255;

			if(map0ToBlack&&(regions(i)==0)){
				points[i].r =points[i].g =points[i].b = 0;
			}
		}
	}

	
	static void colorizeBySignedFunction(explainMe::Cloud::Ptr cloud,
		std::vector<float> & f,
		float mapToOne)
	{

		auto & points = cloud->points;
		for(int i = 0; i < points.size(); i++){
			points[i].r = 255* std::min(std::max(- f[i]/mapToOne,0.f),1.f);
			points[i].g = 255* std::min(std::max(std::abs(f[i]/mapToOne) -1, 0.f), 1.f);
			points[i].b = 255* std::min(std::max(f[i]/mapToOne,0.f),1.f);//*/
		}
	}

	static void colorBySignedFunction(float f,
		float mapToOne, float & r, float &g, float & b)
	{

		r = 255 * std::min(std::max(-f / mapToOne, 0.f), 1.f);
		g = 255 * std::min(std::max(std::abs(f / mapToOne) - 1, 0.f), 1.f);
		b = 255 * std::min(std::max(f / mapToOne, 0.f), 1.f);//*/
		
	}

	static void colorizeBySignedFunction(explainMe::Cloud::Ptr cloud,
		std::vector<float> & f,
		std::vector<float> & mapToOne)
	{

		auto & points = cloud->points;
		for(int i = 0; i < points.size(); i++){
			points[i].r = 255* std::min(std::max(- f[i]/mapToOne[i],0.f),1.f);
			points[i].g = 255* std::min(std::max(std::abs(f[i]/mapToOne[i]) -1, 0.f), 1.f);
			points[i].b = 255* std::min(std::max(f[i]/mapToOne[i],0.f),1.f);//*/
		}
	}

	static void colorizeByNormals(explainMe::Cloud::Ptr cloud)
	{

		auto & points = cloud->points;
		for(int i = 0; i < points.size(); i++){
			points[i].r = 255* std::abs(points[i].normal_x);
			points[i].g = 255* std::abs(points[i].normal_y);
			points[i].b = 255* std::abs(points[i].normal_z);//*/
		}
	}

	static void colorByMagnitude(float val, float scale,
		float & r, float & g, float & b){
		r = (val / scale < 1 ? 1 - val / scale : 0);
		g = (val / scale > 1 ? std::min(val/scale - 1, 1.f) * 2 / 3.f : 0);
		b = (val / scale < 2 ? 1 - std::abs(val/scale- 1) : 0);
	}

	static void colorizeByMagnitude(explainMe::Cloud::Ptr cloud, std::vector<float> & vals, float scale){
		auto & points = cloud->points;
		float scalar;
		for(int i = 0; i < points.size(); i++){
			scalar = vals[i] * scale;
			points[i].r = 255*(scalar < 1 ? 1- scalar : 0);
			points[i].g = 255*(scalar > 1 ? std::min(scalar -1,1.f)*2/3.f : 0);
			points[i].b = 255* (scalar < 2 ? 1- std::abs(scalar-1) : 0);//*/
		}
		//float std = pow(c_v[0], 0.5) * 1000/3.f;
	}

	static void colorizeByMagnitude(explainMe::Cloud::Ptr cloud, Eigen::ArrayXXf & vals, float scale){
		auto & points = cloud->points;
		float scalar;
		for(int i = 0; i < cloud->width; i++){
			for(int j = 0; j < cloud->height; j++){
				scalar = vals(i,j) * scale;
				cloud->at(i,j).r = 255*(scalar < 1 ? 1- scalar : 0);
				cloud->at(i,j).g = 255*(scalar > 1 ? std::min(scalar -1,1.f)*2/3.f : 0);
				cloud->at(i,j).b = 255* (scalar < 2 ? 1- std::abs(scalar-1) : 0);//*/
			}
		}
		//float std = pow(c_v[0], 0.5) * 1000/3.f;
		

	}

	static void colorizeByMagnitude(explainMe::Cloud::Ptr cloud, 
		Eigen::ArrayXXf & vals, 
		float scale, float center_at){
		auto & points = cloud->points;
		float scalar;
		for(int i = 0; i < cloud->width; i++){
			for(int j = 0; j < cloud->height; j++){
				scalar = (vals(i,j) -center_at)* scale;
				cloud->at(i,j).r = 255*(scalar < 1 ? 1- scalar : 0);
				cloud->at(i,j).g = 255*(scalar > 1 ? std::min(scalar -1,1.f)*2/3.f : 0);
				cloud->at(i,j).b = 255* (scalar < 2 ? 1- std::abs(scalar-1) : 0);//*/
			}
		}
		//float std = pow(c_v[0], 0.5) * 1000/3.f;
		

	}

	
	static void colorizeByColors(explainMe::Cloud::Ptr cloud, std::vector<Eigen::Vector3f> & colors){
		for(int i = 0; i < cloud->size(); i++){
			cloud->at(i).r = 255*colors[i].x();
			cloud->at(i).g = 255*colors[i].y();
			cloud->at(i).b = 255*colors[i].z();
			
		}
	
	}


	static void color_viz_log_magnitude(explainMe::MatrixXf_rm &  weights, std::vector<Eigen::Vector3f> & colors_out, int w_i, bool normalize = true)
	{
		//int col[] = {0, 150, 250, 80, 150,0,270,0,180,130};
		float vis;
		colors_out.clear();
		colors_out.resize(weights.rows(), Eigen::Vector3f::Zero());
		int n = weights.cols();
		
		float mx = weights.col(w_i).maxCoeff(), mn = std::numeric_limits<float>::infinity();
		for (int i = 0; i < weights.rows(); i++){
			if (weights(i, w_i) > 1e-4 && weights(i, w_i) < mn){
				mn = weights(i, w_i);
			}
		}
		//mn = 0;

		float nml = std::log(1 + (mx - mn)) + 1e-5;

		if (!normalize){
			nml = std::log(2);
			mn = 0;
		}
		for (int i = 0; i < weights.rows(); i++){
			if (weights(i, w_i) - mn <= 0){
				vis = 0;
			}
			else{
				vis = std::log(1 + (weights(i, w_i) - mn)) / (nml / 5);
			}

		//	if (vis * 0 != 0){
		//		std::cout << "*****colorvismagnitude: " << weights(i, w_i) << "/" << 2 * nml << "  log(" << weights.cols() << ")\n";
		//	}

			//color mapping: r ->g ->b->p->t->y
			//colors_out[i] << std::max<float>(1 - vis, 0.f) ,
			//std::max<float>(std::min<float>(vis, 2 - vis), 0.f),
			//std::max<float>(std::min<float>(vis - 1, 1.f), 0.f);
			colors_out[i] << std::max<float>(1 - vis, 0.f) + std::max<float>(std::min<float>(vis - 3, 1.f), 0.f),
				std::max<float>(std::min<float>(vis, 2 - vis), 0.f) + std::max<float>(std::min(std::min<float>(vis - 2, 5 - vis), 1.f), 0.f),
				std::max<float>(std::min(std::min<float>(vis - 1, 4 - vis), 1.f), 0.f) + std::max<float>(std::min(vis-4, 1.f), 0.f);
			
		}
	}

	static void color_viz_log_magnitude(std::vector<float> & vals, std::vector<Eigen::Vector3f> & colors_out, float scale)
	{
		//int col[] = {0, 150, 250, 80, 150,0,270,0,180,130};
		float vis;
		colors_out.clear();
		colors_out.resize(vals.size(), Eigen::Vector3f::Zero());
		
		
		//mn = 0;

		//float nml = std::log(1 + scale) + 1e-5;
		float nml = std::log(2);
	
		for (int i = 0; i < vals.size(); i++){
			//vis = std::log(1 + (vals[i])) / (nml / 5);
			vis = 5*std::log(1 + (vals[i]/scale))/nml;
			//vis = 5 * vals[i] / scale;
			
			colors_out[i] << std::max<float>(1 - vis, 0.f) + std::max<float>(std::min<float>(vis - 3, 1.f), 0.f),
				std::max<float>(std::min<float>(vis, 2 - vis), 0.f) + std::max<float>(std::min(std::min<float>(vis - 2, 5 - vis), 1.f), 0.f),
				std::max<float>(std::min(std::min<float>(vis - 1, 4 - vis), 1.f), 0.f) + std::max<float>(std::min(vis - 4, 1.f), 0.f);

		}
	}


	static void color_viz_weights(explainMe::MatrixXf_rm & weights, std::vector<Eigen::Vector3f> & colors_out, bool normalize = false)
	{

		//int col[] = {0, 150, 250, 80, 150,0,270,0,180,130};
		//int col[][3] = {{255,0,0},{0,255,0},{0,0,255},{20,20,20},{255,255,0},{255,0,255}, {0,255,255}, {200,200,200}, {20,120,220},{20,220,120},{120,220,20}, {120,20,220}, {220,20,120},{220, 120,20}};
		
		colors_out.clear();
		colors_out.resize( weights.rows(), Eigen::Vector3f::Zero());
		int n = weights.cols();

		std::vector<Eigen::Vector3f> color;
		color.resize(n);
		for (int i = 0; i < n; i++){
			colorWheel(i,color[i].x(), color[i].y(), color[i].z());
		}

		float max = 0;
		for(int i = 0; i < weights.rows(); i++){
			
			if(weights.row(i).cwiseAbs().sum() < 1e-8){
				//zero: stripes.
				colors_out[i](0) = (((i/2)%2) ==0 ? 0.05f : 0.8f) ;
				colors_out[i](1) = (((i / 2) % 2) == 0 ? 0.05f : 0.8f);
				colors_out[i](2) = (((i / 2) % 2) == 0 ? 0.05f : 0.8f);
			}
			else{
				for(int w_i = 0; w_i< n; w_i++){
					/*colors_out[i](0) += weights(i,w_i) * col[(w_i)%13][0];
					colors_out[i](1) += weights(i,w_i) * col[(w_i)%14][1];
					colors_out[i](2) += weights(i,w_i) * col[(w_i)%14][2];*/
					colors_out[i].noalias() += weights(i, w_i) * color[w_i];
					/*colors_out[i](1) += weights(i, w_i) * col[(w_i) % 14][1];
					colors_out[i](2) += weights(i, w_i) * col[(w_i) % 14][2];*/
				}
			}
			//colors_out[i] /= 255;

			//colors_out[i] /= 255;//*weights.row(i).sum();
			max = std::max(max,colors_out[i].maxCoeff());

			//colors_out[i].fill(0);
		}

		if(normalize){
			for(int i = 0; i < weights.rows(); i++){
				colors_out[i] /= max;
			}
		}
	} 


	static void color_viz_weightsmax(explainMe::MatrixXf_rm & weights, std::vector<Eigen::Vector3f> & colors_out)
	{

		//int col[] = {0, 150, 250, 80, 150,0,270,0,180,130};
		//int col[][3] = {{255,0,0},{0,255,0},{0,0,255},{20,20,20},{255,255,0},{255,0,255}, {0,255,255}, {200,200,200}, {20,120,220},{20,220,120},{120,220,20}, {120,20,220}, {220,20,120},{220, 120,20}};

		colors_out.clear();
		colors_out.resize(weights.rows(), Eigen::Vector3f::Zero());
		int n = weights.cols();

		std::vector<Eigen::Vector3f> color;
		color.resize(n);
		for (int i = 0; i < n; i++){
			colorWheel(i, color[i].x(), color[i].y(), color[i].z());
		}

		int maxCoeff;
		for (int i = 0; i < weights.rows(); i++){
			if (weights.row(i).cwiseAbs().sum() < 1e-8){
				//zero: stripes.
				colors_out[i](0) = (((i / 2) % 2) == 0 ? 0.05f : 0.8f);
				colors_out[i](1) = (((i / 2) % 2) == 0 ? 0.05f : 0.8f);
				colors_out[i](2) = (((i / 2) % 2) == 0 ? 0.05f : 0.8f);
			}
			else{
				weights.row(i).maxCoeff(&maxCoeff);
				colors_out[i] = color[maxCoeff];
			}
		}

	}

	static void  color_viz_weights2tow_to_max(explainMe::MatrixXf_rm & weights, std::vector<Eigen::Vector3f> & colors_out){

		//int col[] = {0, 150, 250, 80, 150,0,270,0,180,130};
		//int col[][3] = {{255,0,0},{0,255,0},{0,0,255},{20,20,20},{255,255,0},{255,0,255}, {0,255,255}, {200,200,200}, {20,120,220},{20,220,120},{120,220,20}, {120,20,220}, {220,20,120},{220, 120,20}};

		float vis;
		colors_out.clear();
		colors_out.resize(weights.rows(), Eigen::Vector3f::Zero());
		int n = weights.cols();
		float nml = std::log(2);
		for (int i = 0; i < weights.rows(); i++){
			vis = std::log(1 + weights.row(i).minCoeff() / weights.row(i).maxCoeff()) / (nml / 5);
			//vis = std::log(1+ (1 - weights.row(i).maxCoeff())) / (nml / 5);

			//	if (vis * 0 != 0){
			//		std::cout << "*****colorvismagnitude: " << weights(i, w_i) << "/" << 2 * nml << "  log(" << weights.cols() << ")\n";
			//	}

			//color mapping: r ->g ->b->p->t->y
			//colors_out[i] << std::max<float>(1 - vis, 0.f) ,
			//std::max<float>(std::min<float>(vis, 2 - vis), 0.f),
			//std::max<float>(std::min<float>(vis - 1, 1.f), 0.f);
			colors_out[i] << std::max<float>(1 - vis, 0.f) + std::max<float>(std::min<float>(vis - 3, 1.f), 0.f),
				std::max<float>(std::min<float>(vis, 2 - vis), 0.f) + std::max<float>(std::min(std::min<float>(vis - 2, 5 - vis), 1.f), 0.f),
				std::max<float>(std::min(std::min<float>(vis - 1, 4 - vis), 1.f), 0.f) + std::max<float>(std::min(vis - 4, 1.f), 0.f);

		}
	}
};

