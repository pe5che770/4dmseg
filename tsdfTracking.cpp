#include "tsdfTracking.h"
#include "opencv2/opencv.hpp" 
#include "Sensors.h"
#include "CloudTools.h"

void initCalibFromDesc(ITMLib::Objects::ITMRGBDCalib & calib, Vector2i & dSize, Vector2i & rgbSize, RangeSensorDescription * d){
	//depth calib
	float fx, fy, px, py;
	d->extract(px, py, fx, fy);
	//fx = d->f_x(); fy = d->f_y(); px = d->p_x(); py = d->p_y();
	calib.intrinsics_d.SetFrom(fx, fy, px, py, d->width(), d->height());
	calib.disparityCalib.type = ITMDisparityCalib::TRAFO_AFFINE;
	calib.disparityCalib.params.x = 0.001; calib.disparityCalib.params.y = 0.f;

	//for now: assume depth and color perfectly alignmed,
	calib.intrinsics_rgb = calib.intrinsics_d;
	Matrix4f id; id.setIdentity();
	calib.trafo_rgb_to_depth.SetFrom(id);

	dSize.width = d->width(); dSize.height = d->height();
	rgbSize.width = d->width(); rgbSize.height = d->height();

	//std::cout << "Calibration is: " << fx << ", " <<fy << ", " << px << ", "
	//	<< py << ", " << d->width() << ", " << d->height();
}

class ITMLibObjects{
public:
	//calibration
	ITMLib::Objects::ITMRGBDCalib calib;
	Vector2i depthImgSize, rgbImgSize;

	//gemeral settings
	ITMLib::Objects::ITMLibSettings settings;

	//the voxel grid.
	ITMScene<ITMVoxel, ITMVoxelIndex> * scene;
	//casting rgb raw images into format
	ITMLib::Engine::ITMViewBuilder * viewBuilder;
	//raytracing
	ITMLib::Engine::ITMVisualisationEngine<ITMVoxel, ITMVoxelIndex> * visualisationEngine;
	//marching cubes
	ITMLib::Engine::ITMMeshingEngine<ITMVoxel, ITMVoxelIndex> * meshingEngine;
	//handles fusion. can also be used to reset the scene....
	ITMDenseMapper<ITMVoxel, ITMVoxelIndex> *  denseMapper;
	//manages the current alignment.
	ITMTrackingState * trackingState;
public:
	ITMLibObjects(float maxDepth, RangeSensorDescription * d, float voxelSize){
		//calibration
		initCalibFromDesc(calib, depthImgSize, rgbImgSize, d);
		//settings
		settings.sceneParams.viewFrustum_max = maxDepth;
		//settings.deviceType = ITMLibSettings::DEVICE_CPU;
		settings.sceneParams.voxelSize = voxelSize;
		bool useCuda = settings.deviceType == ITMLibSettings::DEVICE_CUDA;

		scene = new ITMScene<ITMVoxel, ITMVoxelIndex>(&(settings.sceneParams), settings.useSwapping,
			useCuda ? MEMORYDEVICE_CUDA : MEMORYDEVICE_CPU);
		if (useCuda){
			viewBuilder = new ITMViewBuilder_CUDA(&calib);
			visualisationEngine = new ITMVisualisationEngine_CUDA<ITMVoxel, ITMVoxelIndex>(scene);
			meshingEngine = new ITMMeshingEngine_CUDA<ITMVoxel, ITMVoxelIndex>();
		}
		else{
			viewBuilder = new ITMViewBuilder_CPU(&calib);
			visualisationEngine = new ITMVisualisationEngine_CPU<ITMVoxel, ITMVoxelIndex>(scene);
			meshingEngine = new ITMMeshingEngine_CPU<ITMVoxel, ITMVoxelIndex>();
		}

		denseMapper = new ITMDenseMapper<ITMVoxel, ITMVoxelIndex>(&settings);
		denseMapper->ResetScene(scene);
		trackingState = new ITMTrackingState(depthImgSize, useCuda ? MEMORYDEVICE_CUDA : MEMORYDEVICE_CPU);//trackingController->BuildTrackingState(depthImgSize);
	}

	~ITMLibObjects(){
		delete  scene, viewBuilder, visualisationEngine, meshingEngine, denseMapper, trackingState;
		//delete trackingController, lowLevelEngine, tracker;
	}

	bool isUsingCuda(){
		return settings.deviceType == ITMLibSettings::DEVICE_CUDA;
	}
};


tsdfTracking::tsdfTracking(float maxDepth, RangeSensorDescription * d, float voxelSize){
	itmlib = new ITMLibObjects(maxDepth, d, voxelSize);
}

tsdfTracking::~tsdfTracking(){
	delete itmlib;
}

void tsdfTracking::doFusion(std::vector<explainMe::Cloud::Ptr> & clouds, 
	std::vector<Eigen::Matrix4f> & alignments, 
	explainMe::Cloud::Ptr fusedCld, 
	std::string outFileSTL, std::vector<explainMe::Cloud::Ptr>  fullDepth, bool cleanupMode,
	std::string beforeCleanupStl)
{
	//reset everything (how?)
	itmlib->denseMapper->ResetScene(itmlib->scene);

	//Image containers

	ORUtils::Image<ORUtils::Vector4<uchar>> rgbImage(itmlib->rgbImgSize, true, itmlib->isUsingCuda());
	ORUtils::Image<short> depthImage(itmlib->depthImgSize, true, itmlib->isUsingCuda());
	ORUtils::Image<short> fullDepthImage(itmlib->depthImgSize, true, itmlib->isUsingCuda());
	depthImage.ChangeDims(itmlib->depthImgSize);
	fullDepthImage.ChangeDims(itmlib->depthImgSize);
	rgbImage.ChangeDims(itmlib->depthImgSize);

	//containers for results:
	ITMLib::Objects::ITMMesh * mesh = new ITMMesh(itmlib->isUsingCuda() ? MEMORYDEVICE_CUDA : MEMORYDEVICE_CPU);
	//keeps life visualization and some data needed for tracing the volume
	auto renderState_live = itmlib->visualisationEngine->CreateRenderState(itmlib->depthImgSize);
	ITMView * view = NULL, *fullview = NULL;


	//1. Fusion
	std::cout << "Engine processing frames...";
	auto & depthImgSize = itmlib->depthImgSize;
	short * dataPtr = depthImage.GetData(MEMORYDEVICE_CPU);
	short * dataPtr_fullDepth = fullDepthImage.GetData(MEMORYDEVICE_CPU);
	for (int frm = 0; frm < clouds.size(); frm++){
		std::cout << "fame " << frm << "...\n";
		for (int i = 0, idx = 0; i < depthImgSize.height; i++) for (int j = 0; j < depthImgSize.width; j++)
		{
			float dz = clouds[frm]->at(j, i).z;
			dz = (dz * 0 == 0 ? dz : 0);
			dataPtr[idx] = std::floor(dz * 1000 + 0.5);
			rgbImage.GetData(MEMORYDEVICE_CPU)[idx].x = rgbImage.GetData(MEMORYDEVICE_CPU)[idx].y = rgbImage.GetData(MEMORYDEVICE_CPU)[idx].z = 0;
			idx++;
		}
		itmlib->viewBuilder->UpdateView(&view, &rgbImage, &depthImage, itmlib->settings.useBilateralFilter, itmlib->settings.modelSensorNoise);
		// tracking
		//note that the infinitam matrices are column first.
		//everything needs to be transformed to first frame...
		//trackingController->Track(trackingState, view);
		itmlib->trackingState->requiresFullRendering = true;
		Matrix4f mat; mat.setIdentity();
		Eigen::Matrix4f m = alignments[frm] * alignments[0].inverse(); m.transposeInPlace();
		mat.m00 = m(0, 0); mat.m01 = m(0, 1); mat.m02 = m(0, 2); mat.m03 = m(0, 3);
		mat.m10 = m(1, 0); mat.m11 = m(1, 1); mat.m12 = m(1, 2); mat.m13 = m(1, 3);
		mat.m20 = m(2, 0); mat.m21 = m(2, 1); mat.m22 = m(2, 2); mat.m23 = m(2, 3);
		mat.m30 = m(3, 0); mat.m31 = m(3, 1); mat.m32 = m(3, 2); mat.m33 = m(3, 3);
		itmlib->trackingState->pose_d->SetM(mat);

		// fusion
		itmlib->denseMapper->ProcessFrame(view, itmlib->trackingState, itmlib->scene, renderState_live);//, false);
		std::cout << "No visible voxels: " << renderState_live->noVisibleEntries << "\n";

		// raycast to renderState_live for tracking and free visualisation
		//trackingController->Prepare(trackingState, view, renderState_live);
		itmlib->visualisationEngine->CreateExpectedDepths(itmlib->trackingState->pose_d, &(view->calib->intrinsics_d), renderState_live);
		itmlib->visualisationEngine->CreateICPMaps(view, itmlib->trackingState, renderState_live);
		itmlib->trackingState->pose_pointCloud->SetFrom(itmlib->trackingState->pose_d);

	}
		

	//2. cleanup
	if (cleanupMode && fullDepth.size() > 0){
		if (beforeCleanupStl.length() > 0){
			itmlib->meshingEngine->MeshScene(mesh, itmlib->scene);
			mesh->WriteSTL(beforeCleanupStl.c_str());
		}

		//normal computation is not working.
		//itmlib->denseMapper->ComputeAndStoreAllNormals(itmlib->scene);
		for (int frm = 0; frm < fullDepth.size(); frm++){
			std::cout << "fame " << frm << "...\n";
			for (int i = 0, idx = 0; i < depthImgSize.height; i++) for (int j = 0; j < depthImgSize.width; j++)
			{
				float dz = clouds[frm]->at(j, i).z;
				dz = (dz * 0 == 0 ? dz : 0);
				dataPtr[idx] = std::floor(dz * 1000 + 0.5);

				dz = fullDepth[frm]->at(j, i).z;
				dz = (dz * 0 == 0 ? dz : 0);
				dataPtr_fullDepth[idx] = std::floor(dz * 1000 + 0.5);

				rgbImage.GetData(MEMORYDEVICE_CPU)[idx].x = rgbImage.GetData(MEMORYDEVICE_CPU)[idx].y = rgbImage.GetData(MEMORYDEVICE_CPU)[idx].z = 0;
				idx++;
			}
			itmlib->viewBuilder->UpdateView(&view, &rgbImage, &depthImage, itmlib->settings.useBilateralFilter, itmlib->settings.modelSensorNoise);
			itmlib->viewBuilder->UpdateView(&fullview, &rgbImage, &fullDepthImage, itmlib->settings.useBilateralFilter, itmlib->settings.modelSensorNoise);
			// tracking
			//note that the infinitam matrices are column first.
			//everything needs to be transformed to first frame...
			//trackingController->Track(trackingState, view);
			itmlib->trackingState->requiresFullRendering = true;
			Matrix4f mat; mat.setIdentity();
			Eigen::Matrix4f m = alignments[frm] * alignments[0].inverse(); m.transposeInPlace();
			mat.m00 = m(0, 0); mat.m01 = m(0, 1); mat.m02 = m(0, 2); mat.m03 = m(0, 3);
			mat.m10 = m(1, 0); mat.m11 = m(1, 1); mat.m12 = m(1, 2); mat.m13 = m(1, 3);
			mat.m20 = m(2, 0); mat.m21 = m(2, 1); mat.m22 = m(2, 2); mat.m23 = m(2, 3);
			mat.m30 = m(3, 0); mat.m31 = m(3, 1); mat.m32 = m(3, 2); mat.m33 = m(3, 3);
			itmlib->trackingState->pose_d->SetM(mat);

			// clean up
			itmlib->denseMapper->ProcessFrame(fullview, itmlib->trackingState, itmlib->scene, renderState_live);// , true);
			//itmlib->denseMapper->ProcessFrame(view, itmlib->trackingState, itmlib->scene, renderState_live, false);
			std::cout << "No visible voxels: " << renderState_live->noVisibleEntries << "\n";

			// raycast to renderState_live for tracking and free visualisation
			//trackingController->Prepare(trackingState, view, renderState_live);
			itmlib->visualisationEngine->CreateExpectedDepths(itmlib->trackingState->pose_d, &(view->calib->intrinsics_d), renderState_live);
			itmlib->visualisationEngine->CreateICPMaps(view, itmlib->trackingState, renderState_live);
			itmlib->trackingState->pose_pointCloud->SetFrom(itmlib->trackingState->pose_d);

		}
	}//*/

	//meshing.
	itmlib->meshingEngine->MeshScene(mesh, itmlib->scene);

	//MAP MESH To ref cloud:
	{
		ORUtils::MemoryBlock<ITMLib::Objects::ITMMesh::Triangle> *cpu_triangles;
		if (mesh->memoryType == MEMORYDEVICE_CUDA){
			cpu_triangles = new ORUtils::MemoryBlock<ITMLib::Objects::ITMMesh::Triangle>(mesh->noMaxTriangles, MEMORYDEVICE_CPU);
			cpu_triangles->SetFrom(mesh->triangles, ORUtils::MemoryBlock<ITMLib::Objects::ITMMesh::Triangle>::CUDA_TO_CPU);
		}
		else{
			cpu_triangles = mesh->triangles;
		}

		ITMLib::Objects::ITMMesh::Triangle *triangleArray = cpu_triangles->GetData(MEMORYDEVICE_CPU);
		Eigen::Matrix4f to_ref = alignments[0].inverse();
		Eigen::Vector3f p, q, r;
		for (uint i = 0; i < mesh->noTotalTriangles; i++)
		{
			p << triangleArray[i].p0.x, triangleArray[i].p0.y, triangleArray[i].p0.z;
			q << triangleArray[i].p1.x, triangleArray[i].p1.y, triangleArray[i].p1.z;
			r << triangleArray[i].p2.x, triangleArray[i].p2.y, triangleArray[i].p2.z;

			p = to_ref.topLeftCorner<3, 3>() * p + to_ref.topRightCorner<3, 1>();
			q = to_ref.topLeftCorner<3, 3>() * q + to_ref.topRightCorner<3, 1>();
			r = to_ref.topLeftCorner<3, 3>() * r + to_ref.topRightCorner<3, 1>();

			triangleArray[i].p0.x = p.x(); triangleArray[i].p0.y = p.y(); triangleArray[i].p0.z = p.z();
			triangleArray[i].p1.x = q.x(); triangleArray[i].p1.y = q.y(); triangleArray[i].p1.z = q.z();
			triangleArray[i].p2.x = r.x(); triangleArray[i].p2.y = r.y(); triangleArray[i].p2.z = r.z();
		}


		if (mesh->memoryType == MEMORYDEVICE_CUDA){
			mesh->triangles->SetFrom(cpu_triangles, ORUtils::MemoryBlock<ITMLib::Objects::ITMMesh::Triangle>::CPU_TO_CUDA);
			delete cpu_triangles;
		}
	}

	//cast to my cloud format...
	fusedCld->reserve(mesh->noTotalTriangles * 3); //duplicate vertices!
	{
		ORUtils::MemoryBlock<ITMLib::Objects::ITMMesh::Triangle> *cpu_triangles;
		if (mesh->memoryType == MEMORYDEVICE_CUDA){
			cpu_triangles = new ORUtils::MemoryBlock<ITMLib::Objects::ITMMesh::Triangle>(mesh->noMaxTriangles, MEMORYDEVICE_CPU);
			cpu_triangles->SetFrom(mesh->triangles, ORUtils::MemoryBlock<ITMLib::Objects::ITMMesh::Triangle>::CUDA_TO_CPU);
		}
		else{
			cpu_triangles = mesh->triangles;
		}

		ITMLib::Objects::ITMMesh::Triangle *triangleArray = cpu_triangles->GetData(MEMORYDEVICE_CPU);
		Eigen::Vector3f p, q, r;
		//Eigen::Matrix4f to_ref = alignments[0].inverse();
		for (uint i = 0; i < mesh->noTotalTriangles; i++)
		{
			p << triangleArray[i].p0.x, triangleArray[i].p0.y, triangleArray[i].p0.z;
			q << triangleArray[i].p1.x, triangleArray[i].p1.y, triangleArray[i].p1.z;
			r << triangleArray[i].p2.x, triangleArray[i].p2.y, triangleArray[i].p2.z;

			//p = to_ref.topLeftCorner<3, 3>() * p + to_ref.topRightCorner<3, 1>();
			//q = to_ref.topLeftCorner<3, 3>() * q + to_ref.topRightCorner<3, 1>();
			//r = to_ref.topLeftCorner<3, 3>() * r + to_ref.topRightCorner<3, 1>();

			//Should care more about normals.... that is identify duplicate verts and compute "better" normals...
			Eigen::Vector3f facenormal;
			facenormal = (q-p).cross(r-p);
			facenormal.normalize();

			if (facenormal.norm() * 0 == 0){
				fusedCld->push_back(explainMe::Point());
				fusedCld->back().getVector3fMap() = p;
				fusedCld->back().getNormalVector3fMap() = facenormal;
				fusedCld->push_back(explainMe::Point());
				fusedCld->back().getVector3fMap() = q;
				fusedCld->back().getNormalVector3fMap() = facenormal;
				fusedCld->push_back(explainMe::Point());
				fusedCld->back().getVector3fMap() = r;
				fusedCld->back().getNormalVector3fMap() = facenormal;
			}

		}
		if (mesh->memoryType == MEMORYDEVICE_CUDA){
			delete cpu_triangles;
		}
	}
	
	if (outFileSTL.length() > 0){
		mesh->WriteSTL(outFileSTL.c_str());
		//pcl::io::savePLYFile<explainMe::Point>(file + ".ply", *extractedClouds[j]);
	}

	delete mesh, renderState_live;
	if (view != NULL) delete view;
	if (fullview != NULL) delete fullview;

}





void tsdfTracking::doFusion(std::vector<explainMe::Cloud::Ptr> & clouds, std::vector<Eigen::Matrix4f> & alignments, Eigen::Matrix4f & global2Reference,
	explainMe::Cloud::Ptr fusedCld, std::vector<int> & triangles, std::string outFileSTL)
{
	//reset everything (how?)
	itmlib->denseMapper->ResetScene(itmlib->scene);

	//Image containers
	ORUtils::Image<ORUtils::Vector4<uchar>> rgbImage(itmlib->rgbImgSize, true, itmlib->isUsingCuda());
	ORUtils::Image<short> depthImage(itmlib->depthImgSize, true, itmlib->isUsingCuda());
	depthImage.ChangeDims(itmlib->depthImgSize);
	rgbImage.ChangeDims(itmlib->depthImgSize);

	//containers for results:
	ITMLib::Objects::ITMMesh * mesh = new ITMMesh(itmlib->isUsingCuda() ? MEMORYDEVICE_CUDA : MEMORYDEVICE_CPU);
	//keeps life visualization and some data needed for tracing the volume
	auto renderState_live = itmlib->visualisationEngine->CreateRenderState(itmlib->depthImgSize);
	ITMView * view = NULL;


	//1. Get the raw data.
	std::cout << "Engine processing frames...";
	auto & depthImgSize = itmlib->depthImgSize;
	short * dataPtr = depthImage.GetData(MEMORYDEVICE_CPU);
	for (int frm = 0; frm < clouds.size(); frm++){
//		std::cout << "fame " << frm << "...\n";
		for (int i = 0, idx = 0; i < depthImgSize.height; i++) for (int j = 0; j < depthImgSize.width; j++)
		{
			float dz = clouds[frm]->at(j, i).z;
			dz = (dz * 0 == 0 ? dz : 0);
			dataPtr[idx] = std::floor(dz * 1000 + 0.5);
			rgbImage.GetData(MEMORYDEVICE_CPU)[idx].x = rgbImage.GetData(MEMORYDEVICE_CPU)[idx].y = rgbImage.GetData(MEMORYDEVICE_CPU)[idx].z = 0;
						
			idx++;
		}
		itmlib->viewBuilder->UpdateView(&view, &rgbImage, &depthImage, itmlib->settings.useBilateralFilter, itmlib->settings.modelSensorNoise);
		// tracking
		//note that the infinitam matrices are column first.
		//everything needs to be transformed to first frame...
		//trackingController->Track(trackingState, view);
		itmlib->trackingState->requiresFullRendering = true;
		Matrix4f mat; mat.setIdentity();
		Eigen::Matrix4f m = alignments[frm] * alignments[0].inverse(); m.transposeInPlace();
		mat.m00 = m(0, 0); mat.m01 = m(0, 1); mat.m02 = m(0, 2); mat.m03 = m(0, 3);
		mat.m10 = m(1, 0); mat.m11 = m(1, 1); mat.m12 = m(1, 2); mat.m13 = m(1, 3);
		mat.m20 = m(2, 0); mat.m21 = m(2, 1); mat.m22 = m(2, 2); mat.m23 = m(2, 3);
		mat.m30 = m(3, 0); mat.m31 = m(3, 1); mat.m32 = m(3, 2); mat.m33 = m(3, 3);
		itmlib->trackingState->pose_d->SetM(mat);

		// fusion
		itmlib->denseMapper->ProcessFrame(view, itmlib->trackingState, itmlib->scene, renderState_live);
//		std::cout << "No visible voxels: " << renderState_live->noVisibleEntries << "\n";

		// raycast to renderState_live for tracking and free visualisation
		//trackingController->Prepare(trackingState, view, renderState_live);
		itmlib->visualisationEngine->CreateExpectedDepths(itmlib->trackingState->pose_d, &(view->calib->intrinsics_d), renderState_live);
		itmlib->visualisationEngine->CreateICPMaps(view, itmlib->trackingState, renderState_live);
		itmlib->trackingState->pose_pointCloud->SetFrom(itmlib->trackingState->pose_d);

	}

	itmlib->meshingEngine->MeshScene(mesh, itmlib->scene);
	/*	std::cout << "writing stl: " << outFileSTL;
	mesh->WriteSTL(outFileSTL.c_str());*/
	//cast to my cloud format...
	fusedCld->clear();
	triangles.clear();
	fusedCld->reserve(mesh->noTotalTriangles * 3); //duplicate vertices!
	triangles.reserve(mesh->noTotalTriangles * 3); //duplicate vertices!
	{
		ORUtils::MemoryBlock<ITMLib::Objects::ITMMesh::Triangle> *cpu_triangles;
		if (mesh->memoryType == MEMORYDEVICE_CUDA){
			cpu_triangles = new ORUtils::MemoryBlock<ITMLib::Objects::ITMMesh::Triangle>(mesh->noMaxTriangles, MEMORYDEVICE_CPU);
			cpu_triangles->SetFrom(mesh->triangles, ORUtils::MemoryBlock<ITMLib::Objects::ITMMesh::Triangle>::CUDA_TO_CPU);
		}
		else{
			cpu_triangles = mesh->triangles;
		}

		ITMLib::Objects::ITMMesh::Triangle *triangleArray = cpu_triangles->GetData(MEMORYDEVICE_CPU);
		Eigen::Vector3f p, q, r;
		for (uint i = 0; i < mesh->noTotalTriangles; i++)
		{
			p << triangleArray[i].p0.x, triangleArray[i].p0.y, triangleArray[i].p0.z;
			q << triangleArray[i].p1.x, triangleArray[i].p1.y, triangleArray[i].p1.z;
			r << triangleArray[i].p2.x, triangleArray[i].p2.y, triangleArray[i].p2.z;

			//Should care more about normals.... that is identify duplicate verts and compute "better" normals...
			//faces are oriented the wrong way, it seems....
			Eigen::Vector3f facenormal;
			facenormal = -(q - p).cross(r - p);
			facenormal.normalize();

			if (facenormal.norm() * 0 == 0){
				triangles.push_back(fusedCld->size());
				fusedCld->push_back(explainMe::Point());
				fusedCld->back().getVector3fMap() = p;
				fusedCld->back().getNormalVector3fMap() = facenormal;

				triangles.push_back(fusedCld->size());
				fusedCld->push_back(explainMe::Point());
				fusedCld->back().getVector3fMap() = q;
				fusedCld->back().getNormalVector3fMap() = facenormal;

				triangles.push_back(fusedCld->size());
				fusedCld->push_back(explainMe::Point());
				fusedCld->back().getVector3fMap() = r;
				fusedCld->back().getNormalVector3fMap() = facenormal;

			}

		}
		if (mesh->memoryType == MEMORYDEVICE_CUDA){
			delete cpu_triangles;
		}
	}

	Eigen::Matrix4f tmp = global2Reference*alignments[0].inverse();
	CloudTools::apply<explainMe::Cloud::Ptr>(tmp, fusedCld);

	if (outFileSTL.length() > 0){
		mesh->WriteSTL(outFileSTL.c_str());
		//pcl::io::savePLYFile<explainMe::Point>(file + ".ply", *extractedClouds[j]);
	}

	std::cout << "done.\n";
	delete mesh, renderState_live;
	if (view != NULL) delete view;
}

void tsdfTracking::doFusion(std::vector<explainMe::Cloud::Ptr> & clouds, std::vector<Eigen::Matrix4f> & alignments, std::string outFileSTL)
{
	//reset everything (how?)
	itmlib->denseMapper->ResetScene(itmlib->scene);

	//Image containers
	ORUtils::Image<ORUtils::Vector4<uchar>> rgbImage(itmlib->rgbImgSize, true, itmlib->isUsingCuda());
	ORUtils::Image<short> depthImage(itmlib->depthImgSize, true, itmlib->isUsingCuda());
	depthImage.ChangeDims(itmlib->depthImgSize);
	rgbImage.ChangeDims(itmlib->depthImgSize);

	//containers for results:
	ITMLib::Objects::ITMMesh * mesh = new ITMMesh(itmlib->isUsingCuda() ? MEMORYDEVICE_CUDA : MEMORYDEVICE_CPU);
	//keeps life visualization and some data needed for tracing the volume
	auto renderState_live = itmlib->visualisationEngine->CreateRenderState(itmlib->depthImgSize);
	ITMView * view = NULL;

	//1. Get the raw data.
	std::cout << "Engine processing frames...";
	auto & depthImgSize = itmlib->depthImgSize;
	short * dataPtr = depthImage.GetData(MEMORYDEVICE_CPU);
	for (int frm = 0; frm < clouds.size(); frm++){
		std::cout << "fame " << frm << "...\n";
		for (int i = 0, idx = 0; i < depthImgSize.height; i++) for (int j = 0; j < depthImgSize.width; j++)
		{
			float dz = clouds[frm]->at(j, i).z;
			dz = (dz * 0 == 0 ? dz : 0);
			dataPtr[idx] = std::floor(dz * 1000 + 0.5);
			rgbImage.GetData(MEMORYDEVICE_CPU)[idx].x = rgbImage.GetData(MEMORYDEVICE_CPU)[idx].y = rgbImage.GetData(MEMORYDEVICE_CPU)[idx].z = 0;

			//question: is depth to 3d point like it is for me?? //depth = dz. Not clear if converted correctly.
			//Quite a bit of mismatch! (for kinect2, due to lense undistortion.)
			auto & calib = itmlib->calib;
			auto & viewIntrinsics = calib.intrinsics_d.projectionParamsSimple.all;
			float z = float(dataPtr[idx])*calib.disparityCalib.params.x;
			float x = z * ((float(j) - viewIntrinsics.z) / viewIntrinsics.x);
			float y = z * ((float(i) - viewIntrinsics.w) / viewIntrinsics.y);
			if (idx % 10000 == 0 && frm == 0 && dz!= 0){
				std::cout << "(" << x << "," << y << ") vs (" << clouds[frm]->at(j, i).x << "," << clouds[frm]->at(j, i).y << ")\n";
			}//*/

			idx++;
		}
		itmlib->viewBuilder->UpdateView(&view, &rgbImage, &depthImage, itmlib->settings.useBilateralFilter, itmlib->settings.modelSensorNoise);
		// tracking
		//note that the infinitam matrices are column first.
		//everything needs to be transformed to first frame...
		//trackingController->Track(trackingState, view);
		itmlib->trackingState->requiresFullRendering = true;
		Matrix4f mat; mat.setIdentity();
		Eigen::Matrix4f m = alignments[frm] * alignments[0].inverse(); m.transposeInPlace();
		mat.m00 = m(0, 0); mat.m01 = m(0, 1); mat.m02 = m(0, 2); mat.m03 = m(0, 3);
		mat.m10 = m(1, 0); mat.m11 = m(1, 1); mat.m12 = m(1, 2); mat.m13 = m(1, 3);
		mat.m20 = m(2, 0); mat.m21 = m(2, 1); mat.m22 = m(2, 2); mat.m23 = m(2, 3);
		mat.m30 = m(3, 0); mat.m31 = m(3, 1); mat.m32 = m(3, 2); mat.m33 = m(3, 3);
		itmlib->trackingState->pose_d->SetM(mat);

		// fusion
		itmlib->denseMapper->ProcessFrame(view, itmlib->trackingState, itmlib->scene, renderState_live);
		std::cout << "No visible voxels: " << renderState_live->noVisibleEntries << "\n";

		// raycast to renderState_live for tracking and free visualisation
		//trackingController->Prepare(trackingState, view, renderState_live);
		itmlib->visualisationEngine->CreateExpectedDepths(itmlib->trackingState->pose_d, &(view->calib->intrinsics_d), renderState_live);
		itmlib->visualisationEngine->CreateICPMaps(view, itmlib->trackingState, renderState_live);
		itmlib->trackingState->pose_pointCloud->SetFrom(itmlib->trackingState->pose_d);

	}

	itmlib->meshingEngine->MeshScene(mesh, itmlib->scene);
	std::cout << "writing stl: " << outFileSTL;
	mesh->WriteSTL(outFileSTL.c_str());


	delete mesh, renderState_live;
	if (view != NULL) delete view;

	/*//I suspect that the are problems if too many voxels are visible at once. This for example happens for awful registrations...
	//settings:
	ITMLib::Objects::ITMRGBDCalib calib; Vector2i depthImgSize, rgbImgSize;
	initCalibFromDesc(calib, depthImgSize, rgbImgSize, d);
	ITMLib::Objects::ITMLibSettings settings;
	settings.sceneParams.viewFrustum_max = max_dist;
	//settings.deviceType = ITMLibSettings::DEVICE_CPU;
	settings.sceneParams.voxelSize = 0.01; //10 times larger than standard,

	bool useCuda = settings.deviceType == ITMLibSettings::DEVICE_CUDA;

	//Image data containers
	ORUtils::Image<ORUtils::Vector4<uchar>> rgbImage(rgbImgSize, true, useCuda);
	ORUtils::Image<short> depthImage(depthImgSize, true, useCuda);
	depthImage.ChangeDims(depthImgSize);
	rgbImage.ChangeDims(depthImgSize);
	
	//ITMLib Engines needed.
	//managing the voxels
	auto scene = new ITMScene<ITMVoxel, ITMVoxelIndex>(&(settings.sceneParams), settings.useSwapping,
		settings.deviceType == ITMLibSettings::DEVICE_CUDA ? MEMORYDEVICE_CUDA : MEMORYDEVICE_CPU);
		
	//casting rgb raw images into format
	ITMLib::Engine::ITMViewBuilder * viewBuilder;
	//raytracing
	ITMLib::Engine::ITMVisualisationEngine<ITMVoxel, ITMVoxelIndex> * visualisationEngine;
	//marching cubes
	ITMLib::Engine::ITMMeshingEngine<ITMVoxel, ITMVoxelIndex> * meshingEngine;
	if (useCuda){
		viewBuilder = new ITMViewBuilder_CUDA(&calib);
		visualisationEngine = new ITMVisualisationEngine_CUDA<ITMVoxel, ITMVoxelIndex>(scene);
		meshingEngine = new ITMMeshingEngine_CUDA<ITMVoxel, ITMVoxelIndex>();
	}
	else{
		viewBuilder = new ITMViewBuilder_CPU(&calib);
		visualisationEngine = new ITMVisualisationEngine_CPU<ITMVoxel, ITMVoxelIndex>(scene);
		meshingEngine = new ITMMeshingEngine_CPU<ITMVoxel, ITMVoxelIndex>();
	}
	ITMLib::Objects::ITMMesh * mesh = new ITMMesh(useCuda? MEMORYDEVICE_CUDA : MEMORYDEVICE_CPU);
	
	//keeps life visualization and some data needed for tracing the volume
	auto renderState_live = visualisationEngine->CreateRenderState(depthImgSize);
	//handles fusion
	auto denseMapper = new ITMDenseMapper<ITMVoxel, ITMVoxelIndex>(&settings);
	denseMapper->ResetScene(scene);
		
	//Used for tracking...
	//auto lowLevelEngine = new ITMLowLevelEngine_CUDA(); 
	//auto tracker = ITMTrackerFactory<ITMVoxel, ITMVoxelIndex>::Instance().Make(depthImgSize, &settings, lowLevelEngine, NULL, scene); //and removing this yields cuda errors. (The cuda constructor allocs some mem)
	//auto trackingController = new ITMTrackingController(tracker, visualisationEngine, lowLevelEngine, &settings);
	auto trackingState = new ITMTrackingState(depthImgSize, settings.deviceType == ITMLibSettings::DEVICE_CUDA ? MEMORYDEVICE_CUDA : MEMORYDEVICE_CPU);//trackingController->BuildTrackingState(depthImgSize);
	//tracker->UpdateInitialPose(trackingState);

	ITMView * view = NULL;
	
	//1. Get the raw data.
	std::cout << "Engine processing frames...";
	short * dataPtr = depthImage.GetData(MEMORYDEVICE_CPU);
	for (int frm = 0; frm < clouds.size(); frm++){
		std::cout << "fame " << frm << "...\n";
		for (int i = 0, idx = 0; i < depthImgSize.height; i++) for (int j = 0; j < depthImgSize.width; j++)
		{
			float dz = clouds[frm]->at(j, i).z;
			dz = (dz * 0 == 0 ? dz : 0);
			dataPtr[idx] = std::floor(dz * 1000 + 0.5);
			rgbImage.GetData(MEMORYDEVICE_CPU)[idx].x = rgbImage.GetData(MEMORYDEVICE_CPU)[idx].y = rgbImage.GetData(MEMORYDEVICE_CPU)[idx].z = 0;
			
			//question: is depth to 3d point like it is for me?? //depth = dz. Not clear if converted correctly.
			//auto & viewIntrinsics = calib.intrinsics_d.projectionParamsSimple.all;
			//float z = float(dataPtr[idx])*calib.disparityCalib.params.x;
			//float x = z * ((float(j) - viewIntrinsics.z) / viewIntrinsics.x);
			//float y = z * ((float(i) - viewIntrinsics.w) / viewIntrinsics.y);

			idx++;
		}
		viewBuilder->UpdateView(&view, &rgbImage, &depthImage, settings.useBilateralFilter, settings.modelSensorNoise);
		// tracking
		//note that the infinitam matrices are column first.
		//everything needs to be transformed to first frame...
		//trackingController->Track(trackingState, view);
		trackingState->requiresFullRendering = true;
		Matrix4f mat; mat.setIdentity();
		Eigen::Matrix4f m = alignments[frm] * alignments[0].inverse(); m.transposeInPlace();
		mat.m00 = m(0, 0); mat.m01 = m(0, 1); mat.m02 = m(0, 2); mat.m03 = m(0, 3);
		mat.m10 = m(1, 0); mat.m11 = m(1, 1); mat.m12 = m(1, 2); mat.m13 = m(1, 3);
		mat.m20 = m(2, 0); mat.m21 = m(2, 1); mat.m22 = m(2, 2); mat.m23 = m(2, 3);
		mat.m30 = m(3, 0); mat.m31 = m(3, 1); mat.m32 = m(3, 2); mat.m33 = m(3, 3);
		trackingState->pose_d->SetM(mat);		

		// fusion
		denseMapper->ProcessFrame(view, trackingState, scene, renderState_live);
		std::cout << "No visible voxels: " << renderState_live->noVisibleEntries << "\n";

		// raycast to renderState_live for tracking and free visualisation
		//trackingController->Prepare(trackingState, view, renderState_live);
		visualisationEngine->CreateExpectedDepths(trackingState->pose_d, &(view->calib->intrinsics_d), renderState_live);
		visualisationEngine->CreateICPMaps(view, trackingState, renderState_live);
		trackingState->pose_pointCloud->SetFrom(trackingState->pose_d);
				
	}

	meshingEngine->MeshScene(mesh, scene);
	std::cout << "writing stl: " << "D:/test_own_youcandeletethisfile.stl";
	mesh->WriteSTL("D:/test_own_youcandeletethisfile.stl");
	delete  scene, viewBuilder, visualisationEngine, meshingEngine, mesh, renderState_live, denseMapper, trackingState;
	//delete trackingController, lowLevelEngine, tracker;
	if (view != NULL) delete view;//*/
}

void tsdfTracking::doFusion_and_regist(std::vector<explainMe::Cloud::Ptr> & clouds, float max_dist, RangeSensorDescription * d)
{
	ITMLib::Objects::ITMRGBDCalib calib; Vector2i depthImgSize, rgbImgSize;
	initCalibFromDesc(calib, depthImgSize, rgbImgSize, d);

	ITMLib::Objects::ITMLibSettings settings;
	settings.sceneParams.viewFrustum_max = max_dist;
	//ITMLib::Objects::readRGBDCalib("E:/cast_to_rgb_d/rotating_assembly_ppm/calib.txt", calib);
	
	ITMLib::Engine::ITMMainEngine eng(&settings, &calib, depthImgSize, depthImgSize);
	bool useCuda = settings.deviceType == ITMLibSettings::DEVICE_CUDA;


	//1. Get the raw data.
	ORUtils::Image<ORUtils::Vector4<uchar>> rgbImage(rgbImgSize, true, useCuda);
	ORUtils::Image<short> depthImage(depthImgSize, true, useCuda);
	depthImage.ChangeDims(depthImgSize);
	rgbImage.ChangeDims(depthImgSize);
	short * dataPtr = depthImage.GetData(MEMORYDEVICE_CPU);
	
	std::cout << "Engine processing frames...";
	for (int frm = 0; frm < 1; frm++){
		for (int i = 0, idx = 0; i < depthImgSize.height; i++) for (int j = 0; j < depthImgSize.width; j++)
		{
			float dz = clouds[frm]->at(j, i).z;
			dz = (dz * 0 == 0 ? dz: 0);
			dataPtr[idx] = std::floor(dz * 1000 + 0.5);
			rgbImage.GetData(MEMORYDEVICE_CPU)[idx].x = rgbImage.GetData(MEMORYDEVICE_CPU)[idx].y = rgbImage.GetData(MEMORYDEVICE_CPU)[idx].z = 0;
			idx++;
		}
		eng.ProcessFrame(&rgbImage, &depthImage);
	}
		
	std::cout << "done. Storing mesh...";
	eng.SaveSceneToMesh("D:/deleteMe_i_test_mainEngine.stl");
	std::cout << "done.\n";
}
