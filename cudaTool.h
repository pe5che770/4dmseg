#pragma once
#define N_CUDA_EXAMPLE 10

#include "sensorDescriptionData.h"

struct float4;

class cudaTool
{
private:
	sensorDescriptionData sensor_desc;
	float *example/*[640][480]*/;

	float *dev_inSrc, *dev_outSrc;

	float *dev_normals, *dev_positions;
	int dev;


public:
	cudaTool(sensorDescriptionData desc);
	~cudaTool(void);

	void selectCudaDevice();
	void printCudaDeviceStats();

	void dev_setData(float * data);
	void bilateralFilter(float sig_s_in_pixel = 3, float sig_d = 0.05);
	void skipFiltering();
	void computeNormals(bool checkFlying = true);
	void dev_getData(float * data);
	void dev_getNormals(float* data);
};

