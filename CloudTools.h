#pragma once
#include "explainMeDefinitions.h"

class CloudTools
{
public:
	CloudTools(void);
	~CloudTools(void);

	static Eigen::Vector3f compute3DCentroid( explainMe::Cloud::Ptr & organizedCloud, explainMe::MatrixXb & mask );
	static Eigen::Vector3f compute3DCentroid( explainMe::Cloud::Ptr & cloud);
	static Eigen::Vector3f compute3DCentroid( explainMe::Cloud & cloud );
	static Eigen::Vector3f compute3DCentroid( explainMe::Cloud::Ptr & organizedCloud, explainMe::MatrixXf_rm & w, int w_i );

	static Eigen::Vector3f compute3DCentroid( pcl::PointCloud<pcl::PointSurfel>::Ptr & surfelCloud, bool ignoreAwayFacing = false);

	template<class T>
	static void apply(Eigen::Matrix4f & mat, /*explainMe::Cloud::Ptr*/T cloud);

	static void apply(std::vector<Eigen::Matrix4f> & mat, explainMe::Cloud::Ptr cloud, Eigen::ArrayXXi & transformations);

};


