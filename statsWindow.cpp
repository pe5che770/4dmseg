#include "statsWindow.h"
#include "qcustomplot.h"
#include <boost/filesystem.hpp>

statsWindow::statsWindow()
{
	initFields();
	this->setWindowTitle("Statistics");
	this->resize(1200, 400);
	this->show();
}


statsWindow::~statsWindow()
{
	std::cout << "Stats window is deleted";
}

void statsWindow::initFields()
{
	QLabel * timesLabel = new QLabel("Total time");
	QLabel * phases = new QLabel("time/phases");
	QLabel * initlabel = new QLabel("1. Init");
	QLabel * interpolationLabel = new QLabel("2.Interpolation");
	QLabel * motionLabel = new QLabel("3. Motion");
	QLabel * segLabel = new QLabel("4. Segmentation");
	QLabel * documentationLabel = new QLabel("Documentation");

	grandTotalTiming = new QLabel();
	initTiming = new QLabel("1");
	interpolationTiming = new QLabel("1");
	motionTiming = new QLabel("1");
	segTiming = new QLabel("1");
	documentationTiming = new QLabel("5");
	otherTiming = new QLabel("5");

	initPercentage = new QLabel("12");
	interpolationPercentage = new QLabel("33");
	motionPercentage = new QLabel("12");
	segPercentage = new QLabel("1");
	documentationPercentage = new QLabel("5");
	otherPercentage = new QLabel("5");

	initDetails = new QLabel("");
	interpolationDetails = new QLabel("");
	motionDetails = new QLabel("");
	segDetails = new QLabel("");
	documentationDetails = new QLabel("");
	otherDetails = new QLabel("");

	generalInfo = new QLabel("");

	totalProgress = new QProgressBar();
	totalEnergy = new QCustomPlot();
	numHyps = new QCustomPlot();
	numHyps->setFixedHeight(200);
	numHyps->setFixedWidth(300);
	numHyps->xAxis->setLabel("iteration");
	numHyps->yAxis->setLabel("#hypotheses");
	numHyps->addGraph();
	registrationEnergy = new QCustomPlot();
	registrationEnergy->setFixedHeight(200);
	registrationEnergy->setFixedWidth(300);
	registrationEnergy->addGraph();
	registrationEnergy->xAxis->setLabel("iteration");
	registrationEnergy->yAxis->setLabel("avg per point Error");
	registrationEnergy->graph(0)->setScatterStyle(QCPScatterStyle::ssCircle);
	registrationEnergy->graph(0)->setLineStyle(QCPGraph::lsNone);
	numRegEnergyPlots = 0; registrationEnergyMax = 0;
	totalEnergy->setFixedHeight(200);
	totalEnergy->setFixedWidth(300);
	totalEnergy->xAxis->setLabel("iteration");
	totalEnergy->yAxis->setLabel("energy");
	totalEnergy->addGraph();

	//numHyps = new C

	//QVBoxLayout * timing = new QVBoxLayout();
	//timing->addWidget(timesLabel);

	QGridLayout * timings = new QGridLayout();
	int line = 0;
	timings->addWidget(timesLabel, line, 0);
	timings->addWidget(grandTotalTiming, line, 1);
	line++;
	timings->addWidget(phases, line, 0);
	timings->addWidget(initlabel, line, 1);
	timings->addWidget(interpolationLabel, line, 2);
	timings->addWidget(motionLabel, line, 3);
	timings->addWidget(segLabel, line, 4);
	timings->addWidget(documentationLabel, line, 5);
	timings->addWidget(new QLabel("Other"), line, 6);
	
	timings->addWidget(new QLabel("times (ms)"), ++line, 0);
	timings->addWidget(initTiming, line, 1);
	timings->addWidget(interpolationTiming, line, 2);
	timings->addWidget(motionTiming, line, 3);
	timings->addWidget(segTiming, line, 4);
	timings->addWidget(documentationTiming, line, 5);
	timings->addWidget(otherTiming, line, 6);

	timings->addWidget(new QLabel("times (%)"), ++line, 0);
	timings->addWidget(initPercentage, line, 1);
	timings->addWidget(interpolationPercentage, line, 2);
	timings->addWidget(motionPercentage, line, 3);
	timings->addWidget(segPercentage, line, 4);
	timings->addWidget(documentationPercentage, line, 5);
	timings->addWidget(otherPercentage, line, 6);

	timings->addWidget(new QLabel("------------------Details---------------"), ++line, 0,1,6,Qt::AlignHCenter);
	timings->addWidget(new QLabel("init details"), ++line, 0);
	timings->addWidget(initDetails, line, 1, 1,6);
	timings->addWidget(new QLabel("interpolate"), ++line, 0);
	timings->addWidget(interpolationDetails, line, 1, 1, 6);
	timings->addWidget(new QLabel("motion details"), ++line, 0);
	timings->addWidget(motionDetails, line, 1, 1, 6);
	timings->addWidget(new QLabel("seg details"), ++line, 0);
	timings->addWidget(segDetails, line, 1, 1, 6);
	timings->addWidget(new QLabel("doc details"), ++line, 0);
	timings->addWidget(documentationDetails, line, 1, 1, 6);
	timings->addWidget(new QLabel("other details"), ++line, 0);
	timings->addWidget(otherDetails, line, 1, 1, 6);
	timings->addWidget(new QLabel("------------------Algorithm parameters---------------"), ++line, 0, 1, 6, Qt::AlignHCenter);

	QVBoxLayout * mainLayout = new QVBoxLayout();
	mainLayout->addWidget(totalProgress);
	//mainLayout->addLayout(timing);
	mainLayout->addLayout(timings);
	mainLayout->addWidget(generalInfo);
	mainLayout->addWidget(new QWidget(), 1);


	QHBoxLayout * total = new QHBoxLayout();
	total->addLayout(mainLayout);
	//QScrollArea *scroller = new QScrollArea();
	QVBoxLayout * scroller = new QVBoxLayout();
	scroller->addWidget(totalEnergy);
	scroller->addWidget(numHyps);
	scroller->addWidget(registrationEnergy);
	//scroller->addWidget(totalEnergy);
	total->addLayout(scroller);

	QWidget * tmp = new QWidget();
	tmp->setLayout(total);
	this->setCentralWidget(tmp);

	
}

void statsWindow::layoutGui()
{

}

void statsWindow::setGeneralInfo(const std::string & info){
	generalInfo->setText(info.c_str());
}

void statsWindow::captureTimingsAndGraphsAsImage(const std::string & folder, const std::string & file){
	boost::filesystem::path dir(folder);
	boost::filesystem::create_directory(dir);
	updateFromStats();
	this->update();
	std::cout << (QPixmap::grabWidget(this).save((folder + "/" + file).c_str()) ? "+" : "error saving img...");
}


void statsWindow::updateFromStats(){
	int total = 1;

	grandTotalTiming->setText(std::to_string(stats.grandTotal.total() / 1000.f).c_str());

	initTiming->setText(std::to_string(stats.getInitStats().total.total() / 1000.f).c_str());
	interpolationTiming->setText(std::to_string(stats.getInterpolateStats().total.total() / 1000.f).c_str());
	motionTiming->setText(std::to_string(stats.getMotionStats().total.total() / 1000.f).c_str());
	segTiming->setText(std::to_string(stats.getSegmentationStats().total.total() / 1000.f).c_str());
	documentationTiming->setText(std::to_string(stats.getDocStats().total.total() / 1000.f).c_str());
	otherTiming->setText(std::to_string(stats.getOtherStats().total.total() / 1000.f).c_str());

	total += stats.getInitStats().total.total();
	total += stats.getInterpolateStats().total.total();
	total += stats.getMotionStats().total.total();
	total += stats.getSegmentationStats().total.total();
	total += stats.getDocStats().total.total();
	total += stats.getOtherStats().total.total();


	initPercentage->setText(std::to_string(stats.getInitStats().total.total() * 1.0 / total).c_str());
	interpolationPercentage->setText(std::to_string(stats.getInterpolateStats().total.total()* 1.0 / total).c_str());
	motionPercentage->setText(std::to_string(stats.getMotionStats().total.total()* 1.0 / total).c_str());
	segPercentage->setText(std::to_string(stats.getSegmentationStats().total.total()* 1.0 / total).c_str());
	documentationPercentage->setText(std::to_string(stats.getDocStats().total.total()* 1.0 / total).c_str());
	otherPercentage->setText(std::to_string(stats.getOtherStats().total.total()* 1.0 / total).c_str());

	initDetails->setText(stats.getInitStats().details.to_string().c_str());
	interpolationDetails->setText(stats.getInterpolateStats().details.to_string().c_str());
	motionDetails->setText(stats.getMotionStats().details.to_string().c_str());
	segDetails->setText(stats.getSegmentationStats().details.to_string().c_str());
	documentationDetails->setText(stats.getDocStats().details.to_string().c_str());
	otherDetails->setText(stats.getOtherStats().details.to_string().c_str());
	totalProgress->setMaximum(stats.totalNumStep);
	totalProgress->setValue(stats.currentStep);

	if (totalEnergy->graph(0)->data()->count() != stats.energies.size()){
		QVector<double> x(stats.energies.size()), y(stats.energies.size());
		double max = 0;
		for (int i = 0; i < stats.energies.size();i++){
			x[i] = (i+1)*0.5;
			y[i] = stats.energies[i];
			max = (max < y[i] ? y[i] : max);
		}
		totalEnergy->graph(0)->setData(x, y);
		totalEnergy->xAxis->setRange(0, stats.energies.size()/2 + 1);
		totalEnergy->yAxis->setRange(0, max * 1.1);
		totalEnergy->replot();
	}


	if (numHyps->graph(0)->data()->count() != stats.numHyps.size()){
		QVector<double> x(stats.numHyps.size()), y(stats.numHyps.size());
		double max = 0;
		for (int i = 0; i < stats.numHyps.size(); i++){
			x[i] = (i + 1)*0.5;
			y[i] = stats.numHyps[i];
			max = (max < y[i] ? y[i] : max);
		}
		numHyps->graph(0)->setData(x, y);
		numHyps->xAxis->setRange(0, stats.numHyps.size() / 2 + 1);
		numHyps->yAxis->setRange(0, max * 1.1);
		numHyps->replot();
	}


	if (numRegEnergyPlots != stats.avgRegErr.size()){
		numRegEnergyPlots = stats.avgRegErr.size();
		
		//QVector<double> x(stats.numHyps.size()), y(stats.numHyps.size());
		
		for (int i = 0; i < stats.avgRegErr.back().size(); i++){
			registrationEnergy->graph(0)->addData(numRegEnergyPlots, stats.avgRegErr.back()[i]);
			registrationEnergyMax = (registrationEnergyMax <  stats.avgRegErr.back()[i] ? stats.avgRegErr.back()[i] : registrationEnergyMax);
			std::cout << stats.avgRegErr.back()[i];
		}
		registrationEnergy->xAxis->setRange(0, numRegEnergyPlots + 1);
		registrationEnergy->yAxis->setRange(0, registrationEnergyMax * 1.1);
		registrationEnergy->replot();

		
	}
}