#pragma once

#include "trajectory.h"
#include "explainMeDefinitions.h"
#include "Sensors.h"
#include "statsKeeper.h"

/**
* Interface for initialisation method.
*/

class Initializer
{
public:
	typedef boost::shared_ptr<Initializer> Ptr;
	static bool abortInit;

	Initializer(void);
	virtual ~Initializer(void);
	static void abort(){ abortInit = true; }

	virtual void computeInitialisation(std::vector<explainMe::Cloud::Ptr> & clouds_as_grids, 
		RangeSensorDescription & description,
		std::vector<trajectory_R> & target_traj, explainMe::MatrixXf_rm & target_seg_weights,
		stats * statistics = NULL)= 0;

};





//interface for finding trajectories.
class InitializeTrajectories
{
public:
	typedef boost::shared_ptr<InitializeTrajectories> Ptr;

	InitializeTrajectories();
	virtual ~InitializeTrajectories();
	virtual void findTrajectories(std::vector<explainMe::Cloud::Ptr> & clouds_as_grids, 
		RangeSensorDescription * description,
		std::vector<trajectory_R> & target_traj) = 0;

	//remove raggy paths
	static void filterTrajectories(
		std::vector<trajectory_R> & paths_to_filter,
		float topAcceleration);
};


//interface for clustering
class ComputeWeights
{
public:
	typedef boost::shared_ptr<ComputeWeights> Ptr;
	static bool abortComp;

	static void abort(){
		abortComp = true;
	}

	ComputeWeights(){}
	virtual ~ComputeWeights(){}
	virtual void findWeights(std::vector<trajectory_R> & traj_in, explainMe::MatrixXf_rm & w_out) = 0;
};


class CompositeInitializer
	:public Initializer
{
	InitializeTrajectories::Ptr initTraj;
	ComputeWeights::Ptr compWeight;
public:
	CompositeInitializer(InitializeTrajectories::Ptr traj_computation, ComputeWeights::Ptr segmenter)
	{
		initTraj = traj_computation;
		compWeight = segmenter;
	}
	virtual ~CompositeInitializer(){}

	void computeInitialisation(std::vector<explainMe::Cloud::Ptr> & clouds_as_grids, 
		RangeSensorDescription & description,
		std::vector<trajectory_R> & target_traj, explainMe::MatrixXf_rm & target_seg_weights,
		stats * statistics = NULL)
	{
		if (statistics != NULL){
			statistics->total.restart();
			(*statistics)["extract trajectories"].restart();
		}
		initTraj->findTrajectories(clouds_as_grids, & description, target_traj);
		if (abortInit){
			return;
		}
		if (statistics != NULL){
			(*statistics)["extract trajectories"].stop();
			(*statistics)["weights optimization"].restart();
		}
		std::cout << "Minimizing weights...";
		compWeight->findWeights(target_traj, target_seg_weights);
		if (statistics != NULL){
			(*statistics).total.stop();
			(*statistics)["weights optimization"].stop();
		}
	}

};

class FromFileInitializer:
	public Initializer
{
private:
	std::string file;
public:
	FromFileInitializer(std::string file){
		this->file = file;
	}
	virtual ~FromFileInitializer(){}
	void computeInitialisation(std::vector<explainMe::Cloud::Ptr> & clouds_as_grids, 
		RangeSensorDescription & description,
		std::vector<trajectory_R> & target_traj, explainMe::MatrixXf_rm & target_seg_weights,
		stats * statistics = NULL);
};