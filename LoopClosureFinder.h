#pragma once

#include "explainMeDefinitions.h"
#include "Sensors.h"

class LoopClosureFinder
{
public:
	LoopClosureFinder(void);
	~LoopClosureFinder(void);

	void findConstraints(
		int num_constraints,
		std::vector<Eigen::Matrix4f> & alignments, 
		std::vector<Eigen::Vector3f> & centroids,
		RangeSensorDescription & d,
		std::vector<std::pair<int,int>> & result_newConstraints,
		int maxAllowedDist = -1,
		float min_quality = 0.5f // the cos between view angles on the object must be at least 0.5 => the angle must be smaller 60 deg
		);
};

