#############################################################################
# Description:
# Try to findInfiniTAM library & includes
#
# InfiniTAM_FOUND
# InfiniTAM_INCLUDE_DIRS
# InfiniTAM_LIBRARIES
# InfiniTAM_LIBRARIES_DEBUG
#############################################################################

include(FindPackageHandleStandardArgs)

set(InfiniTAM_INC_SEARCH_PATH /usr/local/include/)
set(InfiniTAM_LIB_SEARCH_PATH /usr/local/lib/)
set(InfiniTAM_ROOT "" CACHE PATH "Path to root of InfiniTAM git repository")


#find_path(INFINITAM_CONFIG_INCLUDE_DIR Config.h
#        PATHS
#        $ENV{InfiniTAM_HOME}/InfiniTAM/
#        ${InfiniTAM_INC_SEARCH_PATH}/InfiniTAM/
#        ${InfiniTAM_ROOT}/InfiniTAM/cmake-build-release
#        )

find_path(ITM_INCLUDE_DIR ITMLib.h
        PATHS
        $ENV{InfiniTAM_HOME}/InfiniTAM/ITMLib
        ${InfiniTAM_INC_SEARCH_PATH}/InfiniTAM/ITMLib
        ${InfiniTAM_ROOT}/InfiniTAM/ITMLib
        )

find_library(ITM_LIBRARY
        NAMES ITMLib itmlib
        PATHS
        $ENV{InfiniTAM_HOME}/InfiniTAM/ITMLib
        ${InfiniTAM_LIB_SEARCH_PATH}/InfiniTAM/ITMLib
        ${InfiniTAM_ROOT}/InfiniTAM/ITMLib
        ${InfiniTAM_ROOT}/InfiniTAM/build/ITMLib
        ${InfiniTAM_ROOT}/InfiniTAM/cmake-build-release/ITMLib
        )

find_library(ITM_LIBRARY_DEBUG
        NAMES ITMLib itmlib itmlib_d ITMLib_d
        PATHS
        $ENV{InfiniTAM_HOME}/InfiniTAM/ITMLib
        ${InfiniTAM_LIB_SEARCH_PATH}/InfiniTAM/ITMLib
        ${InfiniTAM_ROOT}/InfiniTAM/ITMLib
        ${InfiniTAM_ROOT}/InfiniTAM/build_debug/ITMLib
        ${InfiniTAM_ROOT}/InfiniTAM/cmake-build-debug/ITMLib
        )

find_package_handle_standard_args(InfiniTAM DEFAULT_MSG
        ITM_LIBRARY ITM_INCLUDE_DIR INFINITAM_CONFIG_INCLUDE_DIR)

if(InfiniTAM_FOUND)
    set(InfiniTAM_LIBRARIES
            ${ITM_LIBRARY})
    set(InfiniTAM_LIBRARIES_DEBUG
            ${ITM_LIBRARY_DEBUG})
    set(InfiniTAM_INCLUDE_DIRS
            ${ITM_INCLUDE_DIR}
            ${INFINITAM_CONFIG_INCLUDE_DIR})
endif()