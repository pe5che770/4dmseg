#include "LoopClosureFinder.h"


LoopClosureFinder::LoopClosureFinder(void)
{
}


LoopClosureFinder::~LoopClosureFinder(void)
{
}


void LoopClosureFinder::findConstraints(
	int num_constraints,
	std::vector<Eigen::Matrix4f> & alignments, 
	std::vector<Eigen::Vector3f> & centroids,
	RangeSensorDescription & d,
	std::vector<std::pair<int,int>> & result_newConstraints,
	int maxAllowedLength,
	float min_cos_angle 
	)
{
	std::vector<std::pair<int,int>> eligible_pairs;

	Eigen::Matrix4f i_to_j, j_to_i, frustum;
	Eigen::Vector4f centroid_i, centroid_j, tmp;

	frustum << 
		d.frustum()(0,0), d.frustum()(0,1),d.frustum()(0,2),d.frustum()(0,3),
		d.frustum()(1,0),d.frustum()(1,1),d.frustum()(1,2),d.frustum()(1,3),
		d.frustum()(2,0),d.frustum()(2,1),d.frustum()(2,2),d.frustum()(2,3),
		d.frustum()(3,0),d.frustum()(3,1),d.frustum()(3,2),d.frustum()(3,3);


	std::vector<Eigen::Matrix4f>  alignments_inv = alignments;
	for(int i = 0; i < alignments.size(); i++){
		alignments_inv[i] = alignments[i].inverse();
	}

	//for each frame pair: Test
	for(int i = 0; i < alignments.size()-1; i++){
		for(int j = i+1; j < alignments.size(); j++){
			if(maxAllowedLength > 0 && std::abs<int>(i-j) > maxAllowedLength){
				continue;
			}


			//1. both centroids inside the frustum
			centroid_i << centroids[i] ,1;
			centroid_j << centroids[j] ,1;
			j_to_i = alignments[i] * alignments_inv[j];
			i_to_j = alignments[j] * alignments_inv[i];
				
			//Everything is in cam coordinates.
			//Does not take distortion into account.
			tmp = frustum * j_to_i * centroid_j;
			tmp /= tmp.w();
			bool j_to_i_inside = std::abs(tmp.x()) <= 1 && std::abs(tmp.y()) <= 1 && tmp.z() >=0;
			tmp = frustum * i_to_j * centroid_i;
			tmp /= tmp.w();
			bool i_to_j_inside = std::abs(tmp.x()) <= 1 && std::abs(tmp.y()) <= 1 && tmp.z() >=0;
			if(!(j_to_i_inside && i_to_j_inside)){
				continue;
			}

			//2. centroid distance to camera similar, ensures that the sampling density 
			// is not totally dissimillar
			//could be used to rate corresp
			float dist_ratio = centroids[i].norm() / (i_to_j * centroid_i).topLeftCorner<3,1>().norm();
			float dist_ratio2 = centroids[j].norm() / (j_to_i * centroid_j).topLeftCorner<3,1>().norm();
			if(dist_ratio < 1.f/3 || dist_ratio > 3 || dist_ratio2 < 1.f/3 || dist_ratio2 > 3){
				//not similar enough
				continue;
			}

			//3. angular view on object similar. That is, the transformed view direction centroid->cam 
			//is small enough. could be used to rate the corresp.
			centroid_i.w() = 0; centroid_j.w() = 0;
			centroid_i.normalize(); centroid_j.normalize();
			float i_dot_i = (i_to_j * centroid_i).dot(centroid_i);
			float j_dot_j = (j_to_i * centroid_j).dot(centroid_j);

			//allow up to 60 degrees..
			if(i_dot_i < min_cos_angle || j_dot_j < min_cos_angle){
				continue;

			}
			eligible_pairs.push_back(std::pair<int,int>(i,j));

		}
	}//end listing up eligible pairs,


	//Greedily select a sparse set of eligible edges, that is somewhat as sparse as possible.
	result_newConstraints.clear();
	//selected edges by default: (i,i+1)
	//select edges (start, end), such that min_selected(abs(start-a) + abs(end-b) ) is maximal

	std::cout << eligible_pairs.size() << " eligible loop edges\n";

	while(result_newConstraints.size() < num_constraints ){
		int max_min_dist = 0, index = -1, min_dist, dist;
		for(auto it = eligible_pairs.begin(); it != eligible_pairs.end(); it++){
			min_dist = it->second - it->first - 1; //dist to the default edges
			for(auto edge = result_newConstraints.begin(); edge != result_newConstraints.end(); edge++){
				dist = std::abs( it->first - edge->first) + std::abs( it->second - edge->second);
				//could exclude eligible pairs further away than something... to incrementally increase the distance.
				min_dist = (min_dist < dist ? min_dist: dist);
			}

			if(min_dist > max_min_dist){
				max_min_dist = min_dist;
				index = it - eligible_pairs.begin();
			}
		}

		//if there is nothing to add - stop.
		if(index < 0 || max_min_dist == 0){
			break;
		}

		result_newConstraints.push_back(std::pair<int,int>(eligible_pairs[index]));
	}


	//std::cout << "Found following loops:\n";
	//for(int i = 0; i< result_newConstraints.size(); i++){
	//	std::cout << "\t" << result_newConstraints[i].first << "," << result_newConstraints[i].second;
	//	
	//}
	//std::cout <<  "\n";

}