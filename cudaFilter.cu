#include "cudaFilter.h"
#include <cuda.h>
#include "cudaHelper.h"


//////////////////////////// CUDA code //////////////////////////////

__constant__ int width_height[2];
texture<float,2> tex_in, tex_in0, tex_in1, tex_in2, tex_out;

__global__ void boxFilter_vertical_inBuffer(float *out, int kernel_width_2, int tex_unit)
{
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	int offset = blockIdx.x + blockIdx.y*width_height[0];//WIDTH;

	float total=0;
	if(tex_unit == -1){
		for(int k = -kernel_width_2; k <= kernel_width_2; k++){
			total += tex2D(tex_in, i+k, j);
		}
	}
	else if(tex_unit == 0){
		for(int k = -kernel_width_2; k <= kernel_width_2; k++){
			total += tex2D(tex_in0, i+k, j);
		}
	}
	else if(tex_unit == 1){
		for(int k = -kernel_width_2; k <= kernel_width_2; k++){
			total += tex2D(tex_in1, i+k, j);
		}
	}
	else{
		for(int k = -kernel_width_2; k <= kernel_width_2; k++){
			total += tex2D(tex_in2, i+k, j);
		}
	}
	out[offset]= total / (2*kernel_width_2 + 1);

}

__global__ void boxFilter_horizontal_outBuffer(float *out, int kernel_width_2)
{
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	int offset = blockIdx.x + blockIdx.y*width_height[0];//WIDTH;

	float total=0;
	for(int k = -kernel_width_2; k <= kernel_width_2; k++){
		total += tex2D(tex_out, i, j+k);
	}
	out[offset]= total / (2*kernel_width_2 + 1);

}


__global__ void boxFilter_vertical_inBuffer(float *out, int * mask, int kernel_width_2, int tex_unit)
{
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	int offset = blockIdx.x + blockIdx.y*width_height[0];//WIDTH;
	
	int ref_mask = mask[offset];
	float total=0;
	float weight;
	float total_weight = 0;
	if(tex_unit == -1){
		for(int k = -kernel_width_2; k <= kernel_width_2; k++){
			weight =  (mask[(i+k) + j* width_height[0]] == ref_mask? 1:0);
			total += tex2D(tex_in, i+k, j) * weight;
			total_weight += weight;
		}
	}
	else if(tex_unit == 0){
		for(int k = -kernel_width_2; k <= kernel_width_2; k++){
			weight =  (mask[(i+k) + j* width_height[0]] == ref_mask? 1:0);
			total += tex2D(tex_in0, i+k, j) * weight;
			total_weight += weight;
		}
	}
	else if(tex_unit == 1){
		for(int k = -kernel_width_2; k <= kernel_width_2; k++){
			weight = (mask[(i+k) + j* width_height[0]] == ref_mask? 1:0);
			total += tex2D(tex_in1, i+k, j)* weight;
			total_weight += weight;
		}
	}
	else if(tex_unit == 2){
		for(int k = -kernel_width_2; k <= kernel_width_2; k++){
			weight = (mask[(i+k) + j* width_height[0]] == ref_mask? 1:0);
			total += tex2D(tex_in2, i+k, j)*weight;
			total_weight += weight;
		}
	}
	out[offset]= total / total_weight;

}

__global__ void boxFilter_horizontal_outBuffer(float *out, int * mask, int kernel_width_2)
{
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	int offset = blockIdx.x + blockIdx.y*width_height[0];//WIDTH;

	int ref_mask = mask[offset];
	float total=0;
	float weight;
	float total_weight = 0;
	for(int k = -kernel_width_2; k <= kernel_width_2; k++){
		weight = (mask[i + (j+k)* width_height[0]] == ref_mask? 1:0);
		total += tex2D(tex_out, i, j+k)*weight;
		total_weight += weight;
	}
	out[offset]= total / total_weight;

}

__global__ void normalGuidedFilter_vertical_inBuffer(float *out, int * mask, int kernel_width_2)
{
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	int offset = blockIdx.x + blockIdx.y*width_height[0];//WIDTH;
	
	int ref_mask = mask[offset];
	float ref_normal_x = tex2D(tex_in0, i, j);
	float ref_normal_y = tex2D(tex_in1, i, j);
	float ref_normal_z = tex2D(tex_in2, i, j);
	float total=0;
	float weight;
	float total_weight = 0;
	
	for(int k = -kernel_width_2; k <= kernel_width_2; k++){
		weight = (ref_normal_x * tex2D(tex_in0, i+k, j) + ref_normal_y * tex2D(tex_in1, i+k, j) + ref_normal_z * tex2D(tex_in2, i+k, j) );
		weight = weight * weight;
		weight = weight * weight;
		
		weight = (weight>0 && weight * 0 == 0? weight: 0) 
			* (mask[(i+k) + j* width_height[0]] == ref_mask? 1:0) ;
		total += tex2D(tex_in, i+k, j) * weight;
		total_weight += weight;
	}
	
	out[offset]= ( total_weight > 0 ? total/total_weight:0);
}

__global__ void normalGuidedFilter_horizontal_outBuffer(float *out, int * mask, int kernel_width_2)
{
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	int offset = blockIdx.x + blockIdx.y*width_height[0];//WIDTH;
	
	int ref_mask = mask[offset];
	float ref_normal_x = tex2D(tex_in0, i, j);
	float ref_normal_y = tex2D(tex_in1, i, j);
	float ref_normal_z = tex2D(tex_in2, i, j);
	float total=0;
	float weight;
	float total_weight = 0;
	
	for(int k = -kernel_width_2; k <= kernel_width_2; k++){
		weight =  (ref_normal_x * tex2D(tex_in0, i, j+k) + ref_normal_y * tex2D(tex_in1, i, j+k) + ref_normal_z * tex2D(tex_in2, i, j+k) );
		weight = weight * weight;
		weight = weight * weight;
		weight = (weight>0 && weight * 0 == 0? weight: 0) 
			*(mask[(i) + (j+k)* width_height[0]] == ref_mask? 1:0);
		total += tex2D(tex_out, i, j+k) * weight;
		total_weight += weight;
	}
	
	out[offset]= ((total_weight > 0 && (total/total_weight)*0 == 0) ? total/total_weight:0);
}


__global__ void abs_gradient_inBuffer(float *out, int * mask, int kernel_width_2)
{
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	int offset = blockIdx.x + blockIdx.y*width_height[0];//WIDTH;
	
	int ref_mask = mask[offset];
	float ref_normal_x = tex2D(tex_in0, i, j);
	float ref_normal_y = tex2D(tex_in1, i, j);
	float ref_normal_z = tex2D(tex_in2, i, j);
	float total=0;
	float nrm;
	float total_weight = 0;
	float weight;
	
	for(int k = -kernel_width_2; k <= kernel_width_2; k++){
		for(int l = -kernel_width_2; l <= kernel_width_2; l++){
			nrm = 1;//sqrtf(0.f + k*k + l*l)/10;
			
			weight =  (ref_normal_x * tex2D(tex_in0, i+k, j+l) + ref_normal_y * tex2D(tex_in1, i+k, j+l) + ref_normal_z * tex2D(tex_in2, i+k, j+l) );
			weight = weight * weight;
			weight = weight * weight;
			weight = weight * (mask[(i+k) + (j+l)* width_height[0]] == ref_mask? 1:0);

			total += abs(tex2D(tex_in, i+k, j+l) -tex2D(tex_in, i, j))*weight/nrm;
			total_weight += weight;
		}	
	}
	
	out[offset]= ( total_weight > 0 ? total/total_weight:0);
}


__global__ void masked_min_horizontal_texin(float *out, float *out_dx, 
									  int * mask, int kernel_width_2)
{
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	int offset = blockIdx.x + blockIdx.y*width_height[0];//WIDTH;
	

	float min =  tex2D(tex_in, i,j);
	float curr;
	int di_min = 1;
	bool is_closer;

	for(int k = -kernel_width_2; k <= kernel_width_2; k++){
		curr =tex2D(tex_in, i+k,j);
		is_closer = (curr > 0  && curr < min && mask[i+k + j*width_height[0]] > 0.5f);
		di_min = (is_closer? k : di_min);
		min = (is_closer? curr: min);
	}

	out_dx[offset] = di_min;
	out[offset] = min;
}


__global__ void flyingPixels_vertical_texout(float *out,  int * mask, int kernel_width_2)
{
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	int offset = blockIdx.x + blockIdx.y*width_height[0];//WIDTH;

	float this_depth = tex2D(tex_in, i,j);
	float curr, min = 1e10;
	int dj_min = 0;
	bool is_closer;

	for(int l = -kernel_width_2; l <= kernel_width_2; l++){
		curr =tex2D(tex_out, i,j+l);
		is_closer = (curr > 0  && curr < min);
		dj_min = (is_closer? l : dj_min);
		min = (is_closer? curr: min);
	}

	float di_min = tex2D(tex_in0, i, j + dj_min);
	out[offset] = (mask[offset] > 0.5f ? 1:(this_depth- min)*30/(sqrtf(1+di_min*di_min + dj_min*dj_min)));

}
__global__ void flyingPixels_inBuffer(float *out, int * mask, int kernel_width_2)
{
	int i = (blockIdx.x*blockDim.x) + threadIdx.x;
	int j = (blockIdx.y*blockDim.y) + threadIdx.y;
	int di_min, dj_min;
	int offset = blockIdx.x + blockIdx.y*width_height[0];//WIDTH;
	
	float this_depth = tex2D(tex_in, i,j);
	float min_depth = 1e10;
	bool is_closer;

	for(int k = -kernel_width_2; k <= kernel_width_2; k++){
		for(int l = -kernel_width_2; l <= kernel_width_2; l++){
			is_closer = (tex2D(tex_in, i+k,j+l) > 0  && tex2D(tex_in, i+k,j+l) < min_depth);
			is_closer = (is_closer? mask[i+k + (j+l)*width_height[0]] >0.5f : false);
			min_depth = (is_closer ? tex2D(tex_in, i+k,j+l): min_depth);
			di_min = (is_closer? k: di_min);
			dj_min = (is_closer? k: dj_min);
		}
	}

	out[offset] = (mask[offset] > 0.5f ? 1: (this_depth- min_depth)*50/(sqrtf(di_min*di_min + dj_min*dj_min)));
}

/////////////////////////////////////////////////////////////////////



cudaFilter::cudaFilter(sensorDescriptionData sd)
{
	result_is_in_IN_buffer = false;
	sensor_desc = sd;

	buffer = new float[ sd._width*sd._height];

	//choose cuda device...
	printCudaDeviceStats();
	selectCudaDevice();

	//allocate buffers
	HANDLE_ERROR(cudaMalloc((void **) & dev_inSrc, sd._width*sd._height*sizeof(float)));
	HANDLE_ERROR(cudaMalloc((void **) & dev_inSrc0, sd._width*sd._height*sizeof(float)));
	HANDLE_ERROR(cudaMalloc((void **) & dev_inSrc1, sd._width*sd._height*sizeof(float)));
	HANDLE_ERROR(cudaMalloc((void **) & dev_inSrc2, sd._width*sd._height*sizeof(float)));
	HANDLE_ERROR(cudaMalloc((void **) & dev_outSrc, sd._width*sd._height*sizeof(float)));

	HANDLE_ERROR(cudaMalloc((void **) & dev_mask, sd._width*sd._height*sizeof(int)));

	//bind textures
	cudaChannelFormatDesc desc = cudaCreateChannelDesc<float>();
	HANDLE_ERROR(cudaBindTexture2D(NULL, tex_in, dev_inSrc,desc, sd._width, sd._height, sd._width*sizeof(float)));
	HANDLE_ERROR(cudaBindTexture2D(NULL, tex_in0, dev_inSrc0,desc, sd._width, sd._height, sd._width*sizeof(float)));
	HANDLE_ERROR(cudaBindTexture2D(NULL, tex_in1, dev_inSrc1,desc, sd._width, sd._height, sd._width*sizeof(float)));
	HANDLE_ERROR(cudaBindTexture2D(NULL, tex_in2, dev_inSrc2,desc, sd._width, sd._height, sd._width*sizeof(float)));
	HANDLE_ERROR(cudaBindTexture2D(NULL, tex_out, dev_outSrc,desc, sd._width, sd._height, sd._width*sizeof(float)));

	//transmit width and height of the input data
	int cpu_wh[2] = {sd._width,sd._height};
	HANDLE_ERROR(cudaMemcpyToSymbol(width_height,cpu_wh, sizeof(int)*2));
}


cudaFilter::~cudaFilter(void)
{
	//unbind cuda texture
	cudaUnbindTexture(tex_in);
	cudaUnbindTexture(tex_in0);
	cudaUnbindTexture(tex_in1);
	cudaUnbindTexture(tex_in2);
	cudaUnbindTexture(tex_out);
	//free cuda memory
	cudaFree(dev_inSrc);
	cudaFree(dev_inSrc0);
	cudaFree(dev_inSrc1);
	cudaFree(dev_inSrc2);
	cudaFree(dev_outSrc);
	cudaFree(dev_mask);

	delete buffer;

}


void cudaFilter::printCudaDeviceStats(){
	cudaDeviceProp prop;
	int count;

	//list devices...
	HANDLE_ERROR(cudaGetDeviceCount(&count));
	//std::cout << "There are " << count << " cuda devices \n";
	for(int i = 0; i < count; i++){
		cudaGetDeviceProperties(&prop,i);
		/*std::cout << prop.name << " \n";
		std::cout << prop.multiProcessorCount << " cuda multi processors\n";
		std::cout << "max Grid dim: " <<prop.maxGridSize[0] << ","
			<<prop.maxGridSize[1] << ","
			<<prop.maxGridSize[2] << "\n";
		std::cout << "Threads: " << prop.maxThreadsDim[0] << ", " 
			<< prop.maxThreadsDim[1] << ", " 
			<< prop.maxThreadsDim[2] << "\n";
			*/
	}
}

void cudaFilter::selectCudaDevice(){
	cudaDeviceProp prop;
	memset(&prop,0,sizeof(cudaDeviceProp));
	prop.major = 1;
	prop.minor = 3;
	HANDLE_ERROR(cudaChooseDevice(&dev, &prop));
	//std::cout << "choosing device number" << dev;
	HANDLE_ERROR(cudaSetDevice(dev));
}


void cudaFilter::dev_setMask(int * data)
{
	HANDLE_ERROR(cudaMemcpy(dev_mask, data, sensor_desc._height * sensor_desc._width * sizeof(int),cudaMemcpyHostToDevice));
}


//set dev_inSrc
void cudaFilter::dev_setData(float * data)
{
											 //HEIGHT * WIDTH*
	HANDLE_ERROR(cudaMemcpy(dev_inSrc, data, sensor_desc._height * sensor_desc._width * sizeof(float),cudaMemcpyHostToDevice));
}

//set dev_inSrc
void cudaFilter::dev_set3dData(float * data)
{
	for(int i = 0; i < 3; i++){
		for(int j = 0; j <  sensor_desc._width*sensor_desc._height; j++){
			buffer[j] =data[j*3+i];
		}
		
		switch(i){
		case 0:
			HANDLE_ERROR(cudaMemcpy(dev_inSrc0, buffer, sensor_desc._height * sensor_desc._width * sizeof(float),cudaMemcpyHostToDevice));
			break;
		case 1:
			HANDLE_ERROR(cudaMemcpy(dev_inSrc1, buffer, sensor_desc._height * sensor_desc._width * sizeof(float),cudaMemcpyHostToDevice));
			break;
		case 2:
			HANDLE_ERROR(cudaMemcpy(dev_inSrc2, buffer, sensor_desc._height * sensor_desc._width * sizeof(float),cudaMemcpyHostToDevice));
			break;
		}
	}
}

void cudaFilter::dev_getData(float * data)
{
	if(result_is_in_IN_buffer){
		HANDLE_ERROR(cudaMemcpy(data, dev_inSrc, sensor_desc._height * sensor_desc._width*sizeof(float),cudaMemcpyDeviceToHost));
	}
	else{
		HANDLE_ERROR(cudaMemcpy(data, dev_outSrc, sensor_desc._height * sensor_desc._width*sizeof(float),cudaMemcpyDeviceToHost));
	}
}


void cudaFilter::dev_get3dData(float * data)
{

	HANDLE_ERROR(cudaMemcpy(buffer, dev_inSrc0, sensor_desc._height * sensor_desc._width*sizeof(float),cudaMemcpyDeviceToHost));
	for(int i = 0; i < sensor_desc._height * sensor_desc._width; i++){
		data[i*3] = buffer[i];
	}
	HANDLE_ERROR(cudaMemcpy(buffer, dev_inSrc1, sensor_desc._height * sensor_desc._width*sizeof(float),cudaMemcpyDeviceToHost));
	for(int i = 0; i < sensor_desc._height * sensor_desc._width; i++){
		data[i*3+1] = buffer[i];
	}
	HANDLE_ERROR(cudaMemcpy(buffer, dev_inSrc2, sensor_desc._height * sensor_desc._width*sizeof(float),cudaMemcpyDeviceToHost));
	for(int i = 0; i < sensor_desc._height * sensor_desc._width; i++){
		data[i*3+2] = buffer[i];
	}
}

void cudaFilter::boxFilter(int width_2, bool useMask){
	//in=>out=>in
	result_is_in_IN_buffer = true;
	
	dim3 blocks(sensor_desc._width, sensor_desc._height);

	if(useMask){
		boxFilter_vertical_inBuffer<<< blocks,1>>>(dev_outSrc, dev_mask, width_2,-1);
		boxFilter_horizontal_outBuffer<<< blocks,1>>>(dev_inSrc,dev_mask, width_2);
	}
	else{
		boxFilter_vertical_inBuffer<<< blocks,1>>>(dev_outSrc, width_2,-1);
		boxFilter_horizontal_outBuffer<<< blocks,1>>>(dev_inSrc, width_2);
	}

}

void cudaFilter::boxFilter3d(int width_2, bool useMask){
	//in=>out=>in
	
	dim3 blocks(sensor_desc._width, sensor_desc._height);

	//box filter all channels independently.
	if(!useMask){
		boxFilter_vertical_inBuffer<<< blocks,1>>>(dev_outSrc, width_2,0);
		boxFilter_horizontal_outBuffer<<< blocks,1>>>(dev_inSrc0, width_2);

	
		boxFilter_vertical_inBuffer<<< blocks,1>>>(dev_outSrc, width_2,1);
		boxFilter_horizontal_outBuffer<<< blocks,1>>>(dev_inSrc1, width_2);
	

		boxFilter_vertical_inBuffer<<< blocks,1>>>(dev_outSrc, width_2,2);
		boxFilter_horizontal_outBuffer<<< blocks,1>>>(dev_inSrc2, width_2);
	}
	else{
		
		boxFilter_vertical_inBuffer<<< blocks,1>>>(dev_outSrc, dev_mask, width_2,0);
		boxFilter_horizontal_outBuffer<<< blocks,1>>>(dev_inSrc0,dev_mask, width_2);

	
		boxFilter_vertical_inBuffer<<< blocks,1>>>(dev_outSrc, dev_mask,width_2,1);
		boxFilter_horizontal_outBuffer<<< blocks,1>>>(dev_inSrc1, dev_mask,width_2);
	

		boxFilter_vertical_inBuffer<<< blocks,1>>>(dev_outSrc, dev_mask,width_2,2);
		boxFilter_horizontal_outBuffer<<< blocks,1>>>(dev_inSrc2, dev_mask,width_2);

	}
}

void cudaFilter::normalGuidedFilter(int width_2){
	result_is_in_IN_buffer = true;

	dim3 blocks(sensor_desc._width, sensor_desc._height);

	normalGuidedFilter_vertical_inBuffer<<<blocks,1>>>(dev_outSrc, dev_mask, width_2);
	//boxFilter_vertical_inBuffer<<< blocks,1>>>(dev_outSrc,dev_mask, width_2,-1);

	normalGuidedFilter_horizontal_outBuffer<<<blocks,1>>>(dev_inSrc, dev_mask, width_2);
	//boxFilter_horizontal_outBuffer<<< blocks,1>>>(dev_inSrc,dev_mask, width_2);
}

void cudaFilter::normalGuidedGradient(int width_2){
	result_is_in_IN_buffer = false;

	dim3 blocks(sensor_desc._width, sensor_desc._height);
	abs_gradient_inBuffer<<<blocks,1>>>(dev_outSrc, dev_mask, width_2);

}

void cudaFilter::flyingPixels(int width_2)
{
	result_is_in_IN_buffer = true;
	dim3 blocks(sensor_desc._width, sensor_desc._height);
	/*
	result_is_in_IN_buffer = false;
	flyingPixels_inBuffer<<<blocks,1>>>(dev_outSrc, dev_mask, width_2);*/
	masked_min_horizontal_texin<<<blocks,1>>>(dev_outSrc, dev_inSrc0, dev_mask, width_2);
	flyingPixels_vertical_texout<<<blocks,1>>>(dev_inSrc, dev_mask, width_2);

}