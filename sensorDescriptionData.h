#pragma once

class sensorDescriptionData{
public:
	int _width, _height;
	float _unproject[16];
	float _frustum[16];

	void unproject(int i, int j, float z, float& target_x, float & target_y)
	{
		float up_x = _unproject[0] * i + _unproject[4]*j + _unproject[8]*z + _unproject[12];
		float up_y = _unproject[1] * i + _unproject[5]*j + _unproject[9]*z + _unproject[13];
		target_x = -up_x*z;///tmp.w();
		target_y = up_y*z;///tmp.w();
	}
};