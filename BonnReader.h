#pragma once

#include "CloudReader.h"
#include "Sensors.h"

class cudaTool;
class BonnReader :public CloudReader
{
	std::vector<float> depthFloatBuffer, normalFloatBuffer, buffer_f;
	RangeSensorDescription * d;
	explainMe::Cloud::Ptr currentCloud;
	std::vector<std::pair<std::string, std::string>> d_rgb_files;
	std::string datafolder;
	cudaTool * cuda;
public:
	BonnReader(const std::string & folder);
	~BonnReader();


	RangeSensorDescription * getDescription(){
		return d;
	}
	virtual size_t numberOfFrames(){
		return d_rgb_files.size();
	}

	virtual bool loadFrame(int frame, bool cleanUp, float maxDist, bool bilateralFilter, float sig_z = 0.05f);

	virtual std::vector<std::string> fileNames(int frame);

	virtual explainMe::Cloud::Ptr & getPointCloud(){
		return currentCloud;
	}
private:
	void listFiles(const std::string & associationFile);
};

class SintelReader :public CloudReader
{
	std::vector<float> depthFloatBuffer, normalFloatBuffer, buffer_f;
	RangeSensorDescription * d;
	explainMe::Cloud::Ptr currentCloud;
	std::vector<std::string> d_files;
	std::vector<std::string> rgb_files;
	std::string datafolder;
	cudaTool * cuda;
public:
	SintelReader(const std::string & folder);
	~SintelReader();


	RangeSensorDescription * getDescription(){
		return d;
	}
	virtual size_t numberOfFrames(){
		return d_files.size();
	}

	virtual bool loadFrame(int frame, bool cleanUp, float maxDist, bool bilateralFilterInput, float sig_z = 0.05);

	virtual std::vector<std::string> fileNames(int frame);

	virtual explainMe::Cloud::Ptr & getPointCloud(){
		return currentCloud;
	}
private:
	void listFiles(const std::string & associationFile);
};


class Kin2PNGReader :public CloudReader
{
	std::vector<float> depthFloatBuffer, normalFloatBuffer, buffer_f;
	RangeSensorDescription * d;
	explainMe::Cloud::Ptr currentCloud;
	std::vector<std::string> d_files;
	std::vector<std::string> ir_files;
	std::string datafolder;
	cudaTool * cuda;
public:
	Kin2PNGReader(const std::string & folder);
	~Kin2PNGReader();


	RangeSensorDescription * getDescription(){
		return d;
	}
	virtual size_t numberOfFrames(){
		return d_files.size();
	}

	virtual bool loadFrame(int frame, bool cleanUp, float maxDist, bool bilateralFilterInput, float sig_z = 0.05);

	virtual std::vector<std::string> fileNames(int frame);

	virtual explainMe::Cloud::Ptr & getPointCloud(){
		return currentCloud;
	}
private:
	void listFiles(const std::string & associationFile);
};