#pragma once



class tuple3f{
public:
	float x;
	float y;
	float z;
	tuple3f(){
		x=y=z=0;
	}
	tuple3f(float x_, float y_, float z_){
		x=x_;y=y_;z=z_;
	}
	void set(float x_, float y_, float z_){
		x=x_;y=y_;z=z_;
	}
};

class tuple3i{
public:
	int a;
	int b;
	int c;
	tuple3i(int a_, int b_, int c_){
		a=a_;b=b_;c=c_;
	}
};

class tuple2i{
public:
	int a;
	int b;
	tuple2i(int a_, int b_){
		a=a_;b=b_;
	}
};

class tuple2f{
public:
	float x;
	float y;
	tuple2f(){x=y=0;}
	tuple2f(float x_,float y_){
		x=x_;y=y_;
	}
};

struct bgr_t{
	//uint_t bgra;
	
	unsigned char b;
	unsigned char g;
	unsigned char r;
	//char a;
	
};

class pxyz_normal_frame{
public:
	float x,y,z;
	float nx,ny,nz;
	int frame, idx;
};

