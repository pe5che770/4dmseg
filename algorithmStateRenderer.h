#pragma once
#include <qobject.h>
#include <qmainwindow.h>
#include "glDisplay.h"

class algorithmStateRenderer:QObject
{
	Q_OBJECT;
private:
	//gl context.
	QMainWindow * win;
	glDisplay * gldisplay;
	
	//temporarily compute colors to color clouds with.
	std::vector<Eigen::Vector3f> colBuffer, posBuffer;

	QMatrix4x4 preferredProj; 
	int preferred_w, preferred_h;

public:
	algorithmStateRenderer(int w, int h);
	~algorithmStateRenderer();

	void clear(bool resetMatrices = true);
	void setCloudPosition(Eigen::Matrix4f & mat);
	//void setUpRenderingForLines(){ std::cout << __FILE__ << __LINE__ <<"Rendering lines, yaya, todo\n"; }
	void setUpRenderingForCloud(){ std::cout << __FILE__ << __LINE__ << "Rendering cloud, yaya, todo\n"; }
	void setUpRenderingForLines(std::vector<Eigen::Matrix4f> &alignments);
	void setUpRenderingForLines(std::vector<explainMe::Cloud::Ptr> &lineCorresps_listedByCloud);
	void setUpRenderingForLines(std::vector<explainMe::Cloud::Ptr> &lineCorresps_listedByCloud, std::vector<int> & labels_by_line,
		bool resize_to_preferred_size =true);
	void setUpRenderingForLines(std::vector<explainMe::Cloud::Ptr> &lineCorresps_listedByCloud, Eigen::VectorXf pos_neg,
		bool resize_to_preferred_size = true);
	void setUpRenderingForCloud(explainMe::Cloud::Ptr cloud, bool resizeToPreferredSize = true);
	void setUpRenderingForCloud(explainMe::Cloud::Ptr cloud, std::vector<float> & vals, float scale);
	void setUpRenderingForCloud(explainMe::Cloud::Ptr cloud, explainMe::MatrixXf_rm & w);
	void setUpRenderingForCloud_wmax(explainMe::Cloud::Ptr cloud, explainMe::MatrixXf_rm & w);
	void setUpRenderingForCloud_wsec_to_wmax(explainMe::Cloud::Ptr cloud, explainMe::MatrixXf_rm & w);
	void setUpRenderingForCloud_log_wi(explainMe::Cloud::Ptr cloud, explainMe::MatrixXf_rm & w, int w_i);
	void setCamera(QVector3D & eye, QVector3D & lookat);
	void setPreferredProjection(QMatrix4x4 & proj, int w, int h);
public Q_SLOTS:
	void recordImage(const std::string & folder, const std::string & file);
};

