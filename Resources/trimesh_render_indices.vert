#version 330

// Uniform variables
uniform mat4 projection; 
uniform mat4 modelview;

// Input
in vec3 position;

//output
out vec3 position_v;
out int vertexId;

void main()
{
	//pass everything along.
	position_v= position;
	vertexId = gl_VertexID;
}
