#version 150
// Default fragment shader
uniform mat4 projection; 
uniform mat4 modelview;

layout(points) in;
layout(line_strip, max_vertices = 2) out;

in vec4[] position2_v;
in vec4[] position_v;
in vec4[] color_v;

out vec4 frag_color;

void main()
{		

	gl_PrimitiveID = gl_PrimitiveIDIn;
	frag_color = abs(color_v[0]);
	//frag_color = vec4(0.5,0.5,0.5,0.5) + 0.5 * normalize(color_v[0]);
	/*float cos_dir = abs((modelview * normalize(position_v[0]-position2_v[0])).z);
	cos_dir = sqrt(1-cos_dir*cos_dir);
	frag_color =  cos_dir* frag_color;*/

	gl_Position = projection * modelview * position_v[0];
	EmitVertex();
	
	gl_Position = projection * modelview * position2_v[0];
	EmitVertex();
}
