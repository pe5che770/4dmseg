#version 150
// Default vertex shader

// Uniform variables, set in main program
uniform mat4 projection; 
uniform mat4 modelview;

// Input vertex attributes; passed from main program to shader 

in vec4 position;
in vec4 position2;


// Output variables are passed to the fragment shader, or, if existent to the geometry shader
// and have to be declared as in variables within the next shader.
out vec4 position_v;
out vec4 position2_v;

void main()
{
	position_v= position;
	position2_v= position2;
}
