#version 330

// Input variable, passed from vertex to fragment shader
// and interpolated automatically to each fragment
in vec4 frag_color;
flat in int frag_vertexId1,frag_vertexId2,frag_vertexId3;
in vec3 vertexIdWeight;
// Output variable, will be written to framebuffer/display automatically
//out vec4 out_color;
layout(location = 0) out int vertexId;

void main()
{		
	vertexId = (vertexIdWeight.x > vertexIdWeight.y && vertexIdWeight.x > vertexIdWeight.z ? frag_vertexId1 :
				vertexIdWeight.y > vertexIdWeight.z ? frag_vertexId2 :
				frag_vertexId3) + 1;
}
