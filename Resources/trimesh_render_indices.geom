#version 330

// Default fragment shader
uniform mat4 projection; 
uniform mat4 modelview;

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3[] position_v;
in int[] vertexId;


out vec4 frag_color;
flat out int frag_vertexId1,frag_vertexId2,frag_vertexId3;
out vec3 vertexIdWeight;

void main()
{		
	frag_vertexId1 = vertexId[0];
	frag_vertexId2 = vertexId[1];
	frag_vertexId3 = vertexId[2];;

	gl_PrimitiveID = gl_PrimitiveIDIn;
	frag_vertexId1 = vertexId[0];
	
	//colors for debugging
	frag_color = vec4((vertexId[0] % 5)/5.f , (vertexId[0] % 7)/14.f +0.5f,(vertexId[0] % 11)/11.f,1);

	// emit 3 verts of a triangle
	vertexIdWeight = vec3(1,0,0);
	gl_Position = projection * modelview * vec4(position_v[0],1);
	EmitVertex();
	
	vertexIdWeight = vec3(0,1,0);
	gl_Position = projection * modelview * vec4(position_v[1],1);
	EmitVertex();

	vertexIdWeight = vec3(0,0,1);
	gl_Position = projection * modelview * vec4(position_v[2],1);
	EmitVertex();
	
}
