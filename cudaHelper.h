#pragma once
#include <cuda.h>
#include <iostream>
#include <cstdio>

static void HandleError( cudaError_t err,
	const char *file,
	int line ) {
		if (err != cudaSuccess) {
			printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
				file, line );
			exit( EXIT_FAILURE );
		}
}
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))


static void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
