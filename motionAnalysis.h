#pragma once
#include "explainMeDefinitions.h"



class motionAnalysis
{
	class signature{
	public:
		int s_r;
		int axes_r;
		int s_t;

		signature(){ s_r = -1; s_t = -1; axes_r = -1; }
		void set(int _s_r, int _s_t)
		{
			s_r = _s_r; s_t = _s_t;
			axes_r = (s_r == 0 ? 0 :
				(s_r <=4 ?	1:
				(s_r <=8 ?	2: 
							3)));
		}
	};
private:
	Eigen::MatrixXf trajectories,M;
	float relative_eps, absolute_eps;
	std::vector<Eigen::Matrix4f> motion;
	signature s;
public:
	motionAnalysis(std::vector<Eigen::Matrix4f> _motion, float noise_sig)
	{
		relative_eps = std::max<float>(5 * noise_sig, 1e-6);
		motion = _motion;
		s= computeMotionSignature();
		coutSignature();
	}


	//to analyse rleative 
	motionAnalysis(std::vector<Eigen::Matrix4f>& motion1, 
		std::vector<Eigen::Matrix4f>& motion2, float noise_sig)
	{
		relative_eps = std::max<float>(5 * noise_sig , 1e-6);
		for (int i = 0; i < motion1.size(); i++){
			motion.push_back(motion1[i] * motion2[i].inverse());
		}
		auto sig1 = computeMotionSignature();
		std::cout << "relative Signature m1m2inf: ";
		coutSignature(sig1);

		for (int i = 0; i < motion1.size(); i++){
			motion.push_back(motion2[i] * motion1[i].inverse());
		}
		
		auto sig2 = computeMotionSignature();
		std::cout << "relative Signature m2m1inf: ";
		coutSignature(sig2);


	}
	~motionAnalysis(){}

	motionAnalysis::signature getSignature(){
		return s;
	}

	void coutSignature();
	void coutSignature(signature & s_);
private:
	signature computeMotionSignature();
};