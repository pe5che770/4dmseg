#pragma once
#ifndef Q_MOC_RUN
#include "StopWatch.h"
#endif
#include <map>
#include <vector>

class stats{
public:
	StopWatch total;
	allTimings details;
	std::vector<std::string> info;
	std::string name;

	stats(const std::string & name){ this->name = name; }
	~stats(){}

	StopWatch & operator[](const std::string & name){
		return details.getWatch(name);
	}

	void print();
};

class statsKeeper
{
public:
	int totalNumStep, currentStep;
	std::vector<float> energies;
	std::vector<int> numHyps;
	std::vector<std::vector<float>> avgRegErr;

	StopWatch grandTotal;
protected:
	stats init, interpolate, motion, segmentation, doc;
	std::vector<stats*> allStats;
	stats other;
public:
	statsKeeper();
	~statsKeeper();

	void setProgressXoutOfY(int x, int y){ currentStep = x; totalNumStep = y; }

	stats & getInitStats(){ return init; }
	stats & getInterpolateStats(){ return interpolate; }
	stats & getMotionStats(){ return motion; }
	stats & getSegmentationStats(){ return segmentation; }
	stats & getDocStats(){ return doc; }
	stats & getOtherStats(){ return other; }

	void printAll();
};

