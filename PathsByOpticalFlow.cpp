#include "PathsByOpticalFlow.h"
#include "opencv2/opencv.hpp" 
//#include "opencv2/nonfree/features2d.hpp"

PathsByOF::PathsByOF(float minOFQuality, int delta_seedFrame, float topAcceleration)
{
	this->minOFQuality = minOFQuality;
	this->delta_seedFrame = delta_seedFrame;
	this->topAcceleration = topAcceleration;
}


PathsByOF::~PathsByOF(void)
{
}




void PathsByOF::computePointsToTrack(cv::Mat & img1, std::vector<cv::Point2f> & points_img1, float minOFQuality)
{
	//could do something smarter. choose only points to track, where i have good depth data..
	points_img1.clear();
	cv::goodFeaturesToTrack(img1, points_img1,1000,minOFQuality,12, cv::noArray(),7);
}



//fill the matrix mat_ir with the values of the structured cloud
//mat_ir assumed to be already resized appropriately,
//the vector assumed to store the values by enumerating one colum after the other
void PathsByOF::fillCvMatrix(cv::Mat & mat_ir, explainMe::Cloud::Ptr cloud){
	for(int i=0; i<mat_ir.rows; ++i)
		for(int j=0; j<mat_ir.cols; ++j)
		{
			//std::cout << "i,j: " << i << "  " << j << "\n";
			auto & p = cloud->at(j,i);
			float y = 0.3 * p.r + 0.59*p.g + 0.11 * p.b;
			 mat_ir.at<uint8_t>(i, j) =y;			
		}

}

void PathsByOF::lucasKanadeSparseFlow(cv::Mat& img1, cv::Mat& img2, std::vector<cv::Point2f> & points_img1, std::vector<cv::Point2f> & corresp_img2, std::vector<uchar>& status)
{
	//std::cout << ".";

	auto & corners = points_img1;
	auto & corresps = corresp_img2;


	//optical flow with pyramid , lucas kanade
	int maxLevel = 3;
	cv::Size winSize(21,21);
	//Size winSize(30,30);
	cv::Mat error;
	calcOpticalFlowPyrLK(img1, img2,corners,corresps,status,error,winSize, maxLevel, cv::TermCriteria((cv::TermCriteria::COUNT + cv::TermCriteria::EPS), 10, 0.03));
	

	//daw the matches
	//cvDrawMatches(img1, corners, corresps, status);
}



//split pushback and filter paths.
void PathsByOF::push_back(std::vector<trajectory_R> & paths,std::vector<bool> & path_stopped, 
	std::vector<cv::Point2f> & newImgSpacePoints, 
	std::vector<uchar> & status, 
	explainMe::Cloud::Ptr cloud)
{

	for(int i = 0, sz = newImgSpacePoints.size(); i < sz; i++){
		int qi =std::floor(newImgSpacePoints[i].x + 0.5f) , qj= std::floor(newImgSpacePoints[i].y + 0.5f);
		if(qi < cloud->width && qj < cloud->height && qi > 0 && qj > 0){
			auto & pxyz = cloud->at(qi,qj);
			if( pxyz.z!=0 && pxyz.z*0 ==0 && ! path_stopped[i] && status[i] == 1 && pxyz.getNormalVector3fMap().norm() > 0.99f){
				pcl::PointXYZINormal normal;
				paths[i].push_back(normal);
				paths[i].back().getVector3fMap() = pxyz.getVector3fMap();
				paths[i].back().getNormalVector3fMap() << pxyz.getNormalVector3fMap();
			}
			else{
				path_stopped[i] = true;
			}
		}
		else{
			path_stopped[i] = true;
		}

	}
}


void PathsByOF::push_front(std::vector<trajectory_R> & paths,std::vector<bool> & path_stopped, 
			   std::vector<cv::Point2f> & newImgSpacePoints, 
			   std::vector<uchar> & status, 
			   explainMe::Cloud::Ptr cloud)
{

		
	for(int i = 0, sz = newImgSpacePoints.size(); i < sz; i++){
		int qi =std::floor(newImgSpacePoints[i].x + 0.5f) , qj= std::floor(newImgSpacePoints[i].y + 0.5f);
		if(qi < cloud->width && qj < cloud->height && qi > 0 && qj > 0){
			auto & pxyz = cloud->at(qi,qj);
			if( pxyz.z!=0 && pxyz.z*0 ==0 && ! path_stopped[i] && status[i] == 1 && pxyz.getNormalVector3fMap().norm() > 0.99f){
				pcl::PointXYZINormal normal;
				paths[i].push_front(normal);
				paths[i].front().getVector3fMap() = pxyz.getVector3fMap();
				paths[i].front().getNormalVector3fMap() << pxyz.getNormalVector3fMap();
			}
			else{
				path_stopped[i] = true;
			}
		}
		else{
			path_stopped[i] = true;
		}

	}
}

void PathsByOF::findTrajectories(
	std::vector<explainMe::Cloud::Ptr> & clouds, RangeSensorDescription * d,
	std::vector<trajectory_R> & trajectories)
{
	cv::Mat mat_ir1(d->height(), d->width(), CV_8U);
	cv::Mat mat_ir2(d->height(), d->width(), CV_8U);
	cv::Mat mat_ir0(d->height(), d->width(), CV_8U);

	
	//points to track
	std::vector<cv::Point2f> points_img1, points_img2, points_frame0;
	std::vector<uchar> status;
	std::vector<bool> path_stopped, initialSample_invalid;
		
	trajectories.clear();

	std::vector<trajectory_R> local_trajectories;

	//in each frame: find good points to track,
	//track them, forward and backward.
	
	//load next frame
	for(int frame1 = 0; frame1 < clouds.size(); frame1+=delta_seedFrame){//frame1+=5){
		std::cout << "-";
		auto c1 = clouds[frame1];
		
		fillCvMatrix(mat_ir1, c1);
		
		computePointsToTrack(mat_ir1,points_img1, minOFQuality);

		local_trajectories.clear();
		local_trajectories.resize(points_img1.size());
		initialSample_invalid.clear();
		initialSample_invalid.resize(points_img1.size(), false);

		//start Trajectories
		//init paths with starting points
		for(int i = 0, sz = points_img1.size(); i < sz; i++){
			auto & pxyz = c1->at(std::floor(points_img1[i].x + 0.5f),std::floor(points_img1[i].y + 0.5f));
			pcl::PointXYZINormal newp;
			if(pxyz.z!=0 && pxyz.z*0 ==0 && pxyz.getNormalVector3fMap().norm() > 0.99f){
				newp.getVector3fMap() = pxyz.getVector3fMap();
				newp.getNormalVector3fMap() = pxyz.getNormalVector3fMap();
				local_trajectories[i].init(newp,frame1,-1);
			}
			else{
				initialSample_invalid[i] = true;
			}
		}

		path_stopped = initialSample_invalid;
		//and track the features: forward
		for(int frame2 = frame1+1; frame2 <clouds.size() && frame2 < frame1 + 30; frame2++){
			auto c2 =  clouds[frame2];
			fillCvMatrix(mat_ir2, c2);
			//flow
			lucasKanadeSparseFlow(mat_ir1, mat_ir2, points_img1, points_img2, status);
			push_back(local_trajectories,path_stopped, points_img2, status, c2);

		}

		//and backward:
		path_stopped = initialSample_invalid;
		for(int frame2 = frame1-1; frame2 >= 0 && frame2 >= frame1 -30; frame2--){
			auto c2 =  clouds[frame2];
			fillCvMatrix(mat_ir2, c2);
			//flow
			lucasKanadeSparseFlow(mat_ir1, mat_ir2, points_img1, points_img2, status);
			push_front(local_trajectories,path_stopped, points_img2, status, c2);

		}

		trajectories.insert(trajectories.end(),local_trajectories.begin(), local_trajectories.end());
	}


	filterTrajectories(trajectories, topAcceleration);

	//remove degenerated trajectories
	auto traj_filtered = trajectories;
	trajectories.clear();
	for(int i = 0; i < traj_filtered.size(); i++){
		if(traj_filtered[i].numEl() >1){
			trajectories.push_back(traj_filtered[i]);
		}
	}
	
	std::cout << "Extracted "  << trajectories.size() << " trajectories!\n";

}
