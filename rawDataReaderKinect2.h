#pragma once
//#include <Windows.h>
#include <string>
#include "explainMeDefinitions.h"
#include "Sensors.h"
#include "CloudReader.h"

class fileReader;
class cudaTool;
class cudaFilter;


class rawDataReaderKinect2: public CloudReader
{
private:
	std::vector<explainMe::UINT16> depthBuffer;
	std::vector<explainMe::UINT16> IRBuffer;
	
	std::vector<float> depthFloatBuffer, normalFloatBuffer, buffer_f;
	std::vector<int> buffer_i;

	explainMe::Cloud::Ptr cloud;

	RangeSensorDescription * sensorDescription;
	fileReader * reader;
	cudaTool * cuda;
	cudaFilter * filter;
	int frame;

public:
	rawDataReaderKinect2(std::string folder);
	virtual ~rawDataReaderKinect2(void);

	virtual RangeSensorDescription * getDescription(){
		return sensorDescription;
	}

	virtual size_t numberOfFrames();

	virtual bool loadFrame(int frame, bool cleanUp, float maxDist, bool bilateralFilter, float sig_z = 0.05);
	void recomputeClouds();
	void cleanUpFrame(int howMuch);

	virtual explainMe::Cloud::Ptr & getPointCloud(){
		return cloud;
	}

	virtual std::vector<std::string> fileNames(int frame);

	void getRawBuffers(std::vector<explainMe::UINT16> & depth, 
		std::vector<explainMe::UINT16> & alignedIR){
			depth = depthBuffer;
			alignedIR = IRBuffer;
	}

	void getDepthInMeter(std::vector<float> & depth)
	{
		depth = depthFloatBuffer;
	}

	void restart(){
		frame = 0;
	}
};

