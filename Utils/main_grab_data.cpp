#include <array>
#include <iostream>
#include <map>
#include <vector>

#include "opencv2/opencv.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <OpenNI.h>
#include <fstream>
#include <pcl/io/pcd_io.h>


using namespace std;
using namespace openni;

void
convertToPCL (const cv::Mat &depth, const cv::Mat &color,
              pcl::PointCloud<pcl::PointXYZRGBA> &cloud)
{
  const double fx = 525.;
  const double fy = 525.;
  const double cx = 319.5;
  const double cy = 239.5;

  pcl::PointXYZRGBA p_nan;
  p_nan.x = p_nan.y = p_nan.z = std::numeric_limits<float>::quiet_NaN ();

  cloud.width = depth.cols;
  cloud.height = depth.rows;
  cloud.points.resize (cloud.width * cloud.height, p_nan);
  cloud.is_dense = false;
  for (size_t v = 0; v < depth.rows; ++v)
    for (size_t u = 0; u < depth.cols; ++u)
    {
      double z = 1e-3 * static_cast<double> (depth.at<unsigned short> (cv::Point (u, v)));

      pcl::PointXYZRGBA p;
      p.getVector3fMap () = Eigen::Vector3f (z / fx * (static_cast<float> (u) - cx),
                                             z / fy * (static_cast<float> (v) - cy),
                                             //z / fy * (static_cast<float> (cloud.height - 1) - static_cast<float> (v) - cy),
                                             z);

      // if (pcl_isfinite (p.z) && fabs (p.z) > 1e-3)
      // {
        cv::Vec3b c = color.at<cv::Vec3b> (cv::Point (u, v));
        p.r = c [2];
        p.g = c [1];
        p.b = c [0];
        p.a = 255;
      // }

      int index = v * cloud.width + u; //(cloud.height - 1 - v) * cloud.width + u;
      cloud[index] = p;
    }
}




int main( int argc, char **argv )
{
  OpenNI::initialize();

  Device  mDevice;
  mDevice.open( ANY_DEVICE );

  VideoStream mDepthStream;
  mDepthStream.create( mDevice, SENSOR_DEPTH );

  VideoMode mDepthMode;
  mDepthMode.setResolution( 640, 480 );
  mDepthMode.setFps( 30 );
  mDepthMode.setPixelFormat( PIXEL_FORMAT_DEPTH_1_MM );
  mDepthStream.setVideoMode(mDepthMode);

  VideoStream mColorStream;
  mColorStream.create( mDevice, SENSOR_COLOR );

  VideoMode mColorMode;
  mColorMode.setResolution( 640, 480 );
  mColorMode.setFps( 30 );
  mColorMode.setPixelFormat( PIXEL_FORMAT_RGB888 );
  mColorStream.setVideoMode( mColorMode);


  mDevice.setImageRegistrationMode( IMAGE_REGISTRATION_DEPTH_TO_COLOR );

  cv::namedWindow("Depth Image", CV_WINDOW_AUTOSIZE);
  cv::namedWindow( "Color Image",  CV_WINDOW_AUTOSIZE );

  mDepthStream.start();
  mColorStream.start();


  std::vector<cv::Mat> depth_images_vec, color_images_vec;

  int iMaxDepth = mDepthStream.getMaxPixelValue();
  // start
  while( true )
  {
    cv::Mat cImageBGR;
    VideoFrameRef mColorFrame;
    mColorStream.readFrame( &mColorFrame );
    const cv::Mat mImageRGB( mColorFrame.getHeight(), mColorFrame.getWidth(),
                             CV_8UC3, (void*)mColorFrame.getData() );

    // RGB ==> BGR
    cv::cvtColor( mImageRGB, cImageBGR, CV_RGB2BGR );


    openni::VideoFrameRef mDepthFrame;
    mDepthStream.readFrame (&mDepthFrame);
    const cv::Mat mImageDepth( mDepthFrame.getHeight(), mDepthFrame.getWidth(), CV_16UC1, (void*)mDepthFrame.getData() );

    cv::imshow( "Depth Image", mImageDepth );
    cv::imshow("Color Image", cImageBGR);

    depth_images_vec.push_back (mImageDepth.clone ());
    color_images_vec.push_back (cImageBGR.clone ());

    if( cv::waitKey( 1 ) == 'q' )
      break;
  }


  printf ("saving data to hdd -> %ld frames...\n", color_images_vec.size ());
  for (size_t f_i = 0; f_i < color_images_vec.size (); ++f_i)
  {
    fprintf (stderr, ".");
    char str[512];
    sprintf (str, "output/%05zu_rgb.png", f_i);
    cv::imwrite (str, color_images_vec[f_i]);

    sprintf (str, "output/%05zu_depth.png", f_i);
    cv::imwrite (str, depth_images_vec[f_i]);

    /// Additionally save as PCD files
    pcl::PointCloud<pcl::PointXYZRGBA> cloud;
    convertToPCL (depth_images_vec[f_i], color_images_vec[f_i],
                  cloud);

    sprintf (str, "output/%05zu_cloud.pcd", f_i);
    pcl::io::savePCDFileBinaryCompressed (str, cloud);
  }


  mColorStream.destroy();
  OpenNI::shutdown();

  return 0;
}
