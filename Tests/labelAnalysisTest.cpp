#include <QApplication>
#include <iostream>
#include <fstream>
//#include "../demo_global_features.h"
#include "../explainMe_algorithm_withFeatures.h"
#include "../Experiment.h"

#include <boost/program_options.hpp>
#include "../pcdReader.h"
#ifndef USE_NO_CUDA
#include "../rawDataReaderKinect2.h"
#endif
#include <tuple>

namespace po = boost::program_options;

typedef std::tuple<int,int,int,int> quadrupel;

void readGroundTruthFile(std::string filePath, std::vector<quadrupel> & groundTruth){
	std::ifstream file;
	std::string line;
	file.open(filePath);
	//std::cout << filePath<< "\n";


	//allow floats, bools and string parameters
	//float parameter names start with 'f'
	//string parameter names start with 's'
	//bool parameters start with 'b'
	//comment lines with #
	boost::char_separator<char> sep(" []>");
	std::map<std::string, std::vector<float>> val;
	std::map<std::string, std::string> string_val;
	std::map<std::string, bool> bool_val;
	
	if(file.is_open()){
		while(std::getline(file, line))
		{
			try{
				boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
				auto it = tokens.begin();
				if(it != tokens.end()){
					if (it->at(0) == '#'){
						continue; //comment, skip line
					}
				
					int a = boost::lexical_cast<int>(*it); it++;
					if(it == tokens.end()){
						std::cout << "**** invalid line " << line <<"\n";
						continue;
					}
					int b = boost::lexical_cast<int>(*it); it++;
					if(it == tokens.end()){
						std::cout << "**** invalid line " << line <<"\n";
						continue;
					}
					int from = boost::lexical_cast<int>(*it); it++;
					if(it == tokens.end()){
						std::cout << "**** invalid line " << line <<"\n";
						continue;
					}
					int to = boost::lexical_cast<int>(*it); it++;
					if(it != tokens.end()){
						std::cout << "**** invalid line " << line <<"\n";
						continue;
					}

				
					groundTruth.push_back(quadrupel(a,b,from,to));
				}
			}catch(std::exception & e){
				std::cout  << "**** invalid line " << line <<"\n";
				std::cout << e.what();
			}
		}
		file.close();
	}

}

void readGroundTruthSupportFile(std::string filePath, std::vector<hyp_range> & groundTruth){
	std::ifstream file;
	std::string line;
	file.open(filePath);
	//std::cout << filePath<< "\n";


	//allow floats, bools and string parameters
	//float parameter names start with 'f'
	//string parameter names start with 's'
	//bool parameters start with 'b'
	//comment lines with #
	boost::char_separator<char> sep(" []>");
	std::map<std::string, std::vector<float>> val;
	std::map<std::string, std::string> string_val;
	std::map<std::string, bool> bool_val;
	
	if(file.is_open()){
		while(std::getline(file, line))
		{
			try{
				boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
				auto it = tokens.begin();
				if(it != tokens.end()){
					if (it->at(0) == '#'){
						continue; //comment, skip line
					}
				
					int h = boost::lexical_cast<int>(*it); it++;
					if(it == tokens.end()){
						std::cout << "**** invalid line " << line <<"\n";
						continue;
					}
					int from = boost::lexical_cast<int>(*it); it++;
					if(it == tokens.end()){
						std::cout << "**** invalid line " << line <<"\n";
						continue;
					}
					int to = boost::lexical_cast<int>(*it); it++;
					if(it != tokens.end()){
						std::cout << "**** invalid line " << line <<"\n";
						continue;
					}

				
					hyp_range hr;
					hr.hyp = h; hr.start = from; hr.stop = to;
					groundTruth.push_back(hr);
				}
			}catch(std::exception & e){
				std::cout  << "**** invalid line " << line <<"\n";
				std::cout << e.what();
			}
		}
		file.close();
	}

}

void toMatlab(
	std::string & folder,
	std::vector<Eigen::MatrixXf>  frame_shift_mat,
	std::vector<Eigen::RowVectorXf>  frame_abs_shift,
	std::vector<Eigen::RowVectorXf>  frame_support,
	explainMe_algorithm_withFeatures::Parameters &params
	)
{
	std::ofstream of(folder + "/labelSwitch.m");

	of << "frame_shift_mat =[ ";
	for (int i = 0; i < frame_shift_mat.size(); i++){
		for (int j = 0; j < frame_shift_mat[i].rows(); j++){
			of << frame_shift_mat[i].row(j) << " ";
		}
		of << ";\n";
	}
	of << "];\n";

	of << "frame_abs_shift =[";
	for (int i = 0; i < frame_shift_mat.size(); i++){
		of << frame_abs_shift[i] << " ";
		
		of << ";\n";
	}
	of << "];\n";

	of << "frame_support =[";
	for (int i = 0; i < frame_shift_mat.size(); i++){
		of << frame_support[i] << " ";

		of << ";\n";
	}
	of << "];\n";

	int col[][3] = { { 255, 0, 0 }, { 0, 255, 0 }, { 0, 0, 255 }, { 20, 20, 20 }, { 255, 255, 0 }, { 255, 0, 255 }, { 0, 255, 255 }, { 200, 200, 200 }, { 20, 120, 220 }, { 20, 220, 120 }, { 120, 220, 20 }, { 120, 20, 220 }, { 220, 20, 120 }, { 220, 120, 20 } };
	std::string colors = "colors = {";
	for (int i = 0; i < frame_support[0].cols(); i++){
		colors += "[" + std::to_string(col[(i) % 13][0] / 255.0) + "," + std::to_string(col[(i) % 14][1] / 255.0) + "," + std::to_string(col[(i) % 14][2] / 255.0) + "]";
			if (i != frame_support[0].cols() - 1){
				colors += ",";
			}
	}
	colors += "}";
		
	of << "mx = max(max(frame_shift_mat));\n"
		"localMaxes = [];\n"
		"numHyp = size(frame_abs_shift,2);\n"
		"localMaxInd = [];\n"
		"localMaxI = [];\n"
		"localMaxJ = [];\n"
		"frame_shift_mat_filtered = frame_shift_mat;\n"
		"k=" << std::to_string(params.k_generated_hyps) << ";\n"
		"for j = 0:(numHyp-1)\n"
		"	windowSize = 4;\n"
		"	a=1;\n"
		"	b = (1/windowSize)*ones(1,windowSize);\n"
		"	for i = 1:numHyp\n"
		"		x = frame_shift_mat(:,i + j*4);\n"
		"		y = filter(b,a,[x; zeros(windowSize/2,1)]); \n"
		"		y = y(windowSize/2+1:end);\n"
		"		isLocalMax = [false;(y(2:end-1) > y(1:end-2))&(y(2:end-1) > y(3:end)); false];\n"
		"		frame_shift_mat_filtered(:,i + j*4) = y;\n"
		"		localMaxes = [localMaxes; frame_shift_mat_filtered(isLocalMax,i + j*4)/mx];\n"
		"		localMaxInd = [localMaxInd; find(isLocalMax)];\n"
		"		localMaxI = [localMaxI; ones(size(find(isLocalMax)))*(i-1)];\n"
		"		localMaxJ = [localMaxJ; ones(size(find(isLocalMax)))*j];\n"
		"	end\n"
		"end\n"
		""
		"[val, ind] = sort(localMaxes, 'descend')\n"
		"val = val(1:k); ind = ind(1:k); maxis = localMaxI(ind); maxjs = localMaxJ (ind); maxInd=localMaxInd(ind);\n"
		""
		"for j = 0:(numHyp-1)\n"
		"	hFig=figure;\n"
		"	set(hFig, 'Position', [100 100+j*230 1200 200])\n"
		//"	color = {'r', 'g', 'b', 'k', 'y'};\n"
		<< colors << "\n"
		"	hold on\n"
		""
		"	for i = 1:numHyp\n"
		"		plot(0:(size(frame_shift_mat,1)-1),frame_shift_mat(:,i + j*4)/mx, 'color', colors{i}, 'LineStyle' , ':');\n"
		"		y = frame_shift_mat_filtered(:,i + j*4) ;\n"
		"		isLocalMax = [false;(y(2:end-1) > y(1:end-2))&(y(2:end-1) > y(3:end)); false];\n"
		"		plot(0:(size(frame_shift_mat_filtered,1)-1),frame_shift_mat_filtered(:,i + j*4)/mx, 'color', colors{i});\n"
		"		plot(find(isLocalMax)-1,frame_shift_mat_filtered(isLocalMax,i + j*4)/mx,'r*')\n"
		"		plot(maxInd(find(maxis==(i-1) & maxjs==j)) -1, val(find(maxis==(i-1) & maxjs==j)), 'o', 'lineWidth', 8)\n"
		"	end\n"
		"	title(['Shift for: \\color[rgb]{' num2str(colors{j+1}) '}######'])\n"
		"	axis([0 (size(frame_shift_mat,1)-1) 0 1])\n"
		"end";
	
	of.close();
}

void runIt(int dataType, std::string & folder, std::string & seg, 
		   std::string & gtFile, std::string & gtRangeFile,
		   explainMe_algorithm_withFeatures::Parameters &params,
		   std::string outFolder)
{
	
	CloudReader::Ptr reader;

	if(dataType == 1){
		reader = CloudReader::Ptr(new pcdReader(folder));
	}
	else if(dataType == 0){
#ifndef USE_NO_CUDA
		reader = CloudReader::Ptr(new rawDataReaderKinect2(folder));
#else
		std::cout << "Error! Kinect data can only be read when cuda is enabled";
		return;
#endif // !USE_NO_CUDA
	}


	explainMe_algorithm_withFeatures::Ptr alg( new explainMe_algorithm_withFeatures(reader, params));
	FromFileInitializer::Ptr init(new FromFileInitializer(seg));

	
	alg->step_1_and_2(init);
	alg->step3_updateDenseSeg_from_sparse();
	//alg.step4_computeMotionHypothesesFromDenseSegmentation(false);
	//do only analysing substep of step 4
	std::vector<std::vector<int>> res_frame_from_to;
	std::vector<hyp_range> res_supportRanges;

	float lambda_outlier = 0.5;//1;//2.5;
	int k = std::max<int>(10,9 * alg->allClouds.size() * 1.0/60);
	//for(lambda_outlier = 0.5; lambda_outlier < 1.4; lambda_outlier += 0.1)
	//for(int k  = 5; k < 11; k++)
	{
		
		std::vector<Eigen::MatrixXf>  frame_shift_mat, frame_shift_mat_fwd;
		std::vector<Eigen::RowVectorXf>  frame_abs_shift;
		std::vector<Eigen::RowVectorXf>  frame_support;

		//std::cout << "Collecting dwdt statistics...\n";
		//compute matrices for documentation purposes.
		if (outFolder.length() > 0){
			alg->motions.computeDwDt_etc(
					//in
					alg->allClouds,
					alg->denseSegmentation_allClouds,
					alg->allClouds_kdTree,
					//out
					frame_shift_mat,
					frame_shift_mat_fwd,
					frame_abs_shift);
			alg->motions.computeSupport(alg->denseSegmentation_allClouds, frame_support);
			toMatlab(outFolder, frame_shift_mat, frame_abs_shift, frame_support, params);
		}

		//do the real thing.
		alg->motions.step2_analyseDenseSegmentation(k, lambda_outlier,
			alg->allClouds, alg->denseSegmentation_allClouds,
			alg->allClouds_kdTree,
			res_frame_from_to,
			res_supportRanges, true, true);

		
		struct sortByFirst{
			bool operator()(const std::vector<int> & first, const std::vector<int> & second) const{
				return first[0] < second[0];
			}
		};


		/*std::cout << "Extracted combos : \n";
		std::sort(res_frame_from_to.begin(),res_frame_from_to.end(), sortByFirst());
		for(int i = 0; i < res_frame_from_to.size(); i++){
			std::cout << "\t" << res_frame_from_to[i][0] << "/" <<res_frame_from_to[i][0]  - alg->central_cloud << ":" <<
				res_frame_from_to[i][1] << "->" <<
				res_frame_from_to[i][2] << "\n";

		}//*/

	
		if(gtFile.size() > 0){
			std::vector<quadrupel> groundTruth;
			std::vector<quadrupel> missed;
			readGroundTruthFile(gtFile, groundTruth);

			//recall
			for(int i = 0; i < groundTruth.size(); i++){
				bool found = false;
				for(int j = 0; j < res_frame_from_to.size(); j++){
					if(res_frame_from_to[j][0] >= std::get<0>(groundTruth[i]) &&
						res_frame_from_to[j][0] <= std::get<1>(groundTruth[i]) &&
						res_frame_from_to[j][1] == std::get<2>(groundTruth[i]) &&
						res_frame_from_to[j][2] == std::get<3>(groundTruth[i]) ){

							//success
							found = true;
							break;
					}
				}
				if(!found){
					missed.push_back(groundTruth[i]);
				}
			}

			float recall = (groundTruth.size() - missed.size())* 1.0/groundTruth.size();

			//precision
			float precision = (groundTruth.size() - missed.size())* 1.0/ res_frame_from_to.size() ;

			std::cout << "Extracted : " << res_frame_from_to.size() << "Elements. ";
			std::cout << "Groundtruth : " << groundTruth.size() << "Elements. ";
			std::cout <<  k << " : " << lambda_outlier <<"\tRecall \t" << recall << "\t"// << "\n";
				<< "Precision \t" << precision <<"\n";
			std::cout << "Missed:\n";
			for(int i = 0; i < missed.size(); i++){
				std::cout << "[" << std::get<0>(missed[i]) << " "
					<< std::get<1>(missed[i]) << "] "
					<<  std::get<2>(missed[i]) << "->"
					<< std::get<3>(missed[i]) 
					<< "\n";
			}//*/
		}

		if(gtRangeFile.size() > 0){
			std::vector<hyp_range> groundTruth;
			std::vector<hyp_range> missed;
			readGroundTruthSupportFile(gtRangeFile, groundTruth);

			//recall
			for(int i = 0; i < groundTruth.size(); i++){
				bool found = false;
				auto & gt = groundTruth[i];
				for(int j = 0; j < res_supportRanges.size(); j++){
					auto & r = res_supportRanges[j];
					if(r.hyp == gt.hyp && std::abs(r.start - gt.start) < 5 &&
						std::abs(r.stop - gt.stop) < 5){

							//success
							found = true;
							break;
					}
				}
				if(!found){
					missed.push_back(groundTruth[i]);
				}
			}

			float recall = (groundTruth.size() - missed.size())* 1.0/groundTruth.size();

			//precision
			float precision = (groundTruth.size() - missed.size())* 1.0/ res_supportRanges.size() ;

			std::cout << "Support: Extracted : " << res_supportRanges.size() << "Elements. ";
			std::cout << "Groundtruth : " << groundTruth.size() << "Elements. ";
			std::cout <<  k << " : " << lambda_outlier <<"\tRecall \t" << recall << "\t"// << "\n";
				<< "Precision \t" << precision <<"\n";
			std::cout << "Missed:\n";
			for(int i = 0; i < missed.size(); i++){
				std::cout << missed[i].hyp << "[" << missed[i].start << " "
					<< missed[i].stop << "] "
					<< "\n";
			}
			std::cout << "Found:\n";
			for(int i = 0; i < res_supportRanges.size(); i++){
				std::cout << res_supportRanges[i].hyp << "[" << res_supportRanges[i].start << " "
					<< res_supportRanges[i].stop << "] "
					<< "\n";
			}//*///*/
		}

	}
	//demo_global_features d(demo_global_features::ALGORITHM, alg);
	//d.run();
}


int main (int argc, char ** arg)
{

	
	std::string config_file = "";
	std::string dataFolder, segFile, gtFile, gtRangeFile, outFolder; int dataType;
	explainMe_algorithm_withFeatures::Parameters params;

	po::options_description generic("Options");
	generic.add_options()
		("help", "display Help")
		("config,c", po::value<std::string>()->default_value(""), "Path to a configuration .cfg file")
	;

	po::options_description config("Configuration");
	config.add_options()
		("seg,S", po::value<std::string>(&segFile)->required(), "specify the sparse seg file")
		("ground-truth-shifts,GT", po::value<std::string>(&gtFile), "specify the groundTruthShift file")
		("ground-truth-ranges,GTR", po::value<std::string>(&gtRangeFile), "specify the groundTruth range file")
		("outfolder,O", po::value<std::string>(&outFolder)->default_value(""), "specify the groundTruth range file")
		("data.folder,D" , po::value<std::string>(&dataFolder)->required() , "specify the folder the 3d data lies")
		("data.type" , po::value<int>(&dataType)->required(), "specify if its kinect or pcl data")
		//("data.cf", po::value<int>(& params.central_frame)->required(), "which frame defines the reference frame")
		("data.d", po::value<int>(& params.delta_frame)->required(), "take each dth frame")
		("data.firstFrame",po::value<int>(& params.first_frame)->required(), "... for a total of fw frames after cf")
		("data.lastFrame",po::value<int>(& params.last_frame)->required(), "... for a total of bw frames before cf")
	;


	po::options_description cmdline_options;
	cmdline_options.add(generic).add(config);

	po::options_description config_file_options;
	config_file_options.add(config);

	po::variables_map vm;
	po::store(po::parse_command_line(argc, arg, cmdline_options), vm);
	if(vm.count("help")){
		std::cout << cmdline_options <<  "\n";
		return 0;
	}

	
	try{
		config_file = vm["config"].as<std::string>();
		std::cout << "config file: " << config_file << "\n";
		if(config_file.size() > 0){
			std::ifstream ifs(config_file.c_str());
			if (!ifs)
			{
			   std::cout << "can not open config file: " << config_file << "\n";
				return 0;
			}
			else
			{
				std::cout << "Parsing config file...";
				po::store(po::parse_config_file(ifs, config_file_options), vm);
				po::notify(vm);
			}
		}

		po::notify(vm);
	}
	catch(std::exception & e){
		std::cout << e.what() << "\n";
		std::cout << "Usage:\n" << cmdline_options << "\n";
		return 0;
	}


	runIt(dataType, dataFolder, segFile, gtFile, gtRangeFile, params, outFolder);
	return 0;
}