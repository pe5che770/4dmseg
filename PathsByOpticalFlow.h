#pragma once
#include "Initializer.h"
#include "opencv2/opencv.hpp" 

class PathsByOF:
	public InitializeTrajectories
{
private:
	float minOFQuality; int delta_seedFrame; float topAcceleration;

	//int zeroFrameCloud;//kill this.
	
public:
	//of quality thresholds badquality of trajectories away, 
	//delta_seedFrame controls for what k each kth frame is used to seed of trajs
	//top acceleration is used to filter out non-smooth, raggy trajectories, when back projected in 3d
	
	PathsByOF(
		float minOFQuality, int delta_seedFrame, float topAcceleration
		//int zeroFrameCloud //kill this param!
		);
	virtual ~PathsByOF(void);


	void findTrajectories(
		std::vector<explainMe::Cloud::Ptr> & clouds_as_grids, 
		RangeSensorDescription * description,
		std::vector<trajectory_R> & target_traj);

private:
	void push_front(std::vector<trajectory_R> & paths,std::vector<bool> & path_stopped, 
			   std::vector<cv::Point2f> & newImgSpacePoints, 
			   std::vector<uchar> & status, 
			   explainMe::Cloud::Ptr cloud);
	void push_back(std::vector<trajectory_R> & paths,std::vector<bool> & path_stopped, 
			   std::vector<cv::Point2f> & newImgSpacePoints, 
			   std::vector<uchar> & status, 
			   explainMe::Cloud::Ptr cloud);
	void lucasKanadeSparseFlow(
		cv::Mat& img1, 
		cv::Mat& img2, 
		std::vector<cv::Point2f> & points_img1, 
		std::vector<cv::Point2f> & corresp_img2, 
		std::vector<uchar>& status);
	void computePointsToTrack(cv::Mat & img1, std::vector<cv::Point2f> & points_img1, float minOFQuality);
	void fillCvMatrix(cv::Mat & mat_ir, explainMe::Cloud::Ptr cloud);
};


