/**
* A lightweight, fast, kdtree implementation based on the stl library.
* It allows fast nn and knn search, and also allows threadsafe 
* nn/knn querrying- which can additionally be sped up by using
* The nnFinder class- This will guarantee that no memory is allocated during
* the querry calls, leading to a nice speed up when more than one querry is made.
*
* While nnFinder itself is not thread safe,
* one can create one nnFinder per thread based on the same kdtree.
*
* (c) Alf 2015
*/

#pragma once
#include <vector>
#include <algorithm>
#include <stack>
#include <iostream>
#include <cmath>

class pxyz{
public:
	union{
		struct{float x, y,z;};
		float data[3];
	};
	int index;

	pxyz(){}
	pxyz(float x_,float y_, float z_, int idx = -1/*invalid index*/){
		x= x_; y= y_; z=z_;
		index = idx;
	}
	~pxyz(){}

	const float & operator[] (int idx) const {
		return data[idx];
	}

	void set(float x_, float y_, float z_, float r_, float g_, float b_){
		x = x_; y = y_; z = z_;
	}

	bool operator <(const pxyz & y) const{
		return y.x < x;
	}
};


class sortBy
{
	int dim;
public:
	sortBy(){dim = 0;}
	~sortBy(){}

	void setDim(int what){
		dim = what;
	}

	 bool operator() (const pxyz& x, const pxyz & y) const {return x[dim]<y[dim];}

};

class a_b_level
{
public:
	int a,b,level;
	a_b_level(){
		a= b =level = 0;
	}
	a_b_level(int a_, int b_, int level_){
		a= a_;b=b_;level = level_;
	}
};

class a_b_level_dad
{
public:
	int a,b,level,dad;
	a_b_level_dad(){
		a= b =level = dad = 0;
	}
	a_b_level_dad(int a_, int b_, int level_, int dad_){
		a= a_;b=b_;level = level_;dad = dad_;
	}
};


class distSqr_idx
{
public:
	float dist_sqr; int index;
	distSqr_idx(){index = -1; dist_sqr = 1e10;}
	distSqr_idx(float dist_sqr_, int index_){index = index_; dist_sqr = dist_sqr_;}
	~distSqr_idx(){}

	void set(float dist_sqr_, int index_){index = index_; dist_sqr = dist_sqr_;}

	bool operator <(const distSqr_idx & o) const{
		return dist_sqr < o.dist_sqr;
	}
};

class mySimpleKdTree
{
public:
	std::vector<pxyz> data;
	mySimpleKdTree(void){}
	~mySimpleKdTree(void){}



	void setData(std::vector<pxyz> & data){
		this->data = data;
		//buildTree();
	}

	template<typename pointT>
	void setDataT(std::vector<pointT> & data){
		this->data.resize(data.size());
		for(int i = 0; i < data.size(); i++){
			this->data[i] = pxyz(data[i].x,data[i].y, data[i].z);
			this->data[i].index = i;
		}
	}

	template <typename pcl_cloudPtr>
	void setData(pcl_cloudPtr cloud){
		data.resize(cloud->size());
		for(int i = 0; i < cloud->size(); i++){
			auto & p = cloud->at(i);
			data[i].x = p.x;
			data[i].y = p.y;
			data[i].z = p.z;
			data[i].index = i;
		}
	}

	template <typename pcl_cloudPtr, typename t_MatrixXf>
	void setData(pcl_cloudPtr cloud, t_MatrixXf seg, int label){
		data.reserve(cloud->size());
		for (int i = 0; i < cloud->size(); i++){
			auto & p = cloud->at(i);
			if (seg(i, label) > 0.5){
				data.push_back(pxyz(p.x, p.y, p.z, i));
			}
		}
	}
	
	void buildTree(){

		int sz = data.size();
		if(sz == 0){
			return;
		}
		sortBy comp;

		std::vector<a_b_level> stack;
		stack.push_back(a_b_level(0,sz,0));
		a_b_level curr;
		int mid;

		auto beg = data.begin();

		while(stack.size() != 0){
			curr = stack.back();
			stack.pop_back();

			mid =curr.a+(curr.b-curr.a)/2;
			
			//std::cout << "(" << curr.a << " " << mid << " " << curr.b << ")\n";

			comp.setDim(curr.level % 3);
			std::nth_element(beg+curr.a,beg + mid, beg+curr.b, comp);
			//std::nth_element(beg+curr.a,beg + mid, beg+curr.b);
			if(curr.a < mid -1){ //else the range [a, mid) is sorted anyway
				stack.push_back(a_b_level(curr.a, mid, curr.level +1));
			}
			if(curr.b > mid+1){
				stack.push_back(a_b_level(mid +1, curr.b, curr.level +1));
			}
		}
	}

	//return the next neighbor. Note it is more efficient, when doing many querries, to use th
	//separate finder class for NN search.
	pxyz findNN(pxyz &p, int maxTests = 2147483647){
		std::vector<a_b_level_dad> stack;
		int sz = data.size();

				
		//init stack
		stack.push_back(a_b_level_dad(0,sz,0,-1));
		
		a_b_level_dad curr;
		int mid, axis;
		pxyz result = data[sz/2], current, dad;	
		float dist_sqr = (result.x - p.x)*(result.x - p.x) + (result.y - p.y)*(result.y - p.y) + (result.z - p.z)*(result.z - p.z) ;
		float temp_dist_sqr;

		while(stack.size() != 0 && maxTests > 0){
			curr = stack.back();
			stack.pop_back();
			mid =curr.a+(curr.b-curr.a)/2;
			current = data[mid];

			//DECIDE IF THIS BRANCH CAN BE SKIPPED.
			if(curr.dad >= 0){
				dad = data[curr.dad];
				//if this is the second child visited, i.e. p and current lie on different sides.
				//and dads split axis is too far away
				axis = (curr.level -1)%3;
				if((dad[axis] - current[axis])*(dad[axis] - p[axis]) <0 
					&&
					dist_sqr < (dad[axis] - p[axis])*(dad[axis] - p[axis]))
				{
					//skip this branch
					continue;
				}
			}

			//if not skipped: check current node
			temp_dist_sqr = (current.x - p.x)*(current.x - p.x) + (current.y - p.y)*(current.y - p.y) + (current.z - p.z)*(current.z - p.z) ;
			//adapt number of remaining tests
			maxTests--;

			//temp_dist_sqr = pow(current.x - p.x,2) + pow(current.y - p.y,2)+ pow(current.z - p.z,2);
			if(temp_dist_sqr < dist_sqr)
			{
				dist_sqr = temp_dist_sqr;
				result = current;
			}

			//and push childs, in the correct order.
			axis = curr.level % 3;
			if(current[axis] > p[axis]){
				//visit right child second.
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
				//visit left child first
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}
				
			}
			else{
				//visit left second.
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}
				//visit right first
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
			}
			
		}
		return result;
	}

	/*void testTree(){
		std::vector<a_b_level> stack;
		int sz = data.size();
		stack.push_back(a_b_level(0,sz,0));
		a_b_level curr;
		int mid;

		auto beg = data.begin();

		while(stack.size() != 0){
			curr = stack.back();
			stack.pop_back();
			mid =curr.a+(curr.b-curr.a)/2;

			if(curr.a <= mid ){
				int axis = curr.level % 3;

				for(int children = curr.a; children < mid; children++){
					if(!(data[children][axis] <= data[mid][axis])){
						std::cout << "Tree malformed " << mid <<">"<< children <<"\n";
					}
				}
			}
			if(curr.b >= mid){
				int axis = curr.level % 3;
				for(int children = mid; children < curr.b; children++){
					if(! (data[children][axis] >= data[mid][axis] ))
					{
						std::cout << "Tree malformed" << mid <<"<"<< children <<"\n";
					}
				}
			}

			if(curr.a < mid){
				stack.push_back(a_b_level(curr.a, mid, curr.level +1));
			}
			if(curr.b > mid+1){
				stack.push_back(a_b_level(mid +1, curr.b, curr.level +1));
			}
		}
	}*/

};


//do NN search, reusing all memory in each search. For many querries, this is up to 10 times faster.
//For multithreading: create only one kdtree per dataset but one finder per thread
class simpleNNFinder
{
	std::vector<pxyz> & data;
	std::vector<a_b_level_dad> stack;
	a_b_level_dad curr;
	int mid, axis, sz;
	float dist_sqr;
	float temp_dist_sqr;

	pxyz current, dad;	
	distSqr_idx result;
public:
	simpleNNFinder(mySimpleKdTree & tree):
		data(tree.data)
	{
		stack.reserve(128);
		sz = data.size();
	}

	~simpleNNFinder(){}
		

	distSqr_idx findNN(pxyz &p, float maxR_sqr = 1e20, int maxTests = 2147483647){
		//init stack
		stack.push_back(a_b_level_dad(0,sz,0,-1));


		//init result
		result.index = -1;//current.index;
		dist_sqr = result.dist_sqr = maxR_sqr;//(current.x - p.x)*(current.x - p.x) + (current.y - p.y)*(current.y - p.y) + (current.z - p.z)*(current.z - p.z) ;

		if(sz == 0){
			//std::cout << "No nn";
			return result;
		}
		current = data[sz/2];	
		while(stack.size() != 0 && maxTests > 0){
			curr = stack.back();
			stack.pop_back();
			mid =curr.a+(curr.b-curr.a)/2;
			current = data[mid];

			//DECIDE IF THIS BRANCH CAN BE SKIPPED.
			if(curr.dad >= 0){
				dad = data[curr.dad];
				//if this is the second child visited, i.e. p and current lie on different sides.
				//and dads split axis is too far away
				axis = (curr.level -1)%3;
				if((dad[axis] - current[axis])*(dad[axis] - p[axis]) <0 
					&&
					dist_sqr < (dad[axis] - p[axis])*(dad[axis] - p[axis]))
				{
					//skip this branch
					continue;
				}
			}

			//if not skipped: check current node
			temp_dist_sqr = (current.x - p.x)*(current.x - p.x) + (current.y - p.y)*(current.y - p.y) + (current.z - p.z)*(current.z - p.z) ;
			maxTests--;

			//temp_dist_sqr = pow(current.x - p.x,2) + pow(current.y - p.y,2)+ pow(current.z - p.z,2);
			if(temp_dist_sqr < dist_sqr)
			{
				dist_sqr = temp_dist_sqr;
				result.set(dist_sqr, current.index);
			}

			//and push childs, in the correct order.
			axis = curr.level % 3;
			if(current[axis] > p[axis]){
				//visit right child second.
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
				//visit left child first
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}

			}
			else{
				//visit left second.
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}
				//visit right first
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
			}

		}
		return result;
	}

	//find knn
	void findkNN(pxyz &p, int k, std::vector<distSqr_idx> & dist_sqrs_idx, int maxTests = 2147483647){
		dist_sqrs_idx.clear();

		if(sz == 0){
			return;
		}
		//init stack
		stack.push_back(a_b_level_dad(0,sz,0,-1));

		//init result
		//result = data[sz/2];	
		//dist_sqr = (result.x - p.x)*(result.x - p.x) + (result.y - p.y)*(result.y - p.y) + (result.z - p.z)*(result.z - p.z) ;

		while(stack.size() != 0 && maxTests > 0){
			curr = stack.back();
			stack.pop_back();
			mid =curr.a+(curr.b-curr.a)/2;
			current = data[mid];


			//DECIDE IF THIS BRANCH CAN BE SKIPPED. Skip only when k elements have been found..
			if(curr.dad >= 0 && dist_sqrs_idx.size() == k){
				dad = data[curr.dad];
				//if this is the second child visited, i.e. p and current lie on different sides.
				//and dads split axis is too far away
				axis = (curr.level -1)%3;
				if((dad[axis] - current[axis])*(dad[axis] - p[axis]) <0 
					&&
					dist_sqr < (dad[axis] - p[axis])*(dad[axis] - p[axis]))
				{
					//skip this branch
					continue;
				}
			}


			//if not skipped: check current node
			temp_dist_sqr = (current.x - p.x)*(current.x - p.x) + (current.y - p.y)*(current.y - p.y) + (current.z - p.z)*(current.z - p.z) ;
			maxTests--;

			//not k guys found? add to result.
			if(dist_sqrs_idx.size() < k){
				dist_sqrs_idx.push_back(distSqr_idx(temp_dist_sqr, current.index));
				push_heap(dist_sqrs_idx.begin(), dist_sqrs_idx.end());
				dist_sqr = dist_sqrs_idx[0].dist_sqr; //max distance
			}
			else if(temp_dist_sqr < dist_sqr)
			{
				pop_heap(dist_sqrs_idx.begin(), dist_sqrs_idx.end());
				dist_sqrs_idx.back().set(temp_dist_sqr, current.index);
				push_heap(dist_sqrs_idx.begin(), dist_sqrs_idx.end());
				dist_sqr = dist_sqrs_idx[0].dist_sqr; //max distance
			}

			//and push childs, in the correct order.
			axis = curr.level % 3;
			if(current[axis] > p[axis]){
				//visit right child second.
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
				//visit left child first
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}

			}
			else{
				//visit left second.
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}
				//visit right first
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
			}

		}
		//		return result;
	}

	//find knn
	void radiusSearch(pxyz &p, float r_squared, std::vector<distSqr_idx> & dist_sqrs_idx, int maxTests = 2147483647){
		dist_sqrs_idx.clear();

		if(sz == 0){
			return;
		}
		//init stack
		stack.push_back(a_b_level_dad(0,sz,0,-1));

		while(stack.size() != 0&& maxTests > 0){
			curr = stack.back();
			stack.pop_back();
			mid =curr.a+(curr.b-curr.a)/2;
			current = data[mid];


			//DECIDE IF THIS BRANCH CAN BE SKIPPED. 
			if(curr.dad >= 0 ){
				dad = data[curr.dad];
				//if this is the second child visited, i.e. p and current lie on different sides.
				//and dads split axis is too far away
				axis = (curr.level -1)%3;
				if((dad[axis] - current[axis])*(dad[axis] - p[axis]) <0 
					&&
					r_squared < (dad[axis] - p[axis])*(dad[axis] - p[axis]))
				{
					//skip this branch
					continue;
				}
			}


			//if not skipped: check current node
			temp_dist_sqr = (current.x - p.x)*(current.x - p.x) + (current.y - p.y)*(current.y - p.y) + (current.z - p.z)*(current.z - p.z) ;
			maxTests--;

			if(temp_dist_sqr < r_squared)
			{
				dist_sqrs_idx.push_back(distSqr_idx(temp_dist_sqr, current.index));
				//dist_sqrs_idx.back().set(temp_dist_sqr, current.index);
				
				//dist_sqr = dist_sqrs_idx[0].dist_sqr; //max distance
			}

			//and push childs, in the correct order.
			axis = curr.level % 3;
			if(current[axis] > p[axis]){
				//visit right child second.
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
				//visit left child first
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}

			}
			else{
				//visit left second.
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}
				//visit right first
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
			}

		}
		//		return result;
	}
};

