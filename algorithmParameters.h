#pragma once
#include "MotionHypotheses.h"
#include "CloudReader.h"


class experimentParameters
{
public:
	//enum
	const static int kinect2_ir_data = 0;
	const static int pcd_data = 1;
	const static int bonn_data = 2;
	const static int ppm_jpg_data = 3;
	const static int sintel_data = 10;
	const static int kinect2_ir_png_data = 20;
	const static int unified_reader = 100;

	enum documentationLevel { LVL_0_ONLYFINALRESULT = 0, LVL_1_POST_RM = 1, LVL_2_POST_RM_MO_crfW = 2, LVL_3_POST_RM_ENERGY_DWDT = 3, LVL_4_FULL = 4 };
	enum segMode {SEGMENTATION_GC, SEGMENTATION_CRF};
	//input
	std::string dataFolder;
	int dataType;

	//documentation
	std::string gitTag;
	documentationLevel docLevel;
	segMode segmentationMode;
	std::string ground_truth_motion_file;
	
	//pipeline parameters.
	bool skipInit;
	bool loadState;
	std::string stateFile;
	std::string motionStateFile;
	int numIt_init;
	int numIt_EM;

	bool useGpuDataTerm;

	experimentParameters(){
		//storeResults = true;
		//show_results = false;
		skipInit = false;
		loadState = false;
		//storeResultClouds = false;
		stateFile = "";
		docLevel = LVL_0_ONLYFINALRESULT;
		useGpuDataTerm = true;
		segmentationMode = SEGMENTATION_CRF; //SEGMENTATION_GC;// 
	}
	~experimentParameters(){

	}

	CloudReader::Ptr createReader();
	
};

class algorithmParameters
{
public:
	//number of hypotheses in the pool
	int numHypo;

	//frame selection: will choose each delta_frame'th frame
	// beginning at central_frame. I.e. c_frame, c_frame + delta,... c_frame + max_fwd*delta
	//int central_frame, max_pathlength_fwd, max_pathlength_bwd;
	//frames f = first_frame + k*delta_frame with first_frame <= f<=last_frame
	int first_frame, last_frame;
	int  delta_frame;
	//ignore data points further away than this,
	float maxDistance;

	//optical flow parameter
	float minOFQuality; //the lower the quality, the more (and low quality) features will be tracked at init.
	float topTrajAcceleration; //For filtering improbable trajectories, measured in meters per deltaframe square.
	int delta_seedFrame; //start optical flow trajectory in every delta_seedth frame.

	//optimization energy parameters
	//use pow(w,p) for transformation weights, p2: for energy <w.^p2,delta(x)>
	float p;
	float p2;
	//gradient descent scale factor.
	float stepSize;
	float alpha_smootheness_w; //how much to weight temporal smoothness term should be in interval (0,1)

	//main iteration parameters
	float occlusion_basepenalty; //tweeking parameter, will hopefully become superfluous. Higher will penalize occlusion more,

	float clamp_distSqr;//0.001f; //clamping for the robust distance metric //0.01f; was good for asus// = 0.001f; was ggod for kinect
//	float stdDev_z_sqr; //0.001f; //used to model occlusion, an estimate of the noise level in z direction.
	float sig_windowSize_sqr;//100; //used to weight the errors down for frames far away from the seed frame. 
	bool model_occlusion;//true; //explicitely model occlusion
	bool weight_error_via_windowSize; //true; //weight errors down for frames far away from the seed frame uses sig_windowSize.

	float alpha_point2plane, alpha_point2point; //mixed point to plane and point to point energy.
	float icp_culling_dist;
	int maxItICP;

	MotionHypotheses::RegistrationType icp_type;

	//graphcut parameters
	int samplesPerFrame; //#samples used in each frame
	bool samplePerLabel; //#distribute the same number of samples on each label.
	int k_nn; //#nbrs to use to build the graphcut graph
	//int k_nn_temporal;
	float sigma_dist; //controlls the falloff of edgeweights
	float lambda_dataTerm; //weight the dataterm against the smoothness (terms)
	float lambda_smoothness_spatial; //weight in-frame spatial smoothness constraint
	float lambda_smoothness_temporal; //weight interframe smoothness constraint
	bool gc_use_normals; //use the angle between normals for the smoothness term in gc.

	float per_hyp_cost_factor;
	
	//parameters describing the crf annealing procedure.
	int maxItCRF;
	float lambda_crf_s;
	float max_annealing_factor_crf;
	float crf_sigma_sp_factor; //for crf filtering, deduced parameters are used. this factor optionally allows finer control, using factor * the automatic spatial filter ranges.
	float crf_sigma_t_factor;
	bool crf_min_by_merge;

	int k_generated_hyps; //when generating hypotheses, do this for the top k label shifts.
	float lambda_generate_hyp; //the label shifts have to be lambda std dev larger than the average per frame shift.
	float delete_labels_numframes_threshold; //delete labels if they are not supported by thisnumber *points_per_frame * 0.5% points.
	float removeThreshold; //is derived from other parameters.

	//clean up clouds?
	bool cleanUpClouds;
	//during the iteration: merge similar motions (as in post processing
	bool mergeMotions;

	//ways to generate Hypotheses
	struct gen{
		bool byLabelShifts;
		bool byCarryOvers;
		bool byConnectedComponents;
		bool byOutliers;
		gen(){
			byLabelShifts = true;
			byCarryOvers = true;
			byConnectedComponents = false;
			byOutliers = true;
		}
	} generateHypotheses;


	//some standard parameters. Eliminating them when everything works...
	algorithmParameters(){
		numHypo = 3;
		minOFQuality = 1e-3;
		topTrajAcceleration = 0.04f;
		delta_seedFrame = 3;
		maxDistance = std::numeric_limits<float>::infinity();

		p = p2 = 1.1;
		stepSize = 0.1f;
		alpha_smootheness_w = 0.4f;

		occlusion_basepenalty = 1e-3;
		clamp_distSqr = 1e-3f;
//		stdDev_z_sqr = 1e-3;
		sig_windowSize_sqr = 100;
		model_occlusion = true;
		weight_error_via_windowSize = true;
		alpha_point2plane = 1;//.5f;
		alpha_point2point = 0;//0.5f;
		icp_culling_dist = 0.15f;
		maxItICP = 10;

		samplesPerFrame = 1000;
		samplePerLabel = false;
		k_nn = 10;
		sigma_dist = 5e-2;
		gc_use_normals = true;
		lambda_dataTerm = 5;
		lambda_smoothness_temporal = 1;
		lambda_smoothness_spatial = 1;
		maxItCRF = 5;
		lambda_crf_s = 1;
		max_annealing_factor_crf = 100;
		crf_sigma_sp_factor = 2;
		crf_sigma_t_factor = 1;
		crf_min_by_merge = true;

		lambda_generate_hyp = 2.5f;
		k_generated_hyps = 3;
		cleanUpClouds = true;
		mergeMotions = true;

		delete_labels_numframes_threshold = 50;

		per_hyp_cost_factor = 1;

		icp_type = MotionHypotheses::ICP_FANCY;

		removeThreshold = std::max<float>(samplesPerFrame * delete_labels_numframes_threshold * 0.005, 50.f);
	}


	~algorithmParameters();
};

class parameterLoader{
public:
	static void loadFromFile(const std::string & filePath, experimentParameters & exp_params, algorithmParameters & alg_params);
	static std::string to_long_string(experimentParameters & exp_params, algorithmParameters & alg_params);
};