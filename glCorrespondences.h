#pragma once
#include "glDisplayable.h"
#include "explainMeDefinitions.h"
#include "trajectory.h"

class glCorrespondences:public glDisplayable
{
private:
	QGLBuffer m_pos1Buffer, m_pos2Buffer, m_indexBuffer, m_colorBuffer;
	std::vector<int> indx;
	std::vector<tuple3f> pos_1, pos_2;
	std::vector<tuple3f> color;
	bool isUpToDate;
	bool hasColor;
public:
	
	glCorrespondences(std::vector<pcl::PointXYZ> & cloud_a, 
		std::vector<pcl::PointXYZ>  cloud_b);
	glCorrespondences(std::vector<Eigen::Vector3f> & cloud_a, 
		std::vector<Eigen::Vector3f>  cloud_b);
	glCorrespondences(std::vector<Eigen::Vector3f> & cloud_a,
		std::vector<Eigen::Vector3f> & cloud_b,
		std::vector<Eigen::Vector3f> & color);
	glCorrespondences(std::vector<std::vector<pcl::PointXYZ>> & paths);
	glCorrespondences(std::vector<std::vector<pcl::PointXYZ>> & paths, std::vector<Eigen::Vector3f> & colors);
	glCorrespondences(std::vector<trajectory_R> & paths, std::vector<Eigen::Vector3f> & colors);
	~glCorrespondences(void);

	virtual void glPrepare();

	virtual bool isGlPrepared();

	virtual void glUpdate();

	virtual bool isGlUpToDate();

	virtual void glDraw();


	void updateData(std::vector<trajectory_R> & paths, std::vector<Eigen::Vector3f> &colors);
	void updateData(std::vector<std::vector<pcl::PointXYZ>> & paths, std::vector<Eigen::Vector3f> &colors);
	void updateData(std::vector<Eigen::Vector3f> & cloud_a, std::vector<Eigen::Vector3f> & cloud_b);
	void updateData(std::vector<Eigen::Vector3f> & cloud_a, std::vector<Eigen::Vector3f> & cloud_b, std::vector<Eigen::Vector3f> & color);
	void updateData(std::vector<pcl::PointXYZ> & cloud_a, std::vector<pcl::PointXYZ> & cloud_b);

};

