#pragma once
#include "mySimpleKdTree.h"
#include "Sensors.h"
#include <Eigen/Core>

template<unsigned int N>
class p_ndim{
public:
	//union{
		Eigen::Matrix<float,N,1> vec;
	//	float data[N];
	//};
	int index;
	p_ndim(){}
	~ p_ndim(){}

	const float & operator[] (int idx) const {
		return vec(idx);//data[idx];
	}

	void set(float x, float y, float z, float r, float g, float b){
		vec << x, y, z, r, g, b;
	}
};

template<unsigned int N>
class sortBy_ndim
{
	int dim;
public:
	sortBy_ndim(){dim = 0;}
	~sortBy_ndim(){}

	void setDim(int what){
		dim = what;
	}

	 bool operator() (const p_ndim<N> & x, const p_ndim<N> & y) const {return x[dim]<y[dim];}

};

template<unsigned int N>
class mySimpleKdTree_ndim
{
public:
	std::vector<p_ndim<N>> data;
	Eigen::Matrix<float,N,1> scale;

	//scales are used to rescale dimensions internally
	mySimpleKdTree_ndim(Eigen::Matrix<float,N,1> & scls){ scale = scls;}
	~mySimpleKdTree_ndim(void){}


	void setData(std::vector<p_ndim<N>> & data){
		this->data = data;
		//buildTree();
	}

	/*template<typename pointT>
	void setDataT(std::vector<pointT> & data){
		this->data.resize(data.size());
		for(int i = 0; i < data.size(); i++){
			this->data[i] = pxyz(data[i].x,data[i].y, data[i].z);
			this->data[i].index = i;
		}
	}

	template <typename pcl_cloudPtr>
	void setData(pcl_cloudPtr cloud){
		data.resize(cloud->size());
		for(int i = 0; i < cloud->size(); i++){
			auto & p = cloud->at(i);
			data[i].x = p.x;
			data[i].y = p.y;
			data[i].z = p.z;
			data[i].index = i;
		}
	}*/

	void buildTree(){

		int sz = data.size();
		if(sz == 0){
			return;
		}
		sortBy_ndim<N> comp;

		std::vector<a_b_level> stack;
		stack.push_back(a_b_level(0,sz,0));
		a_b_level curr;
		int mid;

		auto beg = data.begin();

		while(stack.size() != 0){
			curr = stack.back();
			stack.pop_back();

			mid =curr.a+(curr.b-curr.a)/2;
			
			//std::cout << "(" << curr.a << " " << mid << " " << curr.b << ")\n";

			comp.setDim(curr.level % N);
			std::nth_element(beg+curr.a,beg + mid, beg+curr.b, comp);
			//std::nth_element(beg+curr.a,beg + mid, beg+curr.b);
			if(curr.a < mid -1){ //else the range [a, mid) is sorted anyway
				stack.push_back(a_b_level(curr.a, mid, curr.level +1));
			}
			if(curr.b > mid+1){
				stack.push_back(a_b_level(mid +1, curr.b, curr.level +1));
			}
		}
	}
		
};

template<unsigned int N>
class simpleNNFinder_ndim
{
	std::vector<p_ndim<N>> & data;
	std::vector<a_b_level_dad> stack;
	a_b_level_dad curr;
	int mid, axis, sz;
	float dist_sqr;
	float temp_dist_sqr;

	p_ndim<N> current, dad;	
	Eigen::Matrix<float,N,1> scale;
	distSqr_idx result;
public:
	simpleNNFinder_ndim(mySimpleKdTree_ndim<N> & tree):
		data(tree.data)
	{
		stack.reserve(128);
		sz = data.size();
		scale = tree.scale;
	}

	~simpleNNFinder_ndim(){}
		

	distSqr_idx findNN(p_ndim<N> p, float maxR_sqr = 1e20, int maxTests = 2147483647){
		//init stack
		stack.push_back(a_b_level_dad(0,sz,0,-1));
		p.vec = p.vec.array() * scale.array();

		//init result
		result.index = -1;//current.index;
		dist_sqr = result.dist_sqr = maxR_sqr;//(current.x - p.x)*(current.x - p.x) + (current.y - p.y)*(current.y - p.y) + (current.z - p.z)*(current.z - p.z) ;

		if(sz == 0){
			return result;
		}
		current = data[sz/2];	
		while(stack.size() != 0 && maxTests > 0){
			curr = stack.back();
			stack.pop_back();
			mid =curr.a+(curr.b-curr.a)/2;
			current = data[mid];

			//DECIDE IF THIS BRANCH CAN BE SKIPPED.
			if(curr.dad >= 0){
				dad = data[curr.dad];
				//if this is the second child visited, i.e. p and current lie on different sides.
				//and dads split axis is too far away
				axis = (curr.level -1)%N;
				if((dad[axis] - current[axis])*(dad[axis] - p[axis]) <0 
					&&
					dist_sqr < (dad[axis] - p[axis])*(dad[axis] - p[axis]))
				{
					//skip this branch
					continue;
				}
			}

			//if not skipped: check current node
			temp_dist_sqr = (current[0] - p[0])*(current[0] - p[0]) + (current[1] - p[1])*(current[1] - p[1]) + (current[2] - p[2])*(current[2] - p[2]) ;
			maxTests--;
			//temp_dist_sqr = pow(current.x - p.x,2) + pow(current.y - p.y,2)+ pow(current.z - p.z,2);
			if(temp_dist_sqr < dist_sqr)
			{
				dist_sqr = temp_dist_sqr;
				result.set(dist_sqr, current.index);
			}

			//and push childs, in the correct order.
			axis = curr.level % N;
			if(current[axis] > p[axis]){
				//visit right child second.
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
				//visit left child first
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}

			}
			else{
				//visit left second.
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}
				//visit right first
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
			}

		}
		return result;
	}

	//find knn
	void findkNN(p_ndim<N> p, int k, std::vector<distSqr_idx> & dist_sqrs_idx, int maxTests = 2147483647){
		dist_sqrs_idx.clear();

		if(sz == 0){
			return;
		}
		p.vec = p.vec.array() * scale.array();
		//init stack
		stack.push_back(a_b_level_dad(0,sz,0,-1));

		//init result
		//result = data[sz/2];	
		//dist_sqr = (result.x - p.x)*(result.x - p.x) + (result.y - p.y)*(result.y - p.y) + (result.z - p.z)*(result.z - p.z) ;

		while(stack.size() != 0 && maxTests > 0){
			curr = stack.back();
			stack.pop_back();
			mid =curr.a+(curr.b-curr.a)/2;
			current = data[mid];


			//DECIDE IF THIS BRANCH CAN BE SKIPPED. Skip only when k elements have been found..
			if(curr.dad >= 0 && dist_sqrs_idx.size() == k){
				dad = data[curr.dad];
				//if this is the second child visited, i.e. p and current lie on different sides.
				//and dads split axis is too far away
				axis = (curr.level -1)%N;
				if((dad[axis] - current[axis])*(dad[axis] - p[axis]) <0 
					&&
					dist_sqr < (dad[axis] - p[axis])*(dad[axis] - p[axis]))
				{
					//skip this branch
					continue;
				}
			}


			//if not skipped: check current node
			temp_dist_sqr = (current.x - p.x)*(current.x - p.x) + (current.y - p.y)*(current.y - p.y) + (current.z - p.z)*(current.z - p.z) ;
			maxTests--;

			//not k guys found? add to result.
			if(dist_sqrs_idx.size() < k){
				dist_sqrs_idx.push_back(distSqr_idx(temp_dist_sqr, current.index));
				push_heap(dist_sqrs_idx.begin(), dist_sqrs_idx.end());
				dist_sqr = dist_sqrs_idx[0].dist_sqr; //max distance
			}
			else if(temp_dist_sqr < dist_sqr)
			{
				pop_heap(dist_sqrs_idx.begin(), dist_sqrs_idx.end());
				dist_sqrs_idx.back().set(temp_dist_sqr, current.index);
				push_heap(dist_sqrs_idx.begin(), dist_sqrs_idx.end());
				dist_sqr = dist_sqrs_idx[0].dist_sqr; //max distance
			}

			//and push childs, in the correct order.
			axis = curr.level % N;
			if(current[axis] > p[axis]){
				//visit right child second.
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
				//visit left child first
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}

			}
			else{
				//visit left second.
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}
				//visit right first
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
			}

		}
		//		return result;
	}

	//find knn
	void radiusSearch(p_ndim<N> p, float r_squared, std::vector<distSqr_idx> & dist_sqrs_idx, int maxTests = 2147483647){
		dist_sqrs_idx.clear();

		if(sz == 0){
			return;
		}
		p.vec = p.vec.array() * scale.array();
		//init stack
		stack.push_back(a_b_level_dad(0,sz,0,-1));

		while(stack.size() != 0 && maxTests > 0){
			curr = stack.back();
			stack.pop_back();
			mid =curr.a+(curr.b-curr.a)/2;
			current = data[mid];


			//DECIDE IF THIS BRANCH CAN BE SKIPPED. 
			if(curr.dad >= 0 ){
				dad = data[curr.dad];
				//if this is the second child visited, i.e. p and current lie on different sides.
				//and dads split axis is too far away
				axis = (curr.level -1)%N;
				if((dad[axis] - current[axis])*(dad[axis] - p[axis]) <0 
					&&
					r_squared < (dad[axis] - p[axis])*(dad[axis] - p[axis]))
				{
					//skip this branch
					continue;
				}
			}


			//if not skipped: check current node
			temp_dist_sqr = (current.x - p.x)*(current.x - p.x) + (current.y - p.y)*(current.y - p.y) + (current.z - p.z)*(current.z - p.z) ;
			maxTests--;

			if(temp_dist_sqr < r_squared)
			{
				dist_sqrs_idx.push_back(distSqr_idx(temp_dist_sqr, current.index));
				//dist_sqrs_idx.back().set(temp_dist_sqr, current.index);
				
				//dist_sqr = dist_sqrs_idx[0].dist_sqr; //max distance
			}

			//and push childs, in the correct order.
			axis = curr.level % N;
			if(current[axis] > p[axis]){
				//visit right child second.
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
				//visit left child first
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}

			}
			else{
				//visit left second.
				if(curr.a < mid){
					stack.push_back(a_b_level_dad(curr.a, mid, curr.level +1, mid));
				}
				//visit right first
				if(curr.b > mid+1){
					stack.push_back(a_b_level_dad(mid +1, curr.b, curr.level +1, mid));
				}
			}

		}
		//		return result;
	}
};



class simpleTreeTools{
public:
	static p_ndim<6> p(float x, float y, float z, float r, float g, float b){
		p_ndim<6> res;
		res.index = -1;
		res.vec << x,y,z,r,g,b;
		return res;
	}

	template <typename pcl_cloudPtr>
	static void setData(mySimpleKdTree_ndim<6> & tree, pcl_cloudPtr cloud){
		auto & data = tree.data;
		auto & scales = tree.scale;

		data.resize(cloud->size());
		for(int i = 0; i < cloud->size(); i++){
			auto & p = cloud->at(i);
			data[i].vec << scales(0) *p.x, scales(1) *p.y , scales(2)*p.z, scales(3)*p.r, scales(4) * p.g, scales(5)* p.b;
			data[i].index = i;
		}
	}

	template <typename pcl_cloudPtr>
	static void set2dData(mySimpleKdTree & tree, pcl_cloudPtr cloud, RangeSensorDescription * d	){
		auto & data = tree.data;
		
		data.resize(cloud->size());
		for (int i = 0; i < cloud->size(); i++){
			auto & p = cloud->at(i);
			float i_, j_;
			d->project(p.x, p.y, p.z, i_, j_);
			data[i].x = i_ / 100;
			data[i].y = j_ / 100;
			data[i].z = 1;
			data[i].index = i;
		}
	}

	template <typename pointT>
	static void to_2d_point(RangeSensorDescription * d, pointT &p, pxyz & target, int idx = -1){
		float i_, j_;
		d->project(p.x, p.y, p.z, i_, j_);
		target.x = i_ / 100;
		target.y = j_ / 100;
		target.z = 1;
		target.index = idx;

	}
};