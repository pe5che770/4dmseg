#pragma once
#include "sensorDescriptionData.h"
//#include <thrust/host_vector.h>
#include <vector>

#include "tuples.h"
//#include <Eigen/Dense>

struct gpu_frame;
struct gpu_cam;
struct gpu_alignments;
struct gpu_dataterm;
struct gpu_queries;



//To be initialized once
//-push all clouds to the gpu (once)
//Either in init or in an iteration:
//setSparsePoints()
//
//In an iteration; after queries have been set.
//+initSparseValues()
//+setMotionSet()
//
//Then; call the addDataterm method of choice.
class cudaDataterm
{
	class spatial_crf_singleFrameResults
	{
		friend class cudaDataterm;
	private:
		int w, h;
		float lambda_spatial, sigma_spatial;
		int spatial_support_2;
		std::vector<float> zeros;
		// one buffer per hypothesis for crf normalization
		std::vector<gpu_frame*> gpu_buffers_Q;
		//three general buffer frames
		gpu_frame * mins, *sums, *tempBuffer, *maxs;
		//for domain transform
		//gpu_frame *dIdxSAT, *dIdySAT, *valSAT;
		gpu_frame *generalpurposeBuffer;


	public:
		spatial_crf_singleFrameResults(int w, int h);
		~spatial_crf_singleFrameResults();
		void getAndNormalize(int hyp, std::vector<float> & target, bool doNormalization = true, bool divByQ = false);

	private:
		void resize(int numHyp);
		
		//will crf filter the query set allQueries[firstQuery].... allQueries[lastQuery] for the given depth frame.
		//while normalization will not be performed yet, the normalization factor is also computed.
		void filterSparse2Dense(gpu_queries * allQueries, gpu_dataterm * sparse_dterm, int firstQuery, int lastQuery,
			gpu_frame * depthFrame, gpu_frame * stddevFrame, gpu_cam * cam,
			float sigma_spatial, float lambda_spatial, bool div_by_max);		

		void interpolateSparse2Dense(gpu_queries * allQueries, gpu_dataterm * sparse_dterm, int firstQuery, int lastQuery,
			gpu_frame * depthFrame, gpu_frame * stdevFrame, gpu_cam * cam,
			float sigma_spatial, float lambda_spatial, float sigma_factor);
		void interpolateSparse2Dense_surroundingFrames(
			gpu_queries * allQueries, 
			gpu_dataterm * sparse_dterm, 
			gpu_alignments * als,
			std::vector<std::pair<int, int> > &frame_QueryRange,
			int target_frame, int delta,
			gpu_frame * depthFrame, gpu_frame * stdevFrame, gpu_cam * cam,
			float sigma_spatial, float sigma_factor);
		void normalizeFilteredHypAndCopyTo(int hyp, gpu_frame * target);

		//only fill in sparse values, no filtering.
		void fillInSparse2Dense(gpu_queries * allQueries, gpu_dataterm * sparse_dterm, int firstQuery, int lastQuery,
			gpu_frame * depthFrame, gpu_cam * cam);


		void getMinsAndSums(gpu_frame * mins_target, gpu_frame * sums_target, gpu_frame * maxs_target = NULL);
		void normalizeFilteredHypAndCopyTo(int hyp, gpu_frame * mins, gpu_frame * sums, gpu_frame * target);
		void normalizeFilteredHypAndCopyTo(int hyp, gpu_frame * mins, gpu_frame * maxs, gpu_frame * sums, gpu_frame * target);
		void filterSparse2Dense_single_hyp(int hyp, gpu_queries * allQueries, gpu_dataterm * sparse_dterm, int firstQuery, int lastQuery,
			gpu_frame * depthFrame, gpu_frame * stddevFrame, gpu_cam * cam,
			float sigma_spatial, float lambda_spatial);

		void filterFillinSparse2Dense_NN_single_hyp(int hyp, gpu_queries * allQueries, gpu_dataterm * sparse_dterm, int firstQuery, int lastQuery,
			gpu_frame * depthFrame, gpu_frame * stddevFrame, gpu_cam * cam
			);

		void interpolateSparse2Dense_single_hyp(int hyp, gpu_queries * allQueries, gpu_dataterm * sparse_dterm, int firstQuery, int lastQuery,
			gpu_frame * depthFrame, gpu_frame * stddev, gpu_cam * cam,
			float sigma_spatial, float lambda_spatial, float sigma_factor);
		void interpolateSparse2Dense_surroundingFrames_single_hyp(
			int hyp, 
			gpu_queries * allQueries,
			gpu_dataterm * sparse_dterm,
			gpu_alignments * als,
			std::vector<std::pair<int, int> > &frame_QueryRange,
			int target_frame, int delta,
			gpu_frame * depthFrame, gpu_frame * stdev, gpu_cam * cam,
			float sigma_spatial, float sigma_factor);


	};

private:
	int w, h;
	float clamp_dist_sqr, base_penalty;
	gpu_cam * cam;
	std::vector<gpu_frame*> gpu_frames;
	std::vector<gpu_frame*> gpu_frames_noise;
	std::vector<gpu_frame*> gpu_frames_dense_Q;

	//buffers for crf normalization:
	std::vector<gpu_frame*> all_crf_sums, all_crf_mins, all_crf_maxs;
	//buffers with normals
	std::vector<gpu_frame*> gpu_frame_nx, gpu_frame_ny, gpu_frame_nz;

	gpu_dataterm * dterm;
	gpu_queries * allQueries;
	std::vector<std::pair<int, int> > frame_QueryRange;

	gpu_alignments * gpuMotion;

	//a few buffers for temp values.
	// one buffer per hypothesis for crf normalization
	/*std::vector<gpu_frame*> gpu_buffers_Q;
	//three general buffer frames
	gpu_frame * mins, *sums, *tempBuffer;*/
	spatial_crf_singleFrameResults spatial_crf;

	//dummyvalue objects.
	gpu_frame * dummyFrame, *dummy_Q_frame;
public:
	cudaDataterm(sensorDescriptionData & sensor, float clamp_dist_squared, float base_penalty, int numFrames, int samples_per_frame);
	~cudaDataterm();
		
	void pushCloud2Gpu(std::vector<float> & depth_frame, std::vector<float> & stddev_frame, std::vector<float> & nx, std::vector<float> & ny, std::vector<float> & nz);
	//init the sparse dataterm
	void initSparseValues(int num_hyp);//initDataTerm->initSparseValues
	//resets the dterm buffer to basepenalty
	//and the weights to zero.
	void resetSparseValues();//resetDataTerm->resetSparseValues
	void resetSparseValuesTo(float penalty);
	void resetSparseValues(int label);
	void resetSparseValues(int label, float base_penlty);

	void getSparseValues_FilledIn(std::vector<std::vector<float> > & target, int frame);
	

	void setMotionSet(std::vector<std::vector<float*> > & motion, std::vector<std::vector<float*> > & motion_inv);
	void setSparsePoints(std::vector<pxyz_normal_frame> & queries_all_frames); //setQueries->set sparse points.

	//init dense Q values
	void setQ(int frame, std::vector<float> & probs);
	void setQ(int frame, float toConst);
	void setQ(int frame, int coord_x, int coord_y, float toConst);
	
	//fast, samples query indices on the gpu
	void addDataTerm(int frame);// , std::vector<int> & queries_for_frame);
	//more overhead, pushes the provided sample indices to the gpu.
	void addDataTerm(int frame, std::vector<int> & queries_for_frame);
	//do not sample the queries but use them all! O(n^2) in the number of frames,
	//but fast enough for 100ds of frames.
	void addDataTerm_allQueries(int frame);
	void addDataTerm_allQueries_allFrames();
	//do a crf update (temporal) (rename method)
	void filter_dense2sparse_temporal_crf_oneHyp_no_Normalization(int hyp, float simga_temporal);
	//void filter_sparse_compatibilityTransform(std::vector<std::vector<float>> & compatibilities);
	void crf_exp_normalization_for_sparse(float lambda);
	void crf_compatibility_and_exp_normalization_for_sparse(std::vector<std::vector<float> > & compat, float lambda);
	void crf_maxcompatibility_and_exp_normalization_for_sparse(std::vector<std::vector<float> > & compat, float lambda);
	//void crf_compatibility_for_sparse(std::vector<std::vector<float>> & compat, float lambda);

	void filter_sparse2dense_prepare_spatial_crf(float sigma_spatial, float lambda_spatila, bool divByMax);
	void filter_sparse2dense_do_prepared_spatial_crf(int hyp, float sigma_spatial, float lambda_spatila, float sigma_factor, bool doNormalization, bool div_by_max);

	void filter_sparse2dense_viaNN(int hyp);

	//just smoothing on current dense buffers.
	void filter_dense2dense_allFrames_currentData(
		float sigma_sp, float factor);
	void filter_sparse2dense_allFrames_currentData(int hypothesis,
		float sigma_sp, float factor);


	spatial_crf_singleFrameResults & filter_sparse2dense_spatial_crf_oneFrame(int frame, float sigma_spatial, float lambda_spatila, float sigma_factor,bool normalize = true, bool divByMax = false);
	
	void updateLabelsForInterpolationFormMultipleFrames();
	spatial_crf_singleFrameResults & interpolate_sparse2dense_oneFrame_fromMultipleFrames(int frame, float sigma_spatial, float sigma_factor, int surroundingFrames);
	spatial_crf_singleFrameResults & interpolate_sparse2dense_spatial_oneFrame_oneHyp(int frame, int hyp, float sigma_spatial, float sigma_factor);
	
	void getUnnormedDataterm(int hyp, std::vector<float> & dataterm, std::vector<float> & weights);
	void getUnnormedDataterm(int hyp, std::vector<float> & dataterm);
	void getSparseCRFResult(std::vector<std::vector<float> > & result);
	void getSparseValues(int hyp, std::vector<float> & dataterm); //getDataterm->getSparseValues
	void normalize_sparse_by_weights();

	void getCurrentDenseQBuffer(int frame, std::vector<float> & target);
	void getNoise(int i, std::vector<float> & buff);

	float getSummedUpSparseValues(int sparse_channels, std::vector<int> & hyps_to_sum_up_over, std::vector<int> & labels_per_sparse_point);

	void setSparseValues(std::vector<float> & dataterm, int hyp); 
	void setSparseValues(std::vector<std::vector<float> > & dataterm);
};

