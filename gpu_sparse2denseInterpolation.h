#pragma once
#include "explainMe_algorithm_withFeatures.h"
class cudaDataterm;

class gpuInterpolation
{
	explainMe_algorithm_withFeatures::Ptr alg;
	std::vector<float> buffer, buffer_sprs;
	
public:
	gpuInterpolation(explainMe_algorithm_withFeatures::Ptr algo);
	~gpuInterpolation();

	void prepareInterpolation(cudaDataterm * cuda_dev,
		explainMe::MatrixXf_rm & sparse_weights);

	void doInterpolation(cudaDataterm * cuda_dev,
		int frame,
		explainMe::MatrixXf_rm & denseweights_frame_out
		);
};

