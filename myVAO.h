#pragma once
//#include "gl3w.h"
#include <QtOpenGL/QGLWidget>

/************************************************************************/
/* Just a wrapper for VAOs                                    */
/************************************************************************/
#ifndef _MSC_VER
#include <QOpenGLFunctions_3_3_Core>
#endif

class myVAO
{
private:
#ifdef _MSC_VER
	typedef void (WINAPI *GL_GEN_VARRAYS)(GLsizei , GLuint *);
	typedef void (WINAPI *GL_BIND_VARRAY)(GLuint);
	typedef void (WINAPI *GL_DELETE_VARRAY)(GLsizei , GLuint *);

	GL_GEN_VARRAYS glGenVertexArrays;
	GL_BIND_VARRAY glBindVertexArray;
	GL_DELETE_VARRAY glDeleteVertexArrays;
#else
	QOpenGLFunctions_3_3_Core* fun;
#endif
	uint vao;
	bool created;
public:
	myVAO(void);
	~myVAO(void);

	void create();
	bool isCreated();
	void bind();
	void release();
};

