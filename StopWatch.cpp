#include "StopWatch.h"
#include <iostream>

void StopWatch::stop(){
	if (started == true){
		t_elapsed += (boost::posix_time::microsec_clock::local_time() - start).total_milliseconds();
	}
	else{
		std::cout << "stopwatch stopped without being started";
	}
	started = false;
}