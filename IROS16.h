#pragma once
#include <qobject.h>
#include <QApplication>
#include <iostream>
#include "statsKeeper.h"
#include "CloudReader.h"
#include <vector>
#include "algorithmParameters.h"
#include "algorithmStateRenderer.h"
#include "mySimpleKdTree.h"

//The IROS algorithm
class ExperimentIros : public QObject
{
	Q_OBJECT;
private:
	int start_with_frame;
public:
	typedef boost::shared_ptr<ExperimentIros> Ptr;
	//the parameters
	statsKeeper * mystats;
	algorithmParameters params;

	//the reader
	CloudReader::Ptr data;

	//the data
	std::vector<explainMe::Cloud::Ptr> allClouds;
	std::vector<mySimpleKdTree> allClouds_kdTree;
	explainMe::Cloud::Ptr sampledPoints_zero;

	//trajectories[cloud][trajIdx]
	std::vector<explainMe::Cloud::Ptr> trajectories;

	//currentMotionSets[cloud][label]
	//to have consistent dimensions, for f frames there 
	//will be f transformations; the transformation to the first frame is
	//fixed to be the identity.
	std::vector<std::vector<Eigen::Matrix4f>> currentMotions;
	//currentLabeling[cloud][trajIdx]
	//std::vector<std::vector<float>> labelWeights;
	std::vector<int> hardLabels;
	Eigen::MatrixXf trajectory_p_traj_giv_label;

	std::string outputFolder;
public:

	ExperimentIros(CloudReader::Ptr data, algorithmParameters  &params, std::string &subresultFolder);
	~ExperimentIros();
	void setStats(statsKeeper & stats_keeper){
		mystats = &stats_keeper;
	}
	void loadAllClouds();
	int numLabels(){ return (currentMotions.size() == 0 ? -1 : currentMotions[0].size()); }
	int numTrajectories(){ return (trajectories.size() == 0 ? -1 : trajectories[0]->size()); }

	//the algorithm
	void sampleCloud0();
	//init
	void initTrajectories(explainMe::Cloud::Ptr baseSamples);
	//initially trajectories are extended via NN
	void extendTrajectories(int frame);
	//subspaceClustering on the trajectories (soft labels
	void EMsubspaceClustering();
	//gc optimizaiton (hard labels)
	void gcOptForLabels();
	//motion estimation and analysis
	void motionEstimation(int frame);

	void store(std::string & file);
	void restore(std::string & file);
private:
	Eigen::MatrixXf gramSchmidt(Eigen::MatrixXf & V);
	Eigen::MatrixXf null_space(Eigen::MatrixXf & V);
	//for stability: the normal distribution normalization factor 1./2pi^...det(sigma)
	//is returned separately as normalization_factor * 2^normalization_factor_2_exponent
	Eigen::VectorXf mvnpdf_unnormalized_around0(Eigen::MatrixXf &x, Eigen::MatrixXf &sigma,
		float & normalization_factor, float & normalization_factor_2_exponent);

	//the smallest angular distance to any other space
	float minimalMaxPrincipleAngle(Eigen::MatrixXf & new_space_ortho, std::vector<Eigen::MatrixXf> & old_spaces_ortho);

	//reproject: the trajectories according to the found trajectories
	//are reprojected onto the observed clouds via NN
	void updateTrajectories_fromLabelsAndMos_between(
		std::vector<std::vector<Eigen::Matrix4f>> & motions_by_frame_label,
		int from_frame, int to_frame, bool reproject);
	void waitForCMDok(int frame, bool wait = true, bool wait_caused_by_error = false);
	void printStats(std::string name, int line, Eigen::MatrixXf  mat, bool doit = false);
public Q_SLOTS:
	//for documnetation
	void run_with_stats();

Q_SIGNALS:
	void documentSampledPoints();
	void documentTrajectories();
	void documentCloud(explainMe::Cloud::Ptr);
	void documentLabeledTrajectories(bool resetViewAndSize, std::string filename);
	void documentWeightedTrajectoriesVS(bool, Eigen::MatrixXf &, int, int, std::string);
	void documentWeightedTrajectories_row(bool, Eigen::MatrixXf &, int, std::string);
	void documentWeightedTrajectories_col(bool, Eigen::MatrixXf &, int, std::string);
};

//output/vis of data & results.
//setup is a bit convoluted because for rendering
//I need to be in the gui thread, while computation 
//happens in back trhead
class IROSChronist : public QObject
{
	Q_OBJECT;
	ExperimentIros::Ptr experimentToDocument;
	algorithmStateRenderer * renderer;
	std::string outputFolder;
public:
	typedef boost::shared_ptr<IROSChronist> Ptr;
	IROSChronist(ExperimentIros::Ptr experiment, std::string & folder){
		experimentToDocument = experiment;
		outputFolder = folder;

		auto d = experiment->data->getDescription();
		renderer = new algorithmStateRenderer(d->width(), d->height());//new demo_global_features();
		QMatrix4x4 proj_matrix = d->frustum();
		renderer->setPreferredProjection(proj_matrix, d->width(), d->height());
	}

	~IROSChronist(){
		delete renderer;
	}
public Q_SLOTS:
	void documentSampledPoints();
	void documentTrajectories();
	void documentCloud(explainMe::Cloud::Ptr);
	void documentLabeledTrajectories(bool resetViewAndSize, std::string filename);
	void documentWeightedTrajectoriesVS(bool resetViewAndSize, Eigen::MatrixXf & weights, int i, int j, std::string filename);
	void documentWeightedTrajectories_row(bool resetViewAndSize, Eigen::MatrixXf & weights, int i, std::string filename);
	void documentWeightedTrajectories_col(bool resetViewAndSize, Eigen::MatrixXf & weights, int i, std::string filename);
};

//QApplication. inits experiment, parses parameter
class QIROS16Application : public QApplication
{
	Q_OBJECT;
private:
	ExperimentIros::Ptr experiment;
	IROSChronist::Ptr chronist;
public:
	QIROS16Application(int argc, char ** arg,
		std::map<std::string, std::string> & cmd_str, //already parsed
		std::map<std::string, bool> & cmd_bool);

	~QIROS16Application(){
		std::cout << "QIROS16Application being killed...";
	}
};

