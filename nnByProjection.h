#pragma once
#include "mySimpleKdTree_ndim.h"
#include "Sensors.h"
#include "explainMeDefinitions.h"


//Allows finding nn via projection along a random direction.
template<unsigned int N>
class nnByProjection
{
	//Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>	 & idx;
	Eigen::Matrix4f input2QueryView;
	Eigen::MatrixXi	 & idx;
	explainMe::Cloud::Ptr cloud;
	RangeSensorDescription * d;
public:
	nnByProjection(
		//Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>	 & projectedInd_1based,
		Eigen::MatrixXi	 & projectedInd_1based,
		explainMe::Cloud::Ptr cloud_,
		Eigen::Matrix4f cloud2ProjView,
		RangeSensorDescription * description) :idx(projectedInd_1based)
	{
		d = description;
		cloud = cloud_;
		input2QueryView = cloud2ProjView;
	}
	~nnByProjection(){}


	//consistent interface with kdtree.
	distSqr_idx findNN(p_ndim<N> p, float maxR_sqr){
		int i, j, index, best_index = -1; float distSqr, best_distSqr = maxR_sqr;
		Eigen::Vector4f query;
		query << p[0], p[1], p[2], 1;
		query = input2QueryView * query;

		d->project(query.x(),query.y(), query.z(), i, j);
		if (! d->isCoordValid(i, j)){
			return distSqr_idx(maxR_sqr, -2);
		}

		//search small nbrhood
		for (int di = -1; di <= 1; di++){
			for (int dj = -1; dj <= 1; dj++){
				//indices one based...
				if (d->isCoordValid(i + di, j + dj) && idx(i + di, j + dj) >= 1) {
					index = idx(i + di, j + dj) - 1;
					auto & q = cloud->at(index);
					distSqr = (q.x - p[0])*(q.x - p[0]) + (q.y - p[1])*(q.y - p[1]) + (q.z - p[2])*(q.z - p[2]);
					if (distSqr < best_distSqr){
						best_index = index;
						best_distSqr = distSqr;
					}
				}
			}
		}
			
		//best_index -1 if there where no valid pixels around..
		return distSqr_idx(best_distSqr, best_index);
	}
};


//consistent with my_simple_kdtree.
//ofc easier to just use grid cloud and return position instead of index.
//K: windows (size-1) /2
template<int K>
class nnByProjection3D
{
	//Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>	 & idx;
	//make them all const
	//Eigen::Matrix4f input2QueryView;
	Eigen::MatrixXi	 idx;
	explainMe::Cloud::Ptr cloud;
	RangeSensorDescription * d;
	int _k;
public:
	nnByProjection3D(
		//Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>	 & projectedInd_1based,
		//this to be consistent with the kdtree, to return the index of the result....
		//Eigen::MatrixXi	 & projectedInd_1based,
		explainMe::Cloud::Ptr cloud_,
		//Eigen::Matrix4f cloud2ProjView,
		RangeSensorDescription * description,
		int winsize_2) //:idx(projectedInd_1based)
	{
		d = description;
		cloud = cloud_;
		idx.resize(d->width(), d->height());
		idx.fill(0);
		int i, j;
		int ind = 0;
		for (auto & p : cloud_->points){
			d->project(p.x, p.y, p.z, i, j);
			if (d->isCoordValid(i, j)){
				//onebased.
				idx(i, j) = ind + 1;
			}
			ind++;

		}
		_k = winsize_2;
		//input2QueryView = cloud2ProjView;
	}
	~nnByProjection3D(){}


	//make this const. then totally safe to use in parallel.
	//consistent interface with kdtree.
	distSqr_idx findNN(pxyz p, float maxR_sqr, float dummy = 0);
};
