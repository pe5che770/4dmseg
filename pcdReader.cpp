#include "pcdReader.h"
#include "fileReader.h"
#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/features/normal_3d_omp.h>
#include "cudaTool.h"
//#include <pcl/features/normal_.h>
#include <pcl/filters/fast_bilateral.h>

pcdReader::pcdReader(std::string folder_,sensorType t):
	cloud(new explainMe::Cloud()),
	cloud_in(new pcl::PointCloud<pcl::PointXYZRGB>())
{
	if( t== DONT_CARE || t == ASUS_XTION){
		sensorDescription = new asusXtionProDescription();
	}
	else if (t == VIRTUAL_SCANNER){
		sensorDescription = new virtualCamera();
	}
	folder = folder_;
	fileReader::listFiles(folder, ".*\\.pcd", files);


#ifndef _COMPILE_NO_CUDA_
	cuda = new cudaTool(sensorDescription->descriptionData());
#endif // !_COMPIE_NO_CUDA

	std::cout << folder << ":\n";
	std::cout << "Found " << files.size() << " pcd files";
}


pcdReader::~pcdReader(void)
{
	delete sensorDescription;
#ifndef _COMPILE_NO_CUDA_
	delete cuda;
#endif // !_COMPIE_NO_CUDA
}


void pcdReader::computeNormals(explainMe::Cloud::Ptr cloud)
{
  
/*  // Create the normal estimation class, and pass the input dataset to it
	//pcl::NormalEstimation<explainMe::Point, explainMe::Point> ne;
	pcl::NormalEstimationOMP<explainMe::Point, explainMe::Point> ne;
  ne.setInputCloud (cloud);

  // Create an empty kdtree representation, and pass it to the normal estimation object.
  // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
  pcl::search::KdTree<explainMe::Point>::Ptr tree (new pcl::search::KdTree<explainMe::Point> ());
  ne.setSearchMethod (tree);

  // Output datasets
  //pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

  // Use all neighbors in a sphere of radius 3cm
  ne.setRadiusSearch (0.03);

  // Compute the features
  //ne.compute (*cloud_normals);
  ne.compute (*cloud);
  std::cout << "normals added.\n";

  // cloud_normals->points.size () should have the same size as the input cloud->points.size ()**/
}


#ifndef _COMPILE_NO_CUDA_
void pcdReader::computeNormalsFast(explainMe::Cloud::Ptr cloud)
{
  
  // Create the normal estimation class, and pass the input dataset to it
	//pcl::NormalEstimation<explainMe::Point, explainMe::Point> ne;

	normalBuff.resize(3* cloud->size());
	depthBuff.resize(cloud->size());

	for(int i = 0; i< cloud->size(); i++){
		depthBuff[i] = cloud->at(i).z;
	}
  std::cout << "normals added.\n";

  cuda->dev_setData(& depthBuff[0]);
  cuda->bilateralFilter();
  cuda->bilateralFilter();
  cuda->computeNormals();
  cuda->dev_getNormals(& normalBuff[0]);

  int sz= cloud->size();
  for(int i = 0; i< cloud->size(); i++){
	  cloud->at(i).getNormalVector3fMap() << normalBuff[i],normalBuff[sz+i],normalBuff[2*sz+i];
	}

  // cloud_normals->points.size () should have the same size as the input cloud->points.size ()*
}
#else
void pcdReader::computeNormalsFast(explainMe::Cloud::Ptr cloud)
{
	computeNormals(cloud);
}
#endif // !_COMPILE_NO_CUDA_



bool pcdReader::loadFrame(int frame, bool cleanUp, float maxDistAllowed, bool bilateralFilter, float sig_z){

	if(frame < numberOfFrames() && frame >=0){
		if(pcl::io::loadPCDFile<explainMe::Point> (folder + "/" + files[frame], *cloud) == -1) //* load the file
		{
			std::cout << "Couldn't read file test_pcd.pcd \n";
			return false;
		}

		//if(maxDistAllowed * 0 == 0){
			float nan = std::numeric_limits<float>::quiet_NaN();
			for (int i = 0; i < cloud->width; i++){
				for (int j = 0; j < cloud->height; j++){
					

					if (cloud->at(i, j).z > maxDistAllowed || cloud->at(i, j).z ==0){
						cloud->at(i,j).x = nan;
						cloud->at(i,j).y = nan;
						cloud->at(i,j).z = nan;
					}
					cloud->at(i, j).curvature = sensorDescription->sigmaHat(i, j, cloud->at(i, j).z, 0, cloud->at(i, j).getNormalVector3fMap().dot(-cloud->at(i, j).getVector3fMap().normalized()));
				}
			}
			/*for(int i = 0; i < cloud->size(); i++){
				//cloud->at(i).curvature = sensorDescription->std_dev_noise(-1, -1, cloud->at(i).z, 0);

				if(cloud->at(i).z > maxDistAllowed){
					cloud->at(i).x = nan;
					cloud->at(i).y = nan;
					cloud->at(i).z = nan;
				}
				//cloud->at(i).curvature = 0; //just so it is set to something.
			}*/
		//}

		//pcl::copyPointCloud<pcl::PointXYZRGB, explainMe::Point>(*cloud_in, *cloud);
		//computeNormals(cloud);
		//computeNormalsFast(cloud);
		//std::cout << "Loaded cloud " << cloud->width << ", " << cloud->height << "\n";

			if (bilateralFilter){
				std::cout << "****Bilaterally filtering...";
				pcl::FastBilateralFilter<pcl::PointXYZ> filter;
				pcl::PointCloud<pcl::PointXYZ>  output;
				pcl::PointCloud<pcl::PointXYZ>::Ptr input(new pcl::PointCloud<pcl::PointXYZ>());
				input->resize(cloud->size());
				input->width = cloud->width;
				input->height = cloud->height;
				input->getMatrixXfMap() = cloud->getMatrixXfMap();
				/*explainMe::Cloud::Ptr input(new explainMe::Cloud());
				input->resize(cloud->size());
				*input = *cloud;*/
				filter.setInputCloud(input);
				filter.setSigmaS(0.1);
				filter.setSigmaR(sig_z);
				filter.applyFilter(output);
				cloud->getMatrixXfMap() = output.getMatrixXfMap();
				std::cout << "done.\n";
			}
		return true;
	}
	return false;
}


std::vector<std::string> pcdReader::fileNames(int frame){
	std::cout << frame;
	std::vector<std::string> files_;
	files_.push_back(folder + "/" + files[frame]);
	return files_;
}

bool pcdReader::loadFrame_withoutNormals(int frame, bool cleanUp){

	if(frame < numberOfFrames() && frame >=0){
		if(pcl::io::loadPCDFile<pcl::PointXYZRGB> (folder + "/" + files[frame], *cloud_in) == -1) //* load the file
		{
			std::cout << "Couldn't read file test_pcd.pcd \n";
			return false;
		}

		pcl::copyPointCloud<pcl::PointXYZRGB, explainMe::Point>(*cloud_in, *cloud);
		//computeNormals(cloud);
		//computeNormalsFast(cloud);
		//std::cout << "Loaded cloud " << cloud->width << ", " << cloud->height << "\n";
		return true;
	}
	return false;
}

void pcdReader::storeCloud(explainMe::Cloud::Ptr cloud, int frame){
	pcl::io::savePCDFileBinaryCompressed<explainMe::Point> (folder + "/withNormals/"+ files[frame], *cloud);
	
}
