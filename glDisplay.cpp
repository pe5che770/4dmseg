#include "glDisplay.h"
#include "glDebugging.h"
/*#include "kinectProvider.h"*/
#include <assert.h>
#include <iostream>


glDisplay::glDisplay(QGLFormat & format, QWidget *parent,
						int width, int height):
	QGLWidget(format,parent)
{

	this->setMinimumHeight(height);
	this->setMinimumWidth(width);
	this->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
	this->setBaseSize(width, height);
	//this->setSizePolicy(QSizePolicy())

	/*timer = new QTimer(this);
	timer->connect(timer,SIGNAL(timeout()),this,SLOT(refresh()));*/

	trackBall = new trackBallListener(this, scene);
	connect(trackBall, SIGNAL(rotation(QMatrix4x4 & )), & scene, SLOT(onRotate(QMatrix4x4 &)));
	connect(trackBall, SIGNAL(camMovement(QMatrix4x4 & )), & scene, SLOT(onCamMovement(QMatrix4x4 &)));
}


glDisplay::~glDisplay(void)
{

}

void glDisplay::initializeGL()
{

//	int bla =gl3wInit();
	glDebugging::didIDoWrong();
	glClearColor( 1.f, 1.f, 1.f, 0.f );
	glDebugging::didIDoWrong();
	glViewport(0,0,this->width(),this->height());

	glEnable(GL_LINE_SMOOTH);
	glPointSize(3.f);
	glLineWidth(5.f);//2.f
	glDebugging::didIDoWrong();

	//bindTexture();

	proj.perspective(30 , (0. +this->width())/this->height(), 0.1, 100);
	glEnable(GL_DEPTH_TEST);

	glDebugging::didIDoWrong();
	//timer->start();

	//glEnable (GL_BLEND);
	//just summing everything up.
	//glBlendFunc (GL_ONE, GL_ONE);
}


void glDisplay::setLineWidth(float w){

	glLineWidth(w);
	glDebugging::didIDoWrong();
}

void glDisplay::setPointWidth(float w){

	glPointSize(w);
	glDebugging::didIDoWrong();
}

void glDisplay::resizeGL(int width, int height)
{
	glDebugging::didIDoWrong();
	glViewport( 0, 0, width, height );

	//recompute projection matrix...
	proj.setToIdentity();
	proj.perspective(60, (0.f + this->width())/this->height(),0.1,100);
	//proj.perspective(60, (0.f + this->width())/this->height(),0.1,100);
	
	//kinect2
	//proj.frustum(-0.06972,0.069712,-0.0577,0.0577,0.1,100);
	//eva
	//proj.frustum(-0.0186,0.0196,-.022,.0268, 0.1,100);
}

void glDisplay::resizeGL(int width, int height, QMatrix4x4 & projection){
	glDebugging::didIDoWrong();
	glViewport(0, 0, width, height);
	//recompute projection matrix...
	proj = projection;
}

void glDisplay::paintGL()
{
	//std::cout << "updated!";
	this->makeCurrent();
	glDebugging_didIDoWrong();
	//std::cout << "($)";

	
	// Clear the buffer with the current clearing color
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	//draw stuff...
	for(int i = 0; i < scene.numberDisplayables(); i++){
		glDisplayable * disp = scene.get(i);
		if (disp->isHidden()){
			continue;
		}
		if(!disp->isGlPrepared()){
			disp->glPrepare();
			assert(disp->isGlPrepared());
			glDebugging_didIDoWrong();
		}
		
		if(!disp->isGlUpToDate()){
			//std::cout <<  "u<";
			disp->glUpdate();
			glDebugging_didIDoWrong();
			//std::cout <<  ">";
		}

		disp->setUniformValue("projection", proj);
		QMatrix4x4 modelview_matrix =  scene.getCam() * disp->getModel2World();
		disp->setUniformValue("modelview", modelview_matrix);

		//std::cout << "d[";
		disp->glDraw();
		//std::cout << "]";

		glDebugging_didIDoWrong();
	}

	//for each drawable in the scene...

	//load projection matrix or if ortho is wanted
	//cam matrix.
	//Object matrix

	//select shader...
	//e.g. have a shader manager. each shader has a name.

	//doneCurrent();
	
}

/*void glDisplay::onMessageLogged(QOpenGLDebugMessage m)
{
	 qDebug() << m;
}*/

void glDisplay::refresh()
{
	updateGL();
}

void glDisplay::mouseMoveEvent(QMouseEvent * event)
{
	this->trackBall->onMouseMove(event);
}

void glDisplay::mousePressEvent(QMouseEvent * event)
{
	this->trackBall->onMousePress(event);
}

sceneManager * glDisplay::getSceneManager()
{
	return &scene;
}

void glDisplay::wheelEvent( QWheelEvent * event )
{
	this->trackBall->onMouseWheel(event);
}
