#include "glCorrespondences.h"
#include "glDebugging.h"


glCorrespondences::glCorrespondences( std::vector<pcl::PointXYZ> & cloud_a, 
			std::vector<pcl::PointXYZ> cloud_b ):
	glDisplayable("Resources/default_pair.vert", 
			"Resources/default_pair.frag",
			"Resources/default_pair.geom"),
	m_pos1Buffer(QGLBuffer::VertexBuffer),
	m_pos2Buffer(QGLBuffer::VertexBuffer),
	m_colorBuffer(QGLBuffer::VertexBuffer),
	m_indexBuffer(QGLBuffer::IndexBuffer)
{
	isUpToDate = true;
	hasColor = false;
	assert(cloud_a.size() == cloud_b.size());
	int idx=0;
	for(int i = 0; i < cloud_b.size(); i++){
		auto & p1 =cloud_a[i];
		auto & p2 =cloud_b[i];
		pos_1.push_back(tuple3f(p1.x,p1.y, p1.z));
		pos_2.push_back((tuple3f(p2.x, p2.y, p2.z)));
		indx.push_back(idx);
		idx++;
		
		
	}
}



glCorrespondences::glCorrespondences( std::vector<Eigen::Vector3f> & cloud_a, 
			std::vector<Eigen::Vector3f> cloud_b ):
	glDisplayable("Resources/default_pair.vert", 
			"Resources/default_pair.frag",
			"Resources/default_pair.geom"),
	m_pos1Buffer(QGLBuffer::VertexBuffer),
	m_pos2Buffer(QGLBuffer::VertexBuffer),
	m_colorBuffer(QGLBuffer::VertexBuffer),
	m_indexBuffer(QGLBuffer::IndexBuffer)
{

	hasColor = false;
	updateData(cloud_a,cloud_b);	
	isUpToDate = true;
}

glCorrespondences::glCorrespondences(std::vector<Eigen::Vector3f> & cloud_a,
	std::vector<Eigen::Vector3f> & cloud_b,
	std::vector<Eigen::Vector3f> & color) :
	glDisplayable("Resources/default_pair_color.vert",
	"Resources/default_pair_color.frag",
	"Resources/default_pair_color.geom"),
	m_pos1Buffer(QGLBuffer::VertexBuffer),
	m_pos2Buffer(QGLBuffer::VertexBuffer),
	m_colorBuffer(QGLBuffer::VertexBuffer),
	m_indexBuffer(QGLBuffer::IndexBuffer)
{

	hasColor = true;
	updateData(cloud_a, cloud_b, color);
	isUpToDate = true;
}


glCorrespondences::glCorrespondences(std::vector<std::vector<pcl::PointXYZ>> & paths):
	glDisplayable("Resources/default_pair.vert", 
			"Resources/default_pair.frag",
			"Resources/default_pair.geom"),
	m_pos1Buffer(QGLBuffer::VertexBuffer),
	m_pos2Buffer(QGLBuffer::VertexBuffer),
	m_colorBuffer(QGLBuffer::VertexBuffer),
	m_indexBuffer(QGLBuffer::IndexBuffer)
{
	isUpToDate = true;
	hasColor = false;
	int idx=0;
	for(int i = 0; i < paths.size(); i++){
		auto & path = paths[i];
		
		for(int j = 0; j + 1 < (path.size());j++){
			auto & p1 =path[j];
			auto & p2 =path[j+1];
			pos_1.push_back(tuple3f(p1.x,p1.y, p1.z));
			pos_2.push_back((tuple3f(p2.x, p2.y, p2.z)));
			indx.push_back(idx);
			idx++;
		}		
	}
}


glCorrespondences::glCorrespondences(std::vector<std::vector<pcl::PointXYZ>> & paths,
	std::vector<Eigen::Vector3f> &colors):
	glDisplayable("Resources/default_pair_color.vert", 
			"Resources/default_pair_color.frag",
			"Resources/default_pair_color.geom"),
	m_pos1Buffer(QGLBuffer::VertexBuffer),
	m_pos2Buffer(QGLBuffer::VertexBuffer),
	m_colorBuffer(QGLBuffer::VertexBuffer),
	m_indexBuffer(QGLBuffer::IndexBuffer)
{
	
	hasColor = true;
	updateData(paths, colors);
	isUpToDate = true;
}

glCorrespondences::glCorrespondences(std::vector<trajectory_R> & paths, std::vector<Eigen::Vector3f> & colors):
	glDisplayable("Resources/default_pair_color.vert", 
	"Resources/default_pair_color.frag",
	"Resources/default_pair_color.geom"),
	m_pos1Buffer(QGLBuffer::VertexBuffer),
	m_pos2Buffer(QGLBuffer::VertexBuffer),
	m_colorBuffer(QGLBuffer::VertexBuffer),
	m_indexBuffer(QGLBuffer::IndexBuffer)
{

	hasColor = true;
	updateData(paths, colors);
	isUpToDate = true;
}


glCorrespondences::~glCorrespondences(void)
{
}


void glCorrespondences::updateData(std::vector<std::vector<pcl::PointXYZ>> & paths, std::vector<Eigen::Vector3f> &colors)
{
	int idx=0;
	color.clear();
	pos_1.clear();
	pos_2.clear();
	indx.clear();

	color.reserve(5*paths.size());
	for(int i = 0; i < paths.size(); i++){
		auto & path = paths[i];
		
		for(int j = 0; j + 1 < (path.size());j++){
			auto & p1 =path[j];
			auto & p2 =path[j+1];
			pos_1.push_back(tuple3f(p1.x,p1.y, p1.z));
			pos_2.push_back((tuple3f(p2.x, p2.y, p2.z)));
			color.push_back(tuple3f(colors[i].x(),colors[i].y(),colors[i].z()));
			indx.push_back(idx);
			idx++;
		}		
	}
	isUpToDate = false;
}

void glCorrespondences::updateData(std::vector<trajectory_R> & paths, std::vector<Eigen::Vector3f> &colors){
	int idx=0;
	color.clear();
	pos_1.clear();
	pos_2.clear();
	indx.clear();

	color.reserve(5*paths.size());
	for(int i = 0; i < paths.size(); i++){
		auto & path = paths[i];

		for(int j = path.firstCloud(); j + 1 <= (path.lastCloud());j++){
			auto & p1 =path[j];
			auto & p2 =path[j+1];
			pos_1.push_back(tuple3f(p1.x,p1.y, p1.z));
			pos_2.push_back((tuple3f(p2.x, p2.y, p2.z)));
			color.push_back(tuple3f(colors[i].x(),colors[i].y(),colors[i].z()));
			indx.push_back(idx);
			idx++;
		}		
	}
	isUpToDate = false;
}

void glCorrespondences::updateData(std::vector<Eigen::Vector3f> & cloud_a, std::vector<Eigen::Vector3f> & cloud_b, std::vector<Eigen::Vector3f> &colors)
{
	assert(cloud_a.size() == cloud_b.size());
	int idx = 0;
	color.clear();
	pos_1.clear();
	pos_2.clear();
	indx.clear();
	for (int i = 0; i < cloud_b.size(); i++){
		auto & p1 = cloud_a[i];
		auto & p2 = cloud_b[i];
		pos_1.push_back(tuple3f(p1.x(), p1.y(), p1.z()));
		pos_2.push_back((tuple3f(p2.x(), p2.y(), p2.z())));
		color.push_back(tuple3f(colors[i].x(), colors[i].y(), colors[i].z()));
		indx.push_back(idx);
		idx++;
	}
	isUpToDate = false;
}

void glCorrespondences::updateData(std::vector<Eigen::Vector3f> & cloud_a, std::vector<Eigen::Vector3f> & cloud_b)
{
	assert(cloud_a.size() == cloud_b.size());
	int idx=0;
	pos_1.clear();
	pos_2.clear();
	indx.clear();
	for(int i = 0; i < cloud_b.size(); i++){
		auto & p1 =cloud_a[i];
		auto & p2 =cloud_b[i];
		pos_1.push_back(tuple3f(p1.x(),p1.y(), p1.z()));
		pos_2.push_back((tuple3f(p2.x(), p2.y(), p2.z())));
		indx.push_back(idx);
		idx++;
		
		
	}
	isUpToDate = false;
}

void glCorrespondences::updateData(std::vector<pcl::PointXYZ> & cloud_a, std::vector<pcl::PointXYZ> & cloud_b)
{
	assert(cloud_a.size() == cloud_b.size());
	int idx=0;
	pos_1.clear();
	pos_2.clear();
	indx.clear();
	for(int i = 0; i < cloud_b.size(); i++){
		auto & p1 =cloud_a[i];
		auto & p2 =cloud_b[i];
		pos_1.push_back(tuple3f(p1.x,p1.y, p1.z));
		pos_2.push_back((tuple3f(p2.x, p2.y, p2.z)));
		indx.push_back(idx);
		idx++;
		
		
	}
	isUpToDate = false;
}

void glCorrespondences::glPrepare()
{
	prepareShaderProgram(/*"Resources/default_pair.vert", "Resources/default_pair.frag","Resources/default_pair.geom"*/);
	glDebugging::didIDoWrong();


	m_vao.create();
	m_vao.bind();
	glDebugging::didIDoWrong();

	setUpBuffer("position",m_pos1Buffer, pos_1, QGLBuffer::DynamicDraw);
	setUpBuffer("position2",m_pos2Buffer, pos_2, QGLBuffer::DynamicDraw);

	if(hasColor){
		setUpBuffer("color",m_colorBuffer, color, QGLBuffer::DynamicDraw);
	}

	int bid = m_pos1Buffer.bufferId();

	setUpIndexBuffer(m_indexBuffer,indx, QGLBuffer::DynamicDraw);

	if((bid= m_indexBuffer.bufferId())<=0){
		std::cout << "merde!";
	}
	isUpToDate = true;

}

bool glCorrespondences::isGlPrepared()
{
	return m_shader.isLinked();
}

void glCorrespondences::glUpdate()
{
	isUpToDate = true;
	m_vao.bind();
	if(pos_1.size()>0){
		m_pos1Buffer.bind();
		m_pos1Buffer.allocate(&(pos_1[0].x)/**/, 3 * pos_1.size()* sizeof( float ) );

		m_pos2Buffer.bind();
		m_pos2Buffer.allocate(&(pos_2[0].x)/**/, 3 * pos_2.size()* sizeof( float ) );

		if(hasColor){
			m_colorBuffer.bind();
			m_colorBuffer.allocate(&(color[0].x)/**/, 3 * color.size()* sizeof( float ) );
		}
		m_indexBuffer.bind();
		m_indexBuffer.allocate(/*idx*/&(indx[0]), indx.size()* sizeof( int ));
	}
	else{
		float dummy;
		m_pos1Buffer.bind();
		m_pos1Buffer.allocate(&dummy/**/, 0 );

		m_pos2Buffer.bind();
		m_pos2Buffer.allocate(&dummy/**/, 0);

		if(hasColor){
			m_colorBuffer.bind();
			m_colorBuffer.allocate(&dummy/**/, 0 );
		}
		m_indexBuffer.bind();
		int idummy;
		m_indexBuffer.allocate(/*idx*/&idummy, 0);
	}

}

bool glCorrespondences::isGlUpToDate()
{
	return isUpToDate;
}

void glCorrespondences::glDraw()
{
	if(indx.size()<=1){
		return;
	}
	m_shader.bind();
	m_vao.bind();
	m_indexBuffer.bind();

	glDebugging_didIDoWrong();

	glDrawElements(GL_POINTS,indx.size(), GL_UNSIGNED_INT, 0);
	if(glDebugging::didIDoWrong()){
		std::cout << "glError in " << __FILE__<< ", line " << __LINE__ << "\n Sizes: " << pos_1.size() << ", " << pos_2.size() << ", " << indx.size() << "\n";
	}

	m_vao.release();
	m_shader.release();
	//m_shader.disableAttributeArray("position");

	glDebugging_didIDoWrong();
}
