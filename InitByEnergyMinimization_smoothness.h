#pragma once
#include "Initializer.h"

class InitByEnergyMinimization_smoothness:
	public ComputeWeights
{
private:
	int numIt; float p1; float p2; float stepSize; int numHyp;
	float alpha_smoothness;
	explainMe::MatrixXf_rm d_dw_E, d_dw_E_smooth;

	//get rid of theses:
	//int first_frame, last_frame;
	int numFrames;

	std::vector<std::vector<int>>  N_start;
	std::vector<std::vector<int>>  N_end;

public:
	InitByEnergyMinimization_smoothness(int numIt, 
		float p1, float p2, 
		float stepSize, 
		int numHyp, int numFrames,
		float alpha_smoothness);
	virtual ~InitByEnergyMinimization_smoothness(void);

	virtual void findWeights(
		std::vector<trajectory_R> & traj_in, 
		explainMe::MatrixXf_rm & w_out);

	void getddwE(explainMe::MatrixXf_rm & target){
		target = d_dw_E;
	}

private:
	void findNeighborTrajectories(
		std::vector<trajectory_R> & trajs, 
		int k,
		std::vector<std::vector<int>> & N_start, 
		std::vector<std::vector<int>> & N_end, 
		int numClouds);

	void compute_d_dw_E_w_pow_p_tempSmoothness(std::vector<trajectory_R> & paths,
											std::vector<std::vector<int>> & N_start,std::vector<std::vector<int>> & N_end,
											explainMe::MatrixXf_rm &w,
											//T[i_f][i_w]
											std::vector<std::vector<Eigen::Matrix4f>> &T,
											int numFrames, int reference_frame,	
											explainMe::MatrixXf_rm & d_dw_E,
											float p,
											float alpha =0.1f);
};

