#include "nnByProjection.h"

template
class nnByProjection<6>;

template <int K>
distSqr_idx nnByProjection3D<K>::findNN(pxyz p, float maxR_sqr, float dummy){
	int i, j, index, best_index = -1; float distSqr, best_distSqr = maxR_sqr;
	//Eigen::Vector4f query;
	//query << p[0], p[1], p[2], 1;
	//query = input2QueryView * query;

	//d->project(query.x(), query.y(), query.z(), i, j);
	d->project(p.x, p.y, p.z, i, j);
//	if (!d->isCoordValid(i, j)){
//		return distSqr_idx(maxR_sqr, -2);
//	}


	//search small nbrhood
	//for (int di = -K; di <= K; di++){
		//for (int dj = -K; dj <= K; dj++){
			//indices one based..
	for (int di = -_k; di <= _k; di++){
		for (int dj = -_k; dj <= _k; dj++){
			if (d->isCoordValid(i + di, j + dj) && idx(i + di, j + dj) >= 1) {
				index = idx(i + di, j + dj) - 1;
				auto & q = cloud->at(index);
				distSqr = (q.x - p[0])*(q.x - p[0]) + (q.y - p[1])*(q.y - p[1]) + (q.z - p[2])*(q.z - p[2]);
				if (distSqr < best_distSqr){
					best_index = index;
					best_distSqr = distSqr;
				}
			}
		}
	}

	//best_index -1 if there where no valid pixels around..
	return distSqr_idx(best_distSqr, best_index);
}

template
class nnByProjection3D<3>;