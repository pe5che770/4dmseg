#include "Chronist.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <boost/iostreams/device/mapped_file.hpp>

#include <qdir.h>
#include "algorithmStateRenderer.h"
#include "StopWatch.h"
#include "CloudColorizer.h"
#include <boost/filesystem.hpp>

#include <opencv2/core.hpp>
#include <opencv2/core/version.hpp>

#if CV_MAJOR_VERSION > 2
#include <opencv2/imgcodecs.hpp>
#endif

Chronist::Chronist(explainMe_algorithm_withFeatures::Ptr alg){
	std::cout << "initting Chronist...";
	algToDocument = alg;
	auto d = alg->reader->getDescription();
	renderer = new algorithmStateRenderer(d->width(), d->height());//new demo_global_features();
	QMatrix4x4 frustum = d->frustum();
	renderer->setPreferredProjection(frustum,d->width(), d->height() );
	std::cout << "initted Chronist.\n";
}

Chronist::~Chronist(){
	delete renderer;
}

#include <cv.h>
#include <highgui.h>

void Chronist::document_seg_png_masks(const std::string & outputFolder)
{
	std::cout << "casting Segmentation files...\n";

	std::cout << "Initting alg...";

	CloudReader::Ptr reader = algToDocument->reader;
	boost::filesystem::path dir(outputFolder);
	boost::filesystem::create_directory(dir);

	auto & d = *reader->getDescription();
	for (int frame = 0; frame < algToDocument->denseSegmentation_allClouds.size(); frame++){
		auto & dense_seg = algToDocument->denseSegmentation_allClouds.at(frame);
		cv::Mat mat(d.height(), d.width(), CV_8U);
		for (int ii = 0; ii < d.height(); ii++) for (int jj = 0; jj < d.width(); jj++){
			mat.at<uint8_t>(ii, jj) = 0;
		}

		int i, j, seg;
		for (int idx = 0; idx < algToDocument->allClouds.at(frame)->size(); idx++){
			auto & p = algToDocument->allClouds.at(frame)->points[idx];
			d.project(p.x, p.y, p.z, i, j);
			dense_seg.row(idx).maxCoeff(&seg);
			mat.at<uint8_t>(j, i) = seg + 1; //1 based, 0 means no label.			
		}
		cv::imwrite(outputFolder + "/labeling_frame" + std::to_string(algToDocument->cloudIndex2ScannedFrameIndex(frame)) + ".png", mat);
	}
}

void Chronist::document_w_as_hyp_probs(const std::string &folder){
	renderer->clear();
	QVector3D zero_vector(0, 0, 0);
	QVector3D e_vector_z(0, 0, 1);
	renderer->setCamera(zero_vector, e_vector_z);

	for (int f = 0; f < algToDocument->numClouds(); f++)
	{
		renderer->setUpRenderingForCloud_wmax(
			algToDocument->allClouds[f],
			algToDocument->denseSegmentation_allClouds.at(f));

		renderer->recordImage(folder, std::string("seg") + "_frm" + std::to_string(f) + ".png");
	}
}

void Chronist::document_insecurityOfW(const std::string &folder){
	renderer->clear();
	QVector3D zero_vector(0, 0, 0);
	QVector3D e_vector_z(0, 0, 1);
	renderer->setCamera(zero_vector, e_vector_z);

	for (int f = 0; f < algToDocument->numClouds(); f++)
	{
		renderer->setUpRenderingForCloud_wsec_to_wmax(
			algToDocument->allClouds[f ],
			algToDocument->denseSegmentation_allClouds.at(f ));

		renderer->recordImage(folder, std::string("insec") + "_frm" + std::to_string(f) + ".png");
	}
}

void Chronist::document_denseWeights(const std::string & folder){
	renderer->clear();
	QVector3D zero_vector(0, 0, 0);
	QVector3D e_vector_z(0, 0, 1);
	renderer->setCamera(zero_vector, e_vector_z);

	for (int f = 0; f < algToDocument->numClouds(); f++)
	{
		renderer->setUpRenderingForCloud(
			algToDocument->allClouds[f],
			algToDocument->denseSegmentation_allClouds.at(f));

		renderer->recordImage(folder, std::string("seg") + "_frm" + std::to_string(f) + ".png");
	}
	std::cout << "chronist: *stored data, for generations to come, Sir-\n";
}

void Chronist::document_denseperPixelValues(const std::string & folder, std::vector<std::vector<float>>& vals, float scale){
	renderer->clear();
	QVector3D zero_vector(0, 0, 0);
	QVector3D e_vector_z(0, 0, 1);
	renderer->setCamera(zero_vector, e_vector_z);

	for (int f = 0; f < algToDocument->numClouds(); f++)
	{
		renderer->setUpRenderingForCloud(
			algToDocument->allClouds[f],
			vals.at(f), scale);

		renderer->recordImage(folder, std::string("perPix") + "_frm" + std::to_string(f) + ".png");
	}
	std::cout << "chronist: *stored data, for generations to come, Sir-\n";
}


void Chronist::document_alignmentSets(const std::string & folder)
{
	
	//render and save the segmentation of each frame.
	renderer->clear();
	QVector3D zero_vector(0, 0, 0);
	QVector3D e_vector_z(0, 0, 1);
	renderer->setCamera(zero_vector, e_vector_z);

	for (int hyp = 0; hyp < algToDocument->motions.numHypotheses(); hyp++){

		renderer->setUpRenderingForLines(algToDocument->motions.getAlignmentSet(hyp));
		renderer->recordImage(folder, std::string("/set_") + std::to_string(hyp) + ".png");
	}

	std::cout << "chronist: *stored data, for generations to come, Sir-\n";
}


void Chronist::document_alignmentSets_byMovingStuff(const std::string & folder)
{
	//render and save the segmentation of each frame.
	renderer->clear();
	QVector3D zero_vector(0, 0, 0);
	QVector3D e_vector_z(0, 0, 1);
	renderer->setCamera(zero_vector, e_vector_z);

	renderer->setUpRenderingForCloud(algToDocument->allClouds[explainMe_algorithm_withFeatures::REFERENCE_CLOUD]);
	for (int hyp = 0; hyp < algToDocument->motions.numHypotheses(); hyp++){
		int c = 0;
		for (auto & mo : algToDocument->motions.getAlignmentSet(hyp)){
			if (c == 0 || c== 1){
				std::cout << mo;
			}
			renderer->setCloudPosition(mo);
			renderer->recordImage(folder, std::string("/mo_") + std::to_string(hyp) + "_" + std::to_string(c) + ".png");
			c++;
		}
	}

	std::cout << "chronist: *stored data, for generations to come, Sir-\n";
}

void Chronist::document_alignmentSets_byMovingStuff(const std::string & folder, int hyp)
{
	//render and save the segmentation of each frame.
	renderer->clear();
	QVector3D zero_vector(0, 0, 0);
	QVector3D e_vector_z(0, 0, 1);
	renderer->setCamera(zero_vector, e_vector_z);

	renderer->setUpRenderingForCloud(algToDocument->allClouds[explainMe_algorithm_withFeatures::REFERENCE_CLOUD]);

		int c = 0;
		for (auto & mo : algToDocument->motions.getAlignmentSet(hyp)){
			if (c == 0 || c == 1){
				std::cout << mo;
			}
			renderer->setCloudPosition(mo);
			renderer->recordImage(folder, std::string("/mo_") + std::to_string(hyp) + "_" + std::to_string(c) + ".png");
			c++;
		}
	

	std::cout << "chronist: *stored data, for generations to come, Sir-\n";
}


QMatrix4x4 rotateAround(QVector3D & lookAt, float degree){
	QMatrix4x4 rot_pi_4; QMatrix4x4 translate, translate2;
	translate.setToIdentity();
	translate.translate(-lookAt);
	translate2.setToIdentity();
	translate2.translate(lookAt);
	rot_pi_4.setToIdentity();
	rot_pi_4.rotate(degree, 0, 1, 0);//who does use degrees?
	rot_pi_4 = translate2 *rot_pi_4 * translate;
	return rot_pi_4;
}

void Chronist::document_weight(const std::string & folder, int hyp){
	//document densities
	renderer->clear();
	QVector3D zero_vector(0, 0, 0);
	QVector3D e_vector_z(0, 0, 1);
	renderer->setCamera(zero_vector, e_vector_z);

	explainMe::MatrixXf_rm mat;

	for (int f = 0; f < algToDocument->numClouds(); f++){
		renderer->setUpRenderingForCloud_log_wi(algToDocument->allClouds[f], algToDocument->denseSegmentation_allClouds[f], hyp);
		renderer->recordImage(folder, "hyp_" + std::to_string(hyp) + std::string("_frm") + std::to_string(f ) + ".png");

	}
}


explainMe_algorithm_withFeatures::RowVectorXf  computeIt(){
	std::cout << "yaya, gotta do the dwdt data gen somewhow\n";
	return explainMe_algorithm_withFeatures::RowVectorXf();
}

void Chronist::dump_dw_dt_stats_in_m_File(const std::string & folder,
	std::vector<explainMe_algorithm_withFeatures::RowVectorXf> & supports,
	std::vector<explainMe_algorithm_withFeatures::RowVectorXf> & shift_abs,
	std::vector<Eigen::MatrixXf> & shift_mats){
	std::ofstream m_file;
	m_file.open(folder + "/dwdt_stats.m");
	m_file << "shift_abs = [";
	for (int i = 0; i < shift_abs.size(); i++){
		m_file << i << "\t" << shift_abs[i] << (i == shift_abs.size() ? "\n" : ";\n");
	}
	m_file << "];\n";
	m_file << "supports = [";
	for (int i = 0; i < supports.size(); i++){
		m_file << supports[i] << (i == supports.size() ? "\n" : ";\n");
	}
	m_file << "];\n";

	m_file << "shift_mat = {";
	for (int i = 0; i < shift_mats.size(); i++){
		m_file << "[";
		for (int j = 0; j < shift_mats[i].rows(); j++){
			m_file << shift_mats[i].row(j) << (j == shift_mats[i].rows() ? "\n" : ";\n");
		}
		m_file << "]" << (i == shift_mats.size() ? "" : ",\n");
	}
	m_file << "};\n";

	m_file << "cols = {";
	for (int i = 0; i < algToDocument->w.cols(); i++){
		float r, g, b;
		CloudColorizer::colorWheel(i, r, g, b);
		m_file << "[" << r << "," << g << "," << b << "],\n";
	}
	m_file << "};\n";

	m_file.close();
}

void Chronist::document_dw_dt(const std::string & folder)
{
	/*explainMe_algorithm_withFeatures::MatrixXf_rm res;
	CATCH_AND_RETHROW( renderer->alg->compute_dWdT_via_nn(res ,3);, ;)
	std::cout << "Folder: " << folder << "\n";
	*/
	//renderer->win->getGLWidget()->resize(1200,800);


	explainMe_algorithm_withFeatures::RowVectorXf frame_shit_abs, frame_support;
	//render and save the segmentation of each frame.
	QVector3D zero_vector(0, 0, 0);
	QVector3D e_vector_z(0, 0, 1);
	renderer->setCamera(zero_vector, e_vector_z);
	renderer->clear();
	std::cout << "dwdt_pass, sum of vals\n dwdt_stats = [";
	std::vector<explainMe_algorithm_withFeatures::RowVectorXf> shift_abs, supports;
	std::vector<Eigen::MatrixXf> shift_mats;
	for (int pass = 0; pass < algToDocument->numClouds(); pass++){
		//display segmentation of frame as solver for via graphcut: interpolated
		//CATCH_AND_RETHROW( renderer->display_w_by_colors(res,pass);,;);
		Eigen::MatrixXf shift_mat;
		//frame_shit_abs = renderer->display_dwdt(pass, shift_mat, frame_support);
		frame_shit_abs = computeIt();
		renderer->setUpRenderingForCloud();
		renderer->recordImage(folder, std::string("/dwdtC_" + std::to_string(pass)) + ".png");
		

		//further doc.
		shift_abs.push_back(frame_shit_abs);
		shift_mats.push_back(shift_mat);
		supports.push_back(frame_support);
		//std::cout << frame_stats <<(pass == renderer->alg->last_frame ? "\n":";\n");
	}

	dump_dw_dt_stats_in_m_File(folder, supports, shift_abs, shift_mats);
	
	std::cout << "\nchronist: *stored data, for generations to come, Sir-\n";
}

void Chronist::store(std::string filename, std::vector<motion> & alignments)
{
	if(alignments.size() == 0){
		return;
	}
	std::ofstream file (filename);
	if (!file.is_open ())
	{
		std::cerr << "Error sotring motion file at " << filename.c_str () << "\n";
		return;
	}


	size_t num_frames = alignments[0].size();
	file << num_frames << std::endl;

	size_t num_objects = alignments.size();
	file << num_objects << std::endl;

	std::cout << "Motion file has frames and objects." << num_frames  << num_objects << "\n";
	for (size_t o_i = 0; o_i < alignments.size(); ++o_i)
	{
		std::string obj_name = std::to_string(o_i);
		file << obj_name << std::endl;
		    
		
		for (size_t f_i = 0; f_i < alignments[o_i].size(); ++f_i)
		{
			for (size_t i = 0; i < 4; ++i)
			for (size_t j = 0; j < 4; ++j)
				file << alignments[o_i][f_i] (j, i) << std::endl;

		}
	}
	file.close();
}
bool Chronist::load(std::string filename, std::vector<std::vector<Eigen::Matrix4f>> & motion)
{
  std::ifstream file (filename);
  if (!file.is_open ())
  {
	 std::cerr << "Error reading motion file at " << filename.c_str () << "\n";
    return (false);
  }

  int num_frames;
  file >> num_frames;

  int num_objects;
  file >> num_objects;

  std::cout << "Motion file has frames and objects." << num_frames  << num_objects << "\n";
  for (size_t o_i = 0; o_i < num_objects; ++o_i)
  {
    std::string obj_name;
    file >> obj_name;
    obj_name = obj_name.substr (1);
    

    std::vector<Eigen::Matrix4f> transforms (num_frames);
    for (size_t f_i = 0; f_i < num_frames; ++f_i)
    {
      for (size_t i = 0; i < 4; ++i)
        for (size_t j = 0; j < 4; ++j)
          file >> transforms[f_i] (j, i);

    }

	motion.push_back(transforms);
  }
  file.close();
  return (true);
}

void Chronist::store(std::string file,std::vector<trajectory_R> & traj, explainMe::MatrixXf_rm & w)
{
	//simple file format:
	//num traj, dim weights
	//first_frame  last_frame
	//weight.x1, weight.x2,...
	//p0.x p0.y p0.z p0.nx p0.ny p0.nz  p1.x p1.y p1.z etc...
	//so each traj is stored in a line triplet.
	std::cout << "Storing init data to file " << file << "\n";
	std::ofstream myfile;
	myfile.open (file);

	myfile << traj.size() << " " << w.cols() << "\n";
	for(int i = 0; i < traj.size(); i++){
		myfile << traj[i].firstCloud() << " " << traj[i].seedCloud() << " " << traj[i].lastCloud() << " " << traj[i].originalPointIdx() << "\n";
		myfile << w.row(i) << "\n";
		for(int j = traj[i].firstCloud(); j <= traj[i].lastCloud(); j++){
			myfile << traj[i][j].x << " " << traj[i][j].y << " " << traj[i][j].z << " "
				<< traj[i][j].normal_x << " " << traj[i][j].normal_y << " " << traj[i][j].normal_z << " ";
		}
		myfile << "\n";

	}
	myfile.close();
}

void Chronist::load(std::string file, std::vector<trajectory_R> & target_traj, explainMe::MatrixXf_rm & target_w)
{
	std::ifstream myFile;
	myFile.open(file);
	
	std::string line, line1, line2;

	//allow floats, bools and string parameters
	//float parameter names start with 'f'
	//string parameter names start with 's'
	//bool parameters start with 'b'
	//comment lines with #
	boost::char_separator<char> sep(" \t");

	int trajs, hyps, first_frame, seed_cloud, last_frame, pointidx;
	std::vector<float> w_i, p_i;
	std::cout << "Loading trajectories...";
	StopWatch loadtime;
	loadtime.restart();
	if(myFile.is_open()){
		if(std::getline(myFile, line)){
			boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
			auto it = tokens.begin();
			
			trajs = boost::lexical_cast<int>(*it); it++;
			hyps = boost::lexical_cast<int>(*it); it++;

			target_w.resize(trajs, hyps);
			target_traj.clear();
			target_traj.reserve(trajs);
			std::cout << "(" << trajs << ")\n";
		}
		else{
			std::cout << "Error loading File " << file << " in " << __FILE__ << " invalid header";
			return;
		}
		while(std::getline(myFile, line) && std::getline(myFile, line1) && std::getline(myFile, line2) )
		{
			boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
			auto it = tokens.begin();
					
			first_frame = boost::lexical_cast<int>(*it); it++;
			seed_cloud = boost::lexical_cast<int>(*it); it++;
			last_frame = boost::lexical_cast<int>(*it); it++;
			pointidx = boost::lexical_cast<int>(*it); it++;

			//seg weights
			tokens = boost::tokenizer<boost::char_separator<char>>(line1, sep);
			it = tokens.begin();
			w_i.clear();
			while(it != tokens.end())
			{
				w_i.push_back(boost::lexical_cast<float>(*it));
				it++;
			}

			//trajectory
			tokens = boost::tokenizer<boost::char_separator<char>>(line2, sep);
			it = tokens.begin();
			p_i.clear();
			while(it != tokens.end())
			{
				p_i.push_back(boost::lexical_cast<float>(*it));
				it++;
			}


			//check for well formedness:
			if(w_i.size() != hyps || p_i.size() != 6*(last_frame - first_frame +1) ){
				std::cout <<  "Error loading File " << file << " in " << __FILE__ << " invalid line: \n";
				std::cout << line << "\n" << line1 << "\n" << line2 << "\n";
				throw std::runtime_error(std::string("error loading file " ) + file + "in" + __FILE__);
			}
			else{
				//irrelevant for speed.
				pcl::PointXYZINormal p_new;
				
				trajectory_R t(p_new, seed_cloud, pointidx);
				t.resize(first_frame,last_frame);
				for(int i = 0; i < p_i.size() /6; i++){
					t[i+first_frame].getVector3fMap()
						<< p_i[i*6 + 0], p_i[i*6 +1], p_i[i*6 +2];
					t[i+first_frame].getNormalVector3fMap()
						<< p_i[i*6 +3], p_i[i*6 +4], p_i[i*6 +5];
				}

				for(int i = 0; i < hyps; i++){
					target_w(target_traj.size(),i) = w_i.at(i);
				}
				
				target_traj.push_back(t);
				
			}
			if(target_traj.size()%10000 == 0){
				std::cout << std::setprecision(3) << target_traj.size() * 100.0/trajs << " %        ";
				std::cout << "\r";
			}

		}
	}
	loadtime.stop();
	std::cout <<std::setprecision(9)<< "Loading trajectories done in " << loadtime.total() << " ms\n";
}


void Chronist::store_v2(
		std::string folder,
		std::string file,
		std::vector<trajectory_R> & target_traj,
		explainMe::MatrixXf_rm & target_seg_weights,
		std::vector</*std::vector<Eigen::Matrix4f>*/motion> & alignments)
{
	boost::filesystem::path dir = boost::filesystem::path(folder);
	boost::filesystem::create_directory(dir);
	//std::cout << "Storing motion to file " << folder + "/" +file + ".mo2" << "\n";
	store(folder + "/" + file + ".mo2", alignments);
	std::ofstream myfile;
	myfile.open(folder + "/" + file + ".seg2");
	myfile << "traj2File " << target_traj.size() << " " << target_seg_weights.cols()<< "\n";
	int label;
	for (int i = 0; i < target_traj.size(); i++){
		auto & t = target_traj[i];
		target_seg_weights.row(i).maxCoeff(&label);
		auto & seed = t.seedEl();
		myfile << t.firstCloud() << " " << t.seedCloud() << " " << t.lastCloud() << " " << label << " " << t.originalPointIdx() << " "
			<< seed.x << " " << seed.y << " " << seed.z << " "  
			<< seed.normal_x << " " << seed.normal_y << " " << seed.normal_z << "\n";
	}
	myfile.close();
		
}

void Chronist::load_v2(const std::string & segfile, const std::string & mofile, std::vector<trajectory_R> & target_traj, explainMe::MatrixXf_rm & target_seg_weights, std::vector<std::vector<Eigen::Matrix4f>> & alignments){
	load(mofile, alignments);


	boost::iostreams::mapped_file mmap(segfile);

	auto f = mmap.const_data();
	auto l = f + mmap.size();
	int numlines = 0, numTrajs, numLabels;
	int numScanned, firstF, seedCloud, lastF, label, pointIdx;
	pcl::PointXYZINormal p;
	
	char line[512];
	//header.
	std::cout << "Reading header...\n";
	if (f && f != l){
		std::sscanf(f,"traj2File %d %d", &numTrajs, &numLabels);
		std::cout << "seg2 file declares: " << numTrajs << " " << numLabels << "\n";
		
		f = static_cast<const char*>(memchr(f, '\n', l - f));
		f++;
	}
	else{
		std::cout << "error while parsing header!";
		return;
	}
	//checking wellformedness, filling in weights.
	if (numLabels > alignments.size()){
		std::cout << "Error when reading mo2 and seg2 files: too many labels for alignment sets!\n";
		exit(1);
		return;
	}

	if (alignments.size() > target_seg_weights.cols()){
		std::cout << "**** Warning: during loading: more alignments than labels, initializing additional zero labels\n";
		numLabels = alignments.size();
	}

	target_traj.clear();
	target_traj.reserve(numTrajs);
	target_seg_weights.resize(numTrajs, numLabels);
	target_seg_weights.setConstant(0);

	numlines = 0;
	while (f && f != l)
	{
		f = static_cast<const char*>(memchr(f, '\n',(l - f)));
		if (f){
			std::memcpy(line, f, std::min<size_t>(l - f, 512l));
			line[511] = '\0';
			numScanned = std::sscanf(line, "%d %d %d %d %d %f %f %f %f %f %f",
			                         &firstF, &seedCloud, &lastF, &label, &pointIdx,
			                         &(p.x), &(p.y), &(p.z),
			                         &(p.normal_x), &(p.normal_y), &(p.normal_z));
			//numScanned = std::sscanf(f, "%d %d %d %d %f %f %f %f %f %f", &firstF, &seedF, &lastF, &label,
			//	&(p.x), &(p.y), &(p.z), &(p.normal_x), &(p.normal_y), &(p.normal_z));
			if (numScanned == 10){
				target_traj.push_back(trajectory_R(p, seedCloud, pointIdx));
				target_traj.back().resize(firstF, lastF, p);
				target_seg_weights(numlines, label) = 1;
				numlines++;
			}
			f++;
		}
		if (numlines % 100000 == 0){
			std::cout << std::setprecision(3) << numlines * 100.0 / numTrajs << " %        ";
			std::cout << "\r";
		}
	}

	std::cout << "100 %            \n";

	if (numlines != numTrajs){
		std::cout << "Error: Inconsistent traj2 Header! Declared " << numTrajs << " trajectories, found " << numlines << "\n";
	}
	//, generetating trajectories
	//prepare ainvs.
	std::vector<std::vector<Eigen::Matrix4f>> a_inv = alignments;
	for (int i = 0; i < alignments.size(); i++) for (int j = 0; j < alignments[i].size(); j++){
		a_inv[i][j] = alignments[i][j].inverse();
	}
	//computing the trajectories.
	for (int i = 0; i < target_traj.size(); i++){
		//std::cout << i << "\n";
		auto & t = target_traj[i];
		auto & seed = t.seedEl();
		seedCloud = t.seedCloud();
		target_seg_weights.row(i).maxCoeff(&label);
		for (int cld = t.firstCloud(); cld <= t.lastCloud(); cld++){
			auto & q = t[cld];
			
			q.getVector4fMap() = alignments[label][cld] * a_inv[label][seedCloud] * q.getVector4fMap();
			q.getNormalVector3fMap() =
				alignments[label][cld].topLeftCorner<3, 3>() *
					(a_inv[label][seedCloud].topLeftCorner<3, 3>() * q.getNormalVector3fMap() +
					a_inv[label][seedCloud].topRightCorner<3, 1>())
					+ alignments[label][cld].topRightCorner<3, 1>();
		}
	}

}