#include "trackBallListener.h"
#include <math.h>
#include <assert.h>
#include <iostream>
#include "sceneManager.h"

trackBallListener::trackBallListener(QGLWidget * display, sceneManager & manager): QObject(display),
	scene(manager)
{
	this->displyer = display;
	z_translation = 0;
}

trackBallListener::~trackBallListener(void)
{
}

void trackBallListener::onMouseMove( QMouseEvent* event)
{
	float x,y,z;
	float min = (displyer->width() > displyer->height()? displyer->height(): displyer->width());
	x= ((0.f -displyer->width())/2 +event->x())/(min/2);
	y = ((0.f +displyer->height())/2 - event->y())/(min/2);
	z = 1-x*x -y*y;
	z = (z < 0? 0: z);
	z = sqrtf(z);
	assert(z==z);
	float nrm = x*x+y*y+z*z;
	nrm = (nrm < 0? 0: nrm);
	nrm = sqrtf(nrm);
	assert(nrm == nrm);
	x= x/nrm;
	y= y/nrm;
	z= z/nrm;

	QMatrix4x4 c = scene.getCam();
	QMatrix4x4 c_inv = c.inverted();

	if(event->buttons() & Qt::LeftButton){
		
		float axisx=y*lastz-lasty*z,axisy=z*lastx-lastz*x,axisz=x*lasty-y*lastx;
		float axisnrm = axisx*axisx+axisy*axisy+axisz*axisz;
		axisnrm = (axisnrm< 0 ? 0: axisnrm);
		axisnrm = sqrtf(axisnrm);
		assert(axisnrm == axisnrm);
		float cos = x*lastx+y*lasty+z*lastz;
		cos = (cos<-1.f?-1.f :(cos>1?1.f:cos));

		float angle = acos(cos);
		assert(angle == angle);

		if(axisnrm < 0.001){
			return;
		}
		assert(axisnrm > 0);
	
		QMatrix4x4 rot;
		QVector4D axis(axisx/axisnrm,axisy/axisnrm,axisz/axisnrm,0);
		axis = c_inv * axis;

		rot.setToIdentity();
		rot.rotate(-angle/3.14159*180,
			axis.x(),
			axis.y(),
			axis.z());
		/*rot.rotate(-angle/3.14159*180,
			axisx/axisnrm,
			axisy/axisnrm,
			axisz/axisnrm);*/

		/*QMatrix4x4 trans;
		trans.setToIdentity();
		trans.translate(0,0,-z_translation);
		rot = rot * trans;
		trans.setToIdentity();
		trans.translate(0,0,+z_translation);
		rot = trans * rot;*/

		Q_EMIT rotation(rot);

		//displyer->updateGL();
		displyer->update();

		lastx = x;
		lasty=y;
		lastz=z;
	}

	else if(event->buttons() & Qt::MidButton){
		QVector4D delta(x-lastx,y-lasty,0,0);
		delta = c_inv*delta;

		QMatrix4x4 rot;
		rot.setToIdentity();
		//rot.translate(x-lastx,y-lasty,0);
		rot.translate(delta.x(),delta.y(),0);
		Q_EMIT rotation(rot);
		//displyer->updateGL();
		displyer->update();

		lastx = x;
		lasty=y;
		lastz=z;
	}
}

void trackBallListener::onMousePress( QMouseEvent* event )
{
	float min = (displyer->width() > displyer->height()? displyer->height(): displyer->width());
	lastx = ((0.f -displyer->width())/2+event->x()) /(min/2);
	lasty = ((0.f +displyer->height())/2 -event->y()) / (min/2);

	lastz = 1-lastx*lastx -lasty*lasty;
	lastz = (lastz < 0? 0: lastz);
	lastz = sqrtf(lastz); 

	float nrm = lastx*lastx + lasty*lasty+ lastz*lastz;
	nrm = sqrtf(nrm);
	lastx=lastx/nrm;
	lasty=lasty/nrm;
	lastz=lastz/nrm;
}

void trackBallListener::onMouseWheel( QWheelEvent * event )
{
	
	QMatrix4x4 trans;
	trans.setToIdentity();
	trans.translate(0,0,event->delta()*0.0005);
	z_translation += event->delta()*0.0005;
	Q_EMIT camMovement(trans);
	//displyer->updateGL();
	displyer->update();
}
