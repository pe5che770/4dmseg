#include "gpu_sparse2denseInterpolation.h"
#include "cudaDataterm.h"
#include "cudaDatatermTools.h"
#include "StopWatch.h"

gpuInterpolation::gpuInterpolation(explainMe_algorithm_withFeatures::Ptr algo)
{
	alg = algo;
}


gpuInterpolation::~gpuInterpolation()
{

}

void gpuInterpolation::prepareInterpolation(cudaDataterm * cuda_dev,
	explainMe::MatrixXf_rm & sparse_weights)
{

	cudaDatatermTools::setGPUSparsePoints(cuda_dev, alg->trajectories);
	cudaDatatermTools::setGpuMotion(cuda_dev, alg->motions);
	cuda_dev->initSparseValues(sparse_weights.cols());

	buffer_sprs.resize(sparse_weights.rows());
	for (int hyp = 0; hyp < sparse_weights.cols(); hyp++){
		for(int el = 0; el < sparse_weights.rows(); el++){
			buffer_sprs[el] = sparse_weights(el, hyp);/*(sparse_weights(el, hyp) > 0.5 ? 1 : 1e-5); //*/  
			//buffer_sprs[el] = el
		}
		cuda_dev->setSparseValues(buffer_sprs, hyp);
	}
	
	//need labelsto map samples into some frame
	cuda_dev->updateLabelsForInterpolationFormMultipleFrames();
	
}

void gpuInterpolation::doInterpolation(cudaDataterm * cuda_dev,
	int frame,
	explainMe::MatrixXf_rm & out)
{
	int numHyp = alg->motions.numHypotheses();
	
	if (out.cols() != numHyp){
		out.resize(alg->allClouds[frame]->size(), numHyp);
	}

	//std::vector<float> denseVals_grid;
	//for (int frame = 0; frame < alg->allClouds.size(); frame++){
	//	std::cout << "+";
		
		//works
		auto & sp_crf_result = cuda_dev->filter_sparse2dense_spatial_crf_oneFrame(frame, 0.02f, 0, alg->params.crf_sigma_sp_factor, false); //rename to interpolate

		//works too
		//auto & sp_crf_result = cuda_dev->interpolate_sparse2dense_oneFrame_fromMultipleFrames(frame, 0.01f, 10); //rename to interpolate
		
		for (int hyp = 0; hyp < numHyp; hyp++){
			sp_crf_result.getAndNormalize(hyp, buffer, false); //rename to getInterpolated
			//crf object returns per grid values but the values are only stored for valid non nan points---
			//So fill it in
			int idx = 0, out_idx = 0;
			for (auto it = alg->allClouds_asGrids[frame]->begin(); it != alg->allClouds_asGrids[frame]->end(); it++, idx++)
			{
				if (it->z != 0 && it->z * 0 == 0 && it->getNormalVector3fMap().norm() > 0.99){
					out(out_idx, hyp) = buffer[idx];
					out_idx++;
				}
			}
		}
		
		for (int i = 0; i < out.rows(); i++){
			out.row(i) /= (out.row(i).sum()+ 1e-10);
		}

	//}

}
