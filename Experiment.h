#pragma once
#include <qobject.h>
#include <string>
#include "explainMe_algorithm_withFeatures.h"
#include "CloudReader.h"
#include "statsKeeper.h"
#include "statsWindow.h"
#include <QApplication>
#include "Chronist.h"
#include "algorithmParameters.h"

class demo_global_features;
class cudaDataterm;


class Experiment:public QObject{
	Q_OBJECT;
private:
	experimentParameters myParams;
	explainMe_algorithm_withFeatures::Ptr alg;
	cudaDataterm * gpuDatatermComp;


	//output
	std::string outputFolder;;

	//timing
	statsKeeper * mystats;
	statsKeeper dummyStats;

	float last_error, current_error;
	int last_numHyps, current_numHyps;

	//graceful exit
	static bool abortExperiment;
public:
	typedef boost::shared_ptr<Experiment> Ptr;
	Experiment(const std::string & outputFolder, experimentParameters & params, explainMe_algorithm_withFeatures::Ptr alg);
	~Experiment(void);

	static void abort(){
		abortExperiment = true;
	}

	static bool wasaborted(){
		return abortExperiment;
	}

	//void loadFromFile(std::string file);
	void setOutputFolder(std::string folder){
		outputFolder = folder;
	}
	
	void setStats(statsKeeper & stats_keeper){ 
		mystats = &stats_keeper; 
	}


	void step5_graphcut_withDetailedDocumentation(
		explainMe_algorithm_withFeatures::Ptr alg,
		int step, 
		explainMe_algorithm_withFeatures::MatrixXf_rm & hypothesis_probability, 
		statsKeeper * stats);
	
	std::vector<std::vector<std::vector<float>>> step5_crf_withDocumentation(
		explainMe_algorithm_withFeatures::Ptr alg,
		int step,
		float kill_threshold,
		explainMe_algorithm_withFeatures::MatrixXf_rm & hypothesis_probability,
		statsKeeper * stats	,
		bool addErrorbasedProposals);

	//to experiment around with timing.
	void step5_graphcut_gpudterm_withDoc( //->graphcut_dterm_on_gpu
		explainMe_algorithm_withFeatures::Ptr alg,
		int step,
		float kill_threshold,
		explainMe_algorithm_withFeatures::MatrixXf_rm & hypothesis_probability,
		statsKeeper * stats);


	void do_gc_based_EM_iteration(int mainStep);
	void do_crf_based_EM_iteration(int mainStep);

	//=>document_dw_dt
	//void document_dw_dt(explainMe_algorithm_withFeatures::Ptr alg, std::string  name_suffix = "", bool seeds_only = false);
	
	//push to the chronist?
	void document_motions(explainMe_algorithm_withFeatures::Ptr alg, std::string folder);
	void document_motion_error(explainMe_algorithm_withFeatures::Ptr alg, std::string suffix);
	void documentMotionQuality(MotionHypotheses & hyps, std::vector<std::vector<float>> & doc);
	bool read_motion_file(const std::string & motion_file, std::vector<std::vector<Eigen::Matrix4f>> & target);
private:
	void documentEnergy(explainMe_algorithm_withFeatures::Ptr alg, std::vector<float> & target, stats & timer);
	void document_alignments_metaInformation(std::string folder, int it);
public:
	//run the experiment!
	void run();

public Q_SLOTS:
	void run_with_stats();
Q_SIGNALS:
	void document_seg_png(const std::string & folder);
	void document_alignments(const std::string & folder);
	void document_alignments_fancy(const std::string &);
	void document_weights(const std::string & folder);
	void document_w_i(const std::string & folder, int i);
	void document_w_as_hyp_probs(const std::string & folder);
	void document_dw_dt(const std::string & folder);
	void document_per_pixel_values(const std::string &, std::vector<std::vector<float>> &, float scale);
//	void document_densities(const std::string & folder, std::vector<densityMap> & maps);
//	void document_densities_per_frame(const std::string & folder, densityMap & map, int hyp);
	void document_stats_png(const std::string & folder, const std::string & file_png);
	void document_weights_insecurity(const std::string & folder);
};


class QExperimentApplication : public QApplication
{
	Q_OBJECT;
	//Will have an Experiment, a Chronist and a statswindow.
	explainMe_algorithm_withFeatures::Ptr alg;
	Experiment::Ptr experiment;
	QFutureWatcher<void> experiment_returns;
	Chronist::Ptr chronist;

	//commandline arguments
	std::map<std::string, std::string> cmd_line_str;
	std::map<std::string, bool> cmd_line_bool;

public:
	QExperimentApplication(int argc, char ** arg);

	~QExperimentApplication(){
		std::cout << "Qapplication being killed...";
	}

	//reimplemented to intercept excpetions and cout them before throwing them further.
	bool notify(QObject *receiver, QEvent *event);

public Q_SLOTS:
	void closeAllWindowsIfExperimentWasAborted(){
		if (Experiment::wasaborted()){
			closeAllWindows();
		}
	}
private:
	//sets up ctrl-c handling, else error messages are issued upon ctrl-c
	void catchInterruptSignals(const std::vector<int>& quitSignals,
		const std::vector<int>& ignoreSignals = std::vector<int>());

	void parse(int argc, char ** arg);

};

