#pragma once
#include "explainMe_algorithm_withFeatures.h"
#include "cudaDataterm.h"


class cudaDatatermTools{
public:
	static void setGpuMotion(cudaDataterm * gpuDatatermComp, MotionHypotheses & motions){
		std::vector<std::vector<Eigen::Matrix4f>> all_al_inv = motions.getAlignmentSetsInv();
		std::vector<std::vector<float*>> a, a_inv;
		for (int i = 0; i < motions.numHypotheses(); i++)
		{
			auto & al = motions.getAlignmentSet(i);
			auto & al_inv = all_al_inv.at(i);
			a.push_back(std::vector<float*>(al.size()));
			a_inv.push_back(std::vector<float*>(al.size()));
			for (int j = 0; j < al.size(); j++){
				a[i][j] = al[j].data();
				a_inv[i][j] = al_inv[j].data();
			}
		}
		gpuDatatermComp->setMotionSet(a, a_inv);
	}

	static void setGPUSparsePoints(
		cudaDataterm * gpuDatatermComp,
		std::vector<pcl::PointXYZINormal> & seed_points,
		std::vector<int> & seed_cloud_idx){
		std::vector<pxyz_normal_frame> allQueries(seed_points.size());
		for (int i = 0; i < seed_points.size(); i++){
			allQueries[i].frame = seed_cloud_idx[i];
			allQueries[i].idx = i;
			allQueries[i].x = seed_points[i].x;
			allQueries[i].y = seed_points[i].y;
			allQueries[i].z = seed_points[i].z;
			allQueries[i].nx = seed_points[i].normal_x;
			allQueries[i].ny = seed_points[i].normal_y;
			allQueries[i].nz = seed_points[i].normal_z;
		}
		gpuDatatermComp->setSparsePoints(allQueries);
	}

	static void setGPUSparsePoints(
		cudaDataterm * gpuDatatermComp,
		std::vector<trajectory_R> & trajs){
		std::vector<pxyz_normal_frame> allQueries(trajs.size());
		for (int i = 0; i < trajs.size(); i++){
			allQueries[i].frame = trajs[i].seedCloud();
			allQueries[i].idx = i;
			auto & p = trajs[i].seedEl();
			allQueries[i].x = p.x;
			allQueries[i].y = p.y;
			allQueries[i].z = p.z;
			allQueries[i].nx = p.normal_x;
			allQueries[i].ny = p.normal_y;
			allQueries[i].nz = p.normal_z;
		}
		gpuDatatermComp->setSparsePoints(allQueries);
	}
};