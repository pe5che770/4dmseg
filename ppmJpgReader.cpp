#include "ppmJpgReader.h"
#include "fileReader.h"

#include <opencv2/opencv.hpp>
#include <stdint.h>
#include <limits>
#include "cudaTool.h"

ppmJpgReader::ppmJpgReader(std::string & folder):
cloud(new explainMe::Cloud())
{
	dataFolder = folder;
	std::cout << folder << "\n";
	fileReader::listFiles(folder, ".*_depth\\.pgm", depth_ppm);
	fileReader::listFiles(folder, ".*_rgb\\.jpg", color_jpg);

	std::cout << "loading pgm/jpg files; found " << depth_ppm.size() << " pgms and " << color_jpg.size() << "jpgs.\n";

	float fx = 563.2; //575.8;
	float fy = 562.2; //575.8;
	float cx = 320.49; //319.5;
	float cy = 244.7; //239.5;
	float width = 640;
	float height = 480;
	float near = 0.1f;
	d = new RangeSensorDescription(width,height,(-cx)/fx*near,(width-cx)/fx*near,(-cy)/fy*near,(height-cy)/fy*near,near,100.f);
	cuda = new cudaTool(d->descriptionData());
	cloud->resize(width*height);
	cloud->width = width;
	cloud->height = height;
	
}


ppmJpgReader::~ppmJpgReader()
{
	delete d;
	delete cuda;
}

bool ppmJpgReader::loadFrame(int frame, bool cleanUp, float maxDist, bool bilateralFilter, float sig_z){
	if (frame >= numberOfFrames()){
		return false;
	}

	cv::Mat depth = cv::imread(dataFolder + "/" + depth_ppm[frame], CV_LOAD_IMAGE_ANYDEPTH);
	cv::Mat rgb = cv::imread(dataFolder + "/" + color_jpg[frame]);


	int width = depth.cols;
	int numPx = depth.cols*depth.rows;
	std::cout << "numPx " << numPx << "\n";
	//ensure there is enough space allocated
	depthFloatBuffer.resize(numPx);
	normalFloatBuffer.resize(numPx * 3);

	for (int i = 0; i < depth.cols; i++) for (int j = 0; j< depth.rows; j++){
		depthFloatBuffer[j*depth.cols + i] = depth.at<uint16_t>(j, i) / 1000.f;
	}
	cuda->dev_setData(&depthFloatBuffer[0]);
	
	if (bilateralFilter)
	{
		cuda->bilateralFilter(3, sig_z);
		cuda->dev_getData(&depthFloatBuffer[0]);
	}
	else{
		cuda->bilateralFilter();
	}
	cuda->computeNormals(false);
	cuda->dev_getNormals(&normalFloatBuffer[0]);
	float qnan = std::numeric_limits<float>::quiet_NaN();
	for (int i = 0; i < depth.cols; i++){
		for (int j = 0; j< depth.rows; j++){
			auto & p = cloud->at(i, j);
			p.normal_x = -normalFloatBuffer[j*depth.cols + i];
			p.normal_y = -normalFloatBuffer[j*depth.cols + i + numPx];
			p.normal_z = -normalFloatBuffer[j*depth.cols + i + numPx * 2];

			if (i < 225 || i > 450|| depth.at<uint16_t>(j, i) == 0 && !(p.getNormalVector3fMap().squaredNorm() > 0.5)){
				p.getVector3fMap() << qnan, qnan, qnan;
				p.getNormalVector3fMap() << qnan, qnan, qnan;
			}
			else{
				if (bilateralFilter){
					p.z = depthFloatBuffer[j*depth.cols + i];
				}
				else{
					p.z = depth.at<uint16_t>(j, i) / 1000.f;
				}
				if (p.z > maxDist){
					p.getVector3fMap() << qnan, qnan, qnan;
					p.getNormalVector3fMap() << qnan, qnan, qnan;
				}
				d->unproject(i, j, p.z, p.x, p.y);
			}
			
			p.r = rgb.at<cv::Vec3b>(j, i)[2];
			p.g = rgb.at<cv::Vec3b>(j, i)[1];
			p.b = rgb.at<cv::Vec3b>(j, i)[0];
		}
	}

	return true;
}
