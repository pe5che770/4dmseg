#pragma once

#include <string>
#include "explainMeDefinitions.h"
#include "Sensors.h"
#include <vector>
#include "CloudReader.h"


#ifndef _COMPILE_NO_CUDA_
	class cudaTool;
#endif

class pcdReader:public CloudReader
{
	RangeSensorDescription * sensorDescription;
	std::string folder;
	std::vector<std::string> files;

	explainMe::Cloud::Ptr cloud;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_in;

	std::vector<float> depthBuff, normalBuff;
#ifndef _COMPILE_NO_CUDA_
	cudaTool * cuda;
#endif

public:
	enum sensorType{ ASUS_XTION, VIRTUAL_SCANNER, DONT_CARE};
	pcdReader(std::string folder, sensorType t = DONT_CARE);
	virtual ~pcdReader(void);

	virtual RangeSensorDescription * getDescription(){
		return sensorDescription;
	}

	virtual size_t numberOfFrames(){
		return files.size();
	}

	virtual bool loadFrame(int frame, bool cleanUp, float maxDistAllowed, bool bilateralFilter, float sig_z);
	bool loadFrame_withoutNormals(int frame, bool cleanUp);
	
	virtual explainMe::Cloud::Ptr & getPointCloud(){
		return cloud;
	}

	virtual std::vector<std::string> fileNames(int frame);

	void computeNormals(explainMe::Cloud::Ptr cloud);

	void storeCloud(explainMe::Cloud::Ptr cloud, int frame);

private:
	 void computeNormalsFast(explainMe::Cloud::Ptr cloud);
};

