#include "glDisplayable.h"
//#include <QtOpenGL>
#include "glDebugging.h"
#include <iostream>
#include <fstream>
glDisplayable::glDisplayable(std::string file_vshader,
		std::string file_fshader,
		std::string file_gshader)
{
	ishidden = false;
	setShader(file_vshader, file_fshader, file_gshader);
}


glDisplayable::~glDisplayable(void)
{
}

bool glDisplayable::prepareShaderProgram(/* const QString & vspath, const QString & fspath , const QString & gspath*/)
{
	const QString & vspath = m_vShader;
	const QString & fspath = m_fShader;
	const QString & gspath = m_gShader;
	// First we load and compile the vertex shader...
	m_shader.removeAllShaders();
	bool result = m_shader.addShaderFromSourceFile( QGLShader::Vertex, vspath );
	if ( !result )
		std::cout << m_shader.log().toStdString();

	// ...now the fragment shader...
	result = m_shader.addShaderFromSourceFile( QGLShader::Fragment, fspath );
	if ( !result )
		std::cout << m_shader.log().toStdString();

	//...optionally the geo shader...
	if(gspath != ""){
		//result = m_shader.addShaderFromSourceFile( QGLShader::Geometry, gspath );
		std::ifstream t(gspath.toStdString());
		std::string str((std::istreambuf_iterator<char>(t)),
			std::istreambuf_iterator<char>());

		result = m_shader.addShaderFromSourceCode(QGLShader::Geometry, str.c_str());
		//std::cout << "gshader after this line|\n";
		//std::cout << str;
		/*result = m_shader.addShaderFromSourceCode(QGLShader::Geometry, "#version 330 \n" 
			"uniform mat4 projection;\n" 
			"uniform mat4 modelview;\n" 
			"layout(triangles) in;\n"
			"layout(triangle_strip, max_vertices = 3) out;\n" 
			"in vec3[] position_v;\n" 
			"in int[] vertexId;\n" 
			"out vec4 frag_color;\n" 
			"flat out int frag_vertexId1, frag_vertexId2, frag_vertexId3;\n"
			"out vec3 vertexIdWeight;\n"
			"void main()\n"
			"{\n"
			"frag_vertexId1 = vertexId[0];"
			"frag_vertexId2 = vertexId[1];"
			"frag_vertexId3 = vertexId[2];;"

			"gl_PrimitiveID = gl_PrimitiveIDIn;"
			"frag_vertexId1 = vertexId[0];"

			//colors for debugging
			"frag_color = vec4((vertexId[0] % 5) / 5.f, (vertexId[0] % 7) / 14.f + 0.5f, (vertexId[0] % 11) / 11.f, 1);"

			// emit 3 verts of a triangle
			"vertexIdWeight = vec3(1, 0, 0);"
			"gl_Position = projection * modelview * vec4(position_v[0], 1);"
			"EmitVertex();"

			"vertexIdWeight = vec3(0, 1, 0);"
			"gl_Position = projection * modelview * vec4(position_v[0], 1);"
			"EmitVertex();"

			"vertexIdWeight = vec3(0, 0, 1);"
			"gl_Position = projection * modelview * vec4(position_v[0], 1);"
			"EmitVertex();"
		
			"}\n");*/
		if ( !result )
			std::cout << "G:" <<m_shader.log().toStdString();
		else{
			std::cout << "Successful geometry shader compilation.\n";
		}
	}

	// ...and finally we link them to resolve any references.
	result = m_shader.link();
	if ( !result )
		std::cout << m_shader.log().toStdString();

	if(!m_shader.bind()){
		qWarning() << "Could not bind shader program to the context";
		result = false;
	}

	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();
	return result;
}

void glDisplayable::setUpBuffer( const char *name, 
	QGLBuffer & buffer, 
	std::vector<tuple3f> & values, 
	QGLBuffer::UsagePattern type )
{

	if ( !m_shader.bind() )
	{
		qWarning() << "Could not bind shader program to context";
		return;
	}
	glDebugging::didIDoWrong();

	if(!buffer.isCreated()){
		buffer.create();
		buffer.setUsagePattern( type );
		glDebugging::didIDoWrong();
	}


	if ( !buffer.bind() )
	{
		qWarning() << "Could not bind vertex buffer to the context";
		return;
	}

	if(values.size() == 0){
		float dummy = 0;
		buffer.allocate(&dummy/**/, 3 * values.size()* sizeof( float ) );
	}
	else{
		buffer.allocate(&(values[0].x)/**/, 3 * values.size()* sizeof( float ) );
	}
	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();

	m_shader.setAttributeBuffer( name, GL_FLOAT, 0, 3 );
	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();
	m_shader.enableAttributeArray( name);
	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();

		//generate vao etc old style...
	/*typedef void (WINAPI *GL_GEN_VARRAYS)(GLsizei , GLuint *);
	typedef void (WINAPI *GL_BIND_VARRAY)(GLuint);
	GL_GEN_VARRAYS glGenVertexArrays = (GL_GEN_VARRAYS) wglGetProcAddress("glGenVertexArrays");
	GL_BIND_VARRAY glBindVertexArray = (GL_BIND_VARRAY) wglGetProcAddress("glBindVertexArray");
		uint vao;
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
	glDebuggingStuff::didIDoWrong();
	int loc = m_shader.attributeLocation(name);
	if(loc < 0){
		qWarning()<< "*** Invalid attribute name";
	}
	glDebuggingStuff::didIDoWrong();
	m_shader.setAttributeBuffer(loc,GL_FLOAT,0,3);
	m_shader.enableAttributeArray( name);*/

	buffer.release();
}

void glDisplayable::setUpBuffer( const char *name, 
	QGLBuffer & buffer, 
	std::vector<float> & values, 
	QGLBuffer::UsagePattern type,
	int tupleSize)
{
	if ( !m_shader.bind() )
	{
		qWarning() << "Could not bind shader program to context";
		return;
	}
	glDebugging::didIDoWrong();

	if(!buffer.isCreated()){
		buffer.create();
		buffer.setUsagePattern( type );
		glDebugging::didIDoWrong();
	}


	if ( !buffer.bind() )
	{
		qWarning() << "Could not bind vertex buffer to the context";
		return;
	}

	if(values.size() == 0){
		float dummy = 0;
		buffer.allocate(&dummy/**/,  values.size()* sizeof( float ) );
	}
	else{
		buffer.allocate(&(values[0])/**/,  values.size()* sizeof( float ) );
	}
	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();

	m_shader.setAttributeBuffer( name, GL_FLOAT, 0, tupleSize/*1*/);
	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();;
	m_shader.enableAttributeArray( name);
	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();



	buffer.release();
}

void glDisplayable::setUpIntBuffer(const char * name,
	QGLBuffer & buffer, 
	std::vector<int> & values, 
	QGLBuffer::UsagePattern type)
{
	if ( !m_shader.bind() )
	{
		qWarning() << "Could not bind shader program to context";
		return;
	}

	glDebugging::didIDoWrong();
		
	if(!buffer.isCreated()){
		buffer.create();
		buffer.setUsagePattern( type );
		glDebugging::didIDoWrong();
	}

	if ( !buffer.bind() )
	{
		qWarning() << "Could not bind vertex buffer to the context";
		return;
	}

	if(values.size() == 0){
		int dummy = 0;
		buffer.allocate(&dummy, values.size()* sizeof( int ) );
	}
	else{
		buffer.allocate(&(values[0]), values.size()* sizeof( int ) );
	}

	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();

	m_shader.setAttributeBuffer( name, GL_INT, 0,1);
	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();
	m_shader.enableAttributeArray( name);
	glDebugging::didIDoWrong();
	buffer.release();
}

void glDisplayable::setUpIndexBuffer(QGLBuffer & buffer, 
	std::vector<tuple3i> & values, 
	QGLBuffer::UsagePattern type)
{
	if ( !m_shader.bind() )
	{
		qWarning() << "Could not bind shader program to context";
		return;
	}
	glDebugging::didIDoWrong();

	/*if(!vao.isCreated()){
		vao.create();
	}
	vao.bind();*/
	buffer.create();
	buffer.setUsagePattern( type);

	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();

	if ( !buffer.bind() )
	{
		qWarning() << "Could not bind vertex buffer to the context";
		return;
	}

	if(values.size() == 0){
		int dummy = 0;
		buffer.allocate( /*idx*/&dummy, 3 * values.size()* sizeof( int ) );
	}
	else{
		buffer.allocate( /*idx*/&(values[0].a), 3 * values.size()* sizeof( int ) );
	}

	buffer.release();
	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();
}

void glDisplayable::setUpIndexBuffer(QGLBuffer & buffer, 
	std::vector<int> & values, 
	QGLBuffer::UsagePattern type)
{
	if ( !m_shader.bind() )
	{
		qWarning() << "Could not bind shader program to context";
		return;
	}
	glDebugging::didIDoWrong();

	buffer.create();
	buffer.setUsagePattern( type);

	glDebugging::didIDoWrong();

	if ( !buffer.bind() )
	{
		qWarning() << "Could not bind vertex buffer to the context";
		return;
	}

	if(values.size() == 0){
		int dummy = 0;
		buffer.allocate( /*idx*/&dummy, values.size()* sizeof( int ) );
	}
	else{
		buffer.allocate( /*idx*/&(values[0]), values.size()* sizeof( int ) );
	}
	

	buffer.release();
	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();
}

void glDisplayable::setUniformArray(const char * name, std::vector<QMatrix4x4> & mats, int sz)
{
	if(!get_m_shader().bind()){
		std::cout << "Could not bind shader program to the context";
	}
	get_m_shader().setUniformValueArray(name, & mats[0], sz);
	if(get_m_shader().uniformLocation(name)<0){
		std::cout << "Could not find uniform: " << name;
	}
	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();
}

void glDisplayable::setUniformValue( const char *name, QMatrix4x4 & mat )
{
	if(!get_m_shader().bind()){
		std::cout << "Could not bind shader program to the context";
	}
	get_m_shader().setUniformValue(name, mat);
	if(get_m_shader().uniformLocation(name)<0){
		std::cout << "Could not find uniform: " << name;
	}
	//glDebugging::didIDoWrong();
	glDebugging_didIDoWrong();
}

void glDisplayable::setUniformValue( const char *name, QVector3D & vec )
{
	if(!get_m_shader().bind()){
		qWarning() << "Could not bind shader program to the context";
	}
	get_m_shader().setUniformValue(name, vec);
	glDebugging::didIDoWrong();
}

void glDisplayable::setUniformValue( const char *name, float val )
{
	if(!get_m_shader().bind()){
		qWarning() << "Could not bind shader program to the context";
	}
	get_m_shader().setUniformValue(name, val);
	glDebugging::didIDoWrong();
}

void glDisplayable::enableBuffer( const char *name, QGLBuffer & buffer )
{
	if(!buffer.bind()){
		qWarning() << "*** Could not bind buffer";
	}
	m_shader.setAttributeArray(name, GL_FLOAT,0,3);
	m_shader.enableAttributeArray(name);
	glDebugging::didIDoWrong();

}

void glDisplayable::disableBuffer( const char * name )
{
	m_shader.disableAttributeArray(name);
	glDebugging::didIDoWrong();
}


