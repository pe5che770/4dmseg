#include "glOffscreenRenderer.h"
//#include "gl3w.h"
#include <QMainWindow>
#include <QtOpenGL/QGLWidget>
#include <QtOpenGL/QGLFormat>
#include <QMatrix4x4>
#include <iostream>
#include "glDebugging.h"
#include "sceneManager.h"
#include "Sensors.h"

class OffscreenContext:public QGLWidget{

	//glExampleTriangles * tri;
	glOffscreenRenderer * renderer;

public:
	OffscreenContext(QGLFormat & glFormat) :QGLWidget(glFormat){
		renderer = NULL;
	}
	~OffscreenContext(){
		//if(tri != NULL){
		//	delete tri;
		//}
	}

	void initializeGL(){
		//tri = new glExampleTriangles();
	}

	void setRenderer(glOffscreenRenderer * r){
		renderer = r;
	}
	void paintGL(){
		/*QMatrix4x4 proj;
		proj.setToIdentity();
		proj.perspective(VERTICAL_FOV,WIDTH*1.0/HEIGHT, 0.1, 100);
		QMatrix4x4 modelview;
		modelview.setToIdentity();
		modelview.lookAt(QVector3D(0,0,0.5), QVector3D(-0.6,0.2,0),QVector3D(0,1,0));

		this->makeCurrent();

		std::cout << "nanana";
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		if(!tri->isGlPrepared()){
			tri->glPrepare();
		}
		tri->setUniformValue("projection", proj);
		tri->setUniformValue("modelview", modelview);
		tri->glDraw();*/

		if(renderer != NULL){
			renderer->doOffscreenRendering2IntBuffer("");
		}
	}

};


glOffscreenRenderer::glOffscreenRenderer(
	RangeSensorDescription * desc,
	bool renderToScreen, 
	std::string vshader, 
	std::string fshader, 
	std::string gshader):
	m_renderToScreen(renderToScreen)
{

	sensor_desc = desc;

	//set shader
	m_vshader= vshader;
	m_gshader = gshader;
	m_fshader = fshader;

	////////////////////
	//set up scene and matrices
	////////////////////
	scene = new sceneManager();
	proj = new QMatrix4x4(desc->frustum());
	modelview = new QMatrix4x4();
	//modelview->setToIdentity();
	modelview->setToIdentity();
	modelview->lookAt(QVector3D(0,0,0), QVector3D(0,0,1),QVector3D(0,1,0));
	//modelview->lookAt(QVector3D(0,0,0.5), QVector3D(-0.6,0.2,0),QVector3D(0,1,0));
	
	//////////////////////////////
	//Setup Context using QT
	//////////////////////////////
	QGLFormat glFormat;
	glFormat.setVersion(4, 1);
	glFormat.setProfile(QGLFormat::CoreProfile); // Requires >=Qt-4.8.0
	glFormat.setSampleBuffers(true);

	OffscreenContext * c= new OffscreenContext(glFormat);

	c->setRenderer(this);
	offscreenContext = c;

	win = new QMainWindow();
	win->setMinimumSize(desc->width(),desc->height());
	if(renderToScreen){
		win->show();
	}
	else{
		win->hide();
	}
	win->setCentralWidget(offscreenContext);
	
	//right context...
	offscreenContext->makeCurrent();
	if(gl3wInit()){
		std::cout << "Failed to gl3wInit";
	}

	
	////////////////////////
	//set up opengl buffer, configure context
	///////////////////////

	//set viewport , enable depth test etx
	prepare();

	//setup data and shader programs..
	//glTriangles = new glExampleTriangles();
	/*scene->updateSurfelCloud("surfelCloud" , 
		explainMe::SurfelCloud::Ptr(new explainMe::SurfelCloud()),
		vshader,
		fshader,
		gshader);*/
		
	//setup FBO
	//generate a target fbo
	glGenFramebuffers(1, &m_framebufferName);
	glBindFramebuffer(GL_FRAMEBUFFER, m_framebufferName);


	//we need to allocate space for the output. Create it!

	//one
	/*glGenTextures(1, &m_textureName);
	// Configure the texture
	glBindTexture(GL_TEXTURE_2D, m_textureName);
	
	glTexImage2D(GL_TEXTURE_2D, 0,GL_R32I, desc->width(), desc->height(), 0,GL_RED_INTEGER,GL_INT, 0);

	//more than one buffer:
	/*/glGenTextures(2, m_textureNames);
	glBindTexture(GL_TEXTURE_2D, m_textureNames[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, desc->width(), desc->height(), 0,GL_RED_INTEGER,GL_INT, 0);
	glDebugging::didIDoWrong();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, m_textureNames[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, desc->width(), desc->height(), 0,GL_RED_INTEGER,GL_INT, 0);//*/

	glDebugging::didIDoWrong();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//The fbo can be rendered to and has a selection of attachement points.
	//By default there is nothing, but during rendering, if there are depth tests, a 
	//depth buffer is needed.
	//we need depth. Create it
	glGenRenderbuffers(1, &m_depthBufferName);
	glBindRenderbuffer(GL_RENDERBUFFER, m_depthBufferName);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, desc->width(), desc->height());

	//finally: set up the buffer
	//one
	/*glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_depthBufferName);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_textureName, 0);
	//more than one:
	/*/glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_textureNames[0], 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, m_textureNames[1], 0);//*/


	//gl: draw to the color attachements!

	//one
	/*GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers
	//more than one:
	/*/GLenum DrawBuffers[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
	glDrawBuffers(2, DrawBuffers); // "1" is the size of DrawBuffers//*/

	glDebugging::didIDoWrong();//*/

}


glOffscreenRenderer::~glOffscreenRenderer(void)
{
	offscreenContext->makeCurrent();
	
	glDeleteFramebuffers(1, &m_framebufferName);
	/*glDeleteTextures(1, &m_textureName);/*/
	glDeleteTextures(2, m_textureNames);//*/
	glDeleteRenderbuffers(1, &m_depthBufferName);

	//delete offscreenContext; //this is happening when win is deleted. 
	delete proj;
	delete modelview;
	delete win;
	

	//delete glSurfels;
	delete scene;
}


void glOffscreenRenderer::setCamera(Eigen::Matrix4f cameraTransform){
	scene->setCam(QMatrix4x4(
		cameraTransform(0, 0), cameraTransform(0, 1), cameraTransform(0, 2), cameraTransform(0, 3),
		cameraTransform(1, 0), cameraTransform(1, 1), cameraTransform(1, 2), cameraTransform(1, 3),
		cameraTransform(2, 0), cameraTransform(2, 1), cameraTransform(2, 2), cameraTransform(2, 3),
		cameraTransform(3, 0), cameraTransform(3, 1), cameraTransform(3, 2), cameraTransform(3, 3)
		));
}
void glOffscreenRenderer::prepare()
{
	glClearColor(0,0,0,0);
	glViewport(0,0,sensor_desc->width(),sensor_desc->height());
	glPointSize(1);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
}

void glOffscreenRenderer::updateTriangles(std::string targetName, 
		explainMe::Cloud::Ptr & points,
		std::vector<int> indices,
		std::string vshader , 
		std::string fshader , 
		std::string gshader )
{
	offscreenContext->makeCurrent();	
	render_target = targetName;
	if(vshader.size() != 0){
		scene->updateTriMesh(targetName, points,indices, vshader, fshader, gshader);
	}else{
		scene->updateTriMesh(targetName, points,indices, m_vshader, m_fshader, m_gshader);
	}

}

void glOffscreenRenderer::updateCloud(std::string targetName,
	explainMe::Cloud::Ptr & points)
{
	offscreenContext->makeCurrent();
	render_target = targetName;
	scene->updateCloud(targetName, points, false);
}

void glOffscreenRenderer::doOffscreenRendering2IntBuffer(std::string targetName, int * target)//std::vector<GLint> * target)
{
	if(targetName.length() != 0){
		render_target = targetName;
	}
	offscreenContext->makeCurrent();	
	if(m_renderToScreen){
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	else{
		glBindFramebuffer(GL_FRAMEBUFFER, m_framebufferName);
	}

	//render the target.
	glDisplayable * displayable = scene->get(render_target);
	if(displayable != NULL){
		if(!displayable->isGlPrepared()){
			displayable->glPrepare();
		}
		if(!displayable->isGlUpToDate()){
			displayable->glUpdate();
		}

		displayable->setUniformValue("projection", *proj);
		displayable->setUniformValue("modelview", (*modelview)*scene->getCam());
		glDebugging::didIDoWrong();

		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	//	glTriangles->glDraw();
		displayable->glDraw();
		glDebugging::didIDoWrong();

		//Read stuff to target.
		if(target != NULL && !m_renderToScreen){
			glBindFramebuffer(GL_FRAMEBUFFER, m_framebufferName);

//			std::vector<GLint> & pix = *target;
			
			//specify read buffer
			glReadBuffer( GL_COLOR_ATTACHMENT0 );
			glReadPixels(0, 0, sensor_desc->width(), sensor_desc->height(), GL_RED_INTEGER, GL_INT, target);//& pix[0]);
			glDebugging::didIDoWrong();
		}
		else if(target!= NULL && m_renderToScreen){
			std::cout << "Warning ins glOffscreenRenderer: non null target but onscreen rendering\n";
		}

		if(m_renderToScreen){
			offscreenContext->update();
		}
	}
	else{
		std::cout << "\n ERROR IN glOffscreenRendere, unknown target: " << render_target << " \n";
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void glOffscreenRenderer::doOffscreenRendering2IntBuffer(std::string targetName, std::vector<GLint> * target, std::vector<GLint> * target2)
{
	if(targetName.length() != 0){
		render_target = targetName;
	}
	offscreenContext->makeCurrent();	
	if(m_renderToScreen){
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	else{
		glBindFramebuffer(GL_FRAMEBUFFER, m_framebufferName);
	}

	//render the target.
	glDisplayable * displayable = scene->get(render_target);
	if(displayable != NULL){
		if(!displayable->isGlPrepared()){
			displayable->glPrepare();
		}
		if(!displayable->isGlUpToDate()){
			displayable->glUpdate();
		}

		displayable->setUniformValue("projection", *proj);
		displayable->setUniformValue("modelview", scene->getCam() * (*modelview));
		glDebugging::didIDoWrong();

		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		displayable->glDraw();
		glDebugging::didIDoWrong();

		//Read stuff to target.
		if(target != NULL && !m_renderToScreen){
			glBindFramebuffer(GL_FRAMEBUFFER, m_framebufferName);

			std::vector<GLint> & pix = *target;
			//pix.resize(WIDTH * HEIGHT);
			//specify readbuffer
			glReadBuffer( GL_COLOR_ATTACHMENT0 );
			glReadPixels(0, 0, sensor_desc->width(), sensor_desc->height(), GL_RED_INTEGER, GL_INT, & pix[0]);
			glDebugging::didIDoWrong();

			if(target2!= NULL){
				glReadBuffer( GL_COLOR_ATTACHMENT1 );
				glReadPixels(0, 0, sensor_desc->width(), sensor_desc->height(), GL_RED_INTEGER, GL_INT, & (*target2)[0]);
			}
		}
		else if(target!= NULL && m_renderToScreen){
			std::cout << "Warning ins glOffscreenRenderer: non null target but onscreen rendering\n";
		}

		if(m_renderToScreen){
			offscreenContext->update();
		}
	}
	else{
		std::cout << "\n ERROR IN glOffscreenRendere, unknown target: " << render_target << " \n";
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}



