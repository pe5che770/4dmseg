#pragma once
#include <QMatrix4x4>
#include <QVector3D>
#include "glDisplayable.h"
#include <vector>
#include "glInstance.h"
#include <map>

#ifndef Q_MOC_RUN
#include "trajectory.h"
#include "explainMeDefinitions.h"
#endif

/*#include "Surfel.h"
#include "glBBoxes.h"
#include "enhancedSurfelCloud.h"*/

class glPointCloud;
class glCorrespondences;
class glTriMesh;
/*class glSurflets;
class glSurfelVariance;
class glEnhancedSurfelModel;*/


class sceneManager:public QObject
{
	Q_OBJECT
private:
	std::map<std::string, glPointCloud*> clouds;
	std::map<std::string, glTriMesh*> trimeshs;
/*	std::map<std::string, glSurflets*> surflets;
	std::map<std::string, glEnhancedSurfelModel*> surfelModels;
	std::map<std::string, glBBoxes*> bboxes;*/
	std::map<std::string, glCorrespondences*> correspondences;
	/*std::map<std::string, glSurfelVariance*> surfelVariances;*/

	std::map<std::string, glInstance*> displayables_by_name;

	std::vector<bool> showDisplayable;

public:
	QVector3D eye, lookAt;
	QMatrix4x4 cam;//, proj;
	std::vector<glInstance*> displayables;
public:
	sceneManager(void);
	~sceneManager(void);

	void hideAll();
	void setVisible(const std::string & name, bool what = true);
	void updateCam(QVector3D eye_, QVector3D lookAt_);
	QMatrix4x4 & getCam();
	void setCam(QMatrix4x4 cameraTransform);
	void setPosition(std::string displayable_name, QMatrix4x4 & mat);

	int numberDisplayables();
	glDisplayable * get(int i);
	glDisplayable * get(std::string name);
	//QMatrix4x4 & getProj();

	void addAndManage(glDisplayable * toDisplay);

	void updateCloud(std::string  param1, std::vector<pcl::PointXYZ> & ps, std::vector<pcl::PointXYZ> & cols );
	void updateCloud(std::string  param1, std::vector<Eigen::Vector3f> & ps, std::vector<Eigen::Vector3f> & cols );
	void updateCloud( std::string  param1, const explainMe::Cloud::ConstPtr & cloud, bool useColor = true );
	void updateCloud( std::string  param1, const explainMe::Cloud::ConstPtr & cloud, std::vector<Eigen::Vector3f> & cols );
	

	void updateCorrespondences(std::string  param1, std::vector<trajectory_R> & paths, std::vector<Eigen::Vector3f> & colors);
	void updateCorrespondences(std::string  param1, std::vector<pcl::PointXYZ> & cloud_a,
		std::vector<pcl::PointXYZ> &  cloud_b);
	void updateCorrespondences(std::string  param1, std::vector<Eigen::Vector3f> & cloud_a,
		std::vector<Eigen::Vector3f> &  cloud_b);
	void updateCorrespondences(std::string param1,
		std::vector<Eigen::Vector3f> & cloud_a,
		std::vector<Eigen::Vector3f> & cloud_b,
		std::vector<Eigen::Vector3f> & color);

	void updateTriMesh(std::string  param1, const explainMe::Cloud::ConstPtr & cloud,
		std::vector<int>& ind, std::string vshader,
		std::string fshader, std::string gshader = "", bool useColor = false);

	void resetAllObject2WorldMatrices();
	/*void updateTriMesh( std::string  param1, const explainMe::Cloud::ConstPtr & cloud,std::vector<int>& ind, bool useColor = true );
	

	void updateTriMesh(std::string  param1, std::vector<Eigen::Vector3f> & pos,std::vector<Eigen::Vector3f> & col,
		std::vector<int> & ind);*/

	
	/*void updateSurfelCloud(std::string  param1, pcl::PointCloud<Surfel>::Ptr & cloud );
	
	void updateSurfelModel(std::string  param1, enhancedSurfelModel::Ptr & cloud , 
		std::string vertShader = "", std::string fragShader = "", std::string geomShader = "");
	void updateSurfelModel(std::string  param1, enhancedSurfelModel::Ptr & cloud , 
		std::vector<Eigen::Matrix4f> &alignments_perObject, 
		std::string vertShader = "", std::string fragShader = "", std::string geomShader = "");

	void updateSurfelCloud(std::string  param1, explainMe::SurfelCloud::Ptr & cloud , 
		std::string vertShader = "", std::string fragShader = "", std::string geomShader = "");
	void updateSurfelCloud(std::string  param1, std::vector<explainMe::SurfelCloud::Ptr> & clouds , 
		std::string vertShader = "", std::string fragShader = "", std::string geomShader = "");
	void updateSurfelCloud(std::string  param1, std::vector<enhancedSurfelCloud::Ptr> & clouds , 
		std::string vertShader = "", std::string fragShader = "", std::string geomShader = "");
	
	void updateSurfelVariances(std::string  param1, explainMe::SurfelCloud::Ptr & cloud);

	void updateCorrespondences(std::string  param1, pcl::PointCloud<Surfel>::Ptr & cloud_a,
		explainMe::Cloud::Ptr & cloud_b, Eigen::MatrixXi & bToA);
		
	void updateCorrespondences(std::string  param1, explainMe::SurfelCloud::Ptr & cloud_a,
		std::vector<std::vector<int>> & corresp);
		

	
	void updateCorrespondences(std::string  param1, std::vector<std::vector<pcl::PointXYZ>> & paths);
	void updateCorrespondences(std::string  param1, std::vector<std::vector<pcl::PointXYZ>> & paths, std::vector<Eigen::Vector3f> & colors);
	//
	*/
	

private:
	void displayables_push_back(std::string param1, glInstance * glThing);

public Q_SLOTS:
	void onRotate(QMatrix4x4 & );
	void onCamMovement(QMatrix4x4 &);


	//kinectProvider * prov;
	
};

