#pragma once
#include "explainMe_algorithm_withFeatures.h"
class cudaDataterm;
class recursiveFilter;
template<typename T, typename container>
class matrix;

class crf_segmentation
{
	explainMe_algorithm_withFeatures::Ptr alg;
	int num_crf_iterations;
public:
	crf_segmentation(explainMe_algorithm_withFeatures::Ptr algo, int numIt);
	~crf_segmentation();

	void doSparseCrf(cudaDataterm * cuda_dev,
		std::vector<pcl::PointXYZINormal> & seed_points,
		std::vector<int> & seed_cloud_idx,
		std::vector<int> & seed_point_idx,
		//how strongly to weight temporal consistency
		float lambda_temporal,
		float lambda_spatial,
		//how strongly to weight far away frames.
		float sigma_temporal,
		float sigma_spatial,
		float sigma_sp_factor,
		float max_crf_factor,
		//how much support to ask for in the minimum
		float kill_threshold,
		float per_hyp_cost_factor,
		explainMe::MatrixXf_rm & out, int mainIteration=-1);

	void do_sparse_crf_helper(cudaDataterm * cuda_dev,
		float lambda_temporal,
		float lambda_spatial,
		//how strongly to weight far away frames.
		float sigma_temporal,
		float sigma_spatia, 
		float sigma_sp_factor,
		float start_crf_factotr,
		float max_crf_factor,std::vector<int> & labels_to_use,
		bool initFromCPU, int numIt);

	void doSparseSequentialCrf(cudaDataterm * cuda_dev,
		std::vector<pcl::PointXYZINormal> & seed_points,
		std::vector<int> & seed_cloud_idx,
		std::vector<int> & seed_point_idx,
		//how strongly to weight temporal consistency
		float lambda_temporal,
		float lambda_spatial,
		//how strongly to weight far away frames.
		float sigma_temporal,
		float sigma_spatial,//deprecated
		float sigma_spatial_factor,
		float max_anneal_factor,
		//how much support to ask for in the minimum
		float kill_threshold,
		explainMe::MatrixXf_rm & out, int mainIteration = -1);



	float energy(
		int cuda_dev_channel, std::vector<int> & superLabel,
		cudaDataterm * cuda_dev, std::vector<float> & buffer/* buffer*/,
		float sigma_spatial, float sigma_t, float sigma_sp_factor, std::vector<int> & sparse_labels);
	float energyI(
		int cuda_dev_channel,std::vector<int> & superLabel,
		cudaDataterm * cuda_dev,std::vector<float> & buffer/* buffer*/,
		float sigma_spatial, float sigma_t, float sigma_sp_factor, 
		std::vector<int> & sparse_labels);
	float energyII(
		int cuda_dev_channel, std::vector<int> & superLabel,
		cudaDataterm * cuda_dev, std::vector<float> & buffer/* buffer*/,
		float sigma_spatial, float sigma_t, float sigma_sp_factor,
		//optionally store the per pixel energy
		std::vector<std::vector<float>> * per_pixel_energy = NULL,
		bool relax = false
		);
	//doesn't punish being in different labels, only punishes registration.
	/*float energyII_relaxed(
		int cuda_dev_channel, std::vector<int> & superLabel,
		cudaDataterm * cuda_dev, std::vector<float> & buffer,
		float sigma_spatial, float sigma_t,
		//optionally store the per pixel energy
		std::vector<std::vector<float>> * per_pixel_energy = NULL
		);*/

	//retruns the merged labels
	std::vector<std::vector<int>> minimizeEnergyByMergingLabels(cudaDataterm * cuda_dev,
		std::vector<float> & buffer, 
		float sigma_sp, float sigma_t, float sigma_sp_factor,
		std::vector<int> & sparse_labels, 
		explainMe::MatrixXf_rm & sparse_seg_to_fuse, 
		float hypothesis_cost_factor,
		std::vector<std::pair<int, int>> mergeStrategy,
		bool set_to_0_instead_of_fuse = false);

	std::vector<std::vector<float>> computeEnergyFrames(cudaDataterm * cuda_dev,
		float sigma_sp, float sigma_t, float sigma_sp_factor, bool relax = false
		);
	std::vector<std::vector<float>> computeEnergyOneHypAlone(cudaDataterm * cuda_dev,
		float sigma_sp, float sigma_t, float sigma_sp_factor, int label
		);

	std::vector<std::vector<float>> spatioTemporalSmear(cudaDataterm * cuda_dev,
		float sigma_sp, float sigma_t, 
		explainMe::Point & pos, int frame, int motion
		);

private:
	void copy_cpu_dense_segweights_to_gpu(cudaDataterm * cuda_dev, std::vector<explainMe::MatrixXf_rm> & perCloudVals, int hyp, std::vector<float> & buffer);

	void copyDenseResults_Data2Alg(int frame, std::vector<matrix<float, std::vector<float>>>& data);
	void copyDenseResults_alg2Gpu(int hyp, std::vector<float> & buffer, cudaDataterm * cuda_dev);
	void copyCombinedDenseResults_alg2Gpu(std::vector<int>& hyps, std::vector<float> & buffer, cudaDataterm * cuda_dev);

	void filter_sparse2dense_spatial_crf_allHyp_but_store_on_cpu(cudaDataterm * cuda_dev,
		float sigma_spatial, float lambda_spatial, float sigma_factor,
		std::vector<explainMe::MatrixXf_rm> & where_to_store,
		std::vector<float> & buffer,
		StopWatch & spatial_filter, StopWatch & spatial_cpy, bool doNormalization = true, int numSurroundingFrames = 5);


	void sparseSequentialCrf_perFrame_storeResultsInAlg(cudaDataterm * cuda_dev,
		float scale,
		float sigma_sp,
		float factor_sigma/*,
		std::vector<std::vector<float>> & dense_buffer1,
		std::vector<std::vector<float>> & dense_buffer2,
		std::vector<std::vector<float>> & dense_buffer3*/);
	void infiniteBoxKernelCRF_perFrame_storeResultsInAlg(cudaDataterm * cuda_dev,
		float scale
		);

	float spatial_sequential_crf(
		matrix<explainMe::Point, explainMe::Cloud> & cloud_as_grid,
		std::vector<matrix<float, std::vector<float>>> & q_by_label_in,
		std::vector<matrix<float, std::vector<float>>> & q_by_label_out,
		float nrgScale,
		float sigma_factor,
		recursiveFilter & recursive_filter);


};
