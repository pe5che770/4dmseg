# 4dmseg

Currently the method does not perform as well as it used to - I am still looking for the bug!
What will change
### Definitely:
* Getting back qualitative performance
* Kick out subprojects
* simplify and kick out dead code. Especially the core parts:
    * simplify cuda part
    * experiment.cpp
    * explainMe_algorithm_withFeatures.cpp
    * crf_segmentation.cpp
* Simplify output (lots of stuff dumped currently)
* Get Infinitam back
### Possibly:
* Simplifying and kicking out dependencies (PCL)
* Switch to opencv3
* rely on tool (like hunter) to download and compile the dependencies.
* provide precompiled binaries
*  Add back tools to inspect and record data

### How to currently get started on windows 10, 64 bit.
Windows is a pain. This will take a while.
* Download and build dependencies (64 bit build!) (tested with vs studio 2013) :
    * PCL (Point Cloud Library), tested with pcl 1.7.2 (no need for VTK or any non-core part)
    * Boost (1.6.2)
    * qt (5.7)
    * opencv 2.x
    * cuda
* Run Cmake: Open the cmake gui. run configure. If configure does not work:
    * set PCL_dir to point to pcls cmake folder. (used pcl 1.7.2)
    * specify or add the field BOOST_LIBRARYDIR , pointing to the directory containing the lib files (boost 1.62)
    * If qt is not found: set the Qt_qmake_executable to point to qmake.exe (qt 5.7)
* Once configure runs: open the VS propject and build (Release mode)

### Running the code
To successfully run the program, the following dlls need to be copied into the folder containing the exe (alternatively they need to be on the path - but when having multiple projects messing up the path potentially is a bad idea):
* pcl dlls
* opneNi2 dlls
* QT5 dlls
Run the code:
* download the Data: Link: [ambush5](https://1drv.ms/u/s!Al3VerlKfxqtgYFZiAKP-Q4wlMgZsA) TODO all data.
* navigate to the Example folder
* in the _parameters subfolder: update the buildFolder to point to where you built the project. The folder should contain the * Release subfolder containing the .exe and dlls.
* E.g. choose the subfolder ambush5
* In the .cfg file: adapt the folder to point to the downloaded data
* Open a cmd prompt in that folder, call run_it
