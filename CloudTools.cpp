#include "CloudTools.h"


CloudTools::CloudTools(void)
{
}


CloudTools::~CloudTools(void)
{
}

template<class T>
void CloudTools::apply(Eigen::Matrix4f & mat, /*explainMe::Cloud::Ptr*/T cloud){

	for(int i = 0; i < cloud->size(); i++){
		cloud->at(i).getNormalVector3fMap() = mat.topLeftCorner<3, 3>() *cloud->at(i).getNormalVector3fMap();
		cloud->at(i).getVector4fMap() = mat *cloud->at(i).getVector4fMap();
	}
}

template void CloudTools::apply<explainMe::Cloud::Ptr>(Eigen::Matrix4f & mat, explainMe::Cloud::Ptr cloud);


void CloudTools::apply(
	std::vector<Eigen::Matrix4f> & mat, 
	explainMe::Cloud::Ptr cloud, 
	Eigen::ArrayXXi & transformations)
{
	int t;
	int nTrans = mat.size();
	for(int i = 0; i < cloud->size(); i++){
		t= transformations(i);
		if(t>= 0 && t < nTrans){
			cloud->at(i).getNormalVector3fMap() = mat[t].topLeftCorner<3,3>() *cloud->at(i).getNormalVector3fMap();
			cloud->at(i).getVector4fMap() = mat[t] *cloud->at(i).getVector4fMap();
		}
	}
}


Eigen::Vector3f CloudTools::compute3DCentroid( explainMe::Cloud::Ptr & organizedCloud, explainMe::MatrixXb & mask )
{
	Eigen::Vector3f c;
	c.fill(0);
	int num = 0;
	for(int i = 0; i < organizedCloud->width; i++){
		for(int j = 0; j< organizedCloud->height; j++){
			if(mask(i,j) && organizedCloud->at(i,j).z*0 == 0){
				c[0] += organizedCloud->at(i,j).x;
				c[1] += organizedCloud->at(i,j).y;
				c[2] += organizedCloud->at(i,j).z;	
				num++;
			}
		}
	}
	c/= num;
	return c;
}

Eigen::Vector3f CloudTools::compute3DCentroid( explainMe::Cloud::Ptr & organizedCloud, explainMe::MatrixXf_rm & w, int w_i)
{
	Eigen::Vector3f c;
	c.fill(0);
	float num = 0;
	for(int i = 0; i < organizedCloud->size(); i++){
		if( organizedCloud->at(i).z*0 == 0){
			c[0] += organizedCloud->at(i).x * w(i,w_i);
			c[1] += organizedCloud->at(i).y* w(i,w_i);
			c[2] += organizedCloud->at(i).z* w(i,w_i);	
			num +=  w(i,w_i);
		}
		
	}
	c/= (num + 1e-4);
	return c;
}

Eigen::Vector3f CloudTools::compute3DCentroid( pcl::PointCloud<pcl::PointSurfel>::Ptr & surfelCloud, bool ignore )
{
	Eigen::Vector3f c;
	c.fill(0);
	int num = 0;
	for(int i = 0; i < surfelCloud->size() ; i++){
		/*c[0] += surfelCloud->at(i).x;
		c[1] += surfelCloud->at(i).y;
		c[2] += surfelCloud->at(i).z;*/
		if(!(surfelCloud->at(i).z*0 == 0)){
			continue;
		}
		if(!ignore){
			c+= surfelCloud->at(i).getVector3fMap();
			num++;
		}
		else if(surfelCloud->at(i).normal_z <0.3){
			c+= surfelCloud->at(i).getVector3fMap();
			num++;
		}
	}
	c/= num;
	return c;
}

Eigen::Vector3f CloudTools::compute3DCentroid( explainMe::Cloud::Ptr & cloud )
{
	Eigen::Vector3f c;
	c.fill(0);
	int num = 0;
	for(int i = 0; i < cloud->size() ; i++){
		/*c[0] += surfelCloud->at(i).x;
		c[1] += surfelCloud->at(i).y;
		c[2] += surfelCloud->at(i).z;*/
		c+= cloud->at(i).getVector3fMap();
		num++;
	}
	c/= num;
	return c;
}

Eigen::Vector3f CloudTools::compute3DCentroid( explainMe::Cloud & cloud )
{
	Eigen::Vector3f c;
	c.fill(0);
	int num = 0;
	for(int i = 0; i < cloud.size() ; i++){
		c+= cloud.at(i).getVector3fMap();
		num++;
	}
	c/= num;
	return c;
}
