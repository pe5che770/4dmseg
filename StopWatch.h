#pragma once
#include <boost/date_time/posix_time/posix_time.hpp>
#include <map>
#include <string>
class StopWatch
{
	long t_elapsed;
	boost::posix_time::ptime start;
	bool started;
public:
	StopWatch(void){ t_elapsed = 0; started = false; }
	~StopWatch(void){}

	void restart(){
		if (!started){
			start = boost::posix_time::microsec_clock::local_time();
			started = true;
		}
	}
	void stop();

	void reset(){
		t_elapsed = 0;
	}

	long total(){
		if (started){
			return t_elapsed + (boost::posix_time::microsec_clock::local_time() - start).total_milliseconds();
		}
		return t_elapsed;
	}
};

class allTimings
{
	std::map<std::string, StopWatch> allWatches;
public:
	allTimings(){}
	~allTimings(){}

	StopWatch & getWatch(std::string name){
		auto w = allWatches.find(name);
		if(w != allWatches.end()){
			return w->second;
		}
		else{
			allWatches[name] = StopWatch();
			return allWatches[name];
		}
	}

	void start(const std::string &  name){
		getWatch(name).restart();
	}

	void stop(const std::string & name){
		getWatch(name).stop();
	}

	void printAll(){
		std::cout << "\nTimings: \n";
		for(auto it = allWatches.begin(); it!= allWatches.end(); it++){
			std::cout << it->first << " : \t" << it->second.total() << "\n";
		}
	}

	std::string to_string(){
		std::string st =  "";
		for (auto it = allWatches.begin(); it != allWatches.end(); it++){
			st = st + (it->first) + " : " + std::to_string(it->second.total()) + "\t";
		}
		return st;
	}
};